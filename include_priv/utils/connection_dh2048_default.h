/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
static DH* get_dh2048(void) {
    static unsigned char dhp_2048[] = {
        0xBD, 0x14, 0x14, 0xAE, 0xBC, 0xF4, 0xE7, 0xC1, 0xD7, 0xE5,
        0x71, 0x29, 0x82, 0xD1, 0x3E, 0xEF, 0x1B, 0x24, 0xC5, 0x4F,
        0x74, 0x09, 0x83, 0xEF, 0xF6, 0xF4, 0xA3, 0xE4, 0x65, 0xB7,
        0x6E, 0x92, 0xD4, 0xAC, 0xC3, 0x21, 0x26, 0xCA, 0xB1, 0x9D,
        0xA4, 0xC9, 0xAA, 0xC8, 0x7A, 0x7E, 0xAA, 0xE5, 0x89, 0xB2,
        0x98, 0xE6, 0x18, 0xB1, 0x3C, 0x3A, 0x1B, 0x52, 0xB5, 0xAA,
        0x9F, 0x10, 0x4E, 0x0B, 0x89, 0xC2, 0x27, 0x76, 0xB3, 0xEF,
        0x98, 0xCB, 0x7B, 0x1D, 0x03, 0x01, 0x6C, 0x47, 0x4E, 0xEB,
        0xA1, 0x9B, 0x21, 0x6E, 0x71, 0x89, 0xC5, 0xFD, 0x60, 0x4C,
        0x49, 0x4D, 0xDE, 0x2F, 0x1F, 0x31, 0x71, 0x16, 0x00, 0x8A,
        0x6F, 0xDE, 0x55, 0xBC, 0x2E, 0xE6, 0x29, 0x1E, 0x65, 0xA8,
        0xC5, 0xF1, 0x70, 0x26, 0xE0, 0x64, 0x9D, 0x94, 0xD8, 0xDF,
        0x10, 0x75, 0x68, 0xE1, 0xAB, 0xC3, 0xEC, 0xA7, 0xB4, 0x35,
        0x70, 0x63, 0x7E, 0xBF, 0x99, 0x5B, 0xC0, 0x1F, 0x0A, 0xD0,
        0x12, 0x8B, 0xEB, 0x1F, 0x1F, 0xC6, 0xD8, 0x98, 0xA3, 0x50,
        0xB6, 0x01, 0x41, 0x84, 0x97, 0xC5, 0xEC, 0x99, 0x30, 0x58,
        0x8D, 0x45, 0xD1, 0x0F, 0x86, 0x6E, 0x02, 0x6D, 0xB0, 0xBA,
        0x4F, 0x35, 0xAE, 0x13, 0xFD, 0xA8, 0x39, 0x04, 0x71, 0x45,
        0x9A, 0x4B, 0xCB, 0x43, 0xAD, 0x4B, 0xAA, 0x0E, 0x44, 0x52,
        0x4A, 0x12, 0x93, 0xD5, 0xEE, 0xAE, 0x98, 0x46, 0x18, 0x37,
        0x21, 0xEA, 0xB1, 0xE9, 0x69, 0x4E, 0xA2, 0xFB, 0x89, 0xE8,
        0x26, 0xD7, 0x78, 0x8C, 0xB5, 0x55, 0x4C, 0x60, 0x8D, 0x4E,
        0x91, 0xA1, 0xCA, 0x7F, 0xAD, 0xD5, 0x35, 0xD3, 0x8C, 0x33,
        0x79, 0x23, 0x6A, 0xE0, 0x7F, 0xE3, 0x3C, 0x31, 0x96, 0x97,
        0x80, 0x0F, 0x05, 0x9B, 0x1C, 0xC1, 0xA9, 0x22, 0x9A, 0xC4,
        0xEE, 0x98, 0x0A, 0xDF, 0x6A, 0x4B
    };
    static unsigned char dhg_2048[] = {
        0x02
    };
    DH* dh = DH_new();
    BIGNUM* p, * g;

    if(dh == NULL) {
        return NULL;
    }
    p = BN_bin2bn(dhp_2048, sizeof(dhp_2048), NULL);
    g = BN_bin2bn(dhg_2048, sizeof(dhg_2048), NULL);
    if((p == NULL) || (g == NULL)
       || !DH_set0_pqg(dh, p, NULL, g)) {
        DH_free(dh);
        BN_free(p);
        BN_free(g);
        return NULL;
    }
    return dh;
}
