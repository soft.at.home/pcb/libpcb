/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifdef CAP_DOES_NOT_EXIST
REPLACE_CAP_MACRO(CAP_DOES_NOT_EXIST)
#endif


#ifdef CAP_CHOWN
REPLACE_CAP_MACRO(CAP_CHOWN)
#endif

#ifdef CAP_DAC_OVERRIDE
REPLACE_CAP_MACRO(CAP_DAC_OVERRIDE)
#endif

#ifdef CAP_DAC_READ_SEARCH
REPLACE_CAP_MACRO(CAP_DAC_READ_SEARCH)
#endif

#ifdef CAP_FOWNER
REPLACE_CAP_MACRO(CAP_FOWNER)
#endif

#ifdef CAP_FSETID
REPLACE_CAP_MACRO(CAP_FSETID)
#endif

#ifdef CAP_KILL
REPLACE_CAP_MACRO(CAP_KILL)
#endif

#ifdef CAP_SETGID
REPLACE_CAP_MACRO(CAP_SETGID)
#endif

#ifdef CAP_SETUID
REPLACE_CAP_MACRO(CAP_SETUID)
#endif

#ifdef CAP_SETPCAP
REPLACE_CAP_MACRO(CAP_SETPCAP)
#endif

#ifdef CAP_LINUX_IMMUTABLE
REPLACE_CAP_MACRO(CAP_LINUX_IMMUTABLE)
#endif

#ifdef CAP_NET_BIND_SERVICE
REPLACE_CAP_MACRO(CAP_NET_BIND_SERVICE)
#endif

#ifdef CAP_NET_BROADCAST
REPLACE_CAP_MACRO(CAP_NET_BROADCAST)
#endif

#ifdef CAP_NET_ADMIN
REPLACE_CAP_MACRO(CAP_NET_ADMIN)
#endif

#ifdef CAP_NET_RAW
REPLACE_CAP_MACRO(CAP_NET_RAW)
#endif

#ifdef CAP_IPC_LOCK
REPLACE_CAP_MACRO(CAP_IPC_LOCK)
#endif

#ifdef CAP_IPC_OWNER
REPLACE_CAP_MACRO(CAP_IPC_OWNER)
#endif

#ifdef CAP_SYS_MODULE
REPLACE_CAP_MACRO(CAP_SYS_MODULE)
#endif

#ifdef CAP_SYS_RAWIO
REPLACE_CAP_MACRO(CAP_SYS_RAWIO)
#endif

#ifdef CAP_SYS_CHROOT
REPLACE_CAP_MACRO(CAP_SYS_CHROOT)
#endif

#ifdef CAP_SYS_PTRACE
REPLACE_CAP_MACRO(CAP_SYS_PTRACE)
#endif

#ifdef CAP_SYS_PACCT
REPLACE_CAP_MACRO(CAP_SYS_PACCT)
#endif

#ifdef CAP_SYS_ADMIN
REPLACE_CAP_MACRO(CAP_SYS_ADMIN)
#endif

#ifdef CAP_SYS_BOOT
REPLACE_CAP_MACRO(CAP_SYS_BOOT)
#endif

#ifdef CAP_SYS_NICE
REPLACE_CAP_MACRO(CAP_SYS_NICE)
#endif

#ifdef CAP_SYS_RESOURCE
REPLACE_CAP_MACRO(CAP_SYS_RESOURCE)
#endif

#ifdef CAP_SYS_TIME
REPLACE_CAP_MACRO(CAP_SYS_TIME)
#endif

#ifdef CAP_SYS_TTY_CONFIG
REPLACE_CAP_MACRO(CAP_SYS_TTY_CONFIG)
#endif

#ifdef CAP_DOES_NOT_EXIST
REPLACE_CAP_MACRO(CAP_DOES_NO_EXIST)
#endif

#ifdef CAP_MKNOD
REPLACE_CAP_MACRO(CAP_MKNOD)
#endif

#ifdef CAP_LEASE
REPLACE_CAP_MACRO(CAP_LEASE)
#endif

#ifdef CAP_AUDIT_WRITE
REPLACE_CAP_MACRO(CAP_AUDIT_WRITE)
#endif

#ifdef CAP_AUDIT_CONTROL
REPLACE_CAP_MACRO(CAP_AUDIT_CONTROL)
#endif

#ifdef CAP_SETFCAP
REPLACE_CAP_MACRO(CAP_SETFCAP)
#endif

#ifdef CAP_MAC_OVERRIDE
REPLACE_CAP_MACRO(CAP_MAC_OVERRIDE)
#endif

#ifdef CAP_MAC_ADMIN
REPLACE_CAP_MACRO(CAP_MAC_ADMIN)
#endif

#ifdef CAP_SYSLOG
REPLACE_CAP_MACRO(CAP_SYSLOG)
#endif

#ifdef CAP_WAKE_ALARM
REPLACE_CAP_MACRO(CAP_WAKE_ALARM)
#endif

#ifdef CAP_EPOLLWAKEUP // https://lore.kernel.org/patchwork/patch/314095/
REPLACE_CAP_MACRO(CAP_EPOLLWAKEUP)
#endif
#ifdef CAP_BLOCK_SUSPEND
REPLACE_CAP_MACRO(CAP_BLOCK_SUSPEND)
#endif

#ifdef CAP_AUDIT_READ
REPLACE_CAP_MACRO(CAP_AUDIT_READ)
#endif

