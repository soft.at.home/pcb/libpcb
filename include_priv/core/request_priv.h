/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(PCB_REQUEST_PRIV_H)
#define PCB_REQUEST_PRIV_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <pcb/core/request.h>

#include "reply_priv.h"
#include "datamodel_priv.h"

typedef struct _param_value {
    char* parameterName;
    variant_t value;
    llist_iterator_t it;
} param_value_t;

struct _request {
    uint32_t request_id;
    request_type_t type;
    request_states_t state;
    reply_t reply;
    request_replyHandler replyHandler;
    request_replyChildHandler replyChildHandler;
    request_replyItemHandler replyItemHandler;
    request_doneHandler doneHandler;
    request_cancelHandler cancelHandler;
    request_destroyHandler destroyHandler;
    void* userdata;
    void* serdata;                      /** Data stored by (de)serializer */
    request_cleanupSerializationData cleanupSerializationHandler;
    llist_iterator_t it;                /** Generic list iterator, is used to store notify request in objects, pending request list or received request list */
    llist_iterator_t forwarded_it;      /** List iterator used to store the request in the peer_info_t forwarded list */
    llist_iterator_t notify_it;         /** List iterator used to store the request in the peer_info_t structure  */
    llist_iterator_t list_it;           /** List iterator used to store the request in a request list */
    tree_item_t child_requests;         /** Child requests */
    peer_info_t* peer;                  /** Peer on which the request was sent, NULL for received requests */
};

typedef struct _request_object {
    request_t request;
    uint32_t UID;
    uint32_t sourcePID;                /** Process ID of the process originally created the request */
    char* objectPath;
    char* pattern;
    uint32_t attributes;
    uint32_t depth;
    uint32_t instanceIndex;
    char* instanceKey;
    char* functionName;
    llist_t parameters;
    datamodel_t* datamodel;
    char* namespace;
} request_object_t;

typedef struct _request_close {
    request_t request;
    uint32_t id;
} request_close_t;

typedef struct _request_open_session {
    request_t request;
    char* username;
} request_open_session_t;

#ifdef __cplusplus
}
#endif

#endif


