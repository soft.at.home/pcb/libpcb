/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(PCB_MAIN_PRIV_H)
#define PCB_MAIN_PRIV_H

#ifdef __cplusplus
extern "C"
{
#endif

/**
   @file
   @brief
 */

#include <pcb/core/pcb_main.h>

#include "request_priv.h"
#include "datamodel_priv.h"

typedef struct _pcb_client_data {
    uint32_t attributes;
    uint32_t error;
    uint32_t timeout;
    uint32_t uid;
    llist_t errors;
} pcb_client_data_t;

/**
   @brief

   @details
 */
struct _pcb {
    connection_info_t connection;
    datamodel_t datamodel;                   /**< The connection's datamodel */
    datamodel_t cache;                       /**< Remote object cache */
    request_handlers_t* reqhandlers;         /**< The connection's request handlers, set of functions which are called when there is an incomming request */
    serialize_handlers_t* defaultSerializer; /**< The default serializer */

    /* arguments passed with pcb_create or pcb_initialize */
    int argc;
    char** argv;
    void* userdata;
    app_config_t* config;
    pcb_client_data_t client_data;
    uint32_t request_id;                     /**< When set only replies for requests with this id are handled */

    llist_t notifyHandlers;
    pcb_cleanup_handler_t cleanup;

    bool upcEnabled;                        /**< Is UPC update tracking enabled */
    bool upcOverwrite;                      /**< Is UPC overwrite enabled */
    upc_event_cb_t upcEventCB;              /**< UPC Event Callback */
    void* upcEventUserdata;                 /**< UPC Event Userdata */
};

typedef struct _pcb_peer_data {
    serialize_handlers_t* serializers;      /**< Serialization functions */
    request_t zero_request;                 /**< All notifications send with request id = 0, will be captured here */
    uint32_t last_used_req_id;              /**< Last used request id */
    uint32_t last_used_nreq_id;             /**< Last used notify request id */
    llist_t pendingRequests;                /**< List of sended pending requests */
    llist_t receivedRequests;               /**< List of received requests */
    llist_t notifyRequests;                 /**< List of persistent requests aka notify requests */
    llist_t forwardedRequests;              /**< List of forwarded requests */
    llist_t replies;                        /**< List of received replies */
    void* deserializeInfo;                  /**< Information stucture for deserializer */
    void* serializeInfo;                    /**< Information stucture for serializer */
    peer_info_t* peer;                      /**< Peer where this data is attached to */
    bool trusted;                           /**< Set to true if the peer is a trusted connection */
    bool broadcast;                         /**< Set to true if the peer wants broadcast notification */
    uint32_t UID;                           /**< User ID */
} pcb_peer_data_t;

#if !defined(NO_DOXYGEN)
bool __attribute__ ((visibility("hidden"))) pcb_sendNotificationInternal(pcb_t* pcb, peer_info_t* to, peer_info_t* from, notification_t* notification);
bool __attribute__ ((visibility("hidden"))) pcb_handleRequest(pcb_t* pcb, peer_info_t* peer, request_t* req);
#else
bool pcb_sendNotificationInternal(pcb_t* pcb, peer_info_t* to, peer_info_t* from, notification_t* notification);
bool pcb_handleRequest(pcb_t* pcb, peer_info_t* peer, request_t* req);
#endif

#ifdef __cplusplus
}
#endif

#endif
