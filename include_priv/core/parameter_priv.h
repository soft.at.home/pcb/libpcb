/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(PCB_PARAMETER_PRIV_H)
#define PCB_PARAMETER_PRIV_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <pcb/core/parameter.h>
#include <pcb/utils/linked_list.h>
#include <pcb/utils/string.h>
#include <pcb/utils/variant.h>
#include <pcb/core/mapping.h>

/**
   @file
   @brief
   Header file with private parameter type definitions and private parameter functions
 */

/**
   @ingroup pcb_core
   @defgroup pcb_core_private_parameter Private parameters
   @{

   @brief
   Parameter implementation

   @details

   This file contains the private parameter structures and pricate parameter functions.\n

 */

/**
   @brief
   Parameter handler structure

   @details
   This structure contains the parameter handler functions: destroy, read and write
 */
typedef struct _parameter_handler {
    parameter_destroy_handler_t destroy;           /**< Called when the object is going to be destroyed,this function must succeed, after this handler returns the object pointer is not valid anymore */
    parameter_read_handler_t read;                 /**< Called in the @ref object_update function, if this function is set the object is volatile. If the this handler fails, the update function will fail */
    parameter_write_handler_t write;               /**< Called when the object has been written (after the commit), this function must succeed */
    parameter_translate_value_handler_t translate; /**< Value translation function */
    parameter_prewrite_handler_t prewrite;         /**< Called before comitting a value */
} parameter_handlers_t;

/**
   @brief
   Destination parameter structure
 */
struct _destination_parameter {
    object_destination_t* destination;
    char* parameterName;                         /**< The parameter name as provided by the destination */
    variant_map_t parameters;                    /**< List of parameters for the destination*/
};

typedef struct _parameter_def {
    char* name;                        /**< The name of the parameter */

    parameter_handlers_t* handlers;    /**< User defined handler functions */
    parameter_validator_t* validator;  /**< validator for this parameter */

    uint32_t refcount;

#ifdef PCB_HELP_SUPPORT
    char* description;                 /**< Parameter help text */
#endif

    uint8_t type;                      /**< The parameter type, see @ref parameter_type_t */
} parameter_def_t;

/**
   @brief
   Parameter structure

   @details
   This parameter structure contains all information related to an instance of the parameter\n
 */
struct _parameter {
    llist_iterator_t it;               /**< The place in the owners global parameter list */
    parameter_def_t* definition;       /**< Pointer to parameter definition */
    uint32_t attributes;               /**< The parameter attributes */
    llist_t ACL;                       /**< Access Control List */

    variant_t value[2];                /**< Only one of the values is the actual value, the other one is used while editing */
    destination_parameter_t* map;      /**< Parameter mapping information */

    object_t* countedObject;           /**< If set, the parameter is a multi-instance counter of this object */

    void* userData;                    /**< Pointer to some user data */
    uint8_t state;                     /**< The parameter state, see @ref parameter_state_t */
    uint8_t currentConfig;             /**< 0 or 1, is set to the latest validated and committed value */
};

/**
   @}
 */

bool __attribute__ ((visibility("hidden"))) parameter_destroy(parameter_t* parameter);
variant_type_t __attribute__ ((visibility("hidden"))) parameter_type_to_variant_type(parameter_type_t paramType);
parameter_handlers_t __attribute__ ((visibility("hidden"))) * parameter_getHandlers(parameter_t* parameter);

#ifdef __cplusplus
}
#endif

#endif


