/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(PCB_PATH_H)
#define PCB_PATH_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <pcb/utils/string.h>
#include <pcb/utils/string_list.h>

#include <pcb/core/object.h>
#include <pcb/core/parameter.h>

/**
   @file
   @brief
   Definition of path structures
 */

/**
   @brief
   Exploded path structure.
 */
typedef struct _exploded_path {
    object_t* object;                   /**< The corresponding object, if any */
    uint32_t path_attributes;           /**< The original path attributes */
    string_list_t exploded_path;        /**< The exploded path */
    unsigned int path_size;             /**< The size of the path: the number of path elements */
    parameter_t* parameter;             /**< The corresponding parameter, if any */
    string_t param_name;                /**< The parameter name, if any */
    string_t param_value;               /**< The parameter value, if any. Use 'parameter' instead, if not NULL */
} exploded_path_t;


#if !defined(NO_DOXYGEN)
bool __attribute__ ((visibility("hidden"))) exploded_path_initialize(exploded_path_t* xpath);
void __attribute__ ((visibility("hidden"))) exploded_path_cleanup(exploded_path_t* xpath);
bool __attribute__ ((visibility("hidden"))) exploded_path_fromObject(exploded_path_t* xpath, object_t* object, const uint32_t pathAttributes);
bool __attribute__ ((visibility("hidden"))) exploded_path_fromParameter(exploded_path_t* xpath, parameter_t* parameter, const uint32_t pathAttributes);
bool __attribute__ ((visibility("hidden"))) exploded_path_fromChar(exploded_path_t* xpath, const char* path, const uint32_t pathAttributes);
bool __attribute__ ((visibility("hidden"))) exploded_path_descendObject(exploded_path_t* xpath, object_t* object);
bool __attribute__ ((visibility("hidden"))) exploded_path_descendChar(exploded_path_t* xpath, const char* path);
bool __attribute__ ((visibility("hidden"))) exploded_path_ascend(exploded_path_t* xpath);
bool __attribute__ ((visibility("hidden"))) exploded_path_getPath(exploded_path_t* xpath, string_t* path, const uint32_t pathAttributes);
bool __attribute__ ((visibility("hidden"))) exploded_path_getPartialPath(exploded_path_t* xpath, string_t* path, const uint32_t pathAttributes, const uint32_t start, const uint32_t length);
#else
bool exploded_path_initialize(exploded_path_t* xpath);
void exploded_path_cleanup(exploded_path_t* xpath);
bool exploded_path_fromObject(exploded_path_t* xpath, object_t* object, const uint32_t pathAttributes);
bool exploded_path_fromParameter(exploded_path_t* xpath, parameter_t* parameter, const uint32_t pathAttributes);
bool exploded_path_fromChar(exploded_path_t* xpath, const char* path, const uint32_t pathAttributes);
bool exploded_path_descendObject(exploded_path_t* xpath, object_t* object);
bool exploded_path_descendChar(exploded_path_t* xpath, const char* path);
bool exploded_path_ascend(exploded_path_t* xpath);
bool exploded_path_getPath(exploded_path_t* xpath, string_t* path, const uint32_t pathAttributes);
bool exploded_path_getPartialPath(exploded_path_t* xpath, string_t* path, const uint32_t pathAttributes, const uint32_t start, const uint32_t length);
#endif

#ifdef __cplusplus
}
#endif

#endif

