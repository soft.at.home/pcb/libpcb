/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(PCB_DATAMODEL_PRIV_H)
#define PCB_DATAMODEL_PRIV_H

#ifdef __cplusplus
extern "C"
{
#endif

/**
   @file
   @brief
   Header file with private data model definitions and private data model functions
 */

#include "object_priv.h"
#include <pcb/core/datamodel.h>

/**
   @brief
   Data model attributes. These are for internal use only, and can not be modified using any of the public functions
 */
typedef enum _datamodel_attributes {
    datamodel_attr_default    = 0x00000000,      /**< Default data model (connection less, no caching, no dispatching) */
    datamodel_attr_connected  = 0x00000001,      /**< This datamodel has a connection */
    datamodel_attr_cache      = 0x00000002,      /**< This datamodel is used as cache */
    datamodel_attr_dispatch   = 0x00000004,      /**< This datamodel can only dispatch requests, does not contain any real objects */
    datamodel_attr_no_resolve = 0x00000008,      /**< Do not do automatic function resolving */
} datamodel_attribute_t;

typedef struct _notify_handlers {
    llist_iterator_t it;
    datamodel_notify_handler_t datamodel_changed;
} datamodel_notify_handlers_t;

/**
   @brief
   The data model structure definition
 */
struct _datamodel {
    object_t root;                          /**< The root object of the datamodel*/
    datamodel_attribute_t attributes;       /**< The datamodel attributes, these are not accessible, for internal use only */
    llist_t notify;                         /**< List of callback functions, they are called when something has changed to the datamodel */
    void* userData;                         /**< User data */
    char* mibdir;                           /**< Directory used to search mibs in */
    object_t mibRoot;                       /**< Definition of mibs are added under this object */
};


#if !defined(NO_DOXYGEN)
bool __attribute__ ((visibility("hidden"))) datamodel_initialize(datamodel_t* dm);
void __attribute__ ((visibility("hidden"))) datamodel_cleanup(datamodel_t* dm);
#else
bool datamodel_initialize(datamodel_t* dm);
void datamodel_cleanup(datamodel_t* dm);
#endif

#ifdef __cplusplus
}
#endif

#endif

