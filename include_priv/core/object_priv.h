/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(PCB_OBJECT_PRIV_H)
#define PCB_OBJECT_PRIV_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <regex.h>

#include <pcb/utils/tree.h>
#include <pcb/utils/peer.h>

#include <pcb/core/mapping.h>

#include "local_object.h"
#include "remote_object.h"
#include "path.h"

/**
   @file
   @brief
   Definition of private object structures and private functions

   @todo
 */

/**
   @brief
   Template information structure
 */
typedef struct _template_info {
    uint32_t max_instances;                      /**< Maximum number of instances, only applicable for template objects, see @ref object_attr_template */
    uint32_t instanceCount;                      /**< Current number of instances */
    uint32_t last_used_index;                    /**< Last used index, only applicable for template objects, see @ref object_attr_template */
    parameter_t* instanceCounter;                /**< The parameter used as counter for this multi-instance object, see @ref object_setInstanceCounter */
    llist_t instances;                           /**< The list of instances of this object, only applicable when @ref object_attr_template is set */
    string_list_t deleted[deleted_instance_max]; /**< The deleted instances */
} template_info_t;

/**
   @brief
   Instance information structure
 */
typedef struct _instance_info {
    object_t* instance_of;        /**< The object of witch this is an instance */
    string_t key;                 /**< The key for this instance, if no key parameters are provided, this is used as the key */
} instance_info_t;

/**
   @brief
   Object handlers structure.
 */
typedef struct _object_handler {
    object_delete_handler_t del;                /**< Called when the object is going to be deleted, if this function fails, the object will not be deleted */
    object_destroy_handler_t destroy;           /**< Called when the object is going to be destroyed,this function must succeed, after this handler returns the object pointer is not valid anymore */
    object_read_handler_t read;                 /**< Called in the @ref object_update function, if this function is set the object is volatile. If the this handler fails, the update function will fail */
    object_write_handler_t write;               /**< Called when the object has been written (after the commit), this function must succeed */
    object_instance_add_handler_t instance_add; /**< Called when an instance is created and initialized (only for template objects) */
    object_instance_del_handler_t instance_del; /**< Called when an instance is going to be deleted,  if this function fails, the object will not be deleted */
    object_mib_add_handler_t mib_added;         /**< Called when a mib is added to the object */
    object_mib_del_handler_t mib_deleted;       /**< Called when a mib is deleted from the object */
} object_handlers_t;

typedef enum _modifier {
    modifier_translate_name = 1,
    modifier_translate_value,
    modifier_translate_type,
} modifier_t;

typedef struct _rule_modifier {
    modifier_t type;
    union {
        parameter_translate_name_handler_t rename;
        parameter_translate_value_handler_t translate;
        parameter_translate_type_handler_t cast;
    } data;
    llist_iterator_t it;
} rule_modifier_t;

struct _mapping_rule {
    tree_item_t rules;
    rule_type_t type;
    rule_action_t action;
    regex_t regexp;
    llist_t modifiers;
};

/**
   @brief
   Object destination structure
 */
struct _object_destination {
    llist_iterator_t it;            /** Used to put a destiniation in a list */
    uri_t* uri;                     /** The destiniation uri */
    peer_info_t* peer;              /** Destination peer */
    tree_item_t rules;              /** Mapping rules */
};

/**
   @brief
   Common object structure.
 */
struct _object {
    tree_item_t tree_item;               /**< Tree item reference see @ref pcb_utils_tree */
    uint16_t state;                      /**< The state this object is in see @ref object_state_t */
    uint32_t attributes;                 /**< The object attributes, see @ref object_attribute_t*/
    char* oname;

    llist_t parameters;                  /**< List of all parameters of this object */
    llist_t functions;                   /**< List of all functions of this object */
    llist_t notifyRequests;              /**< List of notification requests */
    llist_t mapRequests;                 /**< List of forwarded requests used in mappings */

    llist_t ACL;                         /**< Access Control List */
    llist_t destinations;                /**< List off destinations */

    llist_iterator_t it;                 /**< Linked list iterator used to put the object in a list, an object can only be in one list at a time */
    llist_iterator_t instance_it;        /**< Linked list iterator used to put the object in the list of instances */
    union {
        template_info_t* template_info;  /**< Template info, only for template objects */
        instance_info_t instance_info;   /**< Instance info, only for instance objects */
    } object_info;

    object_handlers_t* handlers;         /**< User defined handler functions */
    struct _object_validator* validator; /**< Object validator */

#ifdef PCB_HELP_SUPPORT
    char* description;                   /**< Object help text */
#endif

    string_list_t mibs;                  /**< List of mibs used to extend this object */
    void* userData;                      /**< Pointer to some user data */
};

typedef struct _notify_list {
    request_t* req;
    uint32_t notifyDepth;
    struct _notify_list* next;
} notify_list_t;

#if !defined(NO_DOXYGEN)
object_t __attribute__ ((visibility("hidden"))) * object_allocate(const char* name, const uint32_t attributes);
void __attribute__ ((visibility("hidden"))) object_destroy(object_t* object);
void __attribute__ ((visibility("hidden"))) object_destroy_recursive(object_t* object);
bool __attribute__ ((visibility("hidden"))) object_template_initialize(template_info_t* template_info);
bool __attribute__ ((visibility("hidden"))) object_initialize(object_t* object);
void __attribute__ ((visibility("hidden"))) object_cleanup(object_t* object);
bool __attribute__ ((visibility("hidden"))) object_addParameter(object_t* object, parameter_t* parameter);
bool __attribute__ ((visibility("hidden"))) object_addFunction(object_t* object, function_t* function);
void __attribute__ ((visibility("hidden"))) object_setParentTreeModified(object_t* object);
bool __attribute__ ((visibility("hidden"))) object_setParent(object_t* object, object_t* parent);
bool __attribute__ ((visibility("hidden"))) object_matches(exploded_path_t* opath, exploded_path_t* ppath, const uint32_t searchAttr, const uint32_t checked, const uint32_t to_check);
object_handlers_t __attribute__ ((visibility("hidden"))) * object_getHandlers(object_t* object);
void __attribute__ ((visibility("hidden"))) object_attachNotifyRequests(object_t* object);
void __attribute__ ((visibility("hidden"))) object_acl(const object_t* object, llist_t* ACL);
void __attribute__ ((visibility("hidden"))) object_buildNotifyList(object_t* object, notify_list_t** notifyRequests, uint32_t depth);
#else
object_t* object_allocate(const char* name, const uint32_t attributes);
void object_destroy(object_t* object);
void object_destroy_recursive(object_t* object);
bool object_template_initialize(template_info_t* template_info);
bool object_initialize(object_t* object);
void object_cleanup(object_t* object);
bool object_addParameter(object_t* object, parameter_t* parameter);
bool object_addFunction(object_t* object, function_t* function);
void object_setParentTreeModified(object_t* object);
bool object_setParent(object_t* object, object_t* parent);
bool object_matches(exploded_path_t* opath, exploded_path_t* ppath, const uint32_t searchAttr, const uint32_t checked, const uint32_t to_check);
object_handlers_t* object_getHandlers(object_t* object);
void object_attachNotifyRequests(object_t* object);
void object_acl(object_t* object, llist_t* ACL);
void object_buildNotifyList(object_t* object, notify_list_t** notifyRequests, uint32_t depth);
#endif

#ifdef __cplusplus
}
#endif

#endif

