/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(PCB_CORE_PRIV_H)
#define PCB_CORE_PRIV_H

#ifdef __cplusplus
extern "C"
{
#endif

/**
   @file
   @brief
   Header file with private common definitions and private common functions of the core module
 */

#include <pcb/utils/string.h>
#include <pcb/utils/linked_list.h>
#include <pcb/core/acl.h>

/**
   Access control information structure
 */
struct _ACL_info {
    uint16_t acl_flags;           /** read, write, execute, user/group */
    uint32_t id;                  /** the id (user or group) */
    llist_iterator_t it;          /** iterator used to store in the ACL list */
};

#if !defined(NO_DOXYGEN)
bool __attribute__ ((visibility("hidden"))) isValidName(string_t* name);
bool __attribute__ ((visibility("hidden"))) isValidNameChar(const char* name);
ACL_t __attribute__ ((visibility("hidden"))) * aclFind(llist_t* ACL, uint32_t id, uint16_t flags);
bool __attribute__ ((visibility("hidden"))) aclSet(llist_t* ACL, uint32_t id, uint16_t flags);
bool __attribute__ ((visibility("hidden"))) aclCheck(llist_t* ACL, uint32_t uid, uint16_t flags);
void __attribute__ ((visibility("hidden"))) aclDestroy(llist_t* ACL);
#else
bool isValidName(string_t* name);
bool isValidNameChar(string_t* name);
ACL_t* aclFind(llist_t* ACL, uint32_t id, uint16_t flags);
bool aclSet(llist_t* ACL, uint32_t id, uint16_t flags);
bool aclCheck(llist_t* ACL, uint32_t uid, uint32_t gid, uint16_t flags);
void aclDestroy(llist_t* ACL);
#endif

#ifdef __cplusplus
}
#endif

#endif
