/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(PCB_REMOTE_OBJECT_H)
#define PCB_REMOTE_OBJECT_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <pcb/utils/string.h>
#include <pcb/core/serialize.h>

typedef struct _remote_object_info {
    string_t parentPathKey;
    string_t parentPathIndex;
    string_t key;
    uint32_t parameterCount;
    uint32_t childCount;
    uint32_t instanceCount;
    uint32_t functionCount;
    void* destroy_data;
    serialization_object_destroy_handler_t destroy;
    int fd;
    void* userdata;
} remote_object_info_t;

/* cleanup functions */
bool __attribute__ ((visibility("hidden"))) remote_object_delete(object_t* object);

/* tree functions */
object_t __attribute__ ((visibility("hidden"))) * remote_object_root(object_t* object);
object_t __attribute__ ((visibility("hidden"))) * remote_object_getObject(object_t* object, const char* path, const uint32_t pathAttr, bool* ExactMatch, remote_object_info_t* ri);
bool __attribute__ ((visibility("hidden"))) remote_object_hasChildren(const object_t* object);
uint32_t __attribute__ ((visibility("hidden"))) remote_object_childCount(const object_t* object);
void __attribute__ ((visibility("hidden"))) remote_object_path(const object_t* object, string_t* path, const uint32_t pathAttributes);

/* instance functions */
object_t __attribute__ ((visibility("hidden"))) * remote_object_createInstance(object_t* object, uint32_t index, const char* key);
bool __attribute__ ((visibility("hidden"))) remote_object_hasInstances(const object_t* object);
uint32_t __attribute__ ((visibility("hidden"))) remote_object_instanceCount(const object_t* object);

/* parameters */
parameter_t __attribute__ ((visibility("hidden"))) * remote_object_getParameter(const object_t* object, const char* parameterName);
bool __attribute__ ((visibility("hidden"))) remote_object_hasParameters(const object_t* object);
uint32_t __attribute__ ((visibility("hidden"))) remote_object_parameterCount(const object_t* object);

/* functions */
function_t __attribute__ ((visibility("hidden"))) * remote_object_getFunction(object_t* object, const char* functionName);
bool __attribute__ ((visibility("hidden"))) remote_object_hasFunctions(const object_t* object);
uint32_t __attribute__ ((visibility("hidden"))) remote_object_functionCount(const object_t* object);

object_t __attribute__ ((visibility("hidden"))) * object_searchCache(object_t* object, string_t* fullpath, uint32_t pathAttr, bool* exactMatch);

#ifdef __cplusplus
}
#endif

#endif
