/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(PCB_ERROR_H)
#define PCB_ERROR_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdint.h>

/**
   @ingroup pcb_common Common
   @file
   @brief
   Header file with error type definitions and public error functions
 */

/**
   @ingroup pcb_common
   @defgroup pcb_common_error Error
   @{

   @brief
   Error code definitions and functions

   @details
   Common error codes definition and a function to translate the error codes into human readable strings.

   @section pcb_common_error Errors
   A global variable @ref pcb_error exists and contains the last occured error.
   All functions in the pcb library will set this variable when an error occurs.

   @section pcb_common_error_groups Error Groups
   All errors are group in logical groups. An error code is four bytes long where the two most significant
   bytes are the error group and the two least significant bytes the error code .
 */

#define PCB_SYSTEM_BASE         0x00000000   /**< Base for system errors */
#define PCB_COMMON_BASE         0x00010000   /**< Base for common errors */
#define PCB_UTILS_BASE          0x00020000   /**< Base for utils errors */
#define PCB_CORE_BASE           0X00030000   /**< Base for core (data model) errors */
#define PCB_COMMUNICATION_BASE  0x00040000   /**< Base for communication errors */
#define PCB_GETADDRINFO_BASE    0x00050000   /**< Base for communication errors */

#define PCB_ERROR_GROUP(error) (error >> 16) /**< Macro for getting the error group */

/**
   @brief
   Definition of a error groups

   @details
   Enumeration of all error groups
   \n
 */
typedef enum _pcb_error_group {
    pcb_error_group_system = 0,    /**< system based errors */
    pcb_error_group_common,        /**< common errors */
    pcb_error_group_utils,         /**< utility errors */
    pcb_error_group_core,          /**< core (data model) errors */
    pcb_error_group_communication, /**< communication errors */
    pcb_error_group_getaddrinfo,   /**< getaddrinfo errors */
} pcb_error_group_t;

/**
   @brief
   Definition of a errors

   @details
   Enumeration of all errors
   \n
 */
typedef enum _pcb_error {
    pcb_ok = 0,                                 /**< No error */
    // system errors
    //////////////////////////////////////////////////
    pcb_error_system_error = PCB_SYSTEM_BASE,  /**< System error base */
    // all erno errors will fall in this category

    // common errors
    //////////////////////////////////////////////////
    pcb_error_common = PCB_COMMON_BASE,       /**< Common error base */
    pcb_error_invalid_parameter,              /**< The functions has been called with an invalid argument */
    pcb_error_no_memory,                      /**< Out of memory */
    pcb_error_unknown_system_error,           /**< Unknown system error */
    pcb_error_common_max,                     /**< This must be the last in the list of common errors */

    // utility errors
    //////////////////////////////////////////////////
    pcb_error_utils = PCB_UTILS_BASE,         /**< Utils error base */
    pcb_error_insert_before_begin,            /**< Insert an element before begin */
    pcb_error_insert_after_end,               /**< Insert an element after end */
    pcb_error_can_not_remove_centinels,       /**< Centinels can not be removed */
    pcb_error_out_of_boundaries,              /**< Passed the bounderies of a list or tree */
    pcb_error_substring_not_found,            /**< Substring not found in a string */
    pcb_error_list_empty,                     /**< The given list is empty */
    pcb_error_invalid_variant_type,           /**< Invalid variant type */
    pcb_error_invalid_variant_conversion,     /**< Invalid variant conversion */
    pcb_error_element_not_found,              /**< Not found */
    pcb_error_time_conversion,                /**< Time conversion error (i.e. attempted conversion of "0001-01-01T00:00:00Z" to a 32-bit time_t) */
    pcb_error_utils_max,                      /**< This must be the last in the list of common errors */

    // core errors
    //////////////////////////////////////////////////
    pcb_error_core = PCB_CORE_BASE,                                 /**< Core error base */
    pcb_error_empty_name,                                           /**< Empty name provided */
    pcb_error_remote_local_mismatch,                                /**< Mismatch between operation and object type */
    pcb_error_wrong_state,                                          /**< Object/Parameter is in the wrong state to perform operation */
    pcb_error_no_parent,                                            /**< Object/Parameter does not have parent */
    pcb_error_not_template,                                         /**< Object is not a template */
    pcb_error_max_instances_reached,                                /**< Maximum number of instances reached */
    pcb_error_not_instance,                                         /**< Object is not an instance */
    pcb_error_last_instance,                                        /**< Last instance reached */
    pcb_error_last_parameter,                                       /**< Last parameter reached */
    pcb_error_not_found,                                            /**< Object/Parameter/Function not found */
    pcb_error_parent_not_committed,                                 /**< Parent object is not committed */
    pcb_error_parent_not_commited = pcb_error_parent_not_committed, /**< @deprecated use @ref pcb_error_parent_not_committed */
    pcb_error_not_unique_name,                                      /**< Object/Parameter/Function does not have a unique name */
    pcb_error_invalid_name,                                         /**< Name contains invalid characters */
    pcb_error_unknown_type,                                         /**< Unknown type provided */
    pcb_error_value_empty,                                          /**< The given value is empty */
    pcb_error_value_enum,                                           /**< The given value is not in the enumeration */
    pcb_error_value_range,                                          /**< The given value is out of the range */
    pcb_error_string_length,                                        /**< The string is too large or too small */
    pcb_error_value_minimum,                                        /**< The given value is below the minimum specified */
    pcb_error_value_maximum,                                        /**< The given value is above the maximum specified */
    pcb_error_validator_mismatch,                                   /**< "Validator type is not matching the validation function"  */
    pcb_error_type_mismatch,                                        /**< The parameter or validator values can not be converted to the correct type */
    pcb_error_read_only,                                            /**< Object or parameter is read-only */
    pcb_error_invalid_notification_type,                            /**< Invalid notification type */
    pcb_error_wrong_object_type,                                    /**< Object is of the wrong kind for this operation */
    pcb_error_last_function,                                        /**< Error last function */
    pcb_error_function_object_mismatch,                             /**< Function can not be executed for the specified object */
    pcb_error_function_not_implemented,                             /**< Function is not implemented */
    pcb_error_canceled,                                             /**< Operation is canceled */
    pcb_error_invalid_value,                                        /**< Invalid parameter value */
    pcb_error_function_exec_failed,                                 /**< Function execution failed */
    pcb_error_function_argument_missing,                            /**< Mandatory argument missing */
    pcb_error_object_ismib,                                         /**< Operation not allowed on a MIB */
    pcb_error_access_denied,                                        /**< Acl check failed, can't access to the requested parameter */
    pcb_error_duplicate,                                            /**< The operation would result in an instance with duplicate keys */
    pcb_error_parameter_not_found,                                  /**< Parameter not found*/
    pcb_error_core_max,                                             /**< This must be the last in the list of core errors */

    // communication errors
    //////////////////////////////////////////////////
    pcb_error_communication = PCB_COMMUNICATION_BASE, /**< Communication error base */
    pcb_error_already_listening,                      /**< Already listening */
    pcb_error_already_connected,                      /**< Already connected */
    pcb_error_not_connected,                          /**< Not connected */
    pcb_error_connection_shutdown,                    /**< Connection is closed by peer */
    pcb_error_datamodel_not_connected,                /**< The datamodel does not belong to a connection */
    pcb_error_invalid_header,                         /**< Invalid message header received */
    pcb_error_invalid_data_format,                    /**< Invalid or unknown data format */
    pcb_error_invalid_request,                        /**< Invalid or unknown request */
    pcb_error_not_supported_request,                  /**< Request is not supported */
    pcb_error_invalid_reply,                          /**< Invalid reply */
    pcb_error_no_serializer,                          /**< No (de)serializer available */
    pcb_error_time_out,                               /**< Time-out */
    pcb_error_communication_max,                      /**< This must be the last in the list of core errors */

    //////////////////////////////////////////////////
    pcb_error_getaddrinfo_error = PCB_GETADDRINFO_BASE,  /**< getaddrinfo error base */
    pcb_error_gai_badflags,
    pcb_error_gai_noname,
    pcb_error_gai_again,
    pcb_error_gai_fail,
    pcb_error_gai_family,
    pcb_error_gai_socktype,
    pcb_error_gai_service,
    pcb_error_gai_memory,
    pcb_error_gai_system,
    pcb_error_gai_overflow,
    pcb_error_gai_nodata,
    pcb_error_gai_addrfamily,
    pcb_error_gai_inprogress,
    pcb_error_gai_canceled,
    pcb_error_gai_notcanceled,
    pcb_error_gai_alldone,
    pcb_error_gai_intr,
    pcb_error_gai_idnencode,

    // all erno errors will fall in this category

} pcb_error_t;

#if defined(TLS_AVAILABLE)
extern __thread uint32_t pcb_error;
#else
//---------------------------------------------------------------------------------------------
/**
   @brief
   Global error

   @details
   Whenever an error occurs, this variable will contain the last occured error.
 */
extern uint32_t pcb_error;
#endif

/**
   @}
 */

const char* error_string(uint32_t error);

#ifdef __cplusplus
}
#endif

#endif
