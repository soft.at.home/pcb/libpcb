/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef _PCB_CLIENT_H_
#define _PCB_CLIENT_H_

#include <pcb/common.h>
#include <pcb/utils.h>
#include <pcb/core.h>

#ifdef __cplusplus
extern "C"
{
#endif

typedef bool (* pcb_client_object_callback)(object_t* object, void* userdata);

typedef enum _pcb_client_attribute {
    pcb_client_attr_default         = 0,                               // boolean, reset to defaults if true
    pcb_client_attr_key_notation,                                      // boolean, set key notation on(true)/off(false)
    pcb_client_attr_slash_separator,                                   // boolean, set slash seperator on(true)/off(false)
    pcb_client_attr_slash_seperator = pcb_client_attr_slash_separator, // @deprecated Use pcb_client_attr_slash_separator
    pcb_client_attr_timeout,                                           // integer; set the timeout time in ms, default = 5000
    pcb_client_attr_untrusted,                                         // boolean: All request done using the pcb context will be handled as untrusted
    pcb_client_attr_alias_notation,                                    // boolean, set alias notation on(true)/off(false)
    pcb_client_attr_find_instance_wildcards_only,                      // boolean, only instance wildcards with find
} pcb_client_attribute_t;

typedef struct _pcb_client_error {
    uint32_t error;
    char* info;
    llist_iterator_t it;
} pcb_client_error_t;

bool pcb_client_set_attribute(pcb_t* client, pcb_client_attribute_t attrib, uint32_t value);
bool pcb_client_set_user(pcb_t* client, const char* username);
uint32_t pcb_client_get_path_attributes(pcb_t* client);
uint32_t pcb_client_error(pcb_t* client);
const llist_t* pcb_client_errors(pcb_t* client);
void pcb_client_clear_errors(pcb_t* client);

peer_info_t* pcb_client_connect(pcb_t* client, const char* URI);

bool pcb_client_set(peer_info_t* dest, const char* objectPath, const char* parameter, const variant_t* val);
bool pcb_client_validate(peer_info_t* dest, const char* objectPath, const char* parameter, const variant_t* val);
const variant_t* pcb_client_get(peer_info_t* dest, const char* objectPath, const char* parameter);
parameter_t* pcb_client_get_parameter(peer_info_t* dest, const char* objectPath, const char* parameter);

function_t* pcb_client_get_function(peer_info_t* dest, const char* objectPath, const char* function);

bool pcb_client_wait_for_object(peer_info_t* dest, const char* objectPath);
bool pcb_client_set_object(peer_info_t* dest, const char* objectPath, variant_map_t* parameters);
bool pcb_client_validate_object(peer_info_t* dest, const char* objectPath, variant_map_t* parameters);
object_t* pcb_client_get_object(peer_info_t* dest, const char* objectPath);
bool pcb_client_get_objects(peer_info_t* dest, const char* objectPath, pcb_client_object_callback fn, void* userdata);
bool pcb_client_find_objects(peer_info_t* dest, const char* objectPath, const char* pattern, pcb_client_object_callback fn, void* userdata);

object_t* pcb_client_add_instance(peer_info_t* dest, const char* objectPath, uint32_t index, const char* key, variant_map_t* parameters);
bool pcb_client_delete_instance(peer_info_t* dest, const char* objectPath);

#ifdef __cplusplus
}
#endif
#endif
