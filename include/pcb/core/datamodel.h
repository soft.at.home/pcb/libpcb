/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(PCB_DATAMODEL_H)
#define PCB_DATAMODEL_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <pcb/core/types.h>
#include <pcb/utils/string_list.h>
#include <pcb/utils/variant_map.h>

/**
   @ingroup pcb_core
   @file
   @brief
   Header file with data model type definitions and public data model functions
 */

/**
   @ingroup pcb_core
   @defgroup pcb_core_dm_management Data Model
   @{

   @brief
   A data model is the place where all objects are stored.

   @details
   A data model contains a hierarchical tree of objects. Objects can be added, removed and updated
   at any time.

   @section pcb_core_datamodel_types Data model types
   Four main kinds of data models can be created:
    - Connected or public data model
    - Dispatching data model
    - Cached data model
    - Connectionless or private data model

   <b>Connected (public) data model</b>
   These kind of data models are not created directly but inderectly by creating a pcb context.
   When one of the listen sockets is open or a connection is made with a bus,
   any client can query or modify this data model.

   The owner of the connection object is also the owner of the data model and can restrict access to certain objects.

   Never destoy these kind of data models manually

   <b>Dispatching data model</b>
   A dispatching data model is a special case of a connected data model. The objects in this data model are "place holders"
   for objects in other connected data models (mostly resides in another application).
   The requests received for this data model are not handled by this data model, but "translated" and forwarded to another
   server.

   Never destoy these kind of data models manually

   <b>Cached data model</b>
   The cached data model is also part of a connection, but can not be accessed by other applications. This cache can be used
   to store requested objects. When an application needs an object at a regular basis it is more effecient to keep that object
   in the cache then each time retrieve the object again.

   The cached object can be kept updated at all times by the server, the only thing that has to be done is add the notification attributes
   to the request before sending the request.

   By default all objects received due to a sent request will be stored in the cache. If this behavior is not wanted, add the request_no_object_caching
   attribute to the request attributes.

   Never destoy these kind of data models manually

   <b>Connectionless or private data model</b>
   These are the data models you create by yourself using the @ref datamodel_create function. It is also the responsibility of the creator to
   delete this data model.

   No other applications can access the datamodel (unless you provide your own access mechanism).

   @section pcb_core_datamodel_creation Creating a data model
   As usual creating a data model is easy. You can start from a connection which will already contain a datamodel object,
   or create your own data model object using @ref datamodel_create.

   In both case you have to populate the data model with an object tree. This can be done manualy by using the object
   functions to create objects and manipulate them. For creating objects you always have to specify an parent object.
   Each data model already has a root object, which can be retrieved with @ref datamodel_root.

   You can also use a file descibing your objects. This file is using the "object definition language" to define
   the object tree. Such a file can be loaded into your data model using the function @ref datamodel_loadDefinition.

   @section pcb_core_datamodel_cleanup Cleaning up a data model
   Only manual created data models can be destoyed with @ref datamodel_destroy. Never use this function on
   data models from a connection.
 */

/**
   @brief
   Global notify handler signature definition

   @details
   A global notify handler must have this signature
 */
typedef bool (* datamodel_notify_handler_t) (datamodel_t* dm, object_t* object, parameter_t* parameter, notification_t* notify);

/**
   @}
 */

// initialize
datamodel_t* datamodel_create(void);

// cleanup
void datamodel_clear(datamodel_t* dm);
void datamodel_destroy(datamodel_t* dm);

object_t* datamodel_root(datamodel_t* dm);

// MIB
bool datamodel_setMibDir(datamodel_t* dm, const char* mibDir);
const char* datamodel_getMibDir(datamodel_t* dm);
char* datamodel_mibFile(datamodel_t* dm, const char* mibName);
bool datamodel_mibLoad(datamodel_t* dm, const char* mibName, const char* path);
object_t* datamodel_mibRoot(datamodel_t* dm);
object_t* datamodel_mib(datamodel_t* dm, const char* mibName);

bool datamodel_setNamespace(datamodel_t* dm, const char* ns);
const char* datamodel_namespace(datamodel_t* dm);

// load & save functionality
bool datamodel_load(datamodel_t* dm, const char* file, uint32_t format);
bool datamodel_verify(datamodel_t* dm, const char* file, uint32_t format, variant_map_t* info, bool* hasdefinition);
bool datamodel_save(object_t* object, uint32_t depth, const char* file, uint32_t format);
bool datamodel_loadDefinition(datamodel_t* dm, const char* file);

// datamodel changed notify callback
bool datamodel_addNotifyHandler(datamodel_t* dm, datamodel_notify_handler_t handler);
bool datamodel_removeNotifyHandler(datamodel_t* dm, datamodel_notify_handler_t handler);

// user data
bool datamodel_setUserData(datamodel_t* dm, void* data);
void* datamodel_userData(datamodel_t* dm);

// attributes
bool datamodel_isConnected(datamodel_t* dm);
bool datamodel_isCache(datamodel_t* dm);

bool datamodel_autoResolve(datamodel_t* dm, bool resolve);
bool datamodel_isAutoResolving(datamodel_t* dm);

pcb_t* datamodel_pcb(datamodel_t* dm);


#ifdef __cplusplus
}
#endif


#endif
