/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(PCB_REQUESTHANDLERS_H)
#define PCB_REQUESTHANDLERS_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <pcb/core/types.h>
#include <pcb/utils/connection.h>

bool default_sendErrorList(peer_info_t* peer, request_t* req);

bool default_getObject(peer_info_t* peer, object_t* object, uint32_t depth, uint32_t attributes, request_t* req);
bool default_replyParameters(peer_info_t* peer, object_t* object, request_t* req);
bool default_reply_object(peer_info_t* peer, object_t* object, uint32_t depth, uint32_t attributes, request_t* req, llist_t* parameters);

bool default_getObjectHandler(peer_info_t* peer, datamodel_t* datamodel, request_t* req);
bool default_setObjectHandler(peer_info_t* peer, datamodel_t* datamodel, request_t* req);
bool default_createInstanceHandler(peer_info_t* peer, datamodel_t* datamodel, request_t* req);
bool default_deleteInstanceHandler(peer_info_t* peer, datamodel_t* datamodel, request_t* req);
bool default_executeFunctionHandler(peer_info_t* peer, datamodel_t* datamodel, request_t* req);
bool default_findObjectsHandler(peer_info_t* peer, datamodel_t* datamodel, request_t* req);

bool default_closeRequestHandler(peer_info_t* peer, request_t* req);

bool default_openSessionRequestHandler(peer_info_t* peer, request_t* req);

bool default_requestTranslatePath(request_t* req, uint32_t orig_attribs, uint32_t attributes, object_t* object, object_destination_t* dest);
bool default_objectTranslatePath(object_t* reference, object_t* object, object_destination_t* dest, uint32_t attributes);

#ifdef __cplusplus
}
#endif

#endif
