/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef _PCB_CORE_REQUEST_PARAMETER_H
#define _PCB_CORE_REQUEST_PARAMETER_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <pcb/core/request.h>
#include <pcb/utils/variant.h>

/**
   @brief
   Set a bool request parameter

   @details
   This function is provided for convenience. It is setting a parameter in a request using a bool.

   @param request the request of interest
   @param name the name of the parameter to set
   @param value the value of the parameter to set

   @return
    - The converted value
 */
static inline bool request_addBoolParameter(request_t* req, const char* name, bool value) {
    bool rc;
    variant_t v;
    variant_initialize(&v, variant_type_unknown);
    rc = variant_setBool(&v, value);
    if(rc) {
        rc = request_addParameter(req, name, &v);
    }
    variant_cleanup(&v);
    return rc;
}

/**
   @brief
   Set a int8_t request parameter

   @details
   This function is provided for convenience. It is setting a parameter in a request using a int8_t.

   @param request the request of interest
   @param name the name of the parameter to set
   @param value the value of the parameter to set

   @return
    - The converted value
 */
static inline bool request_addInt8Parameter(request_t* req, const char* name, int8_t value) {
    bool rc;
    variant_t v;
    variant_initialize(&v, variant_type_unknown);
    rc = variant_setInt8(&v, value);
    if(rc) {
        rc = request_addParameter(req, name, &v);
    }
    variant_cleanup(&v);
    return rc;
}

/**
   @brief
   Set a int16_t request parameter

   @details
   This function is provided for convenience. It is setting a parameter in a request using a int16_t.

   @param request the request of interest
   @param name the name of the parameter to set
   @param value the value of the parameter to set

   @return
    - The converted value
 */
static inline bool request_addInt16Parameter(request_t* req, const char* name, int16_t value) {
    bool rc;
    variant_t v;
    variant_initialize(&v, variant_type_unknown);
    rc = variant_setInt16(&v, value);
    if(rc) {
        rc = request_addParameter(req, name, &v);
    }
    variant_cleanup(&v);
    return rc;
}

/**
   @brief
   Set a int32_t request parameter

   @details
   This function is provided for convenience. It is setting a parameter in a request using a int32_t.

   @param request the request of interest
   @param name the name of the parameter to set
   @param value the value of the parameter to set

   @return
    - The converted value
 */
static inline bool request_addInt32Parameter(request_t* req, const char* name, int32_t value) {
    bool rc;
    variant_t v;
    variant_initialize(&v, variant_type_unknown);
    rc = variant_setInt32(&v, value);
    if(rc) {
        rc = request_addParameter(req, name, &v);
    }
    variant_cleanup(&v);
    return rc;
}

/**
   @brief
   Set a int64_t request parameter

   @details
   This function is provided for convenience. It is setting a parameter in a request using a int64_t.

   @param request the request of interest
   @param name the name of the parameter to set
   @param value the value of the parameter to set

   @return
    - The converted value
 */
static inline bool request_addInt64Parameter(request_t* req, const char* name, int64_t value) {
    bool rc;
    variant_t v;
    variant_initialize(&v, variant_type_unknown);
    rc = variant_setInt64(&v, value);
    if(rc) {
        rc = request_addParameter(req, name, &v);
    }
    variant_cleanup(&v);
    return rc;
}

/**
   @brief
   Set a uint8_t request parameter

   @details
   This function is provided for convenience. It is setting a parameter in a request using a uint8_t.

   @param request the request of interest
   @param name the name of the parameter to set
   @param value the value of the parameter to set

   @return
    - The converted value
 */
static inline bool request_addUInt8Parameter(request_t* req, const char* name, uint8_t value) {
    bool rc;
    variant_t v;
    variant_initialize(&v, variant_type_unknown);
    rc = variant_setUInt8(&v, value);
    if(rc) {
        rc = request_addParameter(req, name, &v);
    }
    variant_cleanup(&v);
    return rc;
}

/**
   @brief
   Set a uint16_t request parameter

   @details
   This function is provided for convenience. It is setting a parameter in a request using a uint16_t.

   @param request the request of interest
   @param name the name of the parameter to set
   @param value the value of the parameter to set

   @return
    - The converted value
 */
static inline bool request_addUInt16Parameter(request_t* req, const char* name, uint16_t value) {
    bool rc;
    variant_t v;
    variant_initialize(&v, variant_type_unknown);
    rc = variant_setUInt16(&v, value);
    if(rc) {
        rc = request_addParameter(req, name, &v);
    }
    variant_cleanup(&v);
    return rc;
}

/**
   @brief
   Set a uint32_t request parameter

   @details
   This function is provided for convenience. It is setting a parameter in a request using a uint32_t.

   @param request the request of interest
   @param name the name of the parameter to set
   @param value the value of the parameter to set

   @return
    - The converted value
 */
static inline bool request_addUInt32Parameter(request_t* req, const char* name, uint32_t value) {
    bool rc;
    variant_t v;
    variant_initialize(&v, variant_type_unknown);
    rc = variant_setUInt32(&v, value);
    if(rc) {
        rc = request_addParameter(req, name, &v);
    }
    variant_cleanup(&v);
    return rc;
}

/**
   @brief
   Set a uint64_t request parameter

   @details
   This function is provided for convenience. It is setting a parameter in a request using a uint64_t.

   @param request the request of interest
   @param name the name of the parameter to set
   @param value the value of the parameter to set

   @return
    - The converted value
 */
static inline bool request_addUInt64Parameter(request_t* req, const char* name, uint64_t value) {
    bool rc;
    variant_t v;
    variant_initialize(&v, variant_type_unknown);
    rc = variant_setUInt64(&v, value);
    if(rc) {
        rc = request_addParameter(req, name, &v);
    }
    variant_cleanup(&v);
    return rc;
}

/**
   @brief
   Set a double request parameter

   @details
   This function is provided for convenience. It is setting a parameter in a request using a double.

   @param request the request of interest
   @param name the name of the parameter to set
   @param value the value of the parameter to set

   @return
    - The converted value
 */
static inline bool request_addDoubleParameter(request_t* req, const char* name, double value) {
    bool rc;
    variant_t v;
    variant_initialize(&v, variant_type_unknown);
    rc = variant_setDouble(&v, value);
    if(rc) {
        rc = request_addParameter(req, name, &v);
    }
    variant_cleanup(&v);
    return rc;
}

/**
   @brief
   Set a char * request parameter

   @details
   This function is provided for convenience. It is setting a parameter in a request using a char *.

   @param request the request of interest
   @param name the name of the parameter to set
   @param value the value of the parameter to set

   @return
    - The converted value
 */
static inline bool request_addCharParameter(request_t* req, const char* name, const char* value) {
    bool rc;
    variant_t v;
    variant_initialize(&v, variant_type_unknown);
    rc = variant_setChar(&v, value);
    if(rc) {
        rc = request_addParameter(req, name, &v);
    }
    variant_cleanup(&v);
    return rc;
}

/**
   @brief
   Set a struct tm * request parameter

   @details
   This function is provided for convenience. It is setting a parameter in a request using a struct tm *.

   @param request the request of interest
   @param name the name of the parameter to set
   @param value the value of the parameter to set

   @return
    - The converted value
 */
static inline bool request_addDateTimeParameter(request_t* req, const char* name, const struct tm* value) {
    bool rc;
    variant_t v;
    variant_initialize(&v, variant_type_unknown);
    rc = variant_setDateTime(&v, value);
    if(rc) {
        rc = request_addParameter(req, name, &v);
    }
    variant_cleanup(&v);
    return rc;
}

/**
   @brief
   Set a string_t * request parameter

   @details
   This function is provided for convenience. It is setting a parameter in a request using a string_t *.

   @param request the request of interest
   @param name the name of the parameter to set
   @param value the value of the parameter to set

   @return
    - The converted value
 */
static inline bool request_addStringParameter(request_t* req, const char* name, const string_t* value) {
    bool rc;
    variant_t v;
    variant_initialize(&v, variant_type_unknown);
    rc = variant_setString(&v, value);
    if(rc) {
        rc = request_addParameter(req, name, &v);
    }
    variant_cleanup(&v);
    return rc;
}

/**
   @brief
   Set a variant_list_t * request parameter

   @details
   This function is provided for convenience. It is setting a parameter in a request using a variant_list_t *.

   @param request the request of interest
   @param name the name of the parameter to set
   @param value the value of the parameter to set

   @return
    - The converted value
 */
static inline bool request_addListParameter(request_t* req, const char* name, variant_list_t* value) {
    bool rc;
    variant_t v;
    variant_initialize(&v, variant_type_unknown);
    rc = variant_setListMove(&v, value);
    if(rc) {
        rc = request_addParameter(req, name, &v);
    }
    variant_cleanup(&v);
    return rc;
}

/**
   @brief
   Set a variant_map_t * request parameter

   @details
   This function is provided for convenience. It is setting a parameter in a request using a variant_map_t *.

   @param request the request of interest
   @param name the name of the parameter to set
   @param value the value of the parameter to set

   @return
    - The converted value
 */
static inline bool request_addMapParameter(request_t* req, const char* name, variant_map_t* value) {
    bool rc;
    variant_t v;
    variant_initialize(&v, variant_type_unknown);
    rc = variant_setMapMove(&v, value);
    if(rc) {
        rc = request_addParameter(req, name, &v);
    }
    variant_cleanup(&v);
    return rc;
}

/**
   @brief
   Set a int request parameter

   @details
   This function is provided for convenience. It is setting a parameter in a request using a int.

   @param request the request of interest
   @param name the name of the parameter to set
   @param value the value of the parameter to set

   @return
    - The converted value
 */
static inline bool request_addFdParameter(request_t* req, const char* name, int value) {
    bool rc;
    variant_t v;
    variant_initialize(&v, variant_type_unknown);
    rc = variant_setFd(&v, value);
    if(rc) {
        rc = request_addParameter(req, name, &v);
    }
    variant_cleanup(&v);
    return rc;
}

/**
   @brief
   Set a void * request parameter

   @details
   This function is provided for convenience. It is setting a parameter in a request using a void *.

   @param request the request of interest
   @param name the name of the parameter to set
   @param value the value of the parameter to set

   @return
    - The converted value
 */
static inline bool request_addByteArrayParameter(request_t* req, const char* name, const void* value, uint32_t size) {
    bool rc;
    variant_t v;
    variant_initialize(&v, variant_type_unknown);
    rc = variant_setByteArray(&v, value, size);
    if(rc) {
        rc = request_addParameter(req, name, &v);
    }
    variant_cleanup(&v);
    return rc;
}

/**
   @brief
   Set a string_list_t * request parameter

   @details
   This function is provided for convenience. It is setting a parameter in a request using a string_list_t *.

   @param request the request of interest
   @param name the name of the parameter to set
   @param value the value of the parameter to set

   @return
    - The converted value
 */
static inline bool request_addStringListParameter(request_t* req, const char* name, string_list_t* value, const char* separator) {
    bool rc;
    variant_t v;
    variant_initialize(&v, variant_type_unknown);
    rc = variant_setStringList(&v, value, separator);
    if(rc) {
        rc = request_addParameter(req, name, &v);
    }
    variant_cleanup(&v);
    return rc;
}

#ifdef __cplusplus
}
#endif
#endif
