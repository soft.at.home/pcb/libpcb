/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(PCB_OBJECT_H)
#define PCB_OBJECT_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <pcb/utils/linked_list.h>
#include <pcb/utils/uri.h>
#include <pcb/utils/variant.h>
#include <pcb/utils/connection.h>

#include <pcb/core/parameter.h>

/**
   @ingroup pcb_core
   @file
   @brief
   Header file with object type definitions and public object functions
 */

/**
   @ingroup pcb_core
   @defgroup pcb_core_object Objects
   @{

   @brief
   Object implementation

   @details

   @section pcb_core_object_life Object Life Cycle
   Each object has its own life cycle. An object comes to "life" whenever it is created and "dies" whenever it is deleted.\n
   An object can only be modified when it is in the created or ready state. An object that is modified while it is in the created state,
   will stay in this stated. The only other state that allows modifications to an object is the ready state.\n
   \n
   An object can only be deleted when it is in the ready state.\n
   \n
   An object can be reverted at all times by using the @ref object_rollback function.\n
   To go from one state to another it has to be either comitted or rollbacked.\n
   \n
   @image html object_life_cycle.jpg "Object Life Cycle"

   @section pcb_core_object_types Object Types
   While the behavior and look of all object is almost the same, we can distinguish 3 different kind of objects:
    - single instance objects
    - template objects
    - instance objects
   \n

   Signle instance objects can be defined/created once and used afterwards. These kind of objects can not have mandatory parameters or key parameters.\n
   Template objects are almost simular. Thes kind of objects can be defined/crea7ted and used afterwards, but also copies (instance) can be cerated at all times.\n
   Template objects can have mandatory parameters and/or key parameters. An instance can only be comitted when all madatory parameters are filled in and the key
   parameters are not empty\n.
   Instance objects are an instance of a template object, instance objects inhert from a template the following attributes:
    - attribute flags
    - children (but no instances)
    - parameters
    - functions
    - handler functions (validate and read)
    - signals (and the connected slots)
 */


/**
   @brief
   This definition can be used in the @ref object_createInstance to specify the default index
 */
#define DEFAULT_INDEX 0xFFFFFFFF

/**
   @brief
   Definition of a helper macro for iteration through the children of an object

   @details
   This <b>helper macro</b> iterates forwards through the children of an object\n

   @param child a pointer to an object that will contain the current child
   @param root_item the parent object
 */
#define object_for_each_child(child, root_item) \
    for(child = object_firstChild(root_item); child; child = object_nextSibling(child))

#if __STDC_VERSION__ >= 199901L
/**
   @brief
   Definition of a helper macro for iteration through the children of an object

   @details
   This <b>helper macro</b> iterates forwards through the children of an object\n

   @param child the name of a pointer to an object to be declared that will contain the current child
   @param root_item the parent object
 */
#define object_for_each_declare_child(child, root_item) \
    for(object_t* child = object_firstChild(root_item); child; child = object_nextSibling(child))
#endif

/**
   @brief
   Definition of a helper macro for iteration through the instances of an object

   @details
   This <b>helper macro</b> iterates forwards through the instances of an object\n

   @param child a pointer to an object that will contain the current child
   @param root_item the parent object
 */
#define object_for_each_instance(child, root_item) \
    for(child = object_firstInstance(root_item); child; child = object_nextInstance(child))

#if __STDC_VERSION__ >= 199901L
/**
   @brief
   Definition of a helper macro for iteration through the instances of an object

   @details
   This <b>helper macro</b> iterates forwards through the instances of an object\n

   @param child the name of a pointer to an object to be declared that will contain the current child
   @param root_item the parent object
 */
#define object_for_each_declare_instance(child, root_item) \
    for(object_t* child = object_firstInstance(root_item); child; child = object_nextInstance(child))
#endif

/**
   @brief
   Definition of a helper macro for iteration through the instances of an object

   @details
   This <b>helper macro</b> iterates forwards through the instances of an object\n

   @param child a pointer to an object that will contain the current child
   @param root_item the parent object
 */
#define object_for_each_instance_reverse(child, root_item) \
    for(child = object_lastInstance(root_item); child; child = object_prevInstance(child))

#if __STDC_VERSION__ >= 199901L
/**
   @brief
   Definition of a helper macro for iteration through the instances of an object

   @details
   This <b>helper macro</b> iterates forwards through the instances of an object\n

   @param child the name a pointer to an object to be declared that will contain the current child
   @param root_item the parent object
 */
#define object_for_each_declare_instance_reverse(child, root_item) \
    for(object_t* child = object_lastInstance(root_item); child; child = object_prevInstance(child))
#endif

/**
   @brief
   Definition of a helper macro for iteration through the parameters of an object

   @details
   This <b>helper macro</b> iterates forwards through the parameters of an object\n

   @param parameter a pointer to a parameter that will contain the current parameter
   @param object the parent object
 */
#define object_for_each_parameter(parameter, object) \
    for(parameter = object_firstParameter(object); parameter; parameter = object_nextParameter(parameter))

#if __STDC_VERSION__ >= 199901L
/**
   @brief
   Definition of a helper macro for iteration through the parameters of an object

   @details
   This <b>helper macro</b> iterates forwards through the parameters of an object\n

   @param parameter the name a pointer to a parameter to be declared that will contain the current parameter
   @param object the parent object
 */
#define object_for_each_declare_parameter(parameter, object) \
    for(parameter_t* parameter = object_firstParameter(object); parameter; parameter = object_nextParameter(parameter))
#endif

/**
   @brief
   Definition of a helper macro for iteration through the functions of an object

   @details
   This <b>helper macro</b> iterates forwards through the functions of an object\n

   @param function a pointer to a function that will contain the current parameter
   @param object the parent object
 */
#define object_for_each_function(function, object) \
    for(function = object_firstFunction(object); function; function = object_nextFunction(function))

#if __STDC_VERSION__ >= 199901L
/**
   @brief
   Definition of a helper macro for iteration through the functions of an object

   @details
   This <b>helper macro</b> iterates forwards through the functions of an object\n

   @param function the name of a pointer to a function to be declared that will contain the current parameter
   @param object the parent object
 */
#define object_for_each_declare_function(function, object) \
    for(function_t* function = object_firstFunction(object); function; function = object_nextFunction(function))
#endif

/**
   @brief
   Enumeration of the parameter states
 */
typedef enum _object_state {
    object_state_invalid = 0,  /**< Object can not have this state */
    object_state_created,      /**< Object is being created */
    object_state_ready,        /**< Object is ready, other operations can start */
    object_state_modified,     /**< Object has been modified */
    object_state_validated,    /**< Object is being validated */
    object_state_deleted,      /**< Object is marked for deletion */
} object_state_t;

#define object_attr_default           0x0000                    /**< Apply default attributes */
#define object_attr_read_only         0x0001                    /**< The object is read-only for external processes, for multi-instance objects this applies that no instances can be created or deleted by a remote process */
#define object_attr_template          0x0002                    /**< This object can have many instances */
#define object_attr_accept_parameters 0x0004                    /**< This object can accept unknown parameters */
#define object_attr_persistent        0x0008                    /**< This object is persistent */
#define object_attr_mib               0x0010                    /**< This object defines a MIB and can be used to extend another object */
// the next attributes are for internal use only
#define object_attr_instance          0x0100                    /**< This object is an instance of a multi-instance object */
#define object_attr_executing_func    0x0200                    /**< This object is currently executing a function */
#define object_attr_subtree_changed   0x0400                    /**< The child tree of this object has changed somewhere */
#define object_attr_remote            0x0800                    /**< This object is a snapshot from a remote object */
#define object_attr_mapped            0x1000                    /**< This object is mapped */
#define object_attr_linked            0x2000                    /**< This object is linked */
#define object_attr_not_cached        0x4000                    /**< This remote object is not in the cache */
#define object_attr_upc               0x8000                    /**< This object is upgrade persistent */
#define object_attr_upc_usersetting   0x10000                   /**< This object is a user setting */
#define object_attr_upc_changed       0x20000                   /**< This object is a changed upc */
#define object_attr_upc_overwrite     0x40000                   /**< This object is marked to overwrite an existing upc value. */
#define object_attr_sysbus_root       0x80000                   /**< This object is marked to be the sysbus root. */

#define path_attr_default             0x00000000                /**< Apply default attributes. Defauls are index path with dot seperation*/
#define path_attr_key_notation        0x00000001                /**< Use key notation paths instead of index notation paths. This flag only applies to mult-instance objects */
#define path_attr_slash_separator     0x00000002                /**< Use "/" as a seperator instead of "." */
#define path_attr_slash_seperator     path_attr_slash_separator /**< @deprecated Use @ref path_attr_slash_separator */
#define path_attr_partial_match       0x00000004                /**< Return objects with a partial match of the path */
#define path_attr_parent              0x00000008                /**< Return the parent path */
#define path_attr_include_namespace   0x00000010                /**< Prepend path with namespace */
#define path_attr_include_parameter   0x00000020                /**< Include parameter */
#define path_attr_include_value       0x00000040                /**< Include value, this also includes the parameter */
#define path_attr_alias               0x04000000                /**< Replace indexes with alias if available */


#define search_attr_default            0x00000000                  /**< Apply default attributes. Defauls are index path with dot seperation, no parameters and no values included*/
#define search_attr_key_notation       0x00000001                  /**< Use key notation paths instead of index notation paths. This flag only applies to multi-instance objects */
#define search_attr_slash_separator    0x00000002                  /**< Use "/" as a seperator instead of "." */
#define search_attr_slash_seperator    search_attr_slash_separator /**< @deprecated Use @ref search_attr_slash_separator */
#define search_attr_include_parameters 0x00000004                  /**< Include parameters in the search */
#define search_attr_include_values     0x00000008                  /**< Include values in the search, this also includes the parameters */
#define search_attr_instance_wildcards_only 0x00000010             /**< The wildcard should only match instance objects */

/**
   @brief
   Deleted instance indices
 */
typedef enum _deleted_instance {
    deleted_instance_persistent = 0,                                        /**< Deleted persistent instance */
    deleted_instance_upc,                                                   /**< Deleted UPC instance */
    deleted_instance_usersetting                                            /**< Deleted UserSetting instance */
} deleted_instance_t;
#define deleted_instance_max            (deleted_instance_usersetting + 1)  /**< Number of indices */

/**
   @brief
   Object delete handler

   @details
   A callback function can be added to the object which is called when the object will be deleted.
 */
typedef bool (* object_delete_handler_t) (object_t* object);

/**
   @brief
   Object destroy handler

   @details
   A callback function can be added to the object which is called when the object will be destroyed.
   If you added some user data to the object, it is a good practive to also add a destroy handler.
 */
typedef void (* object_destroy_handler_t) (object_t* object);

/**
   @brief
   Object read handler

   @details
   A callback function can be added to the object which is called when an @ref object_update is called.
   A read handler is typically used to get the correct value from your "system". Objects with a read handler
   set should define all parameters set in the read handeler volatile.
 */
typedef bool (* object_read_handler_t) (object_t* object);

/**
   @brief
   Object write handler

   @details
   A callback function can be added to the object which is called when the object has been changed.
   The intention of this function is to let you known there are changes in the object.
 */
typedef void (* object_write_handler_t) (object_t* object);

/**
   @brief
   Instace add handler

   @details
   A callback function can be added to a template object which is called when a new instance is being created..
   This function will be called after the new instance is created, but before all parameters are set..
   When the implementation returns false, the instance add will fail.
 */
typedef bool (* object_instance_add_handler_t) (object_t* object, object_t* instance);

/**
   @brief
   Instance delete handler

   @details
   A callback function can be added to a template object which is called when a instance is deleted..
   This function will be called before the instance is deleted.. When the implementation returns false,
   the instance will not be deleted.
 */
typedef bool (* object_instance_del_handler_t) (object_t* object, object_t* instance);

/**
   @brief
   MIB added handler

   @details
   A callback function can be added to an object which is called when a MIB is added.
   This function will be called after the MIB is added.
 */
typedef void (* object_mib_add_handler_t) (object_t* object, object_t* mib);

/**
   @brief
   MIB added handler

   @details
   A callback function can be added to an object which is called when a MIB is deleted.
   This function will be called after the MIB is deleted.
 */
typedef void (* object_mib_del_handler_t) (object_t* object, object_t* mib);


/**
   @}
 */

// initializer functions
object_t* object_create(object_t* parent, const char* name, const uint32_t attributes);

// cleanup functions
bool object_delete(object_t* object);
bool object_deleteByKey(object_t* parent, uint32_t attributes, const char* key); // UPC TODO FIXME keypath???

#ifdef PCB_HELP_SUPPORT
bool object_setDescription(object_t* object, const char* description);
const char* object_getDescription(object_t* object);
#endif

// tree functions
object_t* object_root(const object_t* object);
datamodel_t* object_datamodel(const object_t* object);
object_t* object_parent(const object_t* object);
object_t* object_firstChild(object_t* object);
object_t* object_lastChild(object_t* object);
object_t* object_nextSibling(const object_t* object);
object_t* object_prevSibling(const object_t* object);
object_t* object_getObject(object_t* object, const char* path, const uint32_t pathAttr, bool* exactMatch);

/**
   @ingroup pcb_core_object
   @brief
   Get an object starting from a parent object.

   @details
   This function returns an object starting from a given object.\n
   The path given is relative to the starting object and must be in key path notation.

   @param parent pointer to the object from which the given path starts.
   @param keypath the relative path to the object you want to retrieve in key path notation.

   @return
    - pointer to found object.
    - NULL: no object is matching the criteria
 */
object_t* object_getObjectByKey(object_t* parent, const char* keypath, ...);

object_list_t* object_findObjects(object_t* object, const char* pattern, uint32_t depth, const uint32_t pathAttr);
object_t* object_item(llist_iterator_t* it);
bool object_hasChildren(const object_t* object);
uint32_t object_childCount(object_t* object);
void object_path(const object_t* object, string_t* path, const uint32_t pathAttributes);
char* object_pathChar(const object_t* object, const uint32_t pathAttributes);

// instance functions
object_t* object_createInstance(object_t* object, uint32_t index, const char* key);
object_t* object_instanceOf(const object_t* object);
object_t* object_firstInstance(object_t* object);
object_t* object_lastInstance(object_t* object);
object_t* object_nextInstance(const object_t* object);
object_t* object_prevInstance(const object_t* object);
bool object_hasInstances(const object_t* object);
uint32_t object_instanceCount(object_t* object);
bool object_setMaxInstances(object_t* object, const uint32_t max);
uint32_t object_getMaxInstances(object_t* object);
const string_list_t* object_getDeletedInstances(object_t* object, deleted_instance_t index);

// parameters
parameter_t* object_firstParameter(const object_t* object);
parameter_t* object_lastParameter(object_t* object);
parameter_t* object_nextParameter(const parameter_t* parameter);
parameter_t* object_prevParameter(const parameter_t* parameter);
parameter_t* object_getParameter(const object_t* object, const char* parameterName);
bool object_hasParameter(const object_t* object, const char* parameterName);
bool object_hasParameters(const object_t* object);
uint32_t object_parameterCount(object_t* object);

// functions
function_t* object_firstFunction(object_t* object);
function_t* object_lastFunction(object_t* object);
function_t* object_nextFunction(const function_t* func);
function_t* object_prevFunction(const function_t* func);
function_t* object_getFunction(object_t* object, const char* functionName);
bool object_hasFunctions(const object_t* object);
uint32_t object_functionCount(object_t* object);

// mibs
bool object_addMib(object_t* object, const char* mibName);
bool object_removeMib(object_t* object, const char* mibName);
bool object_hasMib(object_t* object, const char* mibName);
const string_list_t* object_mibs(object_t* object);

// direct parameter accessors
// get
const variant_t* object_parameterValue(const object_t* object, const char* parameterName);

// set
bool object_parameterSetValue(object_t* object, const char* parameterName, const variant_t* value);
bool object_parameterSetInt8Value(object_t* object, const char* parameterName, int8_t value);
bool object_parameterSetInt16Value(object_t* object, const char* parameterName, int16_t value);
bool object_parameterSetInt32Value(object_t* object, const char* parameterName, int32_t value);
bool object_parameterSetInt64Value(object_t* object, const char* parameterName, int64_t value);
bool object_parameterSetUInt8Value(object_t* object, const char* parameterName, uint8_t value);
bool object_parameterSetUInt16Value(object_t* object, const char* parameterName, uint16_t value);
bool object_parameterSetUInt32Value(object_t* object, const char* parameterName, uint32_t value);
bool object_parameterSetUInt64Value(object_t* object, const char* parameterName, uint64_t value);
bool object_parameterSetStringValue(object_t* object, const char* parameterName, string_t* value);
bool object_parameterSetCharValue(object_t* object, const char* parameterName, const char* value);
bool object_parameterSetBoolValue(object_t* object, const char* parameterName, bool value);
bool object_parameterSetDoubleValue(object_t* object, const char* parameterName, double value);
bool object_parameterSetDateTimeValue(object_t* object, const char* parameterName, const struct tm* value);
bool object_parameterSetDateTimeExtendedValue(object_t* object, const char* parameterName, const pcb_datetime_t* value);

bool object_parameterCanRead(object_t* object, const char* parameterName, uint32_t uid);
bool object_parameterCanWrite(object_t* object, const char* parameterName, uint32_t uid);
bool object_parameterCanReadValue(object_t* object, const char* parameterName, uint32_t uid);

bool object_getValues(object_t* object, variant_map_t* values);
bool object_setValues(object_t* object, const variant_map_t* values);
bool object_getLowerCaseValues(object_t* object, variant_map_t* values);
bool object_setLowerCaseValues(object_t* object, const variant_map_t* values);
bool object_getValuesWithACL(object_t* object, variant_map_t* values, uint32_t uid);
bool object_setValuesWithACL(object_t* object, const variant_map_t* values, uint32_t uid);
bool object_getLowerCaseValuesWithACL(object_t* object, variant_map_t* values, uint32_t uid);
bool object_setLowerCaseValuesWithACL(object_t* object, const variant_map_t* values, uint32_t uid);

bool object_getMibValues(object_t* object, const char* mibName, variant_map_t* values);
bool object_getMibValuesWithACL(object_t* object, const char* mibName, variant_map_t* values, uint32_t uid);

// operators
bool object_update(object_t* object);
bool object_commit(object_t* object);
void object_rollback(object_t* object);
bool object_validate(object_t* object);

// acl
bool object_hasAcl(object_t* object);
const llist_t* object_getACL(const object_t* object);
void object_getCalculatedACL(const object_t* object, llist_t* acl);
bool object_setACL(object_t* object, const llist_t* acl);
bool object_aclSet(object_t* object, uint32_t id, uint16_t flags);
bool object_aclAdd(object_t* object, uint32_t id, uint16_t flags);
bool object_aclDel(object_t* object, uint32_t id, uint16_t flags);
bool object_canRead(object_t* object, uint32_t uid);
bool object_canWrite(object_t* object, uint32_t uid);
bool object_canExecute(object_t* object, uint32_t uid);

// upc
bool object_upcSet(object_t* object, uint32_t attributes);
bool object_upcClear(object_t* object, uint32_t attributes);

// attribute functions
uint32_t object_attributes(const object_t* object);

void object_setReadOnly(object_t* object, const bool enable);
void object_setPersistent(object_t* object, const bool enable);
void object_setUPC(object_t* object, const bool enable);
void object_setUserSetting(object_t* object, const bool enable);
void object_setUPCOverwrite(object_t* object, const bool enable);
void object_setSysbusRoot(object_t* object, const bool enable);

bool object_isReadOnly(const object_t* object);
bool object_isTemplate(const object_t* object);
bool object_isInstance(const object_t* object);
bool object_isRemote(const object_t* object);
bool object_canAcceptParameters(const object_t* object);
bool object_isMib(const object_t* object);

// Multi-instance counter
parameter_t* object_setInstanceCounter(object_t* object, const char* parameterName);
parameter_t* object_getInstanceCounter(object_t* object);
bool object_hasInstanceCounter(object_t* object);

// others
bool object_setDeleteHandler(object_t* object, object_delete_handler_t handler);
bool object_setDestroyHandler(object_t* object, object_destroy_handler_t handler);
bool object_setReadHandler(object_t* object, object_read_handler_t handler);
bool object_setWriteHandler(object_t* object, object_write_handler_t handler);
bool object_setInstanceAddHandler(object_t* object, object_instance_add_handler_t handler);
bool object_setInstanceDelHandler(object_t* object, object_instance_del_handler_t handler);
bool object_setMibAddHandler(object_t* object, object_mib_add_handler_t handler);
bool object_setMidDelHandler(object_t* object, object_mib_del_handler_t handler);

object_write_handler_t object_getWriteHandler(object_t* object);

bool object_setValidator(object_t* object, object_validator_t* validator);
const char* object_name(const object_t* object, const uint32_t pathAttributes);

object_state_t object_state(const object_t* object);

void object_setUserData(object_t* object, void* data);
void* object_getUserData(object_t* object);

// object notifications
bool object_addNotifyRequest(object_t* object, request_t* req);
request_t* object_firstNotifyRequest(object_t* object);
request_t* object_nextNotifyRequest(object_t* object, request_t* reference);

#include <pcb/core/object_parameter.h>

#ifdef __cplusplus
}
#endif

#endif
