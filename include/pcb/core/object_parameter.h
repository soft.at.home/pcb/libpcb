/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef _PCB_CORE_OBJECT_PARAMETER_H
#define _PCB_CORE_OBJECT_PARAMETER_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <pcb/core/object.h>
#include <pcb/utils/variant.h>

/**
   @ingroup pcb_core
   @file
   @brief
   Header file with function definitions for easier access to object parameter values
 */

/**
   @ingroup pcb_core_object
   @brief
   Get an object parameter and converts it to a bool

   @details
   This function is provided for convenience. It is fetching a parameter in the object
   and converts it to a bool as @ref variant_toBool.\n
   The return pointer MUST be freed.\n

   @param object the object of interest
   @param name the name of the parameter to fetch

   @return
    - The converted value
 */
static inline bool object_parameterBoolValue(const object_t* object, const char* name) {
    return variant_bool(object_parameterValue(object, name));
}

/**
   @ingroup pcb_core_object
   @brief
   Get an object parameter and converts it to a int8_t

   @details
   This function is provided for convenience. It is fetching a parameter in the object
   and converts it to a int8_t as @ref variant_toInt8.\n
   The return pointer MUST be freed.\n

   @param object the object of interest
   @param name the name of the parameter to fetch

   @return
    - The converted value
 */
static inline int8_t object_parameterInt8Value(const object_t* object, const char* name) {
    return variant_int8(object_parameterValue(object, name));
}

/**
   @ingroup pcb_core_object
   @brief
   Get an object parameter and converts it to a int16_t

   @details
   This function is provided for convenience. It is fetching a parameter in the object
   and converts it to a int16_t as @ref variant_toInt16.\n
   The return pointer MUST be freed.\n

   @param object the object of interest
   @param name the name of the parameter to fetch

   @return
    - The converted value
 */
static inline int16_t object_parameterInt16Value(const object_t* object, const char* name) {
    return variant_int16(object_parameterValue(object, name));
}

/**
   @ingroup pcb_core_object
   @brief
   Get an object parameter and converts it to a int32_t

   @details
   This function is provided for convenience. It is fetching a parameter in the object
   and converts it to a int32_t as @ref variant_toInt32.\n
   The return pointer MUST be freed.\n

   @param object the object of interest
   @param name the name of the parameter to fetch

   @return
    - The converted value
 */
static inline int32_t object_parameterInt32Value(const object_t* object, const char* name) {
    return variant_int32(object_parameterValue(object, name));
}

/**
   @ingroup pcb_core_object
   @brief
   Get an object parameter and converts it to a int64_t

   @details
   This function is provided for convenience. It is fetching a parameter in the object
   and converts it to a int64_t as @ref variant_toInt64.\n
   The return pointer MUST be freed.\n

   @param object the object of interest
   @param name the name of the parameter to fetch

   @return
    - The converted value
 */
static inline int64_t object_parameterInt64Value(const object_t* object, const char* name) {
    return variant_int64(object_parameterValue(object, name));
}

/**
   @ingroup pcb_core_object
   @brief
   Get an object parameter and converts it to a uint8_t

   @details
   This function is provided for convenience. It is fetching a parameter in the object
   and converts it to a uint8_t as @ref variant_toUInt8.\n
   The return pointer MUST be freed.\n

   @param object the object of interest
   @param name the name of the parameter to fetch

   @return
    - The converted value
 */
static inline uint8_t object_parameterUInt8Value(const object_t* object, const char* name) {
    return variant_uint8(object_parameterValue(object, name));
}

/**
   @ingroup pcb_core_object
   @brief
   Get an object parameter and converts it to a uint16_t

   @details
   This function is provided for convenience. It is fetching a parameter in the object
   and converts it to a uint16_t as @ref variant_toUInt16.\n
   The return pointer MUST be freed.\n

   @param object the object of interest
   @param name the name of the parameter to fetch

   @return
    - The converted value
 */
static inline uint16_t object_parameterUInt16Value(const object_t* object, const char* name) {
    return variant_uint16(object_parameterValue(object, name));
}

/**
   @ingroup pcb_core_object
   @brief
   Get an object parameter and converts it to a uint32_t

   @details
   This function is provided for convenience. It is fetching a parameter in the object
   and converts it to a uint32_t as @ref variant_toUInt32.\n
   The return pointer MUST be freed.\n

   @param object the object of interest
   @param name the name of the parameter to fetch

   @return
    - The converted value
 */
static inline uint32_t object_parameterUInt32Value(const object_t* object, const char* name) {
    return variant_uint32(object_parameterValue(object, name));
}

/**
   @ingroup pcb_core_object
   @brief
   Get an object parameter and converts it to a uint64_t

   @details
   This function is provided for convenience. It is fetching a parameter in the object
   and converts it to a uint64_t as @ref variant_toUInt64.\n
   The return pointer MUST be freed.\n

   @param object the object of interest
   @param name the name of the parameter to fetch

   @return
    - The converted value
 */
static inline uint64_t object_parameterUInt64Value(const object_t* object, const char* name) {
    return variant_uint64(object_parameterValue(object, name));
}

/**
   @ingroup pcb_core_object
   @brief
   Get an object parameter and converts it to a double

   @details
   This function is provided for convenience. It is fetching a parameter in the object
   and converts it to a double as @ref variant_toDouble.\n
   The return pointer MUST be freed.\n

   @param object the object of interest
   @param name the name of the parameter to fetch

   @return
    - The converted value
 */
static inline double object_parameterDoubleValue(const object_t* object, const char* name) {
    return variant_double(object_parameterValue(object, name));
}

/**
   @ingroup pcb_core_object
   @brief
   Get an object parameter and converts it to a char *

   @details
   This function is provided for convenience. It is fetching a parameter in the object
   and converts it to a char * as @ref variant_toChar.\n
   The return pointer MUST be freed.\n

   @param object the object of interest
   @param name the name of the parameter to fetch

   @return
    - The converted value
 */
static inline char* object_parameterCharValue(const object_t* object, const char* name) {
    return variant_char(object_parameterValue(object, name));
}

/**
   @ingroup pcb_core_object
   @brief
   Get a direct char * pointer to an object parameter

   @details
   This function is provided for convenience. It is fetching a parameter in the object
   and returns a direct pointer to its char * value as @ref variant_da_char.\n
   The return pointer MUST NOT be freed.\n

   @param object the object of interest
   @param name the name of the parameter to fetch

   @return
    - The resulting char * pointer
    - NULL if the variant is invalid or not of char type
 */
static inline const char* object_da_parameterCharValue(const object_t* object, const char* name) {
    return variant_da_char(object_parameterValue(object, name));
}

/**
   @ingroup pcb_core_object
   @brief
   Get an object parameter and converts it to a struct tm *

   @details
   This function is provided for convenience. It is fetching a parameter in the object
   and converts it to a struct tm * as @ref variant_toDateTime.\n
   The return pointer MUST be freed.\n

   @param object the object of interest
   @param name the name of the parameter to fetch

   @return
    - The converted value
 */
static inline struct tm* object_parameterDateTimeValue(const object_t* object, const char* name) {
    return variant_dateTime(object_parameterValue(object, name));
}

/**
   @ingroup pcb_core_object
   @brief
   Get a direct struct tm * pointer to an object parameter

   @details
   This function is provided for convenience. It is fetching a parameter in the object
   and returns a direct pointer to its struct tm * value as @ref variant_da_dateTime.\n
   The return pointer MUST NOT be freed.\n

   @param object the object of interest
   @param name the name of the parameter to fetch

   @return
    - The resulting struct tm * pointer
    - NULL if the variant is invalid or not of dateTime type
 */
static inline const struct tm* object_da_parameterDateTimeValue(const object_t* object, const char* name) {
    return variant_da_dateTime(object_parameterValue(object, name));
}

/**
   @ingroup pcb_core_object
   @brief
   Get an object parameter and converts it to a pcb_datetime_t*

   @details
   This function is provided for convenience. It is fetching a parameter in the object
   and converts it to a pcb_datetime_t * as @ref variant_toDateTimeExtended.\n
   The return pointer MUST be freed.\n

   @param object the object of interest
   @param name the name of the parameter to fetch

   @return
    - The converted value
 */
static inline pcb_datetime_t* object_parameterDateTimeExtendedValue(const object_t* object, const char* name) {
    return variant_dateTimeExtended(object_parameterValue(object, name));
}

/**
   @ingroup pcb_core_object
   @brief
   Get a direct pcb_datetime_t pointer from an object pointer

   @details
   This function is provided for convenience. It is fetching a parameter in the object
   and returns a direct pointer to its pcb_datetime_t structure as @ref variant_da_dateTimeExtended.\n
   The return pointer MUST NOT be freed.\n

   @param object the object of interest
   @param name the name of the parameter to fetch

   @return
    - The resulting pcb_datetime_t * pointer
    - NULL if the variant is invalid or not of dateTime type
 */
static inline const pcb_datetime_t* object_da_parameterDateTimeExtendedValue(const object_t* object, const char* name) {
    return variant_da_dateTimeExtended(object_parameterValue(object, name));
}


/**
   @ingroup pcb_core_object
   @brief
   Get an object parameter and converts it to a string_t *

   @details
   This function is provided for convenience. It is fetching a parameter in the object
   and converts it to a string_t * as @ref variant_toString.\n
   The return pointer MUST be freed.\n

   @param object the object of interest
   @param name the name of the parameter to fetch

   @return
    - The converted value
 */
static inline string_t* object_parameterStringValue(const object_t* object, const char* name) {
    return variant_string(object_parameterValue(object, name));
}

/**
   @ingroup pcb_core_object
   @brief
   Get a direct string_t * pointer to an object parameter

   @details
   This function is provided for convenience. It is fetching a parameter in the object
   and returns a direct pointer to its string_t * value as @ref variant_da_string.\n
   The return pointer MUST NOT be freed.\n

   @param object the object of interest
   @param name the name of the parameter to fetch

   @return
    - The resulting string_t * pointer
    - NULL if the variant is invalid or not of string type
 */
static inline const string_t* object_da_parameterStringValue(const object_t* object, const char* name) {
    return variant_da_string(object_parameterValue(object, name));
}

/**
   @ingroup pcb_core_object
   @brief
   Get an object parameter and converts it to a variant_list_t *

   @details
   This function is provided for convenience. It is fetching a parameter in the object
   and converts it to a variant_list_t * as @ref variant_toList.\n
   The return pointer MUST be freed.\n

   @param object the object of interest
   @param name the name of the parameter to fetch

   @return
    - The converted value
 */
static inline variant_list_t* object_parameterListValue(const object_t* object, const char* name) {
    return variant_list(object_parameterValue(object, name));
}

/**
   @ingroup pcb_core_object
   @brief
   Get a direct variant_list_t * pointer to an object parameter

   @details
   This function is provided for convenience. It is fetching a parameter in the object
   and returns a direct pointer to its variant_list_t * value as @ref variant_da_list.\n
   The return pointer MUST NOT be freed.\n

   @param object the object of interest
   @param name the name of the parameter to fetch

   @return
    - The resulting variant_list_t * pointer
    - NULL if the variant is invalid or not of list type
 */
static inline variant_list_t* object_da_parameterListValue(const object_t* object, const char* name) {
    return variant_da_list(object_parameterValue(object, name));
}

/**
   @ingroup pcb_core_object
   @brief
   Get an object parameter and converts it to a variant_map_t *

   @details
   This function is provided for convenience. It is fetching a parameter in the object
   and converts it to a variant_map_t * as @ref variant_toMap.\n
   The return pointer MUST be freed.\n

   @param object the object of interest
   @param name the name of the parameter to fetch

   @return
    - The converted value
 */
static inline variant_map_t* object_parameterMapValue(const object_t* object, const char* name) {
    return variant_map(object_parameterValue(object, name));
}

/**
   @ingroup pcb_core_object
   @brief
   Get a direct variant_map_t * pointer to an object parameter

   @details
   This function is provided for convenience. It is fetching a parameter in the object
   and returns a direct pointer to its variant_map_t * value as @ref variant_da_map.\n
   The return pointer MUST NOT be freed.\n

   @param object the object of interest
   @param name the name of the parameter to fetch

   @return
    - The resulting variant_map_t * pointer
    - NULL if the variant is invalid or not of map type
 */
static inline variant_map_t* object_da_parameterMapValue(const object_t* object, const char* name) {
    return variant_da_map(object_parameterValue(object, name));
}

/**
   @ingroup pcb_core_object
   @brief
   Get an object parameter and converts it to a int

   @details
   This function is provided for convenience. It is fetching a parameter in the object
   and converts it to a int as @ref variant_toFd.\n
   The return pointer MUST be freed.\n

   @param object the object of interest
   @param name the name of the parameter to fetch

   @return
    - The converted value
 */
static inline int object_parameterFdValue(const object_t* object, const char* name) {
    return variant_fd(object_parameterValue(object, name));
}

/**
   @ingroup pcb_core_object
   @brief
   Get an object parameter and converts it to a void *

   @details
   This function is provided for convenience. It is fetching a parameter in the object
   and converts it to a void * as @ref variant_toByteArray.\n
   The return pointer MUST be freed.\n

   @param object the object of interest
   @param name the name of the parameter to fetch
   @param size pointer to a 32 bit integer, at return will contain the size of the memory block returned.

   @return
    - The converted value
 */
static inline void* object_parameterByteArrayValue(const object_t* object, const char* name, uint32_t* size) {
    return variant_byteArray(object_parameterValue(object, name), size);
}

/**
   @ingroup pcb_core_object
   @brief
   Get a direct void * pointer to an object parameter

   @details
   This function is provided for convenience. It is fetching a parameter in the object
   and returns a direct pointer to its void * value as @ref variant_da_byteArray.\n
   The return pointer MUST NOT be freed.\n

   @param object the object of interest
   @param name the name of the parameter to fetch
   @param size pointer to a 32 bit integer, at return will contain the size of the memory block returned.

   @return
    - The resulting void * pointer
    - NULL if the variant is invalid or not of byteArray type
 */
static inline const void* object_da_parameterByteArrayValue(const object_t* object, const char* name, uint32_t* size) {
    return variant_da_byteArray(object_parameterValue(object, name), size);
}

/**
   @ingroup pcb_core_object
   @brief
   Get an object parameter and converts it to a string_list_t *

   @details
   This function is provided for convenience. It is fetching a parameter in the object
   and converts it to a string_list_t * as @ref variant_toStringList.\n
   The return pointer MUST be freed.\n

   @param object the object of interest
   @param name the name of the parameter to fetch
   @param separator The separator used to split the data

   @return
    - The converted value
 */
static inline string_list_t* object_parameterStringListValue(const object_t* object, const char* name, const char* separator) {
    return variant_stringList(object_parameterValue(object, name), separator);
}

#ifdef __cplusplus
}
#endif
#endif
