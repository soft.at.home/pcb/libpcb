/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(PCB_VALIDATOR_H)
#define PCB_VALIDATOR_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <pcb/core/types.h>

/**
   @ingroup pcb_core
   @file
   @brief
   Header file with validator type definitions and public validator functions
 */

/**
   @ingroup pcb_core
   @defgroup pcb_core_validator Validators
   @{

   @brief
   Validator implementation

   @details
   Objects and parameters can contain a validator. The validator is used to
   validate the value(s) and check constraints.

   @section pcb_core_validator_standard Standard Validators
   A small set of standard parameter validators is provided.\n
    - Enumeration validator: Checks that the value of a parameter is in a set of values
    - Range validator: Checks that the value of a parameter is within a certain range, for string parameters it checks that the length of the string is in this range.
    - Minimum validator: Checks that the value of a parameter is at least this value, for string parameters it checks that the length of the string is at least this value
    - Maximum validator: Checks that the value of a parameter is not higher than this value, for strings parameters it checks that the length of strings is not bigger then this value.
   For each of these standard validators a param_validator_create function is available.\n

   @section pcb_core_validator_custom Custom Validators
   Custom validators can be created as well and not only for parameters but also for objects. To create custom parameter validator
   use the function @ref param_validator_create_custom and to create a custom object validator use the @ref object_validator_create function.
   You also need to write a callback function that is doing the real validation. Such a validation function must return true if validation was successful otherwise false.
   Extra validation data can be provided when creating the validator and this data will be used when calling the validation function.

   @section pcb_core_validator_add Adding Validators
   Adding a validator to an object or parameter is done by using the @ref parameter_setValidator or @ref object_setValidator
 */

/**
   @brief
   Enumeration of the possible validator types.

   @details

 */
typedef enum _parameter_validator_type {
    parameter_validator_custom = 0,  /**< A custom validator */
    parameter_validator_enum,        /**< The enum validator */
    parameter_validator_range,       /**< The range validator */
    parameter_validator_minimum,     /**< The minimum validator */
    parameter_validator_maximum,     /**< The maximum validator */
} parameter_validator_type_t;

/**
   @brief
   Parameter validation function definition

   @details
   Custom parameter validation functions must match this function signature.
   Such a function must return false when the validation fails.
 */
typedef bool (* param_validation_handler_t) (parameter_t* parameter, void* validationData);

/**
   @brief
   Object validation function definition

   @details
   Custom object validation functions must match this function signature.
   Such a function must return false when the validation fails.
 */
typedef bool (* object_validation_handler_t) (object_t* object, void* validationData);

/**
   @}
 */

parameter_validator_t* param_validator_create_custom(param_validation_handler_t handler, void* validationData);
parameter_validator_t* param_validator_create_enum(variant_list_t* values);
parameter_validator_t* param_validator_create_range(variant_t* minimum, variant_t* maximum);
parameter_validator_t* param_validator_create_minimum(variant_t* minimum);
parameter_validator_t* param_validator_create_maximum(variant_t* maximum);

void param_validator_destroy(parameter_validator_t* validator);

object_validator_t* object_validator_create(object_validation_handler_t handler, void* validationData);
void object_validator_destroy(object_validator_t* validator);

parameter_validator_type_t param_validator_type(parameter_validator_t* validator);

const variant_t* param_validator_minimum(parameter_validator_t* validator);
const variant_t* param_validator_maximum(parameter_validator_t* validator);
variant_list_t* param_validator_values(parameter_validator_t* validator);
void* param_validator_data(parameter_validator_t* validator);

#ifdef __cplusplus
}
#endif

#endif
