/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(PCB_REPLY_H)
#define PCB_REPLY_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <pcb/utils/variant.h>
#include <pcb/core/types.h>

/**
   @brief
   Definition of a helper macro for iteration through the items of a reply

   @details
   This <b>helper macro</b> iterates forwards through the items of a reply\n

   @param item a pointer to a reply item  that will contain the current item
   @param reply the reply
 */
#define reply_for_each_item(item, reply) \
    for(item = reply_firstItem(reply); item; item = reply_nextItem(item))

#if __STDC_VERSION__ >= 199901L
/**
   @brief
   Definition of a helper macro for iteration through the items of a reply

   @details
   This <b>helper macro</b> iterates forwards through the items of a reply\n

   @param item the name of a pointer to a reply item to be declared that will contain the current item
   @param reply the reply
 */
#define reply_for_each_declare_item(item, reply) \
    for(reply_item_t* item = reply_firstItem(reply); item; item = reply_nextItem(item))
#endif

typedef enum _reply_item_type {
    reply_type_invalid = 0,
    reply_type_object,
    reply_type_notification,
    reply_type_error,
    reply_type_function_return,
} reply_item_type_t;

bool reply_initialize(reply_t* reply);
void reply_cleanup(reply_t* reply);
void reply_errorsCleanup(reply_t* reply);
void reply_clear(reply_t* reply);
request_t* reply_request(reply_t* reply);

uint32_t reply_itemCount(reply_t* reply);
reply_item_t* reply_firstItem(reply_t* reply);
reply_item_t* reply_nextItem(reply_item_t* item);

bool reply_itemsAvailable(reply_t* reply);
void reply_setCompleted(reply_t* reply, bool completed);
bool reply_isComplete(reply_t* reply);
bool reply_hasErrors(reply_t* reply);
uint32_t reply_firstError(reply_t* reply);

bool reply_addObject(reply_t* reply, object_t* object);
bool reply_addNotification(reply_t* reply, notification_t* object);
bool reply_addError(reply_t* reply, uint32_t error, const char* description, const char* info);
bool reply_addFunctionReturn(reply_t* reply, variant_t* retval, argument_value_list_t* returnArgs);

void reply_item_destroy(reply_item_t* item);
reply_item_t* reply_item_take(reply_item_t* item);
reply_item_type_t reply_item_type(reply_item_t* item);

/* return value must not be freed */
object_t* reply_item_object(reply_item_t* item);
notification_t* reply_item_notification(reply_item_t* item);

uint32_t reply_item_error(reply_item_t* item);
const char* reply_item_errorDescription(reply_item_t* item);
const char* reply_item_errorInfo(reply_item_t* item);

/* must not be freed */
const variant_t* reply_item_returnValue(reply_item_t* item);
argument_value_list_t* reply_item_returnArguments(reply_item_t* item);

/**
   @brief
   Sort a linked list of replies based on the object names.

   @details
   Sorts a list of reply_item_t which are part of the the provided reply argument.
   Uses @ref llist_qsort

   @param reply a reply corresponding to a pcb request
 */
bool reply_sort_object_name(reply_t* reply);

#ifdef __cplusplus
}
#endif

#endif


