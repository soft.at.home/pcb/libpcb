/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(PCB_SERIALIZE_H)
#define PCB_SERIALIZE_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <pcb/core/function.h>
#include <pcb/core/object.h>

#include <pcb/utils/string_list.h>

/**
   @file
   @brief
   Header file with serialization type definitions and public serialization helper functions
 */

/**
   @ingroup pcb_core
   @defgroup pcb_core_serialization Serialization Helper Functions
   @{
   @brief
   Serialization helper functions.

   @details
   Because PCB is about communication between processes, data needs to be serialized and at the other end
   reconstructed. These functions mainly aid in reconstructing the objects and requests.\n
   This process is also known as marshalling.

   @section pcb_core_serialization_plugins Serialization Plugins
   The functionality to convert requests, objects, notification, ... is not in the PCB library itself.\n
   For this purpose it is using "serialization plugins". These are nothing less than a shared library which has to export only one function: @ref pcb_register_func.
   In this function the shared library must fill in the provided structure with function pointers.\n
   If a function pointer is set to NULL, it is interpreted as not supported.\n
   A full list of the handlers:
    - serialize_reply_begin
    - serialize_reply_end.
    - serialize_objectListBegin
    - serialize_objectListNext
    - serialize_objectListEnd
    - serialize_functionReturnBegin
    - serialize_functionReturnEnd
    - serialize_functionReturnValue
    - serialize_functionReturnArgumentsStart
    - serialize_functionReturnArguments
    - serialize_request
    - serialize_objectBegin
    - serialize_objectEnd
    - serialize_parameterListBegin
    - serialize_parameterListNext
    - serialize_parameterListEnd
    - serialize_parameter
    - serialize_notification
    - serialize_errorListBegin
    - serialize_errorListNext
    - serialize_errorListEnd
    - serialize_error
    - serialize_cleanup
    - deserialize_checkFormat
    - deserialize
    - deserialize_cleanup
    - load
    - save
    - cleanup
    - unregister

   To reconstruct the data into the original format the serialization plugin must also provide at least the deserialize function.

   @section pcb_core_serialization_serializing Serializing
   During serialization the serializer writes to the out stream of the given peer. The PCB library will make sure that all data is
   put on the underlying file descriptor.

   @section pcb_core_serialization_deserializing Deserializing
   For deserializing the serialization plugin must read the data from the socket by itslef using non-blocking reads. This because the
   serialization plugin is the only one that can understand the content of the packages coming over the socket.\n
   It has to recreate the original data in the same format as when it was serialized.\n
   The desrializer function must handle the socket errors and closed socket (a read returns 0, which means EOF) as follows:
    - the deserializer must return deserialize_error and set pcb_error to pcb_error_connection_shutdown
   After that the libarary will call the correct cleanup functions.

   @section pcb_core_serialization_requests Serializing and Deserializing Requests
   Serializing requests is very easy, you can use all of the request functions to find out all about the request provided.\n
   Reconstructing the request is also very easy, just call the request_create functions again, with the correct data.\n
   The standard request functions do not provide you with a way to set the request identifier (this is generated by the PCB library).\n
   But do not panic, use @ref serialization_setRequestId to set the request identifier.

   @section pcb_core_serialization_objects Serializing and Deserializing Objects
   Serializing objects is not that hard at all, use the object functions to get all information about the object itself.\n
   Do not serialize parameters in the @ref serialize_handlers_t::serialize_objectBegin callback function. The PCB library will call the apropiate parameter
   serialization functions to serialzie the parametes.
   Functions must be serialized in the @ref serialize_handlers_t::serialize_objectBegin callback function, do not forget to take the function arguments into
   account. Also check the request attributes before generating the messages.
   Do not add children or instances to the message, the library will call the @ref serialize_handlers_t::serialize_objectListBegin, @ref serialize_handlers_t::serialize_objectListNext
   and @ref serialize_handlers_t::serialize_objectListEnd at the apropiate time to add children and instances.

   Reconstructing the object on the other side, is a little bit more difficult. Most of the time, not all inforamtion is available on the client side.
   It is possible that the parent object is not available, not all parameters are loaded, ... That is why extra information about the object has to be sent and some
   helper functions are provided to reconstruct an object:
    - serialization_objectCreate: Allocate memory and create the object, if it was not already in the cache, otherwise return the cahced object
    - serialization_parameterCreate: Allocate memory and initialize it, if the parameter was not already in the cache, otherwise it returns the cached parameter
    - serialization_parameterSetValue: Set the parameters value (without changing the state of the parameter)
    - serialization_objectSetParameterCount: Set the the real number of parameters
    - serialization_objectSetChildCount: Set the real number of children
    - serialization_objectSetInstanceCount: Set the real number of instances
    - serialization_pushObjectToCache: Push the reconstructed object into the object cache.

   Altough a request sender can specify that it does not want to use the object cache, the serializer plugin must push a reconstructed
   object into the cache. The PCB library will decide if it needs to be added to the cache or not.

   @section pcb_core_serialization_defaults Default Serialization Plugins
    - libpcb_serialize_ddw.so: This serialization plugin is the default one. It's a cross-platform binary serialization format.
    - libpcb_serialize_odl.so: Serialization plugin where only the load and save functions are implemented (used for loading the object definitions, and defaults)
    - libpcb_serialize_http.sp: Can only deserialize request and serialize objects and notifications
    - libpcb_serialize_tpl.so: Complete ddw-alike pcb serializer based on Troy D. Hanson's tpl serializer
 */

#define SERIALIZE_DEFAULT_MAJOR 0
#define SERIALIZE_DEFAULT_MINOR 0

/**
   @brief
   Serialization format
 */
typedef enum _serialize_format {
    serialize_format_ddw = 0,       /**< The default format */
    serialize_format_odl = 1,       /**< Only used for loading and saving odl files, not for transport of data */
    serialize_format_http = 2,      /**< HTTP format (body can contain json, xml, ....)*/
    serialize_format_tpl = 3,
    serialize_format_default = serialize_format_ddw
} serialize_format_t;

/**
   @brief
   Desirialization return codes

   @details
   The deserializer function must return one of these values
 */
typedef enum _deserialize_return {
    deserialize_done = 0,       /**< desrialization is done completly */
    deserialize_more_data,      /**< more data needed to complete the deserialization. Return this when the socket WOULD block when reading */
    deserialize_error,          /**< An error occured during the deserialization */
    deserialize_more_available  /**< more data is available */
} deserialize_return_t;

#define SERIALIZE_FORMAT(type, major_version, minor_version) (type << 16 | major_version << 8 | minor_version) /**< Build an id using the serializer id, the major version and the minor version */
#define SERIALIZE_FORMAT_TYPE(format) (format >> 16)                                                           /**< Get the serializater id */
#define SERIALIZE_FORMAT_MAJOR(format) ((format >> 8) & 0x000000FF)                                            /**< Get the serializater major version */
#define SERIALIZE_FORMAT_MINOR(format) (format & 0x000000FF)                                                   /**< Get the serializater minor version */

/**
   @brief
   Type of object list

   @details
   When a list of childeren or instances has to be serialized, this type is used in the
   serialize_objectListBegin function.
 */
typedef enum _object_list_type {
    object_list_children = 0x00000000,      /**< Serialize a list of child objects*/
    object_list_instances  = 0x00000001,    /**< Serialize a list of instance objects */
    object_list = 0x00000002,               /**< Serialize a list of objects */
} object_list_type_t;


/**
   @brief
   Structure definition containing the serialization plugin handler functions

   @details
   An instance of such a structure need to be filled by the serializer plugin.
 */
typedef struct _serialize_handlers {
    bool notificationBroadcastSupport;                                                                                      /**< Set to false when this serializer does not support broadcast notifications */
    // serialize reply begin and end
    bool (* serialize_reply_begin)(peer_info_t* peer, request_t* req, uint32_t error);                                      /**< Handler called when a mew messages must be created */
    bool (* serialize_reply_end)(peer_info_t* peer, request_t* req);                                                        /**< Handler called when a the message is done */
    // serialize object lists
    bool (* serialize_objectListBegin)(peer_info_t* peer, request_t* req, object_list_type_t listType);                     /**< Handler called to indicate a start of an object list */
    bool (* serialize_objectListNext)(peer_info_t* peer, request_t* req);                                                   /**< Handler called to indicate another object will be added to the list */
    bool (* serialize_objectListEnd)(peer_info_t* peer, request_t* req);                                                    /**< Handler called to indicate the object list is ended */
    // serialize function returns
    bool (* serialize_functionReturnBegin)(peer_info_t* peer, request_t* req);                                              /**< Handler called to indicate a start a function reply message */
    bool (* serialize_functionReturnEnd)(peer_info_t* peer, request_t* req);                                                /**< Handler called to indicate the end of the function reply message */
    bool (* serialize_functionReturnValue)(peer_info_t* peer, request_t* req, const variant_t* retval);                     /**< Handler called to add a function return value to the message */
    bool (* serialize_functionReturnArgumentsStart)(peer_info_t* peer, request_t* req);                                     /**< Handler called to indicate the start of the function out argument list */
    bool (* serialize_functionReturnArguments)(peer_info_t* peer, request_t* req, argument_value_list_t* args);             /**< Handler called to add the out arguments to the message */
    // serialize requests
    bool (* serialize_request)(peer_info_t* peer, request_t* req);                                                          /**< Handler called to add a request to the message */
    // serialize object
    bool (* serialize_objectBegin)(peer_info_t* peer, request_t* req, object_t* object);                                    /**< Handler called to add object information to the message (do not add parameters), functions can be added */
    bool (* serialize_objectEnd)(peer_info_t* peer, request_t* req);                                                        /**< Handler called to indicate the end of the object in the message */
    // serialize parameters
    bool (* serialize_parameterListBegin)(peer_info_t* peer, request_t* req);                                               /**< Handler called to indicate the start of the parameter list of an object*/
    bool (* serialize_parameterListNext)(peer_info_t* peer, request_t* req);                                                /**< Handler called to indicate a next parameter will be added */
    bool (* serialize_parameterListEnd)(peer_info_t* peer, request_t* req);                                                 /**< Handler called to indicate the end of the parameter list of an object*/
    bool (* serialize_parameter)(peer_info_t* peer, request_t* req, parameter_t* parameter, bool hideValue);                /**< Handler called to add the parameter info to the message (including the values)*/
    // serialize notification
    bool (* serialize_notification)(peer_info_t* peer, request_t* req, notification_t* notification);                       /**< Handler called to add a notification to the message*/
    // serialize error
    bool (* serialize_errorListBegin)(peer_info_t* peer, request_t* req);                                                   /**< Handler called to indicate the start of the error list*/
    bool (* serialize_errorListNext)(peer_info_t* peer, request_t* req);                                                    /**< Handler called to indicate an other error element will be added to the list*/
    bool (* serialize_errorListEnd)(peer_info_t* peer, request_t* req);                                                     /**< Handler called to indicate the end of the error list*/
    bool (* serialize_error)(peer_info_t* peer, request_t* req, uint32_t error, const char* description, const char* info); /**< Handler called to add the error to the message*/
    void (* serialize_cleanup)(peer_info_t* peer);                                                                          /**< Handler called to do clean up for a certain peer */
    // deserialization functions
    bool (* deserialize_checkFormat)(peer_info_t* peer);                                                                    /**< Handler called to see that incomming data can be handled by this serializer */
    deserialize_return_t (* deserialize)(peer_info_t* peer, pcb_t* connection);                                             /**< Handler called read and parse incomming data from the peer */
    void (* deserialize_cleanup)(peer_info_t* peer);                                                                        /**<  Handler called to do clean up for a certain peer */
    // load & save
    bool (* load)(int fd, object_t* parent, const char* filename);                                                          /**<  Handler called to load from a file descriptor, the file descriptor must be closed */
    bool (* save)(int fd, object_t* parent, uint32_t depth, const char* filename);                                          /**<  Handler called to save an object tree to a file descriptor, the file descriptor must be closed */
    bool (* verify)(int fd, const char* filename, variant_map_t* info, bool* hasDefinition);                                /**<  Handler called to verify the file, the file descriptor must be closed */
    // cleanup
    void ( * cleanup)(void);                                                                                                /**< Global clean up of the serializer */
    // unregister
    void ( * unregister)(void);                                                                                             /**< Called when the serializer will be removed from memory */
} serialize_handlers_t;

/**
   @brief
   Definition of the register function.

   @details
   A serializer should have at least this function available.
   When a serializer is loaded into memory the pcb library will try to resolve only this function.
   The serializer itself should create a structure and set all handlers to the correct function pointers.
 */
typedef void (* pcb_register_func) (void);
typedef void (* pcb_register_func_t) (void);

/**
   @brief
   Used by a deserializer to add on an object being deserialized

   @details
   A callback function can be added to the object being currently deserialized.
   There is no garantee that all object information is available at once.
   When a deserializer is deserializing the object into the object cache it is possible that at any time
   the user of object cache decides to clean it. At that point the object will be destroyed.
   By setting the destroy handler on the currently deserialized object the deserializer will be informed
   he can stop recreating the object. When the object is fulle recreated the handler must be removed again.
 */
typedef void (* serialization_object_destroy_handler_t) (object_t* object, void* userdata);

/**
   @}
 */

bool serialization_initialize(void);
void serialization_cleanup(void);
void serialization_unregister(void);

bool serialization_loadPlugins(const char* pluginPath, const char* serializers);
bool serialization_loadPluginsStatic(pcb_register_func_t serfn[]);
bool serialization_addSerializer(serialize_handlers_t* serializerHandlers, uint32_t format);
serialize_handlers_t* serialization_getHandlers(uint32_t format);
serialize_handlers_t* serialization_getHandlersForSocket(peer_info_t* peer);

object_t* serialization_objectCreate(peer_info_t* peer, object_t* parent, const char* parentPath, const char* name, const char* parentKeyPath, const char* key, const uint32_t attributes, const object_state_t state, request_t* req);
void serialization_objectDelete(object_t* object);
void serialization_setObjectDestroyHandler(object_t* object, serialization_object_destroy_handler_t handler, void* userdata);
parameter_t* serialization_parameterCreate(object_t* object, const char* name, parameter_type_t type, const uint32_t attributes, const parameter_state_t state);
function_t* serialization_functionCreate(object_t* object, const char* name, function_type_t type, const char* typeName, const uint32_t attributes);
function_argument_t* serialization_argumentCreate(function_t* function, const char* name, argument_type_t type, const char* typeName, const uint32_t attributes);

bool serialization_parameterSetValue(parameter_t* parameter, const variant_t* var);

bool serialization_objectSetParameterCount(object_t* object, uint32_t count);
bool serialization_objectSetChildCount(object_t* object, uint32_t count);
bool serialization_objectSetInstanceCount(object_t* object, uint32_t count);
bool serialization_objectSetFunctionCount(object_t* object, uint32_t count);
bool serialization_pushObjectToCache(object_t* parent, object_t* object, request_t* req);
bool serialization_updateCache(object_t* parent, notification_t* notification);

bool serialization_setRequestId(request_t* req, uint32_t id);

#ifdef __cplusplus
}
#endif

#endif
