/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(PCB_MAIN_H)
#define PCB_MAIN_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <pcb/utils/variant.h>
#include <pcb/utils/connection.h>
#include <pcb/core/function.h>
#include <pcb/core/request.h>
#include <pcb/core/serialize.h>
#include <pcb/utils/privilege.h>

/**
   @file
   @brief

 */

/**
   @ingroup pcb_core
   @defgroup pcb_core_pcb PCB
   @{

   @brief
 */

typedef enum upc_event_t {
    upc_event_changed,
    upc_event_deleted,
} upc_event_t;

typedef void (* pcb_cleanup_handler_t) (pcb_t* pcb);

typedef void (* upc_event_cb_t) (upc_event_t event, object_t*, parameter_t*, void* userdata);

// Definitions to keep code backwards compatible
#define plugin_argc argc
#define plugin_argv argv
#define client_argc argc
#define client_argv argv

typedef struct _app_privileges_config {
    bool enabled;
    const char* user;
    const char* group;
    priv_cap_t retain_capabilities;
} privileges_config_t;


struct _app_config {
    // common options
    uint32_t trace_level;
    uint32_t trace_output;
    const char* name;
    const char* busname;
    bool foreground;
    int argc;
    char** argv;
    const char* certificate;
    const char* ca;
    int priority;

    // pcb plugin options
    const char* ipc_socket_name;
    const char* tcp_address;
    const char* tcp_port;
    const char* ns;
    const char* configfile;

    // pcb client options
    const char* uri;
    const char* sofile;
    connection_list_t connections;
    bool exit_app;

    // global pcb parameters
    pcb_t* pcb;
    peer_info_t* system_bus;
    bool sync;
    bool ssl_connection;
    bool block_signals;

    privileges_config_t privileges;
};

typedef struct _app_config app_config_t;
typedef struct _app_config plugin_config_t;
typedef struct _app_config client_config_t;

/**
   @}
 */

pcb_t* pcb_create(const char* name, int argc, char* argv[]);
pcb_t* pcb_createStatic(const char* name, int argc, char* argv[], pcb_register_func_t serfn[]);
bool pcb_initialize(pcb_t* pcb, const char* name, int argc, char* argv[]);
bool pcb_initializeStatic(pcb_t* pcb, const char* name, int argc, char* argv[], pcb_register_func_t serfn[]);
bool pcb_initialize_r(pcb_t* pcb, const char* name, int argc, char* argv[]);

void pcb_cleanup(pcb_t* pcb);
void pcb_cleanup_r(pcb_t* pcb);
void pcb_destroy(pcb_t* pcb);
void pcb_destroy_r(pcb_t* pcb);

int pcb_argumentCount(pcb_t* pcb);
char** pcb_arguments(pcb_t* pcb);

pcb_t* pcb_fromConnection(connection_info_t* connection);

// UPC
bool pcb_upcIsEnabled(pcb_t* pcb);
bool pcb_upcEnable(pcb_t* pcb, bool enable);
bool pcb_upcOverwriteIsEnabled(pcb_t* pcb);
bool pcb_upcOverwriteEnable(pcb_t* pcb, bool enable);
bool pcb_upcSetEventCallback(pcb_t* pcb, upc_event_cb_t cb, void* userdata);

// plug-in config
bool pcb_setPluginConfig(pcb_t* pcb, plugin_config_t* config);
plugin_config_t* pcb_pluginConfig(pcb_t* pcb);

// client config
bool pcb_setClientConfig(pcb_t* pcb, client_config_t* config);
client_config_t* pcb_clientConfig(pcb_t* pcb);

// user data
bool pcb_setUserData(pcb_t* pcb, void* data);
void* pcb_userData(pcb_t* pcb);

// set pcb handler(s)
bool pcb_setCleanUpHandler(pcb_t* pcb, pcb_cleanup_handler_t fn);

// mark a peer as trusted
bool pcb_setTrusted(pcb_t* pcb, peer_info_t* peer, bool trusted);
bool pcb_setManaged(pcb_t* pcb, peer_info_t* peer);
bool pcb_setDefaultUid(pcb_t* pcb, peer_info_t* peer, uint32_t uid);

// send
bool pcb_sendRequest(pcb_t* pcb, peer_info_t* peer, request_t* req);
bool pcb_waitSendRequest(pcb_t* pcb, peer_info_t* peer, request_t* req);
bool pcb_sendNotification(pcb_t* pcb, peer_info_t* to, peer_info_t* from, notification_t* notification);
bool pcb_waitForReply(pcb_t* pcb, request_t* req, struct timeval* timeout);

// data model
datamodel_t* pcb_datamodel(pcb_t* pcb);

// connection
connection_info_t* pcb_connection(pcb_t* pcb);
void pcb_handleQueuedEvents(pcb_t* pcb);

// cache management
datamodel_t* pcb_cache(pcb_t* pcb);
bool pcb_cacheClear(pcb_t* pcb);
bool pcb_cacheRemove(pcb_t* pcb, const char* path, uint32_t pathAttrib);
bool pcb_cacheRemoveObject(pcb_t* pcb, object_t* object);
bool pcb_cacheRemoveSingleObject(pcb_t* pcb, object_t* object);
bool pcb_cacheCommit(pcb_t* pcb);

// object helpers
object_t* pcb_getObject(pcb_t* pcb, const char* path, const uint32_t pathAttr);
object_t* pcb_getRootObject(pcb_t* pcb, request_t* req);

// request handlers
request_handlers_t* pcb_getRequestHandlers(pcb_t* pcb);
bool pcb_setRequestHandlers(pcb_t* pcb, request_handlers_t* handlers);

// non data model notifications
bool pcb_setNotifyHandler(pcb_t* pcb, notify_handler_t notifyHandler);
bool pcb_addNotifyHandler(pcb_t* pcb, notify_handler_t notifyHandler);
bool pcb_removeNotifyHandler(pcb_t* pcb, notify_handler_t notifyHandler);

// data format
bool pcb_setDefaultFormat(pcb_t* pcb, uint32_t format);

// peer helper functions

// write to a peer
bool pcb_replyBegin(peer_info_t* peer, request_t* req, uint32_t error);
bool pcb_replyEnd(peer_info_t* peer, request_t* req);
bool pcb_writeObjectListBegin(peer_info_t* peer, request_t* req, object_list_type_t listType);
bool pcb_writeObjectListNext(peer_info_t* peer, request_t* req);
bool pcb_writeObjectListEnd(peer_info_t* peer, request_t* req);
bool pcb_writeObjectBegin(peer_info_t* peer, request_t* req, object_t* object);
bool pcb_writeObjectEnd(peer_info_t* peer, request_t* req);
bool pcb_writeParameterListBegin(peer_info_t* peer, request_t* req);
bool pcb_writeParameterListNext(peer_info_t* peer, request_t* req);
bool pcb_writeParameterListEnd(peer_info_t* peer, request_t* req);
bool pcb_writeParameter(peer_info_t* peer, request_t* req, parameter_t* parameter, bool hideValue);
bool pcb_writeNotification(peer_info_t* peer, request_t* req, notification_t* notification);
bool pcb_writeFunctionReturnBegin(peer_info_t* peer, request_t* req);
bool pcb_writeFunctionReturnEnd(peer_info_t* peer, request_t* req);
bool pcb_writeReturnValue(peer_info_t* peer, request_t* req, const variant_t* retval);
bool pcb_writeReturnArgumentsStart(peer_info_t* peer, request_t* req);
bool pcb_writeReturnArguments(peer_info_t* peer, request_t* req, argument_value_list_t* args);
bool pcb_writeFunctionReturn(function_call_t* fcall, const variant_t* retval, argument_value_list_t* args);
bool pcb_writeErrorListBegin(peer_info_t* peer, request_t* req);
bool pcb_writeErrorListNext(peer_info_t* peer, request_t* req);
bool pcb_writeErrorListEnd(peer_info_t* peer, request_t* req);
bool pcb_writeError(peer_info_t* peer, request_t* req, uint32_t error, const char* description, const char* info);

// read from a peer
bool pcb_read(pcb_t* pcb, peer_info_t* peer);

// peer and requests
request_t* pcb_findNotifyRequest(peer_info_t* peer, uint32_t requestId);
request_t* pcb_getPendingRequest(peer_info_t* peer, uint32_t requestId);
request_t* pcb_firstPendingRequest(peer_info_t* peer);
request_t* pcb_nextPendingRequest(peer_info_t* peer, request_t* reference);
bool pcb_addReceivedRequest(peer_info_t* peer, request_t* req);
bool pcb_broadcastEnable(peer_info_t* peer, bool enable);

void pcb_getNotifyRequests(peer_info_t* peer, request_list_t* list);
void pcb_getForwardedRequests(peer_info_t* peer, request_list_t* list);

// peer and serializers
bool pcb_setSerializeInfo(peer_info_t* peer, void* info);
void* pcb_getSerializeInfo(peer_info_t* peer);

bool pcb_setDeserializeInfo(peer_info_t* peer, void* info);
void* pcb_getDeserializeInfo(peer_info_t* peer);

#ifdef __cplusplus
}
#endif

#endif
