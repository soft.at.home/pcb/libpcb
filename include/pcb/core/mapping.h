/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(PCB_MAPPING_H)
#define PCB_MAPPING_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <pcb/utils/linked_list.h>
#include <pcb/utils/uri.h>
#include <pcb/utils/connection.h>

#include <pcb/core/parameter.h>

typedef enum _rule_type {
    rule_type_object = 1,
    rule_type_parameter,
    rule_type_parameter_request,
    rule_type_parameter_object,
    rule_type_function,
} rule_type_t;

typedef enum _rule_action {
    rule_action_accept = 1,
    rule_action_drop,
} rule_action_t;

/**
   @brief
   Parameter translate value handler

   @details
   A callback function can be added to the parameter which is called when the parameter value needs to be translated.
   This kind of handler is mainly used by mapping plug-ins, so they can create a view on certain parameters.
 */
typedef bool (* parameter_translate_value_handler_t) (translate_direction_t direction, variant_t* client, variant_map_t* plugin);

/**
   @brief
   Parameter translate name handler

   @details
   A callback function can be added to mapping rules which is called when the parameter name needs to be translated.
   This kind of handler is mainly used by mapping plug-ins, so they can create a view on certain parameters.
 */
typedef void (* parameter_translate_name_handler_t) (translate_direction_t direction, string_t* clientName, string_t* pluginName);

/**
   @brief
   Parameter translate type handler

   @details
   A callback function can be added to mapping rules which is called when the parameter type needs to be translated.
   This kind of handler is mainly used by mapping plug-ins, so they can create a view on certain parameters.
 */
typedef void (* parameter_translate_type_handler_t) (translate_direction_t direction, parameter_t* param);

// object destinations (for mapping)
bool object_hasDestinations(object_t* object);
uint32_t object_destinationCount(object_t* object);
object_destination_t* object_map(object_t* object, const char* uri, bool recursive);
object_destination_t* object_addDestination(object_t* object, const char* uri);
object_destination_t* object_firstDestination(object_t* object);
object_destination_t* object_nextDestination(object_t* object, object_destination_t* reference);

bool request_translate(request_t* orig_req, request_t* trans_req, object_destination_t* dest);
bool object_translate(object_t* object, object_destination_t* dest);

object_destination_t* parameter_destination(parameter_t* parameter);
bool parameter_addMap(parameter_t* parameter, const char* destparam, variant_t* value);
bool parameter_setTranslator(parameter_t* parameter, parameter_translate_value_handler_t translatefn);

// destination management
void destination_delete(object_destination_t* dest);
bool destination_addParameter(object_destination_t* dest, const char* clientName, const char* destName);
bool destination_addFunction(object_destination_t* dest, const char* clientName, const char* destName);
peer_info_t* destination_peer(object_destination_t* dest);
const uri_t* destination_URI(object_destination_t* dest);
bool destination_setURI(object_destination_t* dest, const char* uri);

mapping_rule_t* destination_createRule(object_destination_t* dest, rule_type_t type, rule_action_t action);
mapping_rule_t* rule_createRule(mapping_rule_t* parent, rule_type_t type, rule_action_t action);
bool rule_setRegExp(mapping_rule_t* rule, const char* regexp);
bool rule_addModifierRename(mapping_rule_t* rule, parameter_translate_name_handler_t renameHandler);
bool rule_addModifierTranslate(mapping_rule_t* rule, parameter_translate_value_handler_t translateHandler);
bool rule_addModifierCast(mapping_rule_t* rule, parameter_translate_type_handler_t castHandler);

mapping_rule_t* rule_parent(mapping_rule_t* rule);

void rule_delete(mapping_rule_t* rule);

#ifdef __cplusplus
}
#endif

#endif
