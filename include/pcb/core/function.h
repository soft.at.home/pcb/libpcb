/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(PCB_FUNCTION_H)
#define PCB_FUNCTION_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <pcb/utils/variant.h>
#include <pcb/utils/connection.h>
#include <pcb/utils/string_list.h>
#include <pcb/core/types.h>
#include <pcb/core/reply.h>
#include <pcb/core/request.h>

// iterator macros
//---------------------------------------------------------------------------------------------
/**
   @brief
   Definition of a helper macro for forward iteration through the argument linked list (head->tail)

   @details
   \n
   This <b>helper macro</b> iterates forward through a <b>argument linked list</b> starting from the head\n
   \n
   <b>Warning</b>: don&rsquo;t <b>modify</b> the argument linked list during the iteration!

   @param it a pointer variable to the list items
   @param list the list
 */
#define argument_value_list_for_each(it, list) \
    for(it = argument_valueFirstArgument(list); it; it = argument_valueNextArgument(it))

#if __STDC_VERSION__ >= 199901L
/**
   @brief
   Definition of a helper macro for forward iteration through the argument linked list (head->tail)

   @details
   \n
   This <b>helper macro</b> iterates forward through a <b>argument linked list</b> starting from the head\n
   \n
   <b>Warning</b>: don&rsquo;t <b>modify</b> the argument linked list during the iteration!

   @param it the name of the arguemnt_value_t to be declared
   @param list the list
 */
#define argument_value_list_for_each_declare(it, list) \
    for(argument_value_t* it = argument_valueFirstArgument(list); it; it = argument_valueNextArgument(it))
#endif

typedef enum _function_attributes {
    function_attr_default            = 0x00000000,   /**< Apply default attributes */
    function_attr_template_only      = 0x00000001,   /**< The function is only available in a template object, not in instances. This attribute is ignored for single instance objects */
    function_attr_message            = 0x00000002,   /**< This function is message base and will not generate a reply */
    function_attr_variadic           = 0x00000004,   /**< This function handles variadic arguments */
    function_attr_async              = 0x00000008,   /**< This function is an asynchronous function */
} function_attribute_t;

typedef enum _function_argument_attributes {
    argument_attr_default   = 0x00000000,
    argument_attr_in        = 0x00000001,
    argument_attr_out       = 0x00000002,
    argument_attr_mandatory = 0x00000004,
} argument_attribute_t;

typedef enum _argument_type {
    argument_type_unknown = 0,     /**< An argument can not have this type */
    argument_type_string,          /**< The argument is a string */
    argument_type_int8,            /**< The argument is a 8-bit signed integer */
    argument_type_int16,           /**< The argument is a 16-bit signed integer */
    argument_type_int32,           /**< The argument is a 32-bit signed integer */
    argument_type_int64,           /**< The argument is a 64-bit signed integer */
    argument_type_uint8,           /**< The argument is a 8-bit unsigned signed integer */
    argument_type_uint16,          /**< The argument is a 16-bit unsigned signed integer */
    argument_type_uint32,          /**< The argument is a 32-bit unsigned signed integer */
    argument_type_uint64,          /**< The argument is a 64-bit unsigned signed integer */
    argument_type_bool,            /**< The argument is a boolean */
    argument_type_date_time,       /**< The argument is a date time field */
    argument_type_list,            /**< The argument is a list of variants */
    argument_type_file_descriptor, /**< The argument is a file descriptor */
    argument_type_byte_array,      /**< The argument is a byte array */
    argument_type_custom,          /**< Custom argument type */
    argument_type_variant,         /**< The argument is a variant */
    argument_type_double,          /**< The argument is a double */
} argument_type_t;

typedef enum _function_type {
    function_type_void = 0,        /**< void function */
    function_type_string,          /**< function returns a string */
    function_type_int8,            /**< function returns a 8-bit signed integer */
    function_type_int16,           /**< function returns a 16-bit signed integer */
    function_type_int32,           /**< function returns a 32-bit signed integer */
    function_type_int64,           /**< function returns a 64-bit signed integer */
    function_type_uint8,           /**< function returns a 8-bit unsigned signed integer */
    function_type_uint16,          /**< function returns a 16-bit unsigned signed integer */
    function_type_uint32,          /**< function returns a 32-bit unsigned signed integer */
    function_type_uint64,          /**< function returns a 64-bit unsigned signed integer */
    function_type_bool,            /**< function returns a boolean */
    function_type_date_time,       /**< function returns a date time field */
    function_type_list,            /**< function returns a list of variants */
    function_type_file_descriptor, /**< function returns a file descriptor */
    function_type_byte_array,      /**< function returns a byte array */
    function_type_custom,          /**< function returns a custom type */
    function_type_variant,         /**< function returns a variant */
    function_type_double,          /**< function returns a double */
} function_type_t;

typedef enum _function_exec_state {
    function_exec_init = -1,
    function_exec_done = 0,
    function_exec_executing,
    function_exec_error,
    function_exec_cancel,
} function_exec_state_t;

typedef function_exec_state_t (* function_handler_t) (function_call_t* fcall, argument_value_list_t* args, variant_t* retval);
typedef void (* function_destroy_handler_t) (function_t* function);
typedef void (* function_cancel_handler_t) (function_call_t* fcall, void* userdata);

// functions
function_t* function_create(object_t* object, const char* name, const function_type_t returnType, const uint32_t attributes);
function_t* function_createCustomType(object_t* object, const char* name, const char* returnType, const uint32_t attributes);
function_t* function_copy(object_t* destobject, const function_t* srcFunction);
void function_delete(function_t* function);

function_t* function_base(function_t* function);
bool function_setUserData(function_t* function, void* data);
void* function_getUserData(function_t* function);
bool function_setDestroyHandler(function_t* function, function_destroy_handler_t fn);

// owner
object_t* function_owner(const function_t* function);

const char* function_name(function_t* function);
function_attribute_t function_attributes(function_t* function);
function_type_t function_type(function_t* function);
const char* function_typeName(function_t* function);

bool function_setHandler(function_t* function, function_handler_t funcHandler);
function_handler_t function_getHandler(function_t* function);
bool function_setVariadic(function_t* function, bool enable);
bool function_isImplemented(function_t* function);
bool function_isVariadic(function_t* function);
bool function_verifyMandatoryArguments(function_t* function, argument_value_list_t* args);

// acl
bool function_hasAcl(function_t* function);
const llist_t* function_getACL(const function_t* function);
void function_getCalculatedACL(const function_t* function, llist_t* acl);
bool function_setACL(function_t* function, const llist_t* acl);
bool function_aclSet(function_t* function, uint32_t id, uint16_t flags);
bool function_aclAdd(function_t* function, uint32_t id, uint16_t flags);
bool function_aclDel(function_t* function, uint32_t id, uint16_t flags);
bool function_canRead(function_t* function, uint32_t uid);
bool function_canExecute(function_t* function, uint32_t uid);

#ifdef PCB_HELP_SUPPORT
bool function_setDescription(function_t* function, const char* description);
const char* function_getDescription(function_t* function);
#endif

function_argument_t* function_firstArgument(function_t* function);
function_argument_t* function_nextArgument(function_argument_t* ref);
function_argument_t* function_prevArgument(function_argument_t* ref);
function_argument_t* function_lastArgument(function_t* function);
function_argument_t* function_getArgument(function_t* function, const char* argName);

// function arguments
function_argument_t* argument_create(function_t* function, const char* name, const argument_type_t type, const uint32_t attributes);
function_argument_t* argument_createCustomType(function_t* function, const char* name, const char* type, const uint32_t attributes);
void argument_delete(function_argument_t* argument);

const char* argument_name(function_argument_t* arg);
argument_type_t argument_type(function_argument_t* arg);
const char* argument_typeName(function_argument_t* arg);
argument_attribute_t argument_attributes(function_argument_t* arg);
bool argument_isMandatory(function_argument_t* arg);

// argument values
bool argument_value_list_initialize(argument_value_list_t* args);
argument_value_t* argument_valueCreate(const char* name);
void argument_valueDestroy(argument_value_t* argval);
argument_value_t* argument_valueByName(argument_value_list_t* values, const char* argName);
argument_value_t* argument_valueByIndex(argument_value_list_t* values, uint32_t index);

variant_t* argument_value(argument_value_t* arg);
const char* argument_valueName(argument_value_t* arg);

bool argument_valueAppend(argument_value_list_t* args, argument_value_t* arg);
bool argument_valuePrepend(argument_value_list_t* args, argument_value_t* arg);

bool argument_valueAdd(argument_value_list_t* args, const char* name, const variant_t* value);
bool argument_valueSet(argument_value_list_t* args, const char* name, variant_t* value);

uint32_t argument_valueCount(argument_value_list_t* values);
argument_value_t* argument_valueTakeArgumentFirst(argument_value_list_t* args);
void argument_valueTakeArgument(argument_value_t* arg);

argument_value_t* argument_valueFirstArgument(argument_value_list_t* args);
argument_value_t* argument_valueNextArgument(argument_value_t* it);
argument_value_t* argument_valuePrevArgument(argument_value_t* it);
argument_value_t* argument_valueLastArgument(argument_value_list_t* args);

void argument_valueClear(argument_value_list_t* values);

char* argument_takeChar(argument_value_list_t* args);
int8_t argument_takeInt8(argument_value_list_t* args);
int16_t argument_takeInt16(argument_value_list_t* args);
int32_t argument_takeInt32(argument_value_list_t* args);
int64_t argument_takeInt64(argument_value_list_t* args);
uint8_t argument_takeUInt8(argument_value_list_t* args);
uint16_t argument_takeUInt16(argument_value_list_t* args);
uint32_t argument_takeUInt32(argument_value_list_t* args);
uint64_t argument_takeUInt64(argument_value_list_t* args);
bool argument_takeBool(argument_value_list_t* args);
double argument_takeDouble(argument_value_list_t* args);
struct tm* argument_takeDateTime(argument_value_list_t* args);
pcb_datetime_t* argument_takeDateTimeExtended(argument_value_list_t* args);
variant_list_t* argument_takeArray(argument_value_list_t* args);
variant_map_t* argument_takeMap(argument_value_list_t* args);
void* argument_takeByteArray(argument_value_list_t* args, uint32_t* size);

bool argument_get(argument_value_t** arg, argument_value_list_t* args, uint32_t reqattr, const char* argname);
bool argument_getChar(char** value, argument_value_list_t* args, uint32_t reqattr, const char* argname, const char* def);
bool argument_getInt8(int8_t* value, argument_value_list_t* args, uint32_t reqattr, const char* argname, int8_t def);
bool argument_getInt16(int16_t* value, argument_value_list_t* args, uint32_t reqattr, const char* argname, int16_t def);
bool argument_getInt32(int32_t* value, argument_value_list_t* args, uint32_t reqattr, const char* argname, int32_t def);
bool argument_getInt64(int64_t* value, argument_value_list_t* args, uint32_t reqattr, const char* argname, int64_t def);
bool argument_getUInt8(uint8_t* value, argument_value_list_t* args, uint32_t reqattr, const char* argname, uint8_t def);
bool argument_getUInt16(uint16_t* value, argument_value_list_t* args, uint32_t reqattr, const char* argname, uint16_t def);
bool argument_getUInt32(uint32_t* value, argument_value_list_t* args, uint32_t reqattr, const char* argname, uint32_t def);
bool argument_getUInt64(uint64_t* value, argument_value_list_t* args, uint32_t reqattr, const char* argname, uint64_t def);
bool argument_getBool(bool* value, argument_value_list_t* args, uint32_t reqattr, const char* argname, bool def);
bool argument_getDouble(double* value, argument_value_list_t* args, uint32_t reqattr, const char* argname, double def);
bool argument_getDateTime(struct tm** value, argument_value_list_t* args, uint32_t reqattr, const char* argname, struct tm* def);
bool argument_getDateTimeExtended(pcb_datetime_t** value, argument_value_list_t* args, uint32_t reqattr, const char* argname, pcb_datetime_t* def);
bool argument_getList(variant_list_t** value, argument_value_list_t* args, uint32_t reqattr, const char* argname, variant_list_t* def);
bool argument_getMap(variant_map_t** value, argument_value_list_t* args, uint32_t reqattr, const char* argname, variant_map_t* def);
bool argument_getFd(int* value, argument_value_list_t* args, uint32_t reqattr, const char* argname, int def);
bool argument_getByteArray(void** value, uint32_t* size, argument_value_list_t* args, uint32_t reqattr, const char* argname, char* def);
bool argument_getStringList(string_list_t** value, argument_value_list_t* args, uint32_t reqattr, const char* argname, const char* separator);

// function calls
function_call_t* fcall_create(peer_info_t* peer, request_t* req, function_t* function, object_t* obj);
void fcall_destroy(function_call_t* fcall);

// set handlers function call handlers
bool fcall_setCancelHandler(function_call_t* fcall, function_cancel_handler_t handler);

// user data
bool fcall_setUserData(function_call_t* fcall, void* userdata);
void* fcall_userData(function_call_t* fcall);

// function call properties
peer_info_t* fcall_peer(function_call_t* fcall);
request_t* fcall_request(function_call_t* fcall);
uint32_t fcall_userID(function_call_t* fcall);
function_t* fcall_function(function_call_t* fcall);
object_t* fcall_object(function_call_t* fcall);
function_exec_state_t fcall_state(function_call_t* fcall);

// execute function call
function_exec_state_t fcall_execute(function_call_t* fcall, argument_value_list_t* args, variant_t* retval);

// add errors to the reply of a function call
static inline bool fcall_addErrorReply(function_call_t* fcall, uint32_t error, const char* description, const char* info) {
    return reply_addError(request_reply(fcall_request(fcall)), error, description, info);
}

/* The following function should become deprecated
     - argument_getArray

 */
static inline bool argument_getArray(variant_list_t** value, argument_value_list_t* args, uint32_t reqattr, const char* argname, variant_list_t* def) {
    return argument_getList(value, args, reqattr, argname, def);
}

#ifdef __cplusplus
}
#endif

#endif

