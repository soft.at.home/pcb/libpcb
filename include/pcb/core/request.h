/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(PCB_REQUEST_H)
#define PCB_REQUEST_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <pcb/utils/variant.h>
#include <pcb/utils/connection.h>
#include <pcb/core/types.h>

typedef bool (* request_replyHandler)(request_t* req, pcb_t* pcb, peer_info_t* from, void* userdata);
typedef bool (* request_replyChildHandler)(request_t* req, request_t* parent_req, pcb_t* pcb, peer_info_t* from, void* userdata);
typedef bool (* request_replyItemHandler)(request_t* req, reply_item_t* item, pcb_t* pcb, peer_info_t* from, void* userdata);
typedef bool (* request_doneHandler)(request_t* req, pcb_t* pcb, peer_info_t* from, void* userdata);
typedef void (* request_cancelHandler)(request_t* req, void* userdata);
typedef void (* request_destroyHandler)(request_t* req, void* userdata);
typedef void (* request_cleanupSerializationData)(request_t* req, void* serdata);

typedef enum _request_states {
    request_state_created   = 0x00000000,
    request_state_sent,
    request_state_received,
    request_state_handling,
    request_state_forwarded,
    request_state_handling_reply,
    request_state_waiting_for_reply,
    request_state_done,
    request_state_canceled,
    request_state_destroy,
    request_state_destroyed,
} request_states_t;

typedef enum _request_item_type {
    request_type_unknown = 0,

    // data model requests
    request_type_get_object,        /**< Get object(s) request */
    request_type_set_object,        /**< Set object request */
    request_type_create_instance,   /**< Create object instance request, can only be done on template objects */
    request_type_delete_instance,   /**< Delete object instance request, can only be done on instance objects */
    request_type_find_objects,      /**< Search object request */
    request_type_exec_function,     /**< Execute an object's function */

    request_type_close_request,     /**< Close a persistent request */

    request_type_open_session,      /**< Open a session to a PCB server, optional for IPC connections, obligatorily for TCP connections */
} request_type_t;

#define request_copy_default         0x00000000  /**< Copy full request */
#define request_copy_no_parameters   0x00000001  /**< Copy request without parameters */

typedef bool (* getObject_handler_t) (peer_info_t* peer, datamodel_t* datamodel, request_t* req);
typedef bool (* setObject_handler_t) (peer_info_t* peer, datamodel_t* datamodel, request_t* req);
typedef bool (* createInstance_handler_t) (peer_info_t* peer, datamodel_t* datamodel, request_t* req);
typedef bool (* deleteInstance_handler_t) (peer_info_t* peer, datamodel_t* datamodel, request_t* req);
typedef bool (* findObjects_handler_t) (peer_info_t* peer, datamodel_t* datamodel, request_t* req);
typedef bool (* executeFunction_handler_t) (peer_info_t* peer, datamodel_t* datamodel, request_t* req);

typedef bool (* closeRequest_handler_t) (peer_info_t* peer, request_t* req);
typedef bool (* notify_handler_t) (pcb_t* pcb, peer_info_t* from, notification_t* notification);

typedef bool (* openSession_handler_t) (peer_info_t* peer, request_t* req);


struct _request_handlers {
    getObject_handler_t getObject;
    setObject_handler_t setObject;
    createInstance_handler_t createInstance;
    deleteInstance_handler_t deleteInstance;
    findObjects_handler_t findObjects;
    executeFunction_handler_t executeFunction;
    closeRequest_handler_t closeRequest;
    notify_handler_t notify;

    openSession_handler_t openSession;
};

#define request_common_default                   0x00000000                          /**< Default request behavior */
#define request_common_path_key_notation         0x00000001                          /**< The request holds object paths in key notation */
#define request_common_path_slash_separator      0x00000002                          /**< The request holds object paths with slash seperator */
#define request_common_path_slash_seperator      request_common_path_slash_separator /**< @deprecated Use @ref request_common_path_slash_separator */
#define request_getObject_parameters             0x00000004                          /**< Reply all object parameters */
#define request_getObject_functions              0x00080000                          /**< Reply all object functions */
#define request_getObject_children               0x00000008                          /**< Reply the list of all children */
#define request_getObject_instances              0x00000010                          /**< Reply the list of all instances */
#define request_getObject_template_info          0x00000020                          /**< Include template info in the reply */
#define request_getObject_all                    (request_getObject_parameters | request_getObject_children | request_getObject_instances | request_getObject_functions | request_getObject_template_info)
#define request_getObject_definition             (request_getObject_parameters | request_getObject_children | request_getObject_functions | request_getObject_template_info)
#define request_notify_values_changed            0x00000040 /**< Make the get request persistent, and send updates and notifications if a parameter changes */
#define request_notify_object_added              0x00000080 /**< Make the get request persistent, and send updates and notifications if an object is added */
#define request_notify_object_deleted            0x00000100 /**< Make the get request persistent, and send updates and notifications if an object is deleted */
#define request_notify_custom                    0x00000200 /**< Make the get request persistent, and send custom notifications */
#define request_notify_all                       (request_notify_values_changed | request_notify_object_added | request_notify_object_deleted | request_notify_custom)
#define request_notify_only                      0x00000400 /**< Do not reply the initial values, only notifications and updates */
#define request_notify_no_updates                0x00000800 /**< Do not send updates, notifications only */
#define request_no_object_caching                0x00001000 /**< Do not cache the replied objects */
#define request_find_include_parameters          0x00002000 /**< Only used for search requests, include parameter names in the search */
#define request_find_include_values              0x00004000 /**< Only used for search requests, include parameter values in the search */
#define request_function_args_by_name            0x00008000 /**< Indicate that function arguments are named */
#define request_createObject_persistent          0x00010000 /**< The newly created object will be persistent, used to overide template settings */
#define request_getObject_keep_hierarchy         0x00020000 /**< When replying keep the hierarchy of the objects, only used in dispatchers */
#define request_createObject_not_persistent      0x00040000 /**< The newly created object will be not persistent, used to overide template settings */
#define request_setObject_validate_only          0x00100000 /**< Only validate new values, do not commit */
#define request_common_include_namespace         0x00200000 /**< Include the namespace in the object paths */
#define request_getObject_validators             0x00400000 /**< Include validators in the reply */
#define request_getObject_description            0x00800000 /**< Include descriptions in the reply */
#define request_received_over_tcp                0x01000000 /**< Original request received over TCP connection, setting this will imply that user info must be available */
#define request_wait                             0x02000000 /**< If object is not available, just wait until it becomes available and then handle the request */
#define request_common_path_alias                0x04000000 /**< Replace indexes with alias if available */
#define request_find_instance_wildcards_only     0x08000000 /**< Only used for search requests, the wildcard should only match instance objects */
#define request_reply_include_bus_namespace      0x10000000 /**< The reply should include the namespace of the bus */

request_list_t* request_list_create(void);
bool request_list_initialize(request_list_t* list);
void request_list_cleanup(request_list_t* list);
void request_list_destroy(request_list_t* list);
bool request_list_add(request_list_t* list, request_t* req);
request_t* request_list_first(request_list_t* list);
request_t* request_list_next(request_t* req);
request_t* request_list_prev(request_t* req);
request_t* request_list_last(request_list_t* list);

// general request functions
uint32_t request_id(request_t* req);
reply_t* request_reply(request_t* req);
bool request_replyReceived(request_t* req);

bool request_setSerializationData(request_t* req, void* data);
void* request_serializationData(request_t* req);

bool request_setData(request_t* req, void* data);
void* request_data(request_t* req);
bool request_setReplyHandler(request_t* req, request_replyHandler handler);
bool request_setReplyChildHandler(request_t* req, request_replyChildHandler handler);
bool request_setReplyItemHandler(request_t* req, request_replyItemHandler handler);
bool request_setDoneHandler(request_t* req, request_doneHandler handler);
bool request_setCancelHandler(request_t* req, request_cancelHandler handler);
bool request_setDestroyHandler(request_t* req, request_destroyHandler handler);
bool request_setCleanupSerializationHandler(request_t* req, request_cleanupSerializationData handler);

bool request_setBusy(request_t* req);
bool request_setDone(request_t* req);
bool request_forward(request_t* orig_req, peer_info_t* source, request_t* new_req, peer_info_t* dest);
bool request_addChild(request_t* req, request_t* child);
bool request_hasChildren(request_t* orig_req);
request_states_t request_state(request_t* req);

request_t* request_create(const request_type_t type);
request_t* request_create_getObject(const char* objectPath, uint32_t depth, uint32_t attributes);
request_t* request_create_setObject(const char* objectPath, uint32_t attributes);
request_t* request_create_addInstance(const char* objectPath, uint32_t index, const char* key, uint32_t attributes);
request_t* request_create_deleteInstance(const char* objectPath, uint32_t attributes);
request_t* request_create_findObjects(const char* objectPath, const char* pattern, uint32_t depth, uint32_t attributes);
request_t* request_create_executeFunction(const char* objectPath, const char* functionName, uint32_t attributes);
request_t* request_create_closeRequest(uint32_t requestId);
request_t* request_create_openSession(const char* userName);
request_t* request_copy(request_t* req, uint32_t newDepth, uint32_t attributes, uint32_t copyflags);
void request_destroy(request_t* req);

request_type_t request_type(request_t* req);
bool request_useObjectCache(request_t* req);

bool request_addParameter(request_t* req, const char* parameterName, const variant_t* value);
bool request_addArgument(request_t* req, argument_value_t* arg);

#include <pcb/core/request_parameter.h>

// set request properties
bool request_setPath(request_t* req, const char* path);
bool request_setFunctionName(request_t* req, const char* functionName);
bool request_setPattern(request_t* req, const char* pattern);
bool request_setAttributes(request_t* req, uint32_t request_attributes);
bool request_setDepth(request_t* req, const uint32_t depth);
bool request_setInstanceIndex(request_t* req, const uint32_t index);
bool request_setInstanceKey(request_t* req, const char* key);
bool request_setUsername(request_t* req, const char* username);
bool request_setUserID(request_t* req, uint32_t userId);
bool request_setCloseRequestId(request_t* req, const uint32_t id);

// get request properties
const char* request_path(request_t* req);
const char* request_functionName(request_t* req);
const char* request_pattern(request_t* req);
uint32_t request_attributes(request_t* req);
uint32_t request_depth(request_t* req);
uint32_t request_instanceIndex(request_t* req);
const char* request_instanceKey(request_t* req);
const char* request_username(request_t* req);
uint32_t request_userID(request_t* req);
uint32_t request_closeRequestId(request_t* req);

bool request_setPid(request_t* req, const uint32_t pid);
uint32_t request_getPid(request_t* req);
bool request_setBusNamespace(request_t* req, const char* ns);
char* request_getBusNamespace(request_t* req);

// accessors to parameter properties
uint32_t request_parameterCount(request_t* req);
llist_t* request_parameterList(request_t* req);
bool request_parameterListCloseFd(request_t* req);
bool request_parameterListClear(request_t* req);
parameter_iterator_t* request_findParameter(request_t* req, const char* name);
parameter_iterator_t* request_firstParameter(request_t* req);
parameter_iterator_t* request_nextParameter(const parameter_iterator_t* it);
parameter_iterator_t* request_prevParameter(const parameter_iterator_t* it);
parameter_iterator_t* request_lastParameter(request_t* req);
const char* request_parameterName(const parameter_iterator_t* it);
variant_t* request_parameterValue(const parameter_iterator_t* it);

#ifdef __cplusplus
}
#endif

#endif

