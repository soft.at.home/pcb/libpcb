/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(PCB_PARAMETER_H)
#define PCB_PARAMETER_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <pcb/utils/variant.h>

#include <pcb/core/types.h>

/**
   @ingroup pcb_core
   @file
   @brief
   Header file with parameter type definitions and public parameter functions
 */

/**
   @ingroup pcb_core
   @defgroup pcb_core_parameter Parameters
   @{

   @brief
   Parameter implementation

   @details

   @section pcb_core_parameter_life Parameter Life Cycle
   Each parameter has its own life cycle. A parameter comes to "life" whenever it is created and "dies" whenever it is deleted.\n
   A parameter can only be modified when it is in the created or ready state. A parameter that is modified while it is in the created state,
   will stay in this stat. The only other state that allows modifications to a parameter is the ready state.\n
   \n
   A parameter can only be deleted when it is in the ready state.\n
   \n
   A parameter can be reverted at all times by using the @ref parameter_rollback function.\n
   To go from one state to another it has to be either committed or rollbacked.\n
   \n
   @image html object_life_cycle.jpg "Parameter Life Cycle"

   @section pcb_core_parameter_types Parameter types an attributes
   A parameter always has a specific type e.g. int, string, bool, ...\n
   A parameter also has a set of attributes describing the properties of that parameter e.g. read-only, private,...\n
   A parameter is always a related to an existing object @ref pcb_core_object. \n

 */

/**
   @brief
   Enumeration of the parameter states
 */
typedef enum _parameter_state {
    parameter_state_invalid = 0,      /**< A parameter can not have this state */
    parameter_state_created,          /**< The parameter is created */
    parameter_state_ready,            /**< The parameter is ready */
    parameter_state_modified,         /**< The parameter has being modified */
    parameter_state_validated,        /**< The parameter is being validated */
    parameter_state_deleted,          /**< The parameter is marked for deletion */
    parameter_state_validate_created, /**< The parameter is being created and validated */
} parameter_state_t;

#define parameter_attr_default           0x00000000   /**< Apply default attributes */
#define parameter_attr_read_only         0x00000001   /**< The parameter is read-only for external processes */
#define parameter_attr_persistent        0x00000002   /**< If storage is available, this parameter must be stored */
#define parameter_attr_template_only     0x00000008   /**< The parameter is only available in a template object, not in instances. This attribute is ignored for single instance objects */
#define parameter_attr_volatile          0x00000010   /**< The parameter is volatile. There will be no events generated and the parameter will not be persistent */
#define parameter_attr_volatile_handler  0x00000020   /**< The parameter is volatile, because a read handler was set. There will be no events generated and the parameter will not be persistent */
#define parameter_attr_upc               0x00000040   /**< this parameter is marked as upgrade persistent */
#define parameter_attr_upc_usersetting   0x00000080   /**< this parameter is marked as user setting */
#define parameter_attr_upc_changed       0x00000100   /**< this parameter is marked as change upc */
#define parameter_attr_upc_overwrite     0x00000200   /**< this parameter is marked to overwrite an existing saved value */
#define parameter_attr_key               0x00000400   /**< this parameter is marked as a key parameter */

/**
   @brief
   Enumeration of the parameter types
 */
typedef enum _parameter_type {
    parameter_type_unknown = 0,   /**< A parameter can not have this type */
    parameter_type_string,        /**< The parameter is a string */
    parameter_type_int8,          /**< The parameter is a 8-bit signed integer */
    parameter_type_int16,         /**< The parameter is a 16-bit signed integer */
    parameter_type_int32,         /**< The parameter is a 32-bit signed integer */
    parameter_type_int64,         /**< The parameter is a 64-bit signed integer */
    parameter_type_uint8,         /**< The parameter is a 8-bit unsigned signed integer */
    parameter_type_uint16,        /**< The parameter is a 16-bit unsigned signed integer */
    parameter_type_uint32,        /**< The parameter is a 32-bit unsigned signed integer */
    parameter_type_uint64,        /**< The parameter is a 64-bit unsigned signed integer */
    parameter_type_bool,          /**< The parameter is a boolean */
    parameter_type_date_time,     /**< The parameter is a date time field */
    parameter_type_reference,     /**< The parameter is a reference to another object */
} parameter_type_t;

/**
   @brief
   Enumeration of the translate direction
 */
typedef enum _translate_direction {
    translate_value_to_plugin = 1, /**< translate the parameter value from a client to the plug-in */
    translate_value_to_client,     /**< translate the parameter value from the plug-in to a client */
} translate_direction_t;


/**
   @brief
   Parameter destroy handler

   @details
   A callback function can be added to the parameter which is called as the parameter will be destroyed.
   If you added some user data to the parameter, it is a good practive to also add a destroy handler.
 */
typedef void (* parameter_destroy_handler_t) (parameter_t* parameter);

/**
   @brief
   Parameter read handler

   @details
   A callback function can be added to the parameter which is called when an @ref parameter_update is called.
   A read handler is typically used to get the correct value from your "system". Parameters with a read handler
   set are also considered volatile, but can be persistent. That means that there will be no events generated
   for changes of its value.

   When implementing a read handler, the correct value must be put in the variant (second argument of the function)
 */
typedef bool (* parameter_read_handler_t) (parameter_t* parameter, variant_t* value);

/**
   @brief
   Parameter write handler

   @details
   A callback function can be added to the parameter which is called when the parameter value has been changed.
   The intention of this function is to let you known there is a change of the value.
 */
typedef void (* parameter_write_handler_t) (parameter_t* parameter, const variant_t* oldvalue);

typedef void (* parameter_prewrite_handler_t) (parameter_t* parameter, variant_t* newvalue);

/**
   @}
 */

// initializer functions
parameter_t* parameter_create(object_t* object, const char* name, parameter_type_t type, const uint32_t attributes);
parameter_t* parameter_copy(object_t* destObject, parameter_t* source);

// cleanup functions
bool parameter_delete(parameter_t* parameter);

// owner
object_t* parameter_owner(const parameter_t* parameter);

bool parameter_rename(parameter_t* parameter, const char* name);
bool parameter_cast(parameter_t* parameter, parameter_type_t type);

// setter and getter functions
const variant_t* parameter_getValue(const parameter_t* parameter);
const variant_t* parameter_getModifiedValue(const parameter_t* parameter);

bool parameter_setValue(parameter_t* parameter, const variant_t* variant);
bool parameter_setFromString(parameter_t* parameter, const string_t* string);
bool parameter_setFromChar(parameter_t* parameter, const char* text);

#ifdef PCB_HELP_SUPPORT
bool parameter_setDescription(parameter_t* parameter, const char* description);
const char* parameter_getDescription(parameter_t* parameter);
#endif

// operators
bool parameter_update(parameter_t* param);
bool parameter_commit(parameter_t* parameter);
void parameter_rollback(parameter_t* parameter);
bool parameter_validate(parameter_t* parameter);

// acl
bool parameter_hasAcl(parameter_t* parameter);
const llist_t* parameter_getACL(const parameter_t* parameter);
void parameter_getCalculatedACL(const parameter_t* parameter, llist_t* acl);
bool parameter_setACL(parameter_t* parameter, const llist_t* acl);
bool parameter_aclSet(parameter_t* parameter, uint32_t id, uint16_t flags);
bool parameter_aclAdd(parameter_t* parameter, uint32_t id, uint16_t flags);
bool parameter_aclDel(parameter_t* parameter, uint32_t id, uint16_t flags);
bool parameter_canRead(parameter_t* parameter, uint32_t uid);
bool parameter_canWrite(parameter_t* parameter, uint32_t uid);
bool parameter_canReadValue(parameter_t* parameter, uint32_t uid);

// upc
bool parameter_upcSet(parameter_t* parameter, uint32_t attributes);
bool parameter_upcClear(parameter_t* parameter, uint32_t attributes);

// attribute functions
void parameter_setReadOnly(parameter_t* parameter, const bool enable);
void parameter_setPersistent(parameter_t* parameter, const bool enable);
void parameter_setUPC(parameter_t* parameter, const bool enable);
void parameter_setUserSetting(parameter_t* parameter, const bool enable);
void parameter_setUPCOverwrite(parameter_t* parameter, const bool enable);
void parameter_setKey(parameter_t* parameter, const bool enable);

bool parameter_isReadOnly(const parameter_t* parameter);

uint32_t parameter_attributes(const parameter_t* parameter);

// instance counter functions
object_t* parameter_getCountedObject(parameter_t* parameter);
bool parameter_isInstanceCounter(parameter_t* parameter);

// others
bool parameter_setDestroyHandler(parameter_t* parameter, parameter_destroy_handler_t handler);
bool parameter_setReadHandler(parameter_t* parameter, parameter_read_handler_t handler);
bool parameter_setWriteHandler(parameter_t* parameter, parameter_write_handler_t handler);
bool parameter_setPrewriteHandler(parameter_t* parameter, parameter_prewrite_handler_t handler);

bool parameter_setValidator(parameter_t* parameter, parameter_validator_t* validator);
parameter_validator_t* parameter_getValidator(parameter_t* parameter);
const char* parameter_name(const parameter_t* parameter);

parameter_state_t parameter_state(const parameter_t* parameter);
parameter_type_t parameter_type(const parameter_t* parameter);
const char* parameter_typeName(parameter_t* parameter);

void parameter_setUserData(parameter_t* parameter, void* data);
void* parameter_getUserData(parameter_t* parameter);

#ifdef __cplusplus
}
#endif

#endif

