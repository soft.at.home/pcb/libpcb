/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(PCB_NOTIFICATION_H)
#define PCB_NOTIFICATION_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <pcb/utils/variant.h>
#include <pcb/core/types.h>

/**
   @ingroup pcb_core
   @file
   @brief
   Header file with notification type definitions and public notification functions
 */

/**
   @ingroup pcb_core
   @defgroup pcb_core_notification Notifications
   @{

   @brief
   Notification implementation

   @details
   Notifications can be used as a signaling mechanism. Before a server can send a notification, at least one connection
   to a bus or client must be established.\n

   @section pcb_core_notification_standard Standard Notifications
   Some standard notifications are defined, some of them are related to objects and data models, while others are mnore global.
    - Value changed: Send when a parameter has changed value
    - Object added: Send when an instance is added to a template object
    - Object deleted: Send when an instance is deleted.
    - Application started: Send by a server when it is ready to recieve requests.

   @section pcb_core_notification_custom Custom Notifications
   Custom notifications can be created and send to any client/server.\n

   @section pcb_core_notification_parameter Notification Parameters
   Each notification can contain zero, one or more parameters. These parameters exist out of two parts, the name and a value.
 */

/**
   @brief
   Definition of a helper macro for iteration through the parameters of a notification

   @details
   This <b>helper macro</b> iterates forwards through the parameters of a notification\n

   @param parameter a pointer to a parameter that will contain the current parameter
   @param notification the parent notification
 */
#define notification_for_each_parameter(parameter, notification) \
    for(parameter = notification_firstParameter(notification); parameter; parameter = notification_nextParameter(parameter))

#if __STDC_VERSION__ >= 199901L
/**
   @brief
   Definition of a helper macro for iteration through the parameters of a notification

   @details
   This <b>helper macro</b> iterates forwards through the parameters of a notification\n

   @param parameter the name a pointer to a parameter to be declared that will contain the current parameter
   @param notification the parent notification
 */
#define notification_for_each_declare_parameter(parameter, notification) \
    for(notification_parameter_t* parameter = notification_firstParameter(notification); parameter; parameter = notification_nextParameter(parameter))
#endif

/**
   @brief
   Enumeration of the notification types
 */
typedef enum _notification_types {
    // data model notifications
    notify_value_changed = 1,          /**< Value changed notification identifier */
    notify_object_added,               /**< Object added notification identifier */
    notify_object_deleted,             /**< Object deleted notification identifier */
    // application notifications
    notify_application_started = 10,   /**< Application started notification identifier */
    notify_application_stopping,       /**< Application stopping notification identifier */
    notify_application_change_setting, /**< Application needs to change a setting identifier */
    notify_application_root_added,
    notify_application_root_deleted,
    // bus notifications
    notify_bus_interconnect = 20,     /**< Bus interconnect notification */
    // custom notifications
    notify_custom = 100,              /**< Custom notification base identifier */
} notification_type_t;

/**
   @}
 */

notification_t* notification_create(uint32_t type);

notification_t* notification_createValueChanged(object_t* object, parameter_t* parameter);
notification_t* notification_createObjectAdded(object_t* object);
notification_t* notification_createObjectDeleted(object_t* object);

notification_t* notification_createApplicationStarted(const char* appName);
notification_t* notification_createApplicationStopping(const char* appName);
notification_t* notification_createApplicationChangeSetting(const char* settingName, const char* settingValue);
notification_t* notification_createApplicationRootAdded(const char* object_name);
notification_t* notification_createApplicationRootDeleted(const char* object_name);
notification_t* notification_createBusInterconnect(const char* appName, bool sync);

void notification_destroy(notification_t* notification);

uint32_t notification_type(const notification_t* notification);

bool notification_setName(notification_t* notification, const char* name);
const char* notification_name(notification_t* notification);

bool notification_setObjectPath(notification_t* notification, const char* objectpath);
const char* notification_objectPath(notification_t* notification);
const char* notification_objectName(notification_t* notification);

bool notification_addParameter(notification_t* notification, notification_parameter_t* param);

uint32_t notification_parameterCount(notification_t* notification);
notification_parameter_t* notification_firstParameter(const notification_t* notification);
notification_parameter_t* notification_nextParameter(notification_parameter_t* param);
notification_parameter_t* notification_prevParameter(notification_parameter_t* param);
notification_parameter_t* notification_lastParameter(notification_t* notification);

notification_parameter_t* notification_getParameter(const notification_t* notification, const char* name);

notification_parameter_t* notification_parameter_create(const char* name, const char* value);
notification_parameter_t* notification_parameter_createVariant(const char* name, const variant_t* value);
void notification_parameter_destroy(notification_parameter_t* param);

bool notification_parameter_setName(notification_parameter_t* param, const char* name);
bool notification_parameter_setValue(notification_parameter_t* param, const char* value);
bool notification_parameter_setVariant(notification_parameter_t* param, const variant_t* value);

const char* notification_parameter_name(notification_parameter_t* param);
char* notification_parameter_value(notification_parameter_t* param);
variant_t* notification_parameter_variant(notification_parameter_t* param);

/**
   @ingroup pcb_core_notification
   @brief
   Get a notification parameter value

   @details
   Get the notification parameter value.

   @param n the notification of interest
   @param name the name of the parameter to fetch

   @return
    - The notification parameter value
 */
static inline variant_t* notification_da_parameterValue(const notification_t* n, const char* name) {
    return notification_parameter_variant(notification_getParameter(n, name));
}

#include <pcb/core/notification_parameter.h>

#ifdef __cplusplus
}
#endif

#endif

