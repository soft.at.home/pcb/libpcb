/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(PCB_CONNECTION_H)
#define PCB_CONNECTION_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <netdb.h>
#include <stdbool.h>

#include <pcb/common/error.h>
#include <pcb/utils/uri.h>
#include <pcb/utils/linked_list.h>
#include <pcb/utils/circular_buffer.h>

#ifdef OPEN_SSL_SUPPORT
#include <openssl/ssl.h>
#include <openssl/x509v3.h>
#endif

/**
   @ingroup pcb_utils_socket_events
   @file
   @brief
   Header file with connection type definitions and public connection functions
 */

/**
   @ingroup pcb_utils_socket_events
   @defgroup pcb_socket_layer_connections Connections
   @{

   @brief
   Socket, file descriptor and event management functionallity

   @details
   A connection main purpose is to manage and monitor open sockets, file descriptors and other
   event sources like signals and timers.
   There is a possibility to add custom file descriptors (files, fifo, ...),
   these will only be monitored and are not managed.

   @section pcb_sl_init Library Initializing and Cleanup
   Before the pcb event mechanism can be used to handle signals and timers, the function
   @ref connection_initLibrary must be called. If you need to use openssl functionality,
   the ssl part of the library must be activated before calling @ref connection_initLibrary.
   This can be achieved by calling @ref connection_enableSSL.

   @section pcb_sl_connection_init Connection Info Creating
   To allocate memory on the heap and initialize a connection info
   structure (connection_info_t), use @ref connection_create. To initialize
   an already allocated structure (could be on the stack) use @ref connection_initialize.
   A connection_info_t structure acts as an event sink and handles all events of
   event sources added to the connection_info_t.
   The only exception to this rule are the timers. Timers are global and will be handled by
   any connection_info_t.

   @section pcb_sl_connection_clean Connection Info Cleanup
   Use @ref connection_cleanup to clean up the content of a connection_info_t struct.
   This function does not free memory allocated for the structure itself, but will free
   memory that is used by the members of the structure.
   The stucture can be reused but must be reinitialized.

   If the connectioninfo_t structure was allocated on the heap, it can be freed
   using @ref connection_destroy. After calling this function the pointer is not valid anymore.
   Do not use this function on connection_info_t stuctures allocated on the stack.

   @section pcb_sl_connection_listen Creating a server
   A server must start listening for incoming connections and can accept or refuse them.
   At least one of the @ref connection_listenOnIPC or @ref connection_listenOnTCP must be
   called once to prepair the connection_info_t structure to start listening on a socket for
   incoming connections. These functions can be called multiple times, if you want the
   server to listen on more then one socket.

   An event handler can be set, using @ref connection_setEventHandler,on the connection_info_t
   structure that will be called when a new connection has been accepted. If this event handler
   returns false the socket will be closed.

   New incoming sockets can only be accepted from within the event loop.

   @section pcb_sl_connection_connect Connect to a server
   Connecting to a server can be done using @ref connection_connectToIPC or @ref connection_connectToTCP.
   Both functions can be called multiple times to connect to different servers or even the same server.
   Both functions will return a peer_info_t structure, that is wrapping the socket.including
   some extra information.

   Another connection function is available that accepts an URI and extract all needed information from
   the URI.

   @section pcb_sl_connection_ssl Secure Socket Layer
   On TCP sockets SSL can be activated. Before creating a listen socket or connect to a server,
   the function @ref connection_setupSSL must be called.
   After that a SSL socket can be created at any time, you need to pass the @ref connection_attr_ssl argument
   to the function to create the SSL socket.

   There are two flavors of the pcb socket layer library, one without ssl support and one with ssl support.
   Make sure that you are using the correct one to link against.
   Also add at compile time the -DOPEN_SSL_SUPPORT flag to your compiler instruction.
 */

typedef struct _connection_info connection_info_t;
typedef struct _peer_info peer_info_t;
typedef struct _process_info process_info_t;

/**
   @brief
   Socket or FD event handler function

   @details
   A file descriptor or socket event handler function can be set on every peer_info_t structure or on
   a connection_info_t structure to accept incoming connections.
   You can add event handlers to the connection_info_t structure by using one of the following functions:
    - connection_setEventHandler
    - connection_addConnectedHandler
 */
typedef bool (* peer_event_handler_t) (peer_info_t* peer);

/**
   @brief
   Peer types

   @details
   A peer is always one of these types.
 */
typedef enum _peer_type {
    peer_type_server_connection = 1,        /**< connection to a server */
    peer_type_client_connection,            /**< acccepted connection from a client */
    peer_type_custom_fd,                    /**< custom added file descriptor */
    peer_type_listen,                       /**< listen socket */
    peer_type_pair,                         /**< member of a peer pair */
    peer_type_self,                         /**< the application self */
} peer_type_t;

/**
   @brief
   Peer states

   @details

 */
typedef enum _peer_state {
    peer_state_closed = 0,                  /**< peer is closed */
    peer_state_listening,                   /**< peer is listening */
    peer_state_connecting,                  /**< peer is buzy connecting */
    peer_state_connected,                   /**< peer is connected */
    peer_state_closing,                     /**< peer is buzy closing */
    peer_state_deleted,                     /**< peer is marked for deletion */
#ifdef OPEN_SSL_SUPPORT
    peer_state_retry_connect_need_read,     /**< Connect was buzy, but need to wait until data is available for read */
    peer_state_retry_connect_need_write,    /**< Connect was buzy, but need to wait until the socket is available for write */
    peer_state_retry_accept_need_read,      /**< Accept was buzy, but need to wait until data is available for read */
    peer_state_retry_accept_need_write,     /**< Accept was buzy, but need to wait until the socket is available for write */
    peer_state_retry_read_need_read,        /**< Read was buzy, but need to wait until more data is available for read */
    peer_state_retry_read_need_write,       /**< Read was buzy, but need to wait until the socket is available for write */
    peer_state_retry_write_need_read,       /**< Write was buzy, but need to wait until data is available for read */
    peer_state_retry_write_need_write,      /**< Write was buzy, but more writes would block */
#endif
} peer_state_t;

/**
   @brief
   Stream type

   @details
   A peer stream can be buffered or direct..
 */
typedef enum _peer_stream_type {
    peer_stream_buffered_io = 0, /**< All write actions will be buffered */
    peer_stream_direct_io,       /**< All write actions will be performed direct on the fd */
    peer_stream_auto_io,         /**< Write actions will be performed direct on the fd, if the action return EGAIN, it is buffered until the buffer is empty again */
} peer_stream_type_t;

/**
   @brief
   Stream type

   @details
   A peer stream can be buffered or direct..
 */
typedef enum _peer_event_type {
    peer_event_accepted = 1,
    peer_event_connected,
    peer_event_read,
    peer_event_write,
    peer_event_write_done,
    peer_event_close,
    peer_event_destroy,
    peer_event_reconnect,
} peer_event_type_t;

/**
   @brief
   Structure to wrap a socket and socket information
 */
struct _peer_info {
    int socketfd;                           /**< The (socket) file descriptor */
    struct addrinfo addrInfo;               /**< The peer's addres info */
    circbuf_t* sendBuffer;                  /**< Buffer containing data to send */
    peer_info_t* listensocket;              /**< Pointer to the listen socket, only valid if the peer was accepted */
    FILE* out;                              /**< The out stream */
    FILE* in;                               /**< The in stream */
    peer_type_t type;                       /**< type of the peer info, see @ref peer_type_t */
    peer_state_t state;                     /**< state of the peer */
    peer_stream_type_t stream_type;         /**< type of the stream */
    void* userData;                         /**< User data */
    uint32_t handler_flags;                 /**< Flags used to remove the handler after a certain event was handled */
    peer_event_handler_t read;              /**< The peers read handler */
    peer_event_handler_t write;             /**< The peers write handler */
    peer_event_handler_t writeDone;         /**< The peers write done handler, called when using buffered io */
    peer_event_handler_t destroy;           /**< The peers destroy handler, called when the peer is deleted */
    peer_event_handler_t connect;           /**< The peers connect handler, called when the peer is connected, only usefull for async connection setup */
    llist_t closeHandlers;                  /**< List of close handlers, called when the peer is closed */
    llist_t acceptHandlers;                 /**< List of accept handlers, called when a new connection is made */
    llist_t send_fd_list;                   /**< List of file descriptors to send */
    llist_t recv_fd_list;                   /**< List of file descriptors received */
    bool ssl_auth_checked;                  /**< True if authentication has been checked already */
    bool ssl_auth;                          /**< True if authenticated by client certificate */
    char ssl_cert_cn[256];                  /**< Username, in case ssl_auth is set
                                             *  (cannot be in the ifdef below, because other libs don't have this flag set */
#ifdef OPEN_SSL_SUPPORT
    BIO* ssl_bio;                           /**< The Open SSL BIO */
    SSL* ssl;                               /**< SSL */
    char* node;                             /**< Node */
    char buffer[128];                       /**< Temp buffer to be able to peek the incomming data */
    uint32_t data_size;                     /**< Size of the data in the temp buffer */
    uint32_t data_offset;                   /**< Offset of the temp buffer */
    size_t writesize;                       /**< Last write size, when ssl returns want write, the next write buffer size must be exact the same */
#endif
};

struct _process_info;

/**
   @brief
   Client connection structure.

   @details
   This structure is here to be able to add a peer_info_t structure into a linked list.
 */
typedef struct _connection {
    llist_iterator_t it;            /**< linked list iterator, which is used to add an instance of this structure into a linked list */
    peer_info_t info;               /**< the peer info, containing all connection information of the connected client/server */
} connection_t;

/**
   @brief
   Definition of a signal handler function

   @details
   Signal handler functions must match this signature. A signal handler can be added with the function @ref connection_setSignalEventHandler
 */
typedef void (* signal_event_handler_t) (int signal);

/**
   @brief
   Common event handler

   @details
   A common event handler can be added to the connection_info_t structure. This function is called each timer after
   one or more events has been processed. This handler can be used to do some small common tasks in the event loop.
   Only one handler can be added to each connection_info_t structure using the function @ref connection_setEventsProcessedHandler.
 */
typedef void (* events_processed_handler_t) (connection_info_t* con);

/**
   @brief
   Signal handler info

   @details
   This structure is here to be able to add a signal handler into a linked list
 */
typedef struct _signal_handler {
    llist_iterator_t it;             /**< linked list iterator, which is used to add an instance of this structure into a linked list */
    int signal;                      /**< the signal */
    signal_event_handler_t handler;  /**< the signal handler */
} signal_handler_t;

/**
   @brief
   TCP socket attributes

   @details
   Use one of these attributes to specify what kind of TCP socket you want to create.
 */
typedef enum _connection_attribute {
    connection_attr_default          = 0, /**< Create a default TCP socket */
    connection_attr_ssl,                  /**< Create a SSL enable TCP socket, SSL must be enabled and properly setup*/
    connection_attr_async_connect,        /**< Do not block while setting up the TCP connection. */
} connection_attribute_t;

#ifdef OPEN_SSL_SUPPORT
#define ssl_attribute_default                           0x00000000 /**< Default certificate verification behavior */
#define ssl_attribute_need_client_cert                  0x00000001 /**< Only accept incoming connections when certificate is provided */
#define ssl_attribute_accept_self_signed                0x00000002 /**< Accept self signed certificates */
#define ssl_attribute_check_host_name                   0x00000004 /**< Check that the domain name in the certificates matches the domain name of the server */
#define ssl_attribute_accept_expired                    0x00000008 /**< Accept not yet valid or expired certificates */
#define ssl_attribute_peer_not_verified                 0x00000010 /**< Do not verify peer certificates */
#define ssl_attribute_no_env_cipher_list                0x00000020 /**< Ignore any cipher list set by the environment variable DEFAULT_CIPHER_SUITE */
#define ssl_attribute_peer_verified_once                0x00000040 /**< Verify peer only on the initial TLS/SSL handshake */
#define ssl_attribute_no_resumption_on_renegotiation    0x00000080 /**< On renegotiation, server always restart a new session except fro initial handshakes */

/**
   @brief
   Peer certificate verification function

   @details
   This callback function can be set to perform application specific checks upon the peer's certificate.
   It can be used in both client as server mode, and the application is supposed to know in which mode it operates.
   The callback function is set using @ref connection_setPeerVerify.

   The usage, arguments and return value are exactly like the SSL callback function set using SSL_set_verify(),
   except for the extra 'peer' argument.

   Only one peer verification function can be set.

   Important note: This callback is called from within the SSL certificate verification process.
   It is necessary that it returns as soon as possible, because the SSL connection setup
   remains pending whilst waiting.
 */
typedef int (* verify_peer_certificate_t) (int preverify_ok, X509_STORE_CTX* x509_ctx, peer_info_t* peer);

/**
   @brief
   Server certificate verification function

   @deprecated
   Use @ref verify_peer_certificate_t instead.

   @details
   By default the pcb socket layer library provides a very common server certificate verification routine.
   The behavior of this default implementation can be addapted using one of the <b>ssl_attribute_xxxx</b> flags.
   It is also possible to implement a custom verification function and add it to the connection_info_t structure
   using @ref connection_setServerVerify.

   Only one server verification function can be set. The next call to @ref connection_setServerVerify will overwrite the
   previous one.
 */
typedef long (* verify_server_certificate_t) (X509* cert, const char* host, uint32_t attributes);

/**
   @brief
   Client certificate verification function

   @deprecated
   Use @ref verify_peer_certificate_t instead.

   @details
   The pcb socket layer library does not provide a default implementation for verifying a client certificate.
   A custom implemented verification function must be provided. This function can be added to the connection_info_t structure
   using the function @ref connection_setClientVerify.

   Only one client verification function can be set. The next call to @ref connection_setClientVerify will overwrite the
   previous one.
 */
typedef long (* verify_client_certificate_t) (X509* cert);

/**
   @brief
   Function to retrieve the username from the certifcate after an abbreviated ssl handshake
 */
typedef int (* reuse_username_t) (peer_info_t* peer);

/**
   @brief
   SSL connection pre-connect hook

   @details
   A hook can be set on every connection_info_t structure.
   This allows to be called before SSL_connect is called allowing to interact with the SSL object.
 */
typedef void (* connection_ssl_session_hook_t) (connection_info_t* con, peer_info_t* peer, const char* host);
#endif

/**
   @brief
   The connection structure

   @details
   This structure holds all information about a connection.
 */
struct _connection_info {
    char* name;                                        /**< Connection name, only used for debugging and as SSL session id ctx*/
    llist_iterator_t it;                               /**< Iterator to put the connection in a list */
    // list of listen sockets
    llist_t listenSockets;                             /**< The list of listen sockets */
    // list of connections
    llist_t connections;                               /**< List of all connections */
    // event handlers
    peer_event_handler_t accept;                       /**< The accept event handler */
    llist_t connectHandlers;                           /**< List of connect handlers */
    events_processed_handler_t eventsProcessedHandler; /**< Called when events where processed */
    // signals
    llist_t signalHandlers;                            /**< List of signal handlers */
    // Running processes
    llist_t activeProcesses;                           /**< List of running processes */
    // user data
    void* userData;                                    /**< user data */
    // DNS res address family
    int ai_family;
#ifdef OPEN_SSL_SUPPORT
    SSL_CTX* ssl_ctx;                                   /**< ssl context */
    char* sni;                                          /**< ssl server name indication */
    uint32_t attrib;                                    /**< ssl attributes */
    verify_server_certificate_t verify_server;          /**< callback function to verify the server certificate */
    verify_client_certificate_t verify_client;          /**< callback function to verify the client certificate */
    verify_peer_certificate_t verify_peer;              /**< callback function to verify the peer certificate */
    reuse_username_t reuse_username;                    /**< callback function to reuse certificate username */
    connection_ssl_session_hook_t ssl_session_pre_hook; /**< hook to manipulate SSL session before connect */
#endif
};

/**
   @brief
   List of connection_info_t structures

   @details
   A type definition to indicate that the list must contain connection_info_t structures..
 */
typedef llist_t connection_list_t;

/**
   @brief
   The listen socket callback

   @details
   A callback to set socket options on a socket between the socket() and bind() calls or between the socket() and connect() calls.
 */
typedef bool (* connection_socket_handler_t)(int socket);

/**
   @}
 */

// initialization
bool connection_initLibrary(void);
void connection_exitLibrary(void);
void connection_enableSSL(bool enabled);

void connection_blockSignals(bool block);

connection_info_t* connection_create(const char* name);
bool connection_initialize(connection_info_t* con, const char* name);
#ifdef OPEN_SSL_SUPPORT
bool connection_setupSSL(connection_info_t* con, const char* caFile, const char* certFile, uint32_t attr);
SSL_CTX* connection_createSSL(connection_info_t* con, uint32_t attr);
SSL_CTX* connection_getSSL(connection_info_t* con);
bool connection_setServerVerify(connection_info_t* con, verify_server_certificate_t fn);
bool connection_setClientVerify(connection_info_t* con, verify_client_certificate_t fn);
bool connection_setPeerVerify(connection_info_t* con, verify_peer_certificate_t fn);
bool connection_setReuseUsername(connection_info_t* con, reuse_username_t fn);
void connection_addSSLAttribute(connection_info_t* con, uint32_t attr);
void connection_delSSLAttribute(connection_info_t* con, uint32_t attr);
bool connection_setCipherList(connection_info_t* con, const char* cipher_list);
bool connection_setPreSSLConnectionHook(connection_info_t* con, connection_ssl_session_hook_t fn);
long connection_checkCertificateHostname(X509* cert, const char* hostname);
bool connection_setServerNameIndication(connection_info_t* con, const char* server_name_indication);
#endif

// clean up
void connection_cleanup(connection_info_t* con);
void connection_destroy(connection_info_t* con);

// attributes
const char* connection_name(connection_info_t* con);
bool connection_setAddressFamily(connection_info_t* con, int ai_family);

// open & close
peer_info_t* connection_listenOnIPC(connection_info_t* con, const char* path);
peer_info_t* connection_listenOnTCP(connection_info_t* con, const char* host, const char* service, uint32_t attr);
peer_info_t* connection_listenOnTCPCallback(connection_info_t* con, const char* host, const char* service, uint32_t attr, connection_socket_handler_t handler);
peer_info_t* connection_connectToIPC(connection_info_t* con, const char* path);
peer_info_t* connection_connectToTCPCallback(connection_info_t* con, const char* host, const char* service, uint32_t attr, connection_socket_handler_t handler);
peer_info_t* connection_connectToTCP(connection_info_t* con, const char* host, const char* service, uint32_t attr);
peer_info_t* connection_connect(connection_info_t* con, const char* URI, uri_t** uri);
bool connection_connectPair(connection_info_t* con, peer_info_t* peers[2]);

bool connection_closeAll(connection_info_t* con);
bool connection_closeListenSockets(connection_info_t* con);
bool connection_closeServers(connection_info_t* con);
bool connection_closeClients(connection_info_t* con);

// events
int connection_waitForEvents(connection_list_t* cons, struct timeval* timeout);
int connection_waitForEvents_r(connection_list_t* cons, struct timeval* timeout, fd_set* readset, fd_set* writeset);
int connection_select(int maxfd, fd_set* readset, fd_set* writeset, struct timeval* timeout);
int connection_select_r(int maxfd, fd_set* readset, fd_set* writeset, struct timeval* timeout, bool unblockSignals);
bool connection_handleEvents(connection_list_t* cons);
bool connection_handleEvents_r(connection_list_t* cons, fd_set* readset, fd_set* writeset);
bool connection_setEventsAvailable(void);
int connection_eventFd(void);
void connection_clearEventFd(void);
bool connection_setTerminated(bool terminate);
bool connection_isTerminated(void);

// fd
peer_info_t* connection_addCustom(connection_info_t* con, int fd);
peer_info_t* connection_find(connection_info_t* con, int fd);
peer_info_t* connection_findIPC(connection_info_t* con, const char* path);
peer_info_t* connection_findTCP(connection_info_t* con, const char* host, const char* service);

// processes
bool connection_addRunningProcess(connection_info_t* con, process_info_t* process);
bool connection_removeRunningProcess(connection_info_t* con, process_info_t* process);

// build read - write set
void connection_build_readset(llist_t* connections, fd_set* readset, int* maxfd);
void connection_build_writeset(llist_t* connections, fd_set* writeset, int* maxfd);

// callback functions
bool connection_setEventHandler(connection_info_t* con, peer_event_type_t event, peer_event_handler_t handlerfn);
bool connection_setSignalEventHandler(connection_info_t* con, int signal, signal_event_handler_t handlerfn);
bool connection_addConnectedHandler(connection_info_t* con, peer_event_handler_t handlerfn);
bool connection_setEventsProcessedHandler(connection_info_t* con, events_processed_handler_t handlerfn);

// user data
/**
   @ingroup pcb_socket_layer_connections
   @brief
   Set user data in the connection_info_t structure
   @details
   Set some user data to the connection_info_t structure. The caller of this function must free the user data if needed.
   If memory was allocated for the user data, the library will not free it.
   If user data was already set, this function will overwrite it.
   @param con Pointer to the connection_info_t structure
   @param data User specified data
   @return
    - true when successful
    - false an error occured
 */
static inline bool connection_setUserData(connection_info_t* con, void* data) {
    if(!con) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    con->userData = data;
    return true;
}

/**
   @ingroup pcb_socket_layer_connections
   @brief
   Get user data from the connection_info_t structure
   @details
   This function will return whatever was set using the @ref connection_setUserData function
   @param con Pointer to the connection_info_t structure
   @return
    - The user data.
 */
static inline void* connection_getUserData(connection_info_t* con) {
    if(!con) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    return con->userData;
}

// iterator
/**
   @ingroup pcb_socket_layer_connections
   @brief
   Get the iterator of the connection_info_t structure
   @details
   Get the iterator from the connection_info_t structure. With this iterator you can add the structure to a list.
   @param con Pointer to the connection_info_t structure
   @return
    - The linked list iterator
    - NULL an error has occured.
 */
static inline llist_iterator_t* connection_getIterator(connection_info_t* con) {
    if(!con) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    return &(con->it);
}

#ifdef __cplusplus
}
#endif

#endif
