/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef PCB_VARIANT_H
#define PCB_VARIANT_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdio.h>
#include <time.h>
#include <pcb/utils/linked_list.h>
#include <pcb/utils/string.h>
#include <pcb/utils/string_list.h>
#include <pcb/utils/datetime.h>

/**
   @ingroup pcb_utils_containers
   @file
   @brief
   Header file with variant type definitions and variant functions
 */

/**
   @ingroup pcb_utils_containers
   @defgroup pcb_utils_variant Variant
   @{

   @brief
   The variant type allows users to use one 'generic' variable for different variable types (int, string, bool, double, ...).

   @details
   Often user data can have diffent data types. Instead of providing conversions for each case where
   different data types can be used, one can use the variant type which handles these conversions automatically.

   The current implementation has support for the folowing data types:
   @li string
   @li int8
   @li int16
   @li int32
   @li int64
   @li uint8
   @li uint16
   @li uint32
   @li uint64
   @li bool
   @li double
   @li datetime
   @li array: list of variants
   @li map: key - value pair list of variants
   @li byte array: binary data
   @li file descriptor
   @li variant reference: reference to another variant

   The variant memory allocation is dynamic, depending on the datatype.
 */


/**
   @brief
   Enumeration of the variant types

   @details
   This list defines the supported variant types.
 */
typedef enum _variant_type {
    variant_type_unknown,         /**< unknown  */
    variant_type_string,          /**< string   */
    variant_type_int8,            /**< int8     */
    variant_type_int16,           /**< int16    */
    variant_type_int32,           /**< int32    */
    variant_type_int64,           /**< int64    */
    variant_type_uint8,           /**< uint8    */
    variant_type_uint16,          /**< uint16   */
    variant_type_uint32,          /**< uint32   */
    variant_type_uint64,          /**< uint64   */
    variant_type_bool,            /**< bool     */
    variant_type_double,          /**< double   */
    variant_type_date_time,       /**< datetime */
    variant_type_array,           /**< dynamic array of variant values (uses variant list) */
    variant_type_map,             /**< dynamic array of variant values (uses variant map) */
    variant_type_reference,       /**< variant keeping a reference to another variant */
    variant_type_array_reference, /**< variant keeping a reference to a variant list */
    variant_type_map_reference,   /**< variant keeping a reference to a variant map */
    variant_type_file_descriptor, /**< file descriptor */
    variant_type_byte_array,      /**< Byte array, containing binary data */
} variant_type_t;

/**
   @brief
   List of variant_t structures

   @details
   A type definition to indicate that the list must contain variant_t structures..
 */
typedef llist_t variant_list_t;

/**
   @brief
   List of key value pairs, where the value is represent by a variant_t

   @details
   A type definition to indicate that the list must contain key value pairs, where the value is represented by a variant_t and the key is a simple string.
 */
typedef llist_t variant_map_t;

/**
   @brief
   Can contain a blob of binary data

   @details
   A type definition that contains a pointer to data and the size of the memory block.
 */
typedef struct _byte_array {
    void* data;         /**< Pointer to a memory block data containing binary data */
    uint32_t size;      /**< Size of the memory block */
} byte_array_t;

/**
   @brief
   variant definition

   @details
   This structure defines a variant.
 */
typedef struct _variant {
    variant_type_t type;    /**< the variant type */
    union {
        string_t str;       /**< the string data pointer */
        int8_t i8;          /**< the int8 value          */
        int16_t i16;        /**< the int16 value         */
        int32_t i32;        /**< the int32 value         */
        int64_t i64;        /**< The int64 value         */
        uint8_t ui8;        /**< the uint8 value         */
        uint16_t ui16;      /**< the uint16 value        */
        uint32_t ui32;      /**< the uint32 value        */
        uint64_t ui64;      /**< the uint32 value        */
        bool b;             /**< the boolean value       */
        double d;           /**< the double value        */
        pcb_datetime_t* dt; /**< the datetime data pointer */
        struct tm* dt_old;  /**< the old datetime data pointer, compatible with the new because the first field of pcb_datetime_t is a struct tm*/
        variant_list_t* vl; /**< pointer used for variant arrays */
        variant_map_t* vm;  /**< pointer used for variant maps */
        struct _variant* v; /**< pointer used to store a reference to another variant */
        int fd;             /**< The file descriptor */
        byte_array_t* ba;   /**< Pointer to binary date */
    } data;                 /**< the union containing the variant data */
} variant_t;

#define variant_print_multiline        0x01                                                                    /**< Separate list and map items by newlines and
                                                                                                                    start each line with proper indentation.*/
#define variant_print_spaces           0x02                                                                    /**< Insert spaces before/after delimiters.*/
#define variant_print_outline_colons   0x04                                                                    /**< Put all colons (or key/value delimiter of map
                                                                                                                    items) nicely under each other. This attribute
                                                                                                                    only makes sense if
                                                                                                                    @ref variant_print_multiline is specified
                                                                                                                    too.*/
#define variant_print_quote_keys       0x08                                                                    /**< Surround map keys with quotes and escape
                                                                                                                    special characters.*/
#define variant_print_quote_strings    0x10                                                                    /**< Surround string values with quotes and escape
                                                                                                                    special characters.*/
#define variant_print_bool_to_num      0x20                                                                    /**< Print booleans as 1 or 0 instead of true or
                                                                                                                    false.*/
#define variant_print_need_newline     0x40                                                                    /**< Insert a newline before actually printing the
                                                                                                                    variant if it is a map or a list.*/
#define variant_print_json (variant_print_quote_keys | variant_print_quote_strings)                            /**<
                                                                                                                  Group of attributes that make the resulting
                                                                                                                  string JSON-compatible.*/
#define variant_print_beautify (variant_print_multiline | variant_print_spaces | variant_print_outline_colons) /**<
                                                                                                                  Group of attributes that improve the
                                                                                                                  readability and esthetics and the resulting
                                                                                                                  string. */

/**
   @brief
   Enumeration type definition for all possible delimitors in string representing a variant.

   @details
   Enumeration type definition for all possible delimitors in string representing a variant.
 */
typedef enum variant_print_delimiter {
    variant_print_delimiter_map_open,     /**< Default is "{".*/
    variant_print_delimiter_map_assign,   /**< Default is ":".*/
    variant_print_delimiter_map_iterate,  /**< Default is ",".*/
    variant_print_delimiter_map_close,    /**< Default is "}".*/
    variant_print_delimiter_list_open,    /**< Default is "[".*/
    variant_print_delimiter_list_iterate, /**< Default is ",".*/
    variant_print_delimiter_list_close,   /**< Default is "]".*/
    variant_print_delimiter_count,        /**< Max delimiter count */
} variant_print_delimiter_t;

/**
   @}
 */

bool variant_initialize(variant_t* variant, variant_type_t type);
void variant_copy(variant_t* dest, const variant_t* src);
void variant_move(variant_t* dest, variant_t* src);
bool variant_convert(variant_t* dst, const variant_t* src, variant_type_t type);
void variant_cleanup(variant_t* variant);

bool variant_setChar(variant_t* variant, const char* data);
bool variant_setString(variant_t* variant, const string_t* data);
bool variant_setInt8(variant_t* variant, int8_t data);
bool variant_setInt16(variant_t* variant, int16_t data);
bool variant_setInt32(variant_t* variant, int32_t data);
bool variant_setInt64(variant_t* variant, int64_t data);
bool variant_setUInt8(variant_t* variant, uint8_t data);
bool variant_setUInt16(variant_t* variant, uint16_t data);
bool variant_setUInt32(variant_t* variant, uint32_t data);
bool variant_setUInt64(variant_t* variant, uint64_t data);
bool variant_setBool(variant_t* variant, bool data);
bool variant_setDouble(variant_t* variant, double data);
bool variant_setDateTime(variant_t* variant, const struct tm* data);
bool variant_setDateTimeExtended(variant_t* variant, const pcb_datetime_t* data);
bool variant_setListMove(variant_t* variant, variant_list_t* data);
bool variant_setListCopy(variant_t* variant, const variant_list_t* data);
bool variant_setListRef(variant_t* variant, variant_list_t* data);
bool variant_setMapMove(variant_t* variant, variant_map_t* data);
bool variant_setMapCopy(variant_t* variant, const variant_map_t* data);
bool variant_setMapRef(variant_t* variant, variant_map_t* data);
bool variant_setReference(variant_t* variant, variant_t* ref);
bool variant_setFd(variant_t* variant, int fd);
bool variant_setByteArray(variant_t* variant, const void* data, uint32_t size);
bool variant_setStringList(variant_t* variant, const string_list_t* list, const char* separator);

bool variant_toString(string_t* data, const variant_t* variant);
bool variant_toJSON(string_t* data, const variant_t* variant);
bool variant_toChar(char** data, const variant_t* variant);
bool variant_toInt8(int8_t* data, const variant_t* variant);
bool variant_toInt16(int16_t* data, const variant_t* variant);
bool variant_toInt32(int32_t* data, const variant_t* variant);
bool variant_toInt64(int64_t* data, const variant_t* variant);
bool variant_toUInt8(uint8_t* data, const variant_t* variant);
bool variant_toUInt16(uint16_t* data, const variant_t* variant);
bool variant_toUInt32(uint32_t* data, const variant_t* variant);
bool variant_toUInt64(uint64_t* data, const variant_t* variant);
bool variant_toBool(bool* data, const variant_t* variant);
bool variant_toDouble(double* data, const variant_t* variant);
bool variant_toDateTime(struct tm* data, const variant_t* variant);
bool variant_toDateTimeExtended(pcb_datetime_t* data, const variant_t* variant);
bool variant_toList(variant_list_t* data, const variant_t* variant);
bool variant_toMap(variant_map_t* data, const variant_t* variant);
bool variant_toFd(int* data, const variant_t* variant);
bool variant_toByteArray(void** data, uint32_t* size, const variant_t* variant);
bool variant_toStringList(string_list_t* data, const variant_t* variant, const char* separator);

variant_type_t variant_parseVariantType(const char* string);

char* variant_char(const variant_t* variant);
int8_t variant_int8(const variant_t* variant);
int16_t variant_int16(const variant_t* variant);
int32_t variant_int32(const variant_t* variant);
int64_t variant_int64(const variant_t* variant);
uint8_t variant_uint8(const variant_t* variant);
uint16_t variant_uint16(const variant_t* variant);
uint32_t variant_uint32(const variant_t* variant);
uint64_t variant_uint64(const variant_t* variant);
bool variant_bool(const variant_t* variant);
double variant_double(const variant_t* variant);
struct tm* variant_dateTime(const variant_t* variant);
pcb_datetime_t* variant_dateTimeExtended(const variant_t* variant);
string_t* variant_string(const variant_t* variant);

/**
   @ingroup pcb_utils_variant
   @brief
   Converts a variant to a variant list.

   @details
   This function is provided for convience and is doing the same as @ref variant_toList.
   The returned pointer must be freed.\n

   @param variant the variant of interest

   @return
    - The converted value
 */
variant_list_t* variant_list(const variant_t* variant);
variant_map_t* variant_map(const variant_t* variant);
int variant_fd(const variant_t* variant);
void* variant_byteArray(const variant_t* variant, uint32_t* size);
string_list_t* variant_stringList(const variant_t* variant, const char* separator);

bool variant_print(string_t* data, const variant_t* variant, uint32_t attributes, const char** delimiters);
bool variant_fprint(FILE* stream, const variant_t* variant, uint32_t attributes, const char** delimiters);

const string_t* variant_da_string(const variant_t* variant);
const struct tm* variant_da_dateTime(const variant_t* variant);
const pcb_datetime_t* variant_da_dateTimeExtended(const variant_t* variant);
/**
   @ingroup pcb_utils_variant
   @brief
   Get a variant list pointer directly to the variant list data of an array variant.

   @details
   This function returns a variant list pointer directly to the variant list data contained within the variant.\n
   The resulting variant list may be used and modified, but NOT FREED. The ownership of the pointer is kept by the variant\n
   This function only works when the variant type is an array.\n
   The returned pointer MUST not be freed.\n

   @param variant the variant of interest

   @return
   The resulting variant list pointer, NULL if the variant is invalid or not of the array type.
 */
variant_list_t* variant_da_list(const variant_t* variant);

/**
   @ingroup pcb_utils_variant
   @brief
   Get a variant map pointer directly to the variant map data of an map variant.

   @details
   This function returns a variant map pointer directly to the variant map data contained within the variant.\n
   The resulting variant map may be used and modified, but NOT FREED. The ownership of the pointer is kept by the variant\n
   This function only works when the variant type is a map.\n
   The returned pointer MUST not be freed.\n

   @param variant the variant of interest

   @return
   The resulting variant map pointer, NULL if the variant is invalid or not of the map type.
 */
variant_map_t* variant_da_map(const variant_t* variant);
variant_t* variant_da_variant(const variant_t* variant);
const void* variant_da_byteArray(const variant_t* variant, uint32_t* size);
const char* variant_da_char(const variant_t* variant);

variant_type_t variant_type(const variant_t* variant);
bool variant_setType(variant_t* variant, variant_type_t type);

bool variant_compare(const variant_t* var1, const variant_t* var2, int* result);

/* Following functions should be removed:
     - variant_setArray
     - variant_setMap
   Following functions should be deprecated:
     - variant_setArrayMove
     - variant_setArrayCopy
     - variant_setArrayRef
     - variant_toArray
     - variant_array
 */

/**
   @ingroup pcb_utils_variant
   @deprecated
   Use @ref variant_setListMove

   @brief
   See @ref variant_setListMove

   @param variant See @ref variant_setListMove
   @param data See @ref variant_setListMove

   @return See @ref variant_setListMove
 */
static inline bool variant_setArrayMove(variant_t* variant, variant_list_t* data) {
    return variant_setListMove(variant, data);
}

/**
   @ingroup pcb_utils_variant
   @deprecated
   Use @ref variant_setListCopy

   @brief
   See @ref variant_setListCopy

   @param variant See @ref variant_setListCopy
   @param data See @ref variant_setListCopy

   @return See @ref variant_setListCopy
 */
static inline bool variant_setArrayCopy(variant_t* variant, const variant_list_t* data) {
    return variant_setListCopy(variant, data);
}

/**
   @ingroup pcb_utils_variant
   @deprecated
   Use @ref variant_setListRef

   @brief
   See @ref variant_setListRef

   @param variant See @ref variant_setListRef
   @param data See @ref variant_setListRef

   @return See @ref variant_setListRef
 */
static inline bool variant_setArrayRef(variant_t* variant, variant_list_t* data) {
    return variant_setListRef(variant, data);
}

/**
   @ingroup pcb_utils_variant
   @deprecated
   Use @ref variant_toList

   @brief
   See @ref variant_toList

   @param variant See @ref variant_toList
   @param data See @ref variant_toList

   @return See @ref variant_toList
 */
static inline bool variant_toArray(variant_list_t* data, const variant_t* variant) {
    return variant_toList(data, variant);
}

/**
   @ingroup pcb_utils_variant
   @deprecated
   Use @ref variant_list

   @brief
   See @ref variant_list

   @param variant See @ref variant_list

   @return See @ref variant_list
 */
static inline variant_list_t* variant_array(const variant_t* variant) {
    return variant_list(variant);
}

extern variant_t* variant_true;
extern variant_t* variant_false;

#ifdef __cplusplus
}
#endif

#endif

