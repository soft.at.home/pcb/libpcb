/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef HASH_TABLE_H
#define HASH_TABLE_H

typedef void (* hash_table_destructor_t)(const char* key, void* value);

typedef struct _hash_table hash_table_t;
typedef struct _hash_table_bucket hash_table_bucket_t;
typedef struct _hash_table_entry hash_table_entry_t;

/**
   @ingroup pcb_utils_hash_table
   @brief
   Create an hash table with the specified size (the number of allocated buckets).
   This container allows to maintain a map between keys (string) and values (void *)
   and provide get and set operations in a constant time (The algorithm complexity is O(1)).

   @param size of the hash table.

   @return
    - Reference on an hash table if initialization was successful
    - NULL if an error occured, pcb_error will be set
        - pcb_error_invalid_parameter when table is NULL or value is size
        - pcb_error_no_memory when out of memory
 */
hash_table_t* hash_table_create(uint32_t size);

/**
   @ingroup pcb_utils_hash_table
   @brief
   Destroy the hash table.
   If a destructor was provided with hash_table_setDestructor(),
   the destructor is called for each hash table entry.

   @param table the hash table to cleanup.
 */
void hash_table_destroy(hash_table_t* table);

/**
   @ingroup pcb_utils_hash_table
   @brief
   Set a destructor handler called when an hash table entry is destroyed.
   The destructor should ensure to cleanup the entry's value.

   @param table a reference to the hash table entry.
   @param destructor function used for cleaning up an hash table entry value.

   @return
    - true on success
    - false if an error occured, pcb_error will be set
        - pcb_error_invalid_parameter when entry is invalid
 */
bool hash_table_setDestructor(hash_table_t* table, hash_table_destructor_t destructor);

/**
   @ingroup pcb_utils_hash_table
   @brief
   Return the hash table entry associated to key in the hash table.
   The operation is O(1).

   @param table a reference to the hash table.
   @param key: hash table key.

   @return
    - Reference on an hash table entry on success
    - NULL if an error occured, pcb_error will be set
        - pcb_error_invalid_parameter when table or key is invalid
        - pcb_error_not_found when entry is not found
 */
hash_table_entry_t* hash_table_find(hash_table_t* table, const char* key);

/**
   @ingroup pcb_utils_hash_table
   @brief
   Return the hash table entry associated to key in the hash table.
   The entry is added if it does not exist.

   The operation is O(1).

   @param table a reference to the hash table.
   @param key: hash table key.

   @return
    - Reference on an hash table entry on success
    - NULL if an error occured, pcb_error will be set
        - pcb_error_invalid_parameter when table or key is invalid
        - pcb_error_no_memory when out of memory
 */
hash_table_entry_t* hash_table_find_or_add(hash_table_t* table, const char* key);

/**
   @ingroup pcb_utils_hash_table
   @brief
   Return hash table entry's value.

   @param entry a reference to the hash table entry.

   @return
    - value
 */
void* hash_table_entry_getValue(hash_table_entry_t* entry);

/**
   @ingroup pcb_utils_hash_table
   @brief
   Set hash table entry's value.
   If hash table entry was already containing a value, its destructor is called if it was provided with hash_table_setDestructor().

   @param entry a reference to the hash table entry.
 */
bool hash_table_entry_setValue(hash_table_entry_t* entry, void* value);

/**
   @ingroup pcb_utils_hash_table
   @brief
   Destroy the hash table entry.
   The hash table entry destructor is called if it was provided with hash_table_setdestructor().

   @param table a reference to the hash table entry.

   @return
    - true on success
    - false if an error occured, pcb_error will be set
        - pcb_error_invalid_parameter when entry is invalid
 */
bool hash_table_entry_destroy(hash_table_entry_t* entry);

/**
   @ingroup pcb_utils_hash_table
   @brief
   Return the hash table value associated to key in the hash table.
   The operation is O(1).

   @param table a reference to the hash table.
   @param key: hash table key.

   @return
    - value stored in hash table on success
    - NULL if an error occured, pcb_error will be set
        - pcb_error_invalid_parameter when table or key is invalid
        - pcb_error_not_found when entry is not found
 */
static inline void* hash_table_get(hash_table_t* table, const char* key) {
    return hash_table_entry_getValue(hash_table_find(table, key));
}

/**
   @ingroup pcb_utils_hash_table
   @brief
   Set hash table entry's value referenced by key.
   If hash table entry was already containing a value, its destructor is called if it was provided with hash_table_setDestructor().
   The operation is O(1).

   @param table a reference to the hash table.
   @param key: hash table key.
   @param value: hash table value.

   @return
    - value stored in hash table on success
    - NULL if an error occured, pcb_error will be set
        - pcb_error_invalid_parameter when table or key is invalid
        - pcb_error_no_memory when out of memory
 */
static inline bool hash_table_set(hash_table_t* table, const char* key, void* value) {
    return hash_table_entry_setValue(hash_table_find_or_add(table, key), value);
}

/**
   @ingroup pcb_utils_hash_table
   @brief
   Remove hash table entry referenced by key. The hash table entry destructor is called if it was provided with hash_table_setdestructor().
   The operation is O(1).

   @param table a reference to the hash table.
   @param key: hash table key.
   @param value: hash table value.

   @return
    - value stored in hash table on success
    - NULL if an error occured, pcb_error will be set
        - pcb_error_invalid_parameter when table or key is invalid
        - pcb_error_no_memory when out of memory
 */

static inline bool hash_table_remove(hash_table_t* table, const char* key) {
    return hash_table_entry_destroy(hash_table_find(table, key));
}

#endif
