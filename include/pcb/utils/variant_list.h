/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(PCB_VARIANT_LIST_H)
#define PCB_VARIANT_LIST_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <stddef.h>

#include <pcb/utils/linked_list.h>
#include <pcb/utils/variant.h>

/**
   @ingroup pcb_utils_containers
   @file
   @brief
   Header file with variant list type definitions and public variant list functions
 */

/**
   @ingroup pcb_utils_containers
   @defgroup pcb_utils_variant_list Variant List
   @{

   @brief
   A variant list is a linked list that contains @ref pcb_utils_variant

   @details
   Variant lists can be used to store a list of variants. Each of these variants can be of a different type.
   A variant by itself can be of the @ref variant_type_array, so you can put a variant list in a variant list.

   Managing the variant list is like a normal @ref pcb_utils_linked_list. Almost the same functions are provided as with the
   @ref pcb_utils_linked_list implementation.
   A @ref variant_list_iterator_t will contain the variant. To fetch the variant from the iterator you can use @ref variant_list_iterator_data.

   For your convience some extra functions are provide to help in creating @ref variant_list_iterator_t or for adding values to the list.
 */

// iterator macros
//---------------------------------------------------------------------------------------------
/**
   @brief
   Definition of a helper macro for forward iteration through the variant linked list (head->tail)

   @details
   This <b>helper macro</b> iterates forward through a <b>variant linked list</b> starting from the head

   @warning
   don&rsquo;t <b>modify</b> the variant linked list during the iteration!

   @param it a pointer variable to the list items
   @param list the list
 */
#define variant_list_for_each(it, list) \
    for(it = variant_list_first(list); it; it = variant_list_iterator_next(it))

//---------------------------------------------------------------------------------------------
/**
   @brief
   Definition of a helper macro for backward iteration through the variant linked list (tail->head)

   @details
   This <b>helper macro</b> iterates backwards through a <b>variant linked list</b> starting from the tail
   @warning
   don&rsquo;t <b>modify</b> the variant linked list during the iteration!

   @param it a pointer variable to the list items
   @param list the list
 */
#define variant_list_for_each_reverse(it, list) \
    for(it = variant_list_last(list); it; it = variant_list_iterator_prev(it))

//---------------------------------------------------------------------------------------------

#if __STDC_VERSION__ >= 199901L
/**
   @brief
   Definition of a helper macro for forward iteration through the variant linked list (head->tail)

   @details
   This <b>helper macro</b> iterates forward through a <b>variant linked list</b> starting from the head
   @warning
   don&rsquo;t <b>modify</b> the variant linked list during the iteration!

   @param it a pointer variable to the list items
   @param list the list
 */
#define variant_list_for_each_declare(it, list) \
    for(variant_list_iterator_t* it = variant_list_first(list); it; it = variant_list_iterator_next(it))

//---------------------------------------------------------------------------------------------
/**
   @brief
   Definition of a helper macro for backward iteration through the variant linked list (tail->head)

   @details
   This <b>helper macro</b> iterates backwards through a <b>variant linked list</b> starting from the tail

   @warning
   don&rsquo;t <b>modify</b> the variant linked list during the iteration!

   @param it a pointer variable to the list items
   @param list the list
 */
#define variant_list_for_each_reverse_declare(it, list) \
    for(variant_list_iterator_t* it = variant_list_last(list); it; it = variant_list_iterator_prev(it))

//---------------------------------------------------------------------------------------------

#endif

/**
   @brief
   A variant linked list iterator

   @details
   A variant linked list iterator structure. This structure contains the variant and a @ref llist_iterator_t
 */
typedef struct _variant_list_item {
    llist_iterator_t llist_it; /**< the linked list. */
    variant_t variant;         /**< the variant data. */
} variant_list_iterator_t;

/**
   @}
 */

// initializer functions
bool variant_list_initialize(variant_list_t* varlist);
variant_list_iterator_t* variant_list_iterator_create(const variant_t* variant);
variant_list_iterator_t* variant_list_iterator_createRef(variant_t* variant);
variant_list_iterator_t* variant_list_iterator_createChar(const char* data);
variant_list_iterator_t* variant_list_iterator_createString(const string_t* data);
variant_list_iterator_t* variant_list_iterator_createInt8(int8_t data);
variant_list_iterator_t* variant_list_iterator_createInt16(int16_t data);
variant_list_iterator_t* variant_list_iterator_createInt32(int32_t data);
variant_list_iterator_t* variant_list_iterator_createInt64(int64_t data);
variant_list_iterator_t* variant_list_iterator_createUInt8(uint8_t data);
variant_list_iterator_t* variant_list_iterator_createUInt16(uint16_t data);
variant_list_iterator_t* variant_list_iterator_createUInt32(uint32_t data);
variant_list_iterator_t* variant_list_iterator_createUInt64(uint64_t data);
variant_list_iterator_t* variant_list_iterator_createDouble(double data);
variant_list_iterator_t* variant_list_iterator_createBool(bool data);
variant_list_iterator_t* variant_list_iterator_createDateTime(const struct tm* data);
variant_list_iterator_t* variant_list_iterator_createDateTimeExtended(const pcb_datetime_t* data);
variant_list_iterator_t* variant_list_iterator_createListMove(variant_list_t* data);
variant_list_iterator_t* variant_list_iterator_createListCopy(const variant_list_t* data);
variant_list_iterator_t* variant_list_iterator_createListRef(variant_list_t* data);
variant_list_iterator_t* variant_list_iterator_createMapMove(variant_map_t* data);
variant_list_iterator_t* variant_list_iterator_createMapCopy(const variant_map_t* data);
variant_list_iterator_t* variant_list_iterator_createMapRef(variant_map_t* data);

// cleanup functions
void variant_list_cleanup(variant_list_t* varlist);
void variant_list_clear(variant_list_t* varlist);
void variant_list_iterator_destroy(variant_list_iterator_t* it);

// data function
/**
   @ingroup pcb_utils_variant_list
   @brief
   Get a pointer to the variant in this variant_list_iterator_t

   @details
   This function returns a pointer to the variant_t item of this iterator

   @param it the variant list iterator

   @return
    - a pointer to the variant item
    - NULL if it is NULL
    must not be freed.
 */
variant_t* variant_list_iterator_data(const variant_list_iterator_t* it);
bool variant_list_add(variant_list_t* varlist, const variant_t* variant);
bool variant_list_addMove(variant_list_t* varlist, variant_t* variant);
bool variant_list_addRef(variant_list_t* varlist, variant_t* variant);
bool variant_list_addChar(variant_list_t* varlist, const char* data);
bool variant_list_addString(variant_list_t* varlist, const string_t* data);
bool variant_list_addInt8(variant_list_t* varlist, int8_t data);
bool variant_list_addInt16(variant_list_t* varlist, int16_t data);
bool variant_list_addInt32(variant_list_t* varlist, int32_t data);
bool variant_list_addInt64(variant_list_t* varlist, int64_t data);
bool variant_list_addUInt8(variant_list_t* varlist, uint8_t data);
bool variant_list_addUInt16(variant_list_t* varlist, uint16_t data);
bool variant_list_addUInt32(variant_list_t* varlist, uint32_t data);
bool variant_list_addUInt64(variant_list_t* varlist, uint64_t data);
bool variant_list_addDouble(variant_list_t* varlist, double data);
bool variant_list_addBool(variant_list_t* varlist, bool data);
bool variant_list_addDateTime(variant_list_t* varlist, const struct tm* data);
bool variant_list_addDateTimeExtended(variant_list_t* varlist, const pcb_datetime_t* data);
bool variant_list_addListMove(variant_list_t* varlist, variant_list_t* data);
bool variant_list_addListCopy(variant_list_t* varlist, const variant_list_t* data);
bool variant_list_addListRef(variant_list_t* varlist, variant_list_t* data);
bool variant_list_addMapMove(variant_list_t* varlist, variant_map_t* data);
bool variant_list_addMapCopy(variant_list_t* varlist, const variant_map_t* data);
bool variant_list_addMapRef(variant_list_t* varlist, variant_map_t* data);

// accessor functions
/**
   @ingroup pcb_utils_variant_list
   @brief
   Get the first variant list element.

   @details
   This function returns the first variant list element of the variant linked list. \n

   @param list the variant linked list

   @return
    - a <b>pointer</b> to the first variant list element of the variant linked list.
    - NULL if the variant list is empty.
    - NULL if the variant list parameter is NULL.
    return value must not be freed.
 */
variant_list_iterator_t* variant_list_first(const variant_list_t* list);
variant_list_iterator_t* variant_list_last(const variant_list_t* list);
variant_list_iterator_t* variant_list_at(const variant_list_t* list, unsigned int index);
variant_list_iterator_t* variant_list_iterator_next(const variant_list_iterator_t* it);
variant_list_iterator_t* variant_list_iterator_prev(const variant_list_iterator_t* it);

// insertion functions
bool variant_list_append(variant_list_t* list, variant_list_iterator_t* insert);
bool variant_list_prepend(variant_list_t* list, variant_list_iterator_t* insert);
bool variant_list_insertAt(variant_list_t* list, unsigned int index, variant_list_iterator_t* insert);

// removal functions
variant_list_iterator_t* variant_list_iterator_take(variant_list_iterator_t* it);
variant_list_iterator_t* variant_list_takeFirst(variant_list_t* list);
variant_list_iterator_t* variant_list_takeLast(variant_list_t* list);

// property functions
unsigned int variant_list_size(const variant_list_t* list);
bool variant_list_isEmpty(const variant_list_t* list);

bool variant_list_contains(const variant_list_t* list, const variant_t* data);

bool variant_list_compare(const variant_list_t* list1, const variant_list_t* list2, int* result);

#include <pcb/utils/variant_list_iterator.h>

/* Following functions should be deprecated:
     - variant_list_iterator_createArray
     - variant_list_iterator_createArrayCopy
     - variant_list_iterator_createArrayRef
     - variant_list_iterator_createMap
     - variant_list_addArrayMove
     - variant_list_addArrayCopy
     - variant_list_addMap
 */

/**
   @ingroup pcb_utils_variant_list
   @deprecated Use @ref variant_list_iterator_createListMove
   @brief See @ref variant_list_iterator_createListMove
   @param data See @ref variant_list_iterator_createListMove
   @return
    See @ref variant_list_iterator_createListMove.
 */
static inline variant_list_iterator_t* variant_list_iterator_createArray(variant_list_t* data) {
    return variant_list_iterator_createListMove(data);
}

/**
   @ingroup pcb_utils_variant_list
   @deprecated Use @ref variant_list_iterator_createListCopy
   @brief See @ref variant_list_iterator_createListCopy
   @param data See @ref variant_list_iterator_createListCopy
   @return
    See @ref variant_list_iterator_createListCopy.
 */
static inline variant_list_iterator_t* variant_list_iterator_createArrayCopy(const variant_list_t* data) {
    return variant_list_iterator_createListCopy(data);
}

/**
   @ingroup pcb_utils_variant_list
   @deprecated Use @ref variant_list_iterator_createListRef
   @brief See @ref variant_list_iterator_createListRef
   @param data See @ref variant_list_iterator_createListRef
   @return
    See @ref variant_list_iterator_createListRef.
 */
static inline variant_list_iterator_t* variant_list_iterator_createArrayRef(variant_list_t* data) {
    return variant_list_iterator_createListRef(data);
}

/**
   @ingroup pcb_utils_variant_list
   @deprecated Use @ref variant_list_iterator_createMapMove
   @brief See @ref variant_list_iterator_createMapMove
   @param data See @ref variant_list_iterator_createMapMove
   @return
    See @ref variant_list_iterator_createMapMove.
 */
static inline variant_list_iterator_t* variant_list_iterator_createMap(variant_map_t* data) {
    return variant_list_iterator_createMapMove(data);
}

/**
   @ingroup pcb_utils_variant_list
   @deprecated Use @ref variant_list_addListMove
   @brief See @ref variant_list_addListMove
   @param varlist See @ref variant_list_addListMove
   @param data See @ref variant_list_addListMove
   @return
    See @ref variant_list_addListMove.
 */
static inline bool variant_list_addArrayMove(variant_list_t* varlist, variant_list_t* data) {
    return variant_list_addListMove(varlist, data);
}

/**
   @ingroup pcb_utils_variant_list
   @deprecated Use @ref variant_list_addListCopy
   @brief See @ref variant_list_addListCopy
   @param varlist See @ref variant_list_addListCopy
   @param data See @ref variant_list_addListCopy
   @return
    See @ref variant_list_addListCopy.
 */
static inline bool variant_list_addArrayCopy(variant_list_t* varlist, const variant_list_t* data) {
    return variant_list_addListCopy(varlist, data);
}

/**
   @ingroup pcb_utils_variant_list
   @deprecated Use @ref variant_list_addListRef
   @brief See @ref variant_list_addListRef
   @param varlist See @ref variant_list_addListRef
   @param data See @ref variant_list_addListRef
   @return
    See @ref variant_list_addListRef.
 */
static inline bool variant_list_addArrayRef(variant_list_t* varlist, variant_list_t* data) {
    return variant_list_addListRef(varlist, data);
}

/**
   @ingroup pcb_utils_variant_list
   @deprecated Use @ref variant_list_addMapMove
   @brief See @ref variant_list_addMapMove
   @param varlist See @ref variant_list_addMapMove
   @param data See @ref variant_list_addMapMove
   @return
    See @ref variant_list_addMapMove.
 */
static inline bool variant_list_addMap(variant_list_t* varlist, variant_map_t* data) {
    return variant_list_addMapMove(varlist, data);
}

#ifdef __cplusplus
}
#endif

#endif // PCB_VARIANT_LIST_H

