/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(PCB_VARIANT_MAP_H)
#define PCB_VARIANT_MAP_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <stddef.h>

#include <pcb/utils/linked_list.h>
#include <pcb/utils/variant.h>

/**
   @ingroup pcb_utils_containers
   @file
   @brief
   Header file with variant map type definitions and public variant map functions
 */


/**
   @ingroup pcb_utils_containers
   @defgroup pcb_utils_variant_map Variant map
   @{

   @brief
   Variant map handling functions.

   @brief
   A variant map is a linked list that contains key value pairs, where the value is represented with a @ref pcb_utils_variant

   @details
   Variant list can be used to store a list of variants. Each of these variants can be of a different type.
   A variant by itself can be of the @ref variant_type_map, so you can put a variant map in a variant map

   Managing the variant map is like a normal @ref pcb_utils_linked_list. Almost the same functions are provided as with the
   @ref pcb_utils_linked_list implementation.
   A @ref variant_map_iterator_t will contain the variant and a key. To fetch the variant from the iterator you can use @ref variant_map_iterator_data.
   To fetch the key from an iterator you can use @ ref variant_map_iterator_key

   For your convience some extra functions are provide to help in creating @ref variant_map_iterator_t or for adding values to the map

   Duplicated keys are allowed in the variant map..
 */

// iterator macros
//---------------------------------------------------------------------------------------------
/**
   @brief
   Definition of a helper macro for forward iteration through the variant map (head->tail)

   @details
   \n
   This <b>helper macro</b> iterates forward through a <b>variant map</b> starting from the head\n
   \n
   <b>Warning</b>: don&rsquo;t <b>modify</b> the variant map during the iteration!

   @param it a pointer variable to the map item
   @param map the map
 */
#define variant_map_for_each(it, map) \
    for(it = variant_map_first(map); it; it = variant_map_iterator_next(it))

//---------------------------------------------------------------------------------------------
/**
   @brief
   Definition of a helper macro for backward iteration through the variant map (tail->head)

   @details
   \n
   This <b>helper macro</b> iterates backwards through a <b>variant map</b> starting from the tail\n
   \n
   <b>Warning</b>: don&rsquo;t <b>modify</b> the variant map during the iteration!

   @param it a pointer variable to the map item
   @param map the map
 */
#define variant_map_for_each_reverse(it, map) \
    for(it = variant_map_last(map); it; it = variant_map_iterator_prev(it))

#if __STDC_VERSION__ >= 199901L
/**
   @brief
   Definition of a helper macro for forward iteration through the variant map (head->tail)

   @details
   \n
   This <b>helper macro</b> iterates forward through a <b>variant map</b> starting from the head\n
   \n
   <b>Warning</b>: don&rsquo;t <b>modify</b> the variant map during the iteration!

   @param it the name of a pointer to a variant_map_iterator_t variable
       to the map item that will be declared
   @param map the map
 */
#define variant_map_for_each_declare(it, map) \
    for(variant_map_iterator_t* it = variant_map_first(map); it; it = variant_map_iterator_next(it))

//---------------------------------------------------------------------------------------------
/**
   @brief
   Definition of a helper macro for backward iteration through the variant map (tail->head)

   @details
   \n
   This <b>helper macro</b> iterates backwards through a <b>variant map</b> starting from the tail\n
   \n
   <b>Warning</b>: don&rsquo;t <b>modify</b> the variant map during the iteration!

   @param it the name of a pointer to a variant_map_iterator_t variable to the map item
       that will be declared
   @param map the map
 */
#define variant_map_for_each_reverse_declare(it, map) \
    for(variant_map_iterator_t* it = variant_map_last(map); it; it = variant_map_iterator_prev(it))

#endif

//---------------------------------------------------------------------------------------------
/**
   @brief
   A variant map iterator

   @details
   A variant map iterator
 */
typedef struct _variant_map_item {
    llist_iterator_t llist_it; /**< the linked list. */
    char* key;                 /**< the key */
    variant_t variant;         /**< the variant data. */
} variant_map_iterator_t;

/**
   @}
 */

// initializer functions
bool variant_map_initialize(variant_map_t* varmap);
variant_map_iterator_t* variant_map_iterator_create(const char* key, const variant_t* variant);
variant_map_iterator_t* variant_map_iterator_createRef(const char* key, variant_t* variant);
variant_map_iterator_t* variant_map_iterator_createChar(const char* key, const char* data);
variant_map_iterator_t* variant_map_iterator_createString(const char* key, const string_t* data);
variant_map_iterator_t* variant_map_iterator_createInt8(const char* key, int8_t data);
variant_map_iterator_t* variant_map_iterator_createInt16(const char* key, int16_t data);
variant_map_iterator_t* variant_map_iterator_createInt32(const char* key, int32_t data);
variant_map_iterator_t* variant_map_iterator_createInt64(const char* key, int64_t data);
variant_map_iterator_t* variant_map_iterator_createUInt8(const char* key, uint8_t data);
variant_map_iterator_t* variant_map_iterator_createUInt16(const char* key, uint16_t data);
variant_map_iterator_t* variant_map_iterator_createUInt32(const char* key, uint32_t data);
variant_map_iterator_t* variant_map_iterator_createUInt64(const char* key, uint64_t data);
variant_map_iterator_t* variant_map_iterator_createDouble(const char* key, double data);
variant_map_iterator_t* variant_map_iterator_createBool(const char* key, bool data);
variant_map_iterator_t* variant_map_iterator_createDateTime(const char* key, const struct tm* data);
variant_map_iterator_t* variant_map_iterator_createDateTimeExtended(const char* key, const pcb_datetime_t* data);
variant_map_iterator_t* variant_map_iterator_createListMove(const char* key, variant_list_t* data);
variant_map_iterator_t* variant_map_iterator_createListCopy(const char* key, const variant_list_t* data);
variant_map_iterator_t* variant_map_iterator_createListRef(const char* key, variant_list_t* data);
variant_map_iterator_t* variant_map_iterator_createMapMove(const char* key, variant_map_t* data);
variant_map_iterator_t* variant_map_iterator_createMapCopy(const char* key, const variant_map_t* data);
variant_map_iterator_t* variant_map_iterator_createMapRef(const char* key, variant_map_t* data);

// cleanup functions
void variant_map_cleanup(variant_map_t* varmap);
void variant_map_clear(variant_map_t* varmap);
void variant_map_iterator_destroy(variant_map_iterator_t* it);

// data function
/**
   @ingroup pcb_utils_variant_map
   @brief
   Get a pointer to the variant in this variant_map_iterator_t

   @details
   This function returns a pointer to the variant_t item of this iterator.
   Do not free the pointer returned, if you want to destroy the variant in the iterator, you need to destroy the
   iterator using @ref variant_map_iterator_destroy

   @param it the variant map iterator

   @return
    - a pointer to the variant item
 */
variant_t* variant_map_iterator_data(const variant_map_iterator_t* it);
const char* variant_map_iterator_key(const variant_map_iterator_t* it);
bool variant_map_iterator_setKey(variant_map_iterator_t* it, const char* key);
bool variant_map_add(variant_map_t* varmap, const char* key, const variant_t* variant);
bool variant_map_addMove(variant_map_t* varmap, const char* key, variant_t* variant);
bool variant_map_addRef(variant_map_t* varmap, const char* key, variant_t* variant);
bool variant_map_addChar(variant_map_t* varmap, const char* key, const char* data);
bool variant_map_addString(variant_map_t* varmap, const char* key, const string_t* data);
bool variant_map_addInt8(variant_map_t* varmap, const char* key, int8_t data);
bool variant_map_addInt16(variant_map_t* varmap, const char* key, int16_t data);
bool variant_map_addInt32(variant_map_t* varmap, const char* key, int32_t data);
bool variant_map_addInt64(variant_map_t* varmap, const char* key, int64_t data);
bool variant_map_addUInt8(variant_map_t* varmap, const char* key, uint8_t data);
bool variant_map_addUInt16(variant_map_t* varmap, const char* key, uint16_t data);
bool variant_map_addUInt32(variant_map_t* varmap, const char* key, uint32_t data);
bool variant_map_addUInt64(variant_map_t* varmap, const char* key, uint64_t data);
bool variant_map_addDouble(variant_map_t* varmap, const char* key, double data);
bool variant_map_addBool(variant_map_t* varmap, const char* key, bool data);
bool variant_map_addDateTime(variant_map_t* varmap, const char* key, const struct tm* data);
bool variant_map_addDateTimeExtended(variant_map_t* varmap, const char* key, const pcb_datetime_t* data);
bool variant_map_addListMove(variant_map_t* varmap, const char* key, variant_list_t* data);
bool variant_map_addListCopy(variant_map_t* varmap, const char* key, const variant_list_t* data);
bool variant_map_addListRef(variant_map_t* varmap, const char* key, variant_list_t* data);
bool variant_map_addMapMove(variant_map_t* varmap, const char* key, variant_map_t* data);
bool variant_map_addMapCopy(variant_map_t* varmap, const char* key, const variant_map_t* data);
bool variant_map_addMapRef(variant_map_t* varmap, const char* key, variant_map_t* variant);

// accessor functions
variant_map_iterator_t* variant_map_first(const variant_map_t* map);
variant_map_iterator_t* variant_map_last(const variant_map_t* map);
variant_map_iterator_t* variant_map_at(const variant_map_t* map, unsigned int index);
variant_map_iterator_t* variant_map_iterator_next(const variant_map_iterator_t* it);
variant_map_iterator_t* variant_map_iterator_prev(const variant_map_iterator_t* it);

// insertion functions
bool variant_map_append(variant_map_t* map, variant_map_iterator_t* insert);
bool variant_map_prepend(variant_map_t* map, variant_map_iterator_t* insert);
bool variant_map_insertAt(variant_map_t* map, uint32_t index, variant_map_iterator_t* insert);

// removal functions
variant_map_iterator_t* variant_map_iterator_take(variant_map_iterator_t* it);
variant_map_iterator_t* variant_map_takeFirst(variant_map_t* map);
variant_map_iterator_t* variant_map_takeLast(variant_map_t* map);

// property functions
uint32_t variant_map_size(const variant_map_t* map);
bool variant_map_isEmpty(const variant_map_t* map);

bool variant_map_contains(const variant_map_t* map, const char* key);

/**
   @ingroup pcb_utils_variant_map
   @brief
   Searches an iterator in the map that contains the given key.

   @details
   Searches from the beginning an iterator that contains the given key.
   The first iterator that matches will be returned

   @param map the list in wich you are interested
   @param key the key you want to find

   @return
    - true:
        - The element is part of the list
    - false: pcb_error is set to the correct error code
        - The list pointer or data pointer is invalid
        - The element is not found,
    must not be freed.
 */
variant_map_iterator_t* variant_map_find(const variant_map_t* map, const char* key);

/**
   @ingroup pcb_utils_variant_map
   @brief Does the same as @ref variant_map_find, but returns a pointer to the variant itself.
   @param map The variant map to search
   @param key The key to find
   @return
    Returns a pointer to a variant.
 */
static inline variant_t* variant_map_findVariant(const variant_map_t* map, const char* key) {
    return variant_map_iterator_data(variant_map_find(map, key));
}

bool variant_map_compare(const variant_map_t* map1, const variant_map_t* map2, int* result);

#include <pcb/utils/variant_map_iterator.h>

/* The following functions should become deprecated
     - variant_map_iterator_createArray
     - variant_map_iterator_createArrayCopy
     - variant_map_iterator_createArrayRef
     - variant_map_iterator_createMap
     - variant_map_addArray
     - variant_map_addArrayCopy
     - variant_map_addArrayRef
     - variant_map_addMap

 */

/**
   @ingroup pcb_utils_variant_map
   @deprecated Use @ref variant_map_iterator_createListMove
   @brief See @ref variant_map_iterator_createListMove
   @param key See @ref variant_map_iterator_createListMove
   @param data See @ref variant_map_iterator_createListMove
   @return
    See @ref variant_map_iterator_createListMove.
 */
static inline variant_map_iterator_t* variant_map_iterator_createArray(const char* key, variant_list_t* data) {
    return variant_map_iterator_createListMove(key, data);
}

/**
   @ingroup pcb_utils_variant_map
   @deprecated Use @ref variant_map_iterator_createListCopy
   @brief See @ref variant_map_iterator_createListCopy
   @param key See @ref variant_map_iterator_createListCopy
   @param data See @ref variant_map_iterator_createListCopy
   @return
    See @ref variant_map_iterator_createListCopy.
 */
static inline variant_map_iterator_t* variant_map_iterator_createArrayCopy(const char* key, const variant_list_t* data) {
    return variant_map_iterator_createListCopy(key, data);
}

/**
   @ingroup pcb_utils_variant_map
   @deprecated Use @ref variant_map_iterator_createListRef
   @brief See @ref variant_map_iterator_createListRef
   @param key See @ref variant_map_iterator_createListRef
   @param data See @ref variant_map_iterator_createListRef
   @return
    See @ref variant_map_iterator_createListRef.
 */
static inline variant_map_iterator_t* variant_map_iterator_createArrayRef(const char* key, variant_list_t* data) {
    return variant_map_iterator_createListRef(key, data);
}

/**
   @ingroup pcb_utils_variant_map
   @deprecated Use @ref variant_map_iterator_createMapMove
   @brief See @ref variant_map_iterator_createMapMove
   @param key See @ref variant_map_iterator_createMapMove
   @param data See @ref variant_map_iterator_createMapMove
   @return
    See @ref variant_map_iterator_createMapMove.
 */
static inline variant_map_iterator_t* variant_map_iterator_createMap(const char* key, variant_map_t* data) {
    return variant_map_iterator_createMapMove(key, data);
}

/**
   @ingroup pcb_utils_variant_map
   @deprecated Use @ref variant_map_addListMove
   @brief See @ref variant_map_addListMove
   @param varmap See @ref variant_map_addListMove
   @param key See @ref variant_map_addListMove
   @param data See @ref variant_map_addListMove
   @return
    See @ref variant_map_addListMove.
 */
static inline bool variant_map_addArray(variant_map_t* varmap, const char* key, variant_list_t* data) {
    return variant_map_addListMove(varmap, key, data);
}

/**
   @ingroup pcb_utils_variant_map
   @deprecated Use @ref variant_map_addListCopy
   @brief See @ref variant_map_addListCopy
   @param varmap See @ref variant_map_addListCopy
   @param key See @ref variant_map_addListCopy
   @param data See @ref variant_map_addListCopy
   @return
    See @ref variant_map_addListCopy.
 */
static inline bool variant_map_addArrayCopy(variant_map_t* varmap, const char* key, const variant_list_t* data) {
    return variant_map_addListCopy(varmap, key, data);
}

/**
   @ingroup pcb_utils_variant_map
   @deprecated Use @ref variant_map_addListRef
   @brief See @ref variant_map_addListRef
   @param varmap See @ref variant_map_addListRef
   @param key See @ref variant_map_addListRef
   @param data See @ref variant_map_addListRef
   @return
    See @ref variant_map_addListRef.
 */
static inline bool variant_map_addArrayRef(variant_map_t* varmap, const char* key, variant_list_t* data) {
    return variant_map_addListRef(varmap, key, data);
}

/**
   @ingroup pcb_utils_variant_map
   @deprecated Use @ref variant_map_addMapMove
   @brief See @ref variant_map_addMapMove
   @param varmap See @ref variant_map_addMapMove
   @param key See @ref variant_map_addMapMove
   @param data See @ref variant_map_addMapMove
   @return
    See @ref variant_map_addMapMove.
 */
static inline bool variant_map_addMap(variant_map_t* varmap, const char* key, variant_map_t* data) {
    return variant_map_addMapMove(varmap, key, data);
}

#ifdef __cplusplus
}
#endif

#endif // PCB_VARIANT_MAP_H

