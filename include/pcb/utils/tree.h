/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef PCBU_TREE_H
#define PCBU_TREE_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <stddef.h>

#include <pcb/utils/linked_list.h>

/**
   @ingroup pcb_utils_containers
   @file
   @brief
   Header file with tree type definitions and tree functions
 */

/**
   @ingroup pcb_utils_containers
   @defgroup pcb_utils_tree Tree
   @{

   @brief
   Hierarchical unordered tree implementations.

   @details
   In this tree implementation the data structure contains a pointer to the parent and a linked list of children.
   Because (@ref pcb_utils_linked_list) a linked is used for building the list of children and that each tree item can
   be part of this list also a list iterator member is in the stucture definition.

   For convience a structure is available that defines a tree. This structure only contains a tree_item_t member.

   @section pcb_utils_tree_create_items Adding data to a tree
   The tree elements do not contain a pointer to the data, they are iterators. To create a tree with elements
   containing data, a structure has to be defined that contains a tree_item_t\n\n
   @verbatim
   typedef struct _int32_tree_item {
    uint32_t childId;
    uint32_t depth;
    tree_item_t tree_item;
   } int32_tree_item_t;
   @endverbatim

   In the above example a tree item is defined that contains two 32 bit integers (the childID and depth). Before
   this can be used you have to initialize the iterator part of the structure using @ref tree_item_initialize
   function. After doing that you can add an instance of this structure to any tree.

   @verbatim
   tree_t tree;
   tree_initialize(&tree);
   tree_item_t *root = tree_root(&tree);

   int32_tree_item_t *item = (int32_tree_item_t *)calloc(1,sizeof(int32_tree_item_t));
   tree_item_initialize(&item->tree_item);
   item->childId = 1;
   item->depth = 1;
   tree_item_appendChild(root,&item->tree_item);
   @endverbatim

   To add extra items to the tree, @ref tree_item_appendChild and @ref tree_item_prependChild can be used.
   To insert the children at a specific location @ref tree_item_insertChildAt can be used.

   @section pcb_utils_tree_get_items Retreiving data from a tree
   To retrieve data from a tree, it is needed to get the correct iterator first. This can be done by using
   any of the following functions @ref tree_item_firstChild, @ref tree_item_lastChild, @ref tree_item_nextSibling,
   @ref tree_item_prevSibling and @ref tree_item_childAt.
   To retrieve the data from the iterator, use the macro @ref tree_item_data.

   @verbatim
   tree_iterator_t *some_it = tree_item_firstChild(root);
   int32_tree_item_t *data_element = tree_item_data(some_it,int32_tree_item_t,tree_item);
   @endverbatim

   @section pcb_utils_tree_traversing Walking a tree
   Some convieniance macros are provide to iterate a list: @ref tree_for_each_child and @ref
   tree_for_each_child_reverse.
   While using these macros do not add elements to the list or delete elements from the tree.
 */

// iterator macros
/**
   @brief
   Definition of a helper macro for forward iteration through the children of a tree item.

   @details
   \n
   This <b>helper macro</b> iterates forwards through the linked list of children of a tree item\n
   \n
   <b>Warning</b>: don&rsquo;t <b>modify</b> the linked list during the iteration!

   @param child a pointer variable to a tree item tree_item_t.
   @param root_item the parent tree item (tree_item_t)
 */
#define tree_for_each_child(child, root_item) \
    for(child = tree_item_firstChild(root_item); child; child = tree_item_nextSibling(child))

/**
   @brief
   Definition of a helper macro for backward iteration through the children of a tree item.

   @details
   \n
   This <b>helper macro</b> iterates backwards through the linked list of children of a tree item\n
   \n
   <b>Warning</b>: don&rsquo;t <b>modify</b> the linked list during the iteration!

   @param child a pointer variable to a tree item.
   @param root_item the parent tree item
 */
#define tree_for_each_child_reverse(child, root_item) \
    for(child = tree_item_lastChild(root_item); child; child = tree_item_prevSibling(child))

// data macro
/**
   @brief
   Definition of a helper macro for getting a pointer to the real data structure.

   @details
   \n
   This <b>macro</b> calculates the address of the containing data structure.\n

   @param item a pointer to the tree item for which we want to calculate the pointer to the containing structure
   @param type the type to which the pointer has to be casted
   @param member the name of the data member containing the list pointer
 */
#define tree_item_data(item, type, member) \
    (type*) (((char*) item) - offsetof(type, member));

/**
   @brief
   Tree item definition

   @details
   This structure defines a branch in the three.
 */
typedef struct _tree_item {
    llist_iterator_t listItem; /**< a linked list iterator, which is used to add this branch to the list of children of the parent */
    llist_t children;          /**< linked list of child tree items (branches) */
} tree_item_t;

/**
   @brief
   Tree definition

   @details
   This structure defines the root of a tree. It is not needed to use this structure
   to create a tree, a tree always start with a tree_item_t that has no parent.
   This is a convience structure.
 */
typedef struct _tree {
    tree_item_t root; /**< The root of a tree */
} tree_t;

/**
   @}
 */

// initializer functions
bool tree_initialize(tree_t* tree);
bool tree_item_initialize(tree_item_t* item);

// cleanup functions
void tree_cleanup(tree_t* list);
void tree_item_cleanup(tree_item_t* item);

// accessor functions
tree_item_t* tree_root(const tree_t* tree);

tree_item_t* tree_item_parent(const tree_item_t* item);
tree_item_t* tree_item_firstChild(tree_item_t* item);
tree_item_t* tree_item_childAt(tree_item_t* item, unsigned int index);
tree_item_t* tree_item_lastChild(tree_item_t* item);
tree_item_t* tree_item_nextSibling(const tree_item_t* item);
tree_item_t* tree_item_prevSibling(const tree_item_t* item);

// insertion functions
bool tree_item_appendChild(tree_item_t* parent, tree_item_t* child);
bool tree_item_prependChild(tree_item_t* parent, tree_item_t* child);
bool tree_item_insertChildAt(tree_item_t* parent, unsigned int index, tree_item_t* child);

// removal functions
tree_item_t* tree_item_takeChild(tree_item_t* child);
tree_item_t* tree_item_takeFirstChild(tree_item_t* parent);
tree_item_t* tree_item_takeLastChild(tree_item_t* parent);

// property functions
unsigned int tree_item_childCount(tree_item_t* parent);
bool tree_item_hasChildren(const tree_item_t* parent);

#ifdef __cplusplus
}
#endif

#endif

