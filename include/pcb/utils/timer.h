/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef __TIMER_H__
#define __TIMER_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <stddef.h>
#include <sys/time.h>

#include <pcb/utils/linked_list.h>

/**
   @ingroup pcb_utils_socket_events
   @file
   @brief
   Header file with timer functions
 */

/**
   @ingroup pcb_utils_socket_events
   @defgroup pcb_utils_timers Timers
   @{

   @brief
   Timers can be used to do a job at regular intervals or once some time later.

   @details
   To have timers fully functional an event loop must be started. The timers are using the SIGALRM signal.
   Multiple timers can be started, but only one timer will be the next to trigger the signal.
 */

typedef struct _pcb_timer pcb_timer_t;

/**
   @brief
   Timer timeout handler

   @details
   Definition of the timer timeout handler function
 */
typedef void (* pcb_timer_timeout_handler_t) (pcb_timer_t* timer, void* userdata);

/**
   @brief
   Deferred function

   @details
   Definition of a deferred function
 */
typedef void (* pcb_timer_deferred_function_t) (void* userdata);

/**
   @brief
   The timer states

   @details
   This enum holds all possible timer states
 */
typedef enum _pcb_timer_state {
    pcb_timer_off,       /**< Timer is not running */
    pcb_timer_started,   /**< Timer is started */
    pcb_timer_running,   /**< Timer is running */
    pcb_timer_expired,   /**< Timer has expired */
    pcb_timer_destroyed, /**< Timer is destroyed */
} pcb_timer_state_t;

/**
   @brief
   The timer type

   @details
   A type that holds all timer information.
 */
struct _pcb_timer {
    llist_iterator_t it;                    /**< Linked list iterator to put the tiomer in the global list of timers */
    struct itimerval timer;                 /**< The timer interval, if not set the timer is a single shot timer */
    pcb_timer_state_t state;                /**< The timer state */
    pcb_timer_timeout_handler_t handler;    /**< The handler function, called when a timer expires */
    void* userdata;                         /**< Some user specified data */
};

/**
   @}
 */

bool pcb_timer_initializeTimers(void);
void pcb_timer_cleanupTimers(void);

void pcb_timer_calculateTimers(void);
void pcb_timer_checkTimers(void);

bool pcb_timer_enableTimers(bool enable);

pcb_timer_t* pcb_timer_create(void);
void pcb_timer_destroy(pcb_timer_t* timer);

bool pcb_timer_setInterval(pcb_timer_t* timer, unsigned int msec);
bool pcb_timer_setHandler(pcb_timer_t* timer, pcb_timer_timeout_handler_t handler);
bool pcb_timer_setUserData(pcb_timer_t* timer, void* userData);
void* pcb_timer_getUserData(pcb_timer_t* timer);

unsigned int pcb_timer_remainingTime(pcb_timer_t* timer);

bool pcb_timer_start(pcb_timer_t* timer, unsigned int timeout_msec);
bool pcb_timer_stop(pcb_timer_t* timer);

pcb_timer_state_t pcb_timer_getState(pcb_timer_t* timer);

bool pcb_timer_defer(pcb_timer_deferred_function_t fn, void* userdata, unsigned int time);

bool pcb_timer_createAndStart(pcb_timer_t** timer, pcb_timer_timeout_handler_t handler,
                              void* userData, unsigned int timeInMs);

#ifdef __cplusplus
}
#endif
#endif
