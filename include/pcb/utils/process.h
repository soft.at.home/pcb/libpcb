/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(PCB_PROCESS_H)
#define PCB_PROCESS_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <pcb/utils/linked_list.h>
#include <pcb/utils/connection.h>

/**
   @ingroup pcb_utils_process_events
   @file
   @brief
   Header file with process functions
 */

/**
   @ingroup pcb_utils_process_events
   @defgroup pcb_process_layer Process
   @{

   @brief
   Child process encapsulation

   @details
   The purpose of this API is to manage child processes during their entire
   lifecycle. It maintains a handle per child process, through which the process
   can be managed. It also offers the possibility of a callback if the process
   dies.

   <b>Caution:</b>
   The process API relies on SIGCHLD and wait() to be notified of child death. It is
   important to be careful when implementing your own SIGCHLD handler through the
   PCB signal handling API or when using wait(). This is still possible, even in
   combination with the process API, but it's important to only call wait() on
   select PIDs, not for any child.
 */

/**
   @brief
   Information about a child process which just stopped.
 */
struct _process_exit_info {
    int exit_code;                      /**< The exit code of the process */
    bool signalled;                     /**< True if the process was killed by a signal */
    int signal;                         /**< The signal which caused the process to end */
    bool core_dump;                     /**< True if the process dumped core */
};
typedef struct _process_exit_info process_exit_info_t;

typedef bool (* process_event_handler_t) (process_info_t* process, process_exit_info_t* info);

/**
   @brief
   Structure to wrap child process information. It is to be treated as opaque by users of this API.
 */
struct _process_info {
    llist_iterator_t it;                /**< linked list iterator, which is used to add an instance of this structure into a linked list */
    pid_t pid;                          /**< The PID of the process */
    process_event_handler_t exit;       /**< The process' exit handler */
    bool running;                       /**< True if the process is currently running */
    connection_info_t* con;             /**< The PCB connection info */
    void* data;                         /**< The user data associated with this process */
    int fd[3][2];                       /**< fd pairs */
};

/**
   @brief
   Create a new process handle.
   @param con The PCB connection information object
   @return
   A process handle, or NULL on error
 */
process_info_t* process_create(connection_info_t* con);

/* @brief create a new pipe to be used with the new process
 * Make sure that the child process enables stdio line buffering if needed
 * (call setvbuf(stdout, NULL, _IOLBF, (size_t) 0); in main())
 * Make sure to call process_getFd prior to starting the process.
 * @param process The process handle
 * @param fd the fd number on the child end. one of STDIN_FILENO, STDOUT_FILENO, STDERR_FILENO
 * @return
 * the fd of the local end of the pipe
 */
int process_getFd(process_info_t* process, int fd);

/* @brief Destroy the requested fd of the specified process.
 * This only works if the process is no longer running.
 * @param process The process handle
 * @param requested The fd number on the child end. one of STDIN_FILENO, STDOUT_FILENO, STDERR_FILENO
 * @return -1 on error, 0 if successfull .
 */
int process_closeFd(process_info_t* process, int requested);

/**
   @brief
   Destroy a process handle
   @param process The process handle
   @return
   True if the process handle was destroyed
   False on error (e.g. when the process is still running
 */
bool process_destroy(process_info_t* process);

/**
   @brief
   Start a process
   @param process The process handle
   @param cmd The command to start
   @param ... optional arguments. The last one <b>must</b> be NULL
   @return
   false on error
 */
bool process_start(process_info_t* process, char* cmd, ...);

/**
   @brief
   Start a process
   @param process The process handle
   @param argv list containing the command to start at index 0, followed by arguments.  must be NULL terminated!
   @return
   false on error
 */

bool process_vstart(process_info_t* process, char** argv);

/**
   @brief
   Send a signal to a running process
   @param process The process handle
   @param sig The signal to send
   @return
   false on error
 */
bool process_kill(process_info_t* process, int sig);

/**
   @brief
   Wait for a process to die, or give up after a timeout
   This blocks until the process is actually dead or the timeout expires
   Make sure that the process is NOT dead before calling this function otherwise
   you will recieve a timeout. If you sent a signal just before, there is probably
   a race conditon and you should use process_kill_wait_timeout instead.
   @param process The process handle
   @param timeout The timeout value (in ms)
   @return
   false on error or timeout
 */
bool process_wait_timeout(process_info_t* process, uint32_t timeout);

/**
   @brief
   Send a signal to a running process and wait for it to die, or give up after a timeout
   This blocks until the process is actually dead or the timeout expires.
   @param process The process handle
   @param sig The signal to send
   @param timeout The timeout value (in ms)
   @return
   false on error or timeout
 */
bool process_kill_wait_timeout(process_info_t* process, int sig, uint32_t timeout);

/**
   @brief
   Wait for a process to die
   This blocks until the process is actually dead.
   @param process The process handle
   @return
   false on error
 */
bool process_wait(process_info_t* process);

/**
   @brief
   Add user data to a process handle.
   It can later be retrieved with process_getUserData()
   @param process The process handle
   @param data The user data to add
   @return
   false on error
 */
bool process_setUserData(process_info_t* process, void* data);
void* process_getUserData(process_info_t* process);

/**
   @brief
   Add an exit handler.
   The exit handler callback is called when the child process dies.
   The exit handler gets additional information about the death of the child process.
   @param process The process handle
   @param handler The callback function
   @return
   false on error
 */
bool process_setExitHandler(process_info_t* process, process_event_handler_t handler);

/**
   @brief
   Get the process ID from a process handle
   @param process The process handle
   @return
   - The PID of the process
   - -1 if the process is not running or on error
 */
pid_t process_getPid(process_info_t* process);

/**
   @brief
   Is the process currently running?
   @param process The process handle
   @return
   - true if the process is running
   - false if the process is not running
 */
bool process_isRunning(process_info_t* process);

bool process_handle_sigchld(process_info_t* process);
bool process_handle_sigchld_status(process_info_t* process, int status);

#ifdef __cplusplus
}
#endif
#endif
