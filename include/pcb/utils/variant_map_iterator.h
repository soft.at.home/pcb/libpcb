/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef _PCB_UTILS_VARIANT_MAP_ITERATOR_H
#define _PCB_UTILS_VARIANT_MAP_ITERATOR_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <pcb/utils/variant.h>

/**
   @ingroup pcb_utils_containers
   @file
   @brief
   Header file with variant map helper functions for iterating a variant map
 */

/**
   @ingroup pcb_utils_variant_map
   @brief
   Get a variant map iterator data and converts it to a bool

   @details
   This function is provided for convenience. It is fetching a variant in a variant map iterator
   and converts it to a bool as @ref variant_toBool.\n
   The return pointer MUST be freed.\n

   @param map the variant map of interest
   @param key the name of the iterator

   @return
    - The converted value
 */
static inline bool variant_map_findBool(const variant_map_t* map, const char* key) {
    return variant_bool(variant_map_findVariant(map, key));
}

/**
   @ingroup pcb_utils_variant_map
   @brief
   Get a variant map iterator data and converts it to a int8_t

   @details
   This function is provided for convenience. It is fetching a variant in a variant map iterator
   and converts it to a int8_t as @ref variant_toInt8.\n
   The return pointer MUST be freed.\n

   @param map the variant map of interest
   @param key the name of the iterator

   @return
    - The converted value
 */
static inline int8_t variant_map_findInt8(const variant_map_t* map, const char* key) {
    return variant_int8(variant_map_findVariant(map, key));
}

/**
   @ingroup pcb_utils_variant_map
   @brief
   Get a variant map iterator data and converts it to a int16_t

   @details
   This function is provided for convenience. It is fetching a variant in a variant map iterator
   and converts it to a int16_t as @ref variant_toInt16.\n
   The return pointer MUST be freed.\n

   @param map the variant map of interest
   @param key the name of the iterator

   @return
    - The converted value
 */
static inline int16_t variant_map_findInt16(const variant_map_t* map, const char* key) {
    return variant_int16(variant_map_findVariant(map, key));
}

/**
   @ingroup pcb_utils_variant_map
   @brief
   Get a variant map iterator data and converts it to a int32_t

   @details
   This function is provided for convenience. It is fetching a variant in a variant map iterator
   and converts it to a int32_t as @ref variant_toInt32.\n
   The return pointer MUST be freed.\n

   @param map the variant map of interest
   @param key the name of the iterator

   @return
    - The converted value
 */
static inline int32_t variant_map_findInt32(const variant_map_t* map, const char* key) {
    return variant_int32(variant_map_findVariant(map, key));
}

/**
   @ingroup pcb_utils_variant_map
   @brief
   Get a variant map iterator data and converts it to a int64_t

   @details
   This function is provided for convenience. It is fetching a variant in a variant map iterator
   and converts it to a int64_t as @ref variant_toInt64.\n
   The return pointer MUST be freed.\n

   @param map the variant map of interest
   @param key the name of the iterator

   @return
    - The converted value
 */
static inline int64_t variant_map_findInt64(const variant_map_t* map, const char* key) {
    return variant_int64(variant_map_findVariant(map, key));
}

/**
   @ingroup pcb_utils_variant_map
   @brief
   Get a variant map iterator data and converts it to a uint8_t

   @details
   This function is provided for convenience. It is fetching a variant in a variant map iterator
   and converts it to a uint8_t as @ref variant_toUInt8.\n
   The return pointer MUST be freed.\n

   @param map the variant map of interest
   @param key the name of the iterator

   @return
    - The converted value
 */
static inline uint8_t variant_map_findUInt8(const variant_map_t* map, const char* key) {
    return variant_uint8(variant_map_findVariant(map, key));
}

/**
   @ingroup pcb_utils_variant_map
   @brief
   Get a variant map iterator data and converts it to a uint16_t

   @details
   This function is provided for convenience. It is fetching a variant in a variant map iterator
   and converts it to a uint16_t as @ref variant_toUInt16.\n
   The return pointer MUST be freed.\n

   @param map the variant map of interest
   @param key the name of the iterator

   @return
    - The converted value
 */
static inline uint16_t variant_map_findUInt16(const variant_map_t* map, const char* key) {
    return variant_uint16(variant_map_findVariant(map, key));
}

/**
   @ingroup pcb_utils_variant_map
   @brief
   Get a variant map iterator data and converts it to a uint32_t

   @details
   This function is provided for convenience. It is fetching a variant in a variant map iterator
   and converts it to a uint32_t as @ref variant_toUInt32.\n
   The return pointer MUST be freed.\n

   @param map the variant map of interest
   @param key the name of the iterator

   @return
    - The converted value
 */
static inline uint32_t variant_map_findUInt32(const variant_map_t* map, const char* key) {
    return variant_uint32(variant_map_findVariant(map, key));
}

/**
   @ingroup pcb_utils_variant_map
   @brief
   Get a variant map iterator data and converts it to a uint64_t

   @details
   This function is provided for convenience. It is fetching a variant in a variant map iterator
   and converts it to a uint64_t as @ref variant_toUInt64.\n
   The return pointer MUST be freed.\n

   @param map the variant map of interest
   @param key the name of the iterator

   @return
    - The converted value
 */
static inline uint64_t variant_map_findUInt64(const variant_map_t* map, const char* key) {
    return variant_uint64(variant_map_findVariant(map, key));
}

/**
   @ingroup pcb_utils_variant_map
   @brief
   Get a variant map iterator data and converts it to a double

   @details
   This function is provided for convenience. It is fetching a variant in a variant map iterator
   and converts it to a double as @ref variant_toDouble.\n
   The return pointer MUST be freed.\n

   @param map the variant map of interest
   @param key the name of the iterator

   @return
    - The converted value
 */
static inline double variant_map_findDouble(const variant_map_t* map, const char* key) {
    return variant_double(variant_map_findVariant(map, key));
}

/**
   @ingroup pcb_utils_variant_map
   @brief
   Get a variant map iterator data and converts it to a char *

   @details
   This function is provided for convenience. It is fetching a variant in a variant map iterator
   and converts it to a char * as @ref variant_toChar.\n
   The return pointer MUST be freed.\n

   @param map the variant map of interest
   @param key the name of the iterator

   @return
    - The converted value
 */
static inline char* variant_map_findChar(const variant_map_t* map, const char* key) {
    return variant_char(variant_map_findVariant(map, key));
}

/**
   @ingroup pcb_utils_variant_map
   @brief
   Get a direct char * pointer to a variant in a variant map iterator

   @details
   This function is provided for convenience. It is fetching a variant in a variant map iterator
   and returns a direct pointer to its char * value as @ref variant_da_char.\n
   The return pointer MUST NOT be freed.\n

   @param map the variant map iterator of interest
   @param key the name of the iterator

   @return
    - The resulting char * pointer
    - NULL if the variant is invalid or not of char type
 */
static inline const char* variant_map_da_findChar(const variant_map_t* map, const char* key) {
    return variant_da_char(variant_map_findVariant(map, key));
}

/**
   @ingroup pcb_utils_variant_map
   @brief
   Get a variant map iterator data and converts it to a struct tm *

   @details
   This function is provided for convenience. It is fetching a variant in a variant map iterator
   and converts it to a struct tm * as @ref variant_toDateTime.\n
   The return pointer MUST be freed.\n

   @param map the variant map of interest
   @param key the name of the iterator

   @return
    - The converted value
 */
static inline struct tm* variant_map_findDateTime(const variant_map_t* map, const char* key) {
    return variant_dateTime(variant_map_findVariant(map, key));
}

/**
   @ingroup pcb_utils_variant_map
   @brief
   Get a variant map iterator data and converts it to a pcb_datetime_t*

   @details
   This function is provided for convenience. It is fetching a variant in a variant map iterator
   and converts it to a struct tm * as @ref variant_toDateTime.\n
   The return pointer MUST be freed.\n

   @param map the variant map of interest
   @param key the name of the iterator

   @return
    - The converted value
 */
static inline pcb_datetime_t* variant_map_findDateTimeExtended(const variant_map_t* map, const char* key) {
    return variant_dateTimeExtended(variant_map_findVariant(map, key));
}

/**
   @ingroup pcb_utils_variant_map
   @brief
   Get a direct struct tm * pointer to a variant in a variant map iterator

   @details
   This function is provided for convenience. It is fetching a variant in a variant map iterator
   and returns a direct pointer to its struct tm * value as @ref variant_da_dateTime.\n
   The return pointer MUST NOT be freed.\n

   @param map the variant map iterator of interest
   @param key the name of the iterator

   @return
    - The resulting struct tm * pointer
    - NULL if the variant is invalid or not of dateTime type
 */
static inline const struct tm* variant_map_da_findDateTime(const variant_map_t* map, const char* key) {
    return variant_da_dateTime(variant_map_findVariant(map, key));
}


/**
   @ingroup pcb_utils_variant_map
   @brief
   Get a direct pcb_datetime_t * pointer to a variant in a variant map iterator

   @details
   This function is provided for convenience. It is fetching a variant in a variant map iterator
   and returns a direct pointer to its pcb_datetime_t* value as @ref variant_da_dateTimeExtended.\n
   The return pointer MUST NOT be freed.\n

   @param map the variant map of interest
   @param key the name of the iterator

   @return
    - The resulting pcb_datetime_t* pointer
    - NULL if the variant is invalid or not of dateTime type
 */
static inline const pcb_datetime_t* variant_map_da_findDateTimeExtended(const variant_map_t* map, const char* key) {
    return variant_da_dateTimeExtended(variant_map_findVariant(map, key));
}

/**
   @ingroup pcb_utils_variant_map
   @brief
   Get a variant map iterator data and converts it to a string_t *

   @details
   This function is provided for convenience. It is fetching a variant in a variant map iterator
   and converts it to a string_t * as @ref variant_toString.\n
   The return pointer MUST be freed.\n

   @param map the variant map of interest
   @param key the name of the iterator

   @return
    - The converted value
 */
static inline string_t* variant_map_findString(const variant_map_t* map, const char* key) {
    return variant_string(variant_map_findVariant(map, key));
}

/**
   @ingroup pcb_utils_variant_map
   @brief
   Get a direct string_t * pointer to a variant in a variant map iterator

   @details
   This function is provided for convenience. It is fetching a variant in a variant map iterator
   and returns a direct pointer to its string_t * value as @ref variant_da_string.\n
   The return pointer MUST NOT be freed.\n

   @param map the variant map iterator of interest
   @param key the name of the iterator

   @return
    - The resulting string_t * pointer
    - NULL if the variant is invalid or not of string type
 */
static inline const string_t* variant_map_da_findString(const variant_map_t* map, const char* key) {
    return variant_da_string(variant_map_findVariant(map, key));
}

/**
   @ingroup pcb_utils_variant_map
   @brief
   Get a variant map iterator data and converts it to a variant_list_t *

   @details
   This function is provided for convenience. It is fetching a variant in a variant map iterator
   and converts it to a variant_list_t * as @ref variant_toList.\n
   The return pointer MUST be freed.\n

   @param map the variant map of interest
   @param key the name of the iterator

   @return
    - The converted value
 */
static inline variant_list_t* variant_map_findList(const variant_map_t* map, const char* key) {
    return variant_list(variant_map_findVariant(map, key));
}

/**
   @ingroup pcb_utils_variant_map
   @brief
   Get a direct variant_list_t * pointer to a variant in a variant map iterator

   @details
   This function is provided for convenience. It is fetching a variant in a variant map iterator
   and returns a direct pointer to its variant_list_t * value as @ref variant_da_list.\n
   The return pointer MUST NOT be freed.\n

   @param map the variant map iterator of interest
   @param key the name of the iterator

   @return
    - The resulting variant_list_t * pointer
    - NULL if the variant is invalid or not of list type
 */
static inline variant_list_t* variant_map_da_findList(const variant_map_t* map, const char* key) {
    return variant_da_list(variant_map_findVariant(map, key));
}

/**
   @ingroup pcb_utils_variant_map
   @brief
   Get a variant map iterator data and converts it to a variant_map_t *

   @details
   This function is provided for convenience. It is fetching a variant in a variant map iterator
   and converts it to a variant_map_t * as @ref variant_toMap.\n
   The return pointer MUST be freed.\n

   @param map the variant map of interest
   @param key the name of the iterator

   @return
    - The converted value
 */
static inline variant_map_t* variant_map_findMap(const variant_map_t* map, const char* key) {
    return variant_map(variant_map_findVariant(map, key));
}

/**
   @ingroup pcb_utils_variant_map
   @brief
   Get a direct variant_map_t * pointer to a variant in a variant map iterator

   @details
   This function is provided for convenience. It is fetching a variant in a variant map iterator
   and returns a direct pointer to its variant_map_t * value as @ref variant_da_map.\n
   The return pointer MUST NOT be freed.\n

   @param map the variant map iterator of interest
   @param key the name of the iterator

   @return
    - The resulting variant_map_t * pointer
    - NULL if the variant is invalid or not of map type
 */
static inline variant_map_t* variant_map_da_findMap(const variant_map_t* map, const char* key) {
    return variant_da_map(variant_map_findVariant(map, key));
}

/**
   @ingroup pcb_utils_variant_map
   @brief
   Get a variant map iterator data and converts it to a int

   @details
   This function is provided for convenience. It is fetching a variant in a variant map iterator
   and converts it to a int as @ref variant_toFd.\n
   The return pointer MUST be freed.\n

   @param map the variant map of interest
   @param key the name of the iterator

   @return
    - The converted value
 */
static inline int variant_map_findFd(const variant_map_t* map, const char* key) {
    return variant_fd(variant_map_findVariant(map, key));
}

/**
   @ingroup pcb_utils_variant_map
   @brief
   Get a variant map iterator data and converts it to a void *

   @details
   This function is provided for convenience. It is fetching a variant in a variant map iterator
   and converts it to a void * as @ref variant_toByteArray.\n
   The return pointer MUST be freed.\n

   @param map the variant map of interest
   @param key the name of the iterator
   @param size pointer to an integer where the size of the byte array can be set

   @return
    - The converted value
 */
static inline void* variant_map_findByteArray(const variant_map_t* map, const char* key, uint32_t* size) {
    return variant_byteArray(variant_map_findVariant(map, key), size);
}

/**
   @ingroup pcb_utils_variant_map
   @brief
   Get a direct void * pointer to a variant in a variant map iterator

   @details
   This function is provided for convenience. It is fetching a variant in a variant map iterator
   and returns a direct pointer to its void * value as @ref variant_da_byteArray.\n
   The return pointer MUST NOT be freed.\n

   @param map the variant map iterator of interest
   @param key the name of the iterator
   @param size pointer to an integer where the size of the byte array can be set

   @return
    - The resulting void * pointer
    - NULL if the variant is invalid or not of byteArray type
 */
static inline const void* variant_map_da_findByteArray(const variant_map_t* map, const char* key, uint32_t* size) {
    return variant_da_byteArray(variant_map_findVariant(map, key), size);
}

/**
   @ingroup pcb_utils_variant_map
   @brief
   Get a variant map iterator data and converts it to a string_list_t *

   @details
   This function is provided for convenience. It is fetching a variant in a variant map iterator
   and converts it to a string_list_t * as @ref variant_toStringList.\n
   The return pointer MUST be freed.\n

   @param map the variant map of interest
   @param key the name of the iterator
   @param separator separator to be used to split the data

   @return
    - The converted value
 */
static inline string_list_t* variant_map_findStringList(const variant_map_t* map, const char* key, const char* separator) {
    return variant_stringList(variant_map_findVariant(map, key), separator);
}

#ifdef __cplusplus
}
#endif
#endif
