/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef PCBU_LINKED_LIST_H
#define PCBU_LINKED_LIST_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>

/**
   @ingroup pcb_utils_containers
   @file
   @brief
   Header file with llist type definitions and llist public functions
 */

/**
   @ingroup pcb_utils_containers
   @defgroup pcb_utils_linked_list Doubly Linked List
   @{

   @brief
   Doubly linked list implementation

   @details
   The list has two pointers, one to the first node (called head) and one to the last node (called tail)
   Each element in the list has two pointers, one to the previous element and one to the next.
   @image html 489px-Doubly-linked-list.svg.png "doubly linked list"
   If the linked list is empty, the head and tail pointers will be NULL. The previous pointer of the first node in the
   linked list will always be NULL, the next pointer of the last node in the linked list will always be NULL.

   @section pcb_utils_llist_create_items Adding data to a list
   The elements in the list do not contain a pointer to data, they are iterators. To create a list with elements
   containing data, a structure has to be defined that contains a llist_iterator_t\n
   @verbatim
   typedef struct _int32_list_item {
    uint32_t data;
    llist_iterator_t it;
   } int32_list_item_t;
   @endverbatim
   In the above example a list item is defined that contains a 32 bit signed integer. Before this can be used you
   have to initialize the iterator part of the structure using @ref llist_iterator_initialize function.
   After doing that you can add an instance of this structure to any linked list.
   @verbatim
   llist_t mylist;
   llist_initialize(&mylist);

   int32_list_item_t *element = (int32_list_item_t *)calloc(1,sizeof(int32_list_item_t));
   llist_iterator_initialize(&element->it);
   element->data = 105;
   llist_append(&element->it);
   @endverbatim

   @section pcb_utils_llist_get_items Retreiving data from a list
   To retrieve data from a list, it is needed to get the correct iterator first. This can be done by using any
   of the following functions @ref llist_first, @ref llist_last, @ref llist_iterator_next, @ref llist_iterator_prev
   or even on a position in the list @ref llist_at.\n
   To retrieve the data from the iterator, use the macro @ref llist_item_data.
   @verbatim
   llist_iterator_t *some_it = llist_first(&mylist);
   int32_list_item_t *data_element = llist_item_data(some_it,int32_list_item_t,it);
   @endverbatim

   @section pcb_utils_llist_traversing Traversing a list
   Some convieniance macros are provide to iterate a list:@ref llist_for_each and @ref llist_for_each_reverse.
   @warning
   While using these macros do not add elements to the list or delete elements from the list.

   @section pcb_utils_llist_considerations Some warnings and limitations
   @li Always initialize a linked list and iterators using @ref llist_initialize and @ref llist_iterator_initialize
   functions before using them.
   @li Never free an iterator directly, always free the memory allocated for the structure containing the iterator.
   @li When an iterator is added to a linked list, ownership of the pointer is assumed by the linked list. Freeing
   the iterator (or the iterator's container), will corrupt the linked list. Always make sure the iterator is not
   in a linked list anymore before freeing up the memory taken by the iterator.
   @li Never pass a llist_t or llist_iterator_t by value.
   @li Do not create lists containing different types of data elements, although this is possible, it will hard (or even
   impossible) to find out the iterator's container type. The type is needed to convert the iterator back to its container.
 */

/**
   @brief
   Definition of a helper macro for forward iteration through the linked list (head->tail)

   @details
   This <b>helper macro</b> iterates forwards through a <b>linked list</b> starting from the head

   @warning
   don&rsquo;t <b>modify</b> the linked list during the iteration!

   @param item a pointer variable to the list items
   @param list the list
 */
#define llist_for_each(item, list) \
    for(item = llist_first(list); item; item = llist_iterator_next(item))

//---------------------------------------------------------------------------------------------
/**
   @brief
   Definition of a helper macro for backward iteration through the linked list (tail->head)

   @details
   This <b>helper macro</b> iterates backwards through a <b>linked list</b> starting from the tail

   @warning
   don&rsquo;t <b>modify</b> the linked list during the iteration!

   @param item a pointer variable to the list items
   @param list the list
 */
#define llist_for_each_reverse(item, list) \
    for(item = llist_last(list); item; item = llist_iterator_prev(item))

// data macro
//---------------------------------------------------------------------------------------------
/**
   @brief
   Definition of a helper macro for getting a pointer to the real data structure.

   @details
   This <b>macro</b> calculates the address of the containing data structure.

   @warning
   If the wrong type is specified, the resulting pointer could be invalid.

   @param item a pointer to the linked list item for which we want to calculate the pointer to the containing structure
   @param type the type to which the pointer has to be casted
   @param member the name of the data member containing the list pointer
 */
#define llist_item_data(item, type, member) \
    (type*) (((char*) item) - offsetof(type, member))

/**
   @brief
   Definition of a helper macro for getting a pointer to the real data structure.

   @details
   This <b>macro</b> calculates the address of the containing data structure.

   @warning
   If the wrong type is specified, the resulting pointer could be invalid.

   @param item a pointer to the linked list item for which we want to calculate the pointer to the containing structure
   @param type the type to which the pointer has to be casted
   @param member the name of the data member containing the list pointer
 */
#define llist_iterator_data(item, type, member) \
    (type*) (((char*) item) - offsetof(type, member))

//---------------------------------------------------------------------------------------------
/**
   @brief
   Linked list iterator

   @details
   A linked list iterator
 */
typedef struct _llist_iterator {
    struct _llist_iterator* prev; /**< Pointer to the previous element. */
    struct _llist_iterator* next; /**< Pointer to the next element. */
    struct _llist* list;          /**< Pointer to the linked list to which this element belongs (NULL if not in any list) */
} llist_iterator_t;

//---------------------------------------------------------------------------------------------
/**
   @brief
   Linked list

   @details
   A linked list container
 */
typedef struct _llist {
    struct _llist_iterator* head; /**< Pointer to the head element. */
    struct _llist_iterator* tail; /**< Pointer to the tail element. */
} llist_t;

/**
   @}
 */

// initializer functions
bool llist_initialize(llist_t* list);
bool llist_iterator_initialize(llist_iterator_t* it);

// cleanup functions
void llist_cleanup(llist_t* list);

llist_iterator_t* llist_first(const llist_t* list);
llist_iterator_t* llist_last(const llist_t* list);
llist_iterator_t* llist_at(const llist_t* list, unsigned int index);
llist_iterator_t* llist_iterator_next(const llist_iterator_t* it);
llist_iterator_t* llist_iterator_prev(const llist_iterator_t* it);

// insertion functions
bool llist_insertBefore(llist_t* list, llist_iterator_t* reference, llist_iterator_t* insert);
bool llist_insertAfter(llist_t* list, llist_iterator_t* reference, llist_iterator_t* insert);
bool llist_append(llist_t* list, llist_iterator_t* insert);
bool llist_prepend(llist_t* list, llist_iterator_t* insert);
bool llist_insertAt(llist_t* list, unsigned int index, llist_iterator_t* insert);

// removal functions
llist_iterator_t* llist_iterator_take(llist_iterator_t* it);
llist_iterator_t* llist_takeFirst(llist_t* list);
llist_iterator_t* llist_takeLast(llist_t* list);

// property functions
unsigned int llist_size(const llist_t* list);
bool llist_isEmpty(const llist_t* list);

// sort functions
typedef int (* llist_qsort_compar)(const llist_iterator_t*, const llist_iterator_t*, void* userdata);
bool llist_qsort(llist_t* list, llist_qsort_compar compar, bool reverse, void* userdata);

#ifdef __cplusplus
}
#endif

#endif
