/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(PCB_VARIANT_SETS_H)
#define PCB_VARIANT_SETS_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <pcb/utils/variant.h>
#include <pcb/utils/variant_map.h>
#include <pcb/utils/variant_list.h>

/*
 * The "sets" API exposes the "set theory" to variants.
 * When dealing with maps or lists, the API has direct counterparts for each.
 *
 * A union of "a" and "b" will place all items of "b" in "a". See it as a merge.
 * When an item in "b" is not yet available in "a", it is copied.
 * When the same item is available in "a" and "b", the boolean "update" will
 * indicate whether to update the item in "b" with the value of the item in "a".
 *
 * The intersection of "a" and "b" will keep only those items in "a" which are
 * available in both "a" and "b". In general, it will remove the items from "a"
 * which are not found in "b".
 *
 * The relative complement of "b" in "a" will keep only those items in "a" which are
 * not available in "b". In general, it will remove the items from "a"
 * which are also found in "b".
 *
 * The above operations only succeed when both variants are of type map or list.
 * When dealing with maps, items are normally identified by their "key".
 * When dealing with lists, items can normally not be identified as there is no such key.
 *
 * For both maps and lists, a special case exists.
 * They can contain a "Primary Key name indicator".
 * For a map, this indicator is a map item with the key <VARIANT_SETS_PK_INDICATOR>, whose
 * value must be a string. Its value is the "Primary Key name".
 * For a list, this indicator must be the first list item, which must be a string whose
 * value starts with "<VARIANT_SETS_PK_INDICATOR>:", followed by the "Primary Key name".
 *
 * When operating upon maps or lists, either both "a" and "b" must _not_ have such indicator,
 * or both "a" and "b" must have such indicator with both the same "Primary Key name".
 * Otherwise, failure occurs.
 *
 * In the case a common Primary Key name is given, then items in the map or list can be identified
 * using the name. The items must be of type map, and they must contain a string item with the key
 * equal to the Primary Key name. The item's value is the "Primary Key" of the item used
 * for identification.
 * When dealing with maps, the item's key will be ignored if the item is identified using
 * a primary key. When copying these items from "b" to "a" during a union, the new item's
 * key will be a stringified unsigned integer, unique in "a".
 *
 * Examples:
 * map {
 *   __VAR_SETS_PK_IND__ : "MyPrimaryKey",
 *   1 : {
 *     MyPrimaryKey : "Key1",
 *     Enable       : true,
 *     Parameter1   : "Value1"
 *   },
 *   2 : {
 *     MyPrimaryKey : "Key4",
 *     Enable       : true,
 *     Parameter1   : "Value4"
 *   }
 * }
 *
 * list [
 *   "__VAR_SETS_PK_IND__:MyPrimaryKey",
 *   {
 *     MyPrimaryKey : "Key1",
 *     Enable       : true,
 *     Parameter1   : "Value1"
 *   },
 *   {
 *     MyPrimaryKey : "Key4",
 *     Enable       : true,
 *     Parameter1   : "Value4"
 *   }
 * ]
 *
 * By enabling "descend" in the appropriate functions, the function will descend into
 * the variant if it's a map or array, and it will perform the same operation upon that child.
 *
 * By enabling "verifyOnly" in the appropriate functions, the function will perform a
 * verification of the data only. They will not update "a", but they will throw an error
 * in case such an update should have been taken.
 */

/* Primary Key name indicator: must be unique to not collide with other names */
#define VARIANT_SETS_PK_INDICATOR       "__VAR_SETS_PK_IND__"
/* Multi Instance indicator: must be unique to not collide with other names */
#define VARIANT_SETS_MI_INDICATOR       "__VAR_SETS_MI_IND__"
/* Multi Instance Element value name: must be unique to not collide with other names */
#define VARIANT_SETS_VALUE              "__VALUE__"


bool variant_map_setPKName(variant_map_t* map, const char* value);
bool variant_map_clearPKName(variant_map_t* map);
bool variant_map_hasPKName(const variant_map_t* map);
const char* variant_map_getPKName(const variant_map_t* map);
bool variant_map_union(variant_map_t* a, const variant_map_t* b, bool update, bool descend);
bool variant_map_intersection(variant_map_t* a, const variant_map_t* b, bool descend, bool verifyOnly);
bool variant_map_relComplement(variant_map_t* a, const variant_map_t* b, bool descend, bool verifyOnly);

bool variant_list_setPKName(variant_list_t* list, const char* value);
bool variant_list_clearPKName(variant_list_t* list);
bool variant_list_hasPKName(const variant_list_t* list);
const char* variant_list_getPKName(const variant_list_t* list);
bool variant_list_union(variant_list_t* a, const variant_list_t* b, bool update, bool descend);
bool variant_list_intersection(variant_list_t* a, const variant_list_t* b, bool descend, bool verifyOnly);
bool variant_list_relComplement(variant_list_t* a, const variant_list_t* b, bool descend, bool verifyOnly);

bool variant_sets_setPKName(variant_t* var, const char* value);
bool variant_sets_clearPKName(variant_t* var);
bool variant_sets_hasPKName(const variant_t* var);
const char* variant_sets_getPKName(const variant_t* var);
bool variant_sets_union(variant_t* a, const variant_t* b, bool update, bool descend);
bool variant_sets_intersection(variant_t* a, const variant_t* b, bool descend, bool verifyOnly);
bool variant_sets_relComplement(variant_t* a, const variant_t* b, bool descend, bool verifyOnly);

#ifdef __cplusplus
}
#endif

#endif // PCB_VARIANT_SETS_H

