/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(PCB_STRING_LIST_H)
#define PCB_STRING_LIST_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <stddef.h>

#include <pcb/utils/linked_list.h>
#include <pcb/utils/string.h>

/**
   @ingroup pcb_utils_containers
   @file
   @brief
   Header file with string list type definitions and public string list functions
 */

/**
   @ingroup pcb_utils_containers
   @defgroup pcb_utils_string_list String List
   @{

   @brief
   String list handling functions.
 */

// iterator macros
/**
   @brief
   Definition of a helper macro for forward iteration through the string linked list (head->tail)

   @details
   This <b>helper macro</b> iterates forward through a <b>string linked list</b> starting from the head\n
   <b>Warning</b>: don&rsquo;t <b>modify</b> the string linked list during the iteration!

   @param it a pointer variable to the list items
   @param list the list
 */
#define string_list_for_each(it, list) \
    for(it = string_list_first(list); it; it = string_list_iterator_next(it))
#if __STDC_VERSION__ >= 199901L
/**
   @brief
   Definition of a helper macro for forward iteration through the string linked list (head->tail)

   @details
   This <b>helper macro</b> iterates forward through a <b>string linked list</b> starting from the head\n
   <b>Warning</b>: don&rsquo;t <b>modify</b> the string linked list during the iteration!

   @param it the name of the variable to declare for the list items
   @param list the list
 */
#define string_list_for_each_declare(it, list) \
    for(string_list_iterator_t* it = string_list_first(list); it; it = string_list_iterator_next(it))
#endif

/**
   @brief
   Definition of a helper macro for forward iteration through the string linked list (head->tail)

   @details
   This <b>helper macro</b> iterates forward through a <b>string linked list</b> starting from the head\n
   <b>Warning</b>: don&rsquo;t <b>modify</b> the string linked list during the iteration!

   @param it a pointer variable to the list strings
   @param list the list
 */
#define string_list_for_each_string(it, list) \
    for(it = string_list_firstString(list); \
        it; it = string_list_iterator_nextString(it))

#if __STDC_VERSION__ >= 199901L
/**
   @brief
   Definition of a helper macro for forward iteration through the string linked list (head->tail)

   @details
   This <b>helper macro</b> iterates forward through a <b>string linked list</b> starting from the head\n
   <b>Warning</b>: don&rsquo;t <b>modify</b> the string linked list during the iteration!

   @param it name of a pointer variable to declare to the list strings
   @param list the list
 */
#define string_list_for_each_declare_string(it, list) \
    for(const string_t* it = string_list_firstString(list); \
        it; it = string_list_iterator_nextString(it))
#endif

/**
   @brief
   Definition of a helper macro for forward iteration through the string linked list (head->tail)

   @details
   This <b>helper macro</b> iterates forward through a <b>string linked list</b> starting from the head\n
   <b>Warning</b>: don&rsquo;t <b>modify</b> the string linked list during the iteration!

   @param it a pointer variable to the list chars
   @param list the list
 */
#define string_list_for_each_char(it, list) \
    for(it = string_list_firstChar(list);                               \
        it; it = string_list_iterator_nextChar(it))

#if __STDC_VERSION__ >= 199901L
/**
   @brief
   Definition of a helper macro for forward iteration through the string linked list (head->tail)

   @details
   This <b>helper macro</b> iterates forward through a <b>string linked list</b> starting from the head\n
   <b>Warning</b>: don&rsquo;t <b>modify</b> the string linked list during the iteration!

   @param it name of a pointer variable to declare the list chars
   @param list the list
 */
#define string_list_for_each_declare_char(it, list) \
    for(char** it = string_list_firstChar(list);                               \
        it; it = string_list_iterator_nextChar(it))

#endif

/**
   @brief
   Definition of a helper macro for backward iteration through the string linked list (tail->head)

   @details
   This <b>helper macro</b> iterates backwards through a <b>string linked list</b> starting from the tail\n
   <b>Warning</b>: don&rsquo;t <b>modify</b> the string linked list during the iteration!

   @param it a pointer variable to the list items
   @param list the list
 */
#define string_list_for_each_reverse(it, list) \
    for(it = string_list_last(list); it; it = string_list_iterator_prev(it))

/**
   @brief
   A string linked list iterator

   @details
   A string linked list iterator
 */
typedef struct _string_list_item {
    llist_iterator_t llist_it; /**< the linked list. */
    string_t string;           /**< The string data itme. */
} string_list_iterator_t;

/**
   @brief
   The string split behavior

   @details
   The types of behavior that a string split function can have.
 */
typedef enum _string_splitBehavior {
    strlist_keep_empty_parts = 0,              /**< Keep the empty parts of a string. */
    KeepEmptyParts = strlist_keep_empty_parts, /**< @deprecated Use @ref strlist_keep_empty_parts */
    strlist_skip_empty_parts = 1,              /**< Skip the empty parts of a string. */
    SkipEmptyParts = strlist_skip_empty_parts, /**< @deprecated Use @ref strlist_skip_empty_parts. */
} string_splitBehavior_t;

/**
   @brief
   The string list type

   @details
   A type to specify that the list contains string_t.
 */
typedef llist_t string_list_t;

/**
   @}
 */

// initializer functions
bool string_list_initialize(string_list_t* strlist);
string_list_iterator_t* string_list_iterator_create(const string_t* string);
string_list_iterator_t* string_list_iterator_createFromChar(const char* string);

// cleanup functions
void string_list_cleanup(string_list_t* strlist);
void string_list_clear(string_list_t* strlist);
void string_list_iterator_destroy(string_list_iterator_t* it);

unsigned int string_list_splitChar(string_list_t* list, const char* string, const char* sep, string_splitBehavior_t behavior, string_case_t cs);
unsigned int string_list_split(string_list_t* list, const string_t* string, const char* sep, string_splitBehavior_t behavior, string_case_t cs);
unsigned int string_join(string_t* dest, const string_list_t* strlist, const char* sep);

// data function
string_t* string_list_iterator_data(const string_list_iterator_t* it);
bool string_list_contains(string_list_t* strList, const string_t* string);
bool string_list_containsChar(const string_list_t* strList, const char* string);

string_list_iterator_t* string_list_findChar(const string_list_t* strList, const char* string);

// accessor functions
string_list_iterator_t* string_list_first(const string_list_t* list);
string_list_iterator_t* string_list_last(const string_list_t* list);
string_list_iterator_t* string_list_at(const string_list_t* list, unsigned int index);
string_list_iterator_t* string_list_iterator_next(const string_list_iterator_t* it);
string_list_iterator_t* string_list_iterator_prev(const string_list_iterator_t* it);

// string accessor functions
static inline string_t* string_list_firstString(const string_list_t* list) {
    string_list_iterator_t* first = string_list_first(list);
    if(first == NULL) {
        return NULL;
    }

    return string_list_iterator_data(first);
}

static inline string_t* string_list_iterator_nextString(string_t* it) {
    if(it == NULL) {
        return NULL;
    }

    string_list_iterator_t* prev = llist_item_data(it, string_list_iterator_t, string);
    string_list_iterator_t* next = string_list_iterator_next(prev);
    if(next == NULL) {
        return NULL;
    }

    return string_list_iterator_data(next);
}

// char accessor functions
static inline char** string_list_firstChar(const string_list_t* list) {
    string_t* str = string_list_firstString(list);
    if(str == NULL) {
        return NULL;
    }

    return &str->buffer;
}


static inline char** string_list_iterator_nextChar(char** it) {
    if(it == NULL) {
        return NULL;
    }
    string_t* prev = llist_item_data(it, string_t, buffer);
    string_t* next = string_list_iterator_nextString(prev);
    if(next == NULL) {
        return NULL;
    }

    return &next->buffer;
}

// insertion functions
bool string_list_append(string_list_t* list, string_list_iterator_t* insert);
bool string_list_prepend(string_list_t* list, string_list_iterator_t* insert);
bool string_list_insertAt(string_list_t* list, unsigned int index, string_list_iterator_t* insert);

static inline bool string_list_appendChar(string_list_t* list, const char* insert) {
    string_list_iterator_t* it = string_list_iterator_createFromChar(insert);
    if(!it) {
        return false;
    }
    return string_list_append(list, it);
}

static inline bool string_list_prependChar(string_list_t* list, const char* insert) {
    string_list_iterator_t* it = string_list_iterator_createFromChar(insert);
    if(!it) {
        return false;
    }
    return string_list_prepend(list, it);
}

// removal functions
string_list_iterator_t* string_list_iterator_take(string_list_iterator_t* it);
string_list_iterator_t* string_list_takeFirst(string_list_t* list);
string_list_iterator_t* string_list_takeLast(string_list_t* list);

// property functions
unsigned int string_list_size(string_list_t* list);
bool string_list_isEmpty(const string_list_t* list);

// sort functions
typedef int (* string_list_qsort_compar)(const string_t*, const string_t*, void* userdata);
int string_list_qsort_strcmp(const string_t* a, const string_t* b, void* userdata);
bool string_list_qsort(string_list_t* list, string_list_qsort_compar compar, bool reverse, void* userdata);

#ifdef __cplusplus
}
#endif

#endif // PCB_STRING_LIST_H
