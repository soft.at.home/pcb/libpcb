/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(PCB_PEER_H)
#define PCB_PEER_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <pcb/utils/linked_list.h>
#include <pcb/utils/connection.h>

#ifdef OPEN_SSL_SUPPORT
#include <openssl/bio.h>
#include <openssl/ssl.h>
#endif

/**
   @ingroup pcb_utils_socket_events
   @file
   @brief
   Header file with peer functions
 */

/**
   @ingroup pcb_utils_socket_events
   @defgroup pcb_socket_layer_peer Peer
   @{

   @brief
   File descriptor encapsulation

   @details
   A peers main purpose is to manage all io operations. The read and write operations can be done using event handlers.

   @section pcb_sl_peer_read Reading From a Peer
   When you want to read something from a peer you need to install a read handler. To install the read handler call @ref peer_setEventHandler.
   This will add a callback function to the peer. The read handler will be called when data is available for read on the underlying file descriptor.

   @section pcb_sl_peer_write Writing To a Peer
   Writing to a peer can be done in two different ways:
    - using buffered io
    - using direct io

   When using buffered io all data that needs to be written to the underlying file descriptor will be buffered first. When
   the filedescriptor is ready to accept bytes, these are written to the file descriptor.
   When using direct io all data is put directly on the underlying file descriptor.

   @section pcb_sl_peer_streams Peer Streams
   Altough you could use the underlying file descriptor directly and use that one to read and write, it is encouraged to use the streams provided.
   When using the streams (one for input and one for output) all kind of extras like SSL encryption is handled from within the library.
   You will not need any changes to the code when changing from non-encrypted data transport to encrypted data transport.

   To get the streams use the finctions @ref peer_inStream or @ref peer_outStream

   <b>Caution</b>:
   Never call fclose on one of the streams. When closing the streams using fclose, the underlying file descriptor is closed as well.
   This will cause unpredictable behavior from the PCB functions.

   @section pcb_sl_peer_sockets Peer Sockets
   When the peer info structure encapsulates a file discriptor for a socket, created with one of the @ref connection_connectToIPC,
   @ref connection_connectToTCP or @ref connection_connect, the information to setup the connection is stored within
   the peer_info_t structure. If at any point in time the connection breaks, you can try to setup the connection again
   using @ref peer_reconnect
 */

#define peer_handler_default      0x00000000 /**< Default */
#define peer_handler_read_always  0x00000001 /**< Keep the read handler, after each read */
#define peer_handler_write_always 0x00000002 /**< Keep the write handler, after each write.\n <b>Caution</b>: When there is nothing to write anymore, remove the write handler */
#define peer_close_fd_after_send  0x00000004 /**< Close an transported fd after it is send */

#define peer_cap_direct_fd_access 0x00000001 /**< The fd of the peer can be used for direct read/write access */
#define peer_cap_buffered_io      0x00000002 /**< Writing to the stream is bufferd, the stream will be emptied as soon as the fd is available for write */
#define peer_cap_ssl_enabled      0x00000004 /**< The fd is SSL/TLS enabled */
#define peer_cap_transport_fd     0x00000008 /**< The peer can transport file descriptors */

/**
   @brief
   Structure definition that is used to hold a pointer to an handler and can be put in a list.

   @details
   Some kind of handlers can be added multiple times to a peer (eg close handler). This structure is used
   to store the function pointer and add the handler to the list.
 */
typedef struct _peer_handler_list {
    peer_event_handler_t handler_function; /**< Pointer to the event handler*/
    llist_iterator_t it;                   /**< Used to store in a list*/
} peer_handler_list_t;

/**
   @brief
   Structure that can hold a file descriptor that has been transported o needs to be transported.

   @details
   Structure that can hold a file descriptor that has been transported o needs to be transported.
 */
typedef struct _peer_transport_fd {
    int fd;                                /**< The file descriptor*/
    llist_iterator_t it;                   /**< Used to strore in a list*/
} peer_transport_fd_t;

/**
   @}
 */

bool peer_initialize(peer_info_t* peer);
// close and remove
bool peer_destroy(peer_info_t* peer);
void peer_delete(peer_info_t* peer);
// close
bool peer_close(peer_info_t* peer);

// reconnect
bool peer_reconnect(peer_info_t* peer);

/**
   @ingroup pcb_socket_layer_peer
   @brief Get the peer socket file descriptor.
   @details This function returns the peer file descriptor.\n
   @param peer The peer structure you are interested in
   @return
    - the peer file descriptor
    - -1 if an error occurred, pcb_error contains more info about the error
 */
static inline int peer_getFd(peer_info_t* peer) {
    if(!peer || (peer->state == peer_state_deleted)) {
        pcb_error = pcb_error_invalid_parameter;
        return -1;
    }

    pcb_error = pcb_ok;
    return peer->socketfd;
}

bool peer_setFd(peer_info_t* peer, int fd);

bool peer_needRead(peer_info_t* peer);
bool peer_needWrite(peer_info_t* peer);

bool peer_handleRead(peer_info_t* peer);
bool peer_handleWrite(peer_info_t* peer);

size_t peer_fwrite(peer_info_t* peer, const void* data, size_t size, size_t nmemb);

// attributes
bool peer_isTCPSocket(peer_info_t* peer);
bool peer_isIPCSocket(peer_info_t* peer);

bool peer_isClientConnection(peer_info_t* peer);
bool peer_isServerConnection(peer_info_t* peer);
bool peer_isCustomFd(peer_info_t* peer);
static inline bool peer_isPair(peer_info_t* peer) {
    return peer->type == peer_type_pair;
}

bool peer_isListenSocket(peer_info_t* peer);
bool peer_isConnected(peer_info_t* peer);
bool peer_isConnecting(peer_info_t* peer);
bool peer_isDeleted(peer_info_t* peer);

uint32_t peer_capabilities(peer_info_t* peer);

connection_info_t* peer_connection(peer_info_t* peer);
peer_info_t* peer_listenSocket(peer_info_t* peer);

bool peer_flush(peer_info_t* peer);

// get a file stream
FILE* peer_outStream(peer_info_t* peer, peer_stream_type_t streamtype);
FILE* peer_inStream(peer_info_t* peer);
bool peer_setStreamType(peer_info_t* peer, peer_stream_type_t streamtype);
size_t peer_bytesQueued(peer_info_t* peer);

ssize_t peer_peek(peer_info_t* peer, char* buffer, uint32_t size);

// event handling
bool peer_setEventHandler(peer_info_t* peer, peer_event_type_t event, uint32_t handlerFlags, peer_event_handler_t handlerfn);
bool peer_addCloseHandler(peer_info_t* peer, peer_event_handler_t handlerfn);
bool peer_addAcceptHandler(peer_info_t* peer, peer_event_handler_t handlerfn);
bool peer_removeCloseHandler(peer_info_t* peer, peer_event_handler_t handlerfn);
bool peer_removeAcceptHandler(peer_info_t* peer, peer_event_handler_t handlerfn);

// file descriptor transport
bool peer_attachFd(peer_info_t* peer, int fd);
int peer_fetchFd(peer_info_t* peer);
bool peer_hasReceivedFd(peer_info_t* peer);
bool peer_setCloseFd(peer_info_t* peer, bool close);

// ssl-enabled functions
#ifdef OPEN_SSL_SUPPORT
const char* peer_getUsername(peer_info_t* peer);
bool peer_setUsername(peer_info_t* peer, const char* username);
#endif

// user data
/**
   @ingroup pcb_socket_layer_peer
   @brief
   Set user data in the peer_info_t structure
   @details
   Set some user data to the peer_info_t structure. The caller of this function must free the user data if needed.
   If memory was allocated for the user data, the library will not free it.
   If user data was already set, this function will overwrite it.
   @param peer Pointer to the peer_info_t structure
   @param data User specified data
   @return
    - true when successful
    - false an error occured
 */
static inline bool peer_setUserData(peer_info_t* peer, void* data) {
    if(!peer || (peer->state == peer_state_deleted)) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    peer->userData = data;
    return true;
}

/**
   @ingroup pcb_socket_layer_peer
   @brief
   Get user data from the peer_info_t structure
   @details
   This function will return whatever was set using the @ref peer_setUserData function
   @param peer Pointer to the peer_info_t structure
   @return
    - The user data.
 */
static inline void* peer_getUserData(peer_info_t* peer) {
    if(!peer) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    return peer->userData;
}

#ifdef __cplusplus
}
#endif
#endif
