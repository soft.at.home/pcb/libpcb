/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef __URI_H__
#define __URI_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <stddef.h>
#include <pcb/common/error.h>

/**
   @ingroup pcb_utils
   @file
   @brief
   Header file with uri type definitions and uri functions
 */

/**
   @ingroup pcb_utils
   @defgroup pcb_utils_uri URI Parsing
   @{

   @details
   This utility provides a simple uti parser. It can parse URIs of the following format.
   @image html url.png

   Also a path can be added to the uri and has then the following from.
   @image html url-path.png

   Parsing an uri string can be done by calling @ref uri_parse. This function will return
   a pointer to a @ref uri_t structure. You need to free that structure again by calling
   @ref uri_destroy.

   The structure definition is public available and is directly accesible. But this way of working
   is not encouraged, use the accessor functions instead.

 */

/**
   @brief
   URI structure definition

   @details
   This structure defines a parsed uri.
 */
typedef struct _uri {
    char* scheme;         /**< The uri scheme */
    char* host;           /**< The uri host */
    char* port;           /**< The uri port */
    char* user;           /**< The uri user */
    char* password;       /**< The uri password */
    char* path;           /**< The uri path */
    int parametersLength; /**< Length of the string containing the parameters */
    char* parameters;     /**< The parameters */
} uri_t;

/**
   @}
 */

uri_t* uri_parse(const char* URI);
void uri_destroy(uri_t* URI);

/**
   @ingroup pcb_utils_uri
   @brief Get the uri scheme
   @details Get the uri scheme
   @param URI pointer to the uri_t structure
   @return The string representing the scheme. Do not free the returned string, use @ref uri_destroy.
 */
static inline const char* uri_getScheme(const uri_t* URI) {
    return URI ? URI->scheme : NULL;
}

/**
   @ingroup pcb_utils_uri
   @brief Get the uri host
   @details Get the uri host
   @param URI pointer to the uri_t structure
   @return The string representing the host. Do not free the returned string, use @ref uri_destroy.
 */
static inline const char* uri_getHost(const uri_t* URI) {
    return URI ? URI->host : NULL;
}

/**
   @ingroup pcb_utils_uri
   @brief Get the uri port
   @details Get the uri port
   @param URI pointer to the uri_t structure
   @return The string representing the port. Do not free the returned string, use @ref uri_destroy.
 */
static inline const char* uri_getPort(const uri_t* URI) {
    return URI ? URI->port : NULL;
}

/**
   @ingroup pcb_utils_uri
   @brief Get the uri user
   @details Get the uri user
   @param URI pointer to the uri_t structure
   @return The string representing the user. Do not free the returned string, use @ref uri_destroy.
 */
static inline const char* uri_getUser(const uri_t* URI) {
    return URI ? URI->user : NULL;
}

/**
   @ingroup pcb_utils_uri
   @brief Get the uri password
   @details Get the uri password
   @param URI pointer to the uri_t structure
   @return The string representing the password. Do not free the returned string, use @ref uri_destroy.
 */
static inline const char* uri_getPassword(const uri_t* URI) {
    return URI ? URI->password : NULL;
}

/**
   @ingroup pcb_utils_uri
   @brief Get the uri path
   @details Get the uri path
   @param URI pointer to the uri_t structure
   @return The string representing the path. Do not free the returned string, use @ref uri_destroy.
 */
static inline const char* uri_getPath(const uri_t* URI) {
    return URI ? URI->path : NULL;
}

const char* uri_getParameter(const uri_t* URI, const char* name);
char* uri_string(const uri_t* URI);

#ifdef __cplusplus
}
#endif
#endif
