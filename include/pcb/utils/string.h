/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(PCB_STRING_H)
#define PCB_STRING_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <time.h>
#include <stdbool.h>
#include <stdint.h>
#include <stddef.h>
#include <pcb/utils/datetime.h>

/**
   @ingroup pcb_utils_containers
   @file
   @brief
   Header file with string type definitions and string functions
 */

/**
   @ingroup pcb_utils_containers
   @defgroup pcb_utils_string String
   @{

   @brief
   String handling functions.

   @details
   This string implementation can only handle ASCII strings or UTF-8 strings.

   This group of functions help in string coversion from and to other data types,
   getting sub-strings, finding and replacing sub-strings, extend strings at the end as well at the beginning

   The string buffer allocation is dynamic, but at initilization time, a buffer size hint can be given
 */

/**
   @brief
   String definition

   @details
   This structure defines a string.
 */
typedef struct _string {
    char* buffer;      /**< the string buffer */
    size_t bufferSize; /**< size of the allocated buffer, this is not the size of the string */
} string_t;

/**
   @brief
   Case enumeration

   @details
   Enumeration of different cases. Can be used in compare functions.
 */
typedef enum _string_case {
    string_case_sensitive = 0,                 /**< Case sensitive */
    CaseSensitive = string_case_sensitive,     /**< @deprecated Use @ref string_case_sensitive*/
    string_case_insensitive,                   /**< Case insensitive */
    CaseInsensitive = string_case_insensitive, /**< @deprecated Use @ref string_case_insensitive*/
} string_case_t;

/**
   @}
 */

// initializer functions
bool string_initialize(string_t* string, size_t bufferSize);
bool string_attachBuffer(string_t* string, char* buffer, size_t bufferSize);

// cleanup functions
void string_cleanup(string_t* string);
void string_clear(string_t* string);

/**
   @ingroup pcb_utils_string
   @brief
   Returns the string buffer.

   This can return NULL. Consider using string_safeBuffer.

   @details
   This function returns a pointer to the string buffer. Do not store this pointer for later use.\n
   The returned pointer must not be freed.

   @param string the string from which the buffer is returned

   @return
    - pointer to to string buffer
    - NULL if string is NULL
 */
const char* string_buffer(const string_t* string);
const char* string_safeBuffer(const string_t* string);

size_t string_copy(string_t* dest, const string_t* src);

size_t string_fromBool(string_t* string, const bool data);
size_t string_fromChar(string_t* string, const char* data);
size_t string_fromInt8(string_t* string, const int8_t data);
size_t string_fromInt16(string_t* string, const int16_t data);
size_t string_fromInt32(string_t* string, const int32_t data);
size_t string_fromInt64(string_t* string, const int64_t data);
size_t string_fromUInt8(string_t* string, const uint8_t data);
size_t string_fromUInt16(string_t* string, const uint16_t data);
size_t string_fromUInt32(string_t* string, const uint32_t data);
size_t string_fromUInt64(string_t* string, const uint64_t data);
size_t string_fromDouble(string_t* string, const double data);
size_t string_fromTime(string_t* string, const struct tm* data, const char* format);
size_t string_fromTimeExtended(string_t* string, const pcb_datetime_t* data, const char* format);

bool string_convertToBool(const string_t* string, bool* success);
static inline bool string_toBool(const string_t* string) {
    return string_convertToBool(string, NULL);
}

/**
   @ingroup pcb_utils_string
   @brief
   Returns a copy of the string buffer.

   @details
   This function returns a pointer to a copy of the string buffer.\n
   @warning
   The returned pointer must be freed.

   @param string the string from which the buffer is copied.

   @return
    - pointer to a copy of the string buffer
    - NULL
        - if string is NULL
        - no more memory
 */
char* string_toChar(const string_t* string);

int8_t string_convertToInt8(const string_t* string, bool* success);
static inline int8_t string_toInt8(const string_t* string) {
    return string_convertToInt8(string, NULL);
}

int16_t string_convertToInt16(const string_t* string, bool* success);
static inline int16_t string_toInt16(const string_t* string) {
    return string_convertToInt16(string, NULL);
}

int32_t string_convertToInt32(const string_t* string, bool* success);
static inline int32_t string_toInt32(const string_t* string) {
    return string_convertToInt32(string, NULL);
}

int64_t string_convertToInt64(const string_t* string, bool* success);
static inline int64_t string_toInt64(const string_t* string) {
    return string_convertToInt64(string, NULL);
}

uint8_t string_convertToUInt8(const string_t* string, bool* success);
static inline uint8_t string_toUInt8(const string_t* string) {
    return string_convertToUInt8(string, NULL);
}

uint16_t string_convertToUInt16(const string_t* string, bool* success);
static inline uint16_t string_toUInt16(const string_t* string) {
    return string_convertToUInt16(string, NULL);
}

uint32_t string_convertToUInt32(const string_t* string, bool* success);
static inline uint32_t string_toUInt32(const string_t* string) {
    return string_convertToUInt32(string, NULL);
}

uint64_t string_convertToUInt64(const string_t* string, bool* success);
static inline uint64_t string_toUInt64(const string_t* string) {
    return string_convertToUInt64(string, NULL);
}

double string_convertToDouble(const string_t* string, bool* success);
static inline double string_toDouble(const string_t* string) {
    return string_convertToDouble(string, NULL);
}


struct tm* string_toTime(const string_t* string, const char* format);
struct pcb_datetime_t* string_toTimeExtended(const string_t* string, const char* format);

void string_toUpper(string_t* string);
void string_toLower(string_t* string);

size_t string_appendChar(string_t* dest, const char* string);
size_t string_append(string_t* dest, const string_t* string);
size_t string_prependChar(string_t* dest, const char* string);
size_t string_prepend(string_t* dest, const string_t* string);

size_t string_appendFormat(string_t* dest, const char* format, ...);

size_t string_left(string_t* dest, const string_t* string, size_t length);
size_t string_right(string_t* dest, const string_t* string, size_t length);
size_t string_mid(string_t* dest, const string_t* string, unsigned int start, size_t length);

unsigned int string_findChar(const string_t* haystack, unsigned int start, const char* needle, string_case_t cs);
unsigned int string_find(const string_t* haystack, unsigned int start, const string_t* needle, string_case_t cs);
bool string_containsChar(const string_t* haystack, const char* needle, string_case_t cs);
bool string_contains(const string_t* haystack, const string_t* needle, string_case_t cs);
void string_replaceChar(string_t* string, const char* search, const char* replace);
void string_replace(string_t* string, const string_t* search, const string_t* replace);

int string_compareChar(const string_t* string1, const char* string2, string_case_t cs);
int string_compare(const string_t* string1, const string_t* string2, string_case_t cs);

bool string_isNull(const string_t* string);
bool string_isEmpty(const string_t* string);
size_t string_length(const string_t* string);

void string_trimLeft(string_t* string);
void string_trimRight(string_t* string);
void string_trim(string_t* string);

bool string_isNumeric(string_t* string);

bool string_resolveEnvVar(string_t* string);
char* string_resolveEnvVarChar(const char* string);

#ifdef __cplusplus
}
#endif

#endif // PCB_STRING_H
