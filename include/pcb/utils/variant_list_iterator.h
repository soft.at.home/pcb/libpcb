/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef _PCB_UTILS_VARIANT_LIST_ITERATOR_H
#define _PCB_UTILS_VARIANT_LIST_ITERATOR_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <pcb/utils/variant.h>
#include <pcb/utils/variant_list.h>

/**
   @ingroup pcb_utils_containers
   @file
   @brief
   Header file with variant list helper functions for iterating a variant list
 */

/**
   @ingroup pcb_utils_variant_list
   @brief
   Get a variant list iterator data and convert it to a bool

   @details
   This function is provided for convenience. It is fetching a variant in a variant list iterator
   and converts it to a bool as @ref variant_toBool.\n
   The returned pointer MUST be freed.\n

   @param it the variant list iterator of interest

   @return
    - The converted value
 */
static inline bool variant_list_iterator_bool(const variant_list_iterator_t* it) {
    return variant_bool(variant_list_iterator_data(it));
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Return the first bool * iterator in a list

   @param list the list over which to iterate

   @return an iterator converted to bool
 */
static inline bool* variant_list_firstBool(const variant_list_t* list) {
    variant_list_iterator_t* next = variant_list_first(list);
    if(next == NULL) {
        return NULL;
    }
    if(variant_type(variant_list_iterator_data(next)) != variant_type_bool) {
        return NULL;
    }
    return &(variant_list_iterator_data(next)->data.b);
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Return the next bool * iterator in a list

   @param it the previous iterator
   @return an iterator converted to bool *
 */
static inline bool* variant_list_iterator_nextBool(bool* it) {
    variant_list_iterator_t* prev = llist_item_data(llist_item_data(it, variant_t, data), variant_list_iterator_t, variant);
    variant_list_iterator_t* next = variant_list_iterator_next(prev);
    if(next == NULL) {
        return NULL;
    }
    if(variant_type(variant_list_iterator_data(next)) != variant_type_bool) {
        return NULL;
    }
    return &(variant_list_iterator_data(next)->data.b);
}
/**
   @ingroup pcb_utils_variant_list
   @brief
   Return the last bool * iterator in a list

   @param list the list over which to iterate
   @return an iterator converted to bool *
 */
static inline bool* variant_list_lastBool(const variant_list_t* list) {
    variant_list_iterator_t* next = variant_list_last(list);
    if(next == NULL) {
        return NULL;
    }
    if(variant_type(variant_list_iterator_data(next)) != variant_type_bool) {
        return NULL;
    }
    return &(variant_list_iterator_data(next)->data.b);
}
/**
   @ingroup pcb_utils_variant_list
   @brief
   Return the previous bool * iterator in a list

   @param it the previous iterator
   @return an iterator converted to bool *
 */
static inline bool* variant_list_iterator_prevBool(bool* it) {
    variant_list_iterator_t* prev = llist_item_data(llist_item_data(it, variant_t, data), variant_list_iterator_t, variant);
    variant_list_iterator_t* next = variant_list_iterator_prev(prev);
    if(next == NULL) {
        return NULL;
    }
    if(variant_type(variant_list_iterator_data(next)) != variant_type_bool) {
        return NULL;
    }
    return &(variant_list_iterator_data(next)->data.b);
}

#if __STDC_VERSION__ >= 199901L
/**
   @ingroup pcb_utils_variant_list
   @brief
   Definition of a helper macro for forward iteration through the variant linked list (head->tail)

   @details
   This <b>helper macro</b> iterates forward through a <b>variant linked list</b> starting from the head

   <b>Warning</b>: don&rsquo;t <b>modify</b> the variant linked list during the iteration!

   @param it the name of the ${ctype} to be declared
   @param list the list
 */
#define variant_list_for_each_declare_bool(it, list)     for(bool* it = variant_list_firstBool(list); it; it = variant_list_iterator_nextBool(it))
#endif

/**
   @ingroup pcb_utils_variant_list
   @brief
   Definition of a helper macro for forward iteration through the variant linked list (head->tail)

   @details
   This <b>helper macro</b> iterates forward through a <b>variant linked list</b> starting from the head
   <b>Warning</b>: don&rsquo;t <b>modify</b> the variant linked list during the iteration!

   @param it the name of the ${ctype} to be declared
   @param list the list
 */
#define variant_list_for_each_bool(it, list)     for(it = variant_list_firstBool(list); it; it = variant_list_iterator_nextBool(it))

/**
   @ingroup pcb_utils_variant_list
   @brief
   Get a variant list iterator data and convert it to a int8_t

   @details
   This function is provided for convenience. It is fetching a variant in a variant list iterator
   and converts it to a int8_t as @ref variant_toInt8.\n
   The returned pointer MUST be freed.\n

   @param it the variant list iterator of interest

   @return
    - The converted value
 */
static inline int8_t variant_list_iterator_int8(const variant_list_iterator_t* it) {
    return variant_int8(variant_list_iterator_data(it));
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Return the first int8_t * iterator in a list

   @param list the list over which to iterate
   @return an iterator converted to int8_t *
 */
static inline int8_t* variant_list_firstInt8(const variant_list_t* list) {
    variant_list_iterator_t* next = variant_list_first(list);
    if(next == NULL) {
        return NULL;
    }
    if(variant_type(variant_list_iterator_data(next)) != variant_type_int8) {
        return NULL;
    }
    return &(variant_list_iterator_data(next)->data.i8);
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Return the next int8_t * iterator in a list

   @param it the previous iterator
   @return an iterator converted to int8_t *
 */
static inline int8_t* variant_list_iterator_nextInt8(int8_t* it) {
    variant_list_iterator_t* prev = llist_item_data(llist_item_data(it, variant_t, data), variant_list_iterator_t, variant);
    variant_list_iterator_t* next = variant_list_iterator_next(prev);
    if(next == NULL) {
        return NULL;
    }
    if(variant_type(variant_list_iterator_data(next)) != variant_type_int8) {
        return NULL;
    }
    return &(variant_list_iterator_data(next)->data.i8);
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Return the last int8_t * iterator in a list

   @param list the list over which to iterate
   @return an iterator converted to int8_t *
 */
static inline int8_t* variant_list_lastInt8(const variant_list_t* list) {
    variant_list_iterator_t* next = variant_list_last(list);
    if(next == NULL) {
        return NULL;
    }
    if(variant_type(variant_list_iterator_data(next)) != variant_type_int8) {
        return NULL;
    }
    return &(variant_list_iterator_data(next)->data.i8);
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Return the previous int8_t * iterator in a list

   @param it the previous iterator
   @return an iterator converted to int8_t *
 */
static inline int8_t* variant_list_iterator_prevInt8(int8_t* it) {
    variant_list_iterator_t* prev = llist_item_data(llist_item_data(it, variant_t, data), variant_list_iterator_t, variant);
    variant_list_iterator_t* next = variant_list_iterator_prev(prev);
    if(next == NULL) {
        return NULL;
    }
    if(variant_type(variant_list_iterator_data(next)) != variant_type_int8) {
        return NULL;
    }
    return &(variant_list_iterator_data(next)->data.i8);
}

#if __STDC_VERSION__ >= 199901L
/**
   @ingroup pcb_utils_variant_list
   @brief
   Definition of a helper macro for forward iteration through the variant linked list (head->tail)

   @details
   This <b>helper macro</b> iterates forward through a <b>variant linked list</b> starting from the head
   <b>Warning</b>: don&rsquo;t <b>modify</b> the variant linked list during the iteration!

   @param it the name of the ${ctype} to be declared
   @param list the list
 */
#define variant_list_for_each_declare_int8(it, list)     for(int8_t* it = variant_list_firstInt8(list); it; it = variant_list_iterator_nextInt8(it))
#endif

/**
   @ingroup pcb_utils_variant_list
   @brief
   Definition of a helper macro for forward iteration through the variant linked list (head->tail)

   @details
   This <b>helper macro</b> iterates forward through a <b>variant linked list</b> starting from the head
   <b>Warning</b>: don&rsquo;t <b>modify</b> the variant linked list during the iteration!

   @param it the name of the ${ctype} to be declared
   @param list the list
 */
#define variant_list_for_each_int8(it, list)     for(it = variant_list_firstInt8(list); it; it = variant_list_iterator_nextInt8(it))

/**
   @ingroup pcb_utils_variant_list
   @brief
   Get a variant list iterator data and convert it to a int16_t

   @details
   This function is provided for convenience. It is fetching a variant in a variant list iterator
   and converts it to a int16_t as @ref variant_toInt16.\n
   The returned pointer MUST be freed.\n

   @param it the variant list iterator of interest

   @return
    - The converted value
 */
static inline int16_t variant_list_iterator_int16(const variant_list_iterator_t* it) {
    return variant_int16(variant_list_iterator_data(it));
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Return the first int16_t * iterator in a list

   @param list the list over which to iterate
   @return an iterator converted to int16_t *
 */
static inline int16_t* variant_list_firstInt16(const variant_list_t* list) {
    variant_list_iterator_t* next = variant_list_first(list);
    if(next == NULL) {
        return NULL;
    }
    if(variant_type(variant_list_iterator_data(next)) != variant_type_int16) {
        return NULL;
    }
    return &(variant_list_iterator_data(next)->data.i16);
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Return the next int16_t * iterator in a list

   @param it the previous iterator
   @return an iterator converted to int16_t *
 */
static inline int16_t* variant_list_iterator_nextInt16(int16_t* it) {
    variant_list_iterator_t* prev = llist_item_data(llist_item_data(it, variant_t, data), variant_list_iterator_t, variant);
    variant_list_iterator_t* next = variant_list_iterator_next(prev);
    if(next == NULL) {
        return NULL;
    }
    if(variant_type(variant_list_iterator_data(next)) != variant_type_int16) {
        return NULL;
    }
    return &(variant_list_iterator_data(next)->data.i16);
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Return the last int16_t * iterator in a list

   @param list the list over which to iterate
   @return an iterator converted to int16_t *
 */
static inline int16_t* variant_list_lastInt16(const variant_list_t* list) {
    variant_list_iterator_t* next = variant_list_last(list);
    if(next == NULL) {
        return NULL;
    }
    if(variant_type(variant_list_iterator_data(next)) != variant_type_int16) {
        return NULL;
    }
    return &(variant_list_iterator_data(next)->data.i16);
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Return the previous int16_t * iterator in a list

   @param it the previous iterator
   @return an iterator converted to int16_t *
 */
static inline int16_t* variant_list_iterator_prevInt16(int16_t* it) {
    variant_list_iterator_t* prev = llist_item_data(llist_item_data(it, variant_t, data), variant_list_iterator_t, variant);
    variant_list_iterator_t* next = variant_list_iterator_prev(prev);
    if(next == NULL) {
        return NULL;
    }
    if(variant_type(variant_list_iterator_data(next)) != variant_type_int16) {
        return NULL;
    }
    return &(variant_list_iterator_data(next)->data.i16);
}

#if __STDC_VERSION__ >= 199901L
/**
   @ingroup pcb_utils_variant_list
   @brief
   Definition of a helper macro for forward iteration through the variant linked list (head->tail)

   @details
   This <b>helper macro</b> iterates forward through a <b>variant linked list</b> starting from the head
   <b>Warning</b>: don&rsquo;t <b>modify</b> the variant linked list during the iteration!

   @param it the name of the ${ctype} to be declared
   @param list the list
 */
#define variant_list_for_each_declare_int16(it, list)     for(int16_t* it = variant_list_firstInt16(list); it; it = variant_list_iterator_nextInt16(it))
#endif

/**
   @ingroup pcb_utils_variant_list
   @brief
   Definition of a helper macro for forward iteration through the variant linked list (head->tail)

   @details
   This <b>helper macro</b> iterates forward through a <b>variant linked list</b> starting from the head
   <b>Warning</b>: don&rsquo;t <b>modify</b> the variant linked list during the iteration!

   @param it the name of the ${ctype} to be declared
   @param list the list
 */
#define variant_list_for_each_int16(it, list)     for(it = variant_list_firstInt16(list); it; it = variant_list_iterator_nextInt16(it))

/**
   @ingroup pcb_utils_variant_list
   @brief
   Get a variant list iterator data and convert it to a int32_t

   @details
   This function is provided for convenience. It is fetching a variant in a variant list iterator
   and converts it to a int32_t as @ref variant_toInt32.\n
   The returned pointer MUST be freed.\n

   @param it the variant list iterator of interest

   @return
    - The converted value
 */
static inline int32_t variant_list_iterator_int32(const variant_list_iterator_t* it) {
    return variant_int32(variant_list_iterator_data(it));
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Return the first int32_t * iterator in a list

   @param list the list over which to iterate
   @return an iterator converted to int32_t *
 */
static inline int32_t* variant_list_firstInt32(const variant_list_t* list) {
    variant_list_iterator_t* next = variant_list_first(list);
    if(next == NULL) {
        return NULL;
    }
    if(variant_type(variant_list_iterator_data(next)) != variant_type_int32) {
        return NULL;
    }
    return &(variant_list_iterator_data(next)->data.i32);
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Return the next int32_t * iterator in a list

   @param it the previous iterator
   @return an iterator converted to int32_t *
 */
static inline int32_t* variant_list_iterator_nextInt32(int32_t* it) {
    variant_list_iterator_t* prev = llist_item_data(llist_item_data(it, variant_t, data), variant_list_iterator_t, variant);
    variant_list_iterator_t* next = variant_list_iterator_next(prev);
    if(next == NULL) {
        return NULL;
    }
    if(variant_type(variant_list_iterator_data(next)) != variant_type_int32) {
        return NULL;
    }
    return &(variant_list_iterator_data(next)->data.i32);
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Return the last int32_t * iterator in a list

   @param list the list over which to iterate
   @return an iterator converted to int32_t *
 */
static inline int32_t* variant_list_lastInt32(const variant_list_t* list) {
    variant_list_iterator_t* next = variant_list_last(list);
    if(next == NULL) {
        return NULL;
    }
    if(variant_type(variant_list_iterator_data(next)) != variant_type_int32) {
        return NULL;
    }
    return &(variant_list_iterator_data(next)->data.i32);
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Return the previous int32_t * iterator in a list

   @param it the previous iterator
   @return an iterator converted to int32_t *
 */
static inline int32_t* variant_list_iterator_prevInt32(int32_t* it) {
    variant_list_iterator_t* prev = llist_item_data(llist_item_data(it, variant_t, data), variant_list_iterator_t, variant);
    variant_list_iterator_t* next = variant_list_iterator_prev(prev);
    if(next == NULL) {
        return NULL;
    }
    if(variant_type(variant_list_iterator_data(next)) != variant_type_int32) {
        return NULL;
    }
    return &(variant_list_iterator_data(next)->data.i32);
}

#if __STDC_VERSION__ >= 199901L
/**
   @ingroup pcb_utils_variant_list
   @brief
   Definition of a helper macro for forward iteration through the variant linked list (head->tail)

   @details
   This <b>helper macro</b> iterates forward through a <b>variant linked list</b> starting from the head
   <b>Warning</b>: don&rsquo;t <b>modify</b> the variant linked list during the iteration!

   @param it the name of the ${ctype} to be declared
   @param list the list
 */
#define variant_list_for_each_declare_int32(it, list)     for(int32_t* it = variant_list_firstInt32(list); it; it = variant_list_iterator_nextInt32(it))
#endif

/**
   @ingroup pcb_utils_variant_list
   @brief
   Definition of a helper macro for forward iteration through the variant linked list (head->tail)

   @details
   This <b>helper macro</b> iterates forward through a <b>variant linked list</b> starting from the head
   <b>Warning</b>: don&rsquo;t <b>modify</b> the variant linked list during the iteration!

   @param it the name of the ${ctype} to be declared
   @param list the list
 */
#define variant_list_for_each_int32(it, list)     for(it = variant_list_firstInt32(list); it; it = variant_list_iterator_nextInt32(it))

/**
   @ingroup pcb_utils_variant_list
   @brief
   Get a variant list iterator data and convert it to a int64_t

   @details
   This function is provided for convenience. It is fetching a variant in a variant list iterator
   and converts it to a int64_t as @ref variant_toInt64.\n
   The returned pointer MUST be freed.\n

   @param it the variant list iterator of interest

   @return
    - The converted value
 */
static inline int64_t variant_list_iterator_int64(const variant_list_iterator_t* it) {
    return variant_int64(variant_list_iterator_data(it));
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Return the first int64_t * iterator in a list

   @param list the list over which to iterate
   @return an iterator converted to int64_t *
 */
static inline int64_t* variant_list_firstInt64(const variant_list_t* list) {
    variant_list_iterator_t* next = variant_list_first(list);
    if(next == NULL) {
        return NULL;
    }
    if(variant_type(variant_list_iterator_data(next)) != variant_type_int64) {
        return NULL;
    }
    return &(variant_list_iterator_data(next)->data.i64);
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Return the next int64_t * iterator in a list

   @param it the previous iterator
   @return an iterator converted to int64_t *
 */
static inline int64_t* variant_list_iterator_nextInt64(int64_t* it) {
    variant_list_iterator_t* prev = llist_item_data(llist_item_data(it, variant_t, data), variant_list_iterator_t, variant);
    variant_list_iterator_t* next = variant_list_iterator_next(prev);
    if(next == NULL) {
        return NULL;
    }
    if(variant_type(variant_list_iterator_data(next)) != variant_type_int64) {
        return NULL;
    }
    return &(variant_list_iterator_data(next)->data.i64);
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Return the last int64_t * iterator in a list

   @param list the list over which to iterate
   @return an iterator converted to int64_t *
 */
static inline int64_t* variant_list_lastInt64(const variant_list_t* list) {
    variant_list_iterator_t* next = variant_list_last(list);
    if(next == NULL) {
        return NULL;
    }
    if(variant_type(variant_list_iterator_data(next)) != variant_type_int64) {
        return NULL;
    }
    return &(variant_list_iterator_data(next)->data.i64);
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Return the previous int64_t * iterator in a list

   @param it the previous iterator
   @return an iterator converted to int64_t *
 */
static inline int64_t* variant_list_iterator_prevInt64(int64_t* it) {
    variant_list_iterator_t* prev = llist_item_data(llist_item_data(it, variant_t, data), variant_list_iterator_t, variant);
    variant_list_iterator_t* next = variant_list_iterator_prev(prev);
    if(next == NULL) {
        return NULL;
    }
    if(variant_type(variant_list_iterator_data(next)) != variant_type_int64) {
        return NULL;
    }
    return &(variant_list_iterator_data(next)->data.i64);
}

#if __STDC_VERSION__ >= 199901L
/**
   @ingroup pcb_utils_variant_list
   @brief
   Definition of a helper macro for forward iteration through the variant linked list (head->tail)

   @details
   This <b>helper macro</b> iterates forward through a <b>variant linked list</b> starting from the head
   <b>Warning</b>: don&rsquo;t <b>modify</b> the variant linked list during the iteration!

   @param it the name of the ${ctype} to be declared
   @param list the list
 */
#define variant_list_for_each_declare_int64(it, list)     for(int64_t* it = variant_list_firstInt64(list); it; it = variant_list_iterator_nextInt64(it))
#endif

/**
   @ingroup pcb_utils_variant_list
   @brief
   Definition of a helper macro for forward iteration through the variant linked list (head->tail)

   @details
   This <b>helper macro</b> iterates forward through a <b>variant linked list</b> starting from the head
   <b>Warning</b>: don&rsquo;t <b>modify</b> the variant linked list during the iteration!

   @param it the name of the ${ctype} to be declared
   @param list the list
 */
#define variant_list_for_each_int64(it, list)     for(it = variant_list_firstInt64(list); it; it = variant_list_iterator_nextInt64(it))

/**
   @ingroup pcb_utils_variant_list
   @brief
   Get a variant list iterator data and convert it to a uint8_t

   @details
   This function is provided for convenience. It is fetching a variant in a variant list iterator
   and converts it to a uint8_t as @ref variant_toUInt8.\n
   The returned pointer MUST be freed.\n

   @param it the variant list iterator of interest

   @return
    - The converted value
 */
static inline uint8_t variant_list_iterator_uint8(const variant_list_iterator_t* it) {
    return variant_uint8(variant_list_iterator_data(it));
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Return the first uint8_t * iterator in a list

   @param list the list over which to iterate
   @return an iterator converted to uint8_t *
 */
static inline uint8_t* variant_list_firstUInt8(const variant_list_t* list) {
    variant_list_iterator_t* next = variant_list_first(list);
    if(next == NULL) {
        return NULL;
    }
    if(variant_type(variant_list_iterator_data(next)) != variant_type_uint8) {
        return NULL;
    }
    return &(variant_list_iterator_data(next)->data.ui8);
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Return the next uint8_t * iterator in a list

   @param it the previous iterator
   @return an iterator converted to uint8_t *
 */
static inline uint8_t* variant_list_iterator_nextUInt8(uint8_t* it) {
    variant_list_iterator_t* prev = llist_item_data(llist_item_data(it, variant_t, data), variant_list_iterator_t, variant);
    variant_list_iterator_t* next = variant_list_iterator_next(prev);
    if(next == NULL) {
        return NULL;
    }
    if(variant_type(variant_list_iterator_data(next)) != variant_type_uint8) {
        return NULL;
    }
    return &(variant_list_iterator_data(next)->data.ui8);
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Return the last uint8_t * iterator in a list

   @param list the list over which to iterate
   @return an iterator converted to uint8_t *
 */
static inline uint8_t* variant_list_lastUInt8(const variant_list_t* list) {
    variant_list_iterator_t* next = variant_list_last(list);
    if(next == NULL) {
        return NULL;
    }
    if(variant_type(variant_list_iterator_data(next)) != variant_type_uint8) {
        return NULL;
    }
    return &(variant_list_iterator_data(next)->data.ui8);
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Return the previous uint8_t * iterator in a list

   @param it the previous iterator
   @return an iterator converted to uint8_t *
 */
static inline uint8_t* variant_list_iterator_prevUInt8(uint8_t* it) {
    variant_list_iterator_t* prev = llist_item_data(llist_item_data(it, variant_t, data), variant_list_iterator_t, variant);
    variant_list_iterator_t* next = variant_list_iterator_prev(prev);
    if(next == NULL) {
        return NULL;
    }
    if(variant_type(variant_list_iterator_data(next)) != variant_type_uint8) {
        return NULL;
    }
    return &(variant_list_iterator_data(next)->data.ui8);
}

#if __STDC_VERSION__ >= 199901L
/**
   @ingroup pcb_utils_variant_list
   @brief
   Definition of a helper macro for forward iteration through the variant linked list (head->tail)

   @details
   This <b>helper macro</b> iterates forward through a <b>variant linked list</b> starting from the head
   <b>Warning</b>: don&rsquo;t <b>modify</b> the variant linked list during the iteration!

   @param it the name of the ${ctype} to be declared
   @param list the list
 */
#define variant_list_for_each_declare_uint8(it, list)     for(uint8_t* it = variant_list_firstUInt8(list); it; it = variant_list_iterator_nextUInt8(it))
#endif

/**
   @ingroup pcb_utils_variant_list
   @brief
   Definition of a helper macro for forward iteration through the variant linked list (head->tail)

   @details
   This <b>helper macro</b> iterates forward through a <b>variant linked list</b> starting from the head
   <b>Warning</b>: don&rsquo;t <b>modify</b> the variant linked list during the iteration!

   @param it the name of the ${ctype} to be declared
   @param list the list
 */
#define variant_list_for_each_uint8(it, list)     for(it = variant_list_firstUInt8(list); it; it = variant_list_iterator_nextUInt8(it))

/**
   @ingroup pcb_utils_variant_list
   @brief
   Get a variant list iterator data and convert it to a uint16_t

   @details
   This function is provided for convenience. It is fetching a variant in a variant list iterator
   and converts it to a uint16_t as @ref variant_toUInt16.\n
   The returned pointer MUST be freed.\n

   @param it the variant list iterator of interest

   @return
    - The converted value
 */
static inline uint16_t variant_list_iterator_uint16(const variant_list_iterator_t* it) {
    return variant_uint16(variant_list_iterator_data(it));
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Return the first uint16_t * iterator in a list

   @param list the list over which to iterate
   @return an iterator converted to uint16_t *
 */
static inline uint16_t* variant_list_firstUInt16(const variant_list_t* list) {
    variant_list_iterator_t* next = variant_list_first(list);
    if(next == NULL) {
        return NULL;
    }
    if(variant_type(variant_list_iterator_data(next)) != variant_type_uint16) {
        return NULL;
    }
    return &(variant_list_iterator_data(next)->data.ui16);
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Return the next uint16_t * iterator in a list

   @param it the previous iterator
   @return an iterator converted to uint16_t *
 */
static inline uint16_t* variant_list_iterator_nextUInt16(uint16_t* it) {
    variant_list_iterator_t* prev = llist_item_data(llist_item_data(it, variant_t, data), variant_list_iterator_t, variant);
    variant_list_iterator_t* next = variant_list_iterator_next(prev);
    if(next == NULL) {
        return NULL;
    }
    if(variant_type(variant_list_iterator_data(next)) != variant_type_uint16) {
        return NULL;
    }
    return &(variant_list_iterator_data(next)->data.ui16);
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Return the last uint16_t * iterator in a list

   @param list the list over which to iterate
   @return an iterator converted to uint16_t *
 */
static inline uint16_t* variant_list_lastUInt16(const variant_list_t* list) {
    variant_list_iterator_t* next = variant_list_last(list);
    if(next == NULL) {
        return NULL;
    }
    if(variant_type(variant_list_iterator_data(next)) != variant_type_uint16) {
        return NULL;
    }
    return &(variant_list_iterator_data(next)->data.ui16);
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Return the previous uint16_t * iterator in a list

   @param it the previous iterator
   @return an iterator converted to uint16_t *
 */
static inline uint16_t* variant_list_iterator_prevUInt16(uint16_t* it) {
    variant_list_iterator_t* prev = llist_item_data(llist_item_data(it, variant_t, data), variant_list_iterator_t, variant);
    variant_list_iterator_t* next = variant_list_iterator_prev(prev);
    if(next == NULL) {
        return NULL;
    }
    if(variant_type(variant_list_iterator_data(next)) != variant_type_uint16) {
        return NULL;
    }
    return &(variant_list_iterator_data(next)->data.ui16);
}

#if __STDC_VERSION__ >= 199901L
/**
   @ingroup pcb_utils_variant_list
   @brief
   Definition of a helper macro for forward iteration through the variant linked list (head->tail)

   @details
   This <b>helper macro</b> iterates forward through a <b>variant linked list</b> starting from the head
   <b>Warning</b>: don&rsquo;t <b>modify</b> the variant linked list during the iteration!

   @param it the name of the ${ctype} to be declared
   @param list the list
 */
#define variant_list_for_each_declare_uint16(it, list)     for(uint16_t* it = variant_list_firstUInt16(list); it; it = variant_list_iterator_nextUInt16(it))
#endif

/**
   @ingroup pcb_utils_variant_list
   @brief
   Definition of a helper macro for forward iteration through the variant linked list (head->tail)

   @details
   This <b>helper macro</b> iterates forward through a <b>variant linked list</b> starting from the head
   <b>Warning</b>: don&rsquo;t <b>modify</b> the variant linked list during the iteration!

   @param it the name of the ${ctype} to be declared
   @param list the list
 */
#define variant_list_for_each_uint16(it, list)     for(it = variant_list_firstUInt16(list); it; it = variant_list_iterator_nextUInt16(it))

/**
   @ingroup pcb_utils_variant_list
   @brief
   Get a variant list iterator data and convert it to a uint32_t

   @details
   This function is provided for convenience. It is fetching a variant in a variant list iterator
   and converts it to a uint32_t as @ref variant_toUInt32.\n
   The returned pointer MUST be freed.\n

   @param it the variant list iterator of interest

   @return
    - The converted value
 */
static inline uint32_t variant_list_iterator_uint32(const variant_list_iterator_t* it) {
    return variant_uint32(variant_list_iterator_data(it));
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Return the first uint32_t * iterator in a list

   @param list the list over which to iterate
   @return an iterator converted to uint32_t *
 */
static inline uint32_t* variant_list_firstUInt32(const variant_list_t* list) {
    variant_list_iterator_t* next = variant_list_first(list);
    if(next == NULL) {
        return NULL;
    }
    if(variant_type(variant_list_iterator_data(next)) != variant_type_uint32) {
        return NULL;
    }
    return &(variant_list_iterator_data(next)->data.ui32);
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Return the next uint32_t * iterator in a list

   @param it the previous iterator
   @return an iterator converted to uint32_t *
 */
static inline uint32_t* variant_list_iterator_nextUInt32(uint32_t* it) {
    variant_list_iterator_t* prev = llist_item_data(llist_item_data(it, variant_t, data), variant_list_iterator_t, variant);
    variant_list_iterator_t* next = variant_list_iterator_next(prev);
    if(next == NULL) {
        return NULL;
    }
    if(variant_type(variant_list_iterator_data(next)) != variant_type_uint32) {
        return NULL;
    }
    return &(variant_list_iterator_data(next)->data.ui32);
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Return the last uint32_t * iterator in a list

   @param list the list over which to iterate
   @return an iterator converted to uint32_t *
 */
static inline uint32_t* variant_list_lastUInt32(const variant_list_t* list) {
    variant_list_iterator_t* next = variant_list_last(list);
    if(next == NULL) {
        return NULL;
    }
    if(variant_type(variant_list_iterator_data(next)) != variant_type_uint32) {
        return NULL;
    }
    return &(variant_list_iterator_data(next)->data.ui32);
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Return the previous uint32_t * iterator in a list

   @param it the previous iterator
   @return an iterator converted to uint32_t *
 */
static inline uint32_t* variant_list_iterator_prevUInt32(uint32_t* it) {
    variant_list_iterator_t* prev = llist_item_data(llist_item_data(it, variant_t, data), variant_list_iterator_t, variant);
    variant_list_iterator_t* next = variant_list_iterator_prev(prev);
    if(next == NULL) {
        return NULL;
    }
    if(variant_type(variant_list_iterator_data(next)) != variant_type_uint32) {
        return NULL;
    }
    return &(variant_list_iterator_data(next)->data.ui32);
}

#if __STDC_VERSION__ >= 199901L
/**
   @ingroup pcb_utils_variant_list
   @brief
   Definition of a helper macro for forward iteration through the variant linked list (head->tail)

   @details
   This <b>helper macro</b> iterates forward through a <b>variant linked list</b> starting from the head
   <b>Warning</b>: don&rsquo;t <b>modify</b> the variant linked list during the iteration!

   @param it the name of the ${ctype} to be declared
   @param list the list
 */
#define variant_list_for_each_declare_uint32(it, list)     for(uint32_t* it = variant_list_firstUInt32(list); it; it = variant_list_iterator_nextUInt32(it))
#endif

/**
   @ingroup pcb_utils_variant_list
   @brief
   Definition of a helper macro for forward iteration through the variant linked list (head->tail)

   @details
   This <b>helper macro</b> iterates forward through a <b>variant linked list</b> starting from the head
   <b>Warning</b>: don&rsquo;t <b>modify</b> the variant linked list during the iteration!

   @param it the name of the ${ctype} to be declared
   @param list the list
 */
#define variant_list_for_each_uint32(it, list)     for(it = variant_list_firstUInt32(list); it; it = variant_list_iterator_nextUInt32(it))

/**
   @ingroup pcb_utils_variant_list
   @brief
   Get a variant list iterator data and convert it to a uint64_t

   @details
   This function is provided for convenience. It is fetching a variant in a variant list iterator
   and converts it to a uint64_t as @ref variant_toUInt64.\n
   The returned pointer MUST be freed.\n

   @param it the variant list iterator of interest

   @return
    - The converted value
 */
static inline uint64_t variant_list_iterator_uint64(const variant_list_iterator_t* it) {
    return variant_uint64(variant_list_iterator_data(it));
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Return the first uint64_t * iterator in a list

   @param list the list over which to iterate
   @return an iterator converted to uint64_t *
 */
static inline uint64_t* variant_list_firstUInt64(const variant_list_t* list) {
    variant_list_iterator_t* next = variant_list_first(list);
    if(next == NULL) {
        return NULL;
    }
    if(variant_type(variant_list_iterator_data(next)) != variant_type_uint64) {
        return NULL;
    }
    return &(variant_list_iterator_data(next)->data.ui64);
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Return the next uint64_t * iterator in a list

   @param it the previous iterator
   @return an iterator converted to uint64_t *
 */
static inline uint64_t* variant_list_iterator_nextUInt64(uint64_t* it) {
    variant_list_iterator_t* prev = llist_item_data(llist_item_data(it, variant_t, data), variant_list_iterator_t, variant);
    variant_list_iterator_t* next = variant_list_iterator_next(prev);
    if(next == NULL) {
        return NULL;
    }
    if(variant_type(variant_list_iterator_data(next)) != variant_type_uint64) {
        return NULL;
    }
    return &(variant_list_iterator_data(next)->data.ui64);
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Return the last uint64_t * iterator in a list

   @param list the list over which to iterate
   @return an iterator converted to uint64_t *
 */
static inline uint64_t* variant_list_lastUInt64(const variant_list_t* list) {
    variant_list_iterator_t* next = variant_list_last(list);
    if(next == NULL) {
        return NULL;
    }
    if(variant_type(variant_list_iterator_data(next)) != variant_type_uint64) {
        return NULL;
    }
    return &(variant_list_iterator_data(next)->data.ui64);
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Return the previous uint64_t * iterator in a list

   @param it the previous iterator
   @return an iterator converted to uint64_t *
 */
static inline uint64_t* variant_list_iterator_prevUInt64(uint64_t* it) {
    variant_list_iterator_t* prev = llist_item_data(llist_item_data(it, variant_t, data), variant_list_iterator_t, variant);
    variant_list_iterator_t* next = variant_list_iterator_prev(prev);
    if(next == NULL) {
        return NULL;
    }
    if(variant_type(variant_list_iterator_data(next)) != variant_type_uint64) {
        return NULL;
    }
    return &(variant_list_iterator_data(next)->data.ui64);
}

#if __STDC_VERSION__ >= 199901L
/**
   @ingroup pcb_utils_variant_list
   @brief
   Definition of a helper macro for forward iteration through the variant linked list (head->tail)

   @details
   This <b>helper macro</b> iterates forward through a <b>variant linked list</b> starting from the head
   <b>Warning</b>: don&rsquo;t <b>modify</b> the variant linked list during the iteration!

   @param it the name of the ${ctype} to be declared
   @param list the list
 */
#define variant_list_for_each_declare_uint64(it, list)     for(uint64_t* it = variant_list_firstUInt64(list); it; it = variant_list_iterator_nextUInt64(it))
#endif

/**
   @ingroup pcb_utils_variant_list
   @brief
   Definition of a helper macro for forward iteration through the variant linked list (head->tail)

   @details
   This <b>helper macro</b> iterates forward through a <b>variant linked list</b> starting from the head
   <b>Warning</b>: don&rsquo;t <b>modify</b> the variant linked list during the iteration!

   @param it the name of the ${ctype} to be declared
   @param list the list
 */
#define variant_list_for_each_uint64(it, list)     for(it = variant_list_firstUInt64(list); it; it = variant_list_iterator_nextUInt64(it))

/**
   @ingroup pcb_utils_variant_list
   @brief
   Get a variant list iterator data and convert it to a double

   @details
   This function is provided for convenience. It is fetching a variant in a variant list iterator
   and converts it to a double as @ref variant_toDouble.\n
   The returned pointer MUST be freed.\n

   @param it the variant list iterator of interest

   @return
    - The converted value
 */
static inline double variant_list_iterator_double(const variant_list_iterator_t* it) {
    return variant_double(variant_list_iterator_data(it));
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Return the first double * iterator in a list

   @param list the list over which to iterate
   @return an iterator converted to double *
 */
static inline double* variant_list_firstDouble(const variant_list_t* list) {
    variant_list_iterator_t* next = variant_list_first(list);
    if(next == NULL) {
        return NULL;
    }
    if(variant_type(variant_list_iterator_data(next)) != variant_type_double) {
        return NULL;
    }
    return &(variant_list_iterator_data(next)->data.d);
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Return the next double * iterator in a list

   @param it the previous iterator
   @return an iterator converted to double *
 */
static inline double* variant_list_iterator_nextDouble(double* it) {
    variant_list_iterator_t* prev = llist_item_data(llist_item_data(it, variant_t, data), variant_list_iterator_t, variant);
    variant_list_iterator_t* next = variant_list_iterator_next(prev);
    if(next == NULL) {
        return NULL;
    }
    if(variant_type(variant_list_iterator_data(next)) != variant_type_double) {
        return NULL;
    }
    return &(variant_list_iterator_data(next)->data.d);
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Return the last double * iterator in a list

   @param list the list over which to iterate
   @return an iterator converted to double *
 */
static inline double* variant_list_lastDouble(const variant_list_t* list) {
    variant_list_iterator_t* next = variant_list_last(list);
    if(next == NULL) {
        return NULL;
    }
    if(variant_type(variant_list_iterator_data(next)) != variant_type_double) {
        return NULL;
    }
    return &(variant_list_iterator_data(next)->data.d);
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Return the previous double * iterator in a list

   @param it the previous iterator
   @return an iterator converted to double *
 */
static inline double* variant_list_iterator_prevDouble(double* it) {
    variant_list_iterator_t* prev = llist_item_data(llist_item_data(it, variant_t, data), variant_list_iterator_t, variant);
    variant_list_iterator_t* next = variant_list_iterator_prev(prev);
    if(next == NULL) {
        return NULL;
    }
    if(variant_type(variant_list_iterator_data(next)) != variant_type_double) {
        return NULL;
    }
    return &(variant_list_iterator_data(next)->data.d);
}

#if __STDC_VERSION__ >= 199901L
/**
   @ingroup pcb_utils_variant_list
   @brief
   Definition of a helper macro for forward iteration through the variant linked list (head->tail)

   @details
   This <b>helper macro</b> iterates forward through a <b>variant linked list</b> starting from the head
   <b>Warning</b>: don&rsquo;t <b>modify</b> the variant linked list during the iteration!

   @param it the name of the ${ctype} to be declared
   @param list the list
 */
#define variant_list_for_each_declare_double(it, list)     for(double* it = variant_list_firstDouble(list); it; it = variant_list_iterator_nextDouble(it))
#endif

/**
   @ingroup pcb_utils_variant_list
   @brief
   Definition of a helper macro for forward iteration through the variant linked list (head->tail)

   @details
   This <b>helper macro</b> iterates forward through a <b>variant linked list</b> starting from the head
   <b>Warning</b>: don&rsquo;t <b>modify</b> the variant linked list during the iteration!

   @param it the name of the ${ctype} to be declared
   @param list the list
 */
#define variant_list_for_each_double(it, list)     for(it = variant_list_firstDouble(list); it; it = variant_list_iterator_nextDouble(it))

/**
   @ingroup pcb_utils_variant_list
   @brief
   Get a variant list iterator data and convert it to a char *

   @details
   This function is provided for convenience. It is fetching a variant in a variant list iterator
   and converts it to a char * as @ref variant_toChar.\n
   The returned pointer MUST be freed.\n

   @param it the variant list iterator of interest

   @return
    - The converted value
 */
static inline char* variant_list_iterator_char(const variant_list_iterator_t* it) {
    return variant_char(variant_list_iterator_data(it));
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Get a direct char * pointer to a variant in a variant list iterator

   @details
   This function is provided for convenience. It is fetching a variant in a variant list iterator
   and returns a direct pointer to its char * value as @ref variant_da_char.\n
   The returned pointer MUST NOT be freed.\n

   @param it the variant list iterator of interest

   @return
    - The resulting char * pointer
    - NULL if the variant is invalid or not of char type
 */
static inline const char* variant_list_iterator_da_char(const variant_list_iterator_t* it) {
    return variant_da_char(variant_list_iterator_data(it));
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Return the first char * * iterator in a list

   @param list the list over which to iterate
   @return an iterator converted to char *
 */
static inline char** variant_list_firstChar(const variant_list_t* list) {
    variant_list_iterator_t* next = variant_list_first(list);
    if(next == NULL) {
        return NULL;
    }
    if(variant_type(variant_list_iterator_data(next)) != variant_type_string) {
        return NULL;
    }
    return &(variant_list_iterator_data(next)->data.str.buffer);
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Return the next char * * iterator in a list

   @param it the previous iterator
   @return an iterator converted to char * *
 */
static inline char** variant_list_iterator_nextChar(char** it) {
    variant_list_iterator_t* prev = llist_item_data(llist_item_data(it, variant_t, data), variant_list_iterator_t, variant);
    variant_list_iterator_t* next = variant_list_iterator_next(prev);
    if(next == NULL) {
        return NULL;
    }
    if(variant_type(variant_list_iterator_data(next)) != variant_type_string) {
        return NULL;
    }
    return &(variant_list_iterator_data(next)->data.str.buffer);
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Return the last char * * iterator in a list

   @param list the list over which to iterate
   @return an iterator converted to char * *
 */
static inline char** variant_list_lastChar(const variant_list_t* list) {
    variant_list_iterator_t* next = variant_list_last(list);
    if(next == NULL) {
        return NULL;
    }
    if(variant_type(variant_list_iterator_data(next)) != variant_type_string) {
        return NULL;
    }
    return &(variant_list_iterator_data(next)->data.str.buffer);
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Return the previous char * * iterator in a list

   @param it the previous iterator
   @return an iterator converted to char * *
 */
static inline char** variant_list_iterator_prevChar(char** it) {
    variant_list_iterator_t* prev = llist_item_data(llist_item_data(it, variant_t, data), variant_list_iterator_t, variant);
    variant_list_iterator_t* next = variant_list_iterator_prev(prev);
    if(next == NULL) {
        return NULL;
    }
    if(variant_type(variant_list_iterator_data(next)) != variant_type_string) {
        return NULL;
    }
    return &(variant_list_iterator_data(next)->data.str.buffer);
}

#if __STDC_VERSION__ >= 199901L
/**
   @ingroup pcb_utils_variant_list
   @brief
   Definition of a helper macro for forward iteration through the variant linked list (head->tail)

   @details
   This <b>helper macro</b> iterates forward through a <b>variant linked list</b> starting from the head
   <b>Warning</b>: don&rsquo;t <b>modify</b> the variant linked list during the iteration!

   @param it the name of the ${ctype} to be declared
   @param list the list
 */
#define variant_list_for_each_declare_char(it, list)     for(char** it = variant_list_firstChar(list); it; it = variant_list_iterator_nextChar(it))
#endif

/**
   @ingroup pcb_utils_variant_list
   @brief
   Definition of a helper macro for forward iteration through the variant linked list (head->tail)

   @details
   This <b>helper macro</b> iterates forward through a <b>variant linked list</b> starting from the head
   <b>Warning</b>: don&rsquo;t <b>modify</b> the variant linked list during the iteration!

   @param it the name of the ${ctype} to be declared
   @param list the list
 */
#define variant_list_for_each_char(it, list)     for(it = variant_list_firstChar(list); it; it = variant_list_iterator_nextChar(it))

/**
   @ingroup pcb_utils_variant_list
   @brief
   Get a variant list iterator data and convert it to a struct tm *

   @details
   This function is provided for convenience. It is fetching a variant in a variant list iterator
   and converts it to a struct tm * as @ref variant_toDateTime.\n
   The returned pointer MUST be freed.\n

   @param it the variant list iterator of interest

   @return
    - The converted value
 */
static inline struct tm* variant_list_iterator_dateTime(const variant_list_iterator_t* it) {
    return variant_dateTime(variant_list_iterator_data(it));
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Get a direct struct tm * pointer to a variant in a variant list iterator

   @details
   This function is provided for convenience. It is fetching a variant in a variant list iterator
   and returns a direct pointer to its struct tm * value as @ref variant_da_dateTime.\n
   The returned pointer MUST NOT be freed.\n

   @param it the variant list iterator of interest

   @return
    - The resulting struct tm * pointer
    - NULL if the variant is invalid or not of dateTime type
 */
static inline const struct tm* variant_list_iterator_da_dateTime(const variant_list_iterator_t* it) {
    return variant_da_dateTime(variant_list_iterator_data(it));
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Return the first struct tm * * iterator in a list

   @param list the list over which to iterate
   @return an iterator converted to struct tm * *
 */
static inline struct tm** variant_list_firstDateTime(const variant_list_t* list) {
    variant_list_iterator_t* next = variant_list_first(list);
    if(next == NULL) {
        return NULL;
    }
    if(variant_type(variant_list_iterator_data(next)) != variant_type_date_time) {
        return NULL;
    }
    return &variant_list_iterator_data(next)->data.dt_old;
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Return the next struct tm * * iterator in a list

   @param it the previous iterator
   @return an iterator converted to struct tm * *
 */
static inline struct tm** variant_list_iterator_nextDateTime(struct tm** it) {
    variant_list_iterator_t* prev = llist_item_data(llist_item_data(it, variant_t, data), variant_list_iterator_t, variant);
    variant_list_iterator_t* next = variant_list_iterator_next(prev);
    if(next == NULL) {
        return NULL;
    }
    if(variant_type(variant_list_iterator_data(next)) != variant_type_date_time) {
        return NULL;
    }
    return &variant_list_iterator_data(next)->data.dt_old;
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Return the last struct tm * * iterator in a list

   @param list the list over which to iterate
   @return an iterator converted to struct tm * *
 */
static inline struct tm** variant_list_lastDateTime(const variant_list_t* list) {
    variant_list_iterator_t* next = variant_list_last(list);
    if(next == NULL) {
        return NULL;
    }
    if(variant_type(variant_list_iterator_data(next)) != variant_type_date_time) {
        return NULL;
    }
    return &variant_list_iterator_data(next)->data.dt_old;
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Return the previous struct tm * * iterator in a list

   @param it the previous iterator
   @return an iterator converted to struct tm * *
 */
static inline struct tm** variant_list_iterator_prevDateTime(struct tm** it) {
    variant_list_iterator_t* prev = llist_item_data(llist_item_data(it, variant_t, data), variant_list_iterator_t, variant);
    variant_list_iterator_t* next = variant_list_iterator_prev(prev);
    if(next == NULL) {
        return NULL;
    }
    if(variant_type(variant_list_iterator_data(next)) != variant_type_date_time) {
        return NULL;
    }
    return &variant_list_iterator_data(next)->data.dt_old;
}

#if __STDC_VERSION__ >= 199901L
/**
   @ingroup pcb_utils_variant_list
   @brief
   Definition of a helper macro for forward iteration through the variant linked list (head->tail)

   @details
   This <b>helper macro</b> iterates forward through a <b>variant linked list</b> starting from the head
   <b>Warning</b>: don&rsquo;t <b>modify</b> the variant linked list during the iteration!

   @param it the name of the ${ctype} to be declared
   @param list the list
 */
#define variant_list_for_each_declare_dateTime(it, list)     for(struct tm** it = variant_list_firstDateTime(list); it; it = variant_list_iterator_nextDateTime(it))
#endif

/**
   @ingroup pcb_utils_variant_list
   @brief
   Definition of a helper macro for forward iteration through the variant linked list (head->tail)

   @details
   This <b>helper macro</b> iterates forward through a <b>variant linked list</b> starting from the head
   <b>Warning</b>: don&rsquo;t <b>modify</b> the variant linked list during the iteration!

   @param it the name of the ${ctype} to be declared
   @param list the list
 */
#define variant_list_for_each_dateTime(it, list)     for(it = variant_list_firstDateTime(list); it; it = variant_list_iterator_nextDateTime(it))

/**
   @ingroup pcb_utils_variant_list
   @brief
   Get a variant list iterator data and convert it to a pcb_datetime_t

   @details
   This function is provided for convenience. It is fetching a variant in a variant list iterator
   and converts it to a pcb_datetime_t* as @ref variant_toDateTimeExtended.\n
   The returned pointer MUST be freed.\n

   @param it the variant list iterator of interest

   @return
   - The converted value
 */
static inline pcb_datetime_t* variant_list_iterator_dateTimeExtended(const variant_list_iterator_t* it) {
    return variant_dateTimeExtended(variant_list_iterator_data(it));
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Get a direct pcb_datetime_t * pointer to a variant in a variant list iterator

   @details
   This function is provided for convenience. It is fetching a variant in a variant list iterator
   and returns a direct pointer to its pcb_datetime_t* value as @ref variant_da_dateTimeExtended.\n
   The returned pointer MUST NOT be freed.\n

   @param it the variant list iterator of interest

   @return
   - The resulting pcb_datetime_t* pointer
    - NULL if the variant is invalid or not of dateTime type
 */
static inline const pcb_datetime_t* variant_list_iterator_da_dateTimeExtended(const variant_list_iterator_t* it) {
    return variant_da_dateTimeExtended(variant_list_iterator_data(it));
}


/**
   @ingroup pcb_utils_variant_list
   @brief
   Return the first pcb_datetime_t* iterator in a list

   @param list the list over which to iterate
   @return an iterator converted to pcb_datetime_t*
 */
static inline pcb_datetime_t* variant_list_firstDateTimeExtended(const variant_list_t* list) {
    variant_list_iterator_t* next = variant_list_first(list);
    if(next == NULL) {
        return NULL;
    }
    if(variant_type(variant_list_iterator_data(next)) != variant_type_date_time) {
        return NULL;
    }
    return variant_list_iterator_data(next)->data.dt;
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Return the next pcb_datetime_t* iterator in a list

   @param it the previous iterator
   @return an iterator converted to pcb_datetime_t*
 */
static inline pcb_datetime_t* variant_list_iterator_nextDateTimeExtended(pcb_datetime_t* it) {
    variant_list_iterator_t* prev = llist_item_data(llist_item_data(it, variant_t, data), variant_list_iterator_t, variant);
    variant_list_iterator_t* next = variant_list_iterator_next(prev);
    if(next == NULL) {
        return NULL;
    }
    if(variant_type(variant_list_iterator_data(next)) != variant_type_date_time) {
        return NULL;
    }
    return variant_list_iterator_data(next)->data.dt;
}

/**
   @ ingroup pcb_utils_variant_list
   @brief
   Return the last pcb_datetime_t* iterator in a list

   @param list the list over which to iterate
   @return an iterator converted to pcb_datetime_t*
 */
static inline pcb_datetime_t* variant_list_lastDateTimeExtended(const variant_list_t* list) {
    variant_list_iterator_t* next = variant_list_last(list);
    if(next == NULL) {
        return NULL;
    }
    if(variant_type(variant_list_iterator_data(next)) != variant_type_date_time) {
        return NULL;
    }
    return variant_list_iterator_data(next)->data.dt;
}

/**
   @ ingroup pcb_utils_variant_list
   @brief
   Return the previous struct tm * * iterator in a list

   @param it the previous iterator
   @return an iterator converted to struct tm * *
 */
static inline pcb_datetime_t* variant_list_iterator_prevDateTimeExtended(pcb_datetime_t* it) {
    variant_list_iterator_t* prev = llist_item_data(llist_item_data(it, variant_t, data), variant_list_iterator_t, variant);
    variant_list_iterator_t* next = variant_list_iterator_prev(prev);
    if(next == NULL) {
        return NULL;
    }
    if(variant_type(variant_list_iterator_data(next)) != variant_type_date_time) {
        return NULL;
    }
    return variant_list_iterator_data(next)->data.dt;
}

#if __STDC_VERSION__ >= 199901L
/**
   @ ingroup pcb_utils_variant_list
   @brief
   Definition of a helper macro for forward iteration through the variant linked list (head->tail)

   @details
   This <b>helper macro</b> iterates forward through a <b>variant linked list</b> starting from the head
   <b>Warning</b>: don&rsquo;t <b>modify</b> the variant linked list during the iteration!

   @param it the name of the ${ctype} to be declared
   @param list the list
 */
#define variant_list_for_each_declare_dateTimeExtended(it, list) \
    for(pcb_datetime_t* it = variant_list_firstDateTimeExtended(list); it; it = variant_list_iterator_nextDateTimeExtended(it))
#endif

/**
   @ ingroup pcb_utils_variant_list
   @brief
   Definition of a helper macro for forward iteration through the variant linked list (head->tail)

   @details
   This <b>helper macro</b> iterates forward through a <b>variant linked list</b> starting from the head
   <b>Warning</b>: don&rsquo;t <b>modify</b> the variant linked list during the iteration!

   @param it the name of the ${ctype} to be declared
   @param list the list
 */
#define variant_list_for_each_dateTimeExtended(it, list) \
    for(it = variant_list_firstDateTimeExtended(list); it; it = variant_list_iterator_nextDateTimeExtended(it))

/**
   @ingroup pcb_utils_variant_list
   @brief
   Get a variant list iterator data and convert it to a string_t *

   @details
   This function is provided for convenience. It is fetching a variant in a variant list iterator
   and converts it to a string_t * as @ref variant_toString.\n
   The returned pointer MUST be freed.\n

   @param it the variant list iterator of interest

   @return
    - The converted value
 */
static inline string_t* variant_list_iterator_string(const variant_list_iterator_t* it) {
    return variant_string(variant_list_iterator_data(it));
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Get a direct string_t * pointer to a variant in a variant list iterator

   @details
   This function is provided for convenience. It is fetching a variant in a variant list iterator
   and returns a direct pointer to its string_t * value as @ref variant_da_string.\n
   The returned pointer MUST NOT be freed.\n

   @param it the variant list iterator of interest

   @return
    - The resulting string_t * pointer
    - NULL if the variant is invalid or not of string type
 */
static inline const string_t* variant_list_iterator_da_string(const variant_list_iterator_t* it) {
    return variant_da_string(variant_list_iterator_data(it));
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Return the first string_t *  iterator in a list

   @param list the list over which to iterate
   @return an iterator converted to string_t *
 */
static inline string_t* variant_list_firstString(const variant_list_t* list) {
    variant_list_iterator_t* next = variant_list_first(list);
    if(next == NULL) {
        return NULL;
    }
    if(variant_type(variant_list_iterator_data(next)) != variant_type_string) {
        return NULL;
    }
    return &(variant_list_iterator_data(next)->data.str);
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Return the next string_t *  iterator in a list

   @param it the previous iterator
   @return an iterator converted to string_t *
 */
static inline string_t* variant_list_iterator_nextString(string_t** it) {
    variant_list_iterator_t* prev = llist_item_data(llist_item_data(it, variant_t, data), variant_list_iterator_t, variant);
    variant_list_iterator_t* next = variant_list_iterator_next(prev);
    if(next == NULL) {
        return NULL;
    }
    if(variant_type(variant_list_iterator_data(next)) != variant_type_string) {
        return NULL;
    }
    return &(variant_list_iterator_data(next)->data.str);
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Return the last string_t *  iterator in a list

   @param list the list over which to iterate
   @return an iterator converted to string_t *
 */
static inline string_t* variant_list_lastString(const variant_list_t* list) {
    variant_list_iterator_t* next = variant_list_last(list);
    if(next == NULL) {
        return NULL;
    }
    if(variant_type(variant_list_iterator_data(next)) != variant_type_string) {
        return NULL;
    }
    return &(variant_list_iterator_data(next)->data.str);
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Return the previous string_t *  iterator in a list

   @param it the previous iterator
   @return an iterator converted to string_t *
 */
static inline string_t* variant_list_iterator_prevString(string_t** it) {
    variant_list_iterator_t* prev = llist_item_data(llist_item_data(it, variant_t, data), variant_list_iterator_t, variant);
    variant_list_iterator_t* next = variant_list_iterator_prev(prev);
    if(next == NULL) {
        return NULL;
    }
    if(variant_type(variant_list_iterator_data(next)) != variant_type_string) {
        return NULL;
    }
    return &(variant_list_iterator_data(next)->data.str);
}

#if __STDC_VERSION__ >= 199901L
/**
   @ingroup pcb_utils_variant_list
   @brief
   Definition of a helper macro for forward iteration through the variant linked list (head->tail)

   @details
   This <b>helper macro</b> iterates forward through a <b>variant linked list</b> starting from the head
   <b>Warning</b>: don&rsquo;t <b>modify</b> the variant linked list during the iteration!

   @param it the name of the ${ctype} to be declared
   @param list the list
 */
#define variant_list_for_each_declare_string(it, list)     for(string_t* it = variant_list_firstString(list); it; it = variant_list_iterator_nextString(it))
#endif

/**
   @ingroup pcb_utils_variant_list
   @brief
   Definition of a helper macro for forward iteration through the variant linked list (head->tail)

   @details
   This <b>helper macro</b> iterates forward through a <b>variant linked list</b> starting from the head
   <b>Warning</b>: don&rsquo;t <b>modify</b> the variant linked list during the iteration!

   @param it the name of the ${ctype} to be declared
   @param list the list
 */
#define variant_list_for_each_string(it, list)     for(it = variant_list_firstString(list); it; it = variant_list_iterator_nextString(it))

/**
   @ingroup pcb_utils_variant_list
   @brief
   Get a variant list iterator data and convert it to a variant_list_t *

   @details
   This function is provided for convenience. It is fetching a variant in a variant list iterator
   and converts it to a variant_list_t * as @ref variant_toList.\n
   The returned pointer MUST be freed.\n

   @param it the variant list iterator of interest

   @return
    - The converted value
 */
static inline variant_list_t* variant_list_iterator_list(const variant_list_iterator_t* it) {
    return variant_list(variant_list_iterator_data(it));
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Get a direct variant_list_t * pointer to a variant in a variant list iterator

   @details
   This function is provided for convenience. It is fetching a variant in a variant list iterator
   and returns a direct pointer to its variant_list_t * value as @ref variant_da_list.\n
   The returned pointer MUST NOT be freed.\n

   @param it the variant list iterator of interest

   @return
    - The resulting variant_list_t * pointer
    - NULL if the variant is invalid or not of list type
 */
static inline variant_list_t* variant_list_iterator_da_list(const variant_list_iterator_t* it) {
    return variant_da_list(variant_list_iterator_data(it));
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Return the first variant_list_t * * iterator in a list

   @param list the list over which to iterate
   @return an iterator converted to variant_list_t * *
 */
static inline variant_list_t** variant_list_firstList(const variant_list_t* list) {
    variant_list_iterator_t* next = variant_list_first(list);
    if(next == NULL) {
        return NULL;
    }
    if(variant_type(variant_list_iterator_data(next)) != variant_type_array) {
        return NULL;
    }
    return &(variant_list_iterator_data(next)->data.vl);
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Return the next variant_list_t * * iterator in a list

   @param it the previous iterator
   @return an iterator converted to variant_list_t * *
 */
static inline variant_list_t** variant_list_iterator_nextList(variant_list_t** it) {
    variant_list_iterator_t* prev = llist_item_data(llist_item_data(it, variant_t, data), variant_list_iterator_t, variant);
    variant_list_iterator_t* next = variant_list_iterator_next(prev);
    if(next == NULL) {
        return NULL;
    }
    if(variant_type(variant_list_iterator_data(next)) != variant_type_array) {
        return NULL;
    }
    return &(variant_list_iterator_data(next)->data.vl);
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Return the last variant_list_t * * iterator in a list

   @param list the list over which to iterate
   @return an iterator converted to variant_list_t * *
 */
static inline variant_list_t** variant_list_lastList(const variant_list_t* list) {
    variant_list_iterator_t* next = variant_list_last(list);
    if(next == NULL) {
        return NULL;
    }
    if(variant_type(variant_list_iterator_data(next)) != variant_type_array) {
        return NULL;
    }
    return &(variant_list_iterator_data(next)->data.vl);
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Return the previous variant_list_t * * iterator in a list

   @param it the previous iterator
   @return an iterator converted to variant_list_t * *
 */
static inline variant_list_t** variant_list_iterator_prevList(variant_list_t** it) {
    variant_list_iterator_t* prev = llist_item_data(llist_item_data(it, variant_t, data), variant_list_iterator_t, variant);
    variant_list_iterator_t* next = variant_list_iterator_prev(prev);
    if(next == NULL) {
        return NULL;
    }
    if(variant_type(variant_list_iterator_data(next)) != variant_type_array) {
        return NULL;
    }
    return &(variant_list_iterator_data(next)->data.vl);
}

#if __STDC_VERSION__ >= 199901L
/**
   @ingroup pcb_utils_variant_list
   @brief
   Definition of a helper macro for forward iteration through the variant linked list (head->tail)

   @details
   This <b>helper macro</b> iterates forward through a <b>variant linked list</b> starting from the head
   <b>Warning</b>: don&rsquo;t <b>modify</b> the variant linked list during the iteration!

   @param it the name of the ${ctype} to be declared
   @param list the list
 */
#define variant_list_for_each_declare_list(it, list)     for(variant_list_t** it = variant_list_firstList(list); it; it = variant_list_iterator_nextList(it))
#endif

/**
   @ingroup pcb_utils_variant_list
   @brief
   Definition of a helper macro for forward iteration through the variant linked list (head->tail)

   @details
   This <b>helper macro</b> iterates forward through a <b>variant linked list</b> starting from the head
   <b>Warning</b>: don&rsquo;t <b>modify</b> the variant linked list during the iteration!

   @param it the name of the ${ctype} to be declared
   @param list the list
 */
#define variant_list_for_each_list(it, list)     for(it = variant_list_firstList(list); it; it = variant_list_iterator_nextList(it))

/**
   @ingroup pcb_utils_variant_list
   @brief
   Get a variant list iterator data and convert it to a variant_map_t *

   @details
   This function is provided for convenience. It is fetching a variant in a variant list iterator
   and converts it to a variant_map_t * as @ref variant_toMap.\n
   The returned pointer MUST be freed.\n

   @param it the variant list iterator of interest

   @return
    - The converted value
 */
static inline variant_map_t* variant_list_iterator_map(const variant_list_iterator_t* it) {
    return variant_map(variant_list_iterator_data(it));
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Get a direct variant_map_t * pointer to a variant in a variant list iterator

   @details
   This function is provided for convenience. It is fetching a variant in a variant list iterator
   and returns a direct pointer to its variant_map_t * value as @ref variant_da_map.\n
   The returned pointer MUST NOT be freed.\n

   @param it the variant list iterator of interest

   @return
    - The resulting variant_map_t * pointer
    - NULL if the variant is invalid or not of map type
 */
static inline variant_map_t* variant_list_iterator_da_map(const variant_list_iterator_t* it) {
    return variant_da_map(variant_list_iterator_data(it));
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Return the first variant_map_t * * iterator in a list

   @param list the list over which to iterate
   @return an iterator converted to variant_map_t * *
 */
static inline variant_map_t** variant_list_firstMap(const variant_list_t* list) {
    variant_list_iterator_t* next = variant_list_first(list);
    if(next == NULL) {
        return NULL;
    }
    if(variant_type(variant_list_iterator_data(next)) != variant_type_map) {
        return NULL;
    }
    return &(variant_list_iterator_data(next)->data.vm);
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Return the next variant_map_t * * iterator in a list

   @param it the previous iterator
   @return an iterator converted to variant_map_t * *
 */
static inline variant_map_t** variant_list_iterator_nextMap(variant_map_t** it) {
    variant_list_iterator_t* prev = llist_item_data(llist_item_data(it, variant_t, data), variant_list_iterator_t, variant);
    variant_list_iterator_t* next = variant_list_iterator_next(prev);
    if(next == NULL) {
        return NULL;
    }
    if(variant_type(variant_list_iterator_data(next)) != variant_type_map) {
        return NULL;
    }
    return &(variant_list_iterator_data(next)->data.vm);
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Return the last variant_map_t * * iterator in a list

   @param list the list over which to iterate
   @return an iterator converted to variant_map_t * *
 */
static inline variant_map_t** variant_list_lastMap(const variant_list_t* list) {
    variant_list_iterator_t* next = variant_list_last(list);
    if(next == NULL) {
        return NULL;
    }
    if(variant_type(variant_list_iterator_data(next)) != variant_type_map) {
        return NULL;
    }
    return &(variant_list_iterator_data(next)->data.vm);
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Return the previous variant_map_t * * iterator in a list

   @param it the previous iterator
   @return an iterator converted to variant_map_t * *
 */
static inline variant_map_t** variant_list_iterator_prevMap(variant_map_t** it) {
    variant_list_iterator_t* prev = llist_item_data(llist_item_data(it, variant_t, data), variant_list_iterator_t, variant);
    variant_list_iterator_t* next = variant_list_iterator_prev(prev);
    if(next == NULL) {
        return NULL;
    }
    if(variant_type(variant_list_iterator_data(next)) != variant_type_map) {
        return NULL;
    }
    return &(variant_list_iterator_data(next)->data.vm);
}

#if __STDC_VERSION__ >= 199901L
/**
   @ingroup pcb_utils_variant_list
   @brief
   Definition of a helper macro for forward iteration through the variant linked list (head->tail)

   @details
   This <b>helper macro</b> iterates forward through a <b>variant linked list</b> starting from the head
   <b>Warning</b>: don&rsquo;t <b>modify</b> the variant linked list during the iteration!

   @param it the name of the ${ctype} to be declared
   @param list the list
 */
#define variant_list_for_each_declare_map(it, list)     for(variant_map_t** it = variant_list_firstMap(list); it; it = variant_list_iterator_nextMap(it))
#endif

/**
   @brief
   Definition of a helper macro for forward iteration through the variant linked list (head->tail)

   @details
   This <b>helper macro</b> iterates forward through a <b>variant linked list</b> starting from the head
   <b>Warning</b>: don&rsquo;t <b>modify</b> the variant linked list during the iteration!

   @param it the name of the ${ctype} to be declared
   @param list the list
 */
#define variant_list_for_each_map(it, list)     for(it = variant_list_firstMap(list); it; it = variant_list_iterator_nextMap(it))

/**
   @ingroup pcb_utils_variant_list
   @brief
   Get a variant list iterator data and convert it to a int

   @details
   This function is provided for convenience. It is fetching a variant in a variant list iterator
   and converts it to a int as @ref variant_toFd.\n
   The returned pointer MUST be freed.\n

   @param it the variant list iterator of interest

   @return
    - The converted value
 */
static inline int variant_list_iterator_fd(const variant_list_iterator_t* it) {
    return variant_fd(variant_list_iterator_data(it));
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Return the first int * iterator in a list

   @param list the list over which to iterate
   @return an iterator converted to int *
 */
static inline int* variant_list_firstFd(const variant_list_t* list) {
    variant_list_iterator_t* next = variant_list_first(list);
    if(next == NULL) {
        return NULL;
    }
    if(variant_type(variant_list_iterator_data(next)) != variant_type_file_descriptor) {
        return NULL;
    }
    return &(variant_list_iterator_data(next)->data.fd);
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Return the next int * iterator in a list

   @param it the previous iterator
   @return an iterator converted to int *
 */
static inline int* variant_list_iterator_nextFd(int* it) {
    variant_list_iterator_t* prev = llist_item_data(llist_item_data(it, variant_t, data), variant_list_iterator_t, variant);
    variant_list_iterator_t* next = variant_list_iterator_next(prev);
    if(next == NULL) {
        return NULL;
    }
    if(variant_type(variant_list_iterator_data(next)) != variant_type_file_descriptor) {
        return NULL;
    }
    return &(variant_list_iterator_data(next)->data.fd);
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Return the last int * iterator in a list

   @param list the list over which to iterate
   @return an iterator converted to int *
 */
static inline int* variant_list_lastFd(const variant_list_t* list) {
    variant_list_iterator_t* next = variant_list_last(list);
    if(next == NULL) {
        return NULL;
    }
    if(variant_type(variant_list_iterator_data(next)) != variant_type_file_descriptor) {
        return NULL;
    }
    return &(variant_list_iterator_data(next)->data.fd);
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Return the previous int * iterator in a list

   @param it the previous iterator
   @return an iterator converted to int *
 */
static inline int* variant_list_iterator_prevFd(int* it) {
    variant_list_iterator_t* prev = llist_item_data(llist_item_data(it, variant_t, data), variant_list_iterator_t, variant);
    variant_list_iterator_t* next = variant_list_iterator_prev(prev);
    if(next == NULL) {
        return NULL;
    }
    if(variant_type(variant_list_iterator_data(next)) != variant_type_file_descriptor) {
        return NULL;
    }
    return &(variant_list_iterator_data(next)->data.fd);
}

#if __STDC_VERSION__ >= 199901L
/**
   @ingroup pcb_utils_variant_list
   @brief
   Definition of a helper macro for forward iteration through the variant linked list (head->tail)

   @details
   This <b>helper macro</b> iterates forward through a <b>variant linked list</b> starting from the head
   <b>Warning</b>: don&rsquo;t <b>modify</b> the variant linked list during the iteration!

   @param it the name of the ${ctype} to be declared
   @param list the list
 */
#define variant_list_for_each_declare_fd(it, list)     for(int* it = variant_list_firstFd(list); it; it = variant_list_iterator_nextFd(it))
#endif

/**
   @ingroup pcb_utils_variant_list
   @brief
   Definition of a helper macro for forward iteration through the variant linked list (head->tail)

   @details
   This <b>helper macro</b> iterates forward through a <b>variant linked list</b> starting from the head
   <b>Warning</b>: don&rsquo;t <b>modify</b> the variant linked list during the iteration!

   @param it the name of the ${ctype} to be declared
   @param list the list
 */
#define variant_list_for_each_fd(it, list)     for(it = variant_list_firstFd(list); it; it = variant_list_iterator_nextFd(it))

/**
   @ingroup pcb_utils_variant_list
   @brief
   Get a variant list iterator data and convert it to a void *

   @details
   This function is provided for convenience. It is fetching a variant in a variant list iterator
   and converts it to a void * as @ref variant_toByteArray.\n
   The returned pointer MUST be freed.\n

   @param it the variant list iterator of interest
   @param size pointer to an unsigned 32 bit integer where the size of the binary data block can be stored

   @return
    - The converted value
 */
static inline void* variant_list_iterator_byteArray(const variant_list_iterator_t* it, uint32_t* size) {
    return variant_byteArray(variant_list_iterator_data(it), size);
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Get a direct void * pointer to a variant in a variant list iterator

   @details
   This function is provided for convenience. It is fetching a variant in a variant list iterator
   and returns a direct pointer to its void * value as @ref variant_da_byteArray.\n
   The returned pointer MUST NOT be freed.\n

   @param it the variant list iterator of interest
   @param size pointer to an unsigned 32 bit integer where the size of the binary data block can be stored

   @return
    - The resulting void * pointer
    - NULL if the variant is invalid or not of byteArray type
 */
static inline const void* variant_list_iterator_da_byteArray(const variant_list_iterator_t* it, uint32_t* size) {
    return variant_da_byteArray(variant_list_iterator_data(it), size);
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Get a variant list iterator data and convert it to a string_list_t *

   @details
   This function is provided for convenience. It is fetching a variant in a variant list iterator
   and converts it to a string_list_t * as @ref variant_toStringList.\n
   The returned pointer MUST be freed.\n

   @param it the variant list iterator of interest
   @param separator string that is used as a separator, to split the data

   @return
    - The converted value
 */
static inline string_list_t* variant_list_iterator_stringList(const variant_list_iterator_t* it, const char* separator) {
    return variant_stringList(variant_list_iterator_data(it), separator);
}

#ifdef __cplusplus
}
#endif
#endif
