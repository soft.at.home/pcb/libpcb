/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(PCB_PRIVILEGE_H)
#define PCB_PRIVILEGE_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdbool.h>
#include <stdint.h>
#include <linux/capability.h>
#include <pwd.h>

typedef struct _priv_cap {
    uint32_t cap[_LINUX_CAPABILITY_U32S_3];
} priv_cap_t;

typedef int cap_id_t;

#define PRIV_CAP_FROM_STRING_INVALID ((cap_id_t) -1)
cap_id_t priv_cap_fromString(const char* cap_str);


const char* priv_cap_toString(cap_id_t cap);

bool priv_cap_initialize(priv_cap_t* capt);
void priv_cap_cleanup(priv_cap_t* capt);
bool priv_cap_set(priv_cap_t* capt, cap_id_t cap);
bool priv_cap_clear(priv_cap_t* capt, cap_id_t cap);
bool priv_cap_isSet(const priv_cap_t* capt, cap_id_t cap);

bool priv_proc_initialize(void);
void priv_proc_cleanup(void);
int priv_proc_version(void);
bool priv_proc_getCap(priv_cap_t* capt);
bool priv_proc_setCap(const priv_cap_t* capt);
bool priv_proc_dropPrivileges(const char* user, const char* group, const priv_cap_t* retain);

bool priv_get_uid(const char* username, uid_t* id);
bool priv_get_gid(const char* groupname, gid_t* id);
bool priv_get_default_uid_and_gid(uid_t* uid, gid_t* gid);

const char* priv_get_defaultUser(void);
const char* priv_get_defaultGroup(void);
const priv_cap_t* priv_get_defaultRetain(void);
bool priv_set_defaultUser(const char* user);
bool priv_set_defaultGroup(const char* group);
bool priv_set_defaultRetain(const priv_cap_t* retain);

#ifdef __cplusplus
}
#endif

#endif


