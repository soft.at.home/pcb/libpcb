/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

/**
   @file
   @brief
   Header file that includes all utility header files
 */

/**
   @defgroup pcb_utils Utilities
   @{
   @brief
   Collection of utilities.

   @details
   The pcb utilities is a collection of functions intended to make development of common tasks easier.\n
   The uitilities includes:
    - @ref pcb_utils_containers
    - @ref pcb_utils_buffers
    - @ref pcb_utils_uri
   @}
 */

/**
   @ingroup pcb_utils
   @defgroup pcb_utils_buffers Buffering
   @{
   @brief
   Collection of buffering utilities.

   @details
   Buffering methods, helps in allocating dynamic buffers and managing the buffers
   @}
 */
#include <pcb/utils/circular_buffer.h>

/**
   @ingroup pcb_utils
   @defgroup pcb_utils_containers Containers
   @{
   @brief
   Implementation of some simple containers.

   @details
   Implementation of some simple containers. The following containers are implemented:
    - doubly linked list: @ref pcb_utils_linked_list
    - date time: A container for a date/time with a resolution of 1 nanosecond, see @ref pcb_utils_datetime
    - string: string helper functions: @ref pcb_utils_string
    - string list: linked list contaning strings: @ref pcb_utils_string_list
    - variant: a container that contain a lot of different data types: @ref pcb_utils_variant
    - variant list: linked list of variants: @ref pcb_utils_variant_list
    - variant map: linked list of key-value pairs, where the value is represented by a variant: @ref pcb_utils_variant_map
   @}
 */
#include <pcb/utils/linked_list.h>
#include <pcb/utils/hash_table.h>
#include <pcb/utils/datetime.h>
#include <pcb/utils/string.h>
#include <pcb/utils/string_list.h>
#include <pcb/utils/tree.h>
#include <pcb/utils/variant.h>
#include <pcb/utils/variant_list.h>
#include <pcb/utils/variant_map.h>
#include <pcb/utils/variant_sets.h>

/**
   @ingroup pcb_utils
   @defgroup pcb_utils_uri URI Parsing
   @{
   @brief
   Helper functions to parse a URI.\n

   @details
   A set of functions to parse a URI from a string.\n
   @}
 */
#include <pcb/utils/uri.h>

/**
   @defgroup pcb_utils_socket_events Sockets, Files, Processes and Eventing
   @{
   @brief
   Filed descriptor and event handling

   @details
   Collection of functions to help in creating and managing sockets and file descriptors, dispatch events and
   building event loops.\n
   This includes:
    - Timers: @ref pcb_utils_timers
    - Socket creation and event handling: @ref pcb_socket_layer_connections
    - Managing sockets and file descriptors: @ref pcb_socket_layer_peer
    - Managing child processes: @ref pcb_process_layer
   @}
 */
#include <pcb/utils/connection.h>
#include <pcb/utils/peer.h>
#include <pcb/utils/timer.h>
#include <pcb/utils/process.h>
#include <pcb/utils/sendfile.h>
