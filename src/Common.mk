LIBS += sahtrace

CFLAGS += -Wall -Wextra -Werror -fPIC -I../include -I../include_priv/utils -I../include_priv/core -I$(STAGINGDIR)/include -DNDEBUG $(shell pkg-config --cflags $(LIBS)) -pthread -std=gnu99
LDFLAGS += -L$(STAGINGDIR)/lib -ldl $(shell pkg-config --libs $(LIBS)) -lpthread $(LOCAL_LIBS)

all: $(TARGET).so $(TARGET).a

$(TARGET).so: $(OBJECTS)
	$(CC) -Wl,-soname,$(@) -shared -fPIC -o $(@) $(OBJECTS) $(LDFLAGS)

-include $(OBJECTS:.o=.d)

$(TARGET).a: $(OBJECTS)
	$(AR) rcs $(@) $^

%.o: %.c
	$(CC) $(CFLAGS) -c -o $@ $<
	@$(CC) $(CFLAGS) -MM -MP -MT '$(@) $(@:.o=.d)' -MF $(@:.o=.d) $(<)

.PHONY: all
