ifndef STAGINGDIR
  $(error STAGINGDIR not defined, stopping build)
endif

include $(STAGINGDIR)/components.config

OBJECTS_CORE   = core/datamodel.o \
                 core/object.o \
                 core/parameter.o \
                 core/function.o \
                 core/reply.o \
                 core/request.o \
                 core/requesthandlers.o \
                 core/serialize.o \
                 core/validator.o \
                 core/notification.o \
                 core/pcb_main.o \
                 core/pcb_wait.o \
                 core/pcb_client.o \
                 core/mapping.o \
                 core/acl.o \
                 core/default_rpc.o

OBJECTS_PRIV = core/object_priv.o \
               core/parameter_priv.o \
               core/local_object.o \
               core/remote_object.o \
               core/core_priv.o \
               core/path.o

OBJECTS = $(OBJECTS_CORE) $(OBJECTS_PRIV)

TARGET = libpcb_dm

ifeq ($(CONFIG_PCB_ACL_USERMNGT),y)
  LIBS = usermngt
endif

ifeq ($(CONFIG_PCB_HELP_SUPPORT),y)
  CFLAGS += -DPCB_HELP_SUPPORT
endif

LOCAL_LIBS = -L. -lpcb_utils

include Common.mk

clean:
	rm -f $(TARGET).so $(OBJECTS) $(TARGET).a
	rm -f core/*.d
	rm -f gcno core/*.gcno
	rm -f gcda core/*.gcda
	rm -f *.gcov

.PHONY: clean
