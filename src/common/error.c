/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>

#include <debug/sahtrace.h>

#include <pcb/common/error.h>

#if defined(TLS_AVAILABLE)
__thread uint32_t pcb_error = pcb_ok;
#else
uint32_t pcb_error = pcb_ok;
#endif

/**
   @file
   @brief
   Implementation of error functions
 */

const char* pcb_common_errors(uint32_t error) {
    switch(error) {
    case pcb_error_common:
        return "Common error";
    case pcb_error_invalid_parameter:
        return "Invalid function parameter";
    case pcb_error_no_memory:
        return "Out of memory";
    case pcb_error_unknown_system_error:
        return "Unknown system error";
    default:
        return "Unknown error in group \"common\"";
    }
}

const char* pcb_utils_errors(uint32_t error) {
    switch(error) {
    case pcb_error_utils:
        return "utils error";
    case pcb_error_insert_before_begin:
        return "Can not insert an element in a list before the beginning of the list";
    case pcb_error_insert_after_end:
        return "Can not insert an element in a list after the end of the list";
    case pcb_error_can_not_remove_centinels:
        return "Can not remove begin or end of the list";
    case pcb_error_out_of_boundaries:
        return "Ourside the bounderaries of a string or a list";
    case pcb_error_substring_not_found:
        return "Substring not found";
    case pcb_error_list_empty:
        return "The list is empty";
    case pcb_error_invalid_variant_type:
        return "Invalid variant type specified";
    case pcb_error_invalid_variant_conversion:
        return "Can not convert the variant to the requested type";
    case pcb_error_element_not_found:
        return "Element not found in the list";
    case pcb_error_time_conversion:
        return "Could not convert time value to platform time: Y2038 issue or time_t underflow";
    default:
        return "Unknown error in group \"utils\"";
    }
}

const char* pcb_core_errors(uint32_t error) {
    switch(error) {
    case pcb_error_core:
        return "Core error";
    case pcb_error_empty_name:
        return "Object or parameter name can not be empty";
    case pcb_error_remote_local_mismatch:
        return "Operation not allowed on this kind of object";
    case pcb_error_wrong_state:
        return "Object or parameter is in the wrong state for the operation";
    case pcb_error_no_parent:
        return "Object has no parent (is root object)";
    case pcb_error_not_template:
        return "Object is not a template object";
    case pcb_error_max_instances_reached:
        return "Maximum possible instances of the object is reached";
    case pcb_error_not_instance:
        return "Object is not an instance";
    case pcb_error_last_instance:
        return "Object was the last in the instance list";
    case pcb_error_last_parameter:
        return "Parameter was the last in the parameter list";
    case pcb_error_not_found:
        return "Object or parameter not found";
    case pcb_error_parent_not_committed:
        return "Object can not be committed as long as it's parent is not committed";
    case pcb_error_not_unique_name:
        return "Object or parameter has no unique name within it's containing object";
    case pcb_error_invalid_name:
        return "Object or parameter has no valid name";
    case pcb_error_unknown_type:
        return "Parameter type is not known";
    case pcb_error_value_empty:
        return "Mandatory parameters or key parameters can not be empty";
    case pcb_error_value_enum:
        return "Parameter value is not in the list of possible values";
    case pcb_error_value_range:
        return "Parameter value is not in the specified value range";
    case pcb_error_string_length:
        return "String parameter's string length is not in the specified range";
    case pcb_error_value_minimum:
        return "Parameter value is below the specified minimum";
    case pcb_error_value_maximum:
        return "Parameter value is above the specified maximum";
    case pcb_error_validator_mismatch:
        return "Validator type is not matching the validation function";
    case pcb_error_type_mismatch:
        return "The parameter or validator values can not be converted to the correct type";
    case pcb_error_read_only:
        return "Object or parameter is read-only";
    case pcb_error_invalid_notification_type:
        return "Invalid notification type";
    case pcb_error_wrong_object_type:
        return "Object is of the wrong kind for this operation";
    case pcb_error_last_function:
        return "Error last function";
    case pcb_error_function_object_mismatch:
        return "Function can not be executed for the specified object";
    case pcb_error_function_not_implemented:
        return "Function is not implemented";
    case pcb_error_canceled:
        return "Operation is canceled";
    case pcb_error_invalid_value:
        return "Invalid parameter value";
    case pcb_error_function_exec_failed:
        return "Function execution failed";
    case pcb_error_function_argument_missing:
        return "Mandatory argument missing";
    case pcb_error_object_ismib:
        return "Operation not allowed on a MIB object";
    case pcb_error_access_denied:
        return "Access denied for this parameter";
    case pcb_error_parameter_not_found:
        return "Parameter not found";
    case pcb_error_duplicate:
        return "The operation would result in an instance with duplicate keys";
    default:
        return "Unknown error in group \"core\"";
    }
}

const char* pcb_communication_errors(uint32_t error) {
    switch(error) {
    case pcb_error_communication:
        return "Communication error";
    case pcb_error_already_listening:
        return "Already listening";
    case pcb_error_already_connected:
        return "Already connected";
    case pcb_error_not_connected:
        return "Not connected";
    case pcb_error_connection_shutdown:
        return "Connection is closed by peer";
    case pcb_error_datamodel_not_connected:
        return "The datamodel does not belong to a connection";
    case pcb_error_invalid_header:
        return "Invalid message header received";
    case pcb_error_invalid_data_format:
        return "Invalid or unknown data format";
    case pcb_error_invalid_request:
        return "Invalid or unknown request";
    case pcb_error_not_supported_request:
        return "Request is not supported";
    case pcb_error_invalid_reply:
        return "Invalid reply";
    case pcb_error_no_serializer:
        return "No (de)serializer available";
    case pcb_error_time_out:
        return "Time-out";
    default:
        return "Unknown error in group \"communication\"";
    }
}

/**
   @ingroup pcb_common_error
   @{
 */

/**
   @brief
   Convert an error code into a human readable string.

   @details
   This function converts a error code into a human readable string.

   @param error the error code

   @return
    - a human readable string
 */
const char* error_string(uint32_t error) {
    switch(PCB_ERROR_GROUP(error)) {
    case pcb_error_group_system:
        return strerror(error);
    case pcb_error_group_common:
        return pcb_common_errors(error);
    case pcb_error_group_utils:
        return pcb_utils_errors(error);
    case pcb_error_group_core:
        return pcb_core_errors(error);
    case pcb_error_group_communication:
        return pcb_communication_errors(error);
    case pcb_error_group_getaddrinfo:
        return gai_strerror(-(error & 0x0000FFFF));
    default:
        SAH_TRACEZ_NOTICE("pcb_com", "Unknown error %d", error);
        return "Unknown error group";
    }
}

/**
   @}
 */
