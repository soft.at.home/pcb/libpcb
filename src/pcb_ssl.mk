ifndef STAGINGDIR
  $(error STAGINGDIR not defined, stopping build)
endif

include $(STAGINGDIR)/components.config

OBJECTS = utils/connection_ssl.o \
          utils/peer_ssl.o \
          utils/process_ssl.o

TARGET = libpcb_ssl

CFLAGS += -DOPEN_SSL_SUPPORT

LIBS = openssl

LOCAL_LIBS = -L. -lpcb_utils -lrt

include Common.mk

utils/connection_ssl.o: ../include_priv/utils/connection_dh2048.h

../include_priv/utils/connection_dh2048.h:
	@echo "Validating openssl version: $(shell openssl version)"
ifneq (,$(findstring OpenSSL 1.1.,$(shell openssl version)))
	@echo "Compatible - Generate dhparams"
	openssl dhparam -noout -C 2048 > $@
else
	@echo "Incompatible - Copy dhparams"
	cp ../include_priv/utils/connection_dh2048_default.h $@
endif

clean:
	rm -f $(TARGET).so $(OBJECTS) $(TARGET).a
	rm -f utils/*.d
	rm -f ../include_priv/utils/connection_dh2048.h
	rm -f utils/*.gcno
	rm -f utils/*.gcda
	rm -f *.gcov

.PHONY: all clean
