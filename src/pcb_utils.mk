ifndef STAGINGDIR
  $(error STAGINGDIR not defined, stopping build)
endif

include $(STAGINGDIR)/components.config

OBJECTS_COMMON = common/error.o

OBJECTS_UTILS = utils/linked_list.o \
                utils/hash_table.o \
                utils/datetime.o \
                utils/string.o \
                utils/string_conversion.o \
                utils/string_list.o \
                utils/tree.o \
                utils/variant.o \
                utils/variant_string.o \
                utils/variant_list.o \
                utils/variant_map.o \
                utils/variant_sets.o \
                utils/circular_buffer.o \
                utils/timer.o \
                utils/uri.o \
                utils/privilege.o

OBJECTS = $(OBJECTS_UTILS) $(OBJECTS_COMMON)

TARGET = libpcb_utils

LOCAL_LIBS = -lrt

include Common.mk

clean:
	rm -f $(TARGET).so $(OBJECTS) $(TARGET).a
	rm -f common/*.d utils/*.d
	rm -f common/*.gcno utils/*.gcno
	rm -f common/*.gcda utils/*.gcda
	rm -f *.gcov

.PHONY: clean
