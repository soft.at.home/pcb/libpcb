/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <debug/sahtrace.h>

#include <pcb/common/error.h>
#include <pcb/utils/string_list.h>
#include <pcb/utils/peer.h>

#include "reply_priv.h"
#include "request_priv.h"
#include "notification_priv.h"
#include "pcb_main_priv.h"

static request_object_t* request_object_create(const char* objectPath, uint32_t depth, uint32_t attributes) {
    if(!objectPath) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    size_t pathlen = strlen(objectPath);
    request_object_t* req = (request_object_t*) calloc(1, sizeof(request_object_t));
    if(!req) {
        pcb_error = pcb_error_no_memory;
        goto error_free_req;
    }

    req->UID = UINT32_MAX;
    req->sourcePID = getpid();

    llist_initialize(&req->parameters);
    req->depth = depth;
    req->attributes = attributes;

    req->objectPath = (char*) calloc(1, pathlen + 1);
    if(!req->objectPath) {
        pcb_error = pcb_error_no_memory;
        goto error_free_path;
    }
    if((objectPath[0] == '.') || (objectPath[0] == '/')) {
        strcpy(req->objectPath, objectPath + 1);
    } else {
        strcpy(req->objectPath, objectPath);
    }

    if(pathlen > 0) {
        if((objectPath[pathlen - 1] == '.') || (objectPath[pathlen - 1] == '/')) {
            req->objectPath[pathlen - 1] = 0;
        }
    }

    return req;

error_free_path:
    free(req->objectPath);
error_free_req:
    free(req);
    return NULL;
}

static bool request_initialize(request_t* req) {
    llist_initialize(&req->reply.items);
    llist_iterator_initialize(&req->it);
    llist_iterator_initialize(&req->notify_it);
    llist_iterator_initialize(&req->forwarded_it);
    llist_iterator_initialize(&req->list_it);

    if(!tree_item_initialize(&req->child_requests)) {
        return false;
    }

    req->state = request_state_created;

    return true;
}

static bool request_childReply(request_t* req, pcb_t* pcb, peer_info_t* from, void* userdata) {
    (void) userdata;
    SAH_TRACEZ_IN("pcb");
    tree_item_t* parent = tree_item_parent(&req->child_requests);
    if(!parent) {
        SAH_TRACEZ_WARNING("pcb", "No parent request, closed or canceled?");
        SAH_TRACEZ_OUT("pcb");
        return false;
    }

    request_t* orig_req = tree_item_data(parent, request_t, child_requests);

    reply_t* reply = request_reply(req);
    reply_t* reply_orig = request_reply(orig_req);
    reply_item_t* item = reply_firstItem(reply);
    reply_item_t* prefetch = reply_nextItem(item);
    if(!reply) {
        SAH_TRACEZ_OUT("pcb");
        return true;
    }

    if(request_attributes(orig_req) & request_getObject_keep_hierarchy) {
        object_t* root = pcb_getRootObject(pcb, req);

        while(item) {
            switch(reply_item_type(item)) {
            case reply_type_object: {
                object_t* object = reply_item_object(item);
                if(local_object_parent(object) == root) {
                    SAH_TRACEZ_INFO("pcb", "adding object to original reply");
                    llist_append(&reply_orig->items, &item->it);
                }
            }
            break;
            case reply_type_error: {
                item_error_t* item_error = llist_item_data(item, item_error_t, item);
                llist_append(&reply_orig->errors, &item_error->it);
                /* move the reply item to the original request */
                llist_append(&reply_orig->items, &item->it);
            }
            break;
            default:
                /* move the reply item to the original request */
                llist_append(&reply_orig->items, &item->it);
                break;
            }
            item = prefetch;
            prefetch = reply_nextItem(item);
        }
    } else {
        while(item) {
            /* move the reply item to the original request */
            llist_append(&reply_orig->items, &item->it);
            if(reply_item_type(item) == reply_type_error) {
                item_error_t* item_error = llist_item_data(item, item_error_t, item);
                llist_append(&reply_orig->errors, &item_error->it);
            }
            item = prefetch;
            prefetch = reply_nextItem(item);
        }
    }

    bool retval = true;
    /* call the original request reply callback */
    if(orig_req->replyChildHandler) {
        SAH_TRACEZ_INFO("pcb", "Request reply");
        retval = orig_req->replyChildHandler(req, orig_req, pcb, from, orig_req->userdata);
        if(retval) {
            /* clear the reply items */
            reply_clear(&orig_req->reply);
        }
    } else if(orig_req->replyHandler) {
        SAH_TRACEZ_INFO("pcb", "Parent request reply");
        retval = orig_req->replyHandler(orig_req, pcb, from, orig_req->userdata);
        if(retval) {
            /* clear the reply items */
            reply_clear(&orig_req->reply);
        }
    }

    SAH_TRACEZ_OUT("pcb");
    return retval;
}

static bool request_childDone(request_t* req, pcb_t* pcb, peer_info_t* from, void* userdata) {
    (void) userdata;
    SAH_TRACEZ_IN("pcb");
    tree_item_t* parent = tree_item_parent(&req->child_requests);
    if(!parent) {
        SAH_TRACEZ_WARNING("pcb", "No parent request, closed or canceled?");
        request_destroy(req);
        SAH_TRACEZ_OUT("pcb");
        return false;
    }

    request_t* orig_req = tree_item_data(parent, request_t, child_requests);

    /* remove this child request */
    if((request_attributes(req) & request_notify_all) == 0) {
        SAH_TRACEZ_INFO("pcb", "Remove and destroy child request");
        request_destroy(req);
    }

    bool retval = true;
    SAH_TRACEZ_INFO("pcb", "child request done, call parent request done");
    /* call the orignal request done handler */
    if(orig_req->doneHandler) {
        retval = orig_req->doneHandler(orig_req, pcb, from, orig_req->userdata);
    }
    SAH_TRACEZ_INFO("pcb", "Original request state = %d", orig_req->state);
    if((orig_req->state == request_state_destroyed) && (pcb->request_id == 0)) {
        SAH_TRACEZ_INFO("pcb", "Destroy original request");
        request_destroy(orig_req);
    }

    SAH_TRACEZ_OUT("pcb");
    return retval;
}

static bool request_childDone_Reply(request_t* req, pcb_t* pcb, peer_info_t* from, void* userdata) {
    (void) userdata;
    SAH_TRACEZ_IN("pcb");
    tree_item_t* parent = tree_item_parent(&req->child_requests);
    if(!parent) {
        SAH_TRACEZ_WARNING("pcb", "No parent request, closed or canceled?");
        request_destroy(req);
        SAH_TRACEZ_OUT("pcb");
        return false;
    }

    bool retval = true;
    /* call reply handler */
    retval = request_childReply(req, pcb, from, userdata);

    if(retval) {
        retval = request_childDone(req, pcb, from, userdata);
    }
    SAH_TRACEZ_OUT("pcb");
    return retval;
}


static void request_childCanceled(request_t* req, void* userdata) {
    (void) userdata;
    SAH_TRACEZ_IN("pcb");
    tree_item_t* parent = tree_item_parent(&req->child_requests);
    if(!parent) {
        SAH_TRACEZ_WARNING("pcb", "No parent request, closed or canceled?");
        request_destroy(req);
        SAH_TRACEZ_OUT("pcb");
        return;
    }

    request_t* orig_req = tree_item_data(parent, request_t, child_requests);

    /* remove this child request */
    SAH_TRACEZ_INFO("pcb", "Remove and destroy child request");
    request_destroy(req);

    /* call the original request's cancel handler */
    if(orig_req->cancelHandler) {
        orig_req->cancelHandler(orig_req, orig_req->userdata);
    }

    SAH_TRACEZ_OUT("pcb");
    return;
}

request_list_t* request_list_create(void) {
    request_list_t* rl = (request_list_t*) calloc(1, sizeof(request_list_t));
    if(!rl) {
        pcb_error = pcb_error_no_memory;
        return NULL;
    }

    if(!request_list_initialize(rl)) {
        free(rl);
        return NULL;
    }

    return rl;
}

bool request_list_initialize(request_list_t* rl) {
    if(!rl) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(!llist_initialize(rl)) {
        return false;
    }

    return true;
}

void request_list_destroy(request_list_t* list) {
    if(!list) {
        pcb_error = pcb_error_invalid_parameter;
        return;
    }

    request_list_cleanup(list);
    free(list);
}

void request_list_cleanup(request_list_t* list) {
    if(!list) {
        pcb_error = pcb_error_invalid_parameter;
        return;
    }

    llist_iterator_t* it = llist_takeFirst(list);
    request_t* req = NULL;
    while(it) {
        req = llist_item_data(it, request_t, list_it);
        request_destroy(req);
        it = llist_takeFirst(list);
    }
}

bool request_list_add(request_list_t* list, request_t* req) {
    if(!list || !req) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    return llist_append(list, &req->list_it);
}

request_t* request_list_first(request_list_t* list) {
    if(!list) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    llist_iterator_t* it = llist_first(list);
    if(!it) {
        return NULL;
    }

    return llist_item_data(it, request_t, list_it);
}

request_t* request_list_next(request_t* req) {
    if(!req) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    if(!req->list_it.list) {
        return NULL;
    }

    llist_iterator_t* it = llist_iterator_next(&req->list_it);
    if(!it) {
        return NULL;
    }

    return llist_item_data(it, request_t, list_it);
}

request_t* request_list_prev(request_t* req) {
    if(!req) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    if(!req->list_it.list) {
        return NULL;
    }

    llist_iterator_t* it = llist_iterator_prev(&req->list_it);
    if(!it) {
        return NULL;
    }

    return llist_item_data(it, request_t, list_it);
}

request_t* request_list_last(request_list_t* list) {
    if(!list) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    llist_iterator_t* it = llist_last(list);
    if(!it) {
        return NULL;
    }

    return llist_item_data(it, request_t, list_it);
}

uint32_t request_id(request_t* req) {
    if(!req) {
        pcb_error = pcb_error_invalid_parameter;
        return 0;
    }

    return req->request_id;
}

reply_t* request_reply(request_t* req) {
    if(!req) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    return &req->reply;
}

bool request_replyReceived(request_t* req) {
    if(!req) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    return req->reply.completed;
}

bool request_setSerializationData(request_t* req, void* data) {
    if(!req) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    req->serdata = data;
    return true;
}

void* request_serializationData(request_t* req) {
    if(!req) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    return req->serdata;
}

bool request_setData(request_t* req, void* data) {
    if(!req) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    req->userdata = data;
    return true;
}

void* request_data(request_t* req) {
    if(!req) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    return req->userdata;
}

bool request_setReplyHandler(request_t* req, request_replyHandler handler) {
    if(!req) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    req->replyHandler = handler;
    return true;
}

bool request_setReplyChildHandler(request_t* req, request_replyChildHandler handler) {
    if(!req) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    req->replyChildHandler = handler;
    return true;
}

bool request_setReplyItemHandler(request_t* req, request_replyItemHandler handler) {
    if(!req) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    req->replyItemHandler = handler;
    return true;
}

bool request_setDoneHandler(request_t* req, request_doneHandler handler) {
    if(!req) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    req->doneHandler = handler;
    return true;
}

bool request_setCancelHandler(request_t* req, request_cancelHandler handler) {
    if(!req) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    req->cancelHandler = handler;
    return true;
}

bool request_setDestroyHandler(request_t* req, request_destroyHandler handler) {
    if(!req) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    req->destroyHandler = handler;
    return true;
}

bool request_setCleanupSerializationHandler(request_t* req, request_cleanupSerializationData handler) {
    if(!req) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    req->cleanupSerializationHandler = handler;
    return true;
}

bool request_forward(request_t* orig_req, peer_info_t* source, request_t* new_req, peer_info_t* dest) {
    if(!orig_req || !new_req || !source || !dest || (orig_req == new_req)) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    pcb_peer_data_t* pd = peer_getUserData(source);
    if(!pd) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    pcb_t* pcb = pcb_fromConnection(peer_connection(dest));
    tree_item_appendChild(&orig_req->child_requests, &new_req->child_requests);
    if(!pcb_sendRequest(pcb, dest, new_req)) {
        SAH_TRACEZ_ERROR("pcb", "Failed to send request");
        return false;
    }

    if(request_attributes(orig_req) & request_getObject_keep_hierarchy) {
        /* do not set reply handler, all replies of the child request will be send to the orig in one go */
        request_setDoneHandler(new_req, request_childDone_Reply);
        request_setCancelHandler(new_req, request_childCanceled);
    } else {
        /* need to set reply handler and reply done handler */
        request_setReplyHandler(new_req, request_childReply);
        request_setDoneHandler(new_req, request_childDone);
        request_setCancelHandler(new_req, request_childCanceled);
    }

    llist_append(&pd->forwardedRequests, &orig_req->forwarded_it);
    orig_req->state = request_state_forwarded;

    return true;
}

bool request_addChild(request_t* req, request_t* child) {
    if(!req || !child) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    return tree_item_appendChild(&req->child_requests, &child->child_requests);
}

bool request_hasChildren(request_t* orig_req) {
    if(!orig_req) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    return tree_item_hasChildren(&orig_req->child_requests);
}

bool request_setBusy(request_t* req) {
    if(!req) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    req->state = request_state_handling;
    return true;
}

bool request_setDone(request_t* req) {
    if(!req) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    req->state = request_state_done;
    return true;
}

request_states_t request_state(request_t* req) {
    if(!req) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    return req->state;
}

request_t* request_create(const request_type_t type) {
    request_t* request = NULL;
    switch(type) {
    case request_type_get_object:
    case request_type_set_object:
    case request_type_create_instance:
    case request_type_delete_instance:
    case request_type_exec_function:
    case request_type_find_objects: {
        request_object_t* req = (request_object_t*) calloc(1, sizeof(request_object_t));
        if(!req) {
            pcb_error = pcb_error_no_memory;
            return NULL;
        }
        req->UID = UINT32_MAX;
        req->sourcePID = getpid();
        request = &req->request;
    }
    break;
    case request_type_close_request: {
        request_close_t* req = (request_close_t*) calloc(1, sizeof(request_close_t));
        if(!req) {
            pcb_error = pcb_error_no_memory;
            return NULL;
        }
        request = &req->request;
    }
    break;
    case request_type_open_session: {
        request_open_session_t* req = (request_open_session_t*) calloc(1, sizeof(request_open_session_t));
        if(!req) {
            pcb_error = pcb_error_no_memory;
            return NULL;
        }
        request = &req->request;
    }
    break;
    case request_type_unknown:
        request = (request_t*) calloc(1, sizeof(request_t));
        if(!request) {
            pcb_error = pcb_error_no_memory;
            return NULL;
        }
        break;
    default:
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
        break;
    }
    request->type = type;
    request_initialize(request);

    return request;
}

request_t* request_create_getObject(const char* objectPath, uint32_t depth, uint32_t attributes) {
    request_object_t* req = request_object_create(objectPath, depth, attributes);
    if(!req) {
        return NULL;
    }
    req->request.type = request_type_get_object;
    request_initialize(&req->request);

    return &req->request;
}

request_t* request_create_setObject(const char* objectPath, uint32_t attributes) {
    request_object_t* req = request_object_create(objectPath, 0, attributes);
    if(!req) {
        return NULL;
    }
    req->request.type = request_type_set_object;
    request_initialize((request_t*) req);

    return &req->request;
}

request_t* request_create_addInstance(const char* objectPath, uint32_t index, const char* key, uint32_t attributes) {
    request_object_t* req = request_object_create(objectPath, 0, attributes);
    if(!req) {
        return NULL;
    }
    req->request.type = request_type_create_instance;
    req->instanceIndex = index;

    if(key && (*key)) {
        req->instanceKey = (char*) calloc(1, strlen(key) + 1);
        if(!req->instanceKey) {
            pcb_error = pcb_error_no_memory;
            goto error_free;
        }
        strcpy(req->instanceKey, key);
    }
    request_initialize(&req->request);

    return &req->request;

error_free:
    request_destroy(&req->request);
    return NULL;
}

request_t* request_create_deleteInstance(const char* objectPath, uint32_t attributes) {
    request_object_t* req = request_object_create(objectPath, 0, attributes);
    if(!req) {
        return NULL;
    }
    req->request.type = request_type_delete_instance;
    request_initialize(&req->request);

    return &req->request;
}

request_t* request_create_findObjects(const char* objectPath, const char* pattern, uint32_t depth, uint32_t attributes) {
    request_object_t* req = request_object_create(objectPath, depth, attributes);
    if(!req) {
        return NULL;
    }
    req->request.type = request_type_find_objects;
    req->pattern = (char*) calloc(1, strlen(pattern) + 1);
    if(!req->pattern) {
        pcb_error = pcb_error_no_memory;
        goto error_free;
    }
    strcpy(req->pattern, pattern);
    request_initialize(&req->request);

    return &req->request;

error_free:
    request_destroy(&req->request);
    return NULL;
}

request_t* request_create_executeFunction(const char* objectPath, const char* functionName, uint32_t attributes) {
    if(!functionName) {
        return NULL;
    }
    request_object_t* req = request_object_create(objectPath, 0, attributes);
    if(!req) {
        return NULL;
    }
    req->request.type = request_type_exec_function;

    req->functionName = (char*) calloc(1, strlen(functionName) + 1);
    if(!req->functionName) {
        pcb_error = pcb_error_no_memory;
        goto error_free;
    }
    strcpy(req->functionName, functionName);

    request_initialize(&req->request);

    return &req->request;

error_free:
    request_destroy(&req->request);
    return NULL;
}

request_t* request_create_closeRequest(uint32_t requestId) {
    request_close_t* req = (request_close_t*) calloc(1, sizeof(request_close_t));
    if(!req) {
        pcb_error = pcb_error_no_memory;
        return NULL;
    }

    req->id = requestId;
    req->request.type = request_type_close_request;
    request_initialize(&req->request);

    return &req->request;
}

request_t* request_create_openSession(const char* username) {
    request_open_session_t* req = (request_open_session_t*) calloc(1, sizeof(request_open_session_t));
    if(!req) {
        pcb_error = pcb_error_no_memory;
        return NULL;
    }

    req->username = (char*) calloc(1, strlen(username) + 1);
    if(!req->username) {
        pcb_error = pcb_error_no_memory;
        free(req);
        return NULL;
    }
    strcpy(req->username, username);

    req->request.type = request_type_open_session;
    request_initialize(&req->request);

    return &req->request;

}

request_t* request_copy(request_t* req, uint32_t newDepth, uint32_t attributes, uint32_t copyflags) {
    if(!req) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }
    request_t* copy_req = NULL;
    const char* newPath = request_path(req);
    attributes |= (request_attributes(req) & request_received_over_tcp);
    switch(req->type) {
    case request_type_get_object:
        copy_req = request_create_getObject(newPath, newDepth, attributes);
        break;
    case request_type_set_object:
        copy_req = request_create_setObject(newPath, attributes);
        break;
    case request_type_create_instance:
        copy_req = request_create_addInstance(newPath, request_instanceIndex(req), request_instanceKey(req), attributes);
        break;
    case request_type_delete_instance:
        copy_req = request_create_deleteInstance(newPath, attributes);
        break;
    case request_type_find_objects:
        copy_req = request_create_findObjects(newPath, request_pattern(req), newDepth, attributes);
        break;
    case request_type_exec_function:
        copy_req = request_create_executeFunction(newPath, request_functionName(req), attributes);
        break;
    case request_type_close_request:
        copy_req = request_create_closeRequest(request_closeRequestId(req));
        break;
    default:
        break;
    }

    if(!copy_req) {
        SAH_TRACEZ_ERROR("pcb", "Failed to copy request");
        return NULL;
    }
    request_setUserID(copy_req, request_userID(req));
    request_setPid(copy_req, request_getPid(req));
    if(!(copyflags & request_copy_no_parameters)) {
        if(copy_req && request_parameterCount(req)) {
            parameter_iterator_t* it = request_firstParameter(req);
            while(it) {
                request_addParameter(copy_req, request_parameterName(it), request_parameterValue(it));
                it = request_nextParameter(it);
            }
        }
    }

    return copy_req;
}

static void request_send_cancel(request_t* req) {
    /* if the request is not in a request list, just return.
       It can not be canceled any more. No reference to a peer available.
     */
    if(!req->it.list) {
        return;
    }
    /* Only send a cancel when the request is in one of the following states
       - request_state_sent
       - request_state_handling_reply
       - request_state_waiting_for_reply
       Never send a cancel for request type close request (which is already a cancel)
     */
    if(((req->state == request_state_sent) ||
        (req->state == request_state_handling_reply) ||
        (req->state == request_state_waiting_for_reply)) && (req->type != request_type_close_request)) {
        pcb_t* pcb = NULL;
        /* request is in the pending list of a peer */
        pcb_peer_data_t* pd = llist_item_data(req->it.list, pcb_peer_data_t, pendingRequests);
        /* if the peer is not connected anymore, do not send a cancel. */
        if(peer_isConnected(pd->peer)) {
            request_t* close = request_create_closeRequest(request_id(req));
            pcb = pcb_fromConnection(peer_connection(pd->peer));
            SAH_TRACEZ_INFO("pcb", "Sending request cancel %p", req);
            if(!pcb_sendRequest(pcb, pd->peer, close)) {
                SAH_TRACEZ_ERROR("pcb", "failed to send cancel request");
            }
            /* we do not want any reply on this one, so destroy and cleanup */
            req->state = request_state_destroyed;
            request_destroy(close);
        }
    }
}

void request_destroy(request_t* req) {
    if(!req) {
        pcb_error = pcb_error_invalid_parameter;
        return;
    }
    SAH_TRACEZ_IN("pcb_req");

    /* reset some of the handlers
       just make sure that no handler is called for
       this request any more, except the destroy handler
     */
    req->replyHandler = NULL;
    req->doneHandler = NULL;
    req->cancelHandler = NULL;

    /* remove from parent */
    tree_item_takeChild(&req->child_requests);

    /* remove from a request list */
    llist_iterator_take(&req->list_it);

    SAH_TRACEZ_INFO("pcb_req", "Request (%p) id %d, state = %d", req, request_id(req), req->state);

    if((req->state == request_state_sent) ||
       (req->state == request_state_handling_reply) ||
       (req->state == request_state_waiting_for_reply) ||
       (req->state == request_state_done)) {
        if(req->state != request_state_done) {
            request_send_cancel(req);
        }
        req->state = request_state_destroyed;
        SAH_TRACEZ_OUT("pcb_req");
        return;
    }

    if(req->cleanupSerializationHandler && req->serdata) {
        req->cleanupSerializationHandler(req, req->serdata);
        req->serdata = NULL;
    }

    if(req->destroyHandler) {
        req->destroyHandler(req, req->userdata);
    }

    request_object_t* object_req = NULL;
    switch(req->type) {
    case request_type_get_object:
    case request_type_set_object:
    case request_type_create_instance:
    case request_type_delete_instance:
    case request_type_find_objects:
    case request_type_exec_function: {
        object_req = llist_item_data(req, request_object_t, request);
        free(object_req->namespace);
        object_req->namespace = NULL;
        free(object_req->objectPath);
        free(object_req->pattern);
        free(object_req->functionName);
        object_req->objectPath = NULL;
        free(object_req->instanceKey);
        object_req->instanceKey = NULL;
        request_parameterListClear(req);
        SAH_TRACEZ_INFO("pcb_req", "Clear datamodel");
        datamodel_cleanup(object_req->datamodel);
        free(object_req->datamodel);
        object_req->datamodel = NULL;
    }
    break;
    case request_type_open_session: {
        request_open_session_t* opensession_req = llist_item_data(req, request_open_session_t, request);
        free(opensession_req->username);
        opensession_req->username = NULL;
    }
    break;
    default:
        break;
    }

    /* cleanup tree */
    tree_item_t* it = tree_item_firstChild(&req->child_requests);
    tree_item_t* prefetch = tree_item_nextSibling(it);
    request_t* child_req = NULL;
    while(it) {
        child_req = llist_item_data(it, request_t, child_requests);
        request_send_cancel(child_req);
        child_req->state = request_state_destroy;
        request_destroy(child_req);
        it = prefetch;
        prefetch = tree_item_nextSibling(it);
    }
    tree_item_cleanup(&req->child_requests);

    /* cleanup replies */
    reply_cleanup(&req->reply);
    /* remove the request from all lists */
    SAH_TRACEZ_INFO("pcb_req", "remove request from lists");
    llist_iterator_take(&req->it);
    llist_iterator_take(&req->notify_it);
    llist_iterator_take(&req->forwarded_it);
    free(req);
    req = NULL;
    SAH_TRACEZ_OUT("pcb_req");
}

request_type_t request_type(request_t* req) {
    if(!req) {
        pcb_error = pcb_error_invalid_parameter;
        return request_type_unknown;
    }

    return req->type;
}

bool request_useObjectCache(request_t* req) {
    if(!req) {
        pcb_error = pcb_error_invalid_parameter;
        return true;
    }

    if((req->type != request_type_set_object) &&
       (req->type != request_type_get_object) &&
       (req->type != request_type_create_instance) &&
       (req->type != request_type_delete_instance) &&
       (req->type != request_type_find_objects)) {
        return true;
    }

    request_object_t* object_req = llist_item_data(req, request_object_t, request);
    return (object_req->attributes & request_no_object_caching) ? false : true;
}

bool request_addParameter(request_t* req, const char* parameterName, const variant_t* value) {
    if(!req || !parameterName) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if((req->type != request_type_set_object) &&
       (req->type != request_type_get_object) &&
       (req->type != request_type_create_instance) &&
       (req->type != request_type_exec_function)) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if((req->type == request_type_set_object) ||
       (req->type == request_type_exec_function)) {
        if(value == NULL) {
            pcb_error = pcb_error_invalid_parameter;
            return false;
        }
    }

    param_value_t* param_value = (param_value_t*) calloc(1, sizeof(param_value_t));
    request_object_t* object_req = NULL;
    if(!param_value) {
        pcb_error = pcb_error_no_memory;
        goto error_free_rparam_value;
    }
    param_value->parameterName = (char*) calloc(1, strlen(parameterName) + 1);
    if(!param_value->parameterName) {
        pcb_error = pcb_error_no_memory;
        goto error_free_param_name;
    }
    strcpy(param_value->parameterName, parameterName);
    variant_initialize(&param_value->value, variant_type_unknown);
    if(value) {
        variant_copy(&param_value->value, value);
    }
    object_req = llist_item_data(req, request_object_t, request);
    llist_append(&object_req->parameters, &param_value->it);

    return true;

error_free_param_name:
    free(param_value->parameterName);
error_free_rparam_value:
    free(param_value);
    return false;
}

bool request_addArgument(request_t* req, argument_value_t* arg) {
    if(!req || !arg) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if((req->type != request_type_set_object) &&
       (req->type != request_type_get_object) &&
       (req->type != request_type_create_instance) &&
       (req->type != request_type_exec_function)) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    request_object_t* object_req = llist_item_data(req, request_object_t, request);
    llist_append(&object_req->parameters, &arg->it);

    return true;
}

bool request_setPath(request_t* req, const char* path) {
    if(!req || !path) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if((req->type != request_type_set_object) &&
       (req->type != request_type_get_object) &&
       (req->type != request_type_create_instance) &&
       (req->type != request_type_delete_instance) &&
       (req->type != request_type_find_objects) &&
       (req->type != request_type_exec_function)) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    request_object_t* object_req = llist_item_data(req, request_object_t, request);

    if(object_req->objectPath) {
        free(object_req->objectPath);
    }

    object_req->objectPath = (char*) calloc(1, strlen(path) + 1);
    if(!object_req->objectPath) {
        pcb_error = pcb_error_no_memory;
        return false;
    }
    if((path[0] == '.') || (path[0] == '/')) {
        strcpy(object_req->objectPath, path + 1);
    } else {
        strcpy(object_req->objectPath, path);
    }

    return true;
}

bool request_setFunctionName(request_t* req, const char* functionName) {
    if(!req || !functionName) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    request_object_t* object_req = llist_item_data(req, request_object_t, request);

    if(object_req->functionName) {
        free(object_req->functionName);
    }

    object_req->functionName = (char*) calloc(1, strlen(functionName) + 1);
    if(!object_req->functionName) {
        pcb_error = pcb_error_no_memory;
        return false;
    }
    strcpy(object_req->functionName, functionName);

    return true;
}

bool request_setPattern(request_t* req, const char* pattern) {
    if(!req || !pattern) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(req->type != request_type_find_objects) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    request_object_t* object_req = llist_item_data(req, request_object_t, request);
    if(object_req->pattern) {
        free(object_req->pattern);
    }

    object_req->pattern = (char*) calloc(1, strlen(pattern) + 1);
    if(!object_req->pattern) {
        pcb_error = pcb_error_no_memory;
        return false;
    }
    strcpy(object_req->pattern, pattern);

    return true;
}

bool request_setAttributes(request_t* req, uint32_t request_attributes) {
    if(!req) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if((req->type != request_type_set_object) &&
       (req->type != request_type_get_object) &&
       (req->type != request_type_create_instance) &&
       (req->type != request_type_delete_instance) &&
       (req->type != request_type_find_objects) &&
       (req->type != request_type_exec_function)) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    request_object_t* object_req = llist_item_data(req, request_object_t, request);
    object_req->attributes = request_attributes;

    return true;
}

bool request_setDepth(request_t* req, const uint32_t depth) {
    if(!req) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if((req->type != request_type_get_object) &&
       (req->type != request_type_find_objects)) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    request_object_t* object_req = llist_item_data(req, request_object_t, request);
    object_req->depth = depth;

    return true;
}

bool request_setInstanceIndex(request_t* req, const uint32_t index) {
    if(!req) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(req->type != request_type_create_instance) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    request_object_t* object_req = llist_item_data(req, request_object_t, request);
    object_req->instanceIndex = index;

    return true;
}

bool request_setInstanceKey(request_t* req, const char* key) {
    if(!req) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(req->type != request_type_create_instance) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    request_object_t* object_req = llist_item_data(req, request_object_t, request);
    if(object_req->instanceKey) {
        free(object_req->instanceKey);
    }

    if(key) {
        object_req->instanceKey = (char*) calloc(1, strlen(key) + 1);
        if(!object_req->instanceKey) {
            pcb_error = pcb_error_no_memory;
            return false;
        }
        strcpy(object_req->instanceKey, key);
    } else {
        object_req->instanceKey = NULL;
    }
    return true;
}

bool request_setUsername(request_t* req, const char* username) {
    if(!req || !username) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(req->type != request_type_open_session) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    request_open_session_t* opensession_req = llist_item_data(req, request_open_session_t, request);
    if(opensession_req->username) {
        free(opensession_req->username);
    }

    opensession_req->username = (char*) calloc(1, strlen(username) + 1);
    if(!opensession_req->username) {
        pcb_error = pcb_error_no_memory;
        return false;
    }
    strcpy(opensession_req->username, username);

    return true;
}

bool request_setUserID(request_t* req, uint32_t userId) {
    if(!req) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if((req->type != request_type_set_object) &&
       (req->type != request_type_get_object) &&
       (req->type != request_type_create_instance) &&
       (req->type != request_type_delete_instance) &&
       (req->type != request_type_find_objects) &&
       (req->type != request_type_exec_function)) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    request_object_t* object_req = llist_item_data(req, request_object_t, request);
    object_req->UID = userId;

    return true;
}

bool request_setCloseRequestId(request_t* req, const uint32_t id) {
    if(!req) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(req->type != request_type_close_request) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    request_close_t* close_req = llist_item_data(req, request_close_t, request);
    close_req->id = id;

    return true;
}

const char* request_path(request_t* req) {
    if(!req) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    if((req->type != request_type_set_object) &&
       (req->type != request_type_get_object) &&
       (req->type != request_type_create_instance) &&
       (req->type != request_type_delete_instance) &&
       (req->type != request_type_find_objects) &&
       (req->type != request_type_exec_function)) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    request_object_t* object_req = llist_item_data(req, request_object_t, request);
    return object_req->objectPath;
}

const char* request_functionName(request_t* req) {
    if(!req) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    if(req->type != request_type_exec_function) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    request_object_t* object_req = llist_item_data(req, request_object_t, request);
    return object_req->functionName;
}

const char* request_pattern(request_t* req) {
    if(!req) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    if(req->type != request_type_find_objects) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    request_object_t* object_req = llist_item_data(req, request_object_t, request);
    return object_req->pattern;
}

uint32_t request_attributes(request_t* req) {
    if(!req) {
        pcb_error = pcb_error_invalid_parameter;
        return 0;
    }

    if((req->type != request_type_set_object) &&
       (req->type != request_type_get_object) &&
       (req->type != request_type_create_instance) &&
       (req->type != request_type_delete_instance) &&
       (req->type != request_type_find_objects) &&
       (req->type != request_type_exec_function)) {
        pcb_error = pcb_error_invalid_parameter;
        return 0;
    }

    request_object_t* object_req = llist_item_data(req, request_object_t, request);
    return object_req->attributes;
}

uint32_t request_depth(request_t* req) {
    if(!req) {
        pcb_error = pcb_error_invalid_parameter;
        return 0;
    }

    if((req->type != request_type_get_object) &&
       (req->type != request_type_find_objects)) {
        pcb_error = pcb_error_invalid_parameter;
        return 0;
    }

    request_object_t* object_req = llist_item_data(req, request_object_t, request);
    return object_req->depth;
}

uint32_t request_instanceIndex(request_t* req) {
    if(!req) {
        pcb_error = pcb_error_invalid_parameter;
        return 0;
    }

    if(req->type != request_type_create_instance) {
        pcb_error = pcb_error_invalid_parameter;
        return 0;
    }

    request_object_t* object_req = llist_item_data(req, request_object_t, request);
    return object_req->instanceIndex;
}

const char* request_instanceKey(request_t* req) {
    if(!req) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    if(req->type != request_type_create_instance) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    request_object_t* object_req = llist_item_data(req, request_object_t, request);
    return object_req->instanceKey;
}

const char* request_username(request_t* req) {
    if(!req) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    if(req->type != request_type_open_session) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    request_open_session_t* opensession_req = llist_item_data(req, request_open_session_t, request);
    return opensession_req->username;
}

uint32_t request_userID(request_t* req) {
    if(!req) {
        pcb_error = pcb_error_invalid_parameter;
        return 0;
    }

    if((req->type != request_type_set_object) &&
       (req->type != request_type_get_object) &&
       (req->type != request_type_create_instance) &&
       (req->type != request_type_delete_instance) &&
       (req->type != request_type_find_objects) &&
       (req->type != request_type_exec_function)) {
        pcb_error = pcb_error_invalid_parameter;
        return 0;
    }

    request_object_t* object_req = llist_item_data(req, request_object_t, request);
    return object_req->UID;
}

uint32_t request_closeRequestId(request_t* req) {
    if(!req) {
        pcb_error = pcb_error_invalid_parameter;
        return 0;
    }

    if(req->type != request_type_close_request) {
        pcb_error = pcb_error_invalid_parameter;
        return 0;
    }

    request_close_t* close_req = llist_item_data(req, request_close_t, request);
    return close_req->id;
}

bool request_setPid(request_t* req, const uint32_t pid) {
    if(!req) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if((req->type != request_type_set_object) &&
       (req->type != request_type_get_object) &&
       (req->type != request_type_create_instance) &&
       (req->type != request_type_delete_instance) &&
       (req->type != request_type_find_objects) &&
       (req->type != request_type_exec_function)) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    request_object_t* object_req = llist_item_data(req, request_object_t, request);
    object_req->sourcePID = pid;
    return true;
}

uint32_t request_getPid(request_t* req) {
    if(!req) {
        pcb_error = pcb_error_invalid_parameter;
        return 0;
    }

    if((req->type != request_type_set_object) &&
       (req->type != request_type_get_object) &&
       (req->type != request_type_create_instance) &&
       (req->type != request_type_delete_instance) &&
       (req->type != request_type_find_objects) &&
       (req->type != request_type_exec_function)) {
        pcb_error = pcb_error_invalid_parameter;
        return 0;
    }

    request_object_t* object_req = llist_item_data(req, request_object_t, request);
    return object_req->sourcePID;
}

bool request_setBusNamespace(request_t* req, const char* ns) {
    if(!req || !ns) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if((req->type != request_type_set_object) &&
       (req->type != request_type_get_object) &&
       (req->type != request_type_create_instance) &&
       (req->type != request_type_delete_instance) &&
       (req->type != request_type_find_objects) &&
       (req->type != request_type_exec_function)) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    request_object_t* object_req = llist_item_data(req, request_object_t, request);
    object_req->namespace = strdup(ns);
    return true;
}

char* request_getBusNamespace(request_t* req) {
    if(!req) {
        pcb_error = pcb_error_invalid_parameter;
        return 0;
    }

    if((req->type != request_type_set_object) &&
       (req->type != request_type_get_object) &&
       (req->type != request_type_create_instance) &&
       (req->type != request_type_delete_instance) &&
       (req->type != request_type_find_objects) &&
       (req->type != request_type_exec_function)) {
        pcb_error = pcb_error_invalid_parameter;
        return 0;
    }

    request_object_t* object_req = llist_item_data(req, request_object_t, request);
    return object_req->namespace;
}

uint32_t request_parameterCount(request_t* req) {
    if(!req) {
        pcb_error = pcb_error_invalid_parameter;
        return 0;
    }

    if((req->type != request_type_set_object) &&
       (req->type != request_type_get_object) &&
       (req->type != request_type_create_instance) &&
       (req->type != request_type_exec_function)) {
        pcb_error = pcb_error_invalid_parameter;
        return 0;
    }

    request_object_t* object_req = llist_item_data(req, request_object_t, request);
    return llist_size(&object_req->parameters);
}

llist_t* request_parameterList(request_t* req) {
    if(!req) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    if((req->type != request_type_set_object) &&
       (req->type != request_type_get_object) &&
       (req->type != request_type_create_instance) &&
       (req->type != request_type_exec_function)) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    request_object_t* object_req = llist_item_data(req, request_object_t, request);
    return &(object_req->parameters);
}

bool request_parameterListCloseFd(request_t* req) {
    if(!req) {
        pcb_error = pcb_error_invalid_parameter;
        return true;
    }

    if((req->type != request_type_set_object) &&
       (req->type != request_type_get_object) &&
       (req->type != request_type_create_instance) &&
       (req->type != request_type_exec_function)) {
        pcb_error = pcb_error_invalid_parameter;
        return true;
    }

    request_object_t* object_req = llist_item_data(req, request_object_t, request);
    param_value_t* param_value;
    llist_iterator_t* it = llist_first(&object_req->parameters);
    while(it) {
        param_value = llist_item_data(it, param_value_t, it);
        if(variant_type(&param_value->value) == variant_type_file_descriptor) {
            int fd = -1;
            variant_toFd(&fd, &param_value->value);
            SAH_TRACEZ_NOTICE("pcb", "Found open fd '%d' in request argument. Closing it.", fd);
            close(fd);
            fd = -1;
        }
        it = llist_iterator_next(it);
    }

    return true;
}

bool request_parameterListClear(request_t* req) {
    if(!req) {
        pcb_error = pcb_error_invalid_parameter;
        return true;
    }

    if((req->type != request_type_set_object) &&
       (req->type != request_type_get_object) &&
       (req->type != request_type_create_instance) &&
       (req->type != request_type_exec_function)) {
        pcb_error = pcb_error_invalid_parameter;
        return true;
    }

    request_object_t* object_req = llist_item_data(req, request_object_t, request);
    param_value_t* param_value;
    llist_iterator_t* it = llist_takeFirst(&object_req->parameters);
    while(it) {
        param_value = llist_item_data(it, param_value_t, it);
        free(param_value->parameterName);
        variant_cleanup(&param_value->value);
        free(param_value);
        it = llist_takeFirst(&object_req->parameters);
    }

    return true;
}

parameter_iterator_t* request_findParameter(request_t* req, const char* name) {
    if(!req || !name) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    if((req->type != request_type_set_object) &&
       (req->type != request_type_get_object) &&
       (req->type != request_type_create_instance) &&
       (req->type != request_type_exec_function)) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    bool found = false;
    request_object_t* object_req = llist_item_data(req, request_object_t, request);
    parameter_iterator_t* it = NULL;
    llist_for_each(it, &object_req->parameters) {
        const char* parameterName = request_parameterName(it);
        if(parameterName != NULL) {
            if(strcmp(parameterName, name) == 0) {
                found = true;
                break;
            }
        }
    }

    if(found) {
        return it;
    }

    return NULL;
}

parameter_iterator_t* request_firstParameter(request_t* req) {
    if(!req) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    if((req->type != request_type_set_object) &&
       (req->type != request_type_get_object) &&
       (req->type != request_type_create_instance) &&
       (req->type != request_type_exec_function)) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    request_object_t* object_req = llist_item_data(req, request_object_t, request);
    parameter_iterator_t* it = llist_first(&object_req->parameters);
    return it;
}

parameter_iterator_t* request_nextParameter(const llist_iterator_t* it) {
    if(!it) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    parameter_iterator_t* ret = llist_iterator_next(it);
    return ret;
}

parameter_iterator_t* request_prevParameter(const parameter_iterator_t* it) {
    if(!it) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    parameter_iterator_t* ret = llist_iterator_prev(it);
    return ret;
}

parameter_iterator_t* request_lastParameter(request_t* req) {
    if(!req) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    if((req->type != request_type_set_object) &&
       (req->type != request_type_get_object) &&
       (req->type != request_type_create_instance) &&
       (req->type != request_type_exec_function)) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    request_object_t* object_req = llist_item_data(req, request_object_t, request);
    parameter_iterator_t* it = llist_last(&object_req->parameters);
    return it;
}

const char* request_parameterName(const parameter_iterator_t* it) {
    if(!it) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    param_value_t* param_value = llist_item_data(it, param_value_t, it);
    return param_value->parameterName;
}

variant_t* request_parameterValue(const parameter_iterator_t* it) {
    if(!it) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    param_value_t* param_value = llist_item_data(it, param_value_t, it);
    return &param_value->value;
}
