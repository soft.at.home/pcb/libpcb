/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <string.h>

#include <debug/sahtrace.h>

#include <pcb/common.h>
#include <pcb/pcb_client.h>

#include <components.h>

#ifdef CONFIG_PCB_ACL_USERMNGT
#include <usermngt/usermngt.h>
#else
#include <pwd.h>
#endif

#include "pcb_main_priv.h"

typedef struct _pcb_client_callback_info {
    pcb_client_object_callback callback_function;
    bool error;
    void* userdata;
} pcb_client_callback_info_t;

typedef enum _pcb_client_operation {
    pcb_client_op_set = 1,
    pcb_client_op_validate = 2,
} pcb_client_operation_t;

static void pcb_client_set_error(pcb_t* pcb, uint32_t error) {
    pcb->client_data.error = error;
}

static void pcb_client_add_error(pcb_t* pcb, uint32_t error, const char* info) {
    pcb_client_error_t* e = (pcb_client_error_t*) calloc(1, sizeof(pcb_client_error_t));
    if(!e) {
        SAH_TRACEZ_ERROR("pcb_client", "failed to add error to error list");
        return;
    }

    e->error = error;
    if(info && *info) {
        e->info = strdup(info);
    }

    llist_append(&pcb->client_data.errors, &e->it);
}

static bool pcb_client_reply_recieved(request_t* req, pcb_t* pcb, peer_info_t* from, void* userdata) {
    (void) from;
    pcb_client_callback_info_t* info = (pcb_client_callback_info_t*) userdata;

    reply_t* reply = request_reply(req);
    reply_item_t* item = reply_firstItem(reply);

    while(item) {
        switch(reply_item_type(item)) {
        case reply_type_object:
            if(!info->callback_function(reply_item_object(item), info->userdata)) {
                info->error = true;
                pcb_client_set_error(pcb, pcb_error_canceled);
                return false;
            }
            break;
        case reply_type_error:
            SAH_TRACEZ_ERROR("pcb_client", "Error received : 0x%8.8X %s %s", reply_item_error(item), reply_item_errorDescription(item), reply_item_errorInfo(item));
            pcb_client_set_error(pcb, reply_item_error(item));
            info->error = true;
            return false;
            break;
        default:
            SAH_TRACEZ_WARNING("pcb_client", "Unexpected reply item");
            break;
        }
        item = reply_nextItem(item);
    }

    return true;
}

static bool pcb_client_reply_wait_for_object(request_t* req, pcb_t* pcb, peer_info_t* from, void* userdata) {
    (void) from;
    SAH_TRACEZ_IN("pcb_client");
    pcb_client_callback_info_t* info = (pcb_client_callback_info_t*) userdata;
    reply_t* reply = request_reply(req);
    reply_item_t* item = reply_firstItem(reply);
    if(!info) {
        SAH_TRACEZ_ERROR("pcb_client", "No user data available");
        return false;
    }

    while(item) {
        switch(reply_item_type(item)) {
        case reply_type_object: {
            char* objectName = object_pathChar(reply_item_object(item), (pcb->client_data.attributes & (path_attr_key_notation | path_attr_slash_seperator)));
            SAH_TRACEZ_INFO("pcb_client", "Got object [%s]", (objectName != NULL) ? objectName : "");
            if((objectName != NULL) && (info->userdata != NULL)) {
                if(strcmp(objectName, (char*) info->userdata) == 0) {
                    free(objectName);
                    SAH_TRACEZ_INFO("pcb_client", "Object found, stop request");
                    SAH_TRACEZ_OUT("pcb_client");
                    return false;
                }
            }
            free(objectName);
        }
        break;
        case reply_type_error:
            /* got an error, stop the request */
            SAH_TRACEZ_WARNING("pcb_client", "Error received : 0x%8.8X %s %s", reply_item_error(item), reply_item_errorDescription(item), reply_item_errorInfo(item));
            info->error = reply_item_error(item);
            return false;
            break;
        default:
            SAH_TRACEZ_WARNING("pcb_client", "Skipping reply item");
            break;
        }
        item = reply_nextItem(item);
    }

    SAH_TRACEZ_OUT("pcb_client");
    return true;
}

static bool pcb_client_return_objects(pcb_t* pcb, object_t* object, pcb_client_object_callback fn, void* userdata) {
    if(!fn(object, userdata)) {
        pcb_client_set_error(pcb, pcb_error_canceled);
        return true;
    }
    object_t* child = object_firstChild(object);
    object_t* prefetch = object_nextSibling(child);
    while(child) {
        if(!pcb_client_return_objects(pcb, child, fn, userdata)) {
            pcb_client_set_error(pcb, pcb_error_canceled);
            return false;
        }
        child = prefetch;
        prefetch = object_nextSibling(child);
    }
    child = object_firstInstance(object);
    prefetch = object_nextInstance(child);
    while(child) {
        if(!pcb_client_return_objects(pcb, child, fn, userdata)) {
            pcb_client_set_error(pcb, pcb_error_canceled);
            return false;
        }
        child = prefetch;
        prefetch = object_nextSibling(child);
    }

    return true;
}

static bool pcb_client_return_objects_pattern(pcb_t* pcb, object_t* object, const char* pattern, uint32_t attributes, pcb_client_object_callback fn, void* userdata) {
    bool retval = true;
    uint32_t attrs = attributes;
    object_list_t* list = NULL;
    llist_iterator_t* iter = NULL, * iter_next = NULL;
    object_t* obj = NULL;

    attrs &= (path_attr_key_notation | path_attr_slash_seperator);
    if(attributes & request_find_instance_wildcards_only) {
        attrs |= search_attr_instance_wildcards_only;
    }
    attrs |= (search_attr_include_parameters | search_attr_include_values);

    list = object_findObjects(object, pattern, -1, attrs);
    for(iter = llist_first(list); iter; iter = iter_next) {
        iter_next = llist_iterator_next(iter);
        llist_iterator_take(iter);

        if(retval) {
            obj = llist_item_data(iter, object_t, it);
            if(!fn(obj, userdata)) {
                pcb_client_set_error(pcb, pcb_error_canceled);
                retval = false;
                // Continue looping to clean up the list, don't call fn() anymore
            }
        }
    }

    llist_cleanup(list);
    free(list);
    return retval;
}

static pcb_t* pcb_client_check_destination(peer_info_t* dest, uint32_t* attributes) {
    pcb_t* pcb = NULL;
    if(!dest) {
        goto error;
    }

    /* get pcb context */
    pcb = pcb_fromConnection(peer_connection(dest));
    if(!pcb) {
        pcb_error = pcb_error_not_connected;
        goto error;
    }

    if(!peer_isConnected(dest)) {
        pcb_client_set_error(pcb, pcb_error_not_connected);
        goto error;
    }

    pcb_client_set_error(pcb, 0);
    pcb_client_clear_errors(pcb);
    *attributes = pcb->client_data.attributes;

    SAH_TRACEZ_INFO("pcb_client", "Client Request Attributes: 0x%X", (int) *attributes);

    return pcb;

error:
    return NULL;
}

static reply_t* pcb_client_get_reply(pcb_t* pcb, peer_info_t* dest, request_t* req) {
    struct timeval timeout = { 0, 0 };
    struct timeval* t = NULL;
    reply_t* reply = NULL;

    /* send the request */
    if(!pcb_sendRequest(pcb, dest, req)) {
        SAH_TRACEZ_ERROR("pcb_client", "Failed to send request");
        pcb_client_set_error(pcb, pcb_error);
        goto error;
    }

    if(pcb->client_data.timeout) {
        timeout.tv_sec = pcb->client_data.timeout / 1000;
        timeout.tv_usec = (pcb->client_data.timeout % 1000) * 1000;
        t = &timeout;
    }

    SAH_TRACEZ_INFO("pcb_client", "Waiting for reply ... (timeout = %d)", pcb->client_data.timeout);
    /* wait for reply */
    if(!pcb_waitForReply(pcb, req, t)) {
        SAH_TRACEZ_ERROR("pcb_client", "Failed to get reply");
        pcb_client_set_error(pcb, pcb_error);
        goto error;
    }

    /* check reply */
    reply = request_reply(req);

    return reply;

error:
    SAH_TRACE_INFO("No reply for request");
    return NULL;
}

static bool pcb_client_check_reply_error(pcb_t* pcb, peer_info_t* dest, request_t* req) {
    /* check reply */
    reply_t* reply = pcb_client_get_reply(pcb, dest, req);
    reply_item_t* item = NULL;
    if(!reply) {
        SAH_TRACEZ_ERROR("pcb_client", "Invalid reply");
        pcb_client_set_error(pcb, pcb_error_invalid_reply);
        return false;
    }

    /* reset the pcb_client_error code to 0 */
    pcb_client_set_error(pcb, pcb_ok);
    /* iterate the reply items, check for errors */
    item = reply_firstItem(reply);
    while(item) {
        switch(reply_item_type(item)) {
        case reply_type_error: {
            pcb_client_set_error(pcb, reply_item_error(item));
            SAH_TRACEZ_ERROR("pcb_client", "Error received : 0x%8.8X %s %s", reply_item_error(item), reply_item_errorDescription(item), reply_item_errorInfo(item));
            pcb_client_add_error(pcb, reply_item_error(item), reply_item_errorInfo(item));
        }
        break;
        default:
            break;
        }
        item = reply_nextItem(item);
    }

    return (pcb_client_error(pcb) == 0);
}

static object_t* pcb_client_reply_get_object(pcb_t* pcb, peer_info_t* dest, request_t* req, const char* objectpath) {
    /* check reply */
    reply_t* reply = pcb_client_get_reply(pcb, dest, req);
    if(!reply) {
        return NULL;
    }

    /* reset the pcb_client_error code to 0 */
    pcb_client_set_error(pcb, pcb_ok);
    string_t path;
    string_initialize(&path, 64);
    object_t* retval = NULL;
    /* iterate the reply items, check for errors */
    reply_item_t* item = reply_firstItem(reply);
    while(item && !retval) {
        switch(reply_item_type(item)) {
        case reply_type_object:
            if(!retval) {
                retval = reply_item_object(item);
                if(objectpath) {
                    object_path(retval, &path, (pcb->client_data.attributes & (path_attr_key_notation | path_attr_slash_seperator)));
                    if(string_compareChar(&path, objectpath, 0) != 0) {
                        retval = NULL;
                    }
                }
            }
            break;
        case reply_type_error: {
            SAH_TRACEZ_ERROR("pcb_client", "Error received : 0x%8.8X %s %s", reply_item_error(item), reply_item_errorDescription(item), reply_item_errorInfo(item));
            pcb_client_set_error(pcb, reply_item_error(item));
            pcb_client_add_error(pcb, reply_item_error(item), reply_item_errorInfo(item));
            retval = NULL;
        }
        break;
        default:
            break;
        }
        item = reply_nextItem(item);
    }

    if(!retval) {
        pcb_error = pcb_error_not_found;
        SAH_TRACE_ERROR("Object not found %s", objectpath);
    }

    string_cleanup(&path);
    return retval;
}

static bool pcb_client_set_validate(peer_info_t* dest, const char* objectPath, const char* parameter, const variant_t* val, pcb_client_operation_t operation) {
    uint32_t attributes = 0;
    request_t* req = NULL;
    pcb_t* pcb = pcb_client_check_destination(dest, &attributes);
    if(!pcb) {
        goto error;
    }

    /* check local data model */
    /* TODO: this should be removed, but some applications are pending on this functionality */
    if(objectPath && *objectPath) {
        object_t* object = object_getObject(datamodel_root(pcb_datamodel(pcb)), objectPath, attributes & (path_attr_key_notation | path_attr_slash_seperator), NULL);
        if(object) {
            parameter_t* param = object_getParameter(object, parameter);
            if(!param) {
                pcb_error = pcb_error_not_found;
                goto error;
            }
            if(!parameter_setValue(param, val)) {
                goto error;
            }
            if(!object_commit(object)) {
                object_rollback(object);
                goto error;
            }
            return true;
        }
    }

    /* check remotely
       build request
     */
    req = request_create_setObject(objectPath, attributes | ((operation == pcb_client_op_validate) ? request_setObject_validate_only : 0));
    if(!req) {
        pcb_client_set_error(pcb, pcb_error);
        goto error;
    }

    request_setUserID(req, pcb->client_data.uid);

    /* add parameter */
    if(!request_addParameter(req, parameter, val)) {
        pcb_client_set_error(pcb, pcb_error);
        goto error_cleanup_request;
    }

    if(!pcb_client_check_reply_error(pcb, dest, req)) {
        goto error_cleanup_request;
    }

    request_destroy(req);
    return true;

error_cleanup_request:
    request_destroy(req);
error:
    return false;
}

bool pcb_client_set_validate_object(peer_info_t* dest, const char* objectPath, variant_map_t* parameters, pcb_client_operation_t operation) {
    uint32_t attributes = 0;
    request_t* req = NULL;
    variant_map_iterator_t* it = NULL;
    pcb_t* pcb = pcb_client_check_destination(dest, &attributes);
    if(!pcb) {
        SAH_TRACEZ_ERROR("pcb_client", "Invalide parameter");
        goto error;
    }

    /* check local data model */
    if(objectPath && *objectPath) {
        object_t* object = object_getObject(datamodel_root(pcb_datamodel(pcb)), objectPath, attributes & (path_attr_key_notation | path_attr_slash_seperator), NULL);
        if(object) {
            pcb_client_set_error(pcb, pcb_ok);
            SAH_TRACEZ_INFO("pcb_client", "using local object");
            variant_map_iterator_t* it = NULL;
            variant_map_for_each(it, parameters) {
                parameter_t* param = object_getParameter(object, variant_map_iterator_key(it));
                if(!param) {
                    SAH_TRACEZ_ERROR("pcb_client", "Parameter not found %s", variant_map_iterator_key(it));
                    pcb_client_set_error(pcb, pcb_error_not_found);
                    continue;
                }
                if(!parameter_setValue(param, variant_map_iterator_data(it))) {
                    SAH_TRACEZ_ERROR("pcb_client", "Set value failed %s", variant_map_iterator_key(it));
                    pcb_client_set_error(pcb, pcb_error_invalid_value);
                    continue;
                }
            }

            object_rollback(object);
            return (pcb_client_error(pcb) == pcb_ok);
        }
    }

    /* check remotely
       build request
     */
    req = request_create_setObject(objectPath, attributes | ((operation == pcb_client_op_validate) ? request_setObject_validate_only : 0));
    if(!req) {
        SAH_TRACEZ_ERROR("pcb_client", "Failed to create request");
        pcb_client_set_error(pcb, pcb_error);
        goto error;
    }

    request_setUserID(req, pcb->client_data.uid);

    variant_map_for_each(it, parameters) {
        /* add parameter */
        SAH_TRACEZ_INFO("pcb_client", "Adding parameter %s", variant_map_iterator_key(it));
        if(!request_addParameter(req, variant_map_iterator_key(it), variant_map_iterator_data(it))) {
            SAH_TRACEZ_ERROR("pcb_client", "Failed to add parameter %s", variant_map_iterator_key(it));
            pcb_client_set_error(pcb, pcb_error);
            goto error_cleanup_request;
        }
    }

    if(!pcb_client_check_reply_error(pcb, dest, req)) {
        SAH_TRACEZ_ERROR("pcb_client", "Validation failed");
        goto error_cleanup_request;
    }

    request_destroy(req);
    return true;

error_cleanup_request:
    request_destroy(req);
error:
    return false;
}

bool pcb_client_set_attribute(pcb_t* client, pcb_client_attribute_t attrib, uint32_t value) {
    if(!client) {
        return false;
    }

    switch(attrib) {
    case pcb_client_attr_default:
        if(value) {
            client->client_data.attributes = 0;
            client->client_data.timeout = 5000;
            client->client_data.error = 0;
        }
        break;
    case pcb_client_attr_key_notation:
        if(value) {
            client->client_data.attributes |= request_common_path_key_notation;
        } else {
            client->client_data.attributes &= ~request_common_path_key_notation;
        }
        break;
    case pcb_client_attr_slash_seperator:
        if(value) {
            client->client_data.attributes |= request_common_path_slash_seperator;
        } else {
            client->client_data.attributes &= ~request_common_path_slash_seperator;
        }
        break;
    case pcb_client_attr_untrusted:
        if(value) {
            client->client_data.attributes |= request_received_over_tcp;
        } else {
            client->client_data.attributes &= ~request_received_over_tcp;
        }
        break;
    case pcb_client_attr_timeout:
        client->client_data.timeout = value;
        break;
    case pcb_client_attr_alias_notation:
        if(value) {
            client->client_data.attributes |= request_common_path_alias;
        } else {
            client->client_data.attributes &= ~request_common_path_alias;
        }
        break;
    case pcb_client_attr_find_instance_wildcards_only:
        if(value) {
            client->client_data.attributes |= request_find_instance_wildcards_only;
        } else {
            client->client_data.attributes &= ~request_find_instance_wildcards_only;
        }
        break;
    default:
        /* do nothing */
        break;
    }

    return true;
}

bool pcb_client_set_user(pcb_t* client, const char* username) {
    if(!client || !username || !(*username)) {
        return false;
    }

#ifdef CONFIG_PCB_ACL_USERMNGT
    const usermngt_user_t* pd = usermngt_userFindByName(username);
    if((pd == NULL) || !usermngt_userEnable(pd)) {
        SAH_TRACEZ_ERROR("pcb_client", "User not found or user disabled");
        client->client_data.uid = UINT32_MAX;
        return false;
    }
    client->client_data.uid = usermngt_userID(pd);
#else
    struct passwd* pd;
    if((pd = getpwnam(username)) == NULL) {
        SAH_TRACEZ_ERROR("pcb_client", "User not found or user disabled");
        client->client_data.uid = UINT32_MAX;
        return false;
    }
    client->client_data.uid = pd->pw_uid;
#endif
    return true;
}

uint32_t pcb_client_get_path_attributes(pcb_t* client) {
    if(!client) {
        return 0;
    }

    return client->client_data.attributes & (path_attr_key_notation | path_attr_slash_seperator);
}

uint32_t pcb_client_error(pcb_t* client) {
    return client->client_data.error;
}

const llist_t* pcb_client_errors(pcb_t* client) {
    if(!client) {
        return NULL;
    }

    return &client->client_data.errors;
}

void pcb_client_clear_errors(pcb_t* client) {
    if(!client) {
        return;
    }

    llist_iterator_t* it = llist_takeFirst(&client->client_data.errors);
    pcb_client_error_t* error = NULL;
    while(it) {
        error = llist_item_data(it, pcb_client_error_t, it);
        free(error->info);
        free(error);
        it = llist_takeFirst(&client->client_data.errors);
    }

    return;
}

peer_info_t* pcb_client_connect(pcb_t* client, const char* URI) {
    uri_t* uri = NULL;
    peer_info_t* dest = NULL;
    if(!client || !URI || !*URI) {
        goto error;
    }

    dest = connection_connect(pcb_connection(client), URI, &uri);
    if(!dest) {
        goto error_free_uri;
    }

    if(strcmp(uri_getScheme(uri), "pcb") != 0) {
        pcb_client_set_error(client, 0x00050002);
        goto error_free_dest;
    }

    uri_destroy(uri);

    return dest;

error_free_dest:
    peer_destroy(dest);
error_free_uri:
    uri_destroy(uri);
error:
    return NULL;
}

bool pcb_client_set(peer_info_t* dest, const char* objectPath, const char* parameter, const variant_t* val) {
    return pcb_client_set_validate(dest, objectPath, parameter, val, pcb_client_op_set);
}

bool pcb_client_validate(peer_info_t* dest, const char* objectPath, const char* parameter, const variant_t* val) {
    return pcb_client_set_validate(dest, objectPath, parameter, val, pcb_client_op_validate);
}

parameter_t* pcb_client_get_parameter(peer_info_t* dest, const char* objectPath, const char* parameter) {
    if(!parameter) {
        return NULL;
    }

    object_t* object = pcb_client_get_object(dest, objectPath);
    if(!object) {
        return NULL;
    }

    parameter_t* param = local_object_getParameter(object, parameter);
    if(!param) {
        pcb_t* pcb = pcb_fromConnection(peer_connection(dest));
        if(!pcb) {
            pcb_error = pcb_error_not_connected;
            return NULL;
        }
        pcb_error = pcb_error_not_found;
        pcb_client_set_error(pcb, pcb_error);
        return NULL;
    }

    return param;
}

const variant_t* pcb_client_get(peer_info_t* dest, const char* objectPath, const char* parameter) {
    const variant_t* retval = NULL;

    parameter_t* param = pcb_client_get_parameter(dest, objectPath, parameter);
    if(param) {
        retval = parameter_getValue(param);
    } else {
        pcb_t* pcb = pcb_fromConnection(peer_connection(dest));
        if(!pcb) {
            pcb_error = pcb_error_not_connected;
            return NULL;
        }
        pcb_error = pcb_error_not_found;
        pcb_client_set_error(pcb, pcb_error);
    }

    return retval;
}

function_t* pcb_client_get_function(peer_info_t* dest, const char* objectPath, const char* function) {
    if(!function) {
        return NULL;
    }

    object_t* object = pcb_client_get_object(dest, objectPath);
    if(!object) {
        return NULL;
    }

    function_t* fn = local_object_getFunction(object, function);
    if(!fn) {
        pcb_t* pcb = pcb_fromConnection(peer_connection(dest));
        if(!pcb) {
            pcb_error = pcb_error_not_connected;
            return NULL;
        }
        pcb_error = pcb_error_not_found;
        pcb_client_set_error(pcb, pcb_error);
        return NULL;
    }

    return fn;
}

bool pcb_client_set_object(peer_info_t* dest, const char* objectPath, variant_map_t* parameters) {
    return pcb_client_set_validate_object(dest, objectPath, parameters, pcb_client_op_set);
}

bool pcb_client_validate_object(peer_info_t* dest, const char* objectPath, variant_map_t* parameters) {
    return pcb_client_set_validate_object(dest, objectPath, parameters, pcb_client_op_validate);
}

object_t* pcb_client_get_object(peer_info_t* dest, const char* objectPath) {
    uint32_t attributes = 0;
    pcb_t* pcb = pcb_client_check_destination(dest, &attributes);
    request_t* req = NULL;
    object_t* retval = NULL;
    if(!pcb) {
        goto error;
    }

    /* check local data model */
    /* TODO: This should be removed, but some applications are depening on this functionality */
    if(objectPath && *objectPath) {
        object_t* object = object_getObject(datamodel_root(pcb_datamodel(pcb)), objectPath, attributes & (path_attr_key_notation | path_attr_slash_seperator), NULL);
        if(object) {
            return object;
        }
    }

    /* build request */
    req = request_create_getObject(objectPath, 0, request_getObject_parameters | request_getObject_children | request_getObject_instances | request_getObject_functions | attributes);
    if(!req) {
        pcb_client_set_error(pcb, pcb_error);
        goto error;
    }

    request_setUserID(req, pcb->client_data.uid);

    retval = pcb_client_reply_get_object(pcb, dest, req, objectPath);
    if(!retval) {
        goto error_cleanup_request;
    }

    request_destroy(req);
    return retval;

error_cleanup_request:
    request_destroy(req);
error:
    return NULL;
}

bool pcb_client_wait_for_object(peer_info_t* dest, const char* objectPath) {
    uint32_t attributes = 0;
    struct timeval timeout = { 0, 0 };
    struct timeval* t = NULL;
    pcb_client_callback_info_t info;
    request_t* req = NULL;

    pcb_t* pcb = pcb_client_check_destination(dest, &attributes);
    if(!pcb || !objectPath) {
        goto error;
    }

    /* check local data model */
    /* TODO: This should be removed, but some applications are depending on it. */
    if(objectPath && *objectPath) {
        object_t* object = object_getObject(datamodel_root(pcb_datamodel(pcb)), objectPath, attributes & (path_attr_key_notation | path_attr_slash_seperator), NULL);
        if(object) {
            return true;
        }
    }

    info.callback_function = NULL;
    info.userdata = (char*) objectPath;
    info.error = false;

    /* build request, query root depth 0 */
    req = request_create_getObject(objectPath, 0, request_no_object_caching | request_notify_object_added | attributes);
    if(!req) {
        pcb_client_set_error(pcb, pcb_error);
        goto error;
    }

    request_setUserID(req, pcb->client_data.uid);

    request_setReplyHandler(req, pcb_client_reply_wait_for_object);
    request_setData(req, &info);

    /* send the request */
    if(!pcb_sendRequest(pcb, dest, req)) {
        pcb_client_set_error(pcb, pcb_error);
        goto error_cleanup_request;
    }

    if(pcb->client_data.timeout) {
        timeout.tv_sec = pcb->client_data.timeout / 1000;
        timeout.tv_usec = (pcb->client_data.timeout % 1000) * 1000;
        t = &timeout;
    }

    /* wait for reply */
    if(!pcb_waitForReply(pcb, req, t)) {
        SAH_TRACEZ_ERROR("pcb_client", "Wait for failed, object not found");
        pcb_client_set_error(pcb, pcb_error);
        goto error_cleanup_request;
    }

    if(info.error) {
        SAH_TRACEZ_ERROR("pcb_client", "Wait for failed, object not found");
        goto error_cleanup_request;
    }

    request_setData(req, NULL);
    request_destroy(req);
    return true;

error_cleanup_request:
    request_setData(req, NULL);
    request_destroy(req);
error:
    return false;
}

static bool pcb_client_retrieve_objects(peer_info_t* dest, const char* objectPath, const char* pattern, pcb_client_object_callback fn, void* userdata) {
    uint32_t attributes = 0;
    pcb_t* pcb = pcb_client_check_destination(dest, &attributes);
    struct timeval timeout = { 0, 0 };
    struct timeval* t = NULL;
    pcb_client_callback_info_t info;
    request_t* req = NULL;

    if(!pcb) {
        goto error;
    }

    if(!fn) {
        goto error;
    }

    /* check local data model */
    /* TODO: this should be removed, bu some applications are depending on this functionality */
    if(objectPath && *objectPath) {
        object_t* object = object_getObject(datamodel_root(pcb_datamodel(pcb)), objectPath, attributes & (path_attr_key_notation | path_attr_slash_seperator), NULL);
        if(object) {
            if(pattern) {
                pcb_client_return_objects_pattern(pcb, object, pattern, attributes, fn, userdata);
            } else {
                pcb_client_return_objects(pcb, object, fn, userdata);
            }
            return true;
        }
    }

    info.callback_function = fn;
    info.userdata = userdata;
    info.error = false;

    /* build request */
    if(pattern) {
        req = request_create_findObjects(objectPath, pattern, -1, request_getObject_parameters | request_getObject_children | request_getObject_instances |
                                         request_no_object_caching | request_find_include_parameters | request_find_include_values | attributes);
    } else {
        req = request_create_getObject(objectPath, -1, request_getObject_parameters | request_getObject_children | request_getObject_instances | request_no_object_caching | attributes);
    }
    if(!req) {
        pcb_client_set_error(pcb, pcb_error);
        goto error;
    }

    request_setUserID(req, pcb->client_data.uid);

    request_setReplyHandler(req, pcb_client_reply_recieved);
    request_setData(req, &info);

    /* send the request */
    if(!pcb_sendRequest(pcb, dest, req)) {
        pcb_client_set_error(pcb, pcb_error);
        goto error_cleanup_request;
    }

    if(pcb->client_data.timeout) {
        timeout.tv_sec = pcb->client_data.timeout / 1000;
        timeout.tv_usec = (pcb->client_data.timeout % 1000) * 1000;
        t = &timeout;
    }

    /* wait for reply */
    if(!pcb_waitForReply(pcb, req, t)) {
        pcb_client_set_error(pcb, pcb_error);
        goto error_cleanup_request;
    }

    if(info.error) {
        goto error_cleanup_request;
    }

    request_destroy(req);
    return true;

error_cleanup_request:
    request_destroy(req);
error:
    return false;
}

bool pcb_client_get_objects(peer_info_t* dest, const char* objectPath, pcb_client_object_callback fn, void* userdata) {
    return pcb_client_retrieve_objects(dest, objectPath, NULL, fn, userdata);
}

bool pcb_client_find_objects(peer_info_t* dest, const char* objectPath, const char* pattern, pcb_client_object_callback fn, void* userdata) {
    if(!pattern || !*pattern) {
        return false;
    }

    return pcb_client_retrieve_objects(dest, objectPath, pattern, fn, userdata);
}

object_t* pcb_client_add_instance(peer_info_t* dest, const char* objectPath, uint32_t index, const char* key, variant_map_t* parameters) {
    uint32_t attributes = 0;
    pcb_t* pcb = pcb_client_check_destination(dest, &attributes);
    request_t* req = NULL;
    object_t* object = NULL;
    if(!pcb) {
        goto error;
    }

    /* check local data model */
    /* TODO: This should be removed, but some applications are depending on this functionality */
    if(objectPath && *objectPath) {
        object_t* object = object_getObject(datamodel_root(pcb_datamodel(pcb)), objectPath, attributes & (path_attr_key_notation | path_attr_slash_seperator), NULL);
        if(object) {
            object_t* instance = object_createInstance(object, index, key);
            if(!instance) {
                goto error;
            }
            if(!object_commit(object)) {
                object_rollback(object);
                goto error;
            }
            return instance;
        }
    }

    /* build request */
    req = request_create_addInstance(objectPath, index, key, attributes);
    if(!req) {
        pcb_client_set_error(pcb, pcb_error);
        goto error;
    }

    request_setUserID(req, pcb->client_data.uid);

    if(parameters) {
        /* add parameters to request */
        variant_map_iterator_t* it = NULL;
        const char* name = NULL;
        variant_t* val = NULL;
        variant_map_for_each(it, parameters) {
            val = variant_map_iterator_data(it);
            name = variant_map_iterator_key(it);
            char* data = variant_char(val);
            SAH_TRACEZ_INFO("pcb_client", "Adding parameter %s with value %s", name, data);
            free(data);
            request_addParameter(req, name, val);
        }
    }

    object = pcb_client_reply_get_object(pcb, dest, req, NULL);
    if(!object) {
        goto error_cleanup_request;
    }

    if(!object_isInstance(object)) {
        pcb_client_set_error(pcb, pcb_error_not_instance);
        goto error_cleanup_request;
    }

    request_destroy(req);
    return object;

error_cleanup_request:
    request_destroy(req);
error:
    return NULL;
}

bool pcb_client_delete_instance(peer_info_t* dest, const char* objectPath) {
    uint32_t attributes = 0;
    request_t* req = NULL;
    object_t* object = NULL;
    pcb_t* pcb = pcb_client_check_destination(dest, &attributes);
    if(!pcb) {
        goto error;
    }

    /* check local data model */
    /* TODO: this should be removed, but some applications are depending on it */
    if(objectPath && *objectPath) {
        object_t* object = object_getObject(datamodel_root(pcb_datamodel(pcb)), objectPath, attributes & (path_attr_key_notation | path_attr_slash_seperator), NULL);
        if(object) {
            if(!object_delete(object)) {
                goto error;
            }
            if(!object_commit(object)) {
                object_rollback(object);
                goto error;
            }
            return true;
        }
    }

    /* build request */
    req = request_create_deleteInstance(objectPath, attributes);
    if(!req) {
        pcb_client_set_error(pcb, pcb_error);
        goto error;
    }

    request_setUserID(req, pcb->client_data.uid);

    if(!pcb_client_check_reply_error(pcb, dest, req)) {
        goto error_cleanup_request;
    }

    object = local_object_getObject(datamodel_root(pcb_cache(pcb)), objectPath, attributes & (path_attr_key_notation | path_attr_slash_seperator), NULL);
    if(object) {
        pcb_cacheRemoveObject(pcb, object);
        pcb_cacheCommit(pcb);
    }

    request_destroy(req);
    return true;

error_cleanup_request:
    request_destroy(req);
error:
    return false;
}
