/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <string.h>
#include <errno.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <libgen.h>
#include <unistd.h>
#include <dirent.h>

#include <debug/sahtrace.h>

#include <pcb/common/error.h>
#include <pcb/core/pcb_main.h>
#include <pcb/core/mapping.h>

#include <pcb/utils/string_list.h>

#include "core_priv.h"
#include "datamodel_priv.h"
#include "parameter_priv.h"
#include "pcb_main_priv.h"

/**
   @file
   @brief
   Implementation of a data model
 */

static void datamodel_prefix_filename(string_t* file) {
    const char* prefix = getenv("PCB_PREFIX_PATH");
    string_prependChar(file, prefix);
}

static void datamodel_load_check_autoresolve(datamodel_t* dm) {
    const char* resolve = getenv("PCB_AUTO_RESOLVE");
    if(!resolve) {
        return;
    }

    if(strcmp(resolve, "no") == 0) {
        /* autoresolve off */
        datamodel_autoResolve(dm, false);
    } else if(strcmp(resolve, "yes") == 0) {
        datamodel_autoResolve(dm, true);
    } else {
        return;
    }
}

static bool datamodel_load_file2(object_t* parent, const char* file, serialize_handlers_t* handlers) {
    int fd = open(file, O_RDONLY);
    if(fd == -1) {
        pcb_error = errno;
        SAH_TRACEZ_NOTICE("pcb", "open failed (%s) for file %s", error_string(pcb_error), file);
        return false;
    }

    if(!handlers->load(fd, parent, file)) {
        pcb_error = errno;
        SAH_TRACEZ_NOTICE("pcb", "load failed %s", error_string(pcb_error));
        /* =====================================================================
           closing of the file should be done in the handler
           =====================================================================
         */
        return false;
    }
    /* =====================================================================
       closing of the file should be done in the handler
       =====================================================================
     */

    return true;
}

static bool datamodel_try_file(const char* file, struct stat* statbuf) {
    if(!statbuf || !file || !*file) {
        return false;
    }

    if(stat(file, statbuf) == 0) {
        return true;
    }

    pcb_error = errno;
    SAH_TRACEZ_NOTICE("pcb", "stat failed (%s) for file %s", error_string(pcb_error), file);
    return false;
}

static bool datamodel_is_absolute_path(const char* path) {
    return path && path[0] == '/';
}

static bool datamodel_find_file(datamodel_t* dm, const char* file, struct stat* statbuf, string_t* foundfile) {
    if(!file || !*file) {
        SAH_TRACEZ_ERROR("pcb", "no filename given to search for");
        return false;
    }
    if(!foundfile || !statbuf) {
        SAH_TRACEZ_ERROR("pcb", "invalid structures passed");
        return false;
    }

    /* search file in the following order
        0. use absolute path
        else
        1. use "/etc/defaults" + filename
        2. use filename
        3. use "/usr/lib/" + name + filename
     */
    if(datamodel_is_absolute_path(file)) {
        string_fromChar(foundfile, file);
        datamodel_prefix_filename(foundfile);
        if(datamodel_try_file(string_buffer(foundfile), statbuf)) {
            return true;
        }
    } else {
        // 1. use "/etc/defaults" + filename
        string_fromChar(foundfile, "/etc/defaults/");
        string_appendChar(foundfile, file);
        datamodel_prefix_filename(foundfile);
        if(datamodel_try_file(string_buffer(foundfile), statbuf)) {
            return true;
        }

        // 2. use filename
        if(datamodel_try_file(file, statbuf)) {
            string_fromChar(foundfile, file);
            return true;
        }

        // 3. use "/usr/lib/" + name + filename
        const char* pcb_name = connection_name(pcb_connection(datamodel_pcb(dm)));
        if(pcb_name) {
            string_clear(foundfile);
            string_appendFormat(foundfile, "/usr/lib/%s/%s", pcb_name, file);
            datamodel_prefix_filename(foundfile);
            if(datamodel_try_file(string_buffer(foundfile), statbuf)) {
                return true;
            }
        }
    }

    SAH_TRACEZ_WARNING("pcb", "file %s not found on search paths", file);
    string_clear(foundfile);
    return false;
}

static bool datamodel_readdir(const char* dir, string_list_t* list, bool sort) {
    bool retval = false;
    string_t full_path;
    DIR* dirp = NULL;
    struct dirent* dentry = NULL;
    struct stat statbuf;

    dirp = opendir(dir);
    if(!dirp) {
        pcb_error = errno;
        SAH_TRACEZ_NOTICE("pcb", "open failed (%s) for directory %s", error_string(pcb_error), dir);
        return false;
    }

    string_initialize(&full_path, 0);

    errno = 0;
    while((dentry = readdir(dirp))) {
        if(dentry->d_name[0] == '.') {
            continue;
        }
        int len = strlen(dentry->d_name);
        if((len < 5) ||
           strcmp(&(dentry->d_name[len - 4]), ".odl")) {
            continue;
        }
        string_cleanup(&full_path);
        string_appendFormat(&full_path, "%s/%s", dir, dentry->d_name);

        if(!datamodel_try_file(string_buffer(&full_path), &statbuf)) {
            continue;
        }
        if((statbuf.st_mode & S_IFMT) == S_IFDIR) {
            continue;
        }

        string_list_append(list, string_list_iterator_create(&full_path));
    }

    if(errno) {
        pcb_error = errno;
        SAH_TRACEZ_NOTICE("pcb", "read failed (%s) for directory %s", error_string(pcb_error), dir);
        goto error_cleanup;
    }

    if(sort) {
        if(!string_list_qsort(list, string_list_qsort_strcmp, false, NULL)) {
            goto error_cleanup;
        }
    }

    retval = true;

error_cleanup:
    closedir(dirp);
    string_cleanup(&full_path);
    return retval;
}

static bool datamodel_load_dir(object_t* parent, const char* dir, serialize_handlers_t* handlers) {
    bool retval = false;
    string_list_t file_list;
    string_list_iterator_t* iter = NULL;

    string_list_initialize(&file_list);

    if(!datamodel_readdir(dir, &file_list, true)) {
        goto error_cleanup;
    }

    string_list_for_each(iter, &file_list) {
        if(!datamodel_load_file2(parent, string_buffer(string_list_iterator_data(iter)), handlers)) {
            goto error_cleanup;
        }
    }

    retval = true;

error_cleanup:
    string_list_cleanup(&file_list);
    return retval;
}

static bool datamodel_load_file(datamodel_t* dm, object_t* parent, const char* file, uint32_t format) {
    bool retval = false;
    struct stat statbuf;

    serialize_handlers_t* handlers = serialization_getHandlers(format);
    if(!handlers) {
        SAH_TRACEZ_ERROR("pcb", "serialization_getHandlers failed %s", error_string(pcb_error));
        return false;
    }

    if(!handlers->load) {
        pcb_error = pcb_error_no_serializer;
        SAH_TRACEZ_ERROR("pcb", "Could not find serialization plugin, check plugin directory %s", error_string(pcb_error));
        return false;
    }

    string_t foundfile;
    string_initialize(&foundfile, 0);
    if(!datamodel_find_file(dm, file, &statbuf, &foundfile)) {
        goto error_cleanup;
    }

    datamodel_load_check_autoresolve(dm);

    switch(statbuf.st_mode & S_IFMT) {
    case S_IFREG:
        retval = datamodel_load_file2(parent, string_buffer(&foundfile), handlers);
        break;
    case S_IFDIR:
        retval = datamodel_load_dir(parent, string_buffer(&foundfile), handlers);
        break;
    default:
        pcb_error = EMEDIUMTYPE;
        SAH_TRACEZ_ERROR("pcb", "Unable to load file (%s) for file %s", error_string(pcb_error), string_buffer(&foundfile));
        goto error_cleanup;
    }

    if(retval) {
        object_commit(parent);
    } else {
        object_rollback(parent);
    }

error_cleanup:
    string_cleanup(&foundfile);

    return retval;
}

static void datamodel_remove_object(object_t* root) {
    object_t* child = object_firstChild(root);
    while(child) {
        SAH_TRACEZ_INFO("pcb", "Delete object %s", object_name(child, 0));
        object_destroy_recursive(child);
        child = object_firstChild(root);
    }

    parameter_t* parameter = object_firstParameter(root);
    while(parameter) {
        SAH_TRACEZ_INFO("pcb", "Delete parameter %s from root object", parameter_name(parameter));
        parameter_destroy(parameter);
        parameter = object_firstParameter(root);
    }

    function_t* function = object_firstFunction(root);
    while(function) {
        SAH_TRACEZ_INFO("pcb", "Delete function %s from root object", function_name(function));
        function_delete(function);
        function = object_firstFunction(root);
    }
}

/**
   @ingroup pcb_core_dm_management
   @brief
   Initialize a datamodel_t structure

   @details
   Initializes all elements of a datamodel_t structure

   @param dm A pointer to a data model object created with @ref datamodel_create

   @return
    - true: data model is initialized
    - false: failed to initialize data model
 */
bool datamodel_initialize(datamodel_t* dm) {
    if(!dm) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }
    /* the root object has no name */
    memset(&dm->root, 0, sizeof(object_t));
    /* the mib root object has no name */
    memset(&dm->mibRoot, 0, sizeof(object_t));

    /* set the state to created */
    dm->root.state = object_state_created;
    dm->mibRoot.state = object_state_created;
    /* strip of the internal attributes */
    dm->root.attributes = object_attr_default | object_attr_persistent;
    dm->mibRoot.attributes = object_attr_default | object_attr_mib;

    dm->attributes = datamodel_attr_default;

    if(!object_initialize(&dm->root)) {
        SAH_TRACEZ_ERROR("pcb", "object_initialize %s", error_string(pcb_error));
        goto error_cleanup;
    }

    if(!object_initialize(&dm->mibRoot)) {
        SAH_TRACEZ_ERROR("pcb", "object_initialize %s", error_string(pcb_error));
        goto error_cleanup;
    }

    llist_initialize(&dm->notify);

    dm->root.state = object_state_ready;
    dm->mibRoot.state = object_state_ready;
    dm->mibdir = NULL;

    pcb_error = pcb_ok;
    return true;

error_cleanup:
    object_cleanup(&dm->root);
    object_cleanup(&dm->mibRoot);
    return false;
}

/**
   @ingroup pcb_core_dm_management
   @brief
   Clean up off a datamodel_t structure

   @details
   Clean up all elements of a datamodel_t structure

   @param dm A pointer to a data model object created with @ref datamodel_create
 */
void datamodel_cleanup(datamodel_t* dm) {
    if(!dm) {
        pcb_error = pcb_error_invalid_parameter;
        return;
    }

    datamodel_clear(dm);
    object_cleanup(&dm->root);
    object_cleanup(&dm->mibRoot);
    free(dm->mibdir);

    llist_iterator_t* it = llist_takeFirst(&dm->notify);
    datamodel_notify_handlers_t* handler = NULL;
    while(it) {
        handler = llist_item_data(it, datamodel_notify_handlers_t, it);
        free(handler);
        it = llist_takeFirst(&dm->notify);
    }

    pcb_error = pcb_ok;
}

/**
   @ingroup pcb_core_dm_management
   @brief
   Create a data model object.

   @details
   This function creates and initialize a datamodel_t structure.

   @return
    - A pointer to a datamodel_t structure
    - NULL: an error has occured. See @ref pcb_error to get error details.
 */
datamodel_t* datamodel_create(void) {
    datamodel_t* dm = (datamodel_t*) calloc(1, sizeof(datamodel_t));
    if(!dm) {
        pcb_error = pcb_error_no_memory;
        SAH_TRACEZ_ERROR("pcb", "%s", error_string(pcb_error));
        return NULL;
    }
    if(!datamodel_initialize(dm)) {
        SAH_TRACEZ_ERROR("pcb", "datamodel_initialize failed %s", error_string(pcb_error));
        free(dm);
        return NULL;
    }

    pcb_error = pcb_ok;
    return dm;
}

/**
   @ingroup pcb_core_dm_management
   @brief
   Remove all objects of a datamodel

   @details
   Remove all (except the root object) objects of a datamodel

   @param dm A pointer to a data model object created with @ref datamodel_create
 */
void datamodel_clear(datamodel_t* dm) {
    if(!dm) {
        pcb_error = pcb_error_invalid_parameter;
        return;
    }

    /* remove the destinations */
    object_destination_t* dest = object_firstDestination(&dm->root);
    while(dest) {
        destination_delete(dest);
        dest = object_firstDestination(&dm->root);
    }

    /* delete the tree */
    datamodel_remove_object(&dm->root);

    /* delete the mibs */
    datamodel_remove_object(&dm->mibRoot);

    pcb_error = pcb_ok;
}

/**
   @ingroup pcb_core_dm_management
   @brief
   Destroy a data model object.

   @details
   This function free up the memory used by the fata model object

   @param dm A pointer to a data model object created with @ref datamodel_create
 */
void datamodel_destroy(datamodel_t* dm) {
    if(!dm) {
        pcb_error = pcb_error_invalid_parameter;
        return;
    }

    datamodel_cleanup(dm);
    free(dm);
    pcb_error = pcb_ok;
}

/**
   @ingroup pcb_core_dm_management
   @brief
   Return the root object of a data model

   @details
   Each data model always have at least one object, the root object. This function will return
   this object.

   @param dm A pointer to a data model object created with @ref datamodel_create

   @return
    - a pointer to the root object
 */
object_t* datamodel_root(datamodel_t* dm) {
    if(!dm) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    pcb_error = pcb_ok;
    return &dm->root;
}

bool datamodel_setMibDir(datamodel_t* dm, const char* mibDir) {
    if(!dm) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    free(dm->mibdir);
    dm->mibdir = strdup(mibDir);
    if(!dm->mibdir) {
        return false;
    }

    return true;
}

const char* datamodel_getMibDir(datamodel_t* dm) {
    if(!dm) {
        return NULL;
    }

    return dm->mibdir;
}

char* datamodel_mibFile(datamodel_t* dm, const char* mibName) {
    struct stat buf;
    SAH_TRACEZ_IN("pcb");
    const char* prefix = getenv("PCB_PREFIX_PATH");
    string_t filename;
    string_t prefixed_filename;
    const char* mibdir = getenv("PCB_MIB_DIR");

    if(dm->attributes & datamodel_attr_cache) {
        /* data models used for caching can not load Mibs */
        goto exit_error;
    }

    string_initialize(&filename, 64);
    string_initialize(&prefixed_filename, 64);
    /* search in default mib dir */
    string_appendFormat(&filename, "%s/%s.odl", dm->mibdir, mibName);
    string_appendFormat(&prefixed_filename, "%s%s%s/%s.odl", prefix ? prefix : "", prefix ? "/" : "", dm->mibdir, mibName);
    SAH_TRACEZ_INFO("pcb", "verify mib file [%s] exists", string_buffer(&prefixed_filename));
    if(stat(string_buffer(&prefixed_filename), &buf) != -1) {
        string_cleanup(&prefixed_filename);
        return (char*) string_buffer(&filename);
    }
    SAH_TRACEZ_ERROR("pcb", "mib file [%s] not available", string_buffer(&prefixed_filename));
    string_clear(&prefixed_filename);
    string_clear(&filename);

    /* get content of PCB_MIB_DIR env variable */
    if(!mibdir || !(*mibdir)) {
        /* not available or empty, exit */
        goto exit_done;
    }
    /* search in mid dir */
    string_appendFormat(&filename, "%s/%s.odl", mibdir, mibName);
    string_appendFormat(&prefixed_filename, "%s%s%s/%s.odl", prefix ? prefix : "", prefix ? "/" : "", mibdir, mibName);
    SAH_TRACEZ_INFO("pcb", "verify mib file [%s] exists", string_buffer(&prefixed_filename));
    if(stat(string_buffer(&prefixed_filename), &buf) != -1) {
        string_cleanup(&prefixed_filename);
        return (char*) string_buffer(&filename);
    }
    SAH_TRACEZ_ERROR("pcb", "mib file [%s] not available", string_buffer(&prefixed_filename));

exit_done:
    string_cleanup(&prefixed_filename);
    string_cleanup(&filename);
exit_error:
    SAH_TRACEZ_OUT("pcb");
    return false;
}

/**
   @ingroup pcb_core_dm_management
   @brief
   Search a mib.

   @details
   This function will load a MIB definition file (odl file) with the same name as the given mib and with "odl" extenstion
   in the following directories in the specified order:
   - <default mib dir>/<mibname>.odl
   - $(PCB_MIB_DIR)/<mibname>.odl

   The default MIB dir can be using function @ref datamodel_setMibDir

   @param dm A pointer to a data model object created with @ref datamodel_create
   @param mibName The name of the mib

   @return
    - true: a file was found and loaded
    - false an error occured, see pcb_error for more details
 */
bool datamodel_mibLoad(datamodel_t* dm, const char* mibName, const char* path) {
    SAH_TRACEZ_IN("pcb");
    const char* mibdir = NULL;
    string_t filename;

    if(dm->attributes & datamodel_attr_cache) {
        /* data models used for caching can not load Mibs */
        goto exit_error;
    }

    string_initialize(&filename, 64);

    if(path && *path) {
        /* search in provided path */
        string_appendFormat(&filename, "%s/%s.odl", path, mibName);
        SAH_TRACEZ_INFO("pcb", "Trying to load mib file [%s]", string_buffer(&filename));
        if(datamodel_load_file(dm, datamodel_root(dm), string_buffer(&filename), SERIALIZE_FORMAT(serialize_format_odl, 0, 0))) {
            goto exit_done;
        }
        SAH_TRACEZ_NOTICE("pcb", "Loading [%s] failed", string_buffer(&filename));
        string_clear(&filename);
    }

    /* search in default mib dir */
    string_appendFormat(&filename, "%s/%s.odl", dm->mibdir, mibName);
    SAH_TRACEZ_INFO("pcb", "Trying to load mib file [%s]", string_buffer(&filename));
    if(datamodel_load_file(dm, datamodel_root(dm), string_buffer(&filename), SERIALIZE_FORMAT(serialize_format_odl, 0, 0))) {
        goto exit_done;
    }
    SAH_TRACEZ_NOTICE("pcb", "Loading [%s] failed", string_buffer(&filename));
    string_clear(&filename);

    /* get content of PCB_MIB_DIR env variable */
    mibdir = getenv("PCB_MIB_DIR");
    if(!mibdir || !(*mibdir)) {
        /* not available or empty, exit */
        goto exit_string_cleanup;
    }
    /* search in mid dir */
    string_appendFormat(&filename, "%s/%s.odl", mibdir, mibName);
    SAH_TRACEZ_INFO("pcb", "Trying to load mib file [%s]", string_buffer(&filename));
    if(!datamodel_load_file(dm, datamodel_root(dm), string_buffer(&filename), SERIALIZE_FORMAT(serialize_format_odl, 0, 0))) {
        SAH_TRACEZ_ERROR("pcb", "Loading failed");
        goto exit_string_cleanup;
    }

exit_done:
    string_cleanup(&filename);
    SAH_TRACEZ_OUT("pcb");
    return true;

exit_string_cleanup:
    string_cleanup(&filename);
exit_error:
    SAH_TRACEZ_OUT("pcb");
    return false;
}

/**
   @ingroup pcb_core_dm_management
   @brief
   Return the mib root object of a data model.

   @details
   MIBs can be used to extend objects in the data model. A MIB is defined as an object and is placed under the MIB root.
   MIBs are not visible in the data model.

   @param dm A pointer to a data model object created with @ref datamodel_create

   @return
    - a pointer to the MIB root object
 */
object_t* datamodel_mibRoot(datamodel_t* dm) {
    if(!dm) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    pcb_error = pcb_ok;
    return &dm->mibRoot;
}

/**
   @ingroup pcb_core_dm_management
   @brief
   Search a mib.

   @details
   MIBs can be used to extend objects in the data model. A MIB is defined as an object and is placed under the MIB root.
   MIBs are not visible in the data model.

   @param dm A pointer to a data model object created with @ref datamodel_create
   @param mibName The name of the mib

   @return
    - a pointer to the MIB object
    - or NULL if not available
 */
object_t* datamodel_mib(datamodel_t* dm, const char* mibName) {
    if(!dm) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    return object_getObject(&dm->mibRoot, mibName, path_attr_key_notation, NULL);
}

/**
   @ingroup pcb_core_dm_management
   @brief
   Set a name space for the data model

   @details
   Set a global unique name space for the data model.

   @param dm A pointer to a data model object created with @ref datamodel_create
   @param ns The name space

   @return
    - true: The name space is set.
    - false: An error has occured, see @ref pcb_error for more detailed information.
 */
bool datamodel_setNamespace(datamodel_t* dm, const char* ns) {
    if(!dm) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    free(dm->root.oname);
    dm->root.oname = ns ? strdup(ns) : NULL;

    return true;
}

/**
   @ingroup pcb_core_dm_management
   @brief
   Get the data model name space

   @details
   Get the data model name space.

   @param dm A pointer to a data model object created with @ref datamodel_create

   @return
    - The name space for the dat amodel.
    - NULL when no name space is available.
 */
const char* datamodel_namespace(datamodel_t* dm) {
    if(!dm) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    return dm->root.oname ? dm->root.oname : "";
}

/**
   @ingroup pcb_core_dm_management
   @brief
   Load a data model from a file

   @details
   Load a data model from a file using the specified serialization format.

   @param dm A pointer to a data model object created with @ref datamodel_create
   @param file Absolute or relative path to the file containing the object definitions
   @param format Format identifier, use @ref SERIALIZE_FORMAT macro to build a format identifier

   @return
    - true: The file is loaded successfully and the objects are created.
    - false: An error has occured, see @ref pcb_error for more detailed information.
 */
bool datamodel_load(datamodel_t* dm, const char* file, uint32_t format) {
    if(!dm) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    return datamodel_load_file(dm, datamodel_root(dm), file, format);
}

/**
   @ingroup pcb_core_dm_management
   @brief
   Verify a data model definition file.

   @details
   Verify a data model definition file for syntax correctness using the specified serialization format.

   @param dm A pointer to a data model object created with @ref datamodel_create
   @param file Absolute or relative path to the file containing the object definitions
   @param format Format identifier, use @ref SERIALIZE_FORMAT macro to build a format identifier
   @param includes Pointer to an initialized list, at return will contain a list of all files included
   @param types Pointer to an initialized variant map, at return will include all custom types defined (not yet implemented)
   @param hasdefinition Pointer a boolean, at return will be set to true if the file contains data model definitions or false
                     if the file contains only data model values (data)

   @return
    - true: The file is verified uccessfully.
    - false: An error has occured, see @ref pcb_error for more detailed information.
 */
bool datamodel_verify(datamodel_t* dm, const char* file, uint32_t format, variant_map_t* info, bool* hasdefinition) {
    SAH_TRACEZ_IN("pcb");
    if(!dm) {
        pcb_error = pcb_error_invalid_parameter;
        SAH_TRACEZ_OUT("pcb");
        return false;
    }

    serialize_handlers_t* handlers = serialization_getHandlers(format);
    if(!handlers) {
        SAH_TRACEZ_ERROR("pcb", "serialization_getHandlers failed %s", error_string(pcb_error));
        SAH_TRACEZ_OUT("pcb");
        return false;
    }

    if(!handlers->verify) {
        pcb_error = pcb_error_no_serializer;
        SAH_TRACEZ_ERROR("pcb", "Could not find serialization plugin, check plugin directory %s", error_string(pcb_error));
        SAH_TRACEZ_OUT("pcb");
        return false;
    }

    string_t prefixedfile;
    string_initialize(&prefixedfile, 0);
    string_fromChar(&prefixedfile, file);
    datamodel_prefix_filename(&prefixedfile);

    int fd = open(string_buffer(&prefixedfile), O_RDONLY);
    if(fd == -1) {
        string_cleanup(&prefixedfile);
        pcb_error = errno;
        SAH_TRACEZ_ERROR("pcb", "open failed %s", error_string(pcb_error));
        SAH_TRACEZ_OUT("pcb");
        return false;
    }

    if(!handlers->verify(fd, string_buffer(&prefixedfile), info, hasdefinition)) {
        pcb_error = errno;
        SAH_TRACEZ_ERROR("pcb", "verify failed %s", error_string(pcb_error));
        /* =====================================================================
           closing of the file should be done in the handler
           =====================================================================
         */
        string_cleanup(&prefixedfile);
        SAH_TRACEZ_OUT("pcb");
        return false;
    }
    /* =====================================================================
       closing of the file should be done in the handler
       =====================================================================
     */
    string_cleanup(&prefixedfile);

    SAH_TRACEZ_OUT("pcb");
    return true;
}

/**
   @ingroup pcb_core_dm_management
   @brief
   Save an object and it's children or instances

   @details
   This function will save an object (including the instances and children, depending on the depth specified) to
   a file using the specified serialization format.

   @param object A pointer to an object that has to be saved
   @param depth The depth
   @param file File name (inluding path) to which the object has to be saved
   @param format Format identifier, use @ref SERIALIZE_FORMAT macro to build a format identifier

   @return
    - true: The object is saved and the file is created
    - false: An error has occured, see @ref pcb_error for more detailed information.
 */
bool datamodel_save(object_t* object, uint32_t depth, const char* file, uint32_t format) {
    SAH_TRACEZ_INFO("pcb", "Saving data model ....");
    if(!object) {
        SAH_TRACEZ_ERROR("pcb", "No object specified, save failed");
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    serialize_handlers_t* handlers = serialization_getHandlers(format);
    if(!handlers) {
        SAH_TRACEZ_ERROR("pcb", "serialization_getHandlers failed %s", error_string(pcb_error));
        return false;
    }

    if(!handlers->save) {
        pcb_error = pcb_error_no_serializer;
        SAH_TRACEZ_ERROR("pcb", "Could not find serialization plugin, check plugin directory %s", error_string(pcb_error));
        return false;
    }

    string_t prefixedfile;
    string_initialize(&prefixedfile, 0);
    string_fromChar(&prefixedfile, file);
    datamodel_prefix_filename(&prefixedfile);

    string_t temp_file;
    string_initialize(&temp_file, 0);
    string_copy(&temp_file, &prefixedfile);
    string_appendChar(&temp_file, ".temp");
    SAH_TRACEZ_INFO("pcb", "Saving to file %s", string_buffer(&temp_file));

    int fd = open(string_buffer(&temp_file), O_WRONLY | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
    if(fd == -1) {
        string_cleanup(&temp_file);
        string_cleanup(&prefixedfile);
        pcb_error = errno;
        SAH_TRACEZ_ERROR("pcb", "open failed %s", error_string(pcb_error));
        return false;
    }
    if(!handlers->save(fd, object, depth, string_buffer(&temp_file))) {
        pcb_error = errno;
        SAH_TRACEZ_ERROR("pcb", "save failed %s", error_string(pcb_error));
        /* =====================================================================
           closing of the file should be done in the handler
           =====================================================================
         */
        string_cleanup(&temp_file);
        string_cleanup(&prefixedfile);
        return false;
    }
    /* =====================================================================
       closing of the file should be done in the handler
       =====================================================================
     */

    SAH_TRACEZ_INFO("pcb", "Rename %s to %s", string_buffer(&temp_file), string_buffer(&prefixedfile));
    if(rename(string_buffer(&temp_file), string_buffer(&prefixedfile)) == -1) {
        SAH_TRACEZ_ERROR("pcb", "Failed to rename file %d - %s", errno, error_string(errno));
    }

    /* sync the dir entry */
    int dir_fd = -1;
    char* containing_dir = dirname((char*) string_buffer(&temp_file));
    dir_fd = open(containing_dir, O_RDONLY);
    if(dir_fd < 0) {
        SAH_TRACEZ_ERROR("pcb", "Failed to open containing directory %d - %s", errno, error_string(errno));
    } else {
        if(fsync(dir_fd) < 0) {
            SAH_TRACEZ_ERROR("pcb", "Failed to fsync containing directory %d - %s", errno, error_string(errno));
        }
        if(close(dir_fd) < 0) {
            SAH_TRACEZ_ERROR("pcb", "Failed to close containing directory %d - %s", errno, error_string(errno));
        }
    }
    string_cleanup(&temp_file);
    string_cleanup(&prefixedfile);

    SAH_TRACEZ_INFO("pcb", "Saving done");
    return true;
}

/**
   @ingroup pcb_core_dm_management
   @brief
   Load an object definition file into the data model

   @details
   Build the object tree from a file describing all the objects in the data model.
   You can load multiple definition files in an data model.

   @param dm A pointer to a data model object created with @ref datamodel_create
   @param file Absolute or relative path to the file containing the object definitions

   @return
    - true: The file is loaded successfully and the objects are created.
    - false: An error has occured, see @ref pcb_error for more detailed information.
 */
bool datamodel_loadDefinition(datamodel_t* dm, const char* file) {
    if(!dm) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    return datamodel_load(dm, file, SERIALIZE_FORMAT(serialize_format_odl, 0, 0));
}

/**
   @ingroup pcb_core_dm_management
   @brief
   Add a global notification handler to the data model

   @details
   A global notification handler can be added to the data model. This handler will be called each time something changes in
   the data model. Multiple handlers can be added to each data model.\n
   Use this function with care, adding global handlers to the data model could have some performance impact.

   @param dm A pointer to a data model object created with @ref datamodel_create
   @param fn pointer to the function

   @return
    - true: Handler is added.
    - false: Failed to add the handler.
 */
bool datamodel_addNotifyHandler(datamodel_t* dm, datamodel_notify_handler_t fn) {
    if(!dm || !fn) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    datamodel_notify_handlers_t* handler = NULL;
    llist_iterator_t* it = NULL;
    llist_for_each(it, &dm->notify) {
        handler = llist_item_data(it, datamodel_notify_handlers_t, it);
        if(handler->datamodel_changed == fn) {
            break;
        }
        handler = NULL;
    }

    if(handler) {
        SAH_TRACEZ_INFO("pcb", "Handler already registered found");
        return true;
    }

    handler = (datamodel_notify_handlers_t*) calloc(1, sizeof(datamodel_notify_handlers_t));
    if(!handler) {
        return false;
    }

    handler->datamodel_changed = fn;
    return llist_append(&dm->notify, &handler->it);
}

/**
   @ingroup pcb_core_dm_management
   @brief
   Remove a global notification handler from the data model

   @details
   Remove a previous added global notification handler.

   @param dm A pointer to a data model object created with @ref datamodel_create
   @param fn pointer to the function

   @return
    - true: Handler is removed.
    - false: Failed to remove the handler.
 */
bool datamodel_removeNotifyHandler(datamodel_t* dm, datamodel_notify_handler_t fn) {
    if(!dm || !fn) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    datamodel_notify_handlers_t* handler = NULL;
    llist_iterator_t* it = NULL;
    llist_for_each(it, &dm->notify) {
        handler = llist_item_data(it, datamodel_notify_handlers_t, it);
        if(handler->datamodel_changed == fn) {
            break;
        }
        handler = NULL;
    }

    if(!handler) {
        SAH_TRACEZ_INFO("pcb", "Handler not found");
        return true;
    }

    SAH_TRACEZ_INFO("pcb", "Removing handler");
    llist_iterator_take(&handler->it);
    free(handler);

    return true;
}

/**
   @ingroup pcb_core_dm_management
   @brief
   Add user defined data to the data model

   @details
   Add user defined data to the data model..

   @param dm A pointer to a data model object created with @ref datamodel_create
   @param data user defined data

   @return
    - true: data is added.
    - false: Failed to add the data.
 */
bool datamodel_setUserData(datamodel_t* dm, void* data) {
    if(!dm) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    dm->userData = data;

    return true;
}

/**
   @ingroup pcb_core_dm_management
   @brief
   Get the user defined data from the data model

   @details
   Get the user defined data from the data model..

   @param dm A pointer to a data model object created with @ref datamodel_create

   @return
    - The user defined data.
 */
void* datamodel_userData(datamodel_t* dm) {
    if(!dm) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    return dm->userData;
}

/**
   @ingroup pcb_core_dm_management
   @brief
   Is this a connected datamodel

   @details
   This function will return true if this is a connected data model

   @param dm A pointer to a data model object created with @ref datamodel_create

   @return
    - true: The data model is part of a connection object
    - false: This is a local (private) data model.
 */
bool datamodel_isConnected(datamodel_t* dm) {
    if(!dm) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    pcb_error = pcb_ok;
    return (dm->attributes & datamodel_attr_connected) ? true : false;
}

/**
   @ingroup pcb_core_dm_management
   @brief
   Is this a cached datamodel

   @details
   This function will return true if this is a cached data model

   @param dm A pointer to a data model object created with @ref datamodel_create

   @return
    - true: The data model is part of a connection object, the cached data model
    - false: This is not a cached data model
 */
bool datamodel_isCache(datamodel_t* dm) {
    if(!dm) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    pcb_error = pcb_ok;
    return (dm->attributes & datamodel_attr_cache) ? true : false;
}

/**
   @ingroup pcb_core_dm_management
   @brief
   Enable/disable function auto resolving

   @details
   Use this function to turn on/off function auto resolving. Auto resolving will be used
   when loading data model definition or data. Depending on the serializer used, function will or can be
   resolved from within an so file.

   @param dm A pointer to a data model object created with @ref datamodel_create
   @param resolve true: auto resolving uned on, false turn of auto resolving

   @return
    - true
    -.false
 */
bool datamodel_autoResolve(datamodel_t* dm, bool resolve) {
    if(!dm) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(resolve) {
        dm->attributes &= ~datamodel_attr_no_resolve;
    } else {
        dm->attributes |= datamodel_attr_no_resolve;
    }

    return true;
}

/**
   @ingroup pcb_core_dm_management
   @brief
   Check that function auto resolving is turned on or off.

   @details
   This function will be used typically by serialzers to check that they need to try to resolve the functions mentioned
   in the data model definition file.

   @param dm A pointer to a data model object created with @ref datamodel_create

   @return
    - true: auto resolving is on
    -.false: auto resolving is off.
 */
bool datamodel_isAutoResolving(datamodel_t* dm) {
    if(!dm) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(dm->attributes & datamodel_attr_no_resolve) {
        return false;
    }

    return true;
}

/**
   @ingroup pcb_core_dm_management
   @brief
   Get the connection object of the data model

   @details
   This function will return the connection object of the data model if this data model is part of a connection object.
   If this is a local (private) data model the function will return NULL.

   @param dm A pointer to a data model object created with @ref datamodel_create

   @return
    - A pointer to a connection object.
    - NULL if this is not a connected data model.
 */
pcb_t* datamodel_pcb(datamodel_t* dm) {
    if(!dm) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    if((dm->attributes & datamodel_attr_connected) != datamodel_attr_connected) {
        pcb_error = pcb_error_datamodel_not_connected;
        return NULL;
    }

    pcb_error = pcb_ok;
    pcb_t* pcb = NULL;
    if(dm->attributes & datamodel_attr_cache) {
        pcb = llist_item_data(dm, pcb_t, cache);
    } else {
        pcb = llist_item_data(dm, pcb_t, datamodel);
    }
    return pcb;
}
