/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>

#include <debug/sahtrace.h>
#include <pcb/common/error.h>
#include <pcb/utils/variant_map.h>
#include "parameter_priv.h"
#include "object_priv.h"
#include "validator_priv.h"
#include "core_priv.h"


/**
   @file
   @brief
   Implementation of the private parameter functions
 */

/**
   @brief
   Destroy the specified parameter

   @details
   This function will cleanup and free the complete parameter datastructure.


   @param parameter The parameter we want to destroy

   @return This function always returns true
 */
bool parameter_destroy(parameter_t* parameter) {
    /* call the delete handler */
    if(parameter->definition->handlers && parameter->definition->handlers->destroy) {
        parameter->definition->handlers->destroy(parameter);
    }

    parameter_def_t* def = parameter->definition;
    bool free_parameter = false;

    llist_iterator_take(&parameter->it);
    if(parameter->map) {
        free(parameter->map->parameterName);
        variant_map_cleanup(&parameter->map->parameters);
        free(parameter->map);
    }

    variant_cleanup(&parameter->value[1]);
    variant_cleanup(&parameter->value[0]);
    aclDestroy(&parameter->ACL);

    if(parameter->countedObject != NULL) {
        parameter->countedObject->object_info.template_info->instanceCounter = NULL;
        parameter->countedObject = NULL;
    }

    free_parameter = (parameter != (((void*) def) + sizeof(parameter_def_t)));
    parameter->definition->refcount--;
    if(parameter->definition->refcount == 0) {
        free(parameter->definition->handlers);
        param_validator_destroy(parameter->definition->validator);
        if(parameter->definition->name != (char*) (((void*) parameter->definition) + sizeof(parameter_def_t) + sizeof(parameter_t))) {
            free(parameter->definition->name);
        }

#ifdef PCB_HELP_SUPPORT
        free(parameter->definition->description);
#endif
        free(parameter->definition);
    }

    if(free_parameter) {
        free(parameter);
    }
    return true;
}

/**
   @brief
   Convert the parameter type to a variant type

   @details
   This function converts a parameter type enumeration (@ref parameter_type_t) into a variant type enumeration (@ref variant_type_t)\n

   @param paramType The parameter type we want to translate

   @return The corresponding variant type, variant_type_unknown if a conversion is not possible.
 */
variant_type_t parameter_type_to_variant_type(parameter_type_t paramType) {
    switch(paramType) {
    case parameter_type_unknown:
        return variant_type_unknown;
        break;
    case parameter_type_string:
    case parameter_type_reference:
        return variant_type_string;
        break;
    case parameter_type_int8:
        return variant_type_int8;
        break;
    case parameter_type_int16:
        return variant_type_int16;
        break;
    case parameter_type_int32:
        return variant_type_int32;
        break;
    case parameter_type_int64:
        return variant_type_int64;
        break;
    case parameter_type_uint8:
        return variant_type_uint8;
        break;
    case parameter_type_uint16:
        return variant_type_uint16;
        break;
    case parameter_type_uint32:
        return variant_type_uint32;
        break;
    case parameter_type_uint64:
        return variant_type_uint64;
        break;
    case parameter_type_bool:
        return variant_type_bool;
        break;
    case parameter_type_date_time:
        return variant_type_date_time;
        break;
    default:
        return variant_type_unknown;
        break;
    }
}

/**
   @brief
   Get a pointer to the parameter handlers

   @details
   This function returns a pointer to the parameter handlers structure of this parameter

   @param parameter The parameter in which we are interested

   @return
    - The parameter handler function
    - NULL if an error occurred (out of memory)
 */

parameter_handlers_t* parameter_getHandlers(parameter_t* parameter) {
    if(!parameter->definition->handlers) {
        parameter->definition->handlers = (parameter_handlers_t*) calloc(1, sizeof(parameter_handlers_t));
        if(!parameter->definition->handlers) {
            pcb_error = pcb_error_no_memory;
            return NULL;
        }
    }

    return parameter->definition->handlers;
}

/**
   @}
 */
