/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <string.h>

#include <debug/sahtrace.h>

#include <pcb/utils.h>

#include <pcb/core.h>

#include "core_priv.h"
#include "parameter_priv.h"
#include "object_priv.h"
#include "function_priv.h"

static void parameter_renameModifier(parameter_t* param, rule_modifier_t* mod, translate_direction_t direction) {
    string_t clientName;
    string_t pluginName;
    string_initialize(&clientName, 0);
    string_initialize(&pluginName, 0);
    string_fromChar(&pluginName, parameter_name(param));
    mod->data.rename(direction, &clientName, &pluginName);
    parameter_rename(param, string_buffer(&clientName));
    string_cleanup(&clientName);
    string_cleanup(&pluginName);
}

static void parameter_translateModifier(parameter_t* param, rule_modifier_t* mod, translate_direction_t direction) {
    variant_map_t value;
    variant_map_initialize(&value);
    variant_map_add(&value, parameter_name(param), &param->value[param->currentConfig]);

    mod->data.translate(direction, &param->value[param->currentConfig], &value);

    variant_map_cleanup(&value);
}

static void parameter_setTypeModifier(parameter_t* param, rule_modifier_t* mod, translate_direction_t direction) {
    mod->data.cast(direction, param);
}

static void parameter_executeModifiers(parameter_t* param, mapping_rule_t* rule, translate_direction_t direction) {
    SAH_TRACEZ_IN("pcb_map");

    llist_iterator_t* it = NULL;
    rule_modifier_t* mod = NULL;
    llist_for_each(it, &rule->modifiers) {
        mod = llist_item_data(it, rule_modifier_t, it);
        switch(mod->type) {
        case modifier_translate_name:
            if(mod->data.rename) {
                SAH_TRACEZ_INFO("pcb_map", "Call modifier rename");
                parameter_renameModifier(param, mod, direction);
            }
            break;
        case modifier_translate_value:
            if(mod->data.translate) {
                SAH_TRACEZ_INFO("pcb_map", "Call modifier translate value");
                parameter_translateModifier(param, mod, direction);
            }
            break;
        case modifier_translate_type:
            if(mod->data.translate) {
                SAH_TRACEZ_INFO("pcb_map", "Call modifier cast");
                parameter_setTypeModifier(param, mod, direction);
            }
            break;
        default:
            break;
        }
    }

    SAH_TRACEZ_OUT("pcb_map");
}

static bool request_translateModifier(rule_modifier_t* mod, translate_direction_t direction, variant_t* new_value) {
    variant_map_t value;
    variant_map_initialize(&value);

    if(!mod->data.translate(direction, new_value, &value)) {
        SAH_TRACEZ_ERROR("pcb_map", "Value translation failed");
        variant_map_cleanup(&value);
        return false;
    }

    variant_map_iterator_t* first = variant_map_first(&value);
    if(first) {
        SAH_TRACEZ_INFO("pcb_map", "set value");
        variant_copy(new_value, variant_map_iterator_data(first));
    }
    variant_map_cleanup(&value);

    return true;
}

static bool request_executeModifiers(request_t* req, const char* name, const variant_t* value, mapping_rule_t* rule, translate_direction_t direction) {
    SAH_TRACEZ_IN("pcb_map");

    bool retval = true;

    string_t new_name;
    string_initialize(&new_name, 0);
    string_fromChar(&new_name, name);

    variant_t new_value;
    variant_initialize(&new_value, variant_type_unknown);
    variant_copy(&new_value, value);

    llist_iterator_t* it = NULL;
    rule_modifier_t* mod = NULL;
    llist_for_each(it, &rule->modifiers) {
        mod = llist_item_data(it, rule_modifier_t, it);
        switch(mod->type) {
        case modifier_translate_name:
            if(mod->data.rename) {
                SAH_TRACEZ_INFO("pcb_map", "Call modifier rename - Name = %s", string_buffer(&new_name));
                mod->data.rename(direction, &new_name, &new_name);
                SAH_TRACEZ_INFO("pcb_map", "Renamed to = %s", string_buffer(&new_name));
            }
            break;
        case modifier_translate_value:
            SAH_TRACEZ_INFO("pcb_map", "Translate value");
            if(mod->data.translate &&
               ((request_type(req) == request_type_set_object) || (request_type(req) == request_type_create_instance))) {
                SAH_TRACEZ_INFO("pcb_map", "Call modifier translate value");
                if(!request_translateModifier(mod, direction, &new_value)) {
                    retval = false;
                }
            }
            break;
        case modifier_translate_type:
        default:
            break;
        }
    }

    if(retval) {
        request_addParameter(req, string_buffer(&new_name), &new_value);
    }

    string_cleanup(&new_name);
    variant_cleanup(&new_value);

    SAH_TRACEZ_OUT("pcb_map");

    return retval;
}

/**
   @brief
   Check that the object has destinations..

   @details
   This function is only used for mappers and creating a view.

   An object can be mapped to another object from another data model provider.
   If the object has multiple destinations then the parameters must be mapped individually.
   Otherwise all the parameters from the destination are mapped.

   @param object pointer to an object

   @return
    - true: the object has destinations
 */
bool object_hasDestinations(object_t* object) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    return !llist_isEmpty(&object->destinations);
}

/**
   @brief
   Get the object's destination count..

   @details
   This function is only used for mappers and creating a view.

   An object can be mapped to another object from another data model provider.
   If the object has multiple destinations then the parameters must be mapped individually.
   Otherwise all the parameters from the destination are mapped.

   @param object pointer to an object

   @return
    - the number of destinations
 */
uint32_t object_destinationCount(object_t* object) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    return llist_size(&object->destinations);
}

/**
   @brief
   Map the object..

   @details
   This function is only used for mappers and creating a view.

   An object can be mapped to another object from another data model provider.
   An object can only be mapped to one destination.

   @param object pointer to an object
   @param uri destination uri.

   @return
    - pointer to object_destination_t structure
 */
object_destination_t* object_map(object_t* object, const char* uri, bool recursive) {
    parameter_t* parameter = NULL;
    function_t* function = NULL;
    if(!object || !uri) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    if(object_isRemote(object)) {
        pcb_error = pcb_error_remote_local_mismatch;
        return NULL;
    }

    if(object_hasDestinations(object)) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    object_destination_t* dest = (object_destination_t*) calloc(1, sizeof(object_destination_t));
    if(!dest) {
        SAH_TRACEZ_ERROR("pcb", "Could not allocate memory for destination structure");
        pcb_error = pcb_error_no_memory;
        return NULL;
    }

    dest->uri = uri_parse(uri);
    if(!dest->uri) {
        SAH_TRACEZ_ERROR("pcb", "Invalid URI specified");
        pcb_error = pcb_error_invalid_parameter;
        goto error_free;
    }

    parameter = object_firstParameter(object);
    while(parameter) {
        if(strcmp(parameter_name(parameter), "Alias") != 0) {
            parameter_destroy(parameter);
            parameter = object_firstParameter(object);
        } else {
            parameter = object_nextParameter(parameter);
        }
    }

    function = object_firstFunction(object);
    while(function) {
        function_delete(function);
        function = object_firstFunction(object);
    }

    if(recursive) {
        object_t* child = object_firstChild(object);
        while(child) {
            object_destroy_recursive(child);
            child = object_firstChild(object);
        }

        child = object_firstInstance(object);
        while(child) {
            object_destroy_recursive(child);
            child = object_firstInstance(object);
        }

        object->attributes |= object_attr_linked;
    } else {
        object->attributes |= object_attr_mapped;
    }

    llist_append(&object->destinations, &dest->it);

    return dest;

error_free:
    free(dest);
    return NULL;
}

/**
   @brief
   Add a destnation to the object.

   @details
   This function is only used for mappers and creating a view.

   An object can be mapped to another object from another data model provider.
   If the object has multiple destinations then the parameters must be mapped individually.
   Otherwise all the parameters from the destination are mapped.

   @param object pointer to an object
   @param uri the full uri to the destination includeng the path (must be in key notation and slash seperated)

   @return
    - pointer to the object_destination_t.
    - NULL if adding the destination failed
 */
object_destination_t* object_addDestination(object_t* object, const char* uri) {
    if(!object || !uri) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    if(object->attributes & object_attr_mapped) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    if(object_isRemote(object)) {
        pcb_error = pcb_error_remote_local_mismatch;
        return NULL;
    }

    object_destination_t* dest = (object_destination_t*) calloc(1, sizeof(object_destination_t));
    if(!dest) {
        SAH_TRACEZ_ERROR("pcb", "Could not allocate memory for destination structure");
        pcb_error = pcb_error_no_memory;
        return NULL;
    }

    dest->uri = uri_parse(uri);
    if(!dest->uri) {
        SAH_TRACEZ_ERROR("pcb", "Invalid URI specified");
        pcb_error = pcb_error_invalid_parameter;
        goto error_free;
    }

    llist_append(&object->destinations, &dest->it);

    return dest;

error_free:
    free(dest);
    return NULL;
}

/**
   @brief
   Get the first destination of an object.

   @details
   This function is only used for mappers and creating a view.

   An object can be mapped to another object from another data model provider.
   If the object has multiple destinations then the parameters must be mapped individually.
   Otherwise all the parameters from the destination are mapped.

   This function can be used to start iterating over the object's destinations

   @param object pointer to an object

   @return
    - pointer to the first object_destination_t.
    - NULL when no object_destiniation_t is available
 */
object_destination_t* object_firstDestination(object_t* object) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    if(object_isRemote(object)) {
        pcb_error = pcb_error_remote_local_mismatch;
        return NULL;
    }


    llist_iterator_t* it = llist_first(&object->destinations);
    if(it) {
        return llist_item_data(it, object_destination_t, it);
    }

    return NULL;
}

/**
   @brief
   Get the next destination of an object.

   @details
   This function is only used for mappers and creating a view.

   An object can be mapped to another object from another data model provider.
   If the object has multiple destinations then the parameters must be mapped individually.
   Otherwise all the parameters from the destination are mapped.

   This function can be used to iterate over the object's destinations

   @param object pointer to an object
   @param reference pointer to an object_destination_t used as a reference

   @return
    - pointer to the first object_destination_t.
    - NULL when no more object_destiniation_t is available
 */
object_destination_t* object_nextDestination(object_t* object, object_destination_t* reference) {
    if(!object || !reference) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    if(object_isRemote(object)) {
        pcb_error = pcb_error_remote_local_mismatch;
        return NULL;
    }

    if(reference->it.list != &object->destinations) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    llist_iterator_t* it = llist_iterator_next(&reference->it);
    if(it) {
        return llist_item_data(it, object_destination_t, it);
    }

    return NULL;
}

bool request_translate(request_t* orig_req, request_t* trans_req, object_destination_t* dest) {
    SAH_TRACEZ_IN("pcb_map");
    const char* dest_path = uri_getPath(destination_URI(dest));
    string_t path;
    string_t verify;
    string_list_t parts;
    bool retval = true;
    const char* part = NULL;
    bool errors = false;

    if(!dest_path || !(*dest_path)) {
        /* empty path or NULL not allowed */
        return false;
    }
    string_initialize(&verify, 0);
    string_initialize(&path, 0);
    string_list_initialize(&parts);

    tree_item_t* current_rules = &dest->rules;
    tree_item_t* new_rules = &dest->rules;
    mapping_rule_t* map_rule = NULL;

    string_fromChar(&path, request_path(trans_req));
    if(string_length(&path) >= strlen(dest_path)) {
        string_mid(&verify, &path, strlen(dest_path) + 1, UINT32_MAX);
        string_list_split(&parts, &verify, "/", strlist_keep_empty_parts, string_case_sensitive);
        string_list_iterator_t* p = NULL;
        string_list_for_each(p, &parts) {
            if((string_buffer(string_list_iterator_data(p))[0] == '%') || (string_buffer(string_list_iterator_data(p))[0] == '$')) {
                part = string_buffer(string_list_iterator_data(p)) + 1;
            } else {
                part = string_buffer(string_list_iterator_data(p));
            }
            SAH_TRACEZ_INFO("pcb_map", "Verify %s", part);

            tree_for_each_child(new_rules, current_rules) {
                map_rule = tree_item_data(new_rules, mapping_rule_t, rules);
                if(map_rule->type != rule_type_object) {
                    continue;
                }
                SAH_TRACEZ_INFO("pcb_map", "Found object rule");
                if(regexec(&map_rule->regexp, part, 0, NULL, 0) == REG_NOMATCH) {
                    continue;
                }
                SAH_TRACEZ_INFO("pcb_map", "Object rule matches");
                if(map_rule->action == rule_action_accept) {
                    SAH_TRACEZ_INFO("pcb_map", "Accept object, continue with next level");
                    current_rules = new_rules;
                    break;
                }
                if(map_rule->action == rule_action_drop) {
                    SAH_TRACEZ_INFO("pcb_map", "drop object, stop here");
                    reply_addError(request_reply(orig_req), pcb_error_not_found, error_string(pcb_error_not_found), request_path(orig_req));
                    retval = false;
                    break;
                }
            }

            if(!retval || (new_rules == NULL)) {
                break;
            }
        }

        if(retval) {
            if(new_rules == current_rules) {
                SAH_TRACEZ_INFO("pcb_map", "Verify parameters");
                /* verify parameter rules */
                parameter_iterator_t* param = request_firstParameter(orig_req);
                while(param) {
                    SAH_TRACEZ_INFO("pcb_map", "Checking parameter %s", request_parameterName(param));
                    tree_for_each_child(new_rules, current_rules) {
                        map_rule = tree_item_data(new_rules, mapping_rule_t, rules);
                        if(!((map_rule->type == rule_type_parameter) || (map_rule->type == rule_type_parameter_request))) {
                            continue;
                        }
                        if(regexec(&map_rule->regexp, request_parameterName(param), 0, NULL, 0) == REG_NOMATCH) {
                            continue;
                        }
                        SAH_TRACEZ_INFO("pcb_map", "Parameter rule matches");
                        if(map_rule->action == rule_action_accept) {
                            SAH_TRACEZ_INFO("pcb_map", "Accept parameter");
                            if(!request_executeModifiers(trans_req, request_parameterName(param), request_parameterValue(param), map_rule, translate_value_to_plugin)) {
                                reply_addError(request_reply(orig_req), pcb_error_invalid_value, error_string(pcb_error_invalid_value), request_parameterName(param));
                                errors = true;
                            }
                            break;
                        }
                        if(map_rule->action == rule_action_drop) {
                            SAH_TRACEZ_INFO("pcb_map", "Drop parameter");
                            reply_addError(request_reply(orig_req), pcb_error_not_found, error_string(pcb_error_not_found), request_parameterName(param));
                            errors = true;
                            break;
                        }
                    }
                    if(!new_rules) {
                        SAH_TRACEZ_INFO("pcb_map", "No rule found for parameter %s, accept", request_parameterName(param));
                        request_addParameter(trans_req, request_parameterName(param), request_parameterValue(param));
                    }
                    param = request_nextParameter(param);
                }
            } else {
                SAH_TRACEZ_INFO("pcb_map", "No rules found, accept all parameters");
                parameter_iterator_t* param = request_firstParameter(orig_req);
                while(param) {
                    request_addParameter(trans_req, request_parameterName(param), request_parameterValue(param));
                    param = request_nextParameter(param);
                }
            }
        }

    }

    if(retval && (request_type(trans_req) == request_type_set_object)) {
        SAH_TRACEZ_INFO("pcb_map", "Verify set request");
        if(request_parameterCount(trans_req) == 0) {
            SAH_TRACEZ_INFO("pcb_map", "No parameters available, fail request");
            retval = false;
        }
        if(errors) {
            SAH_TRACEZ_INFO("pcb_map", "Errors in reply, only validate parameters");
            request_setAttributes(trans_req, request_attributes(trans_req) | request_setObject_validate_only);
        }
    }
    if(retval && (request_type(trans_req) == request_type_create_instance)) {
        SAH_TRACEZ_INFO("pcb_map", "Verify add request");
        if(errors) {
            SAH_TRACEZ_INFO("pcb_map", "Errors in reply, only validate parameters");
            request_setAttributes(trans_req, request_attributes(trans_req) | request_setObject_validate_only);
        }
    }

    string_list_cleanup(&parts);
    string_cleanup(&verify);
    string_cleanup(&path);

    SAH_TRACEZ_OUT("pcb_map");
    return retval;

}

bool object_translate(object_t* object, object_destination_t* dest) {
    SAH_TRACEZ_IN("pcb_map");
    const char* dest_path = uri_getPath(destination_URI(dest));
    string_t path;
    string_t verify;
    string_list_t parts;
    bool retval = true;

    string_initialize(&path, 0);
    string_initialize(&verify, 0);
    string_list_initialize(&parts);

    tree_item_t* current_rules = &dest->rules;
    tree_item_t* new_rules = &dest->rules;
    mapping_rule_t* map_rule = NULL;

    object_path(object, &path, path_attr_key_notation);
    if(string_length(&path) >= strlen(dest_path)) {
        string_mid(&verify, &path, strlen(dest_path) + 1, UINT32_MAX);
        string_list_split(&parts, &verify, ".", strlist_keep_empty_parts, string_case_sensitive);
        string_list_iterator_t* part = NULL;
        string_list_for_each(part, &parts) {
            SAH_TRACEZ_INFO("pcb_map", "Verify object %s", string_buffer(string_list_iterator_data(part)));

            tree_for_each_child(new_rules, current_rules) {
                map_rule = tree_item_data(new_rules, mapping_rule_t, rules);
                if(map_rule->type != rule_type_object) {
                    continue;
                }
                SAH_TRACEZ_INFO("pcb_map", "    Found object rule");
                if(regexec(&map_rule->regexp, string_buffer(string_list_iterator_data(part)), 0, NULL, 0) == REG_NOMATCH) {
                    SAH_TRACEZ_INFO("pcb_map", "    Regexp not matching object name, search next object rule");
                    continue;
                }
                SAH_TRACEZ_INFO("pcb_map", "    Object rule matches");
                if(map_rule->action == rule_action_accept) {
                    SAH_TRACEZ_INFO("pcb_map", "    Accept object, continue with next level");
                    current_rules = new_rules;
                    break;
                }
                if(map_rule->action == rule_action_drop) {
                    SAH_TRACEZ_INFO("pcb_map", "    drop object, stop here");
                    retval = false;
                    break;
                }
            }

            if(!retval || (new_rules == NULL)) {
                break;
            }
        }

        if(retval && (new_rules == current_rules)) {
            SAH_TRACEZ_INFO("pcb_map", "Verify parameters");
            /* verify parameter rules */
            parameter_t* param = object_firstParameter(object);
            parameter_t* prefetch_param = object_nextParameter(param);
            while(param) {
                SAH_TRACEZ_INFO("pcb_map", "Checking parameter %s", parameter_name(param));
                tree_for_each_child(new_rules, current_rules) {
                    map_rule = tree_item_data(new_rules, mapping_rule_t, rules);
                    if(!((map_rule->type == rule_type_parameter) || (map_rule->type == rule_type_parameter_object))) {
                        continue; /* next rule */
                    }
                    if(regexec(&map_rule->regexp, parameter_name(param), 0, NULL, 0) == REG_NOMATCH) {
                        continue; /* next rule */
                    }
                    SAH_TRACEZ_INFO("pcb_map", "Parameter rule matches");
                    if(map_rule->action == rule_action_accept) {
                        SAH_TRACEZ_INFO("pcb_map", "Accept parameter");
                        parameter_executeModifiers(param, map_rule, translate_value_to_client);
                        break; /* next parameter */
                    }
                    if(map_rule->action == rule_action_drop) {
                        SAH_TRACEZ_INFO("pcb_map", "Drop parameter");
                        parameter_destroy(param);
                        break; /* next parameter */
                    }
                }
                param = prefetch_param;
                prefetch_param = object_nextParameter(param);
            }

            SAH_TRACEZ_INFO("pcb_map", "Verify functions");
            /* verify function rules */
            function_t* func = object_firstFunction(object);
            function_t* prefetch = object_nextFunction(func);
            while(func) {
                SAH_TRACEZ_INFO("pcb_map", "Checking function %s", function_name(func));
                tree_for_each_child(new_rules, current_rules) {
                    map_rule = tree_item_data(new_rules, mapping_rule_t, rules);
                    if(map_rule->type != rule_type_function) {
                        continue;
                    }
                    if(regexec(&map_rule->regexp, function_name(func), 0, NULL, 0) == REG_NOMATCH) {
                        continue;
                    }
                    SAH_TRACEZ_INFO("pcb_map", "Function rule matches");
                    if(map_rule->action == rule_action_accept) {
                        SAH_TRACEZ_INFO("pcb_map", "Accept function");
                        break;
                    }
                    if(map_rule->action == rule_action_drop) {
                        SAH_TRACEZ_INFO("pcb_map", "Drop function");
                        function_delete(func);
                        break;
                    }
                }
                func = prefetch;
                prefetch = object_nextFunction(func);
            }

            local_object_commit(object);
        }
    }

    string_list_cleanup(&parts);
    string_cleanup(&verify);
    string_cleanup(&path);

    SAH_TRACEZ_OUT("pcb_map");
    return retval;
}

/**
   @brief
   Return the parameter destination

   @details
   This function is only used for mappers and creating a view.

   The destination is where the real parameter is

   @param parameter pointer to a parameter

   @return
    - pointer to an object detiniation
    - NULL if it is not a mapped parameter or belongs to a remote object.
 */
object_destination_t* parameter_destination(parameter_t* parameter) {
    if(!parameter) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    if(object_isRemote(parameter_owner(parameter))) {
        pcb_error = pcb_error_remote_local_mismatch;
        return false;
    }

    if(parameter->map) {
        return parameter->map->destination;
    }

    return NULL;
}

/**
   @brief
   Adds another parameter for the destination

   @details
   This function is only used for mappers and creating a view.

   A parameter can be mapped from multiple parameters within the same destination. If the parameter is not
   mapped already, this function will fail.

   @param parameter pointer to a parameter
   @param destparam the destination parameter to add.
   @param value the initial value

   @return
    - true when the parameter is added
    - false when failed to add the new parameter.
 */
bool parameter_addMap(parameter_t* parameter, const char* destparam, variant_t* value) {
    if(!parameter || !destparam || !(*destparam)) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(object_isRemote(parameter_owner(parameter))) {
        pcb_error = pcb_error_remote_local_mismatch;
        return false;
    }

    if(!parameter->map || !parameter->map->destination) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    return variant_map_add(&parameter->map->parameters, destparam, value);
}

/**
   @brief
   Adds a translator function to the parameter

   @details
   This function is only used for mappers and creating a view.

   A parameter can be mapped from multiple parameters within the same destination. When mapping multiple
   parameters to one parameter a translator function is need to translate the two values into one.
   Also view or mapper can give back another value then the original value from the parameter.using a translator function.

   @param parameter pointer to a parameter
   @param translatefn the translation handler

   @return
    - true when the handler is set
    - false when failed to set the handler
 */
bool parameter_setTranslator(parameter_t* parameter, parameter_translate_value_handler_t translatefn) {
    if(!parameter) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(object_isRemote(parameter_owner(parameter))) {
        pcb_error = pcb_error_remote_local_mismatch;
        return false;
    }

    parameter_handlers_t* handlers = parameter_getHandlers(parameter);
    if(!handlers) {
        return false;
    }

    handlers->translate = translatefn;
    return true;
}

/**
   @brief
   Get the object owner of the destination.

   @details
   This function is only used for mappers and creating a view.

   This function will get the object that owns the destination

   @param dest pointer to an object_destination_t

   @return
    - pointer to the object.
    - NULL no object owns the destination or an error occurred
 */
object_t* destination_object(object_destination_t* dest) {
    if(!dest) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    if(dest->it.list == NULL) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    object_t* object = llist_item_data(dest->it.list, object_t, destinations);

    return object;
}

/**
   @brief
   Delete the destination.

   @details
   This function is only used for mappers and creating a view.

   This function can be used to delete and remove the destination

   @param dest pointer to an object_destination_t
 */
void destination_delete(object_destination_t* dest) {
    if(!dest) {
        return;
    }

    object_t* object = destination_object(dest);
    parameter_t* param = NULL;
    object_for_each_parameter(param, object) {
        if(param->map && (param->map->destination == dest)) {
            param->map->destination = NULL;
            free(param->map->parameterName);
            variant_map_cleanup(&param->map->parameters);
            free(param->map);
            param->map = NULL;
        }
    }

    function_t* func = NULL;
    object_for_each_function(func, object) {
        if(func->destination == dest) {
            func->destination = NULL;
        }
    }

    tree_item_t* item = tree_item_takeFirstChild(&dest->rules);
    mapping_rule_t* child_rule = NULL;
    while(item) {
        child_rule = tree_item_data(item, mapping_rule_t, rules);
        rule_delete(child_rule);
        item = tree_item_takeFirstChild(&dest->rules);
    }

    if(object) {
        object->attributes &= ~(object_attr_linked | object_attr_mapped);
    }

    llist_iterator_take(&dest->it);
    uri_destroy(dest->uri);
    free(dest);
}

/**
   @brief
   Add a parameter to the destination

   @details
   This function is only used for mappers and creating a view.

   This function can be used to add a mapped parameter to the destination

   @param dest pointer to an object_destination_t
   @param clientName name of the parameter seen by the client
   @param destName name of the parameter as provided by the plug-in

   @return
    - true successful
    - false failed to add the parameter
 */
bool destination_addParameter(object_destination_t* dest, const char* clientName, const char* destName) {
    if(!dest || !clientName || !(*clientName) || !destName || !(*destName)) {
        SAH_TRACEZ_ERROR("pcb", "Invalid parameter given (NULL)");
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(dest->it.list == NULL) {
        SAH_TRACEZ_ERROR("pcb", "Destination not in list");
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    object_t* object = llist_item_data(dest->it.list, object_t, destinations);
    parameter_t* parameter = object_getParameter(object, clientName);
    if(!parameter) {
        SAH_TRACEZ_ERROR("pcb", "Parameter not found %s", clientName);
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(parameter->map) {
        SAH_TRACEZ_ERROR("pcb", "Parameter %s already in a map (object = %s, uri path = %s)", clientName, object_name(parameter_owner(parameter), 0), uri_getPath(destination_URI(parameter_destination(parameter))));
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    parameter->map = (destination_parameter_t*) calloc(1, sizeof(destination_parameter_t));
    if(!parameter->map) {
        SAH_TRACEZ_ERROR("pcb", "Failed to allocate map");
        pcb_error = pcb_error_no_memory;
        return false;
    }

    parameter->map->parameterName = strdup(destName);
    if(!parameter->map->parameterName) {
        SAH_TRACEZ_ERROR("pcb", "Failed to allocate mapped name");
        free(parameter->map);
        pcb_error = pcb_error_no_memory;
        return false;
    }
    variant_map_initialize(&parameter->map->parameters);
    parameter->map->destination = dest;

    return true;
}

/**
   @brief
   Add a function to the destination

   @details
   This function is only used for mappers and creating a view.

   This function can be used to add a mapped function to the destination

   @param dest pointer to an object_destination_t
   @param clientName name of the function seen by the client
   @param destName name of the function as provided by the plug-in

   @return
    - true successful
    - false failed to add the function
 */
bool destination_addFunction(object_destination_t* dest, const char* clientName, const char* destName) {
    if(!dest || !clientName || !(*clientName)) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(dest->it.list == NULL) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    object_t* object = llist_item_data(dest->it.list, object_t, destinations);
    function_t* function = function_create(object, clientName, function_type_void, function_attr_default);
    if(!function) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(destName && *destName) {
        function->plugin_name = strdup(destName);
        if(!function->plugin_name) {
            function_delete(function);
            pcb_error = pcb_error_no_memory;
            return false;
        }
    }

    function->destination = dest;

    return true;
}

/**
   @brief
   Get the peer info of the destination

   @details
   This function is only used for mappers and creating a view.

   This function can be used to get the peer information of the destination

   @param dest pointer to an object_destination_t

   @return
    - pointer to the peer_info_t structure
 */
peer_info_t* destination_peer(object_destination_t* dest) {
    if(!dest) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    if(!dest->it.list) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    if(dest->peer) {
        if(!peer_isConnected(dest->peer)) {
            peer_reconnect(dest->peer);
        }
        return dest->peer;
    }

    object_t* object = llist_item_data(dest->it.list, object_t, destinations);

    pcb_t* pcb = datamodel_pcb(object_datamodel(object));
    connection_info_t* con = pcb_connection(pcb);

    const uri_t* uri = NULL;

    /* get destination peer using destionation URI */
    uri = destination_URI(dest);
    if(!uri) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    if(strcmp(uri_getHost(uri), "ipc") == 0) {
        dest->peer = connection_findIPC(con, uri_getPort(uri));
        if(!dest->peer) {
            dest->peer = connection_connectToIPC(con, uri_getPort(uri));
        }
    } else {
        dest->peer = connection_findTCP(con, uri_getHost(uri), uri_getPort(uri));
        if(!dest->peer) {
            dest->peer = connection_connectToTCP(con, uri_getHost(uri), uri_getPort(uri), 0);
        }
    }

    return dest->peer;
}

/**
   @brief
   Get the uri of the destination

   @details
   This function is only used for mappers and creating a view.

   This function can be used to get the uri information of the destination

   @param dest pointer to an object_destination_t

   @return
    - pointer to the uri_t structure
 */
const uri_t* destination_URI(object_destination_t* dest) {
    if(!dest) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    return dest->uri;
}

bool destination_setURI(object_destination_t* dest, const char* uri) {
    if(!dest) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    uri_t* new_uri = uri_parse(uri);
    if(!new_uri) {
        SAH_TRACEZ_ERROR("pcb", "Invalid URI specified");
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(dest->uri) {
        uri_destroy(dest->uri);
        dest->uri = NULL;
    }

    dest->uri = new_uri;
    return true;
}

mapping_rule_t* destination_createRule(object_destination_t* dest, rule_type_t type, rule_action_t action) {
    if(!dest) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    object_t* object = llist_item_data(dest->it.list, object_t, destinations);
    if(type == rule_type_object) {
        if(!(object_attributes(object) & (object_attr_mapped | object_attr_linked))) {
            pcb_error = pcb_error_invalid_parameter;
            return NULL;
        }
        if(object_attributes(object) & object_attr_mapped) {
            pcb_error = pcb_error_invalid_parameter;
            return NULL;
        }
    }
    mapping_rule_t* rule = (mapping_rule_t*) calloc(1, sizeof(mapping_rule_t));
    if(!rule) {
        pcb_error = pcb_error_no_memory;
        return NULL;
    }
    tree_item_initialize(&rule->rules);
    llist_initialize(&rule->modifiers);

    rule->type = type;
    rule->action = action;

    tree_item_appendChild(&dest->rules, &rule->rules);

    return rule;
}

mapping_rule_t* rule_createRule(mapping_rule_t* parent, rule_type_t type, rule_action_t action) {
    if(!parent) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    if(parent->type != rule_type_object) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    mapping_rule_t* rule = (mapping_rule_t*) calloc(1, sizeof(mapping_rule_t));
    if(!rule) {
        pcb_error = pcb_error_no_memory;
        return NULL;
    }
    tree_item_initialize(&rule->rules);
    llist_initialize(&rule->modifiers);

    rule->type = type;
    rule->action = action;

    tree_item_appendChild(&parent->rules, &rule->rules);

    return rule;
}

bool rule_setRegExp(mapping_rule_t* rule, const char* regexp) {
    if(!rule || !regexp || !(*regexp)) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    switch(rule->action) {
    case rule_action_accept:
    case rule_action_drop: {
        int err = regcomp(&rule->regexp, regexp, REG_EXTENDED);
        if(err != 0) {
            char buffer[512];
            regerror(err, &rule->regexp, buffer, 512);
            SAH_TRACEZ_ERROR("pcb", "regcomp returned error 0x%X (%s)", err, buffer);
            return false;
        }
    }
    break;
    default:
        return false;
        break;
    }

    return true;
}

bool rule_addModifierRename(mapping_rule_t* rule, parameter_translate_name_handler_t renameHandler) {
    if(!rule || !renameHandler) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    rule_modifier_t* mod = (rule_modifier_t*) calloc(1, sizeof(rule_modifier_t));
    if(!mod) {
        pcb_error = pcb_error_no_memory;
        return NULL;
    }

    mod->type = modifier_translate_name;
    mod->data.rename = renameHandler;

    llist_append(&rule->modifiers, &mod->it);

    return true;
}

bool rule_addModifierTranslate(mapping_rule_t* rule, parameter_translate_value_handler_t translateHandler) {
    if(!rule || !translateHandler) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    rule_modifier_t* mod = (rule_modifier_t*) calloc(1, sizeof(rule_modifier_t));
    if(!mod) {
        pcb_error = pcb_error_no_memory;
        return NULL;
    }

    mod->type = modifier_translate_value;
    mod->data.translate = translateHandler;

    llist_append(&rule->modifiers, &mod->it);

    return true;
}

bool rule_addModifierCast(mapping_rule_t* rule, parameter_translate_type_handler_t castHandler) {
    if(!rule || !castHandler) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    rule_modifier_t* mod = (rule_modifier_t*) calloc(1, sizeof(rule_modifier_t));
    if(!mod) {
        pcb_error = pcb_error_no_memory;
        return NULL;
    }

    mod->type = modifier_translate_type;
    mod->data.cast = castHandler;

    llist_append(&rule->modifiers, &mod->it);

    return true;
}

mapping_rule_t* rule_parent(mapping_rule_t* rule) {
    if(!rule) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    tree_item_t* parent = tree_item_parent(&rule->rules);
    if(!parent || !tree_item_parent(parent)) {
        return NULL;
    }
    return tree_item_data(parent, mapping_rule_t, rules);
}

void rule_delete(mapping_rule_t* rule) {
    if(!rule) {
        pcb_error = pcb_error_invalid_parameter;
        return;
    }

    tree_item_t* item = tree_item_takeFirstChild(&rule->rules);
    mapping_rule_t* child_rule = NULL;
    while(item) {
        child_rule = tree_item_data(item, mapping_rule_t, rules);
        rule_delete(child_rule);
        item = tree_item_takeFirstChild(&rule->rules);
    }

    switch(rule->action) {
    case rule_action_drop:
    case rule_action_accept:
        regfree(&rule->regexp);
        break;
    default:
        break;
    }

    llist_iterator_t* it = llist_takeFirst(&rule->modifiers);
    rule_modifier_t* mod = NULL;
    while(it) {
        mod = llist_item_data(it, rule_modifier_t, it);
        free(mod);
        it = llist_takeFirst(&rule->modifiers);
    }

    tree_item_takeChild(&rule->rules);
    free(rule);
}
