/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <string.h>

#include <debug/sahtrace.h>

#include <pcb/common/error.h>
#include <pcb/utils/connection.h>
#include <pcb/core/datamodel.h>
#include <pcb/core/pcb_main.h>
#include <pcb/core/reply.h>
#include <pcb/pcb_client.h>

#include "core_priv.h"
#include "object_priv.h"
#include "pcb_main_priv.h"

object_t* object_searchCache(object_t* object, string_t* fullpath, uint32_t pathAttr, bool* exactMatch) {
    pcb_t* pcb = datamodel_pcb(object_datamodel(object));
    if(!pcb) {
        pcb_error = pcb_error_not_connected;
        return NULL;
    }

    datamodel_t* cache = pcb_cache(pcb);
    object_t* cache_root = datamodel_root(cache);
    object_t* foundObject = NULL;
    object_t* child = NULL;
    string_t cache_path;
    string_t path;
    string_initialize(&cache_path, 0);
    string_initialize(&path, 0);
    object_for_each_child(child, cache_root) {
        SAH_TRACEZ_INFO("pcb_cache", "Child %s", object_name(child, pathAttr));
        object_path(child, &cache_path, pathAttr);
        SAH_TRACEZ_INFO("pcb_cache", "Compare: %s - %s", string_buffer(&cache_path), string_buffer(fullpath));
        if(strncmp(string_buffer(&cache_path), string_buffer(fullpath), string_length(&cache_path)) == 0) {
            if(string_length(fullpath) == string_length(&cache_path)) {
                foundObject = child;
                break;
            } else {
                string_mid(&path, fullpath, string_length(&cache_path), UINT32_MAX);
                foundObject = local_object_getObject(child, string_buffer(&path), pathAttr, exactMatch);
                if(foundObject) {
                    break;
                }
            }

        }
    }
    string_cleanup(&cache_path);
    string_cleanup(&path);

    return foundObject;
}

bool remote_object_delete(object_t* object) {
    if(!object_isInstance(object)) {
        pcb_error = pcb_error_not_instance;
        return false;
    }
    pcb_t* pcb = datamodel_pcb(object_datamodel(object));
    if(!pcb) {
        pcb_error = pcb_error_not_connected;
        return false;
    }

    remote_object_info_t* ri = (remote_object_info_t*) object->userData;
    peer_info_t* dest = connection_find(&pcb->connection, ri->fd);
    if(!dest) {
        pcb_error = pcb_error_not_connected;
        return false;
    }

    string_t path;
    string_initialize(&path, 0);
    object_path(object, &path, pcb->client_data.attributes);
    bool retval = pcb_client_delete_instance(dest, string_buffer(&path));
    string_cleanup(&path);

    if(retval) {
        pcb_cacheRemoveObject(pcb, object);
    }

    return retval;
}

object_t* remote_object_getObject(object_t* object, const char* path, const uint32_t pathAttr, bool* exactMatch, remote_object_info_t* ri) {
    object_t* foundObject = NULL;
    peer_info_t* dest = NULL;

    pcb_t* pcb = datamodel_pcb(object_datamodel(object));
    if(!pcb) {
        pcb_error = pcb_error_not_connected;
        return NULL;
    }

    if(object_parent(object) != NULL) {
        if(ri == NULL) {
            ri = (remote_object_info_t*) object->userData;
            if(!ri) {
                pcb_error = pcb_error_not_connected;
                return NULL;
            }
        }
        dest = connection_find(&pcb->connection, ri->fd);
        if(!dest) {
            pcb_error = pcb_error_not_connected;
            return NULL;
        }
    }

    string_t fullpath;
    string_initialize(&fullpath, 0);
    object_path(object, &fullpath, pathAttr);
    if(string_length(&fullpath)) {
        if(pathAttr & path_attr_slash_seperator) {
            string_appendChar(&fullpath, "/");
        } else {
            string_appendChar(&fullpath, ".");
        }
    }
    string_appendChar(&fullpath, path);

    foundObject = object_searchCache(object, &fullpath, pathAttr, exactMatch);

    if(foundObject) {
        string_cleanup(&fullpath);
        return foundObject;
    }

    if(dest) {
        uint32_t store_attrib = pcb->client_data.attributes;
        pcb->client_data.attributes = pathAttr;
        SAH_TRACEZ_INFO("pcb", "request path = %s", string_buffer(&fullpath));
        foundObject = pcb_client_get_object(dest, string_buffer(&fullpath));
        pcb->client_data.attributes = store_attrib;
    }

    string_cleanup(&fullpath);
    return foundObject;
}

bool remote_object_hasChildren(const object_t* object) {
    remote_object_info_t* ri = (remote_object_info_t*) object->userData;
    return (ri->childCount > 0);
}

uint32_t remote_object_childCount(const object_t* object) {
    remote_object_info_t* ri = (remote_object_info_t*) object->userData;
    return ri->childCount;
}

void remote_object_path(const object_t* object, string_t* path, const uint32_t pathAttributes) {
    remote_object_info_t* ri = (remote_object_info_t*) object->userData;
    string_clear(path);

    if(!ri) {
        return;
    }

    if(pathAttributes & path_attr_include_namespace) {
        const char* dm_namespace = datamodel_namespace(object_datamodel(object));
        string_appendFormat(path, "%s", dm_namespace);
    }

    if(pathAttributes & path_attr_key_notation) {
        if(!string_isEmpty(path) && !string_isEmpty(&ri->parentPathKey)) {
            string_appendChar(path, ".");
        }
        string_append(path, &ri->parentPathKey);
    } else {
        if(!string_isEmpty(path) && !string_isEmpty(&ri->parentPathIndex)) {
            string_appendChar(path, ".");
        }
        string_append(path, &ri->parentPathIndex);
    }

    if(pathAttributes & path_attr_slash_seperator) {
        string_replaceChar(path, ".", "/");
    }

    if(!(pathAttributes & path_attr_parent)) {
        if(!string_isEmpty(path)) {
            if(pathAttributes & path_attr_slash_seperator) {
                string_appendChar(path, "/");
            } else {
                string_appendChar(path, ".");
            }
        }

        if(pathAttributes & path_attr_key_notation) {
            string_append(path, &ri->key);
        } else {
            string_appendChar(path, object->oname);
        }
    }
}

object_t* remote_object_createInstance(object_t* object, uint32_t index, const char* key) {
    pcb_t* pcb = datamodel_pcb(object_datamodel(object));
    if(!pcb) {
        pcb_error = pcb_error_not_connected;
        return NULL;
    }

    remote_object_info_t* ri = (remote_object_info_t*) object->userData;
    peer_info_t* dest = connection_find(&pcb->connection, ri->fd);
    if(!dest) {
        pcb_error = pcb_error_not_connected;
        return NULL;
    }

    string_t path;
    string_initialize(&path, 0);
    object_path(object, &path, pcb->client_data.attributes);
    object_t* instance = pcb_client_add_instance(dest, string_buffer(&path), index, key, NULL);
    string_cleanup(&path);

    return instance;
}

bool remote_object_hasInstances(const object_t* object) {
    remote_object_info_t* ri = (remote_object_info_t*) object->userData;
    return (ri->instanceCount > 0);
}

uint32_t remote_object_instanceCount(const object_t* object) {
    remote_object_info_t* ri = (remote_object_info_t*) object->userData;
    return ri->instanceCount;
}

parameter_t* remote_object_getParameter(const object_t* object, const char* parameterName) {
    pcb_t* pcb = datamodel_pcb(object_datamodel(object));
    if(!pcb) {
        pcb_error = pcb_error_not_connected;
        return NULL;
    }

    remote_object_info_t* ri = (remote_object_info_t*) object->userData;
    peer_info_t* dest = connection_find(&pcb->connection, ri->fd);
    if(!dest) {
        pcb_error = pcb_error_not_connected;
        return NULL;
    }

    string_t path;
    string_initialize(&path, 0);
    object_path(object, &path, pcb->client_data.attributes);
    parameter_t* parameter = pcb_client_get_parameter(dest, string_buffer(&path), parameterName);
    string_cleanup(&path);

    return parameter;
}

bool remote_object_hasParameters(const object_t* object) {
    remote_object_info_t* ri = (remote_object_info_t*) object->userData;
    return (ri->parameterCount > 0);
}

uint32_t remote_object_parameterCount(const object_t* object) {
    remote_object_info_t* ri = (remote_object_info_t*) object->userData;
    return ri->parameterCount;
}

function_t* remote_object_getFunction(object_t* object, const char* functionName) {
    pcb_t* pcb = datamodel_pcb(object_datamodel(object));
    if(!pcb) {
        pcb_error = pcb_error_not_connected;
        return NULL;
    }

    remote_object_info_t* ri = (remote_object_info_t*) object->userData;
    peer_info_t* dest = connection_find(&pcb->connection, ri->fd);
    if(!dest) {
        pcb_error = pcb_error_not_connected;
        return NULL;
    }

    string_t path;
    string_initialize(&path, 0);
    object_path(object, &path, pcb->client_data.attributes);

    function_t* func = pcb_client_get_function(dest, string_buffer(&path), functionName);
    string_cleanup(&path);
    return func;
}

bool remote_object_hasFunctions(const object_t* object) {
    remote_object_info_t* ri = (remote_object_info_t*) object->userData;
    return (ri->functionCount > 0);
}

uint32_t remote_object_functionCount(const object_t* object) {
    remote_object_info_t* ri = (remote_object_info_t*) object->userData;
    return ri->functionCount;
}

