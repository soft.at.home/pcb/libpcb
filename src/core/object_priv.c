/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#define _GNU_SOURCE
#include <stdlib.h>
#include <string.h>
#include <fnmatch.h>

#include <debug/sahtrace.h>
#include <pcb/common/error.h>
#include <pcb/utils/connection.h>
#include <pcb/core/datamodel.h>
#include <pcb/core/pcb_main.h>
#include <pcb/core/mapping.h>

#include "core_priv.h"
#include "parameter_priv.h"
#include "validator_priv.h"
#include "object_priv.h"
#include "pcb_main_priv.h"
#include "function_priv.h"

/**
   @file
   @brief
   Implementation of private object functions
 */

/**
   @brief
   Mark the parent tree as modified

   @details
   This function marks the parent tree as modified. This function is called whenever some object is modified.
   This functon works recursive in the parent direction

   @param object Object parent of the object that has been modified
 */
void object_setParentTreeModified(object_t* object) {
    if(local_object_parent(object)) {
        object_setParentTreeModified(local_object_parent(object));
    }

    object->attributes |= object_attr_subtree_changed;
}

/**
   @brief
   Allocate an object structure

   @details
   Allocates memory to contain an object structure and initializes the object

   @param name name of the object
   @param attributes the attributes of the object

   @return
    - a pointer to an object structure
    - NULL
        - failed to allocate memory
        - failed to initialize
        - failed to set the object name
 */
object_t* object_allocate(const char* name, const uint32_t attributes) {
    object_t* object = (object_t*) calloc(1, sizeof(object_t));
    if(object == NULL) {
        SAH_TRACEZ_ERROR("pcb", "Failed to allocate memory for object");
        pcb_error = pcb_error_no_memory;
        goto error;
    }

    /* set the state to created */
    object->state = object_state_created;
    /* strip of the internal attributes (keep UPC attributes, too) */
    object->attributes = attributes & 0x780FF;

    /* initialize the object structure */
    if(!object_initialize(object)) {
        SAH_TRACEZ_ERROR("pcb", "Failed to initialize object");
        goto error_free;
    }

    /* set the name */
    free(object->oname);
    object->oname = strdup(name);
    if(!object->oname) {
        SAH_TRACEZ_ERROR("pcb", "Failed to set object name");
        goto error_cleanup;
    }

    return object;

error_cleanup:
    object_cleanup(object);
error_free:
    free(object);
error:
    return NULL;
}

/**
   @brief
   Destroy an object structure

   @details
   Free up memory allocated for an object

   @param object pointer to an object structure
 */
void object_destroy(object_t* object) {
    /* only delete the object itself, child instances and parameters are already deleted */

    /* remove from the instance list */
    if(object->attributes & object_attr_instance) {
        if(!object_isRemote(object)) {
            object_t* parent = object_parent(object);
            parent->object_info.template_info->instanceCount--;

            /* Set instance counter */
            if(parent->object_info.template_info->instanceCounter != NULL) {
                parameter_t* counter = parent->object_info.template_info->instanceCounter;
                variant_copy(&(counter->value[counter->currentConfig == 0 ? 1 : 0]), &(counter->value[counter->currentConfig]));
                variant_setUInt32(&(counter->value[counter->currentConfig]), parent->object_info.template_info->instanceCount);
            }
        }
        llist_iterator_take(&object->instance_it);
    }

    /* remove the object from another list (if the object is in a list) */
    llist_iterator_take(&object->it);

    /* remove from the object tree
       if it is not in the tree, the next line does not do anything
     */
    tree_item_takeChild(&object->tree_item);

    /* Check for instance counter */
    if(!object_isRemote(object)) {
        if(((object->attributes & object_attr_template) == object_attr_template) && (object->object_info.template_info->instanceCounter != NULL)) {
            object->object_info.template_info->instanceCounter->countedObject = NULL;
            object->object_info.template_info->instanceCounter = NULL;
        }
    }

    /* remove the parameters */
    parameter_t* param = object_firstParameter(object);
    while(param) {
        parameter_destroy(param);
        param = object_firstParameter(object);
    }

    /* remove the functions */
    function_t* func = object_firstFunction(object);
    while(func) {
        function_delete(func);
        func = object_firstFunction(object);
    }

    /* remove the destinations */
    object_destination_t* dest = object_firstDestination(object);
    while(dest) {
        destination_delete(dest);
        dest = object_firstDestination(object);
    }

    aclDestroy(&object->ACL);

    if(object_isRemote(object)) {
        remote_object_info_t* ri = (remote_object_info_t*) object->userData;
        if(ri->destroy) {
            ri->destroy(object, ri->destroy_data);
        }
        string_cleanup(&ri->key);
        string_cleanup(&ri->parentPathKey);
        string_cleanup(&ri->parentPathIndex);
        free(object->userData);
        object->userData = NULL;
    }

    object_cleanup(object);
    free(object);
}

void object_destroy_recursive(object_t* object) {
    object_t* child = object_firstChild(object);
    while(child) {
        SAH_TRACEZ_INFO("pcb", "Delete object %s", object_name(child, 0));
        object_destroy_recursive(child);
        child = object_firstChild(object);
    }

    child = object_firstInstance(object);
    while(child) {
        SAH_TRACEZ_INFO("pcb", "Delete object %s", object_name(child, 0));
        object_destroy_recursive(child);
        child = object_firstInstance(object);
    }

    object_destroy(object);
}

/**
   @brief
   Initialize an object

   @details
   Initializes all parts of an object structure

   @param object pointer to an object structure

   @return
    - true: initialization was successful
    - false: failed to initialize the object
 */
bool object_initialize(object_t* object) {
    object->userData = 0;

    tree_item_initialize(&object->tree_item);
    llist_initialize(&object->parameters);
    llist_iterator_initialize(&object->it);
    llist_iterator_initialize(&object->instance_it);
    llist_initialize(&object->notifyRequests);
    llist_initialize(&object->ACL);
    llist_initialize(&object->functions);
    llist_initialize(&object->destinations);
    llist_initialize(&object->mapRequests);
    string_list_initialize(&object->mibs);
    object->oname = NULL;

    if(object->attributes & object_attr_instance) {
        if(!string_initialize(&object->object_info.instance_info.key, 0)) {
            goto error;
        }
    } else {
        if(object->attributes & object_attr_template) {
            object->object_info.template_info = calloc(1, sizeof(template_info_t));
            if(!object->object_info.template_info) {
                goto error;
            }
            if(!object_template_initialize(object->object_info.template_info)) {
                goto error_free_template;
            }
        }
    }

    return true;

error_free_template:
    if(object->attributes & object_attr_template) {
        free(object->object_info.template_info);
    }
error:
    return false;
}

/**
   @brief
   Cleanup of the object structure

   @details
   Cleanup of the object parts

   @param object pointer to an object structure
 */
void object_cleanup(object_t* object) {
    object_validator_destroy(object->validator);

    /* call the delete handler */
    if(object->handlers && object->handlers->destroy) {
        object->handlers->destroy(object);
    }

    if(object->attributes & object_attr_template) {
        int i = 0;
        llist_cleanup(&object->object_info.template_info->instances);
        for(i = 0; i < deleted_instance_max; i++) {
            string_list_cleanup(&(object->object_info.template_info->deleted[i]));
        }
        free(object->object_info.template_info);
    }
    if((object->attributes & object_attr_instance)) {
        string_cleanup(&object->object_info.instance_info.key);
    }

    llist_cleanup(&object->mapRequests);
    llist_cleanup(&object->notifyRequests);
    llist_cleanup(&object->parameters);
    string_list_cleanup(&object->mibs);
    free(object->oname);
    object->oname = NULL;
    tree_item_cleanup(&object->tree_item);
    llist_iterator_take(&object->it);
    free(object->handlers);
    llist_iterator_t* it = llist_takeFirst(&object->ACL);
    ACL_t* acl = NULL;
    while(it) {
        acl = llist_item_data(it, ACL_t, it);
        free(acl);
        it = llist_takeFirst(&object->ACL);
    }
    object->handlers = NULL;
#ifdef PCB_HELP_SUPPORT
    free(object->description);
#endif
}

/**
   @brief
   Add a parameter to an object

   @details
   Add a parameter to an object

   @param object pointer to an object structure
   @param parameter the parameter that has to be added

   @return
    - true: parameter was added successfully
    - false:
        - parameter name is not unique within the object
 */
bool object_addParameter(object_t* object, parameter_t* parameter) {
    /* reset parameter attributes if this object is a single instance object
       following attributes are removed for single instance objects:
       parameter_attr_template_only : single instance objects can not have template parameters
     */
    if(!object_isTemplate(object) || object_isInstance(object)) {
        parameter->attributes &= ~(parameter_attr_template_only);
    }

    if(!object_isRemote(object)) {
        /* The parameter must have a unique name within the object */
        if(local_object_getParameter(object, parameter_name(parameter)) != NULL) {
            pcb_error = pcb_error_not_unique_name;
            return false;
        }
        object_setParentTreeModified(object);
        if((object->state != object_state_created) &&
           (object->state != object_state_deleted)) {
            object->state = object_state_modified;
        }
    }

    llist_append(&object->parameters, &parameter->it);

    return true;
}

bool object_addFunction(object_t* object, function_t* function) {
    /* The function must have a unique name within the object */
    if(local_object_getFunction(object, function_name(function)) != NULL) {
        pcb_error = pcb_error_not_unique_name;
        return false;
    }

    llist_append(&object->functions, &function->it);

    return true;
}

/**
   @brief
   Initialize a template object

   @details
   Initialize a template object

   @param template_info pointer to a template information structure

   @return
    - true: parameter was removed successfully
    - false:
        - failed to initialize the instance lists
 */
bool object_template_initialize(template_info_t* template_info) {
    int i = 0;

    llist_initialize(&template_info->instances);
    template_info->max_instances = UINT32_MAX;
    for(i = 0; i < deleted_instance_max; i++) {
        string_list_initialize(&(template_info->deleted[i]));
    }

    return true;
}

/**
   @brief
   Set the parent for an object

   @details
   Set the parent for an object

   @param object pointer to an object structure
   @param parent pointer to an object structure, that will become the parent of the specified object

   @return
    - true: parent was set
    - false:
        - the name of the object is not unique within the parent
 */
bool object_setParent(object_t* object, object_t* parent) {
    /* see if an object with the same name already exist in the parent object */
    const char* name = object_name(object, path_attr_default);
    object_t* temp = local_object_getObject(parent, name, path_attr_default, NULL);
    if(temp) {
        pcb_error = pcb_error_not_unique_name;
        return false;
    }

    tree_item_appendChild(&parent->tree_item, &object->tree_item);

    return true;
}

/**
   @brief
   Build a string from an object and optionally a parameter to do the wildcard matching

   @details
   This function builds a string from an object (Full path) and optionally a parameter (name and/or value).\n
   This string is used to do the wild card matching.\n
   The string has the following format: Path.To.An.Object.ParameterName=ParameterValue

   @param object_path String for the parameter's object path
   @param parameter pointer to a parameter, if NULL no parameter name or value are added to the string
   @param searchAttr See @ref search_attributes_t
   @param buffer String buffer where the string is build in.
 */
static void object_buildMatchString(string_t* object_path, parameter_t* parameter, const uint32_t searchAttr, string_t* buffer) {
    string_copy(buffer, object_path);

    if(parameter) {
        // Slash separator mandatory
        string_appendChar(buffer, "/");
        string_appendChar(buffer, parameter_name(parameter));
        if(searchAttr & search_attr_include_values) {
            string_t value;
            string_initialize(&value, 64);
            string_appendChar(buffer, "=");
            variant_toString(&value, parameter_getValue(parameter));
            string_append(buffer, &value);
            string_cleanup(&value);
        }
    }
}

static bool object_matches_checkInstance(exploded_path_t* opath, exploded_path_t* ppath, const uint32_t searchAttr, const uint32_t checked, const uint32_t to_check) {
    uint32_t first = 0, last = 0, i = 0;
    string_list_iterator_t* objectIter = NULL, * patternIter = NULL;
    object_t* object = NULL;

    if(!(searchAttr & search_attr_instance_wildcards_only)) {
        return true;
    }

    first = checked;
    last = (to_check == UINT32_MAX ? opath->path_size : checked + to_check) - 1;

    // Find the last-to-check element for both paths.
    // Due to different path_sizes, we need to search separately.
    for(object = opath->object, i = ((opath->path_size - 1) - last); i; object = object_parent(object), i--) { // NOTE the ';'
    }
    objectIter = string_list_at(&(opath->exploded_path), last);
    patternIter = string_list_at(&(ppath->exploded_path), last);

    // Iterate over path and pattern from last to first, whilst keeping the object in sync with the path
    for(
        i = last;
        i >= first && object && objectIter && patternIter;
        i--, object = object_parent(object), objectIter = string_list_iterator_prev(objectIter), patternIter = string_list_iterator_prev(patternIter)
        ) {
        if(string_compare(string_list_iterator_data(objectIter), string_list_iterator_data(patternIter), string_case_insensitive)) {
            // Elements differ, this is where globbing took place. The object must be an instance.
            if(!object_isInstance(object)) {
                SAH_TRACEZ_ERROR("pcb", "Object %s is not an instance", object_name(object, searchAttr & search_attr_key_notation));
                pcb_error = pcb_error_not_instance;
                return false;
            }
            // Continue to support multiple globs
        }
    }

    return true;
}

/**
   @brief
   Match an object with a pattern

   @details
   Check that a certain object is matching a pattern. From the object a string is build depending on the attributes given (See @ref object_buildMatchString)
   If the object matches the pattern this function will return true.\n
   The system function fnmatch is used to do the matching.
   Because the return value 'false' can mean both "no match" and "error occured",
   the caller must first set 'pcb_error' to 'pcb_ok' to be able to differentiate between the two.

   Leading dir path checks are possible if to_check is not UINT32_MAX. Otherwise, a full path check is performed.
   Parameter and value checks are only performed upon a full path check.

   @param opath Exploded path for object to match
   @param ppath Exploded path for pattern
   @param searchAttr See @ref search_attributes_t
   @param checked The depth already checked
   @param to_check The depth to check

   @return
    - true: the object matches the pattern
    - false: the object does not match the pattern (check pcb_error for any errors)
 */
bool object_matches(exploded_path_t* opath, exploded_path_t* ppath, const uint32_t searchAttr, const uint32_t checked, const uint32_t to_check) {
#if defined(FNM_EXTMATCH)
    int flags = FNM_CASEFOLD | FNM_PATHNAME | FNM_EXTMATCH;
#else
    int flags = FNM_CASEFOLD | FNM_PATHNAME;
#endif
    bool retval = false;
    uint32_t pathAttr = 0;
    string_t object_path, pattern_path;

    if(!opath || !ppath) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(to_check == UINT32_MAX) {
        if(opath->path_size != ppath->path_size) {
            pcb_error = pcb_error_invalid_parameter;
            return false;
        }
    } else {
        if((checked + to_check > opath->path_size) || (checked + to_check > ppath->path_size)) {
            pcb_error = pcb_error_invalid_parameter;
            return false;
        }
    }

    // Slash separator is mandatory for fnmatch() to function properly
    pathAttr = path_attr_slash_separator;
    if(to_check == UINT32_MAX) {
        pathAttr |= (searchAttr & search_attr_include_parameters) ? path_attr_include_parameter : 0;
        pathAttr |= (searchAttr & search_attr_include_values) ? path_attr_include_value : 0;
    }

    string_initialize(&object_path, 0);
    exploded_path_getPartialPath(opath, &object_path, pathAttr, checked, to_check);
    string_initialize(&pattern_path, 0);
    exploded_path_getPartialPath(ppath, &pattern_path, pathAttr, checked, to_check);

    if((to_check == UINT32_MAX) && (searchAttr & (search_attr_include_parameters | search_attr_include_values))) {
        string_t full_path;
        parameter_t* parameter = NULL;

        string_initialize(&full_path, 0);

        object_for_each_parameter(parameter, opath->object) {
            object_buildMatchString(&object_path, parameter, searchAttr, &full_path);
            if(fnmatch(string_buffer(&pattern_path), string_buffer(&full_path), flags) == 0) {
                retval = object_matches_checkInstance(opath, ppath, searchAttr, checked, to_check);
            }
        }

        string_cleanup(&full_path);
    } else {
        if(fnmatch(string_buffer(&pattern_path), string_buffer(&object_path), flags) == 0) {
            retval = object_matches_checkInstance(opath, ppath, searchAttr, checked, to_check);
        }
    }

    string_cleanup(&object_path);
    string_cleanup(&pattern_path);

    return retval;
}

/**
   @brief
   Get the object handlers.

   @details
   This function will return a pointer to the object handlers structure. If such a structure was not allocated before,
   it is allocated here.

   @param object pointer to an object

   @return
    - pointer to the object handlers
    - NULL: failed to allocate memory for this structure.
 */
object_handlers_t* object_getHandlers(object_t* object) {
    if(!object->handlers) {
        object->handlers = (object_handlers_t*) calloc(1, sizeof(object_handlers_t));
        if(!object->handlers) {
            pcb_error = pcb_error_no_memory;
            return NULL;
        }
    }

    return object->handlers;
}

/**
   @brief
   Check that there are pending notification requests for an object.

   @details
   When an object is newly created, it is possible that one or more notification requests are in the pending list.
   These requests are atteached to the object in this function.

   @param object pointer to an object
 */
void object_attachNotifyRequests(object_t* object) {
    datamodel_t* datamodel = object_datamodel(object);
    pcb_t* pcb = datamodel_pcb(datamodel);
    connection_info_t* connection = pcb_connection(pcb);

    if(!connection) {
        return;
    }

    llist_iterator_t* peer_it = NULL;
    llist_iterator_t* it = NULL;
    connection_t* con = NULL;
    request_t* req = NULL;
    llist_for_each(it, &connection->connections) {
        con = llist_item_data(it, connection_t, it);
        if(!peer_isCustomFd(&con->info) && peer_isConnected(&con->info)) {
            pcb_peer_data_t* pd = peer_getUserData(&con->info);
            if(pd) {
                llist_for_each(peer_it, &pd->notifyRequests) {
                    req = llist_item_data(peer_it, request_t, notify_it);
                    if(request_type(req) == request_type_get_object) {
                        object_t* req_object = local_object_getObject(object_root(object), request_path(req), request_attributes(req), NULL);
                        if(req_object == object) {
                            SAH_TRACEZ_INFO("pcb", "re-invoke request for object %s (request id = %d)", request_path(req), request_id(req));
                            pcb->reqhandlers->getObject(&con->info, datamodel, req);
                        }
                    }
                }
            }
        }
    }

}

void object_acl(const object_t* object, llist_t* ACL) {
    object_t* parent = NULL;
    if((object->attributes & object_attr_instance) == object_attr_instance) {
        parent = object_instanceOf(object);
    } else {
        parent = local_object_parent(object);
    }

    if(parent) {
        object_acl(parent, ACL);
    }

    if(!llist_isEmpty(&object->ACL)) {
        llist_iterator_t* it = NULL;
        ACL_t* acl = NULL;
        llist_for_each(it, &object->ACL) {
            acl = llist_item_data(it, ACL_t, it);
            SAH_TRACEZ_INFO("pcb_acl", "add acl for %s: flags = %d, uid = %d",
                            (object_name(object, path_attr_key_notation) ? object_name(object, path_attr_key_notation) : "<root>"),
                            acl->acl_flags, acl->id);
            aclSet(ACL, acl->id, acl->acl_flags);
        }
    }
}

/**
   @brief
   Get all valid notify requests for an object.

   @details
   This function will build a list with all notify requests currently available and valid for
   a certain object. This function will start from the object given and then continues with the parent.
   Each notify request that matches the depth will be considered valid.
 */
void object_buildNotifyList(object_t* object, notify_list_t** notifyRequests, uint32_t depth) {
    if(!object) {
        return;
    }

    if(!llist_isEmpty(&object->notifyRequests)) {
        llist_iterator_t* it = NULL;
        request_t* req = NULL;
        notify_list_t* elm = NULL;
        llist_for_each(it, &object->notifyRequests) {
            req = llist_item_data(it, request_t, it);

            object_t* req_object = local_object_getObject(object_root(object), request_path(req), request_attributes(req), NULL);
            if(req_object != object) {
                /* skip this one, the path is not empty */
                continue;
            }
            if(request_depth(req) >= depth) {
                elm = (notify_list_t*) calloc(1, sizeof(notify_list_t));
                if(!elm) {
                    SAH_TRACEZ_ERROR("pcb", "failed to allocate notify element");
                    continue;
                }
                elm->req = req;
                elm->notifyDepth = 0 - depth;
                elm->next = *notifyRequests;
                *notifyRequests = elm;
            }
        }
    }

    object_buildNotifyList(local_object_parent(object), notifyRequests, ++depth);
}

