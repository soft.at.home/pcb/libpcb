/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#define _GNU_SOURCE
#include <stdlib.h>
#include <dirent.h>
#include <dlfcn.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>

#include <components.h>
#include <debug/sahtrace.h>

#include <pcb/common/error.h>
#include <pcb/utils/linked_list.h>
#include <pcb/utils/peer.h>

#include "object_priv.h"
#include "request_priv.h"
#include "parameter_priv.h"
#include "function_priv.h"
#include "notification_priv.h"

#ifndef CONFIG_PCB_DEFAULT_PLUGIN_PATH
#define CONFIG_PCB_DEFAULT_PLUGIN_PATH "/usr/lib/pcb"
#endif

/**
   @file
   @brief
   Implementation of object functions

   @todo
 */

static int32_t initialized = 0;
static llist_t serializer_libraries;
static llist_t serializers;

typedef struct _serializer_library {
    void* handle;
    llist_iterator_t it;
} serializer_lib_t;

typedef struct _serializer_info {
    serialize_handlers_t* handlers;
    uint32_t format;
    llist_iterator_t it;
} serializer_info_t;

static object_t* serialization_objectRoot(object_t* object) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    object_t* parent = NULL;
    if(object->attributes & object_attr_instance) {
        parent = object_instanceOf(object);
    } else {
        parent = local_object_parent(object);
    }
    return parent ? serialization_objectRoot(parent) : object;
}

static object_t* serialization_objectGetObject(object_t* object, const char* path, const uint32_t pathAttr) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    if(!path || !(*path)) {
        return object;
    }

    object_t* cache_root = serialization_objectRoot(object);
    object_t* search = NULL;
    object_t* child = NULL;
    string_t cache_path;
    string_initialize(&cache_path, 0);
    uint32_t path_len = strlen(path);
    uint32_t cache_len = 0;
    object_for_each_child(child, cache_root) {
        object_path(child, &cache_path, pathAttr);
        cache_len = string_length(&cache_path);
        if(cache_len == path_len) {
            SAH_TRACEZ_INFO("pcb_ser", "path lengths ==");
            if(string_compareChar(&cache_path, path, string_case_sensitive) == 0) {
                string_cleanup(&cache_path);
                return child;
            }
        } else {
            if((cache_len < path_len) && cache_len) {
                SAH_TRACEZ_INFO("pcb_ser", "path lengths >");
                if(strncmp(string_buffer(&cache_path), path, cache_len) == 0) {
                    SAH_TRACEZ_INFO("pcb_ser", "searching subobject %s of %s", path, string_buffer(&cache_path));
                    search = local_object_getObject(child, path + string_length(&cache_path) + 1, 0, 0);
                    if(search) {
                        string_cleanup(&cache_path);
                        return search;
                    }
                }
            }
        }
    }
    string_cleanup(&cache_path);
    return local_object_getObject(object, path, pathAttr, NULL);
}

static void serialization_checkCache(object_t* object) {
    object_t* cache_root = serialization_objectRoot(object);
    remote_object_info_t* child_ri = NULL;
    string_t path;
    string_initialize(&path, 0);
    object_path(object, &path, 0);
    object_t* child = object_firstChild(cache_root);
    object_t* prefetch = object_nextSibling(child);
    while(child) {
        if(child == object) {
            child = prefetch;
            prefetch = object_nextSibling(child);
            continue;
        }
        child_ri = (remote_object_info_t*) child->userData;
        SAH_TRACEZ_INFO("pcb_ser", "Verify object %s.%s", string_buffer(&child_ri->parentPathIndex), object_name(child, 0));
        if(string_length(&path) == string_length(&child_ri->parentPathIndex)) {
            SAH_TRACEZ_INFO("pcb_ser", "Path length matches");
            if(strncmp(string_buffer(&path), string_buffer(&child_ri->parentPathIndex), string_length(&path)) == 0) {
                SAH_TRACEZ_INFO("pcb_ser", "path matches");
                if(object_isInstance(child)) {
                    if(!child->object_info.instance_info.instance_of) {
                        SAH_TRACEZ_INFO("pcb_ser", "Adding object to instance list");
                        tree_item_takeChild(&child->tree_item);
                        llist_append(&object->object_info.template_info->instances, &child->instance_it);
                        child->object_info.instance_info.instance_of = object;
                    }
                } else {
                    SAH_TRACEZ_INFO("pcb_ser", "Adding object to child list");
                    object_setParent(child, object);
                }
            }
        }
        child = prefetch;
        prefetch = object_nextSibling(child);
    }
    string_cleanup(&path);
}

static const char* serialization_getPluginPath(const char* pluginPath) {
    if(pluginPath && *pluginPath) {
        SAH_TRACEZ_INFO("pcb", "Loading plugins from %s", pluginPath);
        return pluginPath;
    }

    /* load serialization plugins from default location */
    const char* plugin_path_default = CONFIG_PCB_DEFAULT_PLUGIN_PATH;
    pluginPath = getenv("PCB_PLUGIN_PATH");
    if(!pluginPath || !(*pluginPath)) {
        pluginPath = plugin_path_default;
        SAH_TRACEZ_INFO("pcb", "env var \"PCB_PLUGIN_PATH\" not defined");
    }
    SAH_TRACEZ_INFO("pcb", "Loading plugins from %s", pluginPath);

    return pluginPath;
}

static const char* serialization_getSerializers(const char* sers) {
    if(sers && *sers) {
        SAH_TRACEZ_INFO("pcb", "Loading serializers: %s", sers);
        return sers;
    }

    const char* serializers_default = "libpcb_serialize_odl.so,libpcb_serialize_ddw.so";
    sers = getenv("PCB_SERIALIZERS");
    if(!sers || !(*sers)) {
        sers = serializers_default;
        SAH_TRACEZ_INFO("pcb", "env var \"PCB_SERIALIZERS\" not defined");
    }
    SAH_TRACEZ_INFO("pcb", "Loading serializers: %s", sers);

    return sers;
}

/**
   @ingroup pcb_core_serialization
   @{
 */

/**
   @brief
   Initialize serialization

   @details
   Initialize the serialization functionality. Every client, plugin or bus must call this function as
   oneof the first things it does. After inialization, serialization plugins can be loaded.

   @return
    - true: Initialization was successful
    - false
 */
bool serialization_initialize(void) {
    if(initialized) {
        initialized++;
        return true;
    }

    if(!llist_initialize(&serializer_libraries)) {
        return false;
    }

    if(llist_initialize(&serializers)) {
        initialized++;
        return true;
    }

    return false;
}

/**
   @brief
   Clean up serializers.

   @details
   One of the last things a PCB application should do is clean-up the serializers.
 */
void serialization_cleanup(void) {
    serializer_info_t* ser = NULL;
    llist_iterator_t* it;
    llist_for_each(it, &serializers) {
        ser = llist_item_data(it, serializer_info_t, it);
        if(ser->handlers && ser->handlers->cleanup) {
            ser->handlers->cleanup();
            ser->handlers->cleanup = NULL;
        }
    }
}

/**
   @brief
   Unregister serializers

   @details
   One of the last things a PCB application should do is unload all loaded serialization plugins and free up all alocated memory.
 */
void serialization_unregister(void) {
    SAH_TRACEZ_OUT("pcb");

    if(initialized > 0) {
        initialized--;
    }

    if(initialized == 0) {
        /* remove all function pointers */
        SAH_TRACEZ_INFO("pcb", "unregistering libraries");
        serializer_info_t* ser = NULL;
        llist_iterator_t* it = llist_takeFirst(&serializers);
        while(it) {
            ser = llist_item_data(it, serializer_info_t, it);
            if(ser->handlers && ser->handlers->unregister) {
                ser->handlers->unregister();
            }
            free(ser->handlers);
            free(ser);
            it = llist_takeFirst(&serializers);
        }

        SAH_TRACEZ_INFO("pcb", "unloading libraries");
        /* remove all libraries */
        serializer_lib_t* serlib = NULL;
        it = llist_takeFirst(&serializer_libraries);
        while(it) {
            serlib = llist_item_data(it, serializer_lib_t, it);
            dlclose(serlib->handle);
            free(serlib);
            it = llist_takeFirst(&serializer_libraries);
        }
    }

    SAH_TRACEZ_OUT("pcb");
}

/**
   @brief
   Load serialization plugins.

   @details
   This function will scan a directory for serialization plugins.
   A serialization plugin is a shared object library (.so file) that exports a function called pcb_register. The function itself will not be called
   here, only resolved.\n
   Even when the function returns true, it is possible that no serialization plugin was found.
   The list of wanted serializers can be given in the second argument of the function. If this is a NULL pointer all serializers found
   will be loaded, otherwhise only these mentioned in the list.

   @param pluginPath Path to a directory. Relative paths a re relative to the working directory of the application.
   @param serializers Comma seperated list of the serializers that may be loaded.

   @return
    - true
    - false
        - Failed to allocate memory
        - Failed to open the directory
 */
bool serialization_loadPlugins(const char* pluginPath, const char* sers) {
    DIR* dp;
    struct dirent* ep;
    void* handle = NULL;
    llist_iterator_t* it = NULL;
    pcb_register_func_t register_plugin = NULL;

    pluginPath = serialization_getPluginPath(pluginPath);
    sers = serialization_getSerializers(sers);

    dp = opendir(pluginPath);
    if(dp == NULL) {
        pcb_error = errno;
        SAH_TRACEZ_ERROR("pcb", "opendir failed %s", error_string(pcb_error));
        return false;
    }
    string_t filename;
    string_initialize(&filename, 0);
    for(ep = readdir(dp); ep; ep = readdir(dp)) {
        if(strstr(ep->d_name, ".so") == NULL) {
            continue;
        }

        if(strstr(sers, ep->d_name) == NULL) {
            continue;
        }

        string_fromChar(&filename, pluginPath);
        string_appendChar(&filename, "/");
        string_appendChar(&filename, ep->d_name);
        handle = dlopen(string_buffer(&filename), RTLD_NOW);
        if(!handle) {
            pcb_error = pcb_error_system_error;
            SAH_TRACE_WARNING("dlopen failed %s", dlerror());
            string_clear(&filename);
            continue;
        }

        /* dlopen will return the same handle if we try to open the same lib again
           but still we need to close the handle, to decrease the reference count
           check if the handle is already available in the list of loaded libraries
         */
        serializer_lib_t* serlib = NULL;
        bool loaded = false;
        llist_for_each(it, &serializer_libraries) {
            serlib = llist_item_data(it, serializer_lib_t, it);
            if(serlib->handle == handle) {
                SAH_TRACEZ_NOTICE("pcb", "plugin %s was already loaded", string_buffer(&filename));
                dlclose(handle);
                loaded = true;
                break;
            }
        }
        if(loaded) {
            continue;
        }

        char* symbol = NULL;
        strtok(ep->d_name, ".");
        if(asprintf(&symbol, "%s_register", ep->d_name) == -1) {
            SAH_TRACEZ_NOTICE("pcb", "Failed to allocate memory for symbol");
        }
        register_plugin = dlsym(handle, symbol);
        free(symbol);
        if(register_plugin == NULL) {
            string_clear(&filename);
            pcb_error = pcb_error_system_error;
            /* always call dlerror, otherwise the next dlsym will fail also */
            const char* error = dlerror();
            if(error) {
                SAH_TRACEZ_WARNING("pcb", "dlsym failed %s", error);
            }
            dlclose(handle);
            continue;
        }

        serlib = (serializer_lib_t*) calloc(1, sizeof(serializer_lib_t));
        if(!serlib) {
            string_clear(&filename);
            pcb_error = pcb_error_no_memory;
            SAH_TRACEZ_ERROR("pcb", "%s", error_string(pcb_error));
            dlclose(handle);
            closedir(dp);
            return false;
        }
        serlib->handle = handle;
        llist_append(&serializer_libraries, &serlib->it);
        /* call the register function */
        register_plugin();

        string_clear(&filename);
    }
    closedir(dp);
    string_cleanup(&filename);

    pcb_error = pcb_ok;
    return true;
}

bool serialization_loadPluginsStatic(pcb_register_func_t serfn[]) {
    int i = 0;
    for(i = 0; serfn[i]; i++) {
        serfn[i]();
    }
    return true;
}

/**
   @brief
   Add a serialization format

   @details
   This function will be called by a serialization plugin when it wants to add a serialization format.

   @param serializerHandlers A pointer to a struct containing all the serialization functions, ownership of the pointer is taken
   @param format the format identifier

   @return
    - true: added the format to the list of supported format
    - false: Failed to add to the list
 */
bool serialization_addSerializer(serialize_handlers_t* serializerHandlers, uint32_t format) {
    if(!serializerHandlers) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    serializer_info_t* ser = (serializer_info_t*) calloc(1, sizeof(serializer_info_t));
    if(!ser) {
        pcb_error = pcb_error_no_memory;
        return false;
    }

    ser->handlers = serializerHandlers;
    ser->format = format;
    return llist_append(&serializers, &ser->it);
}

/**
   @brief
   Get the serialization handlers.

   @details
   Using a format identifier, a loaded serialization plugin is searched supporting that format.
   If such a serialization plugin is found, a serialize_handlers_t structure is filled with all the
   serialization functions, by calling the exported function "pcb_register".\n
   A format identifier can be build using the @ref SERIALIZE_FORMAT macro\n
   The plugins are searched in order they where loaded, the first matching serialization plugin is used.

   @param format the format identifier

   @return
    - A pointer to a struct containing all the serializer functions. Never free this pointer
    - NULL if the format does not exist.
 */
serialize_handlers_t* serialization_getHandlers(uint32_t format) {
    serializer_info_t* ser = NULL;
    llist_iterator_t* it = NULL;

    llist_for_each(it, &serializers) {
        ser = llist_item_data(it, serializer_info_t, it);
        if(ser->format == format) {
            return ser->handlers;
        }
    }

    SAH_TRACEZ_ERROR("pcb_ser", "No serializer found for format 0x%8.8X", format);
    pcb_error = pcb_error_no_serializer;

    return NULL;
}

/**
   @brief
   Get the serialization handlers.

   @details
   Using a socket which has data ready to receive, call each (de)serialization plugin, check format function.\n
   This function returns true, if that plugin can handle the packet format received on the socket.\n

   The plugins are searched in order they where loaded, the first matching serialization plugin is used.

   @param peer Pointer to a peer_info_t structure.

   @return
    - A pointer to a struct containing all the serializer functions. Never free this pointer
    - NULL if the format does not exist.
 */
serialize_handlers_t* serialization_getHandlersForSocket(peer_info_t* peer) {
    serializer_info_t* ser = NULL;
    llist_iterator_t* it = NULL;

    llist_for_each(it, &serializers) {
        ser = llist_item_data(it, serializer_info_t, it);
        if(ser->handlers && ser->handlers->deserialize_checkFormat) {
            if(ser->handlers->deserialize_checkFormat(peer)) {
                return ser->handlers;
            }
        }
    }

    SAH_TRACEZ_ERROR("pcb_ser", "No serializer found");
    pcb_error = pcb_error_no_serializer;

    return NULL;
}

/**
   @brief
   Create a remote object.

   @details
   During deserialization use this function to create a remote object.\n
   Because not all information (like the parent object) is available at that time, extra information is needed to
   give a good representation of the original object. The object created with this function is a mirror object of the original.
   It is possible that not the complete object is transfered (not all parameters).\n
   If the object was already available in the cache, the object is not created but updated if needed, the object is also taken out of
   the cache and can be put back in by calling @ref serialization_pushObjectToCache. This is done to make sure that the
   cached object is not used while it is updated.

   @param peer The peer on which the object was recieved
   @param parent Mostly a pointer to the root object of the object cache (cached datamodel)
   @param parentPathIndex Full path in index notation of the object
   @param name the object name (also in index notation, so this can be numeric)
   @param parentPathKey Full path in key notation of the object
   @param key The key name of the object (for instances this can be different from the real name, which is the index)
   @param attributes The object's attributes
   @param state the objects state

   @return
    - pointer to an object
    - NULL an error has occured
 */
object_t* serialization_objectCreate(peer_info_t* peer, object_t* parent, const char* parentPathIndex, const char* name, const char* parentPathKey, const char* key, const uint32_t attributes, object_state_t state, request_t* req) {
    if(!parent || !name) {
        pcb_error = pcb_error_invalid_parameter;
        SAH_TRACEZ_ERROR("pcb_ser", "parent or name are not set");
        return NULL;
    }

    if(!*name) {
        SAH_TRACEZ_NOTICE("pcb_ser", "Serialization creating root object, skip");
        return parent;
    }

    object_t* object = NULL;
    if(!(request_attributes(req) & request_no_object_caching) ||
       (request_attributes(req) & request_getObject_keep_hierarchy) ||
       !req->replyHandler) {
        object = serialization_objectGetObject(parent, parentPathIndex, path_attr_default);
        if(object) {
            parent = object;
            if(*name) {
                object = local_object_getObject(parent, name, path_attr_default, NULL);
            }
        } else {
            string_t fullpath;
            string_initialize(&fullpath, 64);
            string_appendChar(&fullpath, parentPathIndex);
            string_appendChar(&fullpath, ".");
            string_appendChar(&fullpath, name);
            object = serialization_objectGetObject(parent, string_buffer(&fullpath), path_attr_default);
            string_cleanup(&fullpath);
        }
    }
    remote_object_info_t* remote_info = NULL;
    if(!object) {
        SAH_TRACEZ_INFO("pcb_ser", "Object not found in cache, create new entry");
        object = object_allocate(name, attributes);
        if(!object) {
            SAH_TRACEZ_ERROR("pcb_ser", "failed to allocate object %d %s", pcb_error, error_string(pcb_error));
            goto error;
        }
        if(attributes & object_attr_instance) {
            object->attributes |= object_attr_instance;
        }
        if(request_attributes(req) & request_no_object_caching) {
            object->attributes |= object_attr_not_cached;
        }
        remote_info = (remote_object_info_t*) calloc(1, sizeof(remote_object_info_t));
        if(!remote_info) {
            SAH_TRACEZ_ERROR("pcb_ser", "failed to allocate remote info %d %s", pcb_error, error_string(pcb_error));
            goto error_cleanup;
        }

        object->userData = remote_info;
        object->attributes |= object_attr_remote;
    } else {
        remote_info = object->userData;
    }

    /*
       When adding the code below, the linked lists will be modified,
       if a client was iterating the instances or children, this will be broken by the code below
       WARNING: when the cache is cleared while recieving objects, a crash will occur
                this has to be solved (when clearing cache, check for outstanding requests?)
       if (object_isInstance(object)) {
        object->object_info.instance_info.instance_of = NULL;
       }
       tree_item_takeChild(&object->tree_item);
     */

    remote_info->fd = peer->socketfd;
    string_fromChar(&remote_info->key, key);
    string_fromChar(&remote_info->parentPathIndex, parentPathIndex);
    string_fromChar(&remote_info->parentPathKey, parentPathKey);

    object->state = state;
    if((object->state == object_state_modified) ||
       (object->state == object_state_validated)) {
        object->state = object_state_ready;
    }

    return object;

error_cleanup:
    object_destroy(object);
    return NULL;
error:
    return NULL;
}

/**
   @brief
   Delete a remote object.

   @details
   During deserialization use this function to delete an object that is being recreated.\n
   Wehn a socket is closed or the original request is destroyed and the serializer was recreating an object
   it can call this function to free up all resources already allocated for the object.

   @param object Pointer to a remote object
 */
void serialization_objectDelete(object_t* object) {
    if(!object || !object_isRemote(object)) {
        return;
    }

    object_destroy(object);
}

/**
   @brief
   Set a destroy handler on the object being recreated.

   @details
   As soon as the recreation of an object begins the serializer needs to set the destroy handler on the object.
   There is no garantee that an object is recreated all in one go, so if the object was laready in the object cache
   it is possible that the user of that object cache decides to clean up the cache. With this function the
   serializer will be notified that the object is destroyed and recreation of the object can stop.

   @param object Pointer to a remote object
   @param handler Pointer to the handler function
   @param userdata Serializers specific data
 */
void serialization_setObjectDestroyHandler(object_t* object, serialization_object_destroy_handler_t handler, void* userdata) {
    if(!object || !object_isRemote(object)) {
        return;
    }

    remote_object_info_t* remote_info = object->userData;
    if(!remote_info) {
        return;
    }

    remote_info->destroy = handler;
    remote_info->destroy_data = userdata;
}

/**
   @brief
   Add a remote object to the cache

   @details
   Add a remote object to the object cache.\n
   Whenever possible the object hierarchy is taken into account while adding the object to the cache.\n
   Adding an object to the cache should only be done when deserialization of the object is done completly.\n

   @param parent Mostly a pointer to the root object of the object cache (cached datamodel)
   @param object the remote object

   @return
    - true: the operation was successful.
    - false an error has occured
 */
bool serialization_pushObjectToCache(object_t* parent, object_t* object, request_t* req) {
    if(!parent || !object) {
        pcb_error = pcb_error_invalid_parameter;
        SAH_TRACEZ_ERROR("pcb_ser", "parent or object are not set");
        return false;
    }

    if(!object_isRemote(object)) {
        pcb_error = pcb_error_remote_local_mismatch;
        SAH_TRACEZ_NOTICE("pcb_ser", "object is not a remote object");
        return false;
    }

    if(!(request_attributes(req) & request_no_object_caching) ||
       (request_attributes(req) & request_getObject_keep_hierarchy) ||
       !req->replyHandler) {
        object_t* cache_root = serialization_objectRoot(parent);
        remote_object_info_t* remote_info = object->userData;

        SAH_TRACEZ_INFO("pcb_ser", "adding object to cache: parent = [%s] , key = %s", string_buffer(&remote_info->parentPathKey), string_buffer(&remote_info->key));
        object_t* real_parent = serialization_objectGetObject(parent, string_buffer(&remote_info->parentPathIndex), path_attr_default);
        if(real_parent) {
            parent = real_parent;
        } else {
            SAH_TRACEZ_INFO("pcb_ser", "Parent is not available in cache");
        }


        if(parent == cache_root) {
            SAH_TRACEZ_INFO("pcb_ser", "Adding object to cache root");
            tree_item_appendChild(&parent->tree_item, &object->tree_item);
        } else {
            if(object_isInstance(object)) {
                if(!object->object_info.instance_info.instance_of) {

                    if(!parent->object_info.template_info) {
                        pcb_error = pcb_error_not_template;
                        SAH_TRACEZ_NOTICE("pcb_ser", "template info is missing in the parent object");
                        return false;
                    }
                    SAH_TRACEZ_INFO("pcb_ser", "Adding object to instance list");
                    llist_append(&parent->object_info.template_info->instances, &object->instance_it);
                    object->object_info.instance_info.instance_of = parent;
                }
            } else {
                if(!tree_item_parent(&object->tree_item)) {
                    SAH_TRACEZ_INFO("pcb_ser", "Adding object to child list");
                    object_setParent(object, parent);
                }
            }
        }

        serialization_checkCache(object);
    } else {
        object_t* cache_root = serialization_objectRoot(parent);
        SAH_TRACEZ_INFO("pcb_ser", "Adding object to cache root");
        tree_item_appendChild(&cache_root->tree_item, &object->tree_item);
    }
    return true;
}

/**
   @brief
   Updates the object cache using a notification

   @details
   Updates the object cache using a notification. Currently only using object add and object delete notifications.
   If the object is not in the cache nothing happens.

   @param parent An object from the cache (use the root object)
   @param notification The recieved notification

   @return
    - Applied the notification
    - An error has occured
 */
bool serialization_updateCache(object_t* parent, notification_t* notification) {
    if(!parent && !notification) {
        pcb_error = pcb_error_invalid_parameter;
        SAH_TRACEZ_ERROR("pcb_ser", "parent or object are not set");
        return false;
    }

    object_t* cache_root = serialization_objectRoot(parent);

    object_t* object = serialization_objectGetObject(cache_root, notification_objectPath(notification), path_attr_default);
    if((object != cache_root) && object) {
        switch(notification_type(notification)) {
        case notify_object_deleted:
            object->state = object_state_deleted;
            break;
        case notify_object_added:
            object->state = object_state_created;
            break;
        case notify_value_changed: {
            char* name = notification_parameter_value(notification_getParameter(notification, "parameter"));
            parameter_t* parameter = local_object_getParameter(object, name);
            if(parameter) {
                SAH_TRACEZ_INFO("pcb_ser", "Updating parameter value in cache %s", name);
                serialization_parameterSetValue(parameter, notification_parameter_variant(notification_getParameter(notification, "newvalue")));
            }
            free(name);
        }
        break;
        default:
            /* do nothing */
            break;
        }
    }

    return true;
}

/**
   @brief
   Create a parameter for a remote object

   @details
   Add a parameter to a remote object. Dureing deserialization call this function to create a parameter
   for a cached object. The parameter created is a mirrored parameter from the original object.

   @param object the remote object
   @param name the name of the parameter
   @param type the type of the parameter
   @param attributes the partameter attributes
   @param state the parameter state.

   @return
    - pointer to a parameter
    - NULL an error has occured
 */
parameter_t* serialization_parameterCreate(object_t* object, const char* name, parameter_type_t type, const uint32_t attributes, const parameter_state_t state) {
    if(!object || !name || !(*name)) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    object_state_t prev_state = object->state;
    object->state = object_state_created;

    parameter_t* param = NULL;
    param = local_object_getParameter(object, name);

    if(!param) {
        param = parameter_create(object, name, type, attributes);
    }

    param->state = state;
    if((param->state == parameter_state_modified) ||
       (param->state == parameter_state_validated)) {
        param->state = parameter_state_ready;
    }

    object->state = prev_state;

    return param;
}

/**
   @brief
   Set the parameter's value

   @details
   During deserialization use this function to set the value of the parameter. This function will not modifiy the parameters state.

   @param parameter pointer to the parameter
   @param var pointer to a variant containing the value

   @return
    - true: the value was set
    - false an error has occured
 */
bool serialization_parameterSetValue(parameter_t* parameter, const variant_t* var) {
    if(!parameter || !var) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    variant_t* dest = &parameter->value[parameter->currentConfig];

    return variant_convert(dest, var, parameter_type_to_variant_type(parameter_type(parameter)));
}

/**
   @brief
   Recreate a function definition

   @details
   During deserialization use this function to create function definition for an object.

   @param object pointer to the remote object
   @param name name of the function
   @param type the function return type
   @param typeName the function type name (only used for custom types)
   @param attributes the function attributes

   @return
    - pointer to a function definition
    - null an error occured
 */
function_t* serialization_functionCreate(object_t* object, const char* name, function_type_t type, const char* typeName, const uint32_t attributes) {
    if(!object || !name || !(*name)) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    if(!object_isRemote(object)) {
        pcb_error = pcb_error_remote_local_mismatch;
        SAH_TRACEZ_ERROR("pcb_ser", "object is not a remote object");
        return false;
    }

    function_t* func = local_object_getFunction(object, name);
    if(!func) {
        if(type == function_type_custom) {
            func = function_createCustomType(object, name, typeName, attributes);
        } else {
            func = function_create(object, name, type, attributes);
        }
    }

    return func;
}

/**
   @brief
   Recreate a function argument definition

   @details
   During deserialization use this function to create function argument definition for a function

   @param function pointer to the function definition
   @param name name of the argument
   @param type the argument type
   @param typeName the argument type name (only used for custom types)
   @param attributes the argument attributes

   @return
    - pointer to a function argument definition
    - null an error occured
 */
function_argument_t* serialization_argumentCreate(function_t* function, const char* name, argument_type_t type, const char* typeName, const uint32_t attributes) {
    if(!function || !name || !(*name)) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    function_argument_t* arg = function_getArgument(function, name);
    if(!arg) {
        if(type == argument_type_custom) {
            arg = argument_createCustomType(function, name, typeName, attributes);
        } else {
            arg = argument_create(function, name, type, attributes);
        }
    }

    return arg;
}

/**
   @brief
   Set a remote object's parameter count.

   @details
   Cached objects do not need to be transferred completly. It is possible that none or some of the parameters where
   requested. But still the client wants to know how many parameters are available in the object.\n
   Instead of transferring all the parameters including there values, only the parameter count can be transferred.\n
   Use this function to update the parameter count of a remote object in the cache.

   @param object the remote object
   @param paramCount number of parameters the object has

   @return
    - true: the value was set
    - false an error has occured
 */
bool serialization_objectSetParameterCount(object_t* object, uint32_t paramCount) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(!object_isRemote(object)) {
        pcb_error = pcb_error_remote_local_mismatch;
        return false;
    }

    remote_object_info_t* ri = (remote_object_info_t*) object->userData;
    ri->parameterCount = paramCount;

    return true;
}

/**
   @brief
   Set a remote object's child count.

   @details
   Cached objects do not need to be transferred completly. It is possible that none or some of the children where
   requested. But still the client wants to know how many children are available in the object.\n
   Instead of transferring all the children, only the child count can be transferred.\n
   Use this function to update the child count of a remote object in the cache.

   @param object the remote object
   @param childCount number of children the object has

   @return
    - true: the value was set
    - false an error has occured
 */
bool serialization_objectSetChildCount(object_t* object, uint32_t childCount) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(!object_isRemote(object)) {
        pcb_error = pcb_error_remote_local_mismatch;
        return false;
    }

    remote_object_info_t* ri = (remote_object_info_t*) object->userData;
    ri->childCount = childCount;

    return true;
}

/**
   @brief
   Set a remote object's instance count.

   @details
   Cached objects do not need to be transferred completly. It is possible that none or some of the instances where
   requested. But still the client wants to know how many instances are available in the object.\n
   Instead of transferring all the instances, only the instance count can be transferred.\n
   Use this function to update the instance count of a remote object in the cache.

   @param object the remote object
   @param instanceCount number of children the object has

   @return
    - true: the value was set
    - false an error has occured
 */
bool serialization_objectSetInstanceCount(object_t* object, uint32_t instanceCount) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(!object_isRemote(object)) {
        pcb_error = pcb_error_remote_local_mismatch;
        return false;
    }

    remote_object_info_t* ri = (remote_object_info_t*) object->userData;
    ri->instanceCount = instanceCount;

    return true;
}

/**
   @brief
   Set a remote object's function count.

   @details
   Cached objects do not need to be transferred completly. It is possible that none or some of the functions where
   requested. But still the client wants to know how many functions are available in the object.\n
   Instead of transferring all the functions, only the instance count can be transferred.\n
   Use this function to update the instance count of a remote object in the cache.

   @param object the remote object
   @param count number of children the object has

   @return
    - true: the value was set
    - false an error has occured
 */
bool serialization_objectSetFunctionCount(object_t* object, uint32_t count) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(!object_isRemote(object)) {
        pcb_error = pcb_error_remote_local_mismatch;
        return false;
    }

    remote_object_info_t* ri = (remote_object_info_t*) object->userData;
    ri->functionCount = count;

    return true;
}

/**
   @brief
   Set a recieved request its request identifier

   @details
   When deserializing and reconstructing a request, it is very important that the original request id is set.\n
   Recreating the request can be done using the request functions, but none of them can set the request identifer.\n
   The identifier was generated when the request was send, the reciever should use this identifier in all of its replies,
   if the replies do not contain this identifer, all these replies will get lost.

   @param req the reconstructed request
   @param id original identifer

   @return
    - true: the value was set
    - false an error has occured
 */
bool serialization_setRequestId(request_t* req, uint32_t id) {
    if(!req) {
        return false;
    }

    req->request_id = id;

    return true;
}

/**
   @}
 */
