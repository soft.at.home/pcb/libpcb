/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <string.h>
#include <stdlib.h>

#include <debug/sahtrace.h>

#include <pcb/pcb_client.h>
#include <pcb/utils/variant_map.h>
#include <pcb/common/error.h>
#include <pcb/core/datamodel.h>
#include "datamodel_priv.h"
#include "parameter_priv.h"
#include "core_priv.h"
#include "object_priv.h"
#include "validator_priv.h"
#include "pcb_main_priv.h"

/**
   @file
   @brief
   Implementation of parameter functions
 */

static void parameter_acl(const parameter_t* parameter, llist_t* ACL) {
    object_acl(parameter_owner(parameter), ACL);

    if(!llist_isEmpty(&parameter->ACL)) {
        llist_iterator_t* it = NULL;
        ACL_t* acl = NULL;
        llist_for_each(it, &parameter->ACL) {
            acl = llist_item_data(it, ACL_t, it);
            aclSet(ACL, acl->id, acl->acl_flags);
        }
    }
}

/**
   @ingroup pcb_core_parameter
   @brief
   Create a parameter and add it to the specified object

   @details
   This function will create a parameter and add it to the specified object.\n
   The parameter will be in the created state (@ref parameter_state_created) until it is committed (@ref parameter_commit).\n
   This functions allocates memory for the parameter and initializes the structure.\n
   It also checks that the name is valid. (no . / $ only numeric)\n
   When the object that owns the parameter is deleted, the parameter will be deleted as well. When you
   need to remove the parameter manually use @ref parameter_delete.

   @param object The parent object, a newly created parameter must have a parent
   @param name Name of the parameter. The name can not contain ".", "/","$" or be numeric and must be unique within the parent object
   @param type The parameter type. See @ref parameter_type_t
   @param attributes The parameter attributes

   @return
    - pointer to the created parameter: Creation was successful. The parameter is in the created state
    - NULL: An error has occurred
        - object is NULL
        - name is a NULL pointer or an empty string
        - memory allocation failed
        - initialization failed
        - the name is not valid
 */
parameter_t* parameter_create(object_t* object, const char* name, parameter_type_t type, const uint32_t attributes) {
    size_t size = 0;
    parameter_def_t* paramdef = NULL;
    parameter_t* parameter = NULL;
    variant_type_t vartype = 0;
    if(!object || !name || !(*name)) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    /* verify the name
       object_verify_name characters not allowed are : . / $ only numeric
     */
    if(!object_isRemote(object)) {
        if(!isValidNameChar(name)) {
            goto error;
        }
    }

    size = sizeof(parameter_def_t) + sizeof(parameter_t) + strlen(name) + 1;
    paramdef = (parameter_def_t*) calloc(1, size);
    if(!paramdef) {
        pcb_error = pcb_error_no_memory;
        goto error;
    }
    parameter = (parameter_t*) (((void*) paramdef) + sizeof(parameter_def_t));

    parameter->definition = paramdef;

    vartype = parameter_type_to_variant_type(type);

    if(!variant_initialize(&parameter->value[0], vartype)) {
        goto error_free;
    }
    if(!variant_initialize(&parameter->value[1], vartype)) {
        goto error_cleanup_variant_0;
    }
    llist_iterator_initialize(&parameter->it);
    llist_initialize(&parameter->ACL);

    if(vartype == variant_type_string) {
        variant_setChar(&parameter->value[0], "");
        variant_setChar(&parameter->value[1], "");
    }

    parameter->definition->type = type;

    /* set the state to created */
    parameter->state = parameter_state_created;

    /* set the attributes */
    parameter->attributes = attributes;

    /* set the name */
    parameter->definition->name = (char*) (((void*) paramdef) + sizeof(parameter_def_t) + sizeof(parameter_t));
    strcpy(parameter->definition->name, name);

    if(!object_addParameter(object, parameter)) {
        goto error_cleanup_variant_1;
    }

    parameter->definition->refcount = 1;

    return parameter;

error_cleanup_variant_1:
    variant_cleanup(&parameter->value[1]);
error_cleanup_variant_0:
    variant_cleanup(&parameter->value[0]);
error_free:
    free(paramdef);
error:
    return NULL;
}

/**
   @ingroup pcb_core_parameter
   @brief
   Copy the parameter definition into an object

   @details
   This function will create an exact copy of a given parameter in an object.
   The object specified can not be the object that is the owner of the source object

   @param destObject The object where the copy will be added
   @param source Source parameter, pointer to the parameter that will be copied

   @return
    - pointer to the created parameter: Creation was successful. The parameter is in the created state
    - NULL: An error has occurred
        - object is NULL
        - name is a NULL pointer or an empty string
        - memory allocation failed
        - initialization failed
 */
parameter_t* parameter_copy(object_t* destObject, parameter_t* source) {
    parameter_t* dstParameter = NULL;
    variant_type_t vartype;

    if(!destObject || !source) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    dstParameter = (parameter_t*) calloc(1, sizeof(parameter_t));
    if(!dstParameter) {
        goto exit_error;
    }
    dstParameter->definition = source->definition;
    dstParameter->attributes = source->attributes;
    dstParameter->state = parameter_state_validate_created;
    llist_iterator_initialize(&dstParameter->it);
    llist_initialize(&dstParameter->ACL);

    if(!object_addParameter(destObject, dstParameter)) {
        free(dstParameter);
        goto exit_error;
    }

    dstParameter->definition->refcount++;

    vartype = parameter_type_to_variant_type(parameter_type(source));
    variant_initialize(&dstParameter->value[0], vartype);
    variant_initialize(&dstParameter->value[1], vartype);

    variant_copy(&dstParameter->value[0], parameter_getValue(source));
    parameter_setACL(dstParameter, parameter_getACL(source));

    return dstParameter;

exit_error:
    return NULL;
}

/**
   @ingroup pcb_core_parameter
   @brief
   Delete a parameter

   @details
   This function will delete a parameter.\n
   A parameter can only be deleted if no other operation is busy. (The parameter must be in the ready state)\n
   The parameter will be in the deleted state (@ref parameter_state_deleted) until it is committed (@ref parameter_commit).\n
   During the commit the parameter will be physically deleted.\n

   @param parameter pointer to the parameter that will be deleted

   @return
    - true: deletion was successful. The parameter is in deleted state. Use @ref parameter_commit to really delete the object.
    - false: An error has occurred
        - An other operation is buzy.
        - The parameter is no in the ready state
 */
bool parameter_delete(parameter_t* parameter) {
    if(!parameter) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    /* when remote object, do not check state */
    object_t* owner = parameter_owner(parameter);
    if(!object_isRemote(owner)) {
        /* the parameter must be in ready state, if an other operation is buzy we fail immediatly */
        if(!((parameter->state == parameter_state_ready) || (parameter->state == parameter_state_deleted))) {
            pcb_error = pcb_error_wrong_state;
            return false;
        }

        object_setParentTreeModified(owner);

        if((owner->state != object_state_created) &&
           (owner->state != object_state_deleted)) {
            owner->state = object_state_modified;
        }
    }

    /* mark this parameter as deleted */
    parameter->state = parameter_state_deleted;

    return true;
}

/**
   @ingroup pcb_core_parameter
   @brief
   Get the object that owns the specified parameter.

   @details
   This function will return the object from the specified parameter.

   @param parameter pointer to a parameter

   @return
    - the root object.
    - NULL if not a valid parameter
 */
object_t* parameter_owner(const parameter_t* parameter) {
    if(!parameter) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    llist_t* paramlist = parameter->it.list;
    object_t* owner = llist_item_data(paramlist, object_t, parameters);

    return owner;
}

/**
   @ingroup pcb_core_parameter
   @brief
   Rename a parameter.

   @details
   This function will rename a parameter.

   @param parameter pointer to a parameter
   @param name the new name of the parameter

   @return
    - true: the parameter is renamed
    - false an error has occured
 */
bool parameter_rename(parameter_t* parameter, const char* name) {
    if(!parameter || !name || !(*name)) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    if(strcmp(parameter->definition->name, name) == 0) {
        /* setting same name again */
        return true;
    }

    if(!isValidNameChar(name)) {
        /* setting invalid name */
        return false;
    }

    size_t nameLength = strlen(parameter->definition->name);
    if(strlen(name) <= nameLength) {
        strncpy(parameter->definition->name, name, nameLength + 1);
    } else {
        char* temp = parameter->definition->name;
        parameter->definition->name = strdup(name);
        if(!parameter->definition->name) {
            parameter->definition->name = temp;
            return false;
        }
        if(temp != (char*) (((void*) parameter->definition) + sizeof(parameter_def_t) + sizeof(parameter_t))) {
            free(temp);
        }
    }

    return true;
}

/**
   @ingroup pcb_core_parameter
   @brief
   Change the parameter type.

   @details
   This function will change the parameter type.

   @param parameter pointer to a parameter
   @param type the new type

   @return
    - true: the parameter has changed type
    - false an error has occured
 */
bool parameter_cast(parameter_t* parameter, parameter_type_t type) {
    if(!parameter) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }
    variant_t tmp;
    variant_initialize(&tmp, variant_type_unknown);
    variant_copy(&tmp, &parameter->value[parameter->currentConfig]);

    parameter->definition->type = type;
    parameter_setValue(parameter, &tmp);

    variant_cleanup(&tmp);
    return true;
}

/**
   @ingroup pcb_core_parameter
   @brief
   Get the value of the specified parameter.

   @details
   This function will return a pointer to the parameter value. This pointer may only be used for
   reading the parameter value, not writing.

   @param parameter pointer to a parameter

   @return
    - a variant containing the parameter value
    - NULL if not a valid parameter
 */
const variant_t* parameter_getValue(const parameter_t* parameter) {
    if(!parameter) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    return &parameter->value[parameter->currentConfig];
}

/**
   @ingroup pcb_core_parameter
   @brief
   Get the previous value of the specified parameter.

   @details
   This function will return a pointer to the previous parameter value if the parameter is in the
   modified or validated state. If this parameter is in a different state, the current parameter
   value is returned.

   @param parameter pointer to a parameter

   @return
    - a variant pointing to the previous value if the parameter state is modified or validated
    - a variant pointing to the current value in all other cases
    - NULL if not a valid parameter
 */
const variant_t* parameter_getModifiedValue(const parameter_t* parameter) {
    if(!parameter) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    if((parameter->state == parameter_state_modified) ||
       (parameter->state == parameter_state_validated)) {
        return &parameter->value[(parameter->currentConfig == 0) ? 1 : 0];
    } else {
        return &parameter->value[parameter->currentConfig];
    }
}

/**
   @ingroup pcb_core_parameter
   @brief
   Set the parameter value to the specified data.

   @details
   This function is used for setting a parameter to a certain value,\n
   If the provided variant type is of the same type as the parameter type, we just copy.\n
   If the provided variant type is of another type then the parameter type, we convert.\n

   If the parameter type is different from 'created', the state is set to 'modified'.\n
   Indicate to the parent object that the parameter was modified.\n

   @param parameter pointer to a parameter
   @param variant the new value for the parameter

   @return
    - true if setting the value was succesfull
    - false if something went wrong, pcb_error contains the reason why the set failed
      - parameter is invalid
      - deleted parameters cannot be modified
      - parameters from deleted objects cannot be modified
 */
bool parameter_setValue(parameter_t* parameter, const variant_t* variant) {
    if(!parameter) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }
    /* it is not possible to modify deleted parameters */
    if(parameter->state == parameter_state_deleted) {
        pcb_error = pcb_error_wrong_state;
        return false;
    }

    object_t* owner = parameter_owner(parameter);
    /* it is not possible to modify parameters of deleted objects */
    if((owner && (object_state(owner) == object_state_deleted)) ||
       !owner) {
        pcb_error = pcb_error_wrong_state;
        return false;
    }

    if((parameter->attributes & (parameter_attr_upc | parameter_attr_upc_overwrite)) == (parameter_attr_upc | parameter_attr_upc_overwrite)) {
        pcb_t* pcb = datamodel_pcb(object_datamodel(owner));
        if(pcb && pcb->upcOverwrite) {
            SAH_TRACEZ_NOTICE("pcb", "Not setting value of UPC Overwrite protected parameter %s.%s", object_name(owner, path_attr_key_notation), parameter_name(parameter));
            return true;
        }
    }

    /* which value to use
       if the parameter is in creation state, use value 0
     */
    uint32_t config = 0;
    variant_t restore;
    variant_initialize(&restore, variant_type_unknown);
    /* otherwise use the not currently active value */
    if((parameter->state != parameter_state_created) &&
       (parameter->state != parameter_state_validate_created)) {
        config = (parameter->currentConfig == 0) ? 1 : 0; /* use the not active one */
        int result = 0;
        bool compresult = variant_compare(&parameter->value[parameter->currentConfig], variant, &result);
        if(compresult && (result == 0)) {
            /* nothing has changed */
            parameter->state = parameter_state_ready;
            variant_cleanup(&restore);
            return true;
        }
    } else {
        variant_copy(&restore, &parameter->value[config]);
    }

    /* if the given variant is the same type as the parameter variant just copy */
    if(variant_type(&parameter->value[config]) == variant_type(variant)) {
        variant_copy(&parameter->value[config], variant);
    } else {
        /* otherwise try to convert */
        if(!variant_convert(&parameter->value[config], variant, parameter_type_to_variant_type(parameter->definition->type))) {
            variant_cleanup(&restore);
            return false;
        }
    }

    /* if the parameter is ready, modified or validated, set the state to modified
       do this for the parameter owner as well.
     */
    if((parameter->state == parameter_state_ready) ||
       (parameter->state == parameter_state_modified) ||
       (parameter->state == parameter_state_validated)) {
        parameter->state = parameter_state_modified;
        if(!parameter_validate(parameter)) {
            parameter_rollback(parameter);
            variant_cleanup(&restore);
            return false;
        }
    }

    /* if the parameter is created or created and validated set the state to created */
    if((parameter->state == parameter_state_validate_created) ||
       (parameter->state == parameter_state_created)) {
        parameter->state = parameter_state_created;
        if(!parameter_validate(parameter)) {
            variant_copy(&parameter->value[config], &restore);
            variant_cleanup(&restore);
            return false;
        }
    }
    variant_cleanup(&restore);

    if(owner->state != object_state_created) {
        owner->state = object_state_modified;
    }
    object_setParentTreeModified(owner);

    if(object_isRemote(owner)) {
        pcb_t* pcb = datamodel_pcb(object_datamodel(owner));
        remote_object_info_t* ri = (remote_object_info_t*) owner->userData;
        peer_info_t* dest = connection_find(&pcb->connection, ri->fd);
        if(!dest) {
            pcb_error = pcb_error_not_connected;
            return false;
        }
        string_t path;
        string_initialize(&path, 0);
        object_path(owner, &path, pcb->client_data.attributes);
        SAH_TRACEZ_INFO("pcb", "Set remote parameter value parameter = %s", parameter_name(parameter));
        bool retval = pcb_client_set(dest, string_buffer(&path), parameter_name(parameter), variant);
        string_cleanup(&path);
        return retval;
    }

    return true;
}

/**
   @ingroup pcb_core_parameter
   @brief
   Set the parameter value to the specified string.

   @details
   This function is used for setting a parameter to a certain string value,\n
   First the string is converted into the parameter format and the new parameter value is set.\n
   If the parameter type is different from 'created', the state is set to 'modified'.\n
   Indicate to the parent object that the parameter was modified.\n

   @param parameter pointer to a parameter
   @param string the new value for the parameter

   @return
    - true if setting the value was succesfull
    - false if something went wrong, pcb_error contains the reason why the set failed
      - parameter is invalid
      - deleted parameters cannot be modified
      - parameters from deleted objects cannot be modified
 */
bool parameter_setFromString(parameter_t* parameter, const string_t* string) {
    variant_t temp;
    variant_initialize(&temp, variant_type_string);
    variant_setString(&temp, string);

    bool retval = parameter_setValue(parameter, &temp);

    variant_cleanup(&temp);

    return retval;
}

/**
   @ingroup pcb_core_parameter
   @brief
   Set the parameter value to the specified character array.

   @details
   This function is used for setting a parameter to a certain character array value,\n
   This function first converts the character array to a string_t. Then it calls @ref parameter_setFromString
   to modify the parameter.

   @param parameter pointer to a parameter
   @param text the new value for the parameter

   @return
    - true if setting the value was succesfull
    - false if something went wrong, pcb_error contains the reason why the set failed
      - parameter is invalid
      - deleted parameters cannot be modified
      - parameters from deleted objects cannot be modified
 */
bool parameter_setFromChar(parameter_t* parameter, const char* text) {
    variant_t temp;
    variant_initialize(&temp, variant_type_string);
    variant_setChar(&temp, text);

    bool retval = parameter_setValue(parameter, &temp);

    variant_cleanup(&temp);

    return retval;
}

#ifdef PCB_HELP_SUPPORT
/**
   @ingroup pcb_core_parameter
   @brief
   Add a description to the parameter

   @details
   Add a description to the parameter

   @param parameter Pointer to the parameter_t structure
   @param description the parameter description

   @return
    - true the description is set
    - false failed to set the description
 */
bool parameter_setDescription(parameter_t* parameter, const char* description) {
    if(!parameter || !description) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(parameter->definition->description) {
        free(parameter->definition->description);
        parameter->definition->description = NULL;
    }
    if(*description) {
        parameter->definition->description = strdup(description);
        if(!parameter->definition->description) {
            return false;
        }
    }

    return true;
}

/**
   @ingroup pcb_core_parameter
   @brief
   Get the description of a parameter

   @details
   Get the description of a parameter

   @param parameter Pointer to the parameter_t structure

   @return
    - string containing the description
    - NULL when no description is available
 */
const char* parameter_getDescription(parameter_t* parameter) {
    if(!parameter) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    return parameter->definition->description;
}
#endif

/**
   @ingroup pcb_core_parameter
   @brief
   Update the parameter by calling the 'read' handler.

   @details
   This function is used for updating the specified parameter.
   First the 'read' handler linked with this parameter is called if available

   If no 'read' handler is installed, the parameter value doesn't change and no operation is done

   @param parameter pointer to a parameter

   @return
    - true if updating the value was succesfull
    - false if something went wrong, pcb_error contains the reason why the update failed
       - invalid parameter
       - parameter state is not 'ready'
       - the read handler returned false
       - the validation function returned false
 */
bool parameter_update(parameter_t* parameter) {
    if(!parameter) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(parameter->map) {
        return true;
    }

    if(parameter->definition->handlers && parameter->definition->handlers->read) {
        bool retval = parameter->definition->handlers->read(parameter, &parameter->value[parameter->currentConfig]);
        if(!retval) {
            return false;
        }
    }

    return true;
}

/**
   @ingroup pcb_core_parameter
   @brief
   Check that the parameter contains an access control list

   @details
   This function will check that there is an access control list attached to the parameter.

   @param parameter pointer to a parameter

   @return
    - true an ACL is available
    - false no acl is available
 */
bool parameter_hasAcl(parameter_t* parameter) {
    if(!parameter) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    return !llist_isEmpty(&parameter->ACL);
}

/**
   @ingroup pcb_core_parameter
   @brief
   Get the access control list from a parameter

   @details
   This function will get the access control list of a parameter.

   @param parameter pointer to a parameter

   @return
    - linked list containing the access control of the object.
 */
const llist_t* parameter_getACL(const parameter_t* parameter) {
    if(!parameter) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    return &parameter->ACL;
}

/**
   @ingroup pcb_core_parameter
   @brief
   Get the calculated access control list from a parameter

   @details
   This function will get the calculated access control list of a parameter.

   @param parameter pointer to a parameter
   @param ACL pointer to a linked list, this will be filled with the calculated access control information

   @return
    - linked list containing the access control of the object.
    - free the list using @ref acl_list_destroy
 */
void parameter_getCalculatedACL(const parameter_t* parameter, llist_t* ACL) {
    if(!parameter || !ACL) {
        pcb_error = pcb_error_invalid_parameter;
        return;
    }

    acl_list_destroy(ACL);
    parameter_acl(parameter, ACL);
}

/**
   @ingroup pcb_core_parameter
   @brief
   Apply an access control list on the parameter

   @details
   This function will apply an access control list on the parameter.

   @param parameter pointer to a parameter
   @param acl linked kist containing the access control information

   @return
    - true: the acl is updated.
    - false: failed to update the acl
 */
bool parameter_setACL(parameter_t* parameter, const llist_t* acl) {
    if(!parameter || !acl) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    ACL_t* a = NULL;
    llist_iterator_t* it = NULL;
    llist_for_each(it, acl) {
        a = llist_item_data(it, ACL_t, it);
        parameter_aclSet(parameter, a->id, a->acl_flags);
    }

    return true;
}

/**
   @ingroup pcb_core_parameter
   @brief
   Modify an access control of the parameter

   @details
   This function will modify an existing access control that was already added to the parameter.

   @param parameter pointer to a parameter
   @param id user or group id
   @param flags identifies that the id is a user or group id  and the access wanted

   @return
    - true: the acl is updated.
    - false: failed to update the acl
 */
bool parameter_aclSet(parameter_t* parameter, uint32_t id, uint16_t flags) {
    if(!parameter) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    return aclSet(&parameter->ACL, id, flags);
}

/**
   @ingroup pcb_core_parameter
   @brief
   Add an access control to the parameter

   @details
   This function will add an access control to the parameter. A check is performed to see if there
   is already an access control available for the parameter. If it is not available a new one will be added
   otherwise it will modify the existing one.

   @param parameter pointer to a parameter
   @param id user or group id
   @param flags identifies that the id is a user or group id  and the access that needs to be added

   @return
    - true: the acl is updated.
    - false: failed to update the acl
 */
bool parameter_aclAdd(parameter_t* parameter, uint32_t id, uint16_t flags) {
    if(!parameter) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    llist_t ACL;
    llist_initialize(&ACL);
    parameter_acl(parameter, &ACL);

    ACL_t* acl = aclFind(&ACL, id, flags);
    if(acl) {
        flags |= acl->acl_flags;
    }
    llist_iterator_t* it = llist_takeFirst(&ACL);
    while(it) {
        acl = llist_item_data(it, ACL_t, it);
        free(acl);
        it = llist_takeFirst(&ACL);
    }
    return aclSet(&parameter->ACL, id, flags);
}

/**
   @ingroup pcb_core_parameter
   @brief
   Removes an access control from the parameter

   @details
   This function will remove an access control from the parameter. A check is performed to see if there
   is already an access control available for the parameter. If it is not available a new one will be added
   otherwise it will modify the existing one.

   @param parameter pointer to a parameter
   @param id user or group id
   @param flags identifies that the id is a user or group id and the access that needs to be deleted

   @return
    - true: the acl is updated.
    - false: failed to update the acl
 */
bool parameter_aclDel(parameter_t* parameter, uint32_t id, uint16_t flags) {
    if(!parameter) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    llist_t ACL;
    llist_initialize(&ACL);
    parameter_acl(parameter, &ACL);
    bool group = false;
    if(flags & acl_group_id) {
        group = true;
    }

    ACL_t* acl = aclFind(&ACL, id, flags);
    if(acl) {
        flags = acl->acl_flags & ~flags;
    } else {
        flags = 0;
    }
    if(group) {
        flags |= acl_group_id;
    }
    llist_iterator_t* it = llist_takeFirst(&ACL);
    while(it) {
        acl = llist_item_data(it, ACL_t, it);
        free(acl);
        it = llist_takeFirst(&ACL);
    }
    return aclSet(&parameter->ACL, id, flags);
}

/**
   @ingroup pcb_core_parameter
   @brief
   Check that the given userid can read the parameter

   @details
   Checks the ACL to see that the given userid can read the parameter

   @param parameter pointer to a parameter
   @param uid user id

   @return
    - true: the user can read
    - false: the user is not allowed to read
 */
bool parameter_canRead(parameter_t* parameter, uint32_t uid) {
    if(!parameter) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(object_isRemote(parameter_owner(parameter)) || (uid == 0)) {
        return true;
    }

    llist_t ACL;
    llist_initialize(&ACL);
    parameter_acl(parameter, &ACL);
    bool retval = aclCheck(&ACL, uid, acl_read);
    aclDestroy(&ACL);
    return retval;
}

/**
   @ingroup pcb_core_parameter
   @brief
   Check that the given userid can write the parameter

   @details
   Checks the ACL to see that the given userid can write the parameter

   @param parameter pointer to a parameter
   @param uid user id

   @return
    - true: the user can write
    - false: the user is not allowed to write
 */
bool parameter_canWrite(parameter_t* parameter, uint32_t uid) {
    if(!parameter) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(object_isRemote(parameter_owner(parameter)) || (uid == 0)) {
        return true;
    }

    llist_t ACL;
    llist_initialize(&ACL);
    parameter_acl(parameter, &ACL);
    bool retval = aclCheck(&ACL, uid, acl_write);
    aclDestroy(&ACL);
    return retval;
}

/**
   @ingroup pcb_core_parameter
   @brief
   Check that the given userid can read the parameter value

   @details
   Checks the ACL to see that the given userid can read the parameter value

   @param parameter pointer to a parameter
   @param uid user id

   @return
    - true: the user can read the parameter value
    - false: the user is not allowed to read the parameter value
 */
bool parameter_canReadValue(parameter_t* parameter, uint32_t uid) {
    if(!parameter) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(object_isRemote(parameter_owner(parameter)) || (uid == 0)) {
        return true;
    }

    llist_t ACL;
    llist_initialize(&ACL);
    parameter_acl(parameter, &ACL);
    bool retval = aclCheck(&ACL, uid, acl_execute);
    aclDestroy(&ACL);
    return retval;
}

/**
   @ingroup pcb_core_parameter
   @brief
   Set an upc_attribute of the parameter

   @details
   This function will modify the upc attributes of the parameter.

   @param parameter pointer to a parameter
   @param attributes the new set of upc attributes

   @return
    - true: the upc attributes are updated.
    - false: failed to update the upc attributes
 */
bool parameter_upcSet(parameter_t* parameter, uint32_t attributes) {
    if(!parameter) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    attributes &= (parameter_attr_upc | parameter_attr_upc_usersetting | parameter_attr_upc_overwrite);

    /* Make sure to set as UPC and reboot persistent */
    if(attributes != 0) {
        attributes |= parameter_attr_upc | parameter_attr_persistent;
    }

    parameter->attributes |= attributes;
    return true;
}

/**
   @ingroup pcb_core_parameter
   @brief
   Clear upc attributes of the parameter

   @details
   This function will modify the upc attributes of the parameter.

   @param parameter pointer to a parameter
   @param attributes the set of upc attributes to clear

   @return
    - true: the upc attributes are updated.
    - false: failed to update the upc attributes
 */
bool parameter_upcClear(parameter_t* parameter, uint32_t attributes) {
    if(!parameter) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    attributes &= (parameter_attr_upc | parameter_attr_upc_usersetting | parameter_attr_upc_overwrite);

    parameter->attributes &= ~attributes;
    return true;
}

/**
   @ingroup pcb_core_parameter
   @brief
   Commit the parameter value.

   @details
   This function commits the parameter changes to the visible datamodel.\n

   If the current parameter state is:
   @li created or modified - then this function will validate the new value and put the parameter into the ready state
   @li validated - then this function will update the parameter value with it's new value and put the parameter into the ready state
   @li deleted - the deleted parameter is completely removed from the system, all memory is freed

   The write handler will be called if the parameter is in the created, modified or validated state.

   @param parameter pointer to a parameter

   @return
    - true if commiting the value was succesfull
    - false if something went wrong, pcb_error contains the reason why the set failed
      - parameter is invalid
      - parameter state was 'ready'
      - parameter validation failed
 */
bool parameter_commit(parameter_t* parameter) {
    if(!parameter) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(parameter->state == parameter_state_ready) {
        return true;
    }

    if((parameter->state == parameter_state_modified) ||
       (parameter->state == parameter_state_created)) {
        bool retval = parameter_validate(parameter);
        if(!retval) {
            return false;
        }
    }

    /* the parameter is validated if we reach this code
       from now on the commits can not fail anymore, so no need to check this
     */


    /* commit the parameter itself */
    switch(parameter->state) {
    case parameter_state_validate_created:     /* apply the parameter */
        parameter->state = parameter_state_ready;
        if(parameter->definition->handlers && parameter->definition->handlers->write) {
            parameter->definition->handlers->write(parameter, NULL);
        }
        break;
    case parameter_state_validated:                                         /* apply the changed parameters */
        parameter->currentConfig = (parameter->currentConfig == 0) ? 1 : 0; /* set the correct active config */
        parameter->state = parameter_state_ready;

        /* Parameters marked as UPC must be set to upc_changed when changes are applied */
        if(parameter->attributes & parameter_attr_upc) {
            pcb_t* pcb = datamodel_pcb(object_datamodel(parameter_owner(parameter)));
            if(pcb && pcb->upcEnabled) {
                parameter->attributes |= parameter_attr_upc_changed;

                if(pcb->upcEventCB) {
                    pcb->upcEventCB(upc_event_changed, parameter_owner(parameter), parameter, pcb->upcEventUserdata);
                }
            }
        }

        if(parameter->definition->handlers && parameter->definition->handlers->write) {
            parameter->definition->handlers->write(parameter, &parameter->value[(parameter->currentConfig == 0) ? 1 : 0]);
        }

        variant_cleanup(&parameter->value[(parameter->currentConfig == 0) ? 1 : 0]);
        break;
    case parameter_state_deleted:     /* delete the parameter */
        parameter_destroy(parameter);
        break;
    default:
        break;
    }

    return true;
}

/**
   @ingroup pcb_core_parameter
   @brief
   Roll back the last changes of a parameter.

   @details
   This function restores the latest valid parameter changes.\n

   If the current parameter state is:
   @li created - the current parameter will be destoryed
   @li ready - nothing happens
   @li modified, validated, deleted - the parameter state will return to the 'ready' state without committing the last changes

   @param parameter pointer to a parameter
 */
void parameter_rollback(parameter_t* parameter) {
    if(!parameter) {
        pcb_error = pcb_error_wrong_state;
        return;
    }

    /* parameters in ready can not be rollbacked */
    if(parameter->state == parameter_state_ready) {
        return;
    }

    /* rollback the parametert itself */
    switch(parameter->state) {
    case parameter_state_created:     /* delete the object */
    case parameter_state_validate_created:
        /* if it is not in any object, the next line does not do anything */
        parameter_destroy(parameter);
        break;
    case parameter_state_modified:    /* revert the parameters */
    case parameter_state_validated:   /* revert the parameters */
    case parameter_state_deleted:     /* undelete the object */
        /* rollback a parameter is easy, just reset the state to ready */
        parameter->state = parameter_state_ready;
        break;
    default:
        break;
    }
}

/**
   @ingroup pcb_core_parameter
   @brief
   Validate the new parameter value.

   @details
   This function validates the parameter changes.\n

   Only parameters in the created and modified state can be validated.\n
   This function calls the validation handler specified for the parameter.\n
   Only parameters in the 'modified' state will be set to the 'validated' state, parameters in the 'created'
   state will stay 'created'.

   @param parameter pointer to a parameter

   @return
    - true if validating the value was succesfull or the parameter state was not modified or created
    - false if something went wrong, pcb_error contains the reason why the set failed
      - parameter is invalid
      - the validation handler function returned false
      - a mandatory or key parameter has no value set
 */
bool parameter_validate(parameter_t* parameter) {
    if(!parameter) {
        pcb_error = pcb_error_wrong_state;
        return false;
    }

    /* only validate created parameters and modified parameters
       deleted, validated and ready parameters do not need to be validated
     */
    if((parameter->state != parameter_state_modified) &&
       (parameter->state != parameter_state_created)) {
        return true;
    }

    bool retval = true;

    /* validate the parameter itself */
    if(parameter->definition->validator && parameter->definition->validator->validate) {
        SAH_TRACEZ_INFO("pcb", "Calling parameter validation handler for parameter %s ...", parameter_name(parameter));
        retval = parameter->definition->validator->validate(parameter, parameter->definition->validator);
        SAH_TRACEZ_INFO("pcb", "    Validation %s", retval ? "OK" : "NOK");
        if(!retval) {
            return retval;
        }
    }

    /* only change the state for modified objects, created objects stay in created state */
    if((parameter->state == parameter_state_modified) && retval) {
        parameter->state = parameter_state_validated;
    }

    if((parameter->state == parameter_state_created) && retval) {
        parameter->state = parameter_state_validate_created;
    }
    return retval;
}

/**
   @ingroup pcb_core_parameter
   @brief
   Set the parameter read-only attribute.

   @details
   Enable/disable the parameter read-only attribute with the defined value.\n

   @param parameter pointer to a parameter
   @param enable Set/unset this flag to enable/disable the read-only attribute
 */
void parameter_setReadOnly(parameter_t* parameter, const bool enable) {
    if(!parameter) {
        pcb_error = pcb_error_invalid_parameter;
        return;
    }

    if(enable) {
        parameter->attributes |= parameter_attr_read_only;
    } else {
        parameter->attributes &= ~parameter_attr_read_only;
    }
}

/**
   @ingroup pcb_core_parameter
   @brief
   Set the parameter persistent attribute.

   @details
   Enable/disable the parameter persistent attribute with the defined value.\n

   @param parameter pointer to a parameter
   @param enable Set/unset this flag to enable/disable the persistent attribute
 */
void parameter_setPersistent(parameter_t* parameter, const bool enable) {
    if(!parameter) {
        pcb_error = pcb_error_invalid_parameter;
        return;
    }

    if(enable) {
        parameter->attributes |= parameter_attr_persistent;
    } else {
        parameter->attributes &= ~parameter_attr_persistent;
    }
}

/**
   @ingroup pcb_core_parameter
   @brief
   Set the parameter upc attribute.

   @details
   Enable/disable the parameter upc attribute with the defined value.\n

   @param parameter pointer to a parameter
   @param enable Set/unset this flag to enable/disable the upc attribute
 */
void parameter_setUPC(parameter_t* parameter, const bool enable) {
    if(!parameter) {
        pcb_error = pcb_error_invalid_parameter;
        return;
    }

    if(enable) {
        parameter->attributes |= parameter_attr_upc | parameter_attr_persistent;
    } else {
        parameter->attributes &= ~parameter_attr_upc;
    }
}

/**
   @ingroup pcb_core_parameter
   @brief
   Set the parameter usersetting attribute.

   @details
   Enable/disable the parameter usersetting attribute with the defined value.\n

   @param parameter pointer to a parameter
   @param enable Set/unset this flag to enable/disable the usersetting attribute
 */
void parameter_setUserSetting(parameter_t* parameter, const bool enable) {
    if(!parameter) {
        pcb_error = pcb_error_invalid_parameter;
        return;
    }

    if(enable) {
        parameter->attributes |= parameter_attr_upc | parameter_attr_upc_usersetting | parameter_attr_persistent;
    } else {
        parameter->attributes &= ~parameter_attr_upc_usersetting;
    }
}

/**
   @ingroup pcb_core_parameter
   @brief
   Set the parameter upc_overwrite attribute.

   @details
   Enable/disable the parameter upc_overwrite attribute with the defined value.\n

   @param parameter pointer to a parameter
   @param enable Set/unset this flag to enable/disable the upc_overwrite attribute
 */
void parameter_setUPCOverwrite(parameter_t* parameter, const bool enable) {
    if(!parameter) {
        pcb_error = pcb_error_invalid_parameter;
        return;
    }

    if(enable) {
        if(parameter->attributes & parameter_attr_upc) {
            parameter->attributes |= parameter_attr_upc_overwrite;
        }
    } else {
        parameter->attributes &= ~parameter_attr_upc_overwrite;
    }
}

/**
   @ingroup pcb_core_parameter
   @brief
   Set the parameter key attribute.

   @details
   Enable/disable the parameter key attribute with the defined value.\n

   @param parameter pointer to a parameter
   @param enable Set/unset this flag to enable/disable the key attribute
 */
void parameter_setKey(parameter_t* parameter, const bool enable) {
    if(!parameter) {
        pcb_error = pcb_error_invalid_parameter;
        return;
    }

    if(enable) {
        if(parameter->attributes & parameter_attr_key) {
            parameter->attributes |= parameter_attr_key;
        }
    } else {
        parameter->attributes &= ~parameter_attr_key;
    }
}

/**
   @ingroup pcb_core_parameter
   @brief
   Inspect the parameter read-only attribute flag.

   @details
   Call this function to see if the parameter read-only attribute is set.\n

   @param parameter pointer to a parameter

   @return
   - false if called with a null pointer, pcb_error is set
   - the parameter read-only attribute value
 */
bool parameter_isReadOnly(const parameter_t* parameter) {
    if(!parameter) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    return parameter->attributes & parameter_attr_read_only;
}

/**
   @ingroup pcb_core_parameter
   @brief
   Inspect the parameter attribute flags.

   @details
   Call this function to see all the parameter attributes.\n

   @param parameter pointer to a parameter

   @return
   - 0 if called with a null pointer, pcb_error is set
   - the parameter attribute value
 */
uint32_t parameter_attributes(const parameter_t* parameter) {
    if(!parameter) {
        pcb_error = pcb_error_invalid_parameter;
        return 0;
    }

    return parameter->attributes;
}

/**
   @ingroup pcb_core_parameter
   @brief
   Returns the multi-instance object for which this parameter is an instance counter.

   @details
   This function returns the multi-instance object for which this parameter is an instance counter.

   @param parameter pointer to a parameter

   @return
    - a pointer to a multi-instance object
    - NULL:
        - parameter is a NULL pointer
        - the parameter's object is remote
        - the parameter is no instance counter
 */
object_t* parameter_getCountedObject(parameter_t* parameter) {
    if(!parameter) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }
    if(object_isRemote(parameter_owner(parameter))) {
        pcb_error = pcb_error_remote_local_mismatch;
        return NULL;
    }

    return parameter->countedObject;
}

/**
   @ingroup pcb_core_parameter
   @brief
   Check if the parameter is an instance counter.

   @details
   This function checks if the parameter is an instance counter.

   @param parameter pointer to a parameter

   @return
    - true the parameter is an instance counter
    - false:
        - parameter is a NULL pointer
        - the parameter's object is remote
        - the parameter is no instance counter
 */
bool parameter_isInstanceCounter(parameter_t* parameter) {
    if(!parameter) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }
    if(object_isRemote(parameter_owner(parameter))) {
        pcb_error = pcb_error_remote_local_mismatch;
        return false;
    }

    if(parameter->countedObject != NULL) {
        return true;
    }
    return false;
}

/**
   @ingroup pcb_core_parameter
   @brief
   Set the destroy handler function for the specified parameter.

   @details
   Call this function to set a destoy handler function.\n
   This function can only be called for local objects. \n

   @param parameter pointer to a parameter
   @param handler the destroy handler

   @return
   - false if an error occurred, pcb_error is set
     - parameter is NULL
     - object is a remote object
     - if it is impossible to access the parameter handlers
   - true if the destroy handler was set correctly
 */
bool parameter_setDestroyHandler(parameter_t* parameter, parameter_destroy_handler_t handler) {
    if(!parameter) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }
    if(object_isRemote(parameter_owner(parameter))) {
        pcb_error = pcb_error_remote_local_mismatch;
        return false;
    }

    parameter_handlers_t* handlers = parameter_getHandlers(parameter);
    if(!handlers) {
        return false;
    }

    handlers->destroy = handler;
    return true;
}

/**
   @ingroup pcb_core_parameter
   @brief
   Set the read handler function for the specified parameter.

   @details
   Call this function to set a read handler function.\n
   This function can only be called for local objects. \n

   @param parameter pointer to a parameter
   @param handler the read handler

   @return
   - false if an error occurred, pcb_error is set
     - parameter is NULL
     - object is a remote object
     - if it is impossible to access the parameter handlers
   - true if the read handler was set correctly
 */
bool parameter_setReadHandler(parameter_t* parameter, parameter_read_handler_t handler) {
    if(!parameter) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }
    if(object_isRemote(parameter_owner(parameter))) {
        pcb_error = pcb_error_remote_local_mismatch;
        return false;
    }

    parameter_handlers_t* handlers = parameter_getHandlers(parameter);
    if(!handlers) {
        return false;
    }

    handlers->read = handler;
    /* Removed: Possibly causing regressions. Some parameters with read handlers were sending events if changed from within the plugin */
    if(handlers->read) {
        parameter->attributes |= parameter_attr_volatile_handler;
    } else {
        parameter->attributes &= ~parameter_attr_volatile_handler;
    }
    return true;
}

/**
   @ingroup pcb_core_parameter
   @brief
   Set the write handler function for the specified parameter.

   @details
   Call this function to set a write handler function.\n
   This function can only be called for local objects. \n

   @param parameter pointer to a parameter
   @param handler the write handler

   @return
   - false if an error occurred, pcb_error is set
     - parameter is NULL
     - object is a remote object
     - if it is impossible to access the parameter handlers
   - true if the write handler was set correctly
 */
bool parameter_setWriteHandler(parameter_t* parameter, parameter_write_handler_t handler) {
    if(!parameter) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }
    if(object_isRemote(parameter_owner(parameter))) {
        pcb_error = pcb_error_remote_local_mismatch;
        return false;
    }

    parameter_handlers_t* handlers = parameter_getHandlers(parameter);
    if(!handlers) {
        return false;
    }

    handlers->write = handler;
    return true;
}

bool parameter_setPrewriteHandler(parameter_t* parameter, parameter_prewrite_handler_t handler) {
    if(!parameter) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }
    if(object_isRemote(parameter_owner(parameter))) {
        pcb_error = pcb_error_remote_local_mismatch;
        return false;
    }

    parameter_handlers_t* handlers = parameter_getHandlers(parameter);
    if(!handlers) {
        return false;
    }

    handlers->prewrite = handler;
    return true;
}

/**
   @ingroup pcb_core_parameter
   @brief
   Set the validator function for the specified parameter.

   @details
   Call this function to set a validator function for this parameter.\n
   If the parameter allready has a validator, it is cleaned up with @ref param_validator_destroy.\n

   @param parameter pointer to a parameter
   @param validator the new validator function pointer.

   @return
   - false if an error occurred, pcb_error is set
     - parameter is NULL
   - true if the the validation function was set correctly
 */
bool parameter_setValidator(parameter_t* parameter, parameter_validator_t* validator) {
    if(!parameter) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    param_validator_destroy(parameter->definition->validator);

    parameter->definition->validator = validator;

    return true;
}

/**
   @ingroup pcb_core_parameter
   @brief
   Get a pointer to the validator function for the specified parameter.

   @details
   Call this function to get a pointer to the validator function for this parameter.\n

   @param parameter pointer to a parameter

   @return
   - NULL if the parameter is NULL
   - a pointer tot the current parameter validator function
 */
parameter_validator_t* parameter_getValidator(parameter_t* parameter) {
    if(!parameter) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    return parameter->definition->validator;
}

/**
   @ingroup pcb_core_parameter
   @brief
   Get the parameter name of the specified parameter.

   @details
   Call this function to get a pointer to the parameter name.\n
   The resulting character pointer is read-only. Do NOT use this function to set the parameter name.\n

   @param parameter pointer to a parameter

   @return
   - NULL if the parameter or parameter->definition are NULL
   - a pointer to the current parameter name
 */
const char* parameter_name(const parameter_t* parameter) {
    if(!parameter) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    if(!parameter->definition) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    return parameter->definition->name;
}

/**
   @ingroup pcb_core_parameter
   @brief
   Get the parameter state of the specified parameter.

   @details
   Call this function to get the parameter state (integer value).\n

   @param parameter pointer to a parameter

   @return
   - parameter_state_invalid if the parameter is NULL
   - the current parameter state
 */
parameter_state_t parameter_state(const parameter_t* parameter) {
    if(!parameter) {
        pcb_error = pcb_error_invalid_parameter;
        return parameter_state_invalid;
    }

    return parameter->state;
}

/**
   @ingroup pcb_core_parameter
   @brief
   Get the parameter type of the specified parameter.

   @details
   Call this function to get the parameter type (integer value).\n

   @param parameter pointer to a parameter

   @return
   - parameter_type_unknown if the parameter is NULL
   - the current parameter type
 */
parameter_type_t parameter_type(const parameter_t* parameter) {
    if(!parameter) {
        pcb_error = pcb_error_invalid_parameter;
        return parameter_type_unknown;
    }

    return parameter->definition->type;
}

/**
   @ingroup pcb_core_parameter
   @brief
   Get the parameter type name

   @details
   Returns the parameter type name.

   @param parameter Pointer to the parameter_t structure

   @return
    - the parameter type name
 */
const char* parameter_typeName(parameter_t* parameter) {
    if(!parameter) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    switch(parameter_type(parameter)) {
    case parameter_type_string:
        return "string";
        break;
    case parameter_type_int8:
        return "int8";
        break;
    case parameter_type_int16:
        return "int16";
        break;
    case parameter_type_int32:
        return "int32";
        break;
    case parameter_type_int64:
        return "int64";
        break;
    case parameter_type_uint8:
        return "uint8";
        break;
    case parameter_type_uint16:
        return "uint16";
        break;
    case parameter_type_uint32:
        return "uint32";
        break;
    case parameter_type_uint64:
        return "uint64";
        break;
    case parameter_type_bool:
        return "bool";
        break;
    case parameter_type_date_time:
        return "datetime";
        break;
    case parameter_type_reference:
        return "reference";
        break;
    default:
    case parameter_type_unknown:
        return "unknown";
        break;
    }
}

/**
   @ingroup pcb_core_parameter
   @brief
   Add user data to the parameter.

   @details
   Add some user data to the parameter

   @param parameter pointer to a parameter
   @param data pointer to some data
 */
void parameter_setUserData(parameter_t* parameter, void* data) {
    if(!parameter) {
        pcb_error = pcb_error_invalid_parameter;
        return;
    }

    parameter->userData = data;
}

/**
   @ingroup pcb_core_parameter
   @brief
   Get the user data from the parameter

   @details
   This function gives the pointer back to the user data that was added to the parameter

   @param parameter pointer to a parameter

   @return
    - pointer to the user data
    - NULL if no user data is available
 */
void* parameter_getUserData(parameter_t* parameter) {
    if(!parameter) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    return parameter->userData;
}
