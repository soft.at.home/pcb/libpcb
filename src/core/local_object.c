/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <string.h>

#include <debug/sahtrace.h>
#include <pcb/common/error.h>
#include <pcb/utils/string.h>
#include <pcb/utils/string_list.h>
#include <pcb/utils/tree.h>
#include <pcb/utils/peer.h>
#include <pcb/utils/variant_map.h>
#include <pcb/utils/variant_map_iterator.h>
#include <pcb/utils/variant_list.h>
#include <pcb/utils/variant_list_iterator.h>
#include <pcb/core/notification.h>
#include <pcb/core/function.h>

#include "parameter_priv.h"
#include "validator_priv.h"
#include "object_priv.h"
#include "request_priv.h"
#include "pcb_main_priv.h"
#include "function_priv.h"

/**
   @file
   @brief
   Header file with private function implementation for handling local objects
 */

/**
   @brief
   Create a full copy of an object

   @details
   This function recursivly copies an object and all it's children, but no instances.
   This function is used to create an instance of a template object.
 */
static bool local_object_copy(object_t* dest, object_t* src) {
    /* copy parameters */
    parameter_t* srcParameter = NULL;
    parameter_t* dstParameter = NULL;
    function_t* srcFunction = NULL;
    function_t* dstFunction = NULL;
    object_t* child = NULL;
    object_t* copyChild = NULL;
    const char* name = NULL;
    uint32_t attributes = 0;

    // copy ACL
    object_setACL(dest, object_getACL(src));

    object_for_each_parameter(srcParameter, src) {
        if((srcParameter->attributes & parameter_attr_template_only) == parameter_attr_template_only) {
            continue;
        }
        dstParameter = parameter_copy(dest, srcParameter);
        if(!dstParameter) {
            goto error;
        }
    }

    /* tree_item
       copy children
     */
    object_for_each_child(child, src) {
        name = object_name(child, path_attr_default); /* attributes do not matter here, these should be real object definitions */
        /* remove mib attribute */
        attributes = (child->attributes & ~object_attr_mib);
        if(object_isMib(dest)) {
            /* re-add  mib attribute if destination is a mib object */
            attributes |= object_attr_mib;
        }
        copyChild = object_allocate(name, attributes);
        if(!copyChild) {
            goto error;
        }
        /* set the parent, do not use the object_setParent function here
           no need to check that the child name is OK
         */
        tree_item_appendChild(&dest->tree_item, &copyChild->tree_item);
        if(!local_object_copy(copyChild, child)) {
            goto error;
        }
    }

    /* state not needed to copy
       name not needed to copy
     */

    if(dest->attributes & object_attr_template) {
        dest->object_info.template_info->max_instances = src->object_info.template_info->max_instances;
        if(src->object_info.template_info->instanceCounter != NULL) {
            dest->object_info.template_info->instanceCounter = object_getParameter(object_parent(dest), parameter_name(src->object_info.template_info->instanceCounter));
            if(dest->object_info.template_info->instanceCounter != NULL) {
                dest->object_info.template_info->instanceCounter->countedObject = dest;
            }
        }
        /* last_used_index not needed to copy
           instances not needed to copy
         */
    }
    /* no need to copy instance info. */

    /* copy description */
#ifdef PCB_HELP_SUPPORT
    if(src->description && !(dest->attributes & object_attr_instance)) {
        dest->description = strdup(src->description);
    }
#endif

    /* copy functions */
    object_for_each_function(srcFunction, src) {
        if(srcFunction->definition->attributes & function_attr_template_only) {
            continue;
        }

        dstFunction = function_copy(dest, srcFunction);
        if(!dstFunction) {
            goto error;
        }
    }

    if(src->handlers) {
        dest->handlers = (object_handlers_t*) calloc(1, sizeof(object_handlers_t));
        if(!dest->handlers) {
            goto error;
        }

        memcpy(dest->handlers, src->handlers, sizeof(object_handlers_t));
    }
    dest->validator = src->validator;
    if(dest->validator) {
        dest->validator->reference++;
    }
    /* no need to copy pointer to user data */

    pcb_error = pcb_ok;
    return true;

error:
    return false;
}

/**
   @brief
   Checks that the object is containing a child object with a name

   @details
   When the provided object contains a child object with the name provided this function
   will return true.

   This function is used to check no duplicates are created.
 */
static bool local_object_hasName(const object_t* object, const char* name, uint32_t pathAttributes) {
    pcb_error = pcb_ok;
    const char* objectName = NULL;
    int offset = 0;
    unsigned int length = strlen(name);
    if(object_isInstance(object) && !(pathAttributes & path_attr_key_notation) && (pathAttributes & path_attr_alias)) {
        if((name[0] == '[') && (name[strlen(name) - 1] == ']')) {
            offset = 1;
            length = strlen(name) - 2;
            objectName = object_name(object, pathAttributes | path_attr_alias);
        } else {
            objectName = object_name(object, pathAttributes & ~path_attr_alias);
        }
    } else {
        objectName = object_name(object, pathAttributes);
    }

    if((length == strlen(objectName)) && (strncmp(objectName, name + offset, length) == 0)) {
        return true;
    } else {
        return false;
    }
}

/**
   @brief
   Find an object from an object given the path

   @details
   Search an object within another object, given a path. The path must start from the object given.
   If path_attr_partial_match is set in the attributes the full path must not be matched.
   If the full path is not matched then exactMatch will be set to false.

   The children are searched as well as the instances.
 */
static object_t* local_object_find(object_t* object, string_list_t* pathList, uint32_t attributes, bool* exactMatch) {
    uint32_t offset = 0;
    string_list_iterator_t* piece = string_list_takeFirst(pathList);
    if(piece == NULL) {
        pcb_error = pcb_error_not_found;
        return NULL;
    }
    if(string_buffer(&piece->string)[0] == '%') {
        SAH_TRACEZ_INFO("pcb", "Switch to index path");
        attributes &= ~path_attr_key_notation;
        offset = 1;
    }
    if(string_buffer(&piece->string)[0] == '$') {
        SAH_TRACEZ_INFO("pcb", "Switch to key path");
        attributes |= path_attr_key_notation;
        offset = 1;
    }

    object_t* searchObject = NULL;
    object_t* tempObject = NULL;
    object_for_each_instance(searchObject, object) {
        if(local_object_hasName(searchObject, string_buffer(&piece->string) + offset, attributes)) {
            if(string_list_isEmpty(pathList)) {
                string_list_iterator_destroy(piece);
                pcb_error = pcb_ok;
                if(exactMatch) {
                    *exactMatch = true;
                }
                return searchObject;
            } else {
                string_list_iterator_destroy(piece);
                tempObject = local_object_find(searchObject, pathList, attributes, exactMatch);
                if(tempObject) {
                    return tempObject;
                } else {
                    if(attributes & path_attr_partial_match) {
                        return searchObject;
                    } else {
                        return NULL;
                    }
                }
            }
        }
    }

    object_for_each_child(searchObject, object) {
        if(local_object_hasName(searchObject, string_buffer(&piece->string) + offset, attributes)) {
            if(string_list_isEmpty(pathList)) {
                string_list_iterator_destroy(piece);
                pcb_error = pcb_ok;
                if(exactMatch) {
                    *exactMatch = true;
                }
                return searchObject;
            } else {
                string_list_iterator_destroy(piece);
                tempObject = local_object_find(searchObject, pathList, attributes, exactMatch);
                if(tempObject) {
                    return tempObject;
                } else {
                    if(attributes & path_attr_partial_match) {
                        return searchObject;
                    } else {
                        return NULL;
                    }
                }
            }
        }
    }

    string_list_iterator_destroy(piece);
    pcb_error = pcb_error_not_found;
    return NULL;
}

/**
   @brief
   Send an object update to the originator from the notify request

   @details
   This function will send the object according to the request attributes to the originator from
   a notify request. This function is called from within the @ref local_object_commit_notify function.
 */
static void local_object_sendUpdates(object_t* object, notify_list_t** notifyRequests, uint32_t depth, uint32_t flags, string_list_t* changedParams) {
    notify_list_t* elm = *notifyRequests;

    while(elm) {
        if(((request_depth(elm->req) - elm->notifyDepth) < depth) && (request_depth(elm->req) != UINT32_MAX)) {
            elm = elm->next;
            continue;
        }

        /* this request does not want updates */
        if(request_attributes(elm->req) & request_notify_no_updates) {
            elm = elm->next;
            continue;
        }

        /* this request does match the notification flags */
        if((request_attributes(elm->req) & flags) == 0) {
            elm = elm->next;
            continue;
        }

        if(changedParams && request_firstParameter(elm->req)) {
            parameter_iterator_t* it = request_firstParameter(elm->req);
            bool found = false;
            while(it) {
                if(string_list_containsChar(changedParams, request_parameterName(it))) {
                    found = true;
                    break;
                }
                it = request_nextParameter(it);
            }
            if(!found) {
                elm = elm->next;
                continue;
            }
        }

        llist_t* temp = elm->req->notify_it.list;
        pcb_peer_data_t* pd = llist_item_data(temp, pcb_peer_data_t, notifyRequests);
        SAH_TRACEZ_INFO("pcb_rh", "Send object update to socket %d", peer_getFd(pd->peer));
        SAH_TRACEZ_INFO("pcb_rh", "    Request path      = [%s]", request_path(elm->req));
        SAH_TRACEZ_INFO("pcb_rh", "    Request depth     = [%d]", request_depth(elm->req));
        SAH_TRACEZ_INFO("pcb_rh", "    Destination PID   = [%d]", request_getPid(elm->req));
        pcb_replyBegin(pd->peer, elm->req, 0);
        pcb_writeObjectBegin(pd->peer, elm->req, object);
        default_replyParameters(pd->peer, object, elm->req);
        pcb_writeObjectEnd(pd->peer, elm->req);
        pcb_replyEnd(pd->peer, elm->req);
        SAH_TRACEZ_INFO("pcb_rh", "Flush peer");
        peer_flush(pd->peer);
        SAH_TRACEZ_INFO("pcb_rh", "Data available on peer after flush = %s", peer_needWrite(pd->peer) ? "Yes" : "No");
        if(peer_needWrite(pd->peer)) {
            /* REMARK:
               Workaround for radvision and voice application
               Just to make sure that the socket is added to the rad vision stack
             */
            connection_setEventsAvailable();
        }
        elm = elm->next;
    }
}

/**
   @brief
   Send a notification to the originator of the notify request.

   @details
   This function will send a notification to the originator of a notification request.
   This function is called from within the @ref local_object_commit_notify function.
 */
static void local_object_sendNotification(datamodel_t* dm, notification_t* notify, object_t* object, parameter_t* parameter, notify_list_t** notifyRequests, uint32_t depth, uint32_t flags) {
    notify_list_t* elm = *notifyRequests;

    if((parameter && !(parameter_attributes(parameter) & (parameter_attr_volatile | parameter_attr_volatile_handler))) || !parameter) {
        while(elm) {
            if(((request_depth(elm->req) + elm->notifyDepth) < depth) && (request_depth(elm->req) != UINT32_MAX)) {
                elm = elm->next;
                continue;
            }

            llist_t* temp = elm->req->notify_it.list;
            pcb_peer_data_t* pd = llist_item_data(temp, pcb_peer_data_t, notifyRequests);
            if(!pd) {
                SAH_TRACEZ_ERROR("pcb", "No peer data available");
                elm = elm->next;
                continue;
            }

            if((request_attributes(elm->req) & flags) == 0) {
                elm = elm->next;
                continue;
            }

            if(parameter) {
                SAH_TRACEZ_INFO("pcb", "Verify that the user id %d can read values", request_userID(elm->req));
                if(!parameter_canReadValue(parameter, request_userID(elm->req))) {
                    SAH_TRACEZ_NOTICE("pcb", "User id %d can not read values, do not send event", request_userID(elm->req));
                    elm = elm->next;
                    continue;
                }
            }

            if(parameter && request_firstParameter(elm->req)) {
                parameter_iterator_t* it = request_firstParameter(elm->req);
                bool found = false;
                while(it) {
                    if(strcmp(parameter_name(parameter), request_parameterName(it)) == 0) {
                        found = true;
                        break;
                    }
                    it = request_nextParameter(it);
                }
                if(!found) {
                    elm = elm->next;
                    continue;
                }
            }

            string_t path;
            string_initialize(&path, 64);
            uint32_t path_flags = request_attributes(elm->req) & ( request_common_path_key_notation | request_common_path_slash_seperator);

            if((object_attributes(object_root(object)) & object_attr_sysbus_root) == 0) {
                // This is not the sysbus root.
                path_flags = path_flags | path_attr_include_namespace;
            }
            object_path(object, &path, path_flags);
            notification_setObjectPath(notify, string_buffer(&path));
            string_cleanup(&path);
            SAH_TRACEZ_INFO("pcb_rh", "Send notification to socket %d", peer_getFd(pd->peer));
            SAH_TRACEZ_INFO("pcb_rh", "    Notification type = [%d]", notification_type(notify));
            SAH_TRACEZ_INFO("pcb_rh", "    Notification path = [%s]", notification_objectPath(notify));
            SAH_TRACEZ_INFO("pcb_rh", "    Destination PID   = [%d]", request_getPid(elm->req));
            pcb_replyBegin(pd->peer, elm->req, 0);
            pcb_writeNotification(pd->peer, elm->req, notify);
            pcb_replyEnd(pd->peer, elm->req);

            /* REMARK:
               Workaround for radvision and voice application
               Just to make sure that the socket is added to the rad vision stack
             */
            connection_setEventsAvailable();
            elm = elm->next;
        }
    }

    datamodel_notify_handlers_t* handler;
    llist_iterator_t* it = NULL;
    llist_for_each(it, &dm->notify) {
        handler = llist_item_data(it, datamodel_notify_handlers_t, it);
        handler->datamodel_changed(dm, object, parameter, notify);
    }
}

static void local_object_activateWaitingNotifyRequests(datamodel_t* dm) {
    object_t* root = datamodel_root(dm);
    request_t* notify_request = object_firstNotifyRequest(root);
    request_t* prefetch = object_nextNotifyRequest(root, notify_request);
    while(notify_request) {
        uint32_t attributes = request_attributes(notify_request);
        const char* path = request_path(notify_request);
        /* fetch the peer on which the request was received */
        pcb_peer_data_t* pd = llist_item_data(notify_request->notify_it.list, pcb_peer_data_t, notifyRequests);
        if(path && *path) {
            uint32_t localAttributes = attributes & (path_attr_key_notation | path_attr_slash_seperator);
            object_t* object = object_getObject(root, path, localAttributes, NULL);
            if(!object) {
                notify_request = prefetch;
                prefetch = object_nextNotifyRequest(root, notify_request);
                continue;
            }
            SAH_TRACE_INFO("Request with path was stored, handle now");
            notify_request->state = request_state_received;
            pcb_handleRequest(datamodel_pcb(dm), pd->peer, notify_request);
        }
        notify_request = prefetch;
        prefetch = object_nextNotifyRequest(root, notify_request);
    }
}

static void local_object_updateNotifyList(object_t* object, notify_list_t** notifyRequests, uint32_t currentDepth, uint32_t* removeNotifications) {
    llist_iterator_t* it = NULL;
    request_t* req = NULL;
    notify_list_t* elm = NULL;
    llist_for_each(it, &object->notifyRequests) {
        req = llist_item_data(it, request_t, it);
        if(!req->notify_it.list) {
            continue;
        }

        object_t* req_object = local_object_getObject(object_root(object), request_path(req), request_attributes(req), NULL);
        SAH_TRACEZ_INFO("pcb", "Root object - check request path");
        if(req_object != object) {
            SAH_TRACEZ_INFO("pcb", "request path and object do not match, skip");
            /* skip this one, the path is not empty */
            continue;
        }
        elm = (notify_list_t*) calloc(1, sizeof(notify_list_t));
        if(!elm) {
            SAH_TRACEZ_ERROR("pcb", "failed to allocate notify element");
            continue;
        }
        elm->req = req;
        elm->notifyDepth = currentDepth;
        elm->next = *notifyRequests;
        *notifyRequests = elm;
        (*removeNotifications)++;
    }
}

static void local_object_commit_upcDelete(object_t* object) {
    if(object_isInstance(object) && (object->state == object_state_deleted) && !object_isRemote(object)) {
        /* Add to list of deleted instances */
        int index = -1;
        object_t* templ_obj = object_parent(object);

        if(object->attributes & object_attr_persistent) {
            index = deleted_instance_persistent;

            if(object->attributes & object_attr_upc) {
                pcb_t* pcb = datamodel_pcb(object_datamodel(object));

                if(pcb && pcb->upcEnabled) {
                    if((object->attributes & object_attr_upc_changed) == 0) {
                        index = (object->attributes & object_attr_upc_usersetting) ? deleted_instance_usersetting : deleted_instance_upc;
                    }

                    /* Call UPC event callback */
                    if(pcb->upcEventCB) {
                        pcb->upcEventCB(upc_event_deleted, object, NULL, pcb->upcEventUserdata);
                    }
                }
            }
        }

        if(index != -1) {
            string_list_appendChar(
                &(templ_obj->object_info.template_info->deleted[index]),
                object_name(object, path_attr_key_notation)
                );
        }
    }
}

static void local_object_commit_upcCreate(object_t* object) {
    if(object_isInstance(object) && (object->state == object_state_created) && !object_isRemote(object)) {
        /* Remove from list of deleted instances */
        int i = 0;
        object_t* templ_obj = object_parent(object);

        for(i = 0; i < deleted_instance_max; i++) {
            string_list_iterator_t* iter = NULL;

            iter = string_list_findChar(&(templ_obj->object_info.template_info->deleted[i]), object_name(object, path_attr_key_notation));
            if(iter) {
                SAH_TRACEZ_INFO("pcb", "Removing key '%s' from deleted instances [index=%d]", object_name(object, path_attr_key_notation), i);

                string_list_iterator_destroy(iter);

                /* Make sure revived instance has the same attributes */
                switch(i) {
                case deleted_instance_persistent:
                    object_setPersistent(object, true);
                    break;
                case deleted_instance_upc:
                    object_setUPC(object, true);
                    break;
                case deleted_instance_usersetting:
                    object_setUserSetting(object, true);
                    break;
                default:
                    break;
                }
            }
        }

        /* Call UPC event callback */
        if(object->attributes & (object_attr_upc | object_attr_upc_usersetting)) {
            pcb_t* pcb = datamodel_pcb(object_datamodel(object));
            if(pcb && pcb->upcEnabled && (object->attributes & object_attr_upc_changed) && pcb->upcEventCB) {
                pcb->upcEventCB(upc_event_changed, object, NULL, pcb->upcEventUserdata);
            }
        }
    }
}

/**
   @brief
   Commit the object recursivly and send notification if needed.

   @details
   The notify request list can be modified here and extended

   An object is committed in the following order:
    - All changed parameters, this can result in value changed notifications.
    - All newly created child objects (including instances), this can result in object added notifications
    - All deleted child objects, including instances, (they will be deleted physically here), this can result in object deleted notifications

   The commit is done recursivly, starting from the object provided.

   If the object has an write handler (callback function), this will be called when the object has been changed.
   and is back in the read state.
   No write handler will be called for newly created objects.
 */
static bool local_object_commit_notify(datamodel_t* dm, object_t* object, notify_list_t** notifyRequests, uint32_t currentDepth) {
    notification_t* notification = NULL;
    uint32_t remove_notifications = 0;
    if(!object_isRemote(object)) {
        local_object_updateNotifyList(object, notifyRequests, currentDepth, &remove_notifications);
    }

    uint32_t notify = 0;

    /* Call UPC delete before commiting parameters */
    local_object_commit_upcDelete(object);

    string_list_t changedParams;
    string_list_initialize(&changedParams);
    /* commit parameters */
    if(object->state != object_state_ready) {
        /* we can not use for each here, the parameter list can get changed by commiting a parameter */
        parameter_t* parameter = object_firstParameter(object);
        parameter_t* prefetch;
        while(parameter) {
            // call the prewrite handler if the parameter value has changed
            if((parameter_state(parameter) == parameter_state_validated) ||
               (parameter_state(parameter) == parameter_state_modified) ||
               (parameter_state(parameter) == parameter_state_created) ||
               (parameter_state(parameter) == parameter_state_validate_created)) {
                if(parameter->definition->handlers && parameter->definition->handlers->prewrite) {
                    variant_t temp;
                    variant_initialize(&temp, variant_type_unknown);
                    uint32_t current = 0;
                    if((parameter_state(parameter) == parameter_state_created) ||
                       (parameter_state(parameter) == parameter_state_validate_created)) {
                        current = 0;
                    } else {
                        current = (parameter->currentConfig == 0) ? 1 : 0;
                    }
                    variant_copy(&temp, &parameter->value[current]);
                    parameter->definition->handlers->prewrite(parameter, &parameter->value[current]);
                    if(!parameter_validate(parameter)) {
                        SAH_TRACEZ_ERROR("pcb", "Value not accepted, validation failed (parameter = %s)", parameter_name(parameter));
                        variant_copy(&parameter->value[current], &temp);
                    }
                    variant_cleanup(&temp);
                }
            }
            /* changed parameters are in the validated state, commit can not fail here */
            if((parameter_state(parameter) == parameter_state_validated) && (object->state != object_state_created)) {
                notify |= request_notify_values_changed;
                notification = notification_createValueChanged(object, parameter);
                if(notification) {
                    local_object_sendNotification(dm, notification, object, parameter, notifyRequests, currentDepth, request_notify_values_changed);
                    notification_destroy(notification);
                }
                string_list_append(&changedParams, string_list_iterator_createFromChar(parameter_name(parameter)));
            }
            prefetch = object_nextParameter(parameter);
            parameter_commit(parameter);
            parameter = prefetch;
        }
    }

    object_t* child = NULL;
    object_t* prefetch = NULL;

    /* Created objects are comited from top to bottom
       This is the parent first then the children and instances.

       we do not check the state of the objects/parameters here that is done
       in theire commit functions
     */
    if(object->state == object_state_created) {
        /* if this object is in creation state and its parent is in creation state,
           it can not be comitted before it's parent is comitted
         */
        object_t* parent = local_object_parent(object);
        if(parent && (object_state(parent) == object_state_created)) {
            pcb_error = pcb_error_parent_not_committed;
            goto error;
        }

        local_object_commit_upcCreate(object);

        notify |= request_notify_object_added;
        object->state = object_state_ready;
        notification = notification_createObjectAdded(object);
        if(notification) {
            local_object_sendNotification(dm, notification, object, NULL, notifyRequests, currentDepth, request_notify_object_added);
            notification_destroy(notification);
        }

        if(object->handlers && object->handlers->write) {
            object->handlers->write(object);
        }

        /* Check for instance addition,
           and if the template has an automatic instance counter
         */
        if(object_isInstance(object) && object_hasInstanceCounter(parent)) {
            notification = notification_createValueChanged(local_object_parent(parent), object_getInstanceCounter(parent));
            if(notification) {
                local_object_sendNotification(dm, notification, local_object_parent(parent), object_getInstanceCounter(parent), notifyRequests, currentDepth, request_notify_values_changed);
                notification_destroy(notification);
            }
        }

        local_object_activateWaitingNotifyRequests(dm);
    }

    if(notify != 0) {
        local_object_sendUpdates(object, notifyRequests, currentDepth, notify, string_list_isEmpty(&changedParams) ? NULL : &changedParams);
    }
    string_list_cleanup(&changedParams);

    /* every child, instance, parameter and the object itself are already validated if we reach this code
       from now on the commits can not fail anymore, so no need to check this
     */

    /* we can not use for each here, the tree can get changed by commiting a child */
    child = object_firstChild(object);
    prefetch = object_nextSibling(child);
    /* commit children */
    while(child) {
        currentDepth++;
        local_object_commit_notify(dm, child, notifyRequests, currentDepth);
        currentDepth--;
        child = prefetch;
        prefetch = object_nextSibling(child);
    }

    /* commit instances */
    child = object_firstInstance(object);
    prefetch = object_nextInstance(child);
    while(child) {
        currentDepth++;
        local_object_commit_notify(dm, child, notifyRequests, currentDepth);
        currentDepth--;
        child = prefetch;
        prefetch = object_nextInstance(child);
    }

    object->attributes &= ~object_attr_subtree_changed;
    /* deleted and modified objects are committed from bottom to top
       this is of importance for deleted objects, do not delete a parent
       before its child
     */

    if(object->state != object_state_ready) {
        switch(object->state) {
        case object_state_validated:
            object->state = object_state_ready;
            if(object->handlers && object->handlers->write) {
                object->handlers->write(object);
            }
            break;
        case object_state_deleted:     /* delete the object */
            if(object_isRemote(object)) {
                SAH_TRACEZ_INFO("pcb", "destroy object %s", object_name(object, path_attr_key_notation));
                object_destroy(object);
            } else {
                notification = notification_createObjectDeleted(object);
                if(notification) {
                    SAH_TRACEZ_INFO("pcb", "sending delete notification");
                    local_object_sendNotification(dm, notification, object, NULL, notifyRequests, currentDepth, request_notify_object_deleted);
                    notification_destroy(notification);
                }

                /* Check for instance deletion,
                   and if the template has an automatic instance counter
                 */
                bool send_notification = false;
                object_t* parent = local_object_parent(object);
                if(object_isInstance(object) && object_hasInstanceCounter(parent)) {
                    send_notification = true;
                }
                object_destroy(object);
                if(send_notification == true) {
                    notification = notification_createValueChanged(local_object_parent(parent), object_getInstanceCounter(parent));
                    if(notification) {
                        local_object_sendNotification(dm, notification, local_object_parent(parent), object_getInstanceCounter(parent), notifyRequests, currentDepth, request_notify_values_changed);
                        notification_destroy(notification);
                    }
                }
            }
            break;
        default:
            break;
        }
    }

    while(remove_notifications) {
        notify_list_t* elm = *notifyRequests;
        *notifyRequests = elm->next;
        free(elm);
        remove_notifications--;
    }
    return true;

error:
    while(remove_notifications) {
        notify_list_t* elm = *notifyRequests;
        *notifyRequests = elm->next;
        free(elm);
        remove_notifications--;
    }
    return false;
}

/**
   @brief
   Delete an object and all its children and instances

   @details
   This function does not delete the objects, just marks them as deleted.

   If the object has a delete handler (callback function) this is called after all children and instances
   and the object itself are marked for deletion.
 */
bool local_object_delete(object_t* object) {
    parameter_t* parameter = NULL;
    char** mibName = NULL;
    object_t* child = NULL;

    bool retVal = true;

    if((object->attributes & (object_attr_upc | object_attr_upc_overwrite)) == (object_attr_upc | object_attr_upc_overwrite)) {
        pcb_t* pcb = datamodel_pcb(object_datamodel(object));
        if(pcb && pcb->upcOverwrite) {
            SAH_TRACEZ_NOTICE("pcb", "Not deleting UPC Overwrite protected object %s", object_name(object, path_attr_key_notation));
            return true;
        }
    }

    if(object->attributes & object_attr_instance) {
        object_t* parent = local_object_parent(object);
        if(parent->handlers && parent->handlers->instance_del && !parent->handlers->instance_del(parent, object)) {
            SAH_TRACEZ_ERROR("pcb", "Instance Delete Handler failed for object %s (template = %s)", object_name(object, path_attr_key_notation), object_name(parent, path_attr_key_notation));
            return false;
        }
    }

    /* mark this object as deleted */
    object->state = object_state_deleted;

    /* dereference all mibs */
    mibName = string_list_firstChar(&object->mibs);
    while(mibName) {
        object_removeMib(object, *mibName);
        mibName = string_list_firstChar(&object->mibs);
    }
    string_list_clear(&object->mibs);

    /* try to delete all children */
    object_for_each_child(child, object) {
        retVal = local_object_delete(child);
        if(!retVal) {
            SAH_TRACEZ_ERROR("pcb", "Delete failed for object %s", object_name(child, 0));
            break;
        }
    }
    /* it has failed, the child that failed to be deleted is
       set in the child pointer
     */
    if(!retVal) {
        goto error_rollback_children;
    }

    /* try to delete all instances
       do not need to check if the object is an template, if it is not it will not contain any instances
     */
    object_for_each_instance(child, object) {
        retVal = local_object_delete(child);
        if(!retVal) {
            SAH_TRACEZ_ERROR("pcb", "Delete failed for object %s", object_name(child, 0));
            break;
        }
    }
    /* it has failed, the instance that failed to be deleted is
       set in the child pointer
     */
    if(!retVal) {
        goto error_rollback_instances;
    }

    object_for_each_parameter(parameter, object) {
        retVal = parameter_delete(parameter);
        if(!retVal) {
            SAH_TRACEZ_ERROR("pcb", "Delete failed for parameter %s", parameter_name(parameter));
            break;
        }
    }
    /* if it has failed, the parameter that failed to be deleted is
       set in the parameter pointer
     */
    if(!retVal) {
        goto error_rollback_parameters;
    }

    if(object->handlers && object->handlers->del && !object->handlers->del(object)) {
        SAH_TRACEZ_ERROR("pcb", "Delete Handler failed for object %s", object_name(object, 0));
        goto error_rollback_parameters;
    }

    object_setParentTreeModified(object);

    pcb_error = pcb_ok;
    return true;

error_rollback_parameters:
    parameter = object_prevParameter(parameter);
    while(parameter) {
        parameter_rollback(parameter);
        parameter = object_prevParameter(parameter);
    }
    /* rollback all instances */
    object_for_each_instance(child, object) {
        local_object_rollback(child);
    }
    object->state = object_state_ready;
    /* rollback all children */
    object_for_each_child(child, object) {
        local_object_rollback(child);
    }
    object->state = object_state_ready;
    return false;

error_rollback_instances:
    /* rollback all previous deleted instances */
    child = object_prevInstance(child);
    while(child) {
        local_object_rollback(child);
        child = object_prevInstance(child);
    }
    /* rollback all children */
    object_for_each_child(child, object) {
        local_object_rollback(child);
    }
    object->state = object_state_ready;
    return false;

error_rollback_children:
    /* rollback all previous deleted children */
    child = object_prevSibling(child);
    while(child) {
        local_object_rollback(child);
        child = object_prevSibling(child);
    }

    object->state = object_state_ready;
    return false;
}

object_t* local_object_parent(const object_t* object) {
    if(object_isInstance(object) && object->object_info.instance_info.instance_of) {
        return object->object_info.instance_info.instance_of;
    } else {
        tree_item_t* item = tree_item_parent(&object->tree_item);
        if(!item) {
            return NULL;
        }

        return tree_item_data(item, object_t, tree_item);
    }
}

object_t* local_object_getObject(object_t* object, const char* path, const uint32_t pathAttr, bool* exactMatch) {
    object_t* foundObject = NULL;
    string_t path_string;
    string_initialize(&path_string, 0);
    string_fromChar(&path_string, path);
    string_list_t pathList;
    string_list_initialize(&pathList);

    char seperator[2] = ".";
    if(pathAttr & path_attr_slash_seperator) {
        strncpy(seperator, "/", 2);
    }
    if(string_list_split(&pathList, &path_string, seperator, SkipEmptyParts, string_case_sensitive) != 0) {
        foundObject = local_object_find(object, &pathList, pathAttr, exactMatch);
    }

    string_list_cleanup(&pathList);
    string_cleanup(&path_string);
    pcb_error = pcb_ok;
    return foundObject;
}

bool local_object_hasChildren(const object_t* object) {
    pcb_error = pcb_ok;
    return tree_item_hasChildren(&object->tree_item);
}

uint32_t local_object_childCount(object_t* object) {
    pcb_error = pcb_ok;
    return tree_item_childCount(&object->tree_item);
}

void local_object_path(const object_t* object, string_t* path, const uint32_t pathAttributes) {
    object_t* parent = object_parent(object);

    /* An object has a parent or is an instance object
       if it has no parent and it is not an instance object it is the root object
     */
    if(parent) {
        local_object_path(parent, path, pathAttributes);
    } else {
        if((pathAttributes & path_attr_include_namespace) == 0) {
            return;
        }
    }

    if(string_length(path)) {
        if(pathAttributes & path_attr_slash_seperator) {
            string_appendChar(path, "/");
        } else {
            string_appendChar(path, ".");
        }
    }

    // Back up pcb_error, because most objects don't have the "Alias" parameter
    // and we don't want it to pollute the result of this function.
    uint32_t pcb_error_bu = pcb_error;
    const char* alias = object_da_parameterCharValue(object, "Alias");
    pcb_error = pcb_error_bu;
    if(alias && *alias && object_isInstance(object)) {
        if(!(pathAttributes & path_attr_key_notation) && (pathAttributes & path_attr_alias)) {
            string_appendChar(path, "[");
            string_appendChar(path, object_name(object, pathAttributes));
            string_appendChar(path, "]");
        } else {
            string_appendChar(path, object_name(object, pathAttributes));
        }
    } else {
        string_appendChar(path, object_name(object, pathAttributes));
    }
}

object_t* local_object_createInstance(object_t* object, uint32_t index, const char* key) {
    template_info_t* info = object->object_info.template_info;
    uint32_t error = pcb_ok;

    if(object->attributes & object_attr_mib) {
        pcb_error = pcb_error_object_ismib;
        return NULL;
    }

    /* check the max instances */
    if(info->instanceCount >= info->max_instances) {
        SAH_TRACEZ_WARNING("pcb", "Maximum instances reached");
        pcb_error = pcb_error_max_instances_reached;
        return NULL;
    }

    object_t* instance = (object_t*) calloc(1, sizeof(object_t));
    if(!instance) {
        error = pcb_error_no_memory;
        goto error;
    }

    /* set the flags before initialization
       same flags as object but
       no template flag
       add instance flag
     */
    instance->attributes = (object->attributes | object_attr_instance);
    instance->attributes &= ~(object_attr_template | object_attr_read_only); /* remove template attribute and read-only attribute */
    if(object->attributes & (object_attr_upc | object_attr_upc_usersetting)) {
        pcb_t* pcb = datamodel_pcb(object_datamodel(object));
        if(pcb && pcb->upcEnabled) {
            instance->attributes |= object_attr_upc_changed;
        }
    }
    /* set the state to created */
    instance->state = object_state_created;

    /* initialize the object structure */
    if(!object_initialize(instance)) {
        SAH_TRACEZ_ERROR("pcb", "Failed to initialize instance");
        goto error_free;
    }

    /* set the name, this name is a number only name */
    if((index != UINT32_MAX) && (index != 0)) {
        instance->oname = calloc(1, 15);
        snprintf(instance->oname, 14, "%u", index);
        /* indexes must be unique at all times, even for instances in created state */
        if(local_object_getObject(object, instance->oname, 0, NULL) != NULL) {
            SAH_TRACEZ_ERROR("pcb", "Specified index is not unique");
            error = pcb_error_not_unique_name;
            goto error_cleanup;
        }
        if(index > info->last_used_index) {
            info->last_used_index = index;
        }

    } else {
        instance->oname = calloc(1, 15);
        info->last_used_index++;
        snprintf(instance->oname, 14, "%u", info->last_used_index);
    }
    if(key && (*key)) {
        /* check that this key is unique within the template object */
        if(local_object_getObject(object, key, path_attr_key_notation, NULL) != NULL) {
            SAH_TRACEZ_NOTICE("pcb", "Specified key [%s] is not unique", key);
            error = pcb_error_not_unique_name;
            goto error_cleanup;
        }
        string_fromChar(&instance->object_info.instance_info.key, key);
    }

    /* set the instance of */
    instance->object_info.instance_info.instance_of = object;

    /* add to the instances list */
    llist_append(&info->instances, &instance->instance_it);
    info->instanceCount++;

    /* Set instance counter */
    if(info->instanceCounter != NULL) {
        variant_t var;
        parameter_t* counter = info->instanceCounter;

        variant_initialize(&var, variant_type_uint32);
        variant_setUInt32(&var, info->instanceCount);
        parameter_setValue(counter, &var);
        variant_cleanup(&var);
    }

    /* make a copy of the template object */
    if(!local_object_copy(instance, object)) {
        SAH_TRACEZ_ERROR("pcb", "Failed to initialize instance");
        goto error_rollback;
    }

    object_setParentTreeModified(object);

    if(object->handlers && object->handlers->instance_add && !object->handlers->instance_add(object, instance)) {
        SAH_TRACEZ_ERROR("pcb", "add instance handler returned false");
        error = pcb_error;
        goto error_rollback;
    }
    SAH_TRACEZ_INFO("pcb", "instance added");

    return instance;

error_rollback:
    object_rollback(object);
    pcb_error = error;
    return NULL;
error_cleanup:
    object_cleanup(instance);
error_free:
    free(instance);
error:
    pcb_error = error;
    SAH_TRACEZ_NOTICE("pcb", "add instance failed");
    return NULL;
}

bool local_object_hasInstances(const object_t* object) {
    return llist_isEmpty(&object->object_info.template_info->instances) ? false : true;
}

uint32_t local_object_instanceCount(object_t* object) {
    return object->object_info.template_info->instanceCount;
}

parameter_t* local_object_getParameter(const object_t* object, const char* parameterName) {
    parameter_t* parameter = NULL;
    object_for_each_parameter(parameter, object) {
        const char* name = parameter_name(parameter);
        if(name && (strcmp(name, parameterName) == 0)) {
            return parameter;
        }
    }

    pcb_error = pcb_error_not_found;
    return NULL;
}

bool local_object_hasParameters(const object_t* object) {
    return !llist_isEmpty(&object->parameters);
}

uint32_t local_object_parameterCount(object_t* object) {
    return llist_size(&object->parameters);
}

function_t* local_object_getFunction(object_t* object, const char* functionName) {
    function_t* function = NULL;

    object_for_each_function(function, object) {
        if(strcmp(function_name(function), functionName) == 0) {
            return function;
        }
    }

    pcb_error = pcb_error_not_found;
    return NULL;
}

bool local_object_hasFunctions(const object_t* object) {
    return !llist_isEmpty(&object->functions);
}

uint32_t local_object_functionCount(object_t* object) {
    return llist_size(&object->functions);
}

bool local_object_update(object_t* object, llist_t* parameters) {
    SAH_TRACEZ_IN("pcb");

    if(object->handlers && object->handlers->read && !object->handlers->read(object)) {
        local_object_rollback(object);
        SAH_TRACEZ_OUT("pcb");
        return false;
    }

    /* call for each parameter the update function */
    if(!parameters || llist_isEmpty(parameters)) {
        parameter_t* parameter = NULL;
        object_for_each_parameter(parameter, object) {
            if(!parameter_update(parameter)) {
                SAH_TRACEZ_OUT("pcb");
                return false;
            }
        }
    } else {
        param_value_t* param = NULL;
        parameter_t* parameter = NULL;
        llist_iterator_t* it = NULL;
        llist_for_each(it, parameters) {
            param = llist_item_data(it, param_value_t, it);
            parameter = object_getParameter(object, param->parameterName);
            if(!parameter) {
                continue;
            }
            if(!parameter_update(parameter)) {
                SAH_TRACEZ_OUT("pcb");
                return false;
            }
        }
    }

    if(object->state != object_state_ready) {
        if(!local_object_validate(object)) {
            local_object_rollback(object);
            SAH_TRACEZ_OUT("pcb");
            return false;
        } else {
            local_object_commit(object);
        }
    }

    SAH_TRACEZ_OUT("pcb");
    return true;
}

bool local_object_commit(object_t* object) {
    uint32_t depth = 1;
    if(!local_object_validate(object)) {
        return false;
    }

    datamodel_t* dm = object_datamodel(object);
    notify_list_t* notifyRequests = NULL;

    object_t* templ_obj = NULL;
    if(object_isInstance(object)) {
        templ_obj = object_parent(object);
    } else if(object_isTemplate(object)) {
        templ_obj = object;
    }
    if(templ_obj) {
        if(object_hasInstanceCounter(templ_obj)) {
            parameter_t* counter = object_getInstanceCounter(templ_obj);
            if(parameter_state(counter) != parameter_state_ready) {
                parameter_commit(counter);
            }
            /* tweak: notify of 'auto count' is at instance level (object_added/deleted) while the counter is at instance-1 level */
            depth--;
        }
    }

    /* fetch the all notify requests for this object */
    object_buildNotifyList(local_object_parent(object), &notifyRequests, depth);

    /* commit the objects recursvie and send notifications and updates if needed */
    bool retval = local_object_commit_notify(dm, object, &notifyRequests, 0);

    /* clear the notify request list */
    notify_list_t* elm = notifyRequests;
    notify_list_t* next = NULL;
    while(elm) {
        next = elm->next;
        free(elm);
        elm = next;
    }

    return retval;
}

void local_object_rollback(object_t* object) {
    /* rollback is done from bottom to top
       it really does not matter in which order objects are rollbacked
       rollback will not emit any signals
       also rollback will not fail
       we do not check the state of the objects/parameters here that is done
       in theire rollback functions
     */
    /* rollback each instance */
    object_t* child = object_firstInstance(object);
    object_t* prefetch = object_nextInstance(child);
    while(child) {
        local_object_rollback(child);
        child = prefetch;
        prefetch = object_nextInstance(child);
    }

    /* we can not use for each here, the tree can get changed by rollbacking a child */
    child = object_firstChild(object);
    prefetch = object_nextSibling(child);
    /* rollback children */
    while(child) {
        local_object_rollback(child);
        child = prefetch;
        prefetch = object_nextSibling(child);
    }

    /* rollback parameters */
    if(object->state != object_state_ready) {
        parameter_t* parameter = object_firstParameter(object);
        parameter_t* param_prefetch = object_nextParameter(parameter);
        while(parameter) {
            parameter_rollback(parameter);
            parameter = param_prefetch;
            param_prefetch = object_nextParameter(parameter);
        }
        object->attributes &= ~object_attr_subtree_changed;

        /* rollback the object itself */
        switch(object->state) {
        case object_state_created:     /* delete the object */
            object_destroy(object);
            break;
        case object_state_modified:    /* revert the parameters */
        case object_state_validated:   /* revert the parameters */
        case object_state_deleted:     /* undelete the object */
            object->state = object_state_ready;
            break;
        default:
            break;
        }
    }
}

bool local_object_validate(object_t* object) {
    bool retval = true;
    object_t* child = NULL;

    /* validate the parameters */
    parameter_t* parameter = NULL;
    object_for_each_parameter(parameter, object) {
        retval = parameter_validate(parameter);
        if(!retval) {
            pcb_error = pcb_error_invalid_value;
            goto leave;
        }
    }

    /* validate the children if needed */
    object_for_each_child(child, object) {
        retval = local_object_validate(child);
        if(!retval) {
            pcb_error = pcb_error_invalid_value;
            goto leave;
        }
    }

    /* validate the instances if needed */
    object_for_each_instance(child, object) {
        retval = local_object_validate(child);
        if(!retval) {
            pcb_error = pcb_error_invalid_value;
            goto leave;
        }
    }

    /* validate the object itself */
    if(object->validator && object->validator->validate &&
       ((object->state == object_state_modified) || (object->state == object_state_created))) {
        retval = object->validator->validate(object, object->validator->data);
        if(!retval) {
            pcb_error = pcb_error_invalid_value;
            goto leave;
        }
    }

    /* only change the state for modified objects, created objects stay in created state */
    if(object->state == object_state_modified) {
        object->state = object_state_validated;
    }

leave:
    return retval;
}

bool local_object_addMib(object_t* object, const char* mibName) {
    if(string_list_containsChar(&object->mibs, mibName)) {
        SAH_TRACEZ_NOTICE("pcb", "Mib %s already added", mibName);
        /* mib already added, just return */
        return true;
    }

    if(object_isTemplate(object)) {
        SAH_TRACEZ_ERROR("pcb", "MIB can not be added to template object %s", object_name(object, 0));
        pcb_error = pcb_error_not_instance;
        return false;
    }

    object_t* mib = datamodel_mib(object_datamodel(object), mibName);

    if(!mib) {
        SAH_TRACEZ_INFO("pcb", "Mib %s not available, loading ...", mibName);
        if(!datamodel_mibLoad(object_datamodel(object), mibName, NULL)) {
            SAH_TRACEZ_INFO("pcb", "failed to load mib %s", mibName);
            return NULL;
        }
        mib = datamodel_mib(object_datamodel(object), mibName);
    }

    if(!mib) {
        SAH_TRACEZ_INFO("pcb", "mib not found");
        pcb_error = pcb_error_not_found;
        return false;
    }

    if(!local_object_copy(object, mib)) {
        SAH_TRACEZ_INFO("pcb", "Failed to copy mib in object");
        object_removeMib(object, mibName);
        return false;
    }

    string_list_appendChar(&object->mibs, mibName);
    SAH_TRACEZ_INFO("pcb", "Mib %s added", mibName);

    /* call calback function if available */
    if(object->handlers && object->handlers->mib_added) {
        SAH_TRACEZ_INFO("pcb", "Calling mib added handler ...");
        object->handlers->mib_added(object, mib);
        SAH_TRACEZ_INFO("pcb", "Mib added handler done");
    }

    return true;
}
