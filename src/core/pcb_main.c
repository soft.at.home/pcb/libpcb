/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#define _GNU_SOURCE
#include <stdlib.h>
#include <errno.h>
#include <signal.h>
#include <unistd.h>
#include <sys/time.h>
#include <pwd.h>

#include <components.h>

#include <debug/sahtrace.h>

#ifdef CONFIG_PCB_ACL_USERMNGT
#include <usermngt/usermngt.h>
#endif

#include <pcb/common/error.h>
#include <pcb/utils/string_list.h>
#include <pcb/pcb_client.h>
#include "pcb_main_priv.h"
#include "reply_priv.h"
#include "requesthandlers_priv.h"
#include "notification_priv.h"
#include "remote_object.h"
#include "object_priv.h"

typedef struct _notify_handler {
    llist_iterator_t it;
    notify_handler_t notifyHandler;
} notify_handler_iterator_t;

/**
   @file
   @brief

   @todo
 */

/**
   @brief
   Instance of request_handlers_t structure

   @details
   This instance contains the default implementation of the request handlers
 */
static request_handlers_t defaultRequestHandlers = {
    .getObject = default_getObjectHandler,
    .setObject = default_setObjectHandler,
    .createInstance = default_createInstanceHandler,
    .deleteInstance = default_deleteInstanceHandler,
    .findObjects = default_findObjectsHandler,
    .executeFunction = default_executeFunctionHandler,
    .closeRequest = default_closeRequestHandler,
    .notify = NULL,
    .openSession = default_openSessionRequestHandler,
};

static bool pcb_acceptRequest(peer_info_t* peer, pcb_peer_data_t* pd, request_t* req) {
    SAH_TRACEZ_INFO("pcb", "Security check");
    bool acceptRequest = false;

    if(!pd->trusted) {
        /* authentication will be done in the open session handler */
        if(request_type(req) == request_type_open_session) {
            acceptRequest = true;
            SAH_TRACEZ_INFO("pcb", "Open session request");
        } else {
            /* an open session exists */
            if(pd->UID != UINT32_MAX) {
                request_setUserID(req, pd->UID);
                acceptRequest = true;
                SAH_TRACEZ_INFO("pcb", "Peer has user information");
            }
        }
#ifdef CONFIG_PCB_ALLOW_ANONYMOUS_TCP
        /* user id is filled in
           REMARK: This has to be removed in later versions, security hole
                   We are trusting the sender of the request (can only be done on IPC sockets)
         */
        SAH_TRACEZ_NOTICE("pcb", "Allowing anonymous is set in buildconfig: accept request");
        acceptRequest = true;
#endif

        if(!acceptRequest) {
            SAH_TRACEZ_ERROR("pcb", "Permission denied");
            pcb_replyBegin(peer, req, EACCES);
            pcb_writeError(peer, req, EACCES, error_string(EACCES), "");
            pcb_replyEnd(peer, req);
            pcb_error = EACCES;
            return false;
        }

        /* set the tcp flag to enforce acl checking */
        request_setAttributes(req, request_attributes(req) | request_received_over_tcp);
        SAH_TRACEZ_INFO("pcb", "Security check OK");
    } else {
        acceptRequest = true;
        SAH_TRACEZ_INFO("pcb", "Request accepted user id = %d", request_userID(req));
        /* if an open session exists, use the user id of that session */
        if((pd->UID != UINT32_MAX) && (request_userID(req) == UINT32_MAX)) {
            if(request_attributes(req) & request_received_over_tcp) {
                SAH_TRACEZ_INFO("pcb", "TCP flag set, original source not trusted");
            } else {
                SAH_TRACEZ_INFO("pcb", "Set user id of request to %d", pd->UID);
                request_setUserID(req, pd->UID);
            }
        }
        /* if the request is anonymous and not originating from a tcp connection
           set the user id to 0, to grant full access
         */
        if((request_userID(req) == UINT32_MAX) && !(request_attributes(req) & request_received_over_tcp)) {
            SAH_TRACEZ_NOTICE("pcb", "Accept anonymous IPC request - switch to UID 0 - full access granted");
            request_setUserID(req, 0);
        }
        SAH_TRACEZ_INFO("pcb", "Request user ID = %d", request_userID(req));
    }

    return acceptRequest;
}

/**
   @brief
   Handle an incoming peer request

   @details
   This function will handle an incoming peer request.\n
   Depending on the request type @ref request_type_t, the corresponding peer request handlers are called.\n

   @param peer The peer information
   @param connection The connection information
   @param req The incoming request

   @return
    - true if the request handling function returns true
    - false if an error occurred, pcb_error contains more info about the error
        - if no valid corresponding request handler has been found
        - if the request handling function returns false
        - if an invalid request was received
 */
bool pcb_handleRequest(pcb_t* pcb, peer_info_t* peer, request_t* req) {
    pcb_error = pcb_ok;
    bool retval = true;

    /* request is being handled
       currently: can only be a exec function request
       other request, should not be in this state when entering this function
     */
    if(req->state == request_state_handling) {
        return true;
    }

    if(req->state == request_state_received) {
        pcb_peer_data_t* pd = (pcb_peer_data_t*) peer_getUserData(peer);
        /* new request
           When the peer is connected using a not trusted socket one of the following conditions must be met:
                - the request must be an open session request
                - the request must contain an user id and/or group id
                - an open session must exist on the peer
         */
        if(!pcb_acceptRequest(peer, pd, req)) {
            return false;
        }
    }

    /* set the request state to handling */
    req->state = request_state_handling;

    switch(req->type) {
    case request_type_get_object:
        SAH_TRACEZ_INFO("pcb", "Handle \"get object\" request");
        if(pcb->reqhandlers && pcb->reqhandlers->getObject) {
            datamodel_t* dm = pcb_datamodel(pcb);
            retval = pcb->reqhandlers->getObject(peer, dm, req);
        } else {
            pcb_replyBegin(peer, req, pcb_error_not_supported_request);
            pcb_writeError(peer, req, pcb_error_not_supported_request, error_string(pcb_error_not_supported_request), "");
            pcb_replyEnd(peer, req);
            pcb_error = pcb_error_not_supported_request;
            req->state = request_state_done;
            retval = false;
        }
        break;
    case request_type_set_object:
        SAH_TRACEZ_INFO("pcb", "Handle \"set object\" request");
        if(pcb->reqhandlers && pcb->reqhandlers->setObject) {
            datamodel_t* dm = pcb_datamodel(pcb);
            retval = pcb->reqhandlers->setObject(peer, dm, req);
        } else {
            pcb_replyBegin(peer, req, pcb_error_not_supported_request);
            pcb_writeError(peer, req, pcb_error_not_supported_request, error_string(pcb_error_not_supported_request), "");
            pcb_replyEnd(peer, req);
            pcb_error = pcb_error_not_supported_request;
            req->state = request_state_done;
            retval = false;
        }
        break;
    case request_type_create_instance:
        SAH_TRACEZ_INFO("pcb", "Handle \"create instance\" request");
        if(pcb->reqhandlers && pcb->reqhandlers->createInstance) {
            datamodel_t* dm = pcb_datamodel(pcb);
            retval = pcb->reqhandlers->createInstance(peer, dm, req);
        } else {
            pcb_replyBegin(peer, req, pcb_error_not_supported_request);
            pcb_writeError(peer, req, pcb_error_not_supported_request, error_string(pcb_error_not_supported_request), "");
            pcb_replyEnd(peer, req);
            pcb_error = pcb_error_not_supported_request;
            req->state = request_state_done;
            retval = false;
        }
        break;
    case request_type_delete_instance:
        SAH_TRACEZ_INFO("pcb", "Handle \"delete instance\" request");
        if(pcb->reqhandlers && pcb->reqhandlers->deleteInstance) {
            datamodel_t* dm = pcb_datamodel(pcb);
            retval = pcb->reqhandlers->deleteInstance(peer, dm, req);
        } else {
            pcb_replyBegin(peer, req, pcb_error_not_supported_request);
            pcb_writeError(peer, req, pcb_error_not_supported_request, error_string(pcb_error_not_supported_request), "");
            pcb_replyEnd(peer, req);
            pcb_error = pcb_error_not_supported_request;
            req->state = request_state_done;
            retval = false;
        }
        break;
    case request_type_find_objects:
        SAH_TRACEZ_INFO("pcb", "Handle \"find objects\" request");
        if(pcb->reqhandlers && pcb->reqhandlers->findObjects) {
            datamodel_t* dm = pcb_datamodel(pcb);
            retval = pcb->reqhandlers->findObjects(peer, dm, req);
        } else {
            pcb_replyBegin(peer, req, pcb_error_not_supported_request);
            pcb_writeError(peer, req, pcb_error_not_supported_request, error_string(pcb_error_not_supported_request), "");
            pcb_replyEnd(peer, req);
            pcb_error = pcb_error_not_supported_request;
            req->state = request_state_done;
            retval = false;
        }
        break;
    case request_type_exec_function:
        SAH_TRACEZ_INFO("pcb", "Handle \"execute function request\" request");
        if(pcb->reqhandlers && pcb->reqhandlers->executeFunction) {
            datamodel_t* dm = pcb_datamodel(pcb);
            retval = pcb->reqhandlers->executeFunction(peer, dm, req);

            if(!retval) {

                if(req->state == request_state_handling) {
                    /**
                     * It was not possible to execute the function. The request cannot be handled further.
                     * Therefore, change the state to done. This request must be deleted via request_destroy.
                     */
                    req->state = request_state_done;
                }
            }
        } else {
            pcb_replyBegin(peer, req, pcb_error_not_supported_request);
            pcb_writeError(peer, req, pcb_error_not_supported_request, error_string(pcb_error_not_supported_request), "");
            pcb_replyEnd(peer, req);
            pcb_error = pcb_error_not_supported_request;
            req->state = request_state_done;
            retval = false;
        }
        break;
    case request_type_open_session:
        SAH_TRACEZ_INFO("pcb", "Handle \"open session\" request");
        if(pcb->reqhandlers && pcb->reqhandlers->openSession) {
            retval = pcb->reqhandlers->openSession(peer, req);
        } else {
            pcb_replyBegin(peer, req, pcb_error_not_supported_request);
            pcb_writeError(peer, req, pcb_error_not_supported_request, error_string(pcb_error_not_supported_request), "");
            pcb_replyEnd(peer, req);
            pcb_error = pcb_error_not_supported_request;
            req->state = request_state_done;
            retval = false;
        }
        break;
    case request_type_close_request:
        SAH_TRACEZ_INFO("pcb", "Handle \"close request\" request");
        if(pcb->reqhandlers && pcb->reqhandlers->closeRequest) {
            retval = pcb->reqhandlers->closeRequest(peer, req);
        } else {
            pcb_replyBegin(peer, req, pcb_error_not_supported_request);
            pcb_writeError(peer, req, pcb_error_not_supported_request, error_string(pcb_error_not_supported_request), "");
            pcb_replyEnd(peer, req);
            pcb_error = pcb_error_not_supported_request;
            req->state = request_state_done;
            retval = false;
        }
        break;
    default:
        SAH_TRACEZ_INFO("pcb", "Handle \"unknown request\" request");
        pcb_replyBegin(peer, req, pcb_error_invalid_request);
        pcb_writeError(peer, req, pcb_error_invalid_request, error_string(pcb_error_invalid_request), "");
        pcb_replyEnd(peer, req);
        pcb_error = pcb_error_invalid_request;
        req->state = request_state_done;
        retval = false;
        break;
    }
    if(!peer_isConnected(peer)) {
        /* connection closed unexpected, return false */
        return false;
    }
    return retval;
}

static void pcb_request_replyClear(request_t* req) {
    /* clear the requests datamodel if this is a not cached object request */
    if(!request_useObjectCache(req)) {
        SAH_TRACEZ_INFO("pcb", "Clear request cache");
        request_object_t* object_req = llist_item_data(req, request_object_t, request);
        datamodel_clear(object_req->datamodel);
    }

    /* the reply is handled, so clear the reply list */
    reply_clear(&req->reply);
}

static void pcb_request_replyRemove(request_t* req) {
    reply_t* reply = &req->reply;
    reply_item_t* item = NULL;
    llist_iterator_t* it = NULL;
    llist_for_each(it, &reply->items) {
        item = llist_item_data(it, reply_item_t, it);
        llist_iterator_take(&item->reply_it);
    }
}

static bool pcb_handleRequestDone(pcb_t* pcb, peer_info_t* peer, request_t* req, bool force) {
    bool retval = true;

    switch(req->state) {
    case request_state_sent:
    case request_state_waiting_for_reply:
    case request_state_handling_reply:
        break;
    default:
        return retval;
        break;
    }

    pcb_error = pcb_ok;
    reply_t* reply = request_reply(req);

    /* remove request when:
          reply is complete or reply handler failed and it is not a notify request
     */
    if(reply->completed || force) {
        /* remove request from pending requests list if not a notify request */
        if((request_attributes(req) & request_notify_all) == 0) {
            req->state = request_state_done;
            if(req->doneHandler) {
                SAH_TRACEZ_INFO("pcb_reply", "Calling done handler");
                retval = req->doneHandler(req, pcb, peer, req->userdata);
                if(retval) {
                    pcb_request_replyClear(req);
                }
            }
        } else {
            tree_item_t* parent = tree_item_parent(&req->child_requests);
            if(parent && req->doneHandler) {
                SAH_TRACEZ_INFO("pcb_reply", "Calling done handler - child request");
                req->doneHandler(req, pcb, peer, req->userdata);
            }
            /* reset completed flag */
            if(!force) {
                reply->completed = false;
            }
            /* clear the requests datamodel if this is a not cached object request */
            pcb_request_replyClear(req);
        }
        /* completion is forced, set the reply as completed. */
        if(force) {
            SAH_TRACEZ_INFO("pcb_reply", "Force is set");
            req->state = request_state_done;
            reply->completed = true;
        }
    } else {
        if(llist_isEmpty(&reply->items)) {
            /* clear the reply data and data model */
            pcb_request_replyClear(req);
        }
    }

    return retval;
}

/**
   @brief
   Construct a reply for an incoming peer request

   @details

   @param peer The peer information
   @param connection The connection information
   @param req The incoming request

   @return
    - true if the request handling function returns true
    - false if an error occurred, pcb_error contains more info about the error
 */
static bool pcb_handleReply(pcb_t* pcb, peer_info_t* peer, request_t* req) {
    /* NOTES:

       When no reply handler is available, keep all items in the list
       and objects in the request object cache.

       When a reply handler is available, we can remove the items from the list,
       assume they are handled in the reply handler,
       Also the objects stored in the request cache can be removed
     */
    bool retval = true;
    pcb_error = pcb_ok;
    reply_t* reply = request_reply(req);

    if(!llist_isEmpty(&reply->items)) {
        if(req->replyHandler) {
            SAH_TRACEZ_INFO("pcb_reply", "Calling reply handler");
            retval = req->replyHandler(req, pcb, peer, req->userdata);
            if(peer_isConnected(peer)) {
                /* clear the reply items */
                pcb_request_replyClear(req);
            }
        } else {
            /* remove reply items from reply item list of peer */
            SAH_TRACEZ_INFO("pcb_reply", "Remove reply items from reply list");
            pcb_request_replyRemove(req);
        }
    }

    SAH_TRACEZ_INFO("pcb_reply", "Verify done ...");
    if(peer_isConnected(peer)) {
        retval = pcb_handleRequestDone(pcb, peer, req, retval ? false : true);
    }

    return retval;
}

static bool pcb_handleReplyItem(pcb_t* pcb, peer_info_t* peer, request_t* req, reply_item_t* item) {
    SAH_TRACEZ_INFO("pcb_reply", "Call reply item handler");
    bool retval = req->replyItemHandler(req, item, pcb, peer, req->userdata);
    /* remove reply item */
    reply_item_destroy(item);

    reply_t* reply = request_reply(req);
    if(llist_isEmpty(&reply->items)) {
        retval = pcb_handleRequestDone(pcb, peer, req, retval ? false : true);
    }

    return retval;
}

static bool pcb_peer_handle_requests(pcb_t* pcb, peer_info_t* peer, pcb_peer_data_t* pd) {
    bool retval = true;
    llist_iterator_t* it = llist_first(&pd->receivedRequests);
    llist_iterator_t* prefetch = llist_iterator_next(it);
    pcb_error = pcb_ok;

    /* waiting for a reply, block everything until that reply is received */
    if(pcb->request_id != 0) {
        return true;
    }
    /* check for received requests */
    while(it) {
        request_t* req = llist_item_data(it, request_t, it);

        if((req->state == request_state_handling) ||
           (req->state == request_state_forwarded)) {
            it = prefetch;
            prefetch = llist_iterator_next(it);
            continue;
        }

        if(req->state == request_state_destroyed) {
            it = prefetch;
            prefetch = llist_iterator_next(it);
            if(pcb->request_id == 0) {
                request_destroy(req);
                req = NULL;
            }
            continue;
        }

        if(req->state == request_state_done) {
            if(((request_attributes(req) & request_notify_all) != 0) && (request_type(req) == request_type_get_object)) {
                /* if the request is a notify request, add it to the notify request list if it is an get object request */
                llist_append(&pd->notifyRequests, &req->notify_it);
            } else {
                /* the handled request must be destroyed if
                      - it is not a notify request (can only be a get object request)
                      - it is done
                   destroy the request
                 */
                req->state = request_state_destroyed;
                request_destroy(req);
                req = NULL;
            }
            it = prefetch;
            prefetch = llist_iterator_next(it);
            continue;
        }

        /* handle the request */
        SAH_TRACEZ_INFO("pcb", "Handling request");
        if(pcb_handleRequest(pcb, peer, req)) {
            switch(req->state) {
            case request_state_created:
            case request_state_sent:
            case request_state_received:
                /* remove from the recieved requests */
                llist_iterator_take(it);
                break;
            case request_state_handling:
                /* not fully completed, do nothing here */
                if((((request_attributes(req) & request_notify_all) != 0) && (request_type(req) == request_type_get_object)) ||
                   (request_attributes(req) & request_wait)) {
                    /* if the request is a notify request, add it to the notify request list if it is an get object request */
                    llist_append(&pd->notifyRequests, &req->notify_it);
                }
                break;
            case request_state_forwarded:
                if(((request_attributes(req) & request_notify_all) != 0) && (request_type(req) == request_type_get_object)) {
                    /* if the request is a notify request, add it to the notify request list if it is an get object request */
                    llist_append(&pd->notifyRequests, &req->notify_it);
                } else {
                    /* remove from the recieved requests */
                    llist_iterator_take(it);
                }
                /* request will be destroyed when the reply will come from
                   the forwarded request, do not add the request back to the received ones
                 */
                break;
            case request_state_done:
                /* remove from the recieved requests */
                if(((request_attributes(req) & request_notify_all) != 0) && (request_type(req) == request_type_get_object)) {
                    /* if the request is a notify request, add it to the notify request list if it is an get object request */
                    llist_append(&pd->notifyRequests, &req->notify_it);
                } else {
                    /* the handled request must be destroyed if
                          - it is not a notify request (can only be a get object request)
                          - it is done
                       destroy the request
                     */
                    request_destroy(req);
                    req = NULL;
                }
                break;
            default:
                SAH_TRACEZ_NOTICE("pcb", "Request in unknown state");
                break;
            }
        } else {
            if(!peer_isConnected(peer)) {
                retval = false;
                break;
            }
            // go over the request arguments finding file descriptors and
            // close them
            request_parameterListCloseFd(req);
            /* request handling failed, destroy the request */
            request_destroy(req);
            req = NULL;
            SAH_TRACEZ_NOTICE("pcb", "Failed to handle request");
            retval = false;
        }
        it = prefetch;
        prefetch = llist_iterator_next(it);
    }
    return retval;
}

static bool pcb_peer_handle_replies(pcb_t* pcb, peer_info_t* peer, pcb_peer_data_t* pd) {
    SAH_TRACEZ_IN("pcb_reply");
    bool retval = true;
    pcb_error = pcb_ok;

    /* check the received replies */
    llist_iterator_t* it = llist_first(&pd->replies);
    llist_iterator_t* prefetch = NULL;
    reply_t* reply = NULL;
    reply_item_t* item = NULL;
    request_t* req = NULL;
    while(it) {
        item = llist_item_data(it, reply_item_t, reply_it);
        reply = llist_item_data(item->it.list, reply_t, items);
        req = llist_item_data(reply, request_t, reply);

        if((pcb->request_id != 0) && (request_id(req) != pcb->request_id)) {
            prefetch = it;
            it = llist_iterator_next(it);
            continue;
        }

        switch(req->state) {
        case request_state_sent:
        case request_state_waiting_for_reply:
            SAH_TRACEZ_INFO("pcb_rh", "check request id 0x%x", request_id(req));
            req->state = request_state_handling_reply;
            if(req->replyItemHandler) {
                SAH_TRACEZ_INFO("pcb_reply", "Handle single item");
                retval = pcb_handleReplyItem(pcb, peer, req, item);
            } else {
                SAH_TRACEZ_INFO("pcb_reply", "Handle all items of a request");
                retval = pcb_handleReply(pcb, peer, req);
            }
            if(!retval || !peer_isConnected(peer)) {
                SAH_TRACEZ_NOTICE("pcb_reply", "Peer disconnected or failed to handle reply, stop handling replies");
                goto exit;
            }
            if(req->state == request_state_handling_reply) {
                req->state = request_state_waiting_for_reply;
            }
            break;
        default:
            SAH_TRACEZ_NOTICE("pcb_reply", "Request state = %x", req->state);
            prefetch = it;
            break;
        }
        it = prefetch ? llist_iterator_next(prefetch) : llist_first(&pd->replies);
    }

    /* check the pending requests */
    it = llist_first(&pd->pendingRequests);
    prefetch = llist_iterator_next(it);
    while(it) {
        req = llist_item_data(it, request_t, it);

        /* check if the request is done */
        if((pcb->request_id == 0) || (request_id(req) == pcb->request_id)) {
            SAH_TRACEZ_INFO("pcb_reply", "Verify done ...");
            pcb_handleRequestDone(pcb, peer, req, (req->state == request_state_done) ? true : false);
        }

        if((req->state == request_state_destroyed) && (pcb->request_id == 0)) {
            SAH_TRACEZ_INFO("pcb_reply", "request id 0x%x destroyed", request_id(req));
            request_destroy(req);
        }
        it = prefetch;
        prefetch = llist_iterator_next(it);
    }

exit:
    SAH_TRACEZ_OUT("pcb_reply");
    return retval;
}

static void pcb_handleRequests(connection_info_t* con) {
    if(!con) {
        pcb_error = pcb_error_invalid_parameter;
        return;
    }

    pcb_t* pcb = NULL;
    connection_t* connection = NULL;
    llist_iterator_t* peer_it = NULL;

    pcb = llist_item_data(con, pcb_t, connection);
    llist_for_each(peer_it, &con->connections) {
        connection = llist_item_data(peer_it, connection_t, it);
        if(!peer_isConnected(&connection->info) || peer_isCustomFd(&connection->info) ||
           peer_isPair(&connection->info)) {
            continue;
        }

        pcb_peer_data_t* pd = peer_getUserData(&connection->info);
        if(!pd) {
            continue;
        }
        if(!pcb_peer_handle_replies(pcb, &connection->info, pd)) {
            SAH_TRACEZ_NOTICE("pcb", "Failed to handle replies");
        }
        if(!peer_isConnected(&connection->info)) {
            continue;
        }
        if(!pcb_peer_handle_requests(pcb, &connection->info, pd)) {
            SAH_TRACEZ_NOTICE("pcb", "Failed to handle request(s)");
        }
    }

    return;
}

static bool pcb_connection_read(peer_info_t* peer) {
    connection_info_t* con = peer_connection(peer);
    pcb_t* pcb = llist_item_data(con, pcb_t, connection);

    pcb_peer_data_t* pd = peer_getUserData(peer);
    if(!pd) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(pd->serializers == NULL) {
        /* try to find matching serialization plugin. */
        pd->serializers = serialization_getHandlersForSocket(peer);
    }

    if((pd->serializers == NULL) ||
       (pd->serializers->deserialize == NULL)) {
        pcb_error = pcb_error_no_serializer;
        return false;
    }

    /* read data from the socket */
    SAH_TRACEZ_INFO("pcb", "Read from fd %d, calling deserialization", peer_getFd(peer));
    deserialize_return_t rv = deserialize_done;
    do {
        rv = pd->serializers->deserialize(peer, pcb);
        if(rv == deserialize_error) {
            SAH_TRACEZ_NOTICE("pcb_reply", "Deserialization failed (%d - %s)", pcb_error, error_string(pcb_error));
            /* handle incoming custom notifications with id = 0 */
            pcb_handleReply(pcb, peer, &pd->zero_request);
            return false;
        }

        if(!pcb_peer_handle_replies(pcb, peer, pd)) {
            SAH_TRACEZ_NOTICE("pcb_reply", "Failed to handle replies");
        }

        if(!peer_isConnected(peer)) {
            SAH_TRACEZ_NOTICE("pcb_reply", "Peer is closed - stop processing");
            break;
        }

        if(!pcb_peer_handle_requests(pcb, peer, pd)) {
            SAH_TRACEZ_NOTICE("pcb_reply", "Failed to handle request(s)");
        }

        if(!peer_isConnected(peer)) {
            SAH_TRACEZ_NOTICE("pcb_reply", "Peer is closed - stop processing");
            break;
        }
    } while(rv == deserialize_more_available);
    SAH_TRACEZ_NOTICE("pcb_reply", "Deserialization done");

    return true;
}

static bool pcb_defaultNotificationHandler(request_t* req, pcb_t* pcb, peer_info_t* from, void* userdata) {
    (void) userdata;
    bool retval = true;

    reply_t* reply = request_reply(req);
    reply_item_t* item = reply_firstItem(reply);
    if(!reply) {
        goto exit_reply;
    }

    while(item) {
        switch(reply_item_type(item)) {
        case reply_type_notification: {
            notification_t* notification = reply_item_notification(item);
            notify_handler_iterator_t* nit = NULL;
            llist_iterator_t* it = NULL;
            llist_for_each(it, &pcb->notifyHandlers) {
                nit = llist_item_data(it, notify_handler_iterator_t, it);
                if(nit->notifyHandler(pcb, from, notification)) {
                    break;
                }
                if(!peer_isConnected(from)) {
                    retval = false;
                    goto exit_reply;
                }
            }
            if(pcb->reqhandlers && pcb->reqhandlers->notify) {
                SAH_TRACEZ_INFO("pcb", "Calling global notification handler(s)");
                retval = pcb->reqhandlers->notify(pcb, from, notification);
                if(!retval || !peer_isConnected(from)) {
                    SAH_TRACEZ_INFO("pcb", "Global notification handler failed or peer has closed connection");
                    retval = false;
                    goto exit_reply;
                }
            }
        }
        break;
        default:     /* should not get anything else on request id 0 */
            break;
        }
        item = reply_nextItem(item);
    }
exit_reply:
    return retval;
}

static bool pcb_connection_destroy(peer_info_t* peer) {
    SAH_TRACEZ_IN("pcb");
    pcb_peer_data_t* pd = peer_getUserData(peer);

    /* free data */
    free(pd);
    peer_setUserData(peer, NULL);

    SAH_TRACEZ_OUT("pcb");
    return true;
}

static bool pcb_connection_close(peer_info_t* peer) {
    SAH_TRACEZ_IN("pcb");
    SAH_TRACEZ_INFO("pcb", "Removing peer data");
    pcb_peer_data_t* pd = peer_getUserData(peer);
    if(pd) {

        if(pd->serializers && pd->serializers->deserialize_cleanup) {
            pd->serializers->deserialize_cleanup(peer);
        }
        if(pd->serializers && pd->serializers->serialize_cleanup) {
            pd->serializers->serialize_cleanup(peer);
        }

        /* REMARK: pending request should be destroyed by the creator
                   received request will be destroyed by the library
         */

        /* remove all pending requests */
        SAH_TRACEZ_INFO("pcb", "Removing all pending requests");
        llist_iterator_t* it = llist_takeFirst(&pd->pendingRequests);
        request_t* req = NULL;
        while(it) {
            req = llist_item_data(it, request_t, it);
            SAH_TRACEZ_INFO("pcb", "Cancel pending request");
            switch(req->state) {
            case request_state_destroy:
            case request_state_destroyed:
                request_destroy(req);
                break;
            default:
                req->state = request_state_canceled;
                if(req->cancelHandler) {
                    req->cancelHandler(req, req->userdata);
                }
                break;
            }
            it = llist_takeFirst(&pd->pendingRequests);
        }
        /* remove all received requests */
        SAH_TRACEZ_INFO("pcb", "Removing all received requests");
        it = llist_takeFirst(&pd->receivedRequests);
        req = NULL;
        while(it) {
            req = llist_item_data(it, request_t, it);
            req->state = request_state_destroy;
            request_destroy(req);
            SAH_TRACEZ_INFO("pcb", "Destroyed received request");
            it = llist_takeFirst(&pd->receivedRequests);
        }
        /* remove all forwarded requests */
        SAH_TRACEZ_INFO("pcb", "Clear list of forwarded requests");
        it = llist_takeFirst(&pd->forwardedRequests);
        req = NULL;
        while(it) {
            req = llist_item_data(it, request_t, forwarded_it);
            req->state = request_state_destroy;
            SAH_TRACEZ_INFO("pcb", "Destroy forwarded request");
            request_destroy(req);
            SAH_TRACEZ_INFO("pcb", "Destroyed forwarded request");
            it = llist_takeFirst(&pd->forwardedRequests);
        }
        /* remove all notify requests */
        SAH_TRACEZ_INFO("pcb", "Removing all notify requests");
        it = llist_takeFirst(&pd->notifyRequests);
        req = NULL;
        while(it) {
            req = llist_item_data(it, request_t, notify_it);
            req->state = request_state_destroy;
            request_destroy(req);
            SAH_TRACEZ_INFO("pcb", "Destroyed notify request");
            it = llist_takeFirst(&pd->notifyRequests);
        }

        /* clear all received notifications from zero_request */
        pcb_request_replyClear(&pd->zero_request);

        pd->serializers = NULL;
    }

    SAH_TRACEZ_OUT("pcb");
    return true;
}

static bool pcb_verify_trusted(peer_info_t* peer, pcb_peer_data_t* pd) {
    /* tcp sockets are never trusted */
    if(peer_isTCPSocket(peer)) {
        SAH_TRACEZ_INFO("pcb", "peer is a TCP socket, never trusted");
        pd->trusted = false;
        return true;
    }

    /* get the user id from the other side */
    struct ucred credentials;
    socklen_t ucred_length = sizeof(struct ucred);
    int fd = peer_getFd(peer);
    if(fd < 0) {
        return false;
    }
    if(getsockopt(fd, SOL_SOCKET, SO_PEERCRED, &credentials, &ucred_length) == -1) {
        /* if fetching the credentials fail, do not accept the socket */
        return false;
    }

    uint32_t uid = credentials.uid;
    if(uid == 0) {
        SAH_TRACEZ_INFO("pcb", "peer is a IPC socket, comming from process running as root. Peer is trusted");
        pd->trusted = true;
    } else {
#ifdef CONFIG_PCB_ALWAYS_ACCEPT_IPC
        SAH_TRACEZ_INFO("pcb", "peer is a IPC socket, config overide. Peer is trusted");
        pd->trusted = true;
#else
        pd->trusted = false; /* enforce security checking */

        /* translate system uid to pcb uid */
        struct passwd* passwd;
        /* get system user name */
        passwd = getpwuid(uid);
        if(passwd == NULL) {
            SAH_TRACE_ERROR("User uid %d not found, dropping socket", uid);
            return false;
        }

#ifdef CONFIG_PCB_ACL_USERMNGT
        /* get pcb user id */
        const usermngt_user_t* user = usermngt_userFindByName(passwd->pw_name);
        if(!user) {
            SAH_TRACE_ERROR("User %s not found, dropping socket", passwd->pw_name);
            return false;
        }
        uid = user->uid;
#endif

        pd->UID = uid;
#endif
    }

    return true;
}

static bool pcb_create_connection(peer_info_t* peer) {
    SAH_TRACEZ_INFO("pcb", "PCB new connection: accept or connect");
    if(peer_getUserData(peer)) {
        SAH_TRACEZ_INFO("pcb", "Connection accepted, user data available");
        return true;
    }
    connection_info_t* con = peer_connection(peer);
    pcb_t* pcb = llist_item_data(con, pcb_t, connection);

    if(!pcb_setManaged(pcb, peer)) {
        SAH_TRACEZ_ERROR("pcb", "Failed to set peer as managed");
        return false;
    }

    /* add the peer event handlers */
    peer_setEventHandler(peer, peer_event_read, peer_handler_read_always, pcb_connection_read);

    return true;
}

static bool pcb_accept_connection(peer_info_t* peer) {
    SAH_TRACEZ_INFO("pcb", "PCB new connection: accept");

    /* can only be a pcb context related connection here
       set the user data to NULL before contineuing
     */
    SAH_TRACEZ_INFO("pcb", "Remove user data (was inherited from listen socket");
    peer_setUserData(peer, NULL);

    if(!pcb_create_connection(peer)) {
        return false;
    }

    pcb_peer_data_t* pd = (pcb_peer_data_t*) peer_getUserData(peer);
    if(!pcb_verify_trusted(peer, pd)) {
        SAH_TRACEZ_WARNING("pcb", "Socket is not accepted");
        return false;
    }

    return true;
}

static bool pcb_connect_connection(peer_info_t* peer) {
    SAH_TRACEZ_INFO("pcb", "PCB new connection: connect");

    bool retval = pcb_create_connection(peer);
    if(retval) {
        /* created the connection with connectTo
           so set the default serializer here
         */
        connection_info_t* con = peer_connection(peer);
        pcb_t* pcb = llist_item_data(con, pcb_t, connection);

        pcb_peer_data_t* pd = peer_getUserData(peer);
        pd->serializers = pcb->defaultSerializer;
        if(!pcb_verify_trusted(peer, pd)) {
            SAH_TRACEZ_WARNING("pcb", "Socket is not accepted");
            return false;
        }
    }

    return retval;
}

static bool pcb_writeRequest(peer_info_t* peer, request_t* req) {
    if(!peer) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(peer->socketfd == -1) {
        pcb_error = pcb_error_not_connected;
        return false;
    }

    pcb_peer_data_t* pd = peer_getUserData(peer);
    if(!pd->serializers->serialize_request(peer, req)) {
        return false;
    }

    llist_append(&pd->pendingRequests, &req->it);
    req->peer = peer;

    pcb_error = pcb_ok;
    return true;
}

/**
   @ingroup pcb_core_main
   @{
 */

/**
   @brief
   Create a pcb object.

   @details

   @return
    - A pointer to a connection_info_t structure
    - NULL: an error has occured. See @ref pcb_error to get error details.
 */
pcb_t* pcb_create(const char* name, int argc, char* argv[]) {
    return pcb_createStatic(name, argc, argv, NULL);
}

/**
   @brief
   Create a pcb object.

   @details

   @return
    - A pointer to a connection_info_t structure
    - NULL: an error has occured. See @ref pcb_error to get error details.
 */
pcb_t* pcb_createStatic(const char* name, int argc, char* argv[], pcb_register_func_t serfn[]) {
    pcb_t* pcb = (pcb_t*) calloc(1, sizeof(pcb_t));
    if(!pcb) {
        pcb_error = pcb_error_no_memory;
        SAH_TRACEZ_ERROR("pcb", "%s: con=NULL", error_string(pcb_error));
        return NULL;
    }

    if(!pcb_initializeStatic(pcb, name, argc, argv, serfn)) {
        free(pcb);
        return NULL;
    }

    pcb_error = pcb_ok;
    return pcb;
}

/**
   @brief

   @details
 */
bool pcb_initialize(pcb_t* pcb, const char* name, int argc, char* argv[]) {
    return pcb_initializeStatic(pcb, name, argc, argv, NULL);
}

bool pcb_initializeStatic(pcb_t* pcb, const char* name, int argc, char* argv[], pcb_register_func_t serfn[]) {
    if(!pcb) {
        pcb_error = pcb_error_invalid_parameter;
        SAH_TRACEZ_ERROR("pcb", "%s: pcb=NULL", error_string(pcb_error));
        goto error;
    }

    /* initialize serialization */
    serialization_initialize();

    if(serfn) {
        if(!serialization_loadPluginsStatic(serfn)) {
            SAH_TRACEZ_ERROR("pcb", "Failed to setup serializers");
            goto error;
        }
    } else {
        if(!serialization_loadPlugins(NULL, NULL)) {
            SAH_TRACEZ_ERROR("pcb", "No serialization plugins found");
            goto error;
        }
    }

    if(!pcb_initialize_r(pcb, name, argc, argv)) {
        goto error_clean_serialization;
    }

    return true;

error_clean_serialization:
    serialization_cleanup();
error:
    return false;
}

/**
   @brief

   @details
 */
bool pcb_initialize_r(pcb_t* pcb, const char* name, int argc, char* argv[]) {
    if(!pcb) {
        pcb_error = pcb_error_invalid_parameter;
        SAH_TRACEZ_ERROR("pcb", "%s: pcb=NULL", error_string(pcb_error));
        goto error;
    }

    pcb->argc = argc;
    pcb->argv = argv;

    pcb->client_data.attributes = request_common_path_key_notation;
    pcb->client_data.error = 0;
    pcb->client_data.timeout = 30000;
    pcb->client_data.uid = UINT32_MAX;
    llist_initialize(&pcb->client_data.errors);

    SAH_TRACEZ_INFO("pcb", "PCB arguments count = %d", argc);
    if(argc > 0) {
        SAH_TRACEZ_INFO("pcb", "PCB first argument = %s", pcb->argv[0]);
    }

    /* initialize the connection */
    if(!connection_initialize(&pcb->connection, name)) {
        goto error;
    }

    /* set the connections event handlers */
    connection_setEventHandler(&pcb->connection, peer_event_accepted, pcb_accept_connection);
    connection_setEventHandler(&pcb->connection, peer_event_connected, pcb_connect_connection);
    connection_setEventsProcessedHandler(&pcb->connection, pcb_handleRequests);

    /* preset the default data fromat, this can be modifed by the caller later on
       So if this format is not available, we do not have to panic right here.
       ignoring the return value
     */
    if(!pcb_setDefaultFormat(pcb, SERIALIZE_FORMAT(serialize_format_default, SERIALIZE_DEFAULT_MAJOR, SERIALIZE_DEFAULT_MINOR))) {
        SAH_TRACEZ_ERROR("pcb", "Failed to set default format (%s)", error_string(pcb_error));
        goto error_clean_connections;
    }

    if(!datamodel_initialize(&pcb->datamodel)) {
        SAH_TRACEZ_ERROR("pcb", "datamodel_initialize failed (%s)", error_string(pcb_error));
        goto error_clean_connections;
    }

    if(!datamodel_initialize(&pcb->cache)) {
        SAH_TRACEZ_ERROR("pcb", "datamodel_initialize failed (%s)", error_string(pcb_error));
        goto error_clean_datamodel;
    }

    pcb->datamodel.attributes |= datamodel_attr_connected;
    pcb->reqhandlers = &defaultRequestHandlers;

    pcb->cache.attributes |= datamodel_attr_connected | datamodel_attr_cache;

    pcb->upcEnabled = false;
    pcb->upcOverwrite = false;
    pcb->upcEventCB = NULL;
    pcb->upcEventUserdata = NULL;

    pcb_error = pcb_ok;
    return true;

error_clean_connections:
    connection_cleanup(&pcb->connection);
error_clean_datamodel:
    datamodel_cleanup(&pcb->datamodel);
error:
    return false;
}

/**
   @brief

   @details

 */
void pcb_cleanup(pcb_t* pcb) {
    SAH_TRACEZ_IN("pcb");
    if(!pcb) {
        pcb_error = pcb_error_invalid_parameter;
        SAH_TRACEZ_ERROR("pcb", "%s: con=NULL", error_string(pcb_error));
        SAH_TRACEZ_OUT("pcb");
        return;
    }

    pcb_cleanup_r(pcb);
    SAH_TRACEZ_INFO("pcb", "unregister serializers");
    serialization_unregister();

    pcb_error = pcb_ok;
    SAH_TRACEZ_OUT("pcb");
    return;
}

/**
   @brief

   @details

 */
void pcb_cleanup_r(pcb_t* pcb) {
    SAH_TRACEZ_IN("pcb");
    if(!pcb) {
        pcb_error = pcb_error_invalid_parameter;
        SAH_TRACEZ_ERROR("pcb", "%s: con=NULL", error_string(pcb_error));
        SAH_TRACEZ_OUT("pcb");
        return;
    }

    /* remove all recieved requests */
    connection_info_t* con = pcb_connection(pcb);
    llist_iterator_t* it = llist_first(&con->connections);
    connection_t* c = NULL;
    pcb_peer_data_t* pd = NULL;
    request_t* req = NULL;
    while(it) {
        c = llist_item_data(it, connection_t, it);
        if(peer_isCustomFd(&c->info)) {
            it = llist_iterator_next(it);
            continue;
        }
        pd = peer_getUserData(&c->info);
        if(pd != NULL) {
            llist_iterator_t* rit = llist_takeFirst(&pd->notifyRequests);
            while(rit) {
                req = llist_item_data(rit, request_t, notify_it);
                req->state = request_state_canceled;
                request_destroy(req);
                rit = llist_takeFirst(&pd->notifyRequests);
            }
        }
        it = llist_iterator_next(it);
    }

    SAH_TRACEZ_INFO("pcb", "cleanup client error list");
    pcb_client_clear_errors(pcb);
    SAH_TRACEZ_INFO("pcb", "cleanup serialization");
    serialization_cleanup();
    SAH_TRACEZ_INFO("pcb", "cleanup connection");
    connection_cleanup(&pcb->connection);
    SAH_TRACEZ_INFO("pcb", "cleanup cache");
    datamodel_cleanup(&pcb->cache);
    SAH_TRACEZ_INFO("pcb", "cleanup datamodel");
    datamodel_cleanup(&pcb->datamodel);

    pcb_error = pcb_ok;
    SAH_TRACEZ_OUT("pcb");
    return;
}

/**
   @brief

   @details

   @param pcb
 */
void pcb_destroy(pcb_t* pcb) {
    SAH_TRACEZ_IN("pcb");
    if(!pcb) {
        pcb_error = pcb_error_invalid_parameter;
        SAH_TRACEZ_OUT("pcb");
        return;
    }

    notify_handler_iterator_t* nit = NULL;
    llist_iterator_t* it = llist_takeFirst(&pcb->notifyHandlers);
    while(it) {
        nit = llist_item_data(it, notify_handler_iterator_t, it);
        free(nit);
        it = llist_takeFirst(&pcb->notifyHandlers);
    }

    if(pcb->cleanup) {
        pcb->cleanup(pcb);
    }

    pcb_cleanup(pcb);
    free(pcb);
    pcb_error = pcb_ok;
    SAH_TRACEZ_OUT("pcb");
}

/**
   @brief

   @details

   @param pcb
 */
void pcb_destroy_r(pcb_t* pcb) {
    SAH_TRACEZ_IN("pcb");
    if(!pcb) {
        pcb_error = pcb_error_invalid_parameter;
        SAH_TRACEZ_OUT("pcb");
        return;
    }

    notify_handler_iterator_t* nit = NULL;
    llist_iterator_t* it = llist_takeFirst(&pcb->notifyHandlers);
    while(it) {
        nit = llist_item_data(it, notify_handler_iterator_t, it);
        free(nit);
        it = llist_takeFirst(&pcb->notifyHandlers);
    }

    if(pcb->cleanup) {
        pcb->cleanup(pcb);
    }
    pcb_cleanup_r(pcb);
    free(pcb);
    pcb_error = pcb_ok;
    SAH_TRACEZ_OUT("pcb");
}

pcb_t* pcb_fromConnection(connection_info_t* connection) {
    if(!connection) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    return llist_item_data(connection, pcb_t, connection);
}

bool pcb_upcIsEnabled(pcb_t* pcb) {
    if(!pcb) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    return pcb->upcEnabled;
}

bool pcb_upcEnable(pcb_t* pcb, bool enable) {
    if(!pcb) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    pcb->upcEnabled = enable;
    return true;
}

bool pcb_upcOverwriteIsEnabled(pcb_t* pcb) {
    if(!pcb) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    return pcb->upcOverwrite;
}

bool pcb_upcOverwriteEnable(pcb_t* pcb, bool enable) {
    if(!pcb) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    pcb->upcOverwrite = enable;
    return true;
}

bool pcb_upcSetEventCallback(pcb_t* pcb, upc_event_cb_t cb, void* userdata) {
    if(!pcb) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    pcb->upcEventCB = cb;
    pcb->upcEventUserdata = userdata;
    return true;
}

bool pcb_setPluginConfig(pcb_t* pcb, plugin_config_t* config) {
    if(!pcb) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    pcb->config = config;
    return true;
}

plugin_config_t* pcb_pluginConfig(pcb_t* pcb) {
    if(!pcb) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    return pcb->config;
}

bool pcb_setClientConfig(pcb_t* pcb, client_config_t* config) {
    if(!pcb) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    pcb->config = config;
    return true;
}

client_config_t* pcb_clientConfig(pcb_t* pcb) {
    if(!pcb) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    return pcb->config;
}


bool pcb_setUserData(pcb_t* pcb, void* data) {
    if(!pcb) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }
    pcb->userdata = data;

    return true;
}

void* pcb_userData(pcb_t* pcb) {
    if(!pcb) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    return pcb->userdata;
}

bool pcb_setCleanUpHandler(pcb_t* pcb, pcb_cleanup_handler_t fn) {
    if(!pcb) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    pcb->cleanup = fn;

    return true;
}

int pcb_argumentCount(pcb_t* pcb) {
    if(!pcb) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    return pcb->argc;
}

char** pcb_arguments(pcb_t* pcb) {
    if(!pcb) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    return pcb->argv;
}

bool pcb_setTrusted(pcb_t* pcb, peer_info_t* peer, bool trusted) {
    (void) pcb;
    if(!peer) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }
    pcb_peer_data_t* pd = peer_getUserData(peer);
    pd->trusted = trusted;

    return true;
}

bool pcb_setManaged(pcb_t* pcb, peer_info_t* peer) {
    (void) pcb;
    SAH_TRACEZ_INFO("pcb", "PCB new connection: accept or connect");
    if(peer_getUserData(peer)) {
        SAH_TRACEZ_INFO("pcb", "User data available, can not set as managed peer");
        return false;
    }

    SAH_TRACEZ_INFO("pcb", "Allocate new user data");
    pcb_peer_data_t* pd = (pcb_peer_data_t*) calloc(1, sizeof(pcb_peer_data_t));
    if(!pd) {
        SAH_TRACEZ_ERROR("pcb", "Failed to allocate memory");
        return false;
    }

    /* serializers will be set when the first packet is received */
    pd->serializers = NULL;
    pd->zero_request.peer = peer;

    // by default wants broadcast notifications
    pd->broadcast = true;

    request_setReplyHandler(&pd->zero_request, pcb_defaultNotificationHandler);
    pd->zero_request.state = request_state_waiting_for_reply;
    request_setData(&pd->zero_request, peer);

    pd->UID = UINT32_MAX;
    pd->peer = peer;

    peer_setUserData(peer, pd);

    peer_setEventHandler(peer, peer_event_destroy, 0, pcb_connection_destroy);
    peer_setEventHandler(peer, peer_event_close, 0, pcb_connection_close);

    return true;
}

bool pcb_setDefaultUid(pcb_t* pcb, peer_info_t* peer, uint32_t uid) {
    (void) pcb;
    if(!peer) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }
    pcb_peer_data_t* pd = peer_getUserData(peer);
    pd->UID = uid;

    return true;
}

/**
   @brief
   Send a request to the server.

   @details
   This function sends a request to the server.

   @param pcb
   @param peer
   @param req The request to be send

   @return
    - true: the request is send
    - false failed to send the request.
 */
bool pcb_sendRequest(pcb_t* pcb, peer_info_t* peer, request_t* req) {
    (void) pcb;
    if(!peer || !req) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    int fd = peer_getFd(peer);
    if((fd == -1) && peer_isServerConnection(peer)) {
        SAH_TRACEZ_WARNING("pcb", "Not connected - try to reconnect");
        if(!peer_reconnect(peer)) {
            SAH_TRACEZ_ERROR("pcb", "creconnection failed %s", error_string(pcb_error));
            return false;
        }
        fd = peer_getFd(peer);
    }

    if(fd == -1) {
        pcb_error = pcb_error_not_connected;
        SAH_TRACEZ_ERROR("pcb", "Not connected - sending failed");
        return false;
    }

    pcb_peer_data_t* pd = peer_getUserData(peer);
    if(pd == NULL) {
        pcb_error = pcb_error_no_serializer;
        return false;
    }

    if((pd->serializers == NULL) ||
       (pd->serializers->serialize_request == NULL)) {
        pcb_error = pcb_error_no_serializer;
        return false;
    }

    if(request_attributes(req) & request_notify_all) {
        if((pd->last_used_nreq_id == 0x8FFFFFFF) ||
           (pd->last_used_nreq_id < 0x80000000)) {
            pd->last_used_nreq_id = 0x80000000;
        }
        pd->last_used_nreq_id++;
        req->request_id = pd->last_used_nreq_id;
    } else {
        pd->last_used_req_id++;
        if(pd->last_used_req_id >= 0x80000000) {
            pd->last_used_req_id = 1;
        }
        req->request_id = pd->last_used_req_id;
    }

    SAH_TRACEZ_INFO("pcb", "sending request, has parameter? %s", request_firstParameter(req) ? "yes" : "no");

    if(!pcb_writeRequest(peer, req)) {
        SAH_TRACEZ_ERROR("pcb", "pcb_writeRequest failed %s", error_string(pcb_error));
        return false;
    }

    if(request_type(req) != request_type_close_request) {
        if(!peer_flush(peer)) {
            SAH_TRACEZ_ERROR("pcb", "failed to flush peer: %s", error_string(pcb_error));
            return false;
        }
    }
    SAH_TRACEZ_INFO("pcb_rh", "Data available on peer after flush = %s", peer_needWrite(pd->peer) ? "Yes" : "No");
    if(peer_needWrite(peer)) {
        /* REMARK:
           Workaround for radvision and voice application
           Just to make sure that the socket is added to the rad vision stack
         */
        connection_setEventsAvailable();
    }
    req->state = request_state_sent;
    pcb_error = pcb_ok;
    return true;
}

bool pcb_sendNotificationInternal(pcb_t* pcb, peer_info_t* to, peer_info_t* from, notification_t* notification) {
    if(!pcb || !notification) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    /* notification type 100: add the name of the connection object (if the name is available) */
    if(notification_type(notification) == 100) {
        if(pcb->connection.name) {
            notification_parameter_t* parameter = notification_parameter_create("hop", pcb->connection.name);
            notification_addParameter(notification, parameter);
        } else {
            notification_parameter_t* parameter = notification_parameter_create("hop", "no-name");
            notification_addParameter(notification, parameter);
        }
    }

    object_t* object = NULL;
    if(notification_objectPath(notification) && *notification_objectPath(notification)) {
        object_t* root = datamodel_root(pcb_datamodel(pcb));
        object = object_getObject(root, notification_objectPath(notification), path_attr_default | path_attr_key_notation, NULL);
        if(!object) {
            object = object_getObject(root, notification_objectPath(notification), path_attr_default, NULL);
        }
    }

    bool rc = true;
    if(object) {
        notify_list_t* notifyRequests = NULL;
        object_buildNotifyList(object, &notifyRequests, 0);

        /* clear the notify request list */
        notify_list_t* elm = notifyRequests;
        notify_list_t* next = NULL;
        while(elm) {
            next = elm->next;
            llist_t* temp = elm->req->notify_it.list;
            if(!temp) {
                SAH_TRACEZ_ERROR("pcb", "No peer info avaialble for notify request");
                free(elm);
                elm = next;
                continue;
            }
            pcb_peer_data_t* pd = llist_item_data(temp, pcb_peer_data_t, notifyRequests);
            if(!pd) {
                SAH_TRACEZ_ERROR("pcb", "No peer data available");
                free(elm);
                elm = next;
                continue;
            }
            if((pd->peer == to) || !to) {
                if(request_attributes(elm->req) & request_notify_custom) {
                    SAH_TRACEZ_INFO("notify", "Sending notification");
                    pcb_replyBegin(pd->peer, elm->req, 0);
                    rc = pcb_writeNotification(pd->peer, elm->req, notification);
                    pcb_replyEnd(pd->peer, elm->req);

                    /* REMARK:
                        Workaround for radvision and voice application
                        Just to make sure that the socket is added to the rad vision stack
                     */
                    connection_setEventsAvailable();
                }
            }
            free(elm);
            elm = next;
        }
    } else {
        if(!to) {
            llist_iterator_t* it = NULL;
            SAH_TRACEZ_INFO("pcb", "Sending to all clients ...");
            /* send to client connections */
            connection_t* con = NULL;
            llist_for_each(it, &pcb->connection.connections) {
                con = llist_item_data(it, connection_t, it);
                if((&(con->info) != from) && !peer_isCustomFd(&con->info)) {
                    pcb_peer_data_t* pd = peer_getUserData(&con->info);
                    if((pd == NULL) ||
                       (pd->broadcast == false) ||
                       (pd->serializers == NULL) ||
                       (pd->serializers->notificationBroadcastSupport == false)) {
                        SAH_TRACEZ_NOTICE("pcb", "Skip peer %d, no serializer available", peer_getFd(&con->info));
                        continue;
                    }
                    SAH_TRACEZ_INFO("pcb", "Writing notification to peer %d", peer_getFd(&con->info));
                    pcb_replyBegin(&con->info, 0, 0);
                    rc = pcb_writeNotification(&con->info, 0, notification);
                    pcb_replyEnd(&con->info, 0);
                }
            }
        } else {
            pcb_replyBegin(to, 0, 0);
            rc = pcb_writeNotification(to, 0, notification);
            pcb_replyEnd(to, 0);
        }
    }
    return rc;
}

/**
   @brief
   Send a notification to all connected clients and connected server

   @details
   This function sends a <b>custom</b> notification (notification type must be >= 100) to all connected clients
   and to the server if there is a server connection.

   Any client or server can send a custom notification or forward a received notification.
   It is not recommended that servers or clients are forwarding notifications, this should only be done
   by connections that hold a dispatching data model.

   <b>Special notification types</b>:

   <b>Notification type 100:</b>
   Before sending this type of notification the connection's name is added (if the name is available) to the notification as a parameter.
   This can be used for debugging purposesto see where the notification has been. (Can be compared to the tracert)

   @param con A pointer to a connection object, must be created with @ref connection_create
   @param from If the notification was received from a peer, it can be specified in this parameter, the notification will not be send to that peer again.
            If you are the orignial sender of the notification you can specy NULL.
   @param notification The notification to be send

   @return
    - true: the notification is send
    - false failed to send the notification.
 */
bool pcb_sendNotification(pcb_t* pcb, peer_info_t* to, peer_info_t* from, notification_t* notification) {
    if(!pcb || !notification) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(notification_type(notification) < 10) {
        SAH_TRACEZ_ERROR("pcb", "Invalid notification type");
        pcb_error = pcb_error_invalid_notification_type;
        return false;
    }

    return pcb_sendNotificationInternal(pcb, to, from, notification);
}

static inline bool pcb_wait_isValidRequest(request_t* req) {
    if((req->it.list == NULL) ||
       ((req->state != request_state_sent) &&
        (req->state != request_state_waiting_for_reply))) {
        /* request is not in a pending list */
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(req->state == request_state_destroyed) {
        req->state = request_state_done;
        request_destroy(req);
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    return true;
}

static inline void pcb_wait_build_sets(int* maxfd, fd_set* readset, fd_set* writeset, peer_info_t* peer) {
    FD_ZERO(readset);
    FD_ZERO(writeset);
    *maxfd = -1;

    int fd = peer_getFd(peer);
    if(fd == -1) {
        return;
    }
    if(fd > *maxfd) {
        *maxfd = fd;
    }

    if(peer_needWrite(peer)) {
        FD_SET(fd, writeset);
    }
    if(peer_needRead(peer)) {
        FD_SET(fd, readset);
    }
}

/**
   @brief
   Wait for a reply on a request

   @details
   This function will wait for a reply on a request. This function is provided to help in implementing
   synchronous request and is not recommended to use.

   The function uses the @ref connection_readSet, @ref connection_waitForMessages and @ref connection_handleMessages
   to get messages from the sockets. The timeout is restarted each time messages are received. The timeout must be considered
   as the maximum idle time.

   @param con A pointer to a connection object, must be created with @ref connection_create
   @param req The request we are waiting for
   @param timeout Maximum idle time or NULL for no timeout

   @return
    - true: The reply is received
    - false: No reply has been received and all connections has been idle for the timeout interval
 */
bool pcb_waitForReply(pcb_t* pcb, request_t* req, struct timeval* timeout) {
    static uint32_t recurse = 0;
    bool retval = false;
    llist_t cons;
    llist_t* ref = NULL;
    pcb_peer_data_t* pd = NULL;
    bool eventsAvailable = false;
    fd_set readset;
    fd_set writeset;
    int maxfd = -1;
    uint32_t old_id = 0;
    int events = 0;

    if(!pcb || !req) {
        pcb_error = pcb_error_invalid_parameter;
        goto exit;
    }

    if(!pcb_wait_isValidRequest(req)) {
        goto exit;
    }

    recurse++;

    if(recurse > 100) {
        SAH_TRACE_ERROR("Recursive pcb_waitForReply detected, is this the intention?");
        goto exit;
    }
    pd = llist_item_data(req->it.list, pcb_peer_data_t, pendingRequests);

    llist_initialize(&cons);

    if(pcb->connection.it.list == NULL) {
        llist_append(&cons, &pcb->connection.it);
    } else {
        ref = pcb->connection.it.list;
        llist_append(&cons, &pcb->connection.it);
    }

    old_id = pcb->request_id;
    pcb->request_id = request_id(req);

    SAH_TRACEZ_INFO("pcb", "Waiting for reply: request state = %d", request_state(req));
    pcb_peer_handle_replies(pcb, pd->peer, pd);
    if((req->state == request_state_destroyed) ||
       (req->state == request_state_done) ||
       request_replyReceived(req)) {
        llist_append(ref, &pcb->connection.it);
        llist_cleanup(&cons);
        pcb->request_id = old_id;
        recurse--;
        retval = true;
        goto exit;
    }

    SAH_TRACEZ_INFO("pcb", "Start waiting for reply (socket %d - request id %d)", peer_getFd(pd->peer), request_id(req));
    if(timeout) {
        SAH_TRACEZ_INFO("pcb", "Timeout available - wait time = %ld second + %ld usec", timeout->tv_sec, timeout->tv_usec);
    }
    pcb_wait_build_sets(&maxfd, &readset, &writeset, pd->peer);
    events = connection_select_r(maxfd, &readset, &writeset, timeout, false);
    while(events > 0 && !connection_isTerminated()) {
        eventsAvailable = true;
        pcb_timer_enableTimers(false);
        connection_handleEvents_r(&cons, &readset, &writeset);
        pcb_timer_enableTimers(true);

        if(!peer_isConnected(pd->peer)) {
            SAH_TRACEZ_NOTICE("pcb", "peer is closed, stop waiting");
            break;
        }

        if((req->state == request_state_destroyed) ||
           (req->state == request_state_done)) {
            SAH_TRACEZ_INFO("pcb", "request done or destroyed, stop waiting (socket %d - request id %d)", peer_getFd(pd->peer), request_id(req));
            retval = true;
            break;
        }
        if(request_replyReceived(req)) {
            SAH_TRACEZ_INFO("pcb", "reply received, stop waiting (socket %d - request id %d)", peer_getFd(pd->peer), request_id(req));
            retval = true;
            break;
        }

        if(timeout) {
            SAH_TRACEZ_INFO("pcb", "Timeout available - wait time = %ld second + %ld usec", timeout->tv_sec, timeout->tv_usec);
        }
        pcb_wait_build_sets(&maxfd, &readset, &writeset, pd->peer);
        events = connection_select_r(maxfd, &readset, &writeset, timeout, false);
    }

    if(eventsAvailable) {
        connection_setEventsAvailable();
    }

    llist_append(ref, &pcb->connection.it);
    llist_cleanup(&cons);
    pcb->request_id = old_id;
    recurse--;

    if(events == 0) {
        SAH_TRACEZ_ERROR("pcb", "Timeout (socket %d - request id %d) - no reply available", peer_getFd(pd->peer), request_id(req));
        pcb_error = pcb_error_time_out;
    } else if(events < 0) {
        SAH_TRACEZ_ERROR("pcb", "Error (socket %d - request id %d) - %d (%s) - no reply available", peer_getFd(pd->peer), request_id(req), pcb_error, error_string(pcb_error));
    } else {
        SAH_TRACEZ_INFO("pcb", "Waiting done (socket %d - request id %d) - reply available", peer_getFd(pd->peer), request_id(req));
    }

exit:
    return retval;
}

/**
   @brief
   Return the connection's local data model

   @details
   The pointer returned by this function can be used in all of the data model functions or any
   other function that requieres a data model pointer.
   The application that owns the connection pointer is also the owner and maintainer of this
   data model. This data model is know as the public or connected data model.

   @param con A pointer to a connection object, must be created with @ref connection_create

   @return
    - pointer to the connection's local data model
    - NULL if an invalid contion object was specified.
 */
datamodel_t* pcb_datamodel(pcb_t* pcb) {
    if(!pcb) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    pcb_error = pcb_ok;
    return &pcb->datamodel;
}

connection_info_t* pcb_connection(pcb_t* pcb) {
    if(!pcb) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    pcb_error = pcb_ok;
    return &pcb->connection;
}

void pcb_handleQueuedEvents(pcb_t* pcb) {
    if(!pcb) {
        return;
    }

    pcb_handleRequests(&pcb->connection);
}

/**
   @brief
   Return the cached data model

   @details
   The pointer returned by this function can be used in all of the data model functions or any
   other function that requieres a data model pointer.
   You can do direct modifications in this data model. This data model may not be a complete data model.
   Not all objects are available or objects are partially available (no parameters are loaded, ...).

   @param con A pointer to a connection object, must be created with @ref connection_create

   @return
    - pointer to the connection's cached data model
    - NULL if an invalid contion object was specified.
 */
datamodel_t* pcb_cache(pcb_t* pcb) {
    if(!pcb) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    pcb_error = pcb_ok;
    return &pcb->cache;
}

/**
   @brief
   Clear the complete cached data model

   @details
   This function will remove all cached objects from the cached data model.

   @param con A pointer to a connection object, must be created with @ref connection_create

   @return
    - true: The cache is cleared
    - false: failed to clear the cache. See @ref pcb_error to get error details.
 */
bool pcb_cacheClear(pcb_t* pcb) {
    if(!pcb) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    datamodel_cleanup(&pcb->cache);
    datamodel_initialize(&pcb->cache);

    pcb->cache.attributes |= datamodel_attr_connected | datamodel_attr_cache;

    pcb_error = pcb_ok;
    return true;
}

/**
   @brief
   Remove an object from the cached data model

   @details
   This function will first retrieve the object from the cache and then call @ref connection_cacheRemoveObject.

   @param con A pointer to a connection object, must be created with @ref connection_create
   @param path The full path of the object
   @param pathAttrib see @ref path_attributes_t. The path_attr_parent attribute is ignored in this function.

   @return
    - true: the object and all its sub-objects are removed from the cache.
    - false: failed to remove the object. See @ref pcb_error to get error details.
 */
bool pcb_cacheRemove(pcb_t* pcb, const char* path, uint32_t pathAttrib) {
    if(!pcb || !path) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    pathAttrib &= ~path_attr_parent;

    object_t* cache_root = datamodel_root(pcb_cache(pcb));
    object_t* object = object_getObject(cache_root, path, pathAttrib, NULL);
    if(!object) {
        pcb_error = pcb_error_not_found;
        SAH_TRACEZ_ERROR("pcb", "object_getObject failed %s - %s", path, error_string(pcb_error));
        return false;
    }

    return pcb_cacheRemoveObject(pcb, object);
}

/**
   @brief
   Remove an object from the cached data model

   @details
   Remove an object an all its sub-objects (children and instances) from the cache.
   If the object was returned with an non-caching request, the object will be removed from the reply.
   The object must be a remote object.

   @param con A pointer to a connection object, must be created with @ref connection_create
   @param object The object to be removed.

   @return
    - true: the object and all its sub-objects are removed from the cache.
    - false: failed to remove the object. See @ref pcb_error to get error details.
 */
bool pcb_cacheRemoveObject(pcb_t* pcb, object_t* object) {
    if(!pcb || !object) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(!object_isRemote(object)) {
        pcb_error = pcb_error_remote_local_mismatch;
        SAH_TRACEZ_ERROR("pcb", "Local objects can not be removed with this function %s", error_string(pcb_error));
        return false;
    }

    if(object_state(object) == object_state_created) {
        local_object_rollback(object);
        return true;
    }
    local_object_delete(object);

    pcb_error = pcb_ok;
    return true;
}

bool pcb_cacheRemoveSingleObject(pcb_t* pcb, object_t* object) {
    if(!pcb || !object) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(!object_isRemote(object)) {
        pcb_error = pcb_error_remote_local_mismatch;
        SAH_TRACEZ_ERROR("pcb", "Local objects can not be removed with this function %s", error_string(pcb_error));
        return false;
    }

    object->state = object_state_ready;

    object_t* parent = datamodel_root(pcb_cache(pcb));
    object_t* child = NULL;

    /* move children */
    child = object_firstChild(object);
    while(child) {
        tree_item_appendChild(&parent->tree_item, &child->tree_item);
        child = object_firstChild(object);
    }

    /* move instances */
    child = object_firstInstance(object);
    while(child) {
        child->object_info.instance_info.instance_of = NULL;
        llist_iterator_take(&child->instance_it);
        tree_item_appendChild(&parent->tree_item, &child->tree_item);
        child = object_firstInstance(object);
    }

    /* delete the object from the cache */
    local_object_delete(object);

    pcb_error = pcb_ok;
    return true;
}

bool pcb_cacheCommit(pcb_t* pcb) {
    if(!pcb) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    object_t* cache_root = datamodel_root(pcb_cache(pcb));
    local_object_commit(cache_root);

    return true;
}

/**
   @brief
   Get an object

   @details
   This function will try to retrieve an object from the local data model or from the
   server's data model if this connection object has a connection to a server (see @ref connection_connectToIPC and @ref connection_connectToTCP).

   Objects are searched in the following order:
    - the local data model
    - the connection data model cache
    - the server's data model.

   If the object can not be found in the local data model or the cached data model, synchronous communication is done to
   retrieve the object from a remote data model.

   @param con A pointer to a connection object, must be created with @ref connection_create
   @param path The full path of the object
   @param pathAttr see @ref path_attributes_t. The path_attr_parent attribute is ignored in this function.

   @return
    - pointer to an object
    - NULL if the object is not found.
 */
object_t* pcb_getObject(pcb_t* pcb, const char* path, const uint32_t pathAttr) {
    if(!pcb) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    object_t* object = NULL;
    /* search in local datamodel */
    object = local_object_getObject(datamodel_root(&pcb->datamodel), path, pathAttr & ~path_attr_parent, NULL);
    if(!object) {
        string_t p;
        string_initialize(&p, 64);
        string_fromChar(&p, path);
        object = object_searchCache(datamodel_root(&pcb->cache), &p, pathAttr & ~path_attr_parent, NULL);
        string_cleanup(&p);
    }

    return object;
}

/**
   @brief
   Get the connection's request handlers

   @details
   Returns a pointer to the structure containing all the request handlers of a connection object.

   @param con A pointer to a connection object, must be created with @ref connection_create

   @return
    - The pointer to connection's request handlers.
    - NULL if no valid connection object is specified
 */
request_handlers_t* pcb_getRequestHandlers(pcb_t* pcb) {
    if(!pcb) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    pcb_error = pcb_ok;
    return pcb->reqhandlers;
}

/**
   @brief
   Set the connection's request handlers

   @details
   The request handlers are callback functions which will be called when a request is received.
   When a connection object is created the default request handlers are set. You can modify the implementation
   of these handlers by providing your own request handlers. If one or more function pointers in this structure
   is set to NULL, the according request is not supported.

   Typically you do not need to change these function pointers.

   @param con A pointer to a connection object, must be created with @ref connection_create
   @param handlers Structure containing the function pointers to callback functions.

   @return
    - true: the new request handlers are set
    - false: an error has occured. See @ref pcb_error to get error details.
 */
bool pcb_setRequestHandlers(pcb_t* pcb, request_handlers_t* handlers) {
    if(!pcb) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    pcb->reqhandlers = handlers;

    pcb_error = pcb_ok;
    return true;
}

/**
   @brief
   Set the connection's notify handler

   @details
   This functions sets the connection's notify handler. Each plug-in, client or bus can send out notifucations.
   If your applications wants to receive these notifications a notification handler must be installed.

   @param con A pointer to a connection object, must be created with @ref connection_create
   @param notifyHandler A pointer to the notification handler function.

   @return
    - true: the new request handlers are set
    - false: an error has occured. See @ref pcb_error to get error details.
 */
bool pcb_setNotifyHandler(pcb_t* pcb, notify_handler_t notifyHandler) {
    if(!pcb) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    pcb->reqhandlers->notify = notifyHandler;

    return true;
}

bool pcb_addNotifyHandler(pcb_t* pcb, notify_handler_t notifyHandler) {
    if(!pcb) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    llist_iterator_t* lit = NULL;
    notify_handler_iterator_t* nit = NULL;
    llist_for_each(lit, &pcb->notifyHandlers) {
        nit = llist_item_data(lit, notify_handler_iterator_t, it);
        if(nit->notifyHandler == notifyHandler) {
            break;
        }
        nit = NULL;
    }

    if(nit) {
        return true;
    }

    nit = calloc(1, sizeof(notify_handler_iterator_t));
    if(!nit) {
        return false;
    }
    nit->notifyHandler = notifyHandler;

    llist_append(&pcb->notifyHandlers, &nit->it);

    return true;
}

bool pcb_removeNotifyHandler(pcb_t* pcb, notify_handler_t notifyHandler) {
    if(!pcb) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    llist_iterator_t* lit = NULL;
    notify_handler_iterator_t* nit = NULL;
    llist_for_each(lit, &pcb->notifyHandlers) {
        nit = llist_item_data(lit, notify_handler_iterator_t, it);
        if(nit->notifyHandler == notifyHandler) {
            break;
        }
        nit = NULL;
    }

    if(!nit) {
        return true;
    }
    llist_iterator_take(&nit->it);
    free(nit);

    return true;
}

/**
   @brief
   Set the connection's default serializer.

   @details
   Each new peer will use this serializaer to start with.

   @param con A pointer to a connection object, must be created with @ref connection_create
   @param format User the SERIALIZE_FORMAT macro to create the format id.

   @return
    - true: When a serializer for the specified format is found
    - false: The serializer is not found, no changes are done
 */
bool pcb_setDefaultFormat(pcb_t* pcb, uint32_t format) {
    if(!pcb) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    serialize_handlers_t* new_ser = serialization_getHandlers(format);
    if(!new_ser) {
        /* requested format not found */
        return false;
    }

    pcb->defaultSerializer = new_ser;
    return true;
}

bool pcb_replyBegin(peer_info_t* peer, request_t* req, uint32_t error) {
    if(!peer) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(peer->socketfd == -1) {
        pcb_error = pcb_error_not_connected;
        return false;
    }

    pcb_peer_data_t* pd = peer_getUserData(peer);
    if((pd == NULL) ||
       (pd->serializers == NULL) ||
       (pd->serializers->serialize_reply_begin == NULL)) {
        pcb_error = pcb_error_no_serializer;
        return false;
    }

    pcb_error = pcb_ok;
    return pd->serializers->serialize_reply_begin(peer, req, error);
}

bool pcb_replyEnd(peer_info_t* peer, request_t* req) {
    if(!peer) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(peer->socketfd == -1) {
        pcb_error = pcb_error_not_connected;
        return false;
    }

    pcb_peer_data_t* pd = peer_getUserData(peer);
    if((pd == NULL) ||
       (pd->serializers == NULL) ||
       (pd->serializers->serialize_reply_end == NULL)) {
        pcb_error = pcb_error_no_serializer;
        return false;
    }

    pcb_error = pcb_ok;
    if(pd->serializers->serialize_reply_end(peer, req)) {
        return true;
    }
    return false;
}

bool pcb_writeObjectListBegin(peer_info_t* peer, request_t* req, object_list_type_t type) {
    if(!peer) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(peer->socketfd == -1) {
        pcb_error = pcb_error_not_connected;
        return false;
    }

    pcb_peer_data_t* pd = peer_getUserData(peer);
    if((pd == NULL) ||
       (pd->serializers == NULL) ||
       (pd->serializers->serialize_objectListBegin == NULL)) {
        pcb_error = pcb_error_no_serializer;
        return false;
    }

    pcb_error = pcb_ok;
    return pd->serializers->serialize_objectListBegin(peer, req, type);
}

bool pcb_writeObjectListNext(peer_info_t* peer, request_t* req) {
    if(!peer) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(peer->socketfd == -1) {
        pcb_error = pcb_error_not_connected;
        return false;
    }

    pcb_peer_data_t* pd = peer_getUserData(peer);
    if((pd == NULL) ||
       (pd->serializers == NULL) ||
       (pd->serializers->serialize_objectListNext == NULL)) {
        pcb_error = pcb_error_no_serializer;
        return false;
    }

    pcb_error = pcb_ok;
    return pd->serializers->serialize_objectListNext(peer, req);
}

bool pcb_writeObjectListEnd(peer_info_t* peer, request_t* req) {
    if(!peer) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(peer->socketfd == -1) {
        pcb_error = pcb_error_not_connected;
        return false;
    }

    pcb_peer_data_t* pd = peer_getUserData(peer);
    if((pd == NULL) ||
       (pd->serializers == NULL) ||
       (pd->serializers->serialize_objectListEnd == NULL)) {
        pcb_error = pcb_error_no_serializer;
        return false;
    }

    pcb_error = pcb_ok;
    return pd->serializers->serialize_objectListEnd(peer, req);
}

bool pcb_writeObjectBegin(peer_info_t* peer, request_t* req, object_t* object) {
    if(!peer) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(peer->socketfd == -1) {
        pcb_error = pcb_error_not_connected;
        return false;
    }

    pcb_peer_data_t* pd = peer_getUserData(peer);
    if((pd == NULL) ||
       (pd->serializers == NULL) ||
       (pd->serializers->serialize_objectBegin == NULL)) {
        pcb_error = pcb_error_no_serializer;
        SAH_TRACEZ_ERROR("pcb", "No serializer available");
        return false;
    }

    pcb_error = pcb_ok;
    bool retval = pd->serializers->serialize_objectBegin(peer, req, object);

    return retval;
}

bool pcb_writeObjectEnd(peer_info_t* peer, request_t* req) {
    if(!peer) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(peer->socketfd == -1) {
        pcb_error = pcb_error_not_connected;
        return false;
    }

    pcb_peer_data_t* pd = peer_getUserData(peer);
    if((pd == NULL) ||
       (pd->serializers == NULL) ||
       (pd->serializers->serialize_objectEnd == NULL)) {
        pcb_error = pcb_error_no_serializer;
        return false;
    }

    pcb_error = pcb_ok;
    return pd->serializers->serialize_objectEnd(peer, req);
}

bool pcb_writeParameterListBegin(peer_info_t* peer, request_t* req) {
    if(!peer) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(peer->socketfd == -1) {
        pcb_error = pcb_error_not_connected;
        return false;
    }

    pcb_peer_data_t* pd = peer_getUserData(peer);
    if((pd == NULL) ||
       (pd->serializers == NULL) ||
       (pd->serializers->serialize_parameterListBegin == NULL)) {
        pcb_error = pcb_error_no_serializer;
        return false;
    }

    pcb_error = pcb_ok;
    return pd->serializers->serialize_parameterListBegin(peer, req);
}

bool pcb_writeParameterListNext(peer_info_t* peer, request_t* req) {
    if(!peer) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(peer->socketfd == -1) {
        pcb_error = pcb_error_not_connected;
        return false;
    }

    pcb_peer_data_t* pd = peer_getUserData(peer);
    if((pd == NULL) ||
       (pd->serializers == NULL) ||
       (pd->serializers->serialize_parameterListNext == NULL)) {
        pcb_error = pcb_error_no_serializer;
        return false;
    }

    pcb_error = pcb_ok;
    return pd->serializers->serialize_parameterListNext(peer, req);
}

bool pcb_writeParameterListEnd(peer_info_t* peer, request_t* req) {
    if(!peer) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(peer->socketfd == -1) {
        pcb_error = pcb_error_not_connected;
        return false;
    }

    pcb_peer_data_t* pd = peer_getUserData(peer);
    if((pd == NULL) ||
       (pd->serializers == NULL) ||
       (pd->serializers->serialize_parameterListEnd == NULL)) {
        pcb_error = pcb_error_no_serializer;
        return false;
    }

    pcb_error = pcb_ok;
    return pd->serializers->serialize_parameterListEnd(peer, req);
}

bool pcb_writeParameter(peer_info_t* peer, request_t* req, parameter_t* parameter, bool hideValue) {
    if(!peer) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(peer->socketfd == -1) {
        pcb_error = pcb_error_not_connected;
        return false;
    }

    pcb_peer_data_t* pd = peer_getUserData(peer);
    if((pd == NULL) ||
       (pd->serializers == NULL) ||
       (pd->serializers->serialize_parameter == NULL)) {
        pcb_error = pcb_error_no_serializer;
        return false;
    }

    pcb_error = pcb_ok;
    return pd->serializers->serialize_parameter(peer, req, parameter, hideValue);
}

bool pcb_writeNotification(peer_info_t* peer, request_t* req, notification_t* notification) {
    if(!peer) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(peer->socketfd == -1) {
        pcb_error = pcb_error_not_connected;
        return false;
    }

    pcb_peer_data_t* pd = peer_getUserData(peer);
    if((pd == NULL) ||
       (pd->serializers == NULL) ||
       (pd->serializers->serialize_notification == NULL)) {
        pcb_error = pcb_error_no_serializer;
        return false;
    }

    pcb_error = pcb_ok;
    return pd->serializers->serialize_notification(peer, req, notification);
}

bool pcb_writeFunctionReturnBegin(peer_info_t* peer, request_t* req) {
    if(!peer || !req) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(peer->socketfd == -1) {
        pcb_error = pcb_error_not_connected;
        return false;
    }

    pcb_peer_data_t* pd = peer_getUserData(peer);
    if((pd == NULL) ||
       (pd->serializers == NULL) ||
       (pd->serializers->serialize_functionReturnBegin == NULL)) {
        pcb_error = pcb_error_no_serializer;
        return false;
    }

    pcb_error = pcb_ok;
    return pd->serializers->serialize_functionReturnBegin(peer, req);
}

bool pcb_writeFunctionReturnEnd(peer_info_t* peer, request_t* req) {
    if(!peer || !req) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(peer->socketfd == -1) {
        pcb_error = pcb_error_not_connected;
        return false;
    }

    pcb_peer_data_t* pd = peer_getUserData(peer);
    if((pd == NULL) ||
       (pd->serializers == NULL) ||
       (pd->serializers->serialize_functionReturnEnd == NULL)) {
        pcb_error = pcb_error_no_serializer;
        return false;
    }

    pcb_error = pcb_ok;
    return pd->serializers->serialize_functionReturnEnd(peer, req);
}

bool pcb_writeReturnValue(peer_info_t* peer, request_t* req, const variant_t* retval) {
    if(!peer || !req) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(peer->socketfd == -1) {
        pcb_error = pcb_error_not_connected;
        return false;
    }

    pcb_peer_data_t* pd = peer_getUserData(peer);
    if((pd == NULL) ||
       (pd->serializers == NULL) ||
       (pd->serializers->serialize_functionReturnValue == NULL)) {
        pcb_error = pcb_error_no_serializer;
        return false;
    }

    pcb_error = pcb_ok;
    return pd->serializers->serialize_functionReturnValue(peer, req, retval);
}

bool pcb_writeReturnArgumentsStart(peer_info_t* peer, request_t* req) {
    if(!peer || !req) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(peer->socketfd == -1) {
        pcb_error = pcb_error_not_connected;
        return false;
    }

    pcb_peer_data_t* pd = peer_getUserData(peer);
    if((pd == NULL) ||
       (pd->serializers == NULL) ||
       (pd->serializers->serialize_functionReturnArgumentsStart == NULL)) {
        pcb_error = pcb_error_no_serializer;
        return false;
    }

    pcb_error = pcb_ok;
    return pd->serializers->serialize_functionReturnArgumentsStart(peer, req);
}

bool pcb_writeReturnArguments(peer_info_t* peer, request_t* req, argument_value_list_t* args) {
    if(!peer || !req) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(peer->socketfd == -1) {
        pcb_error = pcb_error_not_connected;
        return false;
    }

    pcb_peer_data_t* pd = peer_getUserData(peer);
    if((pd == NULL) ||
       (pd->serializers == NULL) ||
       (pd->serializers->serialize_functionReturnArguments == NULL)) {
        pcb_error = pcb_error_no_serializer;
        return false;
    }

    pcb_error = pcb_ok;
    return pd->serializers->serialize_functionReturnArguments(peer, req, args);
}

bool pcb_writeFunctionReturn(function_call_t* fcall, const variant_t* retval, argument_value_list_t* args) {
    peer_info_t* peer = fcall_peer(fcall);
    request_t* req = fcall_request(fcall);
    if(!fcall || !peer) {
        return false;
    }

    /* remove cancel handler */
    fcall_setCancelHandler(fcall, NULL);

    /* create reply */
    pcb_replyBegin(peer, req, 0);
    pcb_writeFunctionReturnBegin(peer, req);
    if(retval) {
        pcb_writeReturnValue(peer, req, retval);
    }
    if(!llist_isEmpty(args)) {
        pcb_writeReturnArgumentsStart(peer, req);
        pcb_writeReturnArguments(peer, req, args);
    }
    default_sendErrorList(peer, req);
    pcb_writeFunctionReturnEnd(peer, req);
    pcb_replyEnd(peer, req);
    request_setDone(req);

    return true;
}

bool pcb_writeErrorListBegin(peer_info_t* peer, request_t* req) {
    if(!peer) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(peer->socketfd == -1) {
        pcb_error = pcb_error_not_connected;
        return false;
    }

    pcb_peer_data_t* pd = peer_getUserData(peer);
    if((pd == NULL) ||
       (pd->serializers == NULL) ||
       (pd->serializers->serialize_errorListBegin == NULL)) {
        pcb_error = pcb_error_no_serializer;
        return false;
    }

    pcb_error = pcb_ok;
    return pd->serializers->serialize_errorListBegin(peer, req);
}

bool pcb_writeErrorListNext(peer_info_t* peer, request_t* req) {
    if(!peer) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(peer->socketfd == -1) {
        pcb_error = pcb_error_not_connected;
        return false;
    }

    pcb_peer_data_t* pd = peer_getUserData(peer);
    if((pd == NULL) ||
       (pd->serializers == NULL) ||
       (pd->serializers->serialize_errorListNext == NULL)) {
        pcb_error = pcb_error_no_serializer;
        return false;
    }

    pcb_error = pcb_ok;
    return pd->serializers->serialize_errorListNext(peer, req);
}

bool pcb_writeErrorListEnd(peer_info_t* peer, request_t* req) {
    if(!peer) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(peer->socketfd == -1) {
        pcb_error = pcb_error_not_connected;
        return false;
    }

    pcb_peer_data_t* pd = peer_getUserData(peer);
    if((pd == NULL) ||
       (pd->serializers == NULL) ||
       (pd->serializers->serialize_errorListEnd == NULL)) {
        pcb_error = pcb_error_no_serializer;
        return false;
    }

    pcb_error = pcb_ok;
    return pd->serializers->serialize_errorListEnd(peer, req);
}

bool pcb_writeError(peer_info_t* peer, request_t* req, uint32_t error, const char* description, const char* info) {
    if(!peer) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(peer->socketfd == -1) {
        pcb_error = pcb_error_not_connected;
        return false;
    }

    pcb_peer_data_t* pd = peer_getUserData(peer);
    if((pd == NULL) ||
       (pd->serializers == NULL) ||
       (pd->serializers->serialize_error == NULL)) {
        pcb_error = pcb_error_no_serializer;
        return false;
    }

    pcb_error = pcb_ok;
    return pd->serializers->serialize_error(peer, req, error, description, info);
}

/**
   @brief
   Find the notify request with the specified requestid for the peer.

   @details
   This function looks in the peer notify request list and tries to locate the given requestid.\n

   @param peer The initialized peer structure
   @param requestId The request id we are interested in

   @return
    - A pointer to the request if succesfull
    - NULL if the requestid was not found or an error occurred, pcb_error contains more info about the error
 */
request_t* pcb_findNotifyRequest(peer_info_t* peer, uint32_t requestId) {
    if(!peer) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    pcb_peer_data_t* pd = peer_getUserData(peer);
    if(!pd) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    llist_iterator_t* it = NULL;
    request_t* req = NULL;
    /* search notify requests. */
    if(!req) {
        llist_for_each(it, &pd->notifyRequests) {
            req = llist_item_data(it, request_t, notify_it);
            if(request_id(req) == requestId) {
                break;
            }
            req = NULL;
        }
    }

    /* search recieved requests */
    if(!req) {
        llist_for_each(it, &pd->receivedRequests) {
            req = llist_item_data(it, request_t, it);
            if(request_id(req) == requestId) {
                break;
            }
            req = NULL;
        }
    }

    /* search forwarded requests */
    if(!req) {
        llist_for_each(it, &pd->forwardedRequests) {
            req = llist_item_data(it, request_t, forwarded_it);
            if(request_id(req) == requestId) {
                break;
            }
            req = NULL;
        }
    }

    return req;
}

request_t* pcb_getPendingRequest(peer_info_t* peer, uint32_t requestId) {
    if(!peer) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    pcb_peer_data_t* pd = peer_getUserData(peer);
    if(!pd) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    request_t* req = NULL;
    if(requestId == 0) {
        pd->zero_request.state = request_state_waiting_for_reply;
        return &pd->zero_request;
    } else {
        llist_iterator_t* it = NULL;
        llist_for_each(it, &pd->pendingRequests) {
            req = llist_item_data(it, request_t, it);
            if(request_id(req) == requestId) {
                break;
            }
            req = NULL;
        }
    }

    if(!req) {
        return NULL;
    }

    if((req->state == request_state_canceled) ||
       (req->state == request_state_done) ||
       (req->state == request_state_destroy) ||
       (req->state == request_state_destroyed)) {
        return NULL;
    }

    return req;
}

request_t* pcb_firstPendingRequest(peer_info_t* peer) {
    if(!peer) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    pcb_peer_data_t* pd = peer_getUserData(peer);
    if(!pd) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    llist_iterator_t* it = llist_first(&pd->pendingRequests);
    if(it) {
        return llist_item_data(it, request_t, it);
    }

    return NULL;
}

request_t* pcb_nextPendingRequest(peer_info_t* peer, request_t* reference) {
    if(!peer) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }


    pcb_peer_data_t* pd = peer_getUserData(peer);
    if(!pd) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    if(reference->it.list != &pd->pendingRequests) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    llist_iterator_t* it = llist_iterator_next(&reference->it);
    if(it) {
        return llist_item_data(it, request_t, it);
    }

    return NULL;
}

/**
   @brief
   Get the root object of the peer.

   @details
   This function retrieves the root object of the peer.\n

   If the request uses an object cache, the root object is retrieved out of the connection information.\n
   Otherwise the temporary root object is returned for the specified request.\n

   @param connection The current connection
   @param req The request

   @return
    - A pointer to the root object
    - NULL if an error occurred, pcb_error contains more info about the error
        - peer, connection or req are NULL
        - No root object available
 */
object_t* pcb_getRootObject(pcb_t* pcb, request_t* req) {
    if(!pcb || !req) {
        SAH_TRACEZ_ERROR("pcb", "No peer info, no connection info or no request specified");
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    object_t* root = NULL;
    if(request_useObjectCache(req)) {
        root = &pcb->cache.root;
        if(!root) {
            SAH_TRACEZ_ERROR("pcb", "No root object available");
        }
    } else {
        request_object_t* object_req = llist_item_data(req, request_object_t, request);
        if(!object_req->datamodel) {
            object_req->datamodel = (datamodel_t*) calloc(1, sizeof(datamodel_t));
            if(!object_req->datamodel) {
                return NULL;
            }
            datamodel_initialize(object_req->datamodel);
        }
        root = &object_req->datamodel->root;
        if(!root) {
            SAH_TRACEZ_ERROR("pcb", "No root object available");
        }
    }

    return root;
}

bool pcb_addReceivedRequest(peer_info_t* peer, request_t* req) {
    if(!peer || !req) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    pcb_peer_data_t* pd = peer_getUserData(peer);
    if(!pd) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    llist_append(&pd->receivedRequests, &req->it);
    req->state = request_state_received;

    pcb_error = pcb_ok;
    return true;
}

bool pcb_broadcastEnable(peer_info_t* peer, bool enable) {
    if(!peer) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    pcb_peer_data_t* pd = peer_getUserData(peer);
    if(!pd) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    pd->broadcast = enable;
    return true;
}

void pcb_getNotifyRequests(peer_info_t* peer, request_list_t* list) {
    if(!peer) {
        pcb_error = pcb_error_invalid_parameter;
        return;
    }

    pcb_peer_data_t* pd = peer_getUserData(peer);
    if(!pd) {
        pcb_error = pcb_error_invalid_parameter;
        return;
    }

    llist_iterator_t* it = NULL;
    request_t* req = NULL;

    llist_for_each(it, &pd->notifyRequests) {
        req = llist_item_data(it, request_t, notify_it);
        request_list_add(list, req);
    }
}

void pcb_getForwardedRequests(peer_info_t* peer, request_list_t* list) {
    if(!peer) {
        pcb_error = pcb_error_invalid_parameter;
        return;
    }

    pcb_peer_data_t* pd = peer_getUserData(peer);
    if(!pd) {
        pcb_error = pcb_error_invalid_parameter;
        return;
    }

    llist_iterator_t* it = NULL;
    request_t* req = NULL;

    llist_for_each(it, &pd->forwardedRequests) {
        req = llist_item_data(it, request_t, forwarded_it);
        if(request_attributes(req) & (request_notify_all)) {
            continue;
        }
        request_list_add(list, req);
    }
}

/**
   @brief
   Set the peer serialize information.

   @details
   This function sets the peer serialization info.\n A serializer can add custom data to the peer.\n
   It is the serializers reponsibility to delete any allocated memory.\n

   @param peer The peer structure for which you want to set the deserialization information
   @param info A pointer to the new serialization information

   @return
    - true if the info was set correctly
    - false if an error occurred, pcb_error contains more info about the error
 */
bool pcb_setSerializeInfo(peer_info_t* peer, void* info) {
    if(!peer) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    pcb_peer_data_t* pd = peer_getUserData(peer);
    if(!pd) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    pd->serializeInfo = info;

    return true;
}

/**
   @brief
   Get the peer serialize information.

   @details
   This function gets the peer serialization info.\n

   @param peer The peer structure for which you want to get the deserialization information

   @return
    - A pointer to the serialization information
    - NULL if an error occurred, pcb_error contains more info about the error
 */
void* pcb_getSerializeInfo(peer_info_t* peer) {
    if(!peer) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    pcb_peer_data_t* pd = peer_getUserData(peer);
    if(!pd) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    return pd->serializeInfo;
}

/**
   @brief
   Set the peer deserialize information.

   @details
   This function sets the peer deserialization info.\n

   @param peer The peer structure for which you want to set the deserialization information
   @param info A pointer to the new deserialization information

   @return
    - true if the info was set correctly
    - false if an error occurred, pcb_error contains more info about the error
 */
bool pcb_setDeserializeInfo(peer_info_t* peer, void* info) {
    if(!peer) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    pcb_peer_data_t* pd = peer_getUserData(peer);
    if(!pd) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    pd->deserializeInfo = info;

    return true;
}

/**
   @brief
   Get the peer deserialize information.

   @details
   This function gets the peer deserialization info.\n

   @param peer The peer structure for which you want to get the deserialization information

   @return
    - A pointer to the deserialization information
    - NULL if an error occurred, pcb_error contains more info about the error
 */
void* pcb_getDeserializeInfo(peer_info_t* peer) {
    if(!peer) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    pcb_peer_data_t* pd = peer_getUserData(peer);
    if(!pd) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    return pd->deserializeInfo;
}

/**
   @}
 */
