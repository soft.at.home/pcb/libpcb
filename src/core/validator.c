/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>

#include <pcb/common/error.h>
#include <pcb/utils/variant_list.h>

#include "validator_priv.h"
#include "parameter_priv.h"

/**
   @file
   @brief
   Implementation of validator functions
 */

/**
   @brief
   Standard parameter enumeration validation function

   @details
   This function will check that the value of a parameter is in the set of values.

   @param parameter The parameter that is validated
   @param validationData The validation data, in this case it will contain the set of values

   @return
    - false: the new value of the parameter is not in the defined set of values
    - true: the value of the parameter is in the set of defined values
 */
static bool param_validate_enum(parameter_t* parameter, void* validationData) {
    parameter_validator_t* validator = (parameter_validator_t*) validationData;

    if(validator->type != parameter_validator_enum) {
        pcb_error = pcb_error_validator_mismatch;
        return false;
    }

    if(variant_list_contains(validator->data.values, parameter_getModifiedValue(parameter))) {
        return true;
    }

    pcb_error = pcb_error_value_enum;
    return false;
}

/**
   @brief
   Standard parameter range validation function

   @details
   This function will check that the value of a parameter is in a certain range.\n
   For string parameters the length of the string is checked.

   @param parameter The parameter that is validated
   @param validationData The validation data, in this case it will contain the minimum and the maximum value

   @return
    - false: the new value of the parameter is not in the defined range
    - true: the value of the parameter is in the defined range
 */
static bool param_validate_range(parameter_t* parameter, void* validationData) {
    parameter_validator_t* validator = (parameter_validator_t*) validationData;

    if(validator->type != parameter_validator_range) {
        pcb_error = pcb_error_validator_mismatch;
        return false;
    }

    if(parameter_type(parameter) == parameter_type_string) {  /* for strings very the length*/
        uint32_t minimum;
        uint32_t maximum;
        if(!variant_toUInt32(&minimum, &validator->data.range.minimum)) {
            pcb_error = pcb_error_type_mismatch;
            return false;
        }
        if(!variant_toUInt32(&maximum, &validator->data.range.maximum)) {
            pcb_error = pcb_error_type_mismatch;
            return false;
        }
        /* the following line is valid:
           parameters store theire data in the closest matching type. for strings this is a string type
         */
        uint32_t size = string_length(variant_da_string(parameter_getModifiedValue(parameter)));
        if((size < minimum) || (size > maximum)) {
            pcb_error = pcb_error_string_length;
            return false;
        }
    } else {
        int32_t result;
        if(!variant_compare(parameter_getModifiedValue(parameter), &validator->data.range.minimum, &result)) {
            pcb_error = pcb_error_type_mismatch;
            return false;
        }
        if(result < 0) {
            /* below minimum */
            pcb_error = pcb_error_value_range;
            return false;
        }
        if(!variant_compare(parameter_getModifiedValue(parameter), &validator->data.range.maximum, &result)) {
            pcb_error = pcb_error_type_mismatch;
            return false;
        }
        if(result > 0) {
            /* above minimum */
            pcb_error = pcb_error_value_range;
            return false;
        }
    }
    return true;
}

/**
   @brief
   Standard parameter minimum validation function

   @details
   This function will check that the value of a parameter is in at least the specified value\n
   For string parameters the length of the string is checked.

   @param parameter The parameter that is validated
   @param validationData The validation data, in this case it will contain the minumum value

   @return
    - false: the new value of the parameter is smaller then the defined value
    - true: the value of the parameter is equal or higher then the defined value
 */
static bool param_validate_minimum(parameter_t* parameter, void* validationData) {
    parameter_validator_t* validator = (parameter_validator_t*) validationData;

    if(validator->type != parameter_validator_minimum) {
        pcb_error = pcb_error_validator_mismatch;
        return false;
    }

    if(parameter_type(parameter) == parameter_type_string) {  /* for strings very the length */
        uint32_t minimum;
        if(!variant_toUInt32(&minimum, &validator->data.minimum)) {
            pcb_error = pcb_error_type_mismatch;
            return false;
        }
        /* the following line is valid:
           parameters store theire data in the closest matching type. for strings this is a string type
         */
        uint32_t size = string_length(variant_da_string(parameter_getModifiedValue(parameter)));
        if(size < minimum) {
            pcb_error = pcb_error_string_length;
            return false;
        }
    } else {
        int32_t result;
        if(!variant_compare(parameter_getModifiedValue(parameter), &validator->data.minimum, &result)) {
            pcb_error = pcb_error_type_mismatch;
            return false;
        }
        if(result < 0) {
            /* below minimum */
            pcb_error = pcb_error_value_minimum;
            return false;
        }
    }
    return true;
}

/**
   @brief
   Standard parameter maximum validation function

   @details
   This function will check that the value of a parameter is in not higher then a specified value\n
   For string parameters the length of the string is checked.

   @param parameter The parameter that is validated
   @param validationData The validation data, in this case it will contain the maximum value

   @return
    - false: the new value of the parameter is higher then the defined value
    - true: the value of the parameter is equal or less then the defined value
 */
static bool param_validate_maximum(parameter_t* parameter, void* validationData) {
    parameter_validator_t* validator = (parameter_validator_t*) validationData;

    if(validator->type != parameter_validator_maximum) {
        pcb_error = pcb_error_type_mismatch;
        return false;
    }

    if(parameter_type(parameter) == parameter_type_string) {  /* for strings very the length */
        uint32_t maximum;
        if(!variant_toUInt32(&maximum, &validator->data.maximum)) {
            pcb_error = pcb_error_type_mismatch;
            return false;
        }
        /* the following line is valid:
           parameters store theire data in the closest matching type. for strings this is a string type
         */
        uint32_t size = string_length(variant_da_string(parameter_getModifiedValue(parameter)));
        if(size > maximum) {
            pcb_error = pcb_error_string_length;
            return false;
        }
    } else {
        int32_t result;
        if(!variant_compare(parameter_getModifiedValue(parameter), &validator->data.maximum, &result)) {
            pcb_error = pcb_error_type_mismatch;
            return false;
        }
        if(result > 0) {
            /* above minimum */
            pcb_error = pcb_error_value_maximum;
            return false;
        }
    }
    return true;
}

/**
   @ingroup pcb_core_validator
   @brief
   Create a custom validator

   @details
   This function will allocate a parameter_validator_t structure and initialize the fields of this structure.\n
   The validation data can contain anything and will be passed to the validation function when validation of the parameter is needed.

   @param handler pointer to the validation function.
   @param validationData user defined data, that is needed to validate the value of the parameter

   @return
    - pointer to a parameter_validator_t structure
    - NULL an error has occured
 */
parameter_validator_t* param_validator_create_custom(param_validation_handler_t handler, void* validationData) {
    parameter_validator_t* validator = (parameter_validator_t*) calloc(1, sizeof(parameter_validator_t));

    if(!validator) {
        pcb_error = pcb_error_no_memory;
        return NULL;
    }

    validator->type = parameter_validator_custom;
    validator->validate = handler;
    validator->data.data = validationData;

    return validator;
}

/**
   @ingroup pcb_core_validator
   @brief
   Create a standard enumeration validator

   @details
   This function will allocate a parameter_validator_t structure and initialize the fields of this structure.\n

   @param values the set of allowed values.

   @return
    - pointer to a parameter_validator_t structure
    - NULL an error has occured
 */
parameter_validator_t* param_validator_create_enum(variant_list_t* values) {
    if(!values) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    parameter_validator_t* validator = (parameter_validator_t*) calloc(1, sizeof(parameter_validator_t));

    if(!validator) {
        pcb_error = pcb_error_no_memory;
        return NULL;
    }

    validator->type = parameter_validator_enum;
    validator->validate = param_validate_enum;
    validator->data.values = values;

    return validator;
}

/**
   @ingroup pcb_core_validator
   @brief
   Create a standard range validator

   @details
   This function will allocate a parameter_validator_t structure and initialize the fields of this structure.\n

   @param minimum the minimum value
   @param maximum the maximum value

   @return
    - pointer to a parameter_validator_t structure
    - NULL an error has occured
 */
parameter_validator_t* param_validator_create_range(variant_t* minimum, variant_t* maximum) {
    if(!minimum || !maximum) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    parameter_validator_t* validator = (parameter_validator_t*) calloc(1, sizeof(parameter_validator_t));

    if(!validator) {
        pcb_error = pcb_error_no_memory;
        return NULL;
    }

    validator->type = parameter_validator_range;
    validator->validate = param_validate_range;
    variant_copy(&validator->data.range.minimum, minimum);
    variant_copy(&validator->data.range.maximum, maximum);

    return validator;
}

/**
   @ingroup pcb_core_validator
   @brief
   Create a standard minimum validator

   @details
   This function will allocate a parameter_validator_t structure and initialize the fields of this structure.\n

   @param minimum the minimum value

   @return
    - pointer to a parameter_validator_t structure
    - NULL an error has occured
 */
parameter_validator_t* param_validator_create_minimum(variant_t* minimum) {
    if(!minimum) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    parameter_validator_t* validator = (parameter_validator_t*) calloc(1, sizeof(parameter_validator_t));

    if(!validator) {
        pcb_error = pcb_error_no_memory;
        return NULL;
    }

    validator->type = parameter_validator_minimum;
    validator->validate = param_validate_minimum;
    variant_copy(&validator->data.minimum, minimum);

    return validator;
}

/**
   @ingroup pcb_core_validator
   @brief
   Create a standard maximum validator

   @details
   This function will allocate a parameter_validator_t structure and initialize the fields of this structure.\n

   @param maximum the maximum value

   @return
    - pointer to a parameter_validator_t structure
    - NULL an error has occured
 */
parameter_validator_t* param_validator_create_maximum(variant_t* maximum) {
    if(!maximum) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    parameter_validator_t* validator = (parameter_validator_t*) calloc(1, sizeof(parameter_validator_t));

    if(!validator) {
        pcb_error = pcb_error_no_memory;
        return NULL;
    }

    validator->type = parameter_validator_maximum;
    validator->validate = param_validate_maximum;
    variant_copy(&validator->data.maximum, maximum);

    return validator;
}

/**
   @ingroup pcb_core_validator
   @brief
   Destroy and free a parameter validator structure

   @details
   This function will free up the memory used to for the validator.\n
   Do not destroy a validator that is attached to a parameter. First set the parameter's validator to NULL and then destroy it.\n
   Parameters that get destroyed when they have a validator attached will destroy the validator by themself.

   @param validator the validator to destroy.
 */
void param_validator_destroy(parameter_validator_t* validator) {
    if(!validator) {
        pcb_error = pcb_error_invalid_parameter;
        return;
    }

    switch(validator->type) {
    case parameter_validator_enum:
        variant_list_cleanup(validator->data.values);
        free(validator->data.values);
        break;
    case parameter_validator_range:
        variant_cleanup(&validator->data.range.minimum);
        variant_cleanup(&validator->data.range.maximum);
        break;
    case parameter_validator_minimum:
        variant_cleanup(&validator->data.minimum);
        break;
    case parameter_validator_maximum:
        variant_cleanup(&validator->data.maximum);
        break;
    case parameter_validator_custom:     /* do no cleanup for custom validators */
    default:
        break;
    }

    free(validator);
}

/**
   @ingroup pcb_core_validator
   @brief
   Create an object validator.

   @details
   This function will allocate a object_validator_t structure and initialize the fields of this structure.\n
   The validation data can contain anything and will be passed to the validation function when validation of the object is needed.

   @param handler pointer to the validation function.
   @param validationData user defined data, that is needed to validate the value of the parameter

   @return
    - pointer to a object_validator_t structure
    - NULL an error has occured
 */
object_validator_t* object_validator_create(object_validation_handler_t handler, void* validationData) {
    object_validator_t* validator = (object_validator_t*) calloc(1, sizeof(object_validator_t));

    if(!validator) {
        pcb_error = pcb_error_no_memory;
        return NULL;
    }

    validator->validate = handler;
    validator->data = validationData;

    return validator;
}

/**
   @ingroup pcb_core_validator
   @brief
   Destroy and free a object validator structure

   @details
   This function will free up the memory used to for the validator.\n
   Do not destroy a validator that is attached to an object. First set the objects's validator to NULL and then destroy it.\n
   Objects that get destroyed when they have a validator attached will destroy the validator by themself.

   @param validator the validator to destroy.
 */
void object_validator_destroy(object_validator_t* validator) {
    if(!validator) {
        pcb_error = pcb_error_invalid_parameter;
        return;
    }

    validator->reference--;
    if(validator->reference == 0) {
        free(validator);
    }
}

/**
   @ingroup pcb_core_validator
   @brief
   Get the validator type of a parameter validator

   @details
   This function will return the validation type of a parameter validator.

   @param validator the validator

   @return
    - the parameter validator type. See @ref parameter_validator_type_t
 */
parameter_validator_type_t param_validator_type(parameter_validator_t* validator) {
    if(!validator) {
        pcb_error = pcb_error_invalid_parameter;
        return parameter_validator_custom;
    }

    return validator->type;
}

/**
   @ingroup pcb_core_validator
   @brief
   Get the minimum value of a parameter validator

   @details
   This function will return the minimum value of a range or minimum validator. For all other kinds of
   validators this function will return a NULL pointer

   @param validator the validator

   @return
    - pointer to a variant
    - NULL if the validator type is not a range or minimum validator
 */
const variant_t* param_validator_minimum(parameter_validator_t* validator) {
    if(!validator) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    if((validator->type != parameter_validator_range) &&
       (validator->type != parameter_validator_minimum)) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    if(validator->type == parameter_validator_range) {
        return &validator->data.range.minimum;
    } else {
        return &validator->data.minimum;
    }
}

/**
   @ingroup pcb_core_validator
   @brief
   Get the maximum value of a parameter validator

   @details
   This function will return the maximum value of a range or maximum validator. For all other kinds of
   validators this function will return a NULL pointer

   @param validator the validator

   @return
    - pointer to a variant
    - NULL if the validator type is not a range or maximum validator
 */
const variant_t* param_validator_maximum(parameter_validator_t* validator) {
    if(!validator) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    if((validator->type != parameter_validator_range) &&
       (validator->type != parameter_validator_maximum)) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    if(validator->type == parameter_validator_range) {
        return &validator->data.range.maximum;
    } else {
        return &validator->data.maximum;
    }
}

/**
   @ingroup pcb_core_validator
   @brief
   Get the set of values of a parameter validator

   @details
   This function will return the set of values of enumeration validator. For all other kinds of
   validators this function will return a NULL pointer

   @param validator the validator

   @return
    - pointer to a variant list containing all possible values
    - NULL if the validator type is not a enumeration validator
 */
variant_list_t* param_validator_values(parameter_validator_t* validator) {
    if(!validator) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    if(validator->type != parameter_validator_enum) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    return validator->data.values;
}

/**
   @ingroup pcb_core_validator
   @brief
   Get the custom validator's validation data

   @details
   This function will return the pointer to the custom validator's validation data. For all other kinds
   of parameter validators this function will return a NULL pointer.

   @param validator the validator

   @return
    - pointer to the data
    - NULL if the validator type is not a custom validator
 */
void* param_validator_data(parameter_validator_t* validator) {
    if(!validator) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    if(validator->type != parameter_validator_custom) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    return &validator->data.data;
}
