/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include <debug/sahtrace.h>

#include <pcb/common/error.h>
#include <pcb/utils/variant_map.h>
#include <pcb/utils/variant_list.h>
#include <pcb/core/function.h>

#include "parameter_priv.h"
#include "function_priv.h"
#include "core_priv.h"
#include "request_priv.h"

static void function_acl(const function_t* function, llist_t* ACL) {
    object_acl(function_owner(function), ACL);

    if(!llist_isEmpty(&function->ACL)) {
        llist_iterator_t* it = NULL;
        ACL_t* acl = NULL;
        llist_for_each(it, &function->ACL) {
            acl = llist_item_data(it, ACL_t, it);
            aclSet(ACL, acl->id, acl->acl_flags);
        }
    }
}

static void fcall_request_destroyed(request_t* req, void* userdata) {
    function_call_t* fcall = (function_call_t*) userdata;
    if(fcall) {
        fcall->source = NULL;
        fcall->req = NULL;
        request_setData(req, NULL);
        request_setDestroyHandler(req, NULL);

        if(fcall->cancelExecution) {
            fcall->cancelExecution(fcall, fcall->userdata);
        }
        free(fcall);
    }
}

function_t* function_allocate(const char* name, const char* typeName, function_type_t returnType, uint32_t attributes) {
    char* t = NULL;
    size_t size = 0;
    function_def_t* funcdef = NULL;
    function_t* func = NULL;

    if(!name || !(*name)) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    size = sizeof(function_def_t) + sizeof(function_t) + strlen(name) + 1;
    if(typeName) {
        size += strlen(typeName) + 1;
    }
    funcdef = (function_def_t*) calloc(1, size);
    if(!funcdef) {
        pcb_error = pcb_error_no_memory;
        goto error;
    }

    func = (function_t*) (((void*) funcdef) + sizeof(function_def_t));
    func->definition = funcdef;

    if(!llist_iterator_initialize(&func->it)) {
        goto error_free;
    }

    if(!llist_initialize(&funcdef->arguments)) {
        goto error_free;
    }

    /* verify the name
       object_verify_name characters not allowed are : . / $ only numeric
     */
    if(!isValidNameChar(name)) {
        goto error_free;
    }

    /* set the name */
    t = (char*) funcdef;
    t += sizeof(function_def_t) + sizeof(function_t);
    strcpy(t, name);

    if(typeName) {
        t += strlen(name) + 1;
        strcpy(t, typeName);
    }

    /* set the attributes */
    funcdef->attributes = attributes;
    /* set the return type */
    funcdef->returnType = returnType;

    funcdef->refcount = 1;

    return func;

error_free:
    free(funcdef);
error:
    return NULL;
}


/**
   @ingroup pcb_core_functions
   @{
 */

/**
   @brief
   Create a function and add it to the specified object

   @details
   This function will create a function definition and add it to the specified object.\n
   This functions allocates memory for the function definition and initializes the structure.\n
   It also checks that the name is valid. (no . / $ only numeric)\n
   \n
   When the object that owns the function is deleted, the function will be deleted as well. When you
   need to remove the function manually use @ref function_delete.

   @param object The object that owns the function
   @param name Name of the function. The name can not contain ".", "/","$" or be numeric and must be unique within the object
   @param returnType The function return type. See @ref function_type_t
   @param attributes The function attributes

   @return
    - pointer to the created function: Creation was successful
    - NULL: An error has occurred
        - object is NULL
        - name is a NULL pointer or an empty string
        - memory allocation failed
        - initialization failed
        - the name is not valid
 */
function_t* function_create(object_t* object, const char* name, function_type_t returnType, uint32_t attributes) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }
    function_t* func = function_allocate(name, NULL, returnType, attributes);
    if(!func) {
        return NULL;
    }
    if(!object_addFunction(object, func)) {
        function_delete(func);
        return NULL;
    }
    return func;
}

/**
   @brief
   Create a function, returning a custom type, and add it to the specified object

   @details
   This function will create a function definition and add it to the specified object.\n
   This functions allocates memory for the function definition and initializes the structure.\n
   It also checks that the name is valid. (no . / $ only numeric)\n
   \n
   When the object that owns the function is deleted, the function will be deleted as well. When you
   need to remove the function manually use @ref function_delete.

   @param object The object that owns the function
   @param name Name of the function. The name can not contain ".", "/","$" or be numeric and must be unique within the object
   @param returnType The name of the return type. The function return type will be function_type_custom
   @param attributes The function attributes

   @return
    - pointer to the created function: Creation was successful
    - NULL: An error has occurred
        - object is NULL
        - name is a NULL pointer or an empty string
        - memory allocation failed
        - initialization failed
        - the name is not valid
 */
function_t* function_createCustomType(object_t* object, const char* name, const char* returnType, const uint32_t attributes) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }
    function_t* func = function_allocate(name, returnType, function_type_custom, attributes);
    if(!func) {
        return NULL;
    }
    if(!object_addFunction(object, func)) {
        function_delete(func);
        return NULL;
    }
    return func;
}

function_t* function_copy(object_t* destobject, const function_t* srcFunction) {
    if(!destobject || !srcFunction) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    function_t* dstFunction = (function_t*) calloc(1, sizeof(function_t));
    if(!dstFunction) {
        return NULL;
    }
    dstFunction->definition = srcFunction->definition;
    dstFunction->handler = srcFunction->handler;
    dstFunction->definition->refcount++;
    if(!object_addFunction(destobject, dstFunction)) {
        function_delete(dstFunction);
        return NULL;
    }
    function_setACL(dstFunction, function_getACL(srcFunction));

    return dstFunction;
}

/**
   @brief
   Delete a function

   @details
   This function will delete a function definition and remove from the object owing he function.\n
   All memory allocated for the function will be freed\n
   All arguments added to the function will be deleted as well.

   @param function Pointer to the function_t structure
 */
void function_delete(function_t* function) {
    if(!function) {
        pcb_error = pcb_error_invalid_parameter;
        return;
    }

    function_def_t* def = function->definition;
    bool free_function = false;

    llist_iterator_take(&function->it);
    free(function->plugin_name);
    aclDestroy(&function->ACL);

    if(function->destroy) {
        function->destroy(function);
    }

    free_function = ( function != ((void*) def) + sizeof(function_def_t));
    def->refcount--;
    if(def->refcount == 0) {
#ifdef PCB_HELP_SUPPORT
        free(def->description);
#endif

        function_argument_t* arg = function_firstArgument(function);
        while(arg) {
            argument_delete(arg);
            arg = function_firstArgument(function);
        }

        free(def);
    }

    if(free_function) {
        free(function);
    }

    return;
}

function_t* function_base(function_t* function) {
    function_t* base = (function_t*) (((void*) function->definition) + sizeof(function_def_t));
    return base;
}

bool function_setUserData(function_t* function, void* data) {
    if(!function) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    function->userdata = data;

    return true;
}

void* function_getUserData(function_t* function) {
    if(!function) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    return function->userdata;
}

bool function_setDestroyHandler(function_t* function, function_destroy_handler_t fn) {
    if(!function) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    function->destroy = fn;

    return true;
}

object_t* function_owner(const function_t* function) {
    if(!function) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    llist_t* funclist = function->it.list;
    object_t* owner = llist_item_data(funclist, object_t, functions);

    return owner;
}

/**
   @brief
   Get the function name

   @details
   Returns the function name. The returned pointer must no be freed.

   @param function Pointer to the function_t structure

   @return
    - the function name
 */
const char* function_name(function_t* function) {
    if(!function) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }
    char* name = (char*) function->definition;
    name += sizeof(function_def_t) + sizeof(function_t);
    return name;
}

/**
   @brief
   Get the function attributes

   @details
   Returns the function attributes..

   @param function Pointer to the function_t structure

   @return
    - the function attributes
 */
function_attribute_t function_attributes(function_t* function) {
    if(!function || !function->definition) {
        pcb_error = pcb_error_invalid_parameter;
        return 0;
    }

    return function->definition->attributes;
}

/**
   @brief
   Get the function return type

   @details
   Returns the function return type. When the type is function_type_custom you can get the name
   of the type using @ref function_typeName.

   @param function Pointer to the function_t structure

   @return
    - the function type
 */
function_type_t function_type(function_t* function) {
    if(!function) {
        pcb_error = pcb_error_invalid_parameter;
        return 0;
    }

    return function->definition->returnType;
}

/**
   @brief
   Get the function return type name

   @details
   Returns the function return type name..

   @param function Pointer to the function_t structure

   @return
    - the function return type name
 */
const char* function_typeName(function_t* function) {
    if(!function) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    switch(function->definition->returnType) {
    case function_type_void:
        return "void";
        break;
    case function_type_string:
        return "string";
        break;
    case function_type_int8:
        return "int8";
        break;
    case function_type_int16:
        return "int16";
        break;
    case function_type_int32:
        return "int32";
        break;
    case function_type_uint8:
        return "uint8";
        break;
    case function_type_uint16:
        return "uint16";
        break;
    case function_type_uint32:
        return "uint32";
        break;
    case function_type_bool:
        return "bool";
        break;
    case function_type_date_time:
        return "datetime";
        break;
    case function_type_list:
        return "list";
        break;
    case function_type_custom: {
        char* type = (char*) function->definition;
        type += sizeof(function_def_t) + sizeof(function_t);
        type += strlen(type) + 1;
        return type;
    }
    break;
    case function_type_file_descriptor:
        return "fd";
        break;
    case function_type_byte_array:
        return "byte_array";
        break;
    case function_type_variant:
        return "variant";
        break;
    case function_type_double:
        return "double";
        break;
    default:
        return "unknown";
        break;
    }
}

/**
   @brief
   Set the function implementation handler

   @details
   Set a the real function implementation pointer..

   @param function Pointer to the function_t structure
   @param funcHandler Pointer to the function containing the function implementation

   @return
    - true the handler is set
    - false failed to set the handler
 */
bool function_setHandler(function_t* function, function_handler_t funcHandler) {
    if(!function) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    function->handler = funcHandler;
    return true;
}
/**
   @brief
   Get the function implementation handler

   @param function Pointer to the function_t structure
   @return Pointer to the function containing the function implementation or NULL if not available
 */
function_handler_t function_getHandler(function_t* function) {
    if(!function) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    return function->handler;
}

/**
   @brief
   Indicates that the function can handle variadic arguments

   @details
   Altough each function can receive a variadic list of arguments, this flag will indicate
   that the function implementation is really handling a variadic list of arguments

   @param function Pointer to the function_t structure
   @param enable When set to true, indicates the implementation can handle a variadic argument list

   @return
    - true the flag is set
    - false failed to set the flag
 */
bool function_setVariadic(function_t* function, bool enable) {
    if(!function) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(enable) {
        function->definition->attributes |= function_attr_variadic;
    } else {
        function->definition->attributes &= ~function_attr_variadic;
    }
    return true;
}

/**
   @brief
   Checks that the function is implemented

   @details
   Check that the function handler is set. This function will only work on local objects.
   For remote object this function will always return false.

   @param function Pointer to the function_t structure

   @return
    - true the handler is set and the function is implemented
    - false no handler is set
 */
bool function_isImplemented(function_t* function) {
    if(!function) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    return (function->handler != NULL);
}

/**
   @brief
   Checks that the functions can handle variadic arguments

   @details
   Checks that the functions can handle variadic arguments

   @param function Pointer to the function_t structure

   @return
    - true the function is capable of handling variadic list of arguments
    - false the function is not capable of handling variadic list of arguments
 */
bool function_isVariadic(function_t* function) {
    if(!function) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    return (function->definition->attributes & function_attr_variadic);
}

/**
   @brief
   Check that the function contains an access control list

   @details
   This function will check that there is an access control list attached to the function.

   @param function pointer to a function

   @return
    - true an ACL is available
    - false no acl is available
 */
bool function_hasAcl(function_t* function) {
    if(!function) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(llist_isEmpty(&function->ACL)) {
        return false;
    } else {
        return true;
    }
}

const llist_t* function_getACL(const function_t* function) {
    if(!function) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    return &function->ACL;
}

void function_getCalculatedACL(const function_t* function, llist_t* ACL) {
    if(!function || !ACL) {
        pcb_error = pcb_error_invalid_parameter;
        return;
    }

    acl_list_destroy(ACL);
    function_acl(function, ACL);
}


bool function_setACL(function_t* function, const llist_t* acl) {
    if(!function || !acl) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    ACL_t* a = NULL;
    llist_iterator_t* it = NULL;
    llist_for_each(it, acl) {
        a = llist_item_data(it, ACL_t, it);
        function_aclSet(function, a->id, a->acl_flags);
    }

    return true;
}

/**
   @brief
   Modify an access control of the function

   @details
   This function will modify an existing access control that was already added to the function.

   @param function pointer to a function
   @param id user or group id
   @param flags identifies that the id is a user or group id  and the access wanted

   @return
    - true: the acl is updated.
    - false: failed to update the acl
 */
bool function_aclSet(function_t* function, uint32_t id, uint16_t flags) {
    if(!function) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    return aclSet(&function->ACL, id, flags);
}

/**
   @brief
   Add an access control to the function

   @details
   This function will add an access control to the function. A check is performed to see if there
   is already an access control available for the function. If it is not available a new one will be added
   otherwise it will modify the existing one.

   @param function pointer to a function
   @param id user or group id
   @param flags identifies that the id is a user or group id  and the access that needs to be added

   @return
    - true: the acl is updated.
    - false: failed to update the acl
 */
bool function_aclAdd(function_t* function, uint32_t id, uint16_t flags) {
    if(!function) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    llist_t ACL;
    llist_initialize(&ACL);
    function_acl(function, &ACL);

    ACL_t* acl = aclFind(&ACL, id, flags);
    if(acl) {
        flags |= acl->acl_flags;
    }
    llist_iterator_t* it = llist_takeFirst(&ACL);
    while(it) {
        acl = llist_item_data(it, ACL_t, it);
        free(acl);
        it = llist_takeFirst(&ACL);
    }
    return aclSet(&function->ACL, id, flags);
}

/**
   @brief
   Removes an access control from the function

   @details
   This function will remove an access control from the function. A check is performed to see if there
   is already an access control available for the function. If it is not available a new one will be added
   otherwise it will modify the existing one.

   @param function pointer to a function
   @param id user or group id
   @param flags identifies that the id is a user or group id and the access that needs to be deleted

   @return
    - true: the acl is updated.
    - false: failed to update the acl
 */
bool function_aclDel(function_t* function, uint32_t id, uint16_t flags) {
    if(!function) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    llist_t ACL;
    llist_initialize(&ACL);
    function_acl(function, &ACL);
    bool group = false;
    if(flags & acl_group_id) {
        group = true;
    }

    ACL_t* acl = aclFind(&ACL, id, flags);
    if(acl) {
        flags = acl->acl_flags & ~flags;
    } else {
        flags = 0;
    }
    if(group) {
        flags |= acl_group_id;
    }
    llist_iterator_t* it = llist_takeFirst(&ACL);
    while(it) {
        acl = llist_item_data(it, ACL_t, it);
        free(acl);
        it = llist_takeFirst(&ACL);
    }
    return aclSet(&function->ACL, id, flags);
}

/**
   @brief
   Check that the given userid can read the function

   @details
   Checks the ACL to see that the given userid can read the function

   @param function pointer to a function
   @param uid user id

   @return
    - true: the user or group can read
    - false: the user and group are not allowed to read
 */
bool function_canRead(function_t* function, uint32_t uid) {
    if(!function) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(object_isRemote(function_owner(function)) || (uid == 0)) {
        return true;
    }

    llist_t ACL;
    llist_initialize(&ACL);
    function_acl(function, &ACL);
    bool retval = aclCheck(&ACL, uid, acl_read);
    aclDestroy(&ACL);
    return retval;
}

/**
   @brief
   Check that the given userid can execute the function

   @details
   Checks the ACL to see that the given userid can execute the function

   @param function pointer to a function
   @param uid user id

   @return
    - true: the user or group can execute
    - false: the user and group are not allowed to execute
 */
bool function_canExecute(function_t* function, uint32_t uid) {
    if(!function) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(object_isRemote(function_owner(function)) || (uid == 0)) {
        return true;
    }

    llist_t ACL;
    llist_initialize(&ACL);
    function_acl(function, &ACL);
    bool retval = aclCheck(&ACL, uid, acl_execute);
    aclDestroy(&ACL);
    return retval;
}


#ifdef PCB_HELP_SUPPORT
/**
   @brief
   Add a description to the function

   @details
   Add a description to the function

   @param function Pointer to the function_t structure
   @param description the function description

   @return
    - true the description is set
    - false failed to set the description
 */
bool function_setDescription(function_t* function, const char* description) {
    if(!function || !description) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(function->definition->description) {
        free(function->definition->description);
        function->definition->description = NULL;
    }
    if(*description) {
        function->definition->description = strdup(description);
        if(!function->definition->description) {
            return false;
        }
        SAH_TRACEZ_INFO("pcb", "Set function description: %s", function->definition->description);
    }

    return true;
}

/**
   @brief
   Get the description of a function

   @details
   Get the description of a function

   @param function Pointer to the function_t structure

   @return
    - string containing the description of a function
    - NULL when no description is available
 */
const char* function_getDescription(function_t* function) {
    if(!function) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    return function->definition->description;
}
#endif

/**
   @brief
   Get the first argument definition of the function

   @details
   Get the first argument definition of the function, use this function to start iterating over the argment definitions of a function..

   @param function Pointer to the function_t structure

   @return
    - pointer to function_argument_t structure
    - NULL no arguments available
 */
function_argument_t* function_firstArgument(function_t* function) {
    if(!function) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    llist_iterator_t* it = llist_first(&function->definition->arguments);
    if(!it) {
        pcb_error = pcb_error_last_parameter;
        return NULL;
    }
    function_argument_t* argument = llist_item_data(it, function_argument_t, it);

    return argument;
}

/**
   @brief
   Get the next argument definition of the function

   @details
   Get the next argument definition of the function, use this function to iterate over the argment definitions of a function..

   @param ref pointer to the function_argument_t used as reference

   @return
    - pointer to function_argument_t structure
    - NULL no more arguments are available
 */
function_argument_t* function_nextArgument(function_argument_t* ref) {
    if(!ref) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    llist_iterator_t* it = llist_iterator_next(&ref->it);
    if(!it) {
        pcb_error = pcb_error_last_parameter;
        return NULL;
    }
    function_argument_t* argument = llist_item_data(it, function_argument_t, it);

    return argument;
}

/**
   @brief
   Get the previous argument definition of the function

   @details
   Get the previous argument definition of the function, use this function to iterate over the argment definitions of a function..

   @param ref pointer to the function_argument_t used as reference

   @return
    - pointer to function_argument_t structure
    - NULL no more arguments are available
 */
function_argument_t* function_prevArgument(function_argument_t* ref) {
    if(!ref) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    llist_iterator_t* it = llist_iterator_prev(&ref->it);
    if(!it) {
        pcb_error = pcb_error_last_parameter;
        return NULL;
    }
    function_argument_t* argument = llist_item_data(it, function_argument_t, it);

    return argument;
}

/**
   @brief
   Get the last argument definition of the function

   @details
   Get the last argument definition of the function, use this function to start iterating over the argment definitions of a function..

   @param function Pointer to the function_t structure

   @return
    - pointer to function_argument_t structure
    - NULL no arguments are available
 */
function_argument_t* function_lastArgument(function_t* function) {
    if(!function) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    llist_iterator_t* it = llist_last(&function->definition->arguments);
    if(!it) {
        pcb_error = pcb_error_last_parameter;
        return NULL;
    }
    function_argument_t* argument = llist_item_data(it, function_argument_t, it);

    return argument;
}

/**
   @brief
   Get an argument definition of a function by name

   @details
   Get an argument definition of a function using the arguments name

   @param function Pointer to the function_t structure
   @param argName name of the argument

   @return
    - pointer to function_argument_t structure
    - NULL no argument available with that name
 */
function_argument_t* function_getArgument(function_t* function, const char* argName) {
    if(!function) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    llist_iterator_t* it = NULL;
    function_argument_t* arg = NULL;
    llist_for_each(it, &function->definition->arguments) {
        arg = llist_item_data(it, function_argument_t, it);
        if(strcmp(string_buffer(&arg->name), argName) == 0) {
            return arg;
        }
    }

    return NULL;
}

/**
   @brief
   Create an argument definition

   @details
   Create an argument definition and add it to a function

   @param function Pointer to the function_t structure
   @param name name of the argument
   @param type type of the argument
   @param attributes attributes of the argument

   @return
    - pointer to function_argument_t structure
    - NULL failed to create the argument definition
 */
function_argument_t* argument_create(function_t* function, const char* name, const argument_type_t type, const uint32_t attributes) {
    function_argument_t* argument = NULL;
    if(!function || !name || !(*name) || (type == argument_type_unknown)) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    /* verify the name
       verify name characters not allowed are : . / $ only numeric
     */
    if(!isValidNameChar(name)) {
        goto error;
    }

    if(function_getArgument(function, name)) {
        goto error;
    }

    argument = (function_argument_t*) calloc(1, sizeof(function_argument_t));
    if(!argument) {
        pcb_error = pcb_error_no_memory;
        goto error;
    }

    if(!string_initialize(&argument->name, 0)) {
        goto error_free;
    }

    if(!string_initialize(&argument->typeName, 0)) {
        goto error_cleanup_name;
    }

    /* set the name */
    if(string_fromChar(&argument->name, name) == 0) {
        goto error_cleanup_type;
    }

    /* set the attributes */
    argument->attributes = attributes;
    /* set the argument type */
    argument->type = type;

    llist_append(&function->definition->arguments, &argument->it);

    return argument;

error_cleanup_type:
    string_cleanup(&argument->typeName);
error_cleanup_name:
    string_cleanup(&argument->name);
error_free:
    free(argument);
error:
    return NULL;
}

/**
   @brief
   Create an argument definition

   @details
   Create an argument definition and add it to a function

   @param function Pointer to the function_t structure
   @param name name of the argument
   @param type name of the argument type
   @param attributes attributes of the argument

   @return
    - pointer to function_argument_t structure
    - NULL failed to create the argument definition
 */
function_argument_t* argument_createCustomType(function_t* function, const char* name, const char* type, const uint32_t attributes) {
    function_argument_t* arg = argument_create(function, name, argument_type_custom, attributes);
    if(!arg) {
        return NULL;
    }
    /* set the type */
    if(string_fromChar(&arg->typeName, type) == 0) {
        argument_delete(arg);
        return NULL;
    }

    return arg;
}

/**
   @brief
   Delete an argument definition

   @details
   Delete an argument definition and remove it from the function

   @param argument Pointer to the function_argument_t structure
 */
void argument_delete(function_argument_t* argument) {
    if(!argument) {
        pcb_error = pcb_error_invalid_parameter;
        return;
    }

    string_cleanup(&argument->name);
    string_cleanup(&argument->typeName);

    llist_iterator_take(&argument->it);

    free(argument);
}

/**
   @brief
   Get the name of the argument

   @details
   Get the name of the argument. The returned string must not be deleted.

   @param arg Pointer to the function_argument_t structure

   @return
    - the name of the argument
 */
const char* argument_name(function_argument_t* arg) {
    if(!arg) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    return string_buffer(&arg->name);
}

/**
   @brief
   Get the type of the argument

   @details
   Get the type of the argument. If the argument is of the custom type use @ref argument_typeName to get the name of the type

   @param arg Pointer to the function_argument_t structure

   @return
    - the type of the argument
 */
argument_type_t argument_type(function_argument_t* arg) {
    if(!arg) {
        pcb_error = pcb_error_invalid_parameter;
        return 0;
    }

    return arg->type;
}

/**
   @brief
   Get the name of the type of the argument

   @details
   Get the name of the type of the argument.
   The returned string must not be freed.

   @param arg Pointer to the function_argument_t structure

   @return
    - the name of the type of the argument
 */
const char* argument_typeName(function_argument_t* arg) {
    if(!arg) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    switch(arg->type) {
    case argument_type_string:
        return "string";
        break;
    case argument_type_int8:
        return "int8";
        break;
    case argument_type_int16:
        return "int16";
        break;
    case argument_type_int32:
        return "int32";
        break;
    case argument_type_int64:
        return "int64";
        break;
    case argument_type_uint8:
        return "uint8";
        break;
    case argument_type_uint16:
        return "uint16";
        break;
    case argument_type_uint32:
        return "uint32";
        break;
    case argument_type_uint64:
        return "uint64";
        break;
    case argument_type_bool:
        return "bool";
        break;
    case argument_type_date_time:
        return "datetime";
        break;
    case argument_type_list:
        return "list";
        break;
    case argument_type_custom:
        return string_buffer(&arg->typeName);
        break;
    case argument_type_file_descriptor:
        return "fd";
        break;
    case argument_type_byte_array:
        return "byte_array";
        break;
    case argument_type_double:
        return "double";
        break;
    case argument_type_variant:
        return "variant";
        break;
    default:
        return "unknown";
        break;
    }
}

/**
   @brief
   Get the attributes of the argument

   @details
   Get the attributes of the argument.

   @param arg Pointer to the function_argument_t structure

   @return
    - the attributes of the argument as a bitmap
 */
argument_attribute_t argument_attributes(function_argument_t* arg) {
    if(!arg) {
        pcb_error = pcb_error_invalid_parameter;
        return 0;
    }

    return arg->attributes;
}

bool argument_isMandatory(function_argument_t* arg) {
    if(!arg) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    return (arg->attributes & argument_attr_mandatory);
}

/**
   @brief
   Initialize an argument value list.

   @details
   Initialize an argument value list. This list of argument values is used in a function execute request.
   This list will also be passed to the real implementation of the function.
   It is up to the function implementer to decide what to do with the list of arguments.

   @param args pointer to an argument_value_list_t (can be allocated on the stack or on the heap)

   @return
    - true the initialization is ok, the list can be used
    - false an error occured.
 */
bool argument_value_list_initialize(argument_value_list_t* args) {
    return llist_initialize(args);
}

/**
   @brief
   Create an argument value.

   @details
   Create an argument value. This argument value can be put in the argument value list that will be passed
   to the real implementation of the function.

   @param name Name of the argument. This name can match (but does not have to) an argument definition.

   @return
    - pointer to the argument_value_t structure
    - NULL failed to allocate or initialize the structure
 */
argument_value_t* argument_valueCreate(const char* name) {
    argument_value_t* arg_value = (argument_value_t*) calloc(1, sizeof(argument_value_t));
    if(!arg_value) {
        pcb_error = pcb_error_no_memory;
        goto error;
    }
    if(name) {
        arg_value->parameterName = (char*) calloc(1, strlen(name) + 1);
        if(!arg_value->parameterName) {
            pcb_error = pcb_error_no_memory;
            goto error_free_param_value;
        }
        strcpy(arg_value->parameterName, name);
    }
    variant_initialize(&arg_value->value, variant_type_unknown);

    return arg_value;

error_free_param_value:
    free(arg_value);
error:
    return NULL;
}

/**
   @brief
   Destroy an argument value.

   @details
   Destroy an argument value. If the argument value was in a list of argument values it is removed from the list.
   All allocate resources are freed.

   @param argval pointer to an argument_value_t structure, created with argument_valueCreate.
 */
void argument_valueDestroy(argument_value_t* argval) {
    if(!argval) {
        pcb_error = pcb_error_invalid_parameter;
        return;
    }

    free(argval->parameterName);
    variant_cleanup(&argval->value);
    llist_iterator_take(&argval->it);
    free(argval);
}

/**
   @brief
   Retrieve an argument value from the list by name.

   @details
   Retrieve an argument value from the list by name.

   @param values list of argument values
   @param argName name of the argument that needs to be retrieved

   @return
    - pointer to the argument_value_t structure
    - NULL no argument value found with the given name
 */
argument_value_t* argument_valueByName(argument_value_list_t* values, const char* argName) {
    if(!values) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    if(llist_isEmpty(values)) {
        return NULL;
    }

    argument_value_t* value = NULL;
    llist_iterator_t* it = NULL;
    llist_for_each(it, values) {
        value = llist_item_data(it, argument_value_t, it);
        if(strcmp(value->parameterName, argName) == 0) {
            break;
        }
        value = NULL;
    }

    return value;
}

/**
   @brief
   Retrieve an argument value from the list by index.

   @details
   Retrieve an argument value from the list by index.

   @param values list of argument values
   @param index index of the argument that needs to be retrieved

   @return
    - pointer to the argument_value_t structure
    - NULL no argument value found with the given index
 */
argument_value_t* argument_valueByIndex(argument_value_list_t* values, uint32_t index) {
    if(!values) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    if(llist_isEmpty(values)) {
        return NULL;
    }

    argument_value_t* value = NULL;
    llist_iterator_t* it = NULL;
    uint32_t count = 0;
    llist_for_each(it, values) {
        if(index == count) {
            value = llist_item_data(it, argument_value_t, it);
            break;
        }
        count++;
    }

    return value;
}

/**
   @brief
   Get the value of the value argument.

   @details
   Get the value of the value argument.

   @param arg The argument value

   @return
    - variant that represent the value of the argument
 */
variant_t* argument_value(argument_value_t* arg) {
    if(!arg) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    return &arg->value;
}

/**
   @brief
   Get the name of the value argument.

   @details
   Get the name of the value argument.
   The returned string must not be freed

   @param arg The argument value

   @return
    - The name of the argument
 */
const char* argument_valueName(argument_value_t* arg) {
    if(!arg) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    return arg->parameterName;
}

/**
   @brief
   Append an argument value to the list of argument values.

   @details
   Append an argument value to the list of argument values.

   @param args The argument value list
   @param arg The argument value

   @return
    - true; the argument value is added to the list
    - false: failed to add the argument value to the list
 */
bool argument_valueAppend(argument_value_list_t* args, argument_value_t* arg) {
    if(!args || !arg) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    return llist_append(args, &arg->it);
}

/**
   @brief
   Prepend an argument value to the list of argument values.

   @details
   Prepend an argument value to the list of argument values.

   @param args The argument value list
   @param arg The argument value

   @return
    - true; the argument value is added to the list
    - false: failed to add the argument value to the list
 */
bool argument_valuePrepend(argument_value_list_t* args, argument_value_t* arg) {
    if(!args || !arg) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    return llist_prepend(args, &arg->it);
}

/**
   @brief
   Create and append an argument value to the atgument value list

   @details
   Create and append an argument value to the argument value list

   @param args The argument value list
   @param name The argument value name
   @param value The argument value value

   @return
    - true; the argument value is created and added to the list
    - false: failed to create or add the argument value to the list
 */
bool argument_valueAdd(argument_value_list_t* args, const char* name, const variant_t* value) {
    if(!args || !value) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    argument_value_t* arg_value = argument_valueCreate(name);
    if(!arg_value) {
        goto error;
    }

    if(value) {
        variant_copy(&arg_value->value, value);
    }

    return argument_valueAppend(args, arg_value);

error:
    return false;
}

bool argument_valueSet(argument_value_list_t* args, const char* name, variant_t* value) {
    if(!args || !value) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    argument_value_t* arg_value = argument_valueCreate(name);
    if(!arg_value) {
        goto error;
    }

    if(value) {
        variant_move(&arg_value->value, value);
    }

    return argument_valueAppend(args, arg_value);

error:
    return false;
}

/**
   @brief
   Get the number of argument values in the argument value list

   @details
   Get the number of argument values in the argument value list

   @param args The argument value list

   @return
    - number of argument value items in the list
 */
uint32_t argument_valueCount(argument_value_list_t* args) {
    if(!args) {
        pcb_error = pcb_error_invalid_parameter;
        return 0;
    }

    return llist_size(args);
}

/**
   @brief
   Remove the argument value from an argument value list

   @details
   Remove the argument value from an argument value list

   @param arg The argument value
 */
void argument_valueTakeArgument(argument_value_t* arg) {
    if(!arg) {
        pcb_error = pcb_error_invalid_parameter;
        return;
    }

    llist_iterator_take(&arg->it);
}

/**
   @brief
   Remove the first argument value from the argument value list

   @details
   Remove the first argument value from the argument value list and return the argument value

   @param args The argument value list

   @return
    - the argument value
 */
argument_value_t* argument_valueTakeArgumentFirst(argument_value_list_t* args) {
    if(!args) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    llist_iterator_t* it = llist_takeFirst(args);
    if(it) {
        argument_value_t* arg = llist_item_data(it, argument_value_t, it);
        return arg;
    }

    return NULL;
}

/**
   @brief
   Get the first argument value out of the list

   @details
   Get the first argument out of the value list. This function can be used to start iterating over all the argument values in the list.

   @param args The argument value list

   @return
    - the first argument value
    - NULL no arguments in the list
 */
argument_value_t* argument_valueFirstArgument(argument_value_list_t* args) {
    if(!args) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    llist_iterator_t* it = llist_first(args);
    if(it) {
        argument_value_t* arg = llist_item_data(it, argument_value_t, it);
        return arg;
    }

    return NULL;
}

/**
   @brief
   Get the next argument value out of the list

   @details
   Get the next argument out of the value list. This function can be used to iterate over values in the list.

   @param arg The reference argument value

   @return
    - the next argument value
    - NULL no more values in the list
 */
argument_value_t* argument_valueNextArgument(argument_value_t* arg) {
    if(!arg) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    llist_iterator_t* it = llist_iterator_next(&arg->it);
    if(it) {
        argument_value_t* a = llist_item_data(it, argument_value_t, it);
        return a;
    }

    return NULL;
}

/**
   @brief
   Get the previous argument value out of the list

   @details
   Get the previous argument out of the value list. This function can be used to iterate over values in the list.

   @param arg The reference argument value

   @return
    - the previous argument value
    - NULL no more values in the list
 */
argument_value_t* argument_valuePrevArgument(argument_value_t* arg) {
    if(!arg) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    llist_iterator_t* it = llist_iterator_prev(&arg->it);
    if(it) {
        argument_value_t* a = llist_item_data(it, argument_value_t, it);
        return a;
    }

    return NULL;
}

/**
   @brief
   Get the last argument value out of the list

   @details
   Get the last argument out of the value list. This function can be used to start iterating over all the argument values in the list.

   @param args The argument value list

   @return
    - the last argument value
    - NULL no arguments in the list
 */
argument_value_t* argument_valueLastArgument(argument_value_list_t* args) {
    if(!args) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    llist_iterator_t* it = llist_last(args);
    if(it) {
        argument_value_t* arg = llist_item_data(it, argument_value_t, it);
        return arg;
    }

    return NULL;
}

/**
   @brief
   Clear the argument value list

   @details
   Clear the argument value list. All argument values will be removed from the list.

   @param values The argument value list
 */
void argument_valueClear(argument_value_list_t* values) {
    if(!values) {
        pcb_error = pcb_error_invalid_parameter;
        return;
    }

    argument_value_t* arg_value = argument_valueFirstArgument(values);
    while(arg_value) {
        free(arg_value->parameterName);
        variant_cleanup(&arg_value->value);
        llist_iterator_take(&arg_value->it);
        free(arg_value);
        arg_value = argument_valueFirstArgument(values);
    }
}

/**
   @brief
   Remove the first argument value from the list and convert it to int8

   @details
   Remove the first argument value from the list and convert it to int8

   @param args The argument value list

   @return
    - the value as an int8
 */
int8_t argument_takeInt8(argument_value_list_t* args) {
    if(!args) {
        pcb_error = pcb_error_invalid_parameter;
        return 0;
    }

    int8_t arg = 0;
    argument_value_t* argval = argument_valueTakeArgumentFirst(args);
    if(!argval) {
        SAH_TRACEZ_ERROR("pcb", "No argument specified");
        return 0;
    }
    arg = variant_int8(argument_value(argval));
    argument_valueDestroy(argval);

    return arg;
}

/**
   @brief
   Remove the first argument value from the list and convert it to int16

   @details
   Remove the first argument value from the list and convert it to int16

   @param args The argument value list

   @return
    - the value as an int16
 */
int16_t argument_takeInt16(argument_value_list_t* args) {
    if(!args) {
        pcb_error = pcb_error_invalid_parameter;
        return 0;
    }

    int16_t arg = 0;
    argument_value_t* argval = argument_valueTakeArgumentFirst(args);
    if(!argval) {
        SAH_TRACEZ_ERROR("pcb", "No argument specified");
        return 0;
    }
    arg = variant_int16(argument_value(argval));
    argument_valueDestroy(argval);

    return arg;
}

/**
   @brief
   Remove the first argument value from the list and convert it to int32

   @details
   Remove the first argument value from the list and convert it to int32

   @param args The argument value list

   @return
    - the value as an int32
 */
int32_t argument_takeInt32(argument_value_list_t* args) {
    if(!args) {
        pcb_error = pcb_error_invalid_parameter;
        return 0;
    }

    int32_t arg = 0;
    argument_value_t* argval = argument_valueTakeArgumentFirst(args);
    if(!argval) {
        SAH_TRACEZ_ERROR("pcb", "No argument specified");
        return 0;
    }
    arg = variant_int32(argument_value(argval));
    argument_valueDestroy(argval);

    return arg;
}

/**
   @brief
   Remove the first argument value from the list and convert it to int64

   @details
   Remove the first argument value from the list and convert it to int64

   @param args The argument value list

   @return
    - the value as an int64
 */
int64_t argument_takeInt64(argument_value_list_t* args) {
    if(!args) {
        pcb_error = pcb_error_invalid_parameter;
        return 0;
    }

    int64_t arg = 0;
    argument_value_t* argval = argument_valueTakeArgumentFirst(args);
    if(!argval) {
        SAH_TRACEZ_ERROR("pcb", "No argument specified");
        return 0;
    }
    arg = variant_int64(argument_value(argval));
    argument_valueDestroy(argval);

    return arg;
}

/**
   @brief
   Remove the first argument value from the list and convert it to a string

   @details
   Remove the first argument value from the list and convert it to a string

   @param args The argument value list

   @return
    - the value as a string
 */
char* argument_takeChar(argument_value_list_t* args) {
    if(!args) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    char* arg = NULL;
    argument_value_t* argval = argument_valueTakeArgumentFirst(args);
    if(!argval) {
        SAH_TRACEZ_ERROR("pcb", "No argument specified");
        return NULL;
    }
    arg = variant_char(argument_value(argval));
    argument_valueDestroy(argval);

    return arg;
}

/**
   @brief
   Remove the first argument value from the list and convert it to an uint8

   @details
   Remove the first argument value from the list and convert it to an uint8

   @param args The argument value list

   @return
    - the value as an uint8
 */
uint8_t argument_takeUInt8(argument_value_list_t* args) {
    if(!args) {
        pcb_error = pcb_error_invalid_parameter;
        return 0;
    }

    uint8_t arg = 0;
    argument_value_t* argval = argument_valueTakeArgumentFirst(args);
    if(!argval) {
        SAH_TRACEZ_ERROR("pcb", "No argument specified");
        return 0;
    }
    arg = variant_uint8(argument_value(argval));
    argument_valueDestroy(argval);

    return arg;
}

/**
   @brief
   Remove the first argument value from the list and convert it to an uint16

   @details
   Remove the first argument value from the list and convert it to an uint16

   @param args The argument value list

   @return
    - the value as an uint16
 */
uint16_t argument_takeUInt16(argument_value_list_t* args) {
    if(!args) {
        pcb_error = pcb_error_invalid_parameter;
        return 0;
    }

    uint32_t arg = 0;
    argument_value_t* argval = argument_valueTakeArgumentFirst(args);
    if(!argval) {
        SAH_TRACEZ_ERROR("pcb", "No argument specified");
        return 0;
    }
    arg = variant_uint32(argument_value(argval));
    argument_valueDestroy(argval);

    return arg;
}

/**
   @brief
   Remove the first argument value from the list and convert it to an uint32

   @details
   Remove the first argument value from the list and convert it to an uint32

   @param args The argument value list

   @return
    - the value as an uint32
 */
uint32_t argument_takeUInt32(argument_value_list_t* args) {
    if(!args) {
        pcb_error = pcb_error_invalid_parameter;
        return 0;
    }

    uint32_t arg = 0;
    argument_value_t* argval = argument_valueTakeArgumentFirst(args);
    if(!argval) {
        SAH_TRACEZ_ERROR("pcb", "No argument specified");
        return 0;
    }
    arg = variant_uint32(argument_value(argval));
    argument_valueDestroy(argval);

    return arg;
}

/**
   @brief
   Remove the first argument value from the list and convert it to an uint64

   @details
   Remove the first argument value from the list and convert it to an uint64

   @param args The argument value list

   @return
    - the value as an uint64
 */
uint64_t argument_takeUInt64(argument_value_list_t* args) {
    if(!args) {
        pcb_error = pcb_error_invalid_parameter;
        return 0;
    }

    uint64_t arg = 0;
    argument_value_t* argval = argument_valueTakeArgumentFirst(args);
    if(!argval) {
        SAH_TRACEZ_ERROR("pcb", "No argument specified");
        return 0;
    }
    arg = variant_uint64(argument_value(argval));
    argument_valueDestroy(argval);

    return arg;
}

/**
   @brief
   Remove the first argument value from the list and convert it to a bool

   @details
   Remove the first argument value from the list and convert it to a bool

   @param args The argument value list

   @return
    - the value as a bool
 */
bool argument_takeBool(argument_value_list_t* args) {
    if(!args) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    bool arg = 0;
    argument_value_t* argval = argument_valueTakeArgumentFirst(args);
    if(!argval) {
        SAH_TRACEZ_ERROR("pcb", "No argument specified");
        return false;
    }
    arg = variant_bool(argument_value(argval));
    argument_valueDestroy(argval);

    return arg;
}

/**
   @brief
   Remove the first argument value from the list and convert it to a double

   @details
   Remove the first argument value from the list and convert it to a double

   @param args The argument value list

   @return
    - the value as a double
 */
double argument_takeDouble(argument_value_list_t* args) {
    if(!args) {
        pcb_error = pcb_error_invalid_parameter;
        return 0;
    }

    double arg = 0;
    argument_value_t* argval = argument_valueTakeArgumentFirst(args);
    if(!argval) {
        SAH_TRACEZ_ERROR("pcb", "No argument specified");
        return 0;
    }
    arg = variant_double(argument_value(argval));
    argument_valueDestroy(argval);

    return arg;
}

/**
   @brief
   Remove the first argument value from the list and convert it to a struct tm

   @details
   Remove the first argument value from the list and convert it to a struct tm

   @param args The argument value list

   @return
    - the value as a struct tm
 */
struct tm* argument_takeDateTime(argument_value_list_t* args) {
    if(!args) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    struct tm* arg = 0;
    argument_value_t* argval = argument_valueTakeArgumentFirst(args);
    if(!argval) {
        SAH_TRACEZ_ERROR("pcb", "No argument specified");
        return NULL;
    }
    arg = variant_dateTime(argument_value(argval));
    argument_valueDestroy(argval);

    return arg;
}

/**
   @brief
   Remove the first argument value from the list and convert it to a pcb_datetime_t

   @details
   Remove the first argument value from the list and convert it to a pcb_datetime_t

   @param args The argument value list

   @return
    - the value as a pcb_datetime_t
    - NULL on failure
 */
pcb_datetime_t* argument_takeDateTimeExtended(argument_value_list_t* args) {
    if(!args) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    pcb_datetime_t* arg = 0;
    argument_value_t* argval = argument_valueTakeArgumentFirst(args);
    if(!argval) {
        SAH_TRACEZ_ERROR("pcb", "No argument specified");
        return NULL;
    }
    arg = variant_dateTimeExtended(argument_value(argval));
    argument_valueDestroy(argval);

    return arg;
}

/**
   @brief
   Remove the first argument value from the list and convert it to a list of variants

   @details
   Remove the first argument value from the list and convert it to a list of variants

   @param args The argument value list

   @return
    - the value as a list of variants
 */
variant_list_t* argument_takeArray(argument_value_list_t* args) {
    if(!args) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    variant_list_t* arg = 0;
    argument_value_t* argval = argument_valueTakeArgumentFirst(args);
    if(!argval) {
        SAH_TRACEZ_ERROR("pcb", "No argument specified");
        return NULL;
    }
    arg = variant_list(argument_value(argval));
    argument_valueDestroy(argval);

    return arg;
}

/**
   @brief
   Remove the first argument value from the list and convert it to a map of variants

   @details
   Remove the first argument value from the list and convert it to a map of variants

   @param args The argument value list

   @return
    - the value as a map of variants
 */
variant_map_t* argument_takeMap(argument_value_list_t* args) {
    if(!args) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    variant_map_t* arg = NULL;
    argument_value_t* argval = argument_valueTakeArgumentFirst(args);
    if(!argval) {
        SAH_TRACEZ_ERROR("pcb", "No argument specified");
        return NULL;
    }
    arg = variant_map(argument_value(argval));
    argument_valueDestroy(argval);

    return arg;
}

void* argument_takeByteArray(argument_value_list_t* args, uint32_t* size) {
    if(!args) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    void* arg = NULL;
    argument_value_t* argval = argument_valueTakeArgumentFirst(args);
    if(!argval) {
        SAH_TRACEZ_ERROR("pcb", "No argument specified");
        return NULL;
    }
    arg = variant_byteArray(argument_value(argval), size);
    argument_valueDestroy(argval);

    return arg;
}

bool argument_get(argument_value_t** arg, argument_value_list_t* args, uint32_t reqattr, const char* argname) {
    if(!args) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(reqattr & request_function_args_by_name) {
        *arg = argument_valueByName(args, argname);
    } else {
        *arg = argument_valueFirstArgument(args);
    }

    if(*arg) {
        argument_valueTakeArgument(*arg);
    } else {
        pcb_error = pcb_error_not_found;
        return false;
    }

    return true;
}

bool argument_getChar(char** value, argument_value_list_t* args, uint32_t reqattr, const char* argname, const char* def) {
    argument_value_t* arg = NULL;
    if(!argument_get(&arg, args, reqattr, argname)) {
        if(def) {
            *value = strdup(def);
        }
        return false;
    }

    *value = variant_char(argument_value(arg));
    argument_valueDestroy(arg);

    return true;
}

bool argument_getInt8(int8_t* value, argument_value_list_t* args, uint32_t reqattr, const char* argname, int8_t def) {
    argument_value_t* arg = NULL;
    if(!argument_get(&arg, args, reqattr, argname)) {
        *value = def;
        return false;
    }

    *value = variant_int8(argument_value(arg));
    argument_valueDestroy(arg);

    return true;
}

bool argument_getInt16(int16_t* value, argument_value_list_t* args, uint32_t reqattr, const char* argname, int16_t def) {
    argument_value_t* arg = NULL;
    if(!argument_get(&arg, args, reqattr, argname)) {
        *value = def;
        return false;
    }

    *value = variant_int16(argument_value(arg));
    argument_valueDestroy(arg);

    return true;
}

bool argument_getInt32(int32_t* value, argument_value_list_t* args, uint32_t reqattr, const char* argname, int32_t def) {
    argument_value_t* arg = NULL;
    if(!argument_get(&arg, args, reqattr, argname)) {
        *value = def;
        return false;
    }

    *value = variant_int32(argument_value(arg));
    argument_valueDestroy(arg);

    return true;
}

bool argument_getInt64(int64_t* value, argument_value_list_t* args, uint32_t reqattr, const char* argname, int64_t def) {
    argument_value_t* arg = NULL;
    if(!argument_get(&arg, args, reqattr, argname)) {
        *value = def;
        return false;
    }

    *value = variant_int64(argument_value(arg));
    argument_valueDestroy(arg);

    return true;
}

bool argument_getUInt8(uint8_t* value, argument_value_list_t* args, uint32_t reqattr, const char* argname, uint8_t def) {
    argument_value_t* arg = NULL;
    if(!argument_get(&arg, args, reqattr, argname)) {
        *value = def;
        return false;
    }

    *value = variant_uint8(argument_value(arg));
    argument_valueDestroy(arg);

    return true;
}

bool argument_getUInt16(uint16_t* value, argument_value_list_t* args, uint32_t reqattr, const char* argname, uint16_t def) {
    argument_value_t* arg = NULL;
    if(!argument_get(&arg, args, reqattr, argname)) {
        *value = def;
        return false;
    }

    *value = variant_uint16(argument_value(arg));
    argument_valueDestroy(arg);

    return true;
}

bool argument_getUInt32(uint32_t* value, argument_value_list_t* args, uint32_t reqattr, const char* argname, uint32_t def) {
    argument_value_t* arg = NULL;
    if(!argument_get(&arg, args, reqattr, argname)) {
        *value = def;
        return false;
    }

    *value = variant_uint32(argument_value(arg));
    argument_valueDestroy(arg);

    return true;
}

bool argument_getUInt64(uint64_t* value, argument_value_list_t* args, uint32_t reqattr, const char* argname, uint64_t def) {
    argument_value_t* arg = NULL;
    if(!argument_get(&arg, args, reqattr, argname)) {
        *value = def;
        return false;
    }

    *value = variant_int64(argument_value(arg));
    argument_valueDestroy(arg);

    return true;
}

bool argument_getBool(bool* value, argument_value_list_t* args, uint32_t reqattr, const char* argname, bool def) {
    argument_value_t* arg = NULL;
    if(!argument_get(&arg, args, reqattr, argname)) {
        *value = def;
        return false;
    }

    *value = variant_bool(argument_value(arg));
    argument_valueDestroy(arg);

    return true;
}

bool argument_getDouble(double* value, argument_value_list_t* args, uint32_t reqattr, const char* argname, double def) {
    argument_value_t* arg = NULL;
    if(!argument_get(&arg, args, reqattr, argname)) {
        *value = def;
        return false;
    }

    *value = variant_double(argument_value(arg));
    argument_valueDestroy(arg);

    return true;
}

bool argument_getDateTime(struct tm** value, argument_value_list_t* args, uint32_t reqattr, const char* argname, struct tm* def) {
    argument_value_t* arg = NULL;
    if(!argument_get(&arg, args, reqattr, argname)) {
        *value = def;
        return false;
    }

    *value = variant_dateTime(argument_value(arg));
    argument_valueDestroy(arg);

    return true;
}

bool argument_getDateTimeExtended(pcb_datetime_t** value, argument_value_list_t* args, uint32_t reqattr, const char* argname, pcb_datetime_t* def) {
    argument_value_t* arg = NULL;
    if(!argument_get(&arg, args, reqattr, argname)) {
        *value = def;
        return false;
    }

    *value = variant_dateTimeExtended(argument_value(arg));
    argument_valueDestroy(arg);

    return true;
}

bool argument_getList(variant_list_t** value, argument_value_list_t* args, uint32_t reqattr, const char* argname, variant_list_t* def) {
    argument_value_t* arg = NULL;
    if(!argument_get(&arg, args, reqattr, argname)) {
        *value = def;
        return false;
    }

    *value = variant_list(argument_value(arg));
    argument_valueDestroy(arg);

    return true;
}

bool argument_getMap(variant_map_t** value, argument_value_list_t* args, uint32_t reqattr, const char* argname, variant_map_t* def) {
    argument_value_t* arg = NULL;
    if(!argument_get(&arg, args, reqattr, argname)) {
        *value = def;
        return false;
    }

    *value = variant_map(argument_value(arg));
    argument_valueDestroy(arg);

    return true;
}

bool argument_getFd(int* value, argument_value_list_t* args, uint32_t reqattr, const char* argname, int def) {
    argument_value_t* arg = NULL;
    if(!argument_get(&arg, args, reqattr, argname)) {
        *value = def;
        return false;
    }

    *value = variant_fd(argument_value(arg));
    argument_valueDestroy(arg);

    return true;
}

bool argument_getByteArray(void** value, uint32_t* size, argument_value_list_t* args, uint32_t reqattr, const char* argname, char* def) {
    argument_value_t* arg = NULL;
    if(!argument_get(&arg, args, reqattr, argname)) {
        *value = def;
        return false;
    }

    *value = variant_byteArray(argument_value(arg), size);
    argument_valueDestroy(arg);

    return true;
}

bool argument_getStringList(string_list_t** value, argument_value_list_t* args, uint32_t reqattr, const char* argname, const char* separator) {
    argument_value_t* arg = NULL;
    if(!argument_get(&arg, args, reqattr, argname)) {
        return false;
    }

    *value = variant_stringList(argument_value(arg), separator);
    argument_valueDestroy(arg);
    return true;
}

/**
   @brief
   Create a function call context

   @details
   Create a function call context. Normally you do not need to call this function. It is called from within the
   function execute request handler

   @param peer peer info structure on which the requested is recieved
   @param req the recieved request
   @param function the function definition
   @param object the object on which the function call needs to be invoked.

   @return
    - pointer to a function call context
 */
function_call_t* fcall_create(peer_info_t* peer, request_t* req, function_t* function, object_t* object) {
    if(!peer || !req) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    function_call_t* fcall = (function_call_t*) calloc(1, sizeof(function_call_t));
    if(!fcall) {
        pcb_error = pcb_error_no_memory;
        return NULL;
    }

    fcall->source = peer;
    fcall->req = req;
    fcall->function = function;
    fcall->object = object;
    fcall->state = function_exec_init;

    request_setData(req, fcall);
    request_setDestroyHandler(req, fcall_request_destroyed);

    return fcall;
}

/**
   @brief
   destroy a function call context

   @details
   Destroy a function call context. Normally you do not need to call this function. It is called from within the
   function execute request handler.
   When implementing an asynchrounous function, call this when function execution is done.

   @param fcall The function call context
 */
void fcall_destroy(function_call_t* fcall) {
    if(!fcall) {
        pcb_error = pcb_error_invalid_parameter;
        return;
    }

    request_setData(fcall->req, NULL);
    request_setDestroyHandler(fcall->req, NULL);

    if((request_state(fcall->req) != request_state_done) && fcall->cancelExecution) {
        fcall->cancelExecution(fcall, fcall->userdata);
    }

    request_setDone(fcall->req);
    free(fcall);
}

/**
   @brief
   Set a cancel handler

   @details
   When implementing a asynchronous function, you need to set the cancel handler before returing to the event loop.
   This function will be called whenever the function execution can stop (socket closed, request canceled)

   @param fcall The function call context
   @param handler The cancel handler

   @return
    - true the handler is set.
    - false failed to set the handler
 */
bool fcall_setCancelHandler(function_call_t* fcall, function_cancel_handler_t handler) {
    if(!fcall) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    fcall->cancelExecution = handler;
    return true;
}

/**
   @brief
   Attach some data to the function call context

   @details
   When implementing a asynchronous function, you can add some data to the function call context.

   @param fcall The function call context
   @param userdata pointer to the data

   @return
    - true the data is set.
    - false failed to set the data
 */
bool fcall_setUserData(function_call_t* fcall, void* userdata) {
    if(!fcall) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    fcall->userdata = userdata;
    return true;
}

/**
   @brief
   Get the attached user data

   @details
   Retrieve the attached user data

   @param fcall The function call context

   @return
    - pointer to the user data.
 */
void* fcall_userData(function_call_t* fcall) {
    if(!fcall) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    return fcall->userdata;
}

/**
   @brief
   Get the peer from the function call context

   @details
   Get the peer from the function call context

   @param fcall The function call context

   @return
    - pointer to the peer info structure.
 */
peer_info_t* fcall_peer(function_call_t* fcall) {
    if(!fcall) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    return fcall->source;
}

/**
   @brief
   Get the recieved request from the function call context

   @details
   Get the recieved request from the function call context

   @param fcall The function call context

   @return
    - pointer to the recieved request.
 */
request_t* fcall_request(function_call_t* fcall) {
    if(!fcall) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    return fcall->req;
}

/**
   @brief
   Get the user id of the caller of this function

   @details
   Get the user id of the request that is executing this function.

   @param fcall The function call context

   @return
    - the user id of the caller of this function.
 */
uint32_t fcall_userID(function_call_t* fcall) {
    if(!fcall || !fcall->req) {
        pcb_error = pcb_error_invalid_parameter;
        return 0;
    }

    return request_userID(fcall->req);
}

/**
   @brief
   Get the function definition from the function call context

   @details
   Get the function definition from the function call context

   @param fcall The function call context

   @return
    - pointer to the function definition.
 */
function_t* fcall_function(function_call_t* fcall) {
    if(!fcall) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    return fcall->function;
}

/**
   @brief
   Get the object from the function call context

   @details
   Get the object on which the function call is invoked from the function call context

   @param fcall The function call context

   @return
    - pointer to the object.
 */
object_t* fcall_object(function_call_t* fcall) {
    if(!fcall) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    return fcall->object;
}

function_exec_state_t fcall_state(function_call_t* fcall) {
    if(!fcall) {
        pcb_error = pcb_error_invalid_parameter;
        return function_exec_done;
    }

    return fcall->state;
}

/**
   @brief
   Execute the function call

   @details
   Execute the function context. The real function implementation will be called.
   This function must return one of the following:
    - function_exec_done: the function execution is done
    - function_exec_executing: the function is still executing, a reply will be generated later
    - function_exec_error: The function failed to execute.
   The argument value list will be passed to the function when the function returns with function_exec_done, the same argument list
   will be used to create the list of out arguments. All argument values in the list will be used as out arguments.
   This can be different then what was specified in the definition.

   The return value must be put in the retval. If the function returns with anything else then function_exec_done, the args and retval are ignored

   @param fcall The function call context
   @param args the argument list, also used for the out arguments
   @param retval variant that must contain the return value.

   @return
    - functin execution state.
 */
function_exec_state_t fcall_execute(function_call_t* fcall, argument_value_list_t* args, variant_t* retval) {
    if(!fcall) {
        pcb_error = pcb_error_invalid_parameter;
        return function_exec_error;
    }

    function_exec_state_t state = function_exec_executing;
    if(fcall->function->handler) {
        fcall->state = function_exec_executing;
        state = fcall->function->handler(fcall, args, retval);
    } else {
        pcb_error = pcb_error_function_not_implemented;
        return function_exec_error;
    }

    return state;
}

/**
   @}
 */
