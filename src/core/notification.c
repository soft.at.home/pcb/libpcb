/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <string.h>
#include <stdlib.h>
#include <unistd.h>

#include <debug/sahtrace.h>

#include <pcb/common/error.h>

#include "notification_priv.h"
#include "object_priv.h"
#include "parameter_priv.h"
#include "notification_priv.h"

/**
   @file
   @brief
   Implementation of notification functions
 */


static notification_t* notification_start(const char* appName, notification_type_t type) {
    if(!appName) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    notification_t* notification = notification_create(type);
    if(!notification) {
        return NULL;
    }

    notification_setName(notification, "application_started");

    notification_parameter_t* notif_param = notification_parameter_create("Application", appName);
    notification_addParameter(notification, notif_param);

    string_t temp;
    string_initialize(&temp, 32);

    string_fromInt32(&temp, getpid());
    notif_param = notification_parameter_create("PID", string_buffer(&temp));
    notification_addParameter(notification, notif_param);

    string_fromUInt32(&temp, sahTraceLevel());
    notif_param = notification_parameter_create("TraceLevel", string_buffer(&temp));
    notification_addParameter(notification, notif_param);


    sah_trace_zone_it* zone = sahTraceFirstZone();
    while(zone) {
        string_fromInt32(&temp, sahTraceZoneLevel(zone));
        notif_param = notification_parameter_create(sahTraceZoneName(zone), string_buffer(&temp));
        notification_addParameter(notification, notif_param);
        zone = sahTraceNextZone(zone);
    }

    string_cleanup(&temp);

    return notification;
}

/**
   @ingroup pcb_core_notification
   @brief
   Create a custom notification

   @details
   This function will create a custom notification object. The custom notification type must be higher then 100. All
   types below 100 are reserved for internal use.\n
   After creating a notification, it is possible to add one or more notification parameters.

   @param type identifier of the custom notification.

   @return
    - A pointer to a notification_t structure
    - NULL: failed to create the notification.

 */
notification_t* notification_create(uint32_t type) {
    notification_t* notification = (notification_t*) calloc(1, sizeof(notification_t));
    if(!notification) {
        pcb_error = pcb_error_no_memory;
        return NULL;
    }

    if(!string_initialize(&notification->objectPath, 64)) {
        goto error_free;
    }

    if(!llist_initialize(&notification->parameters)) {
        goto error_free;
    }

    if(!string_initialize(&notification->name, 0)) {
        goto error_free;
    }

    notification->type = type;

    return notification;

error_free:
    free(notification);

    return NULL;
}

/**
   @ingroup pcb_core_notification
   @brief
   Create a value changed notification

   @details
   This function will create a value changed notification.\n
   This kind of notification will contain the object path, the parameter name,
   the old and the new value.\n
   After creating a notification, it is possible to add one or more notification parameters.\n
   Value changed notifications will be created automatically when a notification request has been added
   to the object and the object is committed after a change of one of his parameters.\n

   @param object the object of wich a parameter has been changed
   @param parameter the parameter whose value has been changed

   @return
    - A pointer to a notification_t structure
    - NULL: failed to create the notification.
 */
notification_t* notification_createValueChanged(object_t* object, parameter_t* parameter) {
    if(!object || !parameter) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    notification_t* notification = notification_create(notify_value_changed);
    if(!notification) {
        return NULL;
    }

    notification_setName(notification, "changed");

    string_t data;
    string_initialize(&data, 64);
    object_path(object, &data, 0);
    notification_setObjectPath(notification, string_buffer(&data));

    notification_parameter_t* notif_param = notification_parameter_create("parameter", parameter_name(parameter));
    notification_addParameter(notification, notif_param);

    notif_param = notification_parameter_createVariant("oldvalue", parameter_getValue(parameter));
    notification_addParameter(notification, notif_param);

    notif_param = notification_parameter_createVariant("newvalue", parameter_getModifiedValue(parameter));
    notification_addParameter(notification, notif_param);

    string_cleanup(&data);

    return notification;
}

/**
   @ingroup pcb_core_notification
   @brief
   Create an object added notification

   @details
   This function will create an object added notification.\n
   This kind of notification will contain the object path of the newly created object.\n
   After creating a notification, it is possible to add one or more notification parameters.\n
   Object added notifications will be created automatically for newly created instances when a notification
   request has been added to the template object and the object is committed after adding an instance.\n

   @param object the object that has been added

   @return
    - A pointer to a notification_t structure
    - NULL: failed to create the notification.
 */
notification_t* notification_createObjectAdded(object_t* object) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    notification_t* notification = notification_create(notify_object_added);
    if(!notification) {
        return NULL;
    }

    notification_setName(notification, "add");

    string_t data;
    string_initialize(&data, 64);
    object_path(object, &data, 0);
    notification_setObjectPath(notification, string_buffer(&data));
    string_cleanup(&data);

    return notification;
}

/**
   @ingroup pcb_core_notification
   @brief
   Create an object deleted notification

   @details
   This function will create an object deleted notification.\n
   This kind of notification will contain the object path of the deleted object\n
   After creating a notification, it is possible to add one or more notification parameters.\n
   Object deleted notifications will be created automatically for deleted instances when a notification
   request has been added to the template object and the object is committed after deleting an instance.\n

   @param object the object that is being deleted

   @return
    - A pointer to a notification_t structure
    - NULL: failed to create the notification.
 */
notification_t* notification_createObjectDeleted(object_t* object) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    notification_t* notification = notification_create(notify_object_deleted);
    if(!notification) {
        return NULL;
    }

    notification_setName(notification, "delete");

    string_t data;
    string_initialize(&data, 64);
    object_path(object, &data, 0);
    notification_setObjectPath(notification, string_buffer(&data));
    string_cleanup(&data);

    return notification;
}

/**
   @ingroup pcb_core_notification
   @brief
   Create an application started notification

   @details
   This function will create an application started notification.\n
   This kind of notification will contain the application name, the node (IP address, dns resolvable network name or IPC socket name) and
   the TCP port number or service name.\n
   After creating a notification, it is possible to add one or more notification parameters.\n
   Application started notifications are typically send by servers after initializing there data model and connecting to
   the pcb system bus.\n

   @param appName the name of the application (this is mainly used for debugging purposes)

   @return
    - A pointer to a notification_t structure
    - NULL: failed to create the notification.
 */
notification_t* notification_createApplicationStarted(const char* appName) {
    return notification_start(appName, notify_application_started);
}

/**
   @ingroup pcb_core_notification
   @brief
   Create an application stopping notification

   @details
   This function will create an application stopped notification.\n
   This kind of notification will contain the application pid.\n
   After creating a notification, it is possible to add one or more notification parameters.\n
   Application stopped notifications are typically send by servers when they are going to stop\n

   @param appName the name of the application (this is mainly used for debugging purposes)

   @return
    - A pointer to a notification_t structure
    - NULL: failed to create the notification.
 */
notification_t* notification_createApplicationStopping(const char* appName) {
    notification_t* notification = notification_create(notify_application_stopping);
    if(!notification) {
        return NULL;
    }

    notification_setName(notification, "application_stopping");

    string_t temp;
    string_initialize(&temp, 32);

    notification_parameter_t* notif_param = notification_parameter_create("Application", appName);
    notification_addParameter(notification, notif_param);
    string_fromInt32(&temp, getpid());
    notif_param = notification_parameter_create("PID", string_buffer(&temp));
    notification_addParameter(notification, notif_param);

    string_cleanup(&temp);

    return notification;
}

/**
   @ingroup pcb_core_notification
   @brief
   Create an changed settings notification

   @details
   This function will create an changed settings notification.\n
   This kind of notification will contain the the setting that has been changed.\n
   After creating a notification, it is possible to add one or more notification parameters.\n
   Changed setting notifications are typically send by the system bus when a setting for an application
   has been changed by an user\n

   @param settingName the name of the setting
   @param settingValue the new value of the setting

   @return
    - A pointer to a notification_t structure
    - NULL: failed to create the notification.
 */
notification_t* notification_createApplicationChangeSetting(const char* settingName, const char* settingValue) {
    notification_t* notification = notification_create(notify_application_change_setting);
    if(!notification) {
        return NULL;
    }

    notification_setName(notification, "application_change_setting");

    notification_parameter_t* notif_param = notification_parameter_create(settingName, settingValue);
    notification_addParameter(notification, notif_param);

    return notification;
}

notification_t* notification_createApplicationRootAdded(const char* object_name) {
    notification_t* notification = notification_create(notify_application_root_added);
    if(!notification) {
        return NULL;
    }

    notification_setName(notification, "application_root_added");

    notification_parameter_t* notif_param = notification_parameter_create("Object", object_name);
    notification_addParameter(notification, notif_param);

    return notification;
}

notification_t* notification_createApplicationRootDeleted(const char* object_name) {
    notification_t* notification = notification_create(notify_application_root_deleted);
    if(!notification) {
        return NULL;
    }

    notification_setName(notification, "application_root_deleted");

    notification_parameter_t* notif_param = notification_parameter_create("Object", object_name);
    notification_addParameter(notification, notif_param);

    return notification;
}

notification_t* notification_createBusInterconnect(const char* appName, bool sync) {
    notification_t* notification = notification_start(appName, notify_bus_interconnect);

    variant_t sync_var;
    variant_initialize(&sync_var, variant_type_bool);
    variant_setBool(&sync_var, sync);

    notification_parameter_t* notif_param = notification_parameter_createVariant("Synchronize", &sync_var);
    notification_addParameter(notification, notif_param);

    variant_cleanup(&sync_var);

    return notification;
}

/**
   @ingroup pcb_core_notification
   @brief
   Destroy a notification

   @details
   When a notification is not needed anymore, use this function to destroy the notification.\n
   All memory aqllocated will be freed.

   @param notification a pointer to the notification, which was created with one of the notification create functions
 */
void notification_destroy(notification_t* notification) {
    if(!notification) {
        pcb_error = pcb_error_invalid_parameter;
        return;
    }

    string_cleanup(&notification->objectPath);

    notification_parameter_t* parameter = notification_firstParameter(notification);
    while(parameter) {
        notification_parameter_destroy(parameter);
        parameter = notification_firstParameter(notification);
    }

    string_cleanup(&notification->name);
    free(notification);
}

/**
   @ingroup pcb_core_notification
   @brief
   Returns the notification type

   @details
   This function will return the notification type.

   @param notification a pointer to the notification, which was created with one of the notification create functions

   @return
    - The notification type
 */
uint32_t notification_type(const notification_t* notification) {
    if(!notification) {
        pcb_error = pcb_error_invalid_parameter;
        return 0;
    }

    return notification->type;
}

/**
   @ingroup pcb_core_notification
   @brief
   Add a name to the notification

   @details
   Add a name to the notification. This is of importance for custom notifications, but not mandatory

   @param notification a pointer to the notification, which was created with one of the notification create functions
   @param name the name of the notification

   @return
    - true the notification has been set
    - false failed to set the name
 */
bool notification_setName(notification_t* notification, const char* name) {
    if(!notification) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    string_fromChar(&notification->name, name);
    return true;
}

/**
   @ingroup pcb_core_notification
   @brief
   Get the name to a notification

   @details
   Get the name to a notification

   @param notification a pointer to the notification, which was created with one of the notification create functions

   @return
    - the name of the notification
 */
const char* notification_name(notification_t* notification) {
    if(!notification) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    return string_buffer(&notification->name);
}

/**
   @ingroup pcb_core_notification
   @brief
   Set the object path

   @details
   Sets the notification object path. Not all notification must set the object path, but when set, most
   dispatching data models will only dispatch the notification to clients who has an open notification request
   for this path. (Can be compared with multicasting). Notifications without an object path will mostly be dispatched
   to all clients (can be compared with broadcasting).\n

   Be aware that most of the standard notifications already contain an object path.

   @param notification a pointer to the notification, which was created with one of the notification create functions
   @param objectpath the path the needs to be set in the notification.

   @return
    - true the path has been set
    - false the path has not been set
        - the notification pointer is a NULL pointer
 */
bool notification_setObjectPath(notification_t* notification, const char* objectpath) {
    if(!notification) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    string_fromChar(&notification->objectPath, objectpath);

    return true;
}

/**
   @ingroup pcb_core_notification
   @brief
   Get the object path

   @details
   Get the object path from a notification

   @param notification a pointer to the notification, which was created with one of the notification create functions

   @return
    - The object path
    - NULL:
        - no object path has been set.
        - the notification pointer is a NULL pointer.
 */
const char* notification_objectPath(notification_t* notification) {
    if(!notification) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    return string_buffer(&notification->objectPath);
}

/**
   @ingroup pcb_core_notification
   @brief
   Get the object name

   @details
   Get the object name from the notification object path

   @param notification a pointer to the notification, which was created with one of the notification create functions

   @return
    - A pointer on the object name
    - NULL:
        - no object name has been found.
        - the notification pointer is a NULL pointer.
        - the notification has an invalid format ('.' is the last character)
 */
const char* notification_objectName(notification_t* notification) {

    const char* objectPath = notification_objectPath(notification);
    if(!objectPath) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    char* name = strrchr(objectPath, '.');
    if(!name) {
        name = strrchr(objectPath, '/');
    }

    if(!name) {
        return objectPath;
    } else {
        name++;
        if(*name == '\0') {
            return NULL;
        }
    }

    return name;
}

/**
   @ingroup pcb_core_notification
   @brief
   Add a parameter to a notification

   @details
   Add a parameter to a notification.\n
   A notification parameter has a name and a value, and must be created using @ref notification_parameter_create

   @param notification a pointer to the notification, which was created with one of the notification create functions
   @param param a pointer to a notification parameter

   @return
    - true: the parameter has been added
    - false: failed to add the parameter
 */
bool notification_addParameter(notification_t* notification, notification_parameter_t* param) {
    if(!notification || !param) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    return llist_append(&notification->parameters, &param->it);
}

/**
   @ingroup pcb_core_notification
   @brief
   Get the number of parameters from a notification

   @details
   This function will return the number of parameters from a notification.

   @param notification a pointer to the notification, which was created with one of the notification create functions

   @return
    - The number of parameters contained within this notification
 */
uint32_t notification_parameterCount(notification_t* notification) {
    if(!notification) {
        pcb_error = pcb_error_invalid_parameter;
        return 0;
    }

    return llist_size(&notification->parameters);
}

/**
   @ingroup pcb_core_notification
   @brief
   Get the first parameter of a notification

   @details
   This function will return the first parameter of a notification, or NULL if no parameters are available.

   @param notification a pointer to the notification, which was created with one of the notification create functions

   @return
    - a pointer to a notification parameter
    - NULL no parameter is available.
 */
notification_parameter_t* notification_firstParameter(const notification_t* notification) {
    if(!notification) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    llist_iterator_t* it = llist_first(&notification->parameters);
    if(!it) {
        return NULL;
    }

    notification_parameter_t* param = llist_item_data(it, notification_parameter_t, it);
    return param;
}

/**
   @ingroup pcb_core_notification
   @brief
   Get the next parameter of a notification

   @details
   This function will return the next parameter of a notification, or NULL if no more parameters are available.

   @param param a pointer to the notification parameter

   @return
    - a pointer to a notification parameter
    - NULL no more parameter is available.
 */
notification_parameter_t* notification_nextParameter(notification_parameter_t* param) {
    if(!param) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    llist_iterator_t* it = llist_iterator_next(&param->it);
    if(!it) {
        return NULL;
    }

    notification_parameter_t* parameter = llist_item_data(it, notification_parameter_t, it);
    return parameter;
}

/**
   @ingroup pcb_core_notification
   @brief
   Get the previous parameter of a notification

   @details
   This function will return the previous parameter of a notification, or NULL if no more parameters are available.

   @param param a pointer to the notification parameter

   @return
    - a pointer to a notification parameter
    - NULL no more parameter is available.
 */
notification_parameter_t* notification_prevParameter(notification_parameter_t* param) {
    if(!param) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }


    llist_iterator_t* it = llist_iterator_prev(&param->it);
    if(!it) {
        return NULL;
    }

    notification_parameter_t* parameter = llist_item_data(it, notification_parameter_t, it);
    return parameter;
}

/**
   @ingroup pcb_core_notification
   @brief
   Get the last parameter of a notification

   @details
   This function will return the last parameter of a notification, or NULL if no parameters are available.

   @param notification a pointer to the notification, which was created with one of the notification create functions

   @return
    - a pointer to a notification parameter
    - NULL no parameter is available.
 */
notification_parameter_t* notification_lastParameter(notification_t* notification) {
    if(!notification) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    llist_iterator_t* it = llist_last(&notification->parameters);
    if(!it) {
        return NULL;
    }

    notification_parameter_t* param = llist_item_data(it, notification_parameter_t, it);
    return param;
}

/**
   @ingroup pcb_core_notification
   @brief
   Get a notification parameter

   @details
   Get a notification parameter using the parameter's name

   @param notification a pointer to the notification, which was created with one of the notification create functions
   @param name name of the parameter

   @return
    - a pointer to a notification parameter
    - NULL no parameter with the specified name is available.
 */
notification_parameter_t* notification_getParameter(const notification_t* notification, const char* name) {
    if(!notification || !name || !(*name)) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    notification_parameter_t* param = notification_firstParameter(notification);
    while(param) {
        if(strcmp(notification_parameter_name(param), name) == 0) {
            break;
        }
        param = notification_nextParameter(param);
    }

    return param;
}

/**
   @ingroup pcb_core_notification
   @brief
   Create a notifciation parameter

   @details
   Create a notification parameter with a name and value

   @param name name of the parameter
   @param value value of the parameter

   @return
    - a pointer to a notification parameter
    - NULL failed to create a notification parameter
 */
notification_parameter_t* notification_parameter_create(const char* name, const char* value) {
    if(!name || !(*name) || !value) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    notification_parameter_t* param = (notification_parameter_t*) calloc(1, sizeof(notification_parameter_t));
    if(!param) {
        pcb_error = pcb_error_no_memory;
        return NULL;
    }

    if(!string_initialize(&param->name, strlen(name) + 1)) {
        goto error_free;
    }
    if(!variant_initialize(&param->value, variant_type_string)) {
        goto error_cleanup_name;
    }

    llist_iterator_initialize(&param->it);
    string_fromChar(&param->name, name);
    if(!variant_setChar(&param->value, value)) {
        goto error_cleanup_value;
    }

    return param;

error_cleanup_value:
    variant_cleanup(&param->value);
error_cleanup_name:
    string_cleanup(&param->name);
error_free:
    free(param);
    return NULL;
}

/**
   @ingroup pcb_core_notification
   @brief
   Create a notifciation parameter

   @details
   Create a notification parameter with a name and value

   @param name name of the parameter
   @param value value of the parameter

   @return
    - a pointer to a notification parameter
    - NULL failed to create a notification parameter
 */
notification_parameter_t* notification_parameter_createVariant(const char* name, const variant_t* value) {
    if(!name || !(*name) || !value) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    notification_parameter_t* param = (notification_parameter_t*) calloc(1, sizeof(notification_parameter_t));
    if(!param) {
        pcb_error = pcb_error_no_memory;
        return NULL;
    }

    if(!string_initialize(&param->name, strlen(name) + 1)) {
        goto error_free;
    }
    if(!variant_initialize(&param->value, variant_type_unknown)) {
        goto error_cleanup_name;
    }

    llist_iterator_initialize(&param->it);
    string_fromChar(&param->name, name);
    variant_copy(&param->value, value);
    return param;

error_cleanup_name:
    string_cleanup(&param->name);
error_free:
    free(param);
    return NULL;
}

/**
   @ingroup pcb_core_notification
   @brief
   Destroy a notifciation parameter

   @details
   Destroy a notification parameter. If the parameter was already added to a notification, the parameter
   will be removed from the parameter list of the notification.

   @param param a pointer to a notification parameter
 */
void notification_parameter_destroy(notification_parameter_t* param) {
    if(!param) {
        pcb_error = pcb_error_invalid_parameter;
        return;
    }

    llist_iterator_take(&param->it);
    string_cleanup(&param->name);
    variant_cleanup(&param->value);
    free(param);
}

/**
   @ingroup pcb_core_notification
   @brief
   Change the name of a notification parameter

   @details
   Change the name of a notification parameter

   @param param a pointer to a notification parameter
   @param name the new name

   @return
    - true the name is set
    - false failed to set the name
 */
bool notification_parameter_setName(notification_parameter_t* param, const char* name) {
    if(!param) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    string_fromChar(&param->name, name);

    return true;
}

/**
   @ingroup pcb_core_notification
   @brief
   Change the value of a notification parameter

   @details
   Change the value of a notification parameter

   @param param a pointer to a notification parameter
   @param value the new name

   @return
    - true the value is set
    - false failed to set the value
 */
bool notification_parameter_setValue(notification_parameter_t* param, const char* value) {
    if(!param) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    variant_setChar(&param->value, value);

    return true;
}

/**
   @ingroup pcb_core_notification
   @brief
   Change the value of a notification parameter

   @details
   Change the value of a notification parameter

   @param param a pointer to a notification parameter
   @param value the new name

   @return
    - true the value is set
    - false failed to set the value
 */
bool notification_parameter_setVariant(notification_parameter_t* param, const variant_t* value) {
    if(!param) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    variant_copy(&param->value, value);

    return true;
}

/**
   @ingroup pcb_core_notification
   @brief
   Get the parameter name

   @details
   Get the parameter name

   @param param a pointer to the notificaton parameter

   @return
    - The name of the notificiation parameter
 */
const char* notification_parameter_name(notification_parameter_t* param) {
    if(!param) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    return string_buffer(&param->name);
}

/**
   @ingroup pcb_core_notification
   @brief
   Get the parameter value

   @details
   Get the parameter value

   @param param a pointner to the notificaton parameter

   @return
    - The value of the notificiation parameter
 */
char* notification_parameter_value(notification_parameter_t* param) {
    if(!param) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    char* value = variant_char(&param->value);

    return value;
}

/**
   @ingroup pcb_core_notification
   @brief
   Get the parameter value as a variant

   @details
   Get the parameter value as a variant

   @param param a pointer to a notification parameter

   @return
    - The value of the notificiation parameter
 */
variant_t* notification_parameter_variant(notification_parameter_t* param) {
    if(!param) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    return &param->value;
}
