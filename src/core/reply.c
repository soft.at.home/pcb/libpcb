/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <string.h>

#include <debug/sahtrace.h>

#include <pcb/common/error.h>
#include <pcb/core/notification.h>

#include "pcb_main_priv.h"
#include "local_object.h"

static int reply_sort_comparator(const llist_iterator_t* it1,
                                 const llist_iterator_t* it2,
                                 __attribute__((unused)) void* userdata) {
    reply_item_t* item1 = llist_item_data(it1, reply_item_t, it);
    reply_item_t* item2 = llist_item_data(it2, reply_item_t, it);
    object_t* object1 = NULL;
    object_t* object2 = NULL;
    char* path1 = NULL;
    char* path2 = NULL;
    int retval = 0;

    if((reply_item_type(item1) != reply_type_object) ||
       (reply_item_type(item2) != reply_type_object)) {
        goto exit;
    }

    object1 = reply_item_object(item1);
    object2 = reply_item_object(item2);
    path1 = object_pathChar(object1, path_attr_default);
    path2 = object_pathChar(object2, path_attr_default);

    retval = strcmp(path1, path2);

exit:
    free(path1);
    free(path2);
    return retval;
}

bool reply_initialize(reply_t* reply) {
    if(!llist_initialize(&reply->items)) {
        return false;
    }

    if(!llist_initialize(&reply->errors)) {
        return false;
    }

    reply->completed = false;

    return true;
}

void reply_clear(reply_t* reply) {
    if(!reply) {
        pcb_error = pcb_error_invalid_parameter;
        return;
    }

    reply_item_t* item = NULL;
    llist_iterator_t* it = llist_takeFirst(&reply->items);
    while(it) {
        item = llist_item_data(it, reply_item_t, it);
        reply_item_destroy(item);
        it = llist_takeFirst(&reply->items);
    }
}

void reply_errorsCleanup(reply_t* reply) {
    if(!reply) {
        pcb_error = pcb_error_invalid_parameter;
        return;
    }

    SAH_TRACEZ_INFO("pcb", "Cleanup error list");
    item_error_t* error = NULL;
    reply_item_t* item = NULL;
    llist_iterator_t* it = llist_takeFirst(&reply->errors);
    while(it) {
        SAH_TRACEZ_INFO("pcb", "Cleanup error");
        error = llist_item_data(it, item_error_t, it);
        item = &error->item;
        string_cleanup(&error->description);
        string_cleanup(&error->info);
        llist_iterator_take(&item->it);
        llist_iterator_take(&item->reply_it);
        free(item);
        it = llist_takeFirst(&reply->errors);
    }
}

void reply_cleanup(reply_t* reply) {
    if(!reply) {
        pcb_error = pcb_error_invalid_parameter;
        return;
    }

    SAH_TRACEZ_INFO("pcb", "Cleanup reply");
    reply_errorsCleanup(reply);
    reply_clear(reply);
}

request_t* reply_request(reply_t* reply) {
    if(!reply) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    return llist_item_data(reply, request_t, reply);
}

uint32_t reply_itemCount(reply_t* reply) {
    if(!reply) {
        pcb_error = pcb_error_invalid_parameter;
        return 0;
    }

    return llist_size(&reply->items);
}

reply_item_t* reply_firstItem(reply_t* reply) {
    if(!reply) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }
    llist_iterator_t* it = llist_first(&reply->items);
    if(it) {
        return llist_item_data(it, reply_item_t, it);
    }

    return NULL;
}

reply_item_t* reply_nextItem(reply_item_t* item) {
    if(!item) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }
    llist_iterator_t* it = llist_iterator_next(&item->it);
    if(it) {
        return llist_item_data(it, reply_item_t, it);
    }

    return NULL;
}

bool reply_itemsAvailable(reply_t* reply) {
    if(!reply) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(llist_isEmpty(&reply->items)) {
        return false;
    } else {
        return true;
    }
}

void reply_setCompleted(reply_t* reply, bool completed) {
    if(!reply) {
        pcb_error = pcb_error_invalid_parameter;
        return;
    }

    reply->completed = completed;
}

bool reply_isComplete(reply_t* reply) {
    if(!reply) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    return reply->completed;
}

bool reply_hasErrors(reply_t* reply) {
    if(!reply) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    return !llist_isEmpty(&reply->errors);
}

uint32_t reply_firstError(reply_t* reply) {
    if(!reply) {
        pcb_error = pcb_error_invalid_parameter;
        return 0;
    }

    if(!llist_isEmpty(&reply->errors)) {
        llist_iterator_t* it = llist_first(&reply->errors);
        item_error_t* error = llist_item_data(it, item_error_t, it);
        return error->error;
    }

    return 0;
}

bool reply_addObject(reply_t* reply, object_t* object) {
    if(!reply || !object) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    request_t* req = reply_request(reply);
    if(request_attributes(req) & request_getObject_keep_hierarchy) {
        object_t* parent = local_object_parent(object);
        object_t* root = object_root(object);
        if(parent != root) {
            return true;
        }
    }

    item_object_t* item = (item_object_t*) calloc(1, sizeof(item_object_t));
    if(!item) {
        pcb_error = pcb_error_no_memory;
        return false;
    }

    item->item.type = reply_type_object;
    item->object = object;

    pcb_peer_data_t* pd = peer_getUserData(req->peer);
    if(pd) {
        llist_append(&pd->replies, &item->item.reply_it);
    }
    llist_append(&reply->items, &item->item.it);
    req->state = request_state_waiting_for_reply;
    return true;
}

bool reply_addNotification(reply_t* reply, notification_t* notification) {
    if(!reply || !notification) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    item_notification_t* item = (item_notification_t*) calloc(1, sizeof(item_notification_t));
    if(!item) {
        pcb_error = pcb_error_no_memory;
        return false;
    }

    item->item.type = reply_type_notification;
    item->notification = notification;

    request_t* req = reply_request(reply);
    pcb_peer_data_t* pd = peer_getUserData(req->peer);
    if(pd) {
        llist_append(&pd->replies, &item->item.reply_it);
    }
    llist_append(&reply->items, &item->item.it);
    return true;
}

bool reply_addError(reply_t* reply, const uint32_t error, const char* description, const char* info) {
    if(!reply) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    item_error_t* item = (item_error_t*) calloc(1, sizeof(item_error_t));
    if(!item) {
        pcb_error = pcb_error_no_memory;
        return false;
    }

    SAH_TRACEZ_INFO("pcb", "Adding error %d - %s - %s to error list", error, description, info);
    item->item.type = reply_type_error;
    item->error = error;
    if(!description) {
        string_fromChar(&item->description, error_string(error));
    } else {
        string_fromChar(&item->description, description);
    }
    string_fromChar(&item->info, info);

    request_t* req = reply_request(reply);
    pcb_peer_data_t* pd = peer_getUserData(req->peer);
    if(pd) {
        llist_append(&pd->replies, &item->item.reply_it);
    }
    llist_append(&reply->items, &item->item.it);
    llist_append(&reply->errors, &item->it);
    return true;
}

bool reply_addFunctionReturn(reply_t* reply, variant_t* retval, argument_value_list_t* returnArgs) {
    if(!reply) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    item_function_return_t* item = (item_function_return_t*) calloc(1, sizeof(item_function_return_t));
    if(!item) {
        pcb_error = pcb_error_no_memory;
        return false;
    }

    if(!llist_initialize(&item->returnArguments)) {
        free(item);
        return false;
    }

    item->item.type = reply_type_function_return;
    variant_move(&item->retval, retval);
    llist_iterator_t* it = llist_takeFirst(returnArgs);
    while(it) {
        llist_append(&item->returnArguments, it);
        it = llist_takeFirst(returnArgs);
    }

    request_t* req = reply_request(reply);
    pcb_peer_data_t* pd = peer_getUserData(req->peer);
    if(pd) {
        llist_append(&pd->replies, &item->item.reply_it);
    }
    llist_append(&reply->items, &item->item.it);
    return true;
}

void reply_item_destroy(reply_item_t* item) {
    if(!item) {
        return;
    }
    llist_iterator_take(&item->it);
    llist_iterator_take(&item->reply_it);
    switch(item->type) {
    case reply_type_object:     /* objects are stored in a data model, so do not clear the objects here */
        free(item);
        break;
    case reply_type_notification: {
        item_notification_t* item_notification = llist_item_data(item, item_notification_t, item);
        notification_destroy(item_notification->notification);
        free(item);
    }
    break;
    case reply_type_error: {
        item_error_t* item_error = llist_item_data(item, item_error_t, item);
        /* keep the errors when they are still in the error list */
        if(!item_error->it.list) {
            string_cleanup(&item_error->description);
            string_cleanup(&item_error->info);
            free(item);
        } else {
            SAH_TRACEZ_INFO("pcb", "Keeping error in list");
        }
    }
    break;
    case reply_type_function_return: {
        item_function_return_t* item_functionReturn = llist_item_data(item, item_function_return_t, item);
        variant_cleanup(&item_functionReturn->retval);
        llist_iterator_t* it = llist_takeFirst(&item_functionReturn->returnArguments);
        while(it) {
            argument_value_t* val = llist_item_data(it, argument_value_t, it);
            free(val->parameterName);
            variant_cleanup(&val->value);
            free(val);
            it = llist_takeFirst(&item_functionReturn->returnArguments);
        }
        free(item);
    }
    break;
    default:
        break;
    }
}

reply_item_t* reply_item_take(reply_item_t* item) {
    if(!item) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    llist_iterator_take(&item->it);
    llist_iterator_take(&item->reply_it);
    return item;
}

reply_item_type_t reply_item_type(reply_item_t* item) {
    if(!item) {
        pcb_error = pcb_error_invalid_parameter;
        return reply_type_invalid;
    }

    return item->type;
}

object_t* reply_item_object(reply_item_t* item) {
    if(!item || (item->type != reply_type_object)) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    item_object_t* item_object = llist_item_data(item, item_object_t, item);
    return item_object->object;
}

notification_t* reply_item_notification(reply_item_t* item) {
    if(!item || (item->type != reply_type_notification)) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    item_notification_t* item_notification = llist_item_data(item, item_notification_t, item);
    return item_notification->notification;
}

uint32_t reply_item_error(reply_item_t* item) {
    if(!item || (item->type != reply_type_error)) {
        pcb_error = pcb_error_invalid_parameter;
        return 0;
    }

    item_error_t* item_error = llist_item_data(item, item_error_t, item);
    return item_error->error;
}

const char* reply_item_errorDescription(reply_item_t* item) {
    if(!item || (item->type != reply_type_error)) {
        pcb_error = pcb_error_invalid_parameter;
        return 0;
    }

    item_error_t* item_error = llist_item_data(item, item_error_t, item);
    return string_buffer(&item_error->description);
}

const char* reply_item_errorInfo(reply_item_t* item) {
    if(!item || (item->type != reply_type_error)) {
        pcb_error = pcb_error_invalid_parameter;
        return 0;
    }

    item_error_t* item_error = llist_item_data(item, item_error_t, item);
    return string_buffer(&item_error->info);
}

const variant_t* reply_item_returnValue(reply_item_t* item) {
    if(!item || (item->type != reply_type_function_return)) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    item_function_return_t* item_function_return = llist_item_data(item, item_function_return_t, item);
    return &item_function_return->retval;
}

argument_value_list_t* reply_item_returnArguments(reply_item_t* item) {
    if(!item || (item->type != reply_type_function_return)) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    item_function_return_t* item_function_return = llist_item_data(item, item_function_return_t, item);
    return &item_function_return->returnArguments;
}

bool reply_sort_object_name(reply_t* reply) {
    return llist_qsort(&reply->items, reply_sort_comparator, false, NULL);
}
