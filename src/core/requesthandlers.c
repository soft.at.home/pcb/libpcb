/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#define _GNU_SOURCE
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fnmatch.h>
#include <inttypes.h>

#include <components.h>

#ifdef CONFIG_PCB_ACL_USERMNGT
#include <usermngt/usermngt.h>
#else
#include <pwd.h>
#endif

#include <debug/sahtrace.h>

#include <pcb/common/error.h>
#include <pcb/utils/string_list.h>
#include <pcb/utils/peer.h>
#include <pcb/utils/variant_map.h>
#include <pcb/core/serialize.h>
#include <pcb/core/request.h>
#include <pcb/core/pcb_main.h>
#include <pcb/core/datamodel.h>
#include <pcb/core/mapping.h>

#include "requesthandlers_priv.h"
#include "notification_priv.h"
#include "object_priv.h"
#include "pcb_main_priv.h"
#include "parameter_priv.h"
#include "function_priv.h"
#include "core_priv.h"

#if defined(__GNUC__) && __GNUC__ >= 7
#define FALLTHROUGH __attribute__ ((fallthrough))
#else
#define FALLTHROUGH
#endif


typedef struct _forward_reply_info {
    peer_info_t* source_peer;
    llist_iterator_t it;
    uint32_t request_count;
} forward_reply_info_t;

static void default_destroy_forwardInfo(request_t* req, void* userdata) {
    (void) req;
    SAH_TRACEZ_IN("pcb_rh");

    forward_reply_info_t* info = (forward_reply_info_t*) userdata;
    if(info) {
        llist_iterator_take(&info->it);
        free(info);
    }
    SAH_TRACEZ_OUT("pcb_rh");
}

bool default_requestTranslatePath(request_t* req, uint32_t orig_attribs, uint32_t attributes, object_t* object, object_destination_t* dest) {
    (void) attributes;
    string_t requestPath;
    string_t objectPath;
    string_t destPath;
    string_t patternPath;

    string_initialize(&requestPath, 0);
    string_initialize(&objectPath, 0);
    string_initialize(&destPath, 0);
    string_initialize(&patternPath, 0);

    string_fromChar(&requestPath, request_path(req));
    string_fromChar(&destPath, uri_getPath(destination_URI(dest)));
    string_fromChar(&patternPath, request_pattern(req));

    uint32_t pathAttrib = path_attr_slash_seperator;
    pathAttrib |= (orig_attribs & request_common_path_key_notation) ? path_attr_key_notation : 0;
    pathAttrib |= (orig_attribs & request_common_path_alias) ? path_attr_alias : 0;
    object_path(object, &objectPath, pathAttrib);

    string_replaceChar(&requestPath, ".", "/");
    string_replaceChar(&patternPath, ".", "/");

    SAH_TRACEZ_INFO("pcb_rh", "Request path     = %s", string_buffer(&requestPath));
    SAH_TRACEZ_INFO("pcb_rh", "Object path      = %s", string_buffer(&objectPath));
    SAH_TRACEZ_INFO("pcb_rh", "Destination path = %s", string_buffer(&destPath));
    if(request_type(req) == request_type_find_objects) {
        SAH_TRACEZ_INFO("pcb_rh", "Pattern path     = %s", string_buffer(&patternPath));
    }

    if(string_length(&requestPath) <= string_length(&objectPath)) {
        string_copy(&requestPath, &destPath);
    } else {
        string_appendChar(&objectPath, "/");
        if(orig_attribs & request_common_path_key_notation) {
            SAH_TRACE_INFO("Using key path");
            string_appendChar(&destPath, "/");
        } else {
            SAH_TRACE_INFO("Using index path");
            string_appendChar(&destPath, "/%");
        }
        string_replace(&requestPath, &objectPath, &destPath);

        // Fix destPath
        string_fromChar(&destPath, uri_getPath(destination_URI(dest)));
    }

    SAH_TRACEZ_INFO("pcb_rh", "New request path = %s", string_buffer(&requestPath));
    request_setPath(req, string_buffer(&requestPath));
    if(request_type(req) == request_type_find_objects) {
        string_list_t objectPathList;
        string_list_t patternPathList;

        string_list_initialize(&objectPathList);
        string_list_initialize(&patternPathList);

        string_list_split(&objectPathList, &objectPath, "/", strlist_keep_empty_parts, string_case_sensitive);
        string_list_split(&patternPathList, &patternPath, "/", strlist_keep_empty_parts, string_case_sensitive);

        unsigned int size = string_list_size(&objectPathList);
        while(size--) {
            string_list_iterator_destroy(string_list_takeFirst(&patternPathList));
        }

        string_join(&patternPath, &patternPathList, "/");
        if(string_length(&patternPath) > 0) {
            string_prependChar(&patternPath, "/");
        }
        string_prepend(&patternPath, &destPath);
        SAH_TRACEZ_INFO("pcb_rh", "New pattern path = %s", string_buffer(&patternPath));
        request_setPattern(req, string_buffer(&patternPath));

        string_list_cleanup(&objectPathList);
        string_list_cleanup(&patternPathList);
    }

    string_cleanup(&requestPath);
    string_cleanup(&objectPath);
    string_cleanup(&destPath);
    string_cleanup(&patternPath);

    return true;
}

static bool defaultTranslateIndexPath(string_t* path, string_t* refPath, object_destination_t* dest) {
    string_t destPath;

    string_initialize(&destPath, 0);
    string_fromChar(&destPath, uri_getPath(destination_URI(dest)));
    if(string_containsChar(path, ".", string_case_sensitive)) {
        string_replaceChar(&destPath, "/", ".");
    }

    uint32_t parts = 1;
    uint32_t index = 0;
    while(string_buffer(&destPath)[index]) {
        if(string_buffer(&destPath)[index] == '.') {
            parts++;
        }
        index++;
    }

    index = 0;
    while(parts && string_buffer(path)[index]) {
        if(string_buffer(path)[index] == '.') {
            parts--;
        }
        index++;
    }
    if(string_buffer(path)[index] != 0) {
        string_appendChar(refPath, string_buffer(path) + index - 1);
    }
    string_copy(path, refPath);

    string_cleanup(&destPath);
    return true;
}

static bool defaultTranslateKeyPath(string_t* path, string_t* refPath, object_destination_t* dest) {
    unsigned int start = 0;
    string_t destPath, tmpPath;

    string_initialize(&destPath, 0);
    string_fromChar(&destPath, uri_getPath(destination_URI(dest)));
    if(string_containsChar(path, ".", string_case_sensitive)) {
        string_replaceChar(&destPath, "/", ".");
    }

    string_initialize(&tmpPath, 0);
    string_copy(&tmpPath, path);

    start = string_find(&tmpPath, start, &destPath, string_case_sensitive);
    if(start != UINT32_MAX) {
        if(start > 0) {
            string_left(path, &tmpPath, start);
        } else {
            string_clear(path);
        }

        string_append(path, refPath);

        string_appendChar(path, string_buffer(&tmpPath) + start + string_length(&destPath));
    }

    string_cleanup(&destPath);
    string_cleanup(&tmpPath);
    return true;
}

bool default_objectTranslatePath(object_t* reference, object_t* object, object_destination_t* dest, uint32_t attributes) {
    if(!object_isRemote(object)) {
        return false;
    }

    remote_object_info_t* object_info = object->userData;
    string_t refPath;
    string_t path;

    string_initialize(&refPath, 0);
    string_initialize(&path, 0);

    SAH_TRACEZ_INFO("pcb_rh", "Object index path  = %s.%s", string_buffer(&object_info->parentPathIndex), object->oname);
    SAH_TRACEZ_INFO("pcb_rh", "Object key path    = %s.%s", string_buffer(&object_info->parentPathKey), string_buffer(&object_info->key));
    SAH_TRACEZ_INFO("pcb_rh", "Dest path          = %s", uri_getPath(destination_URI(dest)));

    if(attributes & request_common_include_namespace) {
        attributes = path_attr_include_namespace | (attributes & request_common_path_alias);
    } else {
        attributes = (attributes & request_common_path_alias);
    }

    if(!object_translate(object, dest)) {
        string_cleanup(&refPath);
        string_cleanup(&path);
        return false;
    }

    // make sure aliases are available in mappings
    if(uri_getPath(destination_URI(dest)) && (strlen(uri_getPath(destination_URI(dest))) == string_length(&object_info->parentPathKey) + string_length(&object_info->key) + 1)) {
        if(object_isInstance(reference)) {
            if(object_hasParameter(object, "Alias")) {
                // move alias from mapped object to the reference
                if(object_hasParameter(object, "Alias")) {
                    const char* alias = object_da_parameterCharValue(object, "Alias");
                    parameter_t* alias_param = object_getParameter(reference, "Alias");
                    if(!alias_param) {
                        alias_param = parameter_create(reference, "Alias", parameter_type_string, 0);
                    }
                    if(alias_param) {
                        parameter_setFromChar(alias_param, alias);
                        parameter_commit(alias_param);
                    }
                }
            } else {
                // set the alias on the mapped object
                const char* alias = object_da_parameterCharValue(reference, "Alias");
                if(alias && *alias) {
                    parameter_t* alias_param = parameter_create(object, "Alias", parameter_type_string, 0);
                    if(alias_param) {
                        parameter_setFromChar(alias_param, alias);
                        parameter_commit(alias_param);
                    }
                }
            }
        }
    }

    char* pos = NULL;
    /* translate index path */
    object_path(reference, &refPath, path_attr_default | attributes);
    object_path(object, &path, path_attr_default | attributes);
    defaultTranslateIndexPath(&path, &refPath, dest);
    if(attributes & request_common_path_slash_seperator) {
        string_replaceChar(&path, ".", "/");
        pos = strrchr(string_buffer(&path), '/');
    } else {
        string_replaceChar(&path, "/", ".");
        pos = strrchr(string_buffer(&path), '.');
    }
    if(pos) {
        *pos = 0;
        string_fromChar(&object_info->parentPathIndex, string_buffer(&path));
        free(object->oname);
        object->oname = strdup(pos + 1);
    } else {
        string_fromChar(&object_info->parentPathIndex, "");
        free(object->oname);
        object->oname = strdup(string_buffer(&path));
    }

    /* translate key path */
    object_path(reference, &refPath, path_attr_key_notation | attributes);
    object_path(object, &path, path_attr_key_notation | attributes);
    defaultTranslateKeyPath(&path, &refPath, dest);

    if(attributes & request_common_path_slash_seperator) {
        string_replaceChar(&path, ".", "/");
        pos = strrchr(string_buffer(&path), '/');
    } else {
        string_replaceChar(&path, "/", ".");
        pos = strrchr(string_buffer(&path), '.');
    }
    if(pos) {
        *pos = 0;
        string_fromChar(&object_info->parentPathKey, string_buffer(&path));
        string_fromChar(&object_info->key, pos + 1);
    } else {
        string_fromChar(&object_info->parentPathKey, "");
        string_fromChar(&object_info->key, string_buffer(&path));
    }

    string_cleanup(&refPath);
    string_cleanup(&path);

    SAH_TRACEZ_INFO("pcb_rh", "Translated Object index path  = %s.%s", string_buffer(&object_info->parentPathIndex), object->oname);
    SAH_TRACEZ_INFO("pcb_rh", "Translated Object key path    = %s.%s", string_buffer(&object_info->parentPathKey), string_buffer(&object_info->key));

    return true;
}

static bool default_translateNotificationPath(object_t* reference, request_t* req, notification_t* notification, object_destination_t* dest, uint32_t attributes) {
    string_t refPath;
    string_t notifPath;

    string_initialize(&refPath, 0);
    string_initialize(&notifPath, 0);

    if(attributes & request_common_include_namespace) {
        attributes = path_attr_include_namespace;
    } else {
        attributes = 0;
    }

    string_fromChar(&notifPath, notification_objectPath(notification));
    if(request_attributes(req) & request_common_path_slash_seperator) {
        string_replaceChar(&notifPath, ".", "/");
    } else {
        string_replaceChar(&notifPath, "/", ".");
    }

    if(request_attributes(req) & request_common_path_key_notation) {
        object_path(reference, &refPath, path_attr_key_notation | attributes);
        defaultTranslateKeyPath(&notifPath, &refPath, dest);
    } else {
        object_path(reference, &refPath, path_attr_default | attributes);
        defaultTranslateIndexPath(&notifPath, &refPath, dest);
    }
    if(request_attributes(req) & request_common_path_slash_seperator) {
        string_replaceChar(&notifPath, ".", "/");
    } else {
        string_replaceChar(&notifPath, "/", ".");
    }

    notification_setObjectPath(notification, string_buffer(&notifPath));
    string_cleanup(&refPath);
    string_cleanup(&notifPath);

    return true;
}

static bool default_forwardReplyObject(object_t* object, request_t* req, forward_reply_info_t* info) {
    SAH_TRACEZ_IN("pcb_rh");

    if(info->it.list == NULL) {
        return false;
    }

    if(!info->source_peer) {
        return false;
    }

    object_t* reply_object = llist_item_data(info->it.list, object_t, mapRequests);

    if(reply_object->attributes & (object_attr_linked | object_attr_mapped)) {
        tree_item_t* parent = tree_item_parent(&req->child_requests);
        request_t* orig_req = tree_item_data(parent, request_t, child_requests);
        uint32_t attribs = request_attributes(orig_req) & ~(request_getObject_children | request_getObject_instances);
        SAH_TRACE_INFO("object tranlate path using attributes %x", attribs);
        if(default_objectTranslatePath(reply_object, object, object_firstDestination(reply_object), attribs)) {
            if(!default_reply_object(info->source_peer, object, 0, attribs, orig_req, request_parameterList(orig_req))) {
                SAH_TRACEZ_OUT("pcb_rh");
                return false;
            }
        }
        SAH_TRACEZ_OUT("pcb_rh");
        return true;
    }

    string_t objectPath;
    string_initialize(&objectPath, 64);
    object_path(object, &objectPath, path_attr_key_notation | path_attr_slash_seperator);

    parameter_t* client_param = NULL;
    parameter_t* plugin_param = NULL;
    variant_map_iterator_t* param_it = NULL;

    SAH_TRACEZ_INFO("pcb_rh", "Loop over recieved parameters");
    object_for_each_parameter(client_param, reply_object) {
        if(!client_param->map || !client_param->map->destination) {
            SAH_TRACEZ_NOTICE("pcb_rh", "Missing map or destination");
            continue;
        }

        if(strcmp(string_buffer(&objectPath), uri_getPath(destination_URI(client_param->map->destination))) != 0) {
            SAH_TRACEZ_NOTICE("pcb_rh", "Paths do not match ( %s, %s) ", string_buffer(&objectPath), uri_getPath(destination_URI(client_param->map->destination)));
            continue;
        }

        variant_map_for_each(param_it, &client_param->map->parameters) {
            plugin_param = object_getParameter(object, variant_map_iterator_key(param_it));
            if(!plugin_param) {
                SAH_TRACEZ_NOTICE("pcb_rh", "Plugin parameter not found");
                continue;
            }
            variant_copy(variant_map_iterator_data(param_it), parameter_getValue(plugin_param));
        }

        if(client_param->definition->handlers && client_param->definition->handlers->translate) {
            client_param->definition->handlers->translate(translate_value_to_client, &client_param->value[client_param->currentConfig], &client_param->map->parameters);
        } else {
            param_it = variant_map_first(&client_param->map->parameters);
            variant_copy(&client_param->value[client_param->currentConfig], variant_map_iterator_data(param_it));
        }
    }

    function_t* client_func = NULL;
    function_t* plugin_func = NULL;
    SAH_TRACEZ_INFO("pcb_rh", "Loop over recieved functions");
    object_for_each_function(client_func, reply_object) {
        if(!client_func->destination) {
            SAH_TRACEZ_NOTICE("pcb_rh", "No destination for function %s", function_name(client_func));
            continue;
        }

        if(strcmp(string_buffer(&objectPath), uri_getPath(destination_URI(client_func->destination))) != 0) {
            SAH_TRACEZ_NOTICE("pcb_rh", "Paths do not match ( %s, %s) ", string_buffer(&objectPath), uri_getPath(destination_URI(client_func->destination)));
            continue;
        }
        const char* func_name = client_func->plugin_name ? client_func->plugin_name : function_name(client_func);
        plugin_func = object_getFunction(object, func_name);
        if(function_type(client_func) != function_type(plugin_func)) {
            if(function_type(plugin_func) == function_type_custom) {
                function_t* func = NULL;
                func = function_allocate(function_name(client_func), function_typeName(plugin_func), function_type_custom, function_attributes(plugin_func));
                func->plugin_name = client_func->plugin_name;
                func->destination = client_func->destination;
                llist_insertAfter(&reply_object->functions, &client_func->it, &func->it);
                function_delete(client_func);
                client_func = func;
            } else {
                client_func->definition->returnType = plugin_func->definition->returnType;
                client_func->definition->attributes = plugin_func->definition->attributes;
            }
        }
        function_argument_t* arg = function_firstArgument(client_func);
        while(arg) {
            argument_delete(arg);
            arg = function_firstArgument(client_func);
        }
        arg = function_firstArgument(plugin_func);
        while(arg) {
            llist_append(&client_func->definition->arguments, &arg->it);
            arg = function_firstArgument(plugin_func);
        }
    }
    string_cleanup(&objectPath);

    SAH_TRACEZ_OUT("pcb_rh");
    return true;
}

static bool default_forwardNotification(request_t* req, notification_t* notification, forward_reply_info_t* info) {
    tree_item_t* parent = tree_item_parent(&req->child_requests);
    if(!parent) {
        SAH_TRACEZ_ERROR("pcb_rh", "Parent request not available");
        return false;
    }
    request_t* orig_req = tree_item_data(parent, request_t, child_requests);

    if(!info->source_peer) {
        SAH_TRACEZ_ERROR("pcb_rh", "Peer not available");
        return false;
    }

    if(info->it.list == NULL) {
        SAH_TRACEZ_ERROR("pcb_rh", "Reply object not available");
        return false;
    }
    object_t* reply_object = llist_item_data(info->it.list, object_t, mapRequests);

    if(reply_object->attributes & (object_attr_linked | object_attr_mapped)) {
        parent = tree_item_parent(&req->child_requests);
        orig_req = tree_item_data(parent, request_t, child_requests);
        default_translateNotificationPath(reply_object, orig_req, notification, object_firstDestination(reply_object), request_attributes(orig_req));
        return pcb_writeNotification(info->source_peer, orig_req, notification);
    }


    if(notification_type(notification) == notify_value_changed) {
        SAH_TRACEZ_INFO("pcb_rh", "translate value changed notification");
        /* translate parameter */
        parameter_t* client_param = NULL;
        notification_parameter_t* plugin_parameter = notification_getParameter(notification, "parameter");
        notification_parameter_t* plugin_oldvalue = notification_getParameter(notification, "oldvalue");
        notification_parameter_t* plugin_newvalue = notification_getParameter(notification, "newvalue");
        char* plugin_parameter_name = notification_parameter_value(plugin_parameter);
        variant_map_iterator_t* it = NULL;
        object_for_each_parameter(client_param, reply_object) {
            if(!client_param->map || !client_param->map->destination ||
               (strcmp(notification_objectPath(notification), uri_getPath(destination_URI(client_param->map->destination))) != 0)) {
                continue;
            }
            variant_map_for_each(it, &client_param->map->parameters) {
                if(strcmp(plugin_parameter_name, variant_map_iterator_key(it)) == 0) {
                    variant_copy(variant_map_iterator_data(it), notification_parameter_variant(plugin_oldvalue));
                    if(client_param->definition->handlers && client_param->definition->handlers->translate) {
                        client_param->definition->handlers->translate(translate_value_to_client, &client_param->value[client_param->currentConfig], &client_param->map->parameters);
                    } else {
                        variant_copy(&client_param->value[client_param->currentConfig], variant_map_iterator_data(it));
                    }
                    variant_copy(notification_parameter_variant(plugin_oldvalue), &client_param->value[client_param->currentConfig]);
                    variant_copy(variant_map_iterator_data(it), notification_parameter_variant(plugin_newvalue));
                    if(client_param->definition->handlers && client_param->definition->handlers->translate) {
                        client_param->definition->handlers->translate(translate_value_to_client, &client_param->value[client_param->currentConfig], &client_param->map->parameters);
                    } else {
                        variant_copy(&client_param->value[client_param->currentConfig], variant_map_iterator_data(it));
                    }
                    variant_copy(notification_parameter_variant(plugin_newvalue), &client_param->value[client_param->currentConfig]);
                    variant_setChar(notification_parameter_variant(plugin_parameter), parameter_name(client_param));
                    break;
                }
            }
        }
        free(plugin_parameter_name);
        int32_t result = 0;
        if(!variant_compare(notification_parameter_variant(plugin_newvalue), notification_parameter_variant(plugin_oldvalue), &result)) {
            SAH_TRACEZ_NOTICE("pcb_rh", "Variant compare failed");
        }
        if(result == 0) {
            return true;
        }
    }
    if(notification_objectPath(notification) && *(notification_objectPath(notification))) {
        /* translate path */
        string_t objectPath;
        uint32_t path_flags = request_attributes(orig_req) & ( request_common_path_key_notation | request_common_path_slash_seperator | request_common_path_alias);
        string_initialize(&objectPath, 64);

        if((object_attributes(object_root(reply_object)) & object_attr_sysbus_root) == 0) {
            // This is not the sysbus root.
            path_flags = path_flags | path_attr_include_namespace;
        }
        object_path(reply_object, &objectPath, path_flags);
        notification_setObjectPath(notification, string_buffer(&objectPath));
        string_cleanup(&objectPath);
    }
    return pcb_writeNotification(info->source_peer, orig_req, notification);
}

static bool default_forwardedReply(request_t* req, pcb_t* pcb, peer_info_t* from, void* userdata) {
    SAH_TRACEZ_IN("pcb_rh");
    (void) pcb;
    (void) from;
    SAH_TRACEZ_INFO("pcb_rh", "Got reply from a child request");

    reply_t* reply = request_reply(req);
    forward_reply_info_t* info = NULL;
    reply_item_t* item = reply_firstItem(reply);
    if(!reply) {
        goto exit_reply;
    }

    info = (forward_reply_info_t*) userdata;
    if(!info) {
        SAH_TRACEZ_ERROR("pcb_rh", "No reply info available");
        return false;
    }

    while(item) {
        SAH_TRACEZ_INFO("pcb_rh", "Forwarding item (type = %d)", reply_item_type(item));
        switch(reply_item_type(item)) {
        case reply_type_object:
            default_forwardReplyObject(reply_item_object(item), req, info);
            break;
        case reply_type_notification:
            default_forwardNotification(req, reply_item_notification(item), info);
            break;
        case reply_type_error: {
            SAH_TRACEZ_ERROR("pcb_rh", "Recieved error, forwarding it: %d - %s - %s", reply_item_error(item), reply_item_errorDescription(item), reply_item_errorInfo(item));
            tree_item_t* parent = tree_item_parent(&req->child_requests);
            if(parent) {
                request_t* orig_req = tree_item_data(parent, request_t, child_requests);
                pcb_writeError(info->source_peer, orig_req, reply_item_error(item), reply_item_errorDescription(item), reply_item_errorInfo(item));
            }
        }
        break;
        case reply_type_function_return: {
            SAH_TRACE_INFO("Forwarding reply function return type");
            const variant_t* val = reply_item_returnValue(item);
            pcb_writeFunctionReturnBegin(info->source_peer, req);
            pcb_writeReturnValue(info->source_peer, req, val);
            argument_value_list_t* args = reply_item_returnArguments(item);
            if(!llist_isEmpty(args)) {
                pcb_writeReturnArgumentsStart(info->source_peer, req);
                pcb_writeReturnArguments(info->source_peer, req, args);
            }
            default_sendErrorList(info->source_peer, req);
            pcb_writeFunctionReturnEnd(info->source_peer, req);
        }
        break;
        default:
            SAH_TRACE_WARNING("Not handled reply type !!!");
            break;
        }
        item = reply_nextItem(item);
    }

exit_reply:
    SAH_TRACEZ_OUT("pcb_rh");
    return true;
}

static bool default_forwardedDone(request_t* req, pcb_t* pcb, peer_info_t* from, void* userdata) {
    SAH_TRACEZ_IN("pcb_rh");
    (void) pcb;
    (void) from;

    bool firstReply = false;
    forward_reply_info_t* info = (forward_reply_info_t*) userdata;
    if(!info) {
        SAH_TRACEZ_ERROR("pcb_rh", "No reply info available");
        return false;
    }

    if(info->request_count > 0) {
        firstReply = true;
        info->request_count--;
    }

    if(!info->request_count) {
        SAH_TRACEZ_INFO("pcb_rh", "All child requests are done there will come no more reply");
        if(!info->source_peer) {
            SAH_TRACEZ_ERROR("pcb_rh", "Peer not available");
            SAH_TRACEZ_OUT("pcb_rh");
            return false;
        }

        tree_item_t* parent = tree_item_parent(&req->child_requests);
        if(!parent) {
            SAH_TRACEZ_INFO("pcb_rh", "Parent request not available");
            SAH_TRACEZ_OUT("pcb_rh");
            return true;
        }
        request_t* orig_req = tree_item_data(parent, request_t, child_requests);

        if(info->it.list == NULL) {
            SAH_TRACEZ_ERROR("pcb_rh", "Reply object not available");
            SAH_TRACEZ_OUT("pcb_rh");
            return false;
        }

        object_t* reply_object = llist_item_data(info->it.list, object_t, mapRequests);
        SAH_TRACEZ_INFO("pcb_rh", "=====> orignal request type = %d ====> flags = 0x%X", request_type(orig_req), request_attributes(orig_req));
        if((request_type(orig_req) == request_type_set_object) &&
           (request_attributes(orig_req) & request_setObject_validate_only)) {
            SAH_TRACEZ_INFO("pcb_rh", "Validate only, rollback object");
            if(!object_validate(reply_object)) {
                char* objectPath = object_pathChar(reply_object, request_attributes(orig_req) & (request_common_path_key_notation | request_common_path_slash_seperator));
                pcb_writeError(info->source_peer, orig_req, pcb_error_invalid_value, error_string(pcb_error_invalid_value), objectPath);
                free(objectPath);
            }
            object_rollback(reply_object);
        } else {
            if(llist_isEmpty(&(req->reply.errors)) && reply_object && !(reply_object->attributes & (object_attr_mapped | object_attr_linked)) && (request_type(orig_req) != request_type_exec_function)) {
                bool sendObject = (firstReply && (request_attributes(orig_req) & request_notify_only) == 0) ||
                    (!firstReply && (request_attributes(orig_req) & request_notify_no_updates) == 0);
                if(sendObject) {
                    if((!firstReply && (reply_object->state == object_state_ready)) || firstReply) {
                        SAH_TRACEZ_INFO("pcb_rh", "Reply to peer with fd = %d", peer_getFd(info->source_peer));
                        uint32_t attribs = request_attributes(orig_req) & ~(request_getObject_children | request_getObject_instances);
                        if(!default_reply_object(info->source_peer, reply_object, 0, attribs, orig_req, request_parameterList(orig_req))) {
                            SAH_TRACEZ_OUT("pcb_rh");
                            return false;
                        }
                    } else {
                        local_object_update(reply_object, request_parameterList(orig_req));
                    }
                }
            }
        }

        if((request_attributes(orig_req) & request_notify_all) == 0) {
            peer_info_t* peer = info->source_peer;
            req->state = request_state_done;
            request_destroy(req);
            if(!request_hasChildren(orig_req)) {
                SAH_TRACEZ_INFO("pcb_req", "original request is done");
                default_sendErrorList(peer, orig_req);
                pcb_replyEnd(peer, orig_req);
                request_destroy(orig_req);
            }
            SAH_TRACEZ_INFO("pcb_rh", "cleanup forward info");
        } else {
            SAH_TRACEZ_INFO("pcb_rh", "original request is done");
            default_sendErrorList(info->source_peer, orig_req);
            pcb_replyEnd(info->source_peer, orig_req);
        }
    }
    SAH_TRACEZ_OUT("pcb_rh");
    return true;
}

static void default_forwardedCancel(request_t* req, void* userdata) {
    SAH_TRACEZ_IN("pcb_rh");

    forward_reply_info_t* info = (forward_reply_info_t*) userdata;
    if(!info) {
        SAH_TRACEZ_ERROR("pcb_rh", "No reply info available");
        return;
    }

    tree_item_t* parent = tree_item_parent(&req->child_requests);
    if(!parent) {
        SAH_TRACEZ_ERROR("pcb_rh", "Parent request not available");
        return;
    }
    request_t* orig_req = tree_item_data(parent, request_t, child_requests);
    request_destroy(req);
    if(!request_hasChildren(orig_req)) {
        SAH_TRACEZ_INFO("pcb_rh", "original request is done");
        request_destroy(orig_req);
    }

    SAH_TRACEZ_OUT("pcb_rh");
}

static forward_reply_info_t* default_forward_info(peer_info_t* peer, object_t* object) {
    /* allocate reply info structure */
    forward_reply_info_t* info = (forward_reply_info_t*) calloc(1, sizeof(forward_reply_info_t));
    if(!info) {
        SAH_TRACEZ_ERROR("pcb_rh", "Failed to allocate memory for forwarding info");
        return false;
    }

    /* set source peer */
    info->source_peer = peer;

    /* set reply object */
    llist_append(&object->mapRequests, &info->it);

    return info;
}

static bool default_forward_mapped(request_t* req, peer_info_t* peer, object_t* object, function_t* function, uint32_t depth, object_destination_t* destination) {
    request_t* master_req = request_create(request_type_unknown); /* create a dummy request, to hold all forwarded and the reply */
    if(!master_req) {
        SAH_TRACEZ_ERROR("pcb_rh", "Failed to create master request, not forwarding");
        return false;
    }

    /* add as a child to original request */
    request_addChild(req, master_req);

    /* forward request */
    uint32_t attributes = request_attributes(req);
    attributes |= (request_common_path_key_notation | request_common_path_slash_seperator | request_no_object_caching);
    attributes &= ~(request_common_include_namespace);
    if(object_attributes(object) & object_attr_mapped) {
        attributes &= ~(request_getObject_children | request_getObject_instances);
        depth = 0;
    }
    request_t* forward = NULL;
    if((request_type(req) == request_type_exec_function) && function && function->plugin_name) {
        forward = request_copy(req, depth, attributes, request_copy_default);
    } else {
        forward = request_copy(req, depth, attributes, request_copy_no_parameters);
    }
    if(!forward) {
        SAH_TRACEZ_ERROR("pcb_rh", "could not create forwarding request");
        char* uri = uri_string(destination_URI(destination));
        reply_addError(request_reply(req), pcb_error_no_memory, error_string(pcb_error_no_memory), uri);
        free(uri);
        return false;
    }
    if((request_type(forward) == request_type_exec_function) && function && function->plugin_name) {
        request_setFunctionName(forward, function->plugin_name);
    }
    if(object_attributes(object) & (object_attr_linked | object_attr_mapped)) {
        /* translate request path */
        default_requestTranslatePath(forward, request_attributes(req), attributes, object, destination);
        /* translate request */
        if(!request_translate(req, forward, destination)) {
            SAH_TRACEZ_ERROR("pcb_rh", "request can not be translated");
            request_destroy(forward);
            return false;
        }
    }
    peer_info_t* dest_peer = destination_peer(destination);
    if(!dest_peer || !peer_isConnected(dest_peer)) {
        SAH_TRACEZ_ERROR("pcb_rh", "can not forward request, no peer available");
        char* uri = uri_string(destination_URI(destination));
        reply_addError(request_reply(req), pcb_error_not_connected, error_string(pcb_error_not_connected), uri);
        free(uri);
        request_destroy(forward);
        return false;
    }
    forward_reply_info_t* info = default_forward_info(peer, object);
    if(!info) {
        char* uri = uri_string(destination_URI(destination));
        reply_addError(request_reply(req), pcb_error_no_memory, error_string(pcb_error_no_memory), uri);
        free(uri);
        request_destroy(forward);
        return false;
    }
    request_setData(master_req, info);
    request_setDestroyHandler(master_req, default_destroy_forwardInfo);
    /* set handlers on master request */
    request_setCancelHandler(master_req, default_forwardedCancel);
    request_setReplyHandler(master_req, default_forwardedReply);
    request_setDoneHandler(master_req, default_forwardedDone);
    SAH_TRACEZ_INFO("pcb_rh", "Forwarding mapped ...");
    if(!request_forward(req, peer, forward, dest_peer)) {
        SAH_TRACEZ_ERROR("pcb_rh", "Forwarding failed");
        char* uri = uri_string(destination_URI(destination));
        reply_addError(request_reply(req), pcb_error_not_connected, error_string(pcb_error_not_connected), uri);
        free(uri);
        request_destroy(forward);
        return false;
    }
    info->request_count++;
    request_addChild(master_req, forward);
    SAH_TRACEZ_INFO("pcb_rh", "Forwarding done");
    return true;
}

static request_t* default_build_forward_request(request_t* original, object_t* object, uint32_t attributes, object_destination_t* dest) {
    /* remove some request attribues */
    attributes &= ~(request_getObject_instances |
                    request_getObject_children |
                    request_getObject_template_info |
                    request_notify_object_added |
                    request_notify_object_deleted |
                    request_notify_only |
                    request_common_include_namespace);
    /* add some other attributes */
    attributes |= request_common_path_slash_seperator |
        request_common_path_key_notation |
        request_no_object_caching;
    SAH_TRACEZ_INFO("pcb_rh", "Forwarding attributes = 0x%X", attributes);
    // Forwarding a find_objects is not possible, adapt it to a get_object
    bool back_to_find_objects = false;
    if(original->type == request_type_find_objects) {
        SAH_TRACEZ_INFO("pcb_rh", "Temporarily changing request %p type from find_objects to get_object", original);
        original->type = request_type_get_object;
        back_to_find_objects = true;
    }
    request_t* forward = request_copy(original, 0, attributes, request_copy_no_parameters);
    if(back_to_find_objects) {
        original->type = request_type_find_objects;
    }

    if(!forward) {
        return NULL;
    }

    SAH_TRACEZ_INFO("pcb_rh", "Forwarding path = %s", uri_getPath(destination_URI(dest)));
    request_setPath(forward, uri_getPath(destination_URI(dest)));

    parameter_t* parameter = NULL;
    parameter_iterator_t* req_param = NULL;
    variant_map_iterator_t* it = NULL;

    object_for_each_parameter(parameter, object) {
        if(parameter->map && (parameter->map->destination == dest)) {
            if(!llist_isEmpty(request_parameterList(original))) {
                req_param = request_findParameter(original, parameter_name(parameter));
            } else {
                req_param = NULL;
            }
            if(llist_isEmpty(request_parameterList(original)) || req_param) {
                variant_map_for_each(it, &parameter->map->parameters) {
                    SAH_TRACEZ_INFO("pcb_rh", "Adding parameter %s to forwarded request", variant_map_iterator_key(it));
                    if(request_type(original) == request_type_set_object) {
                        if(!request_addParameter(forward, variant_map_iterator_key(it), variant_map_iterator_data(it))) {
                            SAH_TRACEZ_ERROR("pcb_rh", "Failed adding parameter %s to forwarded request", variant_map_iterator_key(it));
                        }
                    } else {
                        if(!request_addParameter(forward, variant_map_iterator_key(it), NULL)) {
                            SAH_TRACEZ_ERROR("pcb_rh", "Failed adding parameter %s to forwarded request", variant_map_iterator_key(it));
                        }
                    }
                }
            }
        }
    }

    return forward;
}

static bool default_forward_request(peer_info_t* peer, object_t* object, uint32_t depth, uint32_t attributes, request_t* req) {
    peer_info_t* dest_peer = NULL;
    request_t* master_req = request_create(request_type_unknown); /* create a dummy request, to hold all forwarded and the reply */
    if(!master_req) {
        SAH_TRACEZ_ERROR("pcb_rh", "Failed to create master request, not forwarding");
        return false;
    }

    /* add as a child to original request */
    request_addChild(req, master_req);

    forward_reply_info_t* info = default_forward_info(peer, object);
    if(!info) {
        return false;
    }

    /* set reply data in master req as user data */
    request_setData(master_req, info);
    request_setDestroyHandler(master_req, default_destroy_forwardInfo);
    /* set handlers on master request */
    request_setCancelHandler(master_req, default_forwardedCancel);
    request_setReplyHandler(master_req, default_forwardedReply);
    request_setDoneHandler(master_req, default_forwardedDone);
    reply_t* reply = request_reply(req);

    /* loop over all destinations and forward */
    object_destination_t* dest = object_firstDestination(object);
    while(dest) {
        SAH_TRACEZ_INFO("pcb_rh", "Checking destination [%s:%s/%s]", uri_getHost(destination_URI(dest)), uri_getPort(destination_URI(dest)), uri_getPath(destination_URI(dest)));

        dest_peer = destination_peer(dest);
        if(!dest_peer) {
            SAH_TRACEZ_ERROR("pcb_rh", "can not forward request, no peer available");
            char* uri = uri_string(destination_URI(dest));
            SAH_TRACEZ_ERROR("pcb_rh", "**** Error added to reply *****");
            reply_addError(reply, pcb_error_not_connected, error_string(pcb_error_not_connected), uri);
            free(uri);
            dest = object_nextDestination(object, dest);
            continue;
        }

        if(!peer_isConnected(dest_peer)) {
            SAH_TRACEZ_ERROR("pcb_rh", "can not forward request, peer not connected");
            char* uri = uri_string(destination_URI(dest));
            reply_addError(reply, pcb_error_not_connected, error_string(pcb_error_not_connected), uri);
            free(uri);
            dest = object_nextDestination(object, dest);
            continue;
        }

        request_t* forward = default_build_forward_request(req, object, attributes, dest);
        /* create forward request */
        if(!forward) {
            SAH_TRACEZ_ERROR("pcb_rh", "Failed to allocate forwarding request");
            char* uri = uri_string(destination_URI(dest));
            reply_addError(reply, pcb_error_not_connected, error_string(pcb_error_not_connected), uri);
            free(uri);
            dest = object_nextDestination(object, dest);
            continue;
        }

        /* only forward if we need parameters from this destination or functions are requsted */
        if(!llist_isEmpty(request_parameterList(forward)) || (request_attributes(forward) & request_getObject_functions)) {
            SAH_TRACEZ_INFO("pcb_rh", "forwarding req %p - master_req %p - forward_req %p...", req, master_req, forward);
            if(!request_forward(req, peer, forward, dest_peer)) {
                SAH_TRACEZ_ERROR("pcb_rh", "Forwarding failed");
                request_destroy(forward);
            } else {
                request_addChild(master_req, forward);
                info->request_count++;
            }
        } else {
            SAH_TRACEZ_INFO("pcb_rh", "No need to forward");
            /* no need to forward */
            request_destroy(forward);
        }
        dest = object_nextDestination(object, dest);
    }

    if(!request_hasChildren(master_req)) {
        SAH_TRACEZ_INFO("pcb_rh", "Nothing forwarded");
        request_destroy(master_req);
        if(request_type(req) == request_type_get_object) {
            return default_reply_object(peer, object, depth, attributes, req, request_parameterList(req));
        }
    }

    return true;
}

static bool default_setValues(peer_info_t* peer, object_t* object, request_t* req) {
    bool retval = true;
    llist_iterator_t* it = request_firstParameter(req);
    parameter_t* parameter = NULL;
    /* use the reply part of the request to add the errors */
    reply_t* reply = request_reply(req);

    while(it) {
        SAH_TRACEZ_INFO("pcb_rh", "Process parameter %s ....", request_parameterName(it));
        parameter = object_getParameter(object, request_parameterName(it));
        if(!parameter) {
            SAH_TRACEZ_WARNING("pcb_rh", "Parameter %s not found", request_parameterName(it));
            reply_addError(reply, pcb_error_not_found, error_string(pcb_error_not_found), request_parameterName(it));
            it = request_nextParameter(it);
            retval = false;
            continue;
        }
        SAH_TRACEZ_NOTICE("pcb_rh", "Parameter %s found, state = %d", parameter_name(parameter), parameter_state(parameter));
        if(parameter_isReadOnly(parameter)) {
            SAH_TRACEZ_WARNING("pcb_rh", "Parameter %s is read-only", request_parameterName(it));
            reply_addError(reply, pcb_error_read_only, error_string(pcb_error_read_only), request_parameterName(it));
            it = request_nextParameter(it);
            retval = false;
            continue;
        }
        if(!parameter_canWrite(parameter, request_userID(req))) {
            SAH_TRACEZ_WARNING("pcb_rh", "Parameter %s access denied", request_parameterName(it));
            reply_addError(reply, EACCES, error_string(EACCES), request_parameterName(it));
            it = request_nextParameter(it);
            retval = false;
            continue;
        }
        variant_t* value = request_parameterValue(it);
        char* v = variant_char(value);
        SAH_TRACEZ_INFO("pcb_rh", "Set parameter %s = [%s]", request_parameterName(it), v);
        free(v);
        if(parameter->map) {
            /* Need to forward the set */
            if(parameter->definition->handlers && parameter->definition->handlers->translate) {
                parameter->definition->handlers->translate(translate_value_to_plugin, value, &parameter->map->parameters);
            } else {
                variant_map_iterator_t* param_it = variant_map_first(&parameter->map->parameters);
                variant_copy(variant_map_iterator_data(param_it), value);
            }
        } else {
            if(!parameter_setValue(parameter, value)) {
                SAH_TRACEZ_WARNING("pcb_rh", "Parameter %s invalid value", request_parameterName(it));
                reply_addError(reply, pcb_error_invalid_value, error_string(pcb_error_invalid_value), request_parameterName(it));
                retval = false;
            }
        }
        it = request_nextParameter(it);
    }
    if(object_hasDestinations(object) && retval) {
        retval = default_forward_request(peer, object, 0, request_attributes(req), req);
    }
    return retval;
}

bool default_sendErrorList(peer_info_t* peer, request_t* req) {
    reply_t* reply = request_reply(req);
    if(!reply) {
        return false;
    }
    if(llist_isEmpty(&reply->errors)) {
        return true;
    }
    llist_iterator_t* it = NULL;
    bool first = true;
    pcb_writeErrorListBegin(peer, req);
    llist_for_each(it, &reply->errors) {
        item_error_t* error = llist_item_data(it, item_error_t, it);
        if(!first) {
            pcb_writeErrorListNext(peer, req);
        } else {
            first = false;
        }
        SAH_TRACEZ_INFO("pcb_rh", "Writing error (%d - %s - %s)", error->error, string_buffer(&error->description), string_buffer(&error->info));
        pcb_writeError(peer, req, error->error, string_buffer(&error->description), string_buffer(&error->info));
    }
    pcb_writeErrorListEnd(peer, req);
    reply_errorsCleanup(reply);

    return true;
}

bool default_replyParameters(peer_info_t* peer, object_t* object, request_t* req) {
    /* parameter name, parameter type, parameter value */
    bool hideParameterValue = false;
    parameter_t* parameter = NULL;
    pcb_writeParameterListBegin(peer, req);
    bool first = true;
    object_for_each_parameter(parameter, object) {
        if(!parameter_canRead(parameter, request_userID(req))) {
            SAH_TRACEZ_ERROR("pcb_rh", "User ID: %" PRIu32 " has been denied access to parameter %s", request_userID(req), parameter_name(parameter));
            continue;
        }
        hideParameterValue = !(parameter_canReadValue(parameter, request_userID(req)));
        /* is the parameter requested? yes if all are requested (list is empty) or the parameter is specified */
        if(!llist_isEmpty(request_parameterList(req)) && (request_type(req) != request_type_create_instance)) {
            parameter_iterator_t* req_param = request_findParameter(req, parameter_name(parameter));
            if(req_param) {
                if(!first) {
                    pcb_writeParameterListNext(peer, req);
                }
                SAH_TRACEZ_INFO("pcb_rh", "Adding parameter %s in object reply", parameter_name(parameter));
                pcb_writeParameter(peer, req, parameter, hideParameterValue);
                first = false;
            } else {
                SAH_TRACEZ_NOTICE("pcb_rh", "Parameter %s not found in request", parameter_name(parameter));
            }
        } else {
            if(!first) {
                pcb_writeParameterListNext(peer, req);
            }
            SAH_TRACEZ_INFO("pcb_rh", "Adding parameter %s in object reply", parameter_name(parameter));
            pcb_writeParameter(peer, req, parameter, hideParameterValue);
            first = false;
        }
    }
    pcb_writeParameterListEnd(peer, req);

    return true;
}

static inline bool default_getObjectChildren(peer_info_t* peer, object_t* object, uint32_t depth, uint32_t attributes, request_t* req) {
    object_t* child = NULL;
    object_for_each_child(child, object) {
        if(depth == 0) {
            if(!default_getObject(peer, child, 0, attributes & ~(request_getObject_template_info | request_getObject_children | request_getObject_instances | request_getObject_parameters | request_getObject_functions), req)) {
                return false;
            }
        } else {
            if(!default_getObject(peer, child, (depth == UINT32_MAX) ? depth : (depth - 1), attributes, req)) {
                return false;
            }
        }
    }
    return true;
}

static inline bool default_getObjectInstances(peer_info_t* peer, object_t* object, uint32_t depth, uint32_t attributes, request_t* req) {
    object_t* child = NULL;
    object_for_each_instance(child, object) {
        if(depth == 0) {
            if(!default_getObject(peer, child, 0, attributes & ~(request_getObject_template_info | request_getObject_children | request_getObject_instances | request_getObject_parameters | request_getObject_functions), req)) {
                return false;
            }
        } else {
            if(!default_getObject(peer, child, (depth == UINT32_MAX) ? depth : (depth - 1), attributes, req)) {
                return false;
            }
        }
    }
    return true;
}

static bool default_getObject_internal(peer_info_t* peer, object_t* object, uint32_t depth, uint32_t attributes, request_t* req, llist_t* parameters) {
    if(!object_canRead(object, request_userID(req))) {
        SAH_TRACEZ_INFO("pcb_rh", "Reading not allowed, sending object info only");
        return true;
    }

    if(object->attributes & (object_attr_mapped | object_attr_linked)) {
        if(!default_forward_mapped(req, peer, object, NULL, depth, object_firstDestination(object))) {
            SAH_TRACEZ_NOTICE("pcb_rh", "Forwarding mapped failed");
        }
    } else {
        if(object_hasDestinations(object)) {
            SAH_TRACEZ_INFO("pcb_rh", "get object request %p needs forwarding ?", req);
            if(((attributes & (request_getObject_parameters | request_getObject_functions)) == 0) && llist_isEmpty(parameters)) {
                /* no parameters needed, just return the object info */
                return default_reply_object(peer, object, depth, attributes, req, NULL);
            }
            default_forward_request(peer, object, depth, attributes, req);
            SAH_TRACEZ_INFO("pcb_rh", "Forwarding done, continue with child objects");
            bool includeTemplate = (object_isTemplate(object) && (attributes & request_getObject_template_info)) || !object_isTemplate(object);
            if(((attributes & request_getObject_children) && includeTemplate) || (depth > 0)) {
                if(!default_getObjectChildren(peer, object, depth, attributes, req)) {
                    return false;
                }
            }

            if(attributes & request_getObject_instances || ((depth > 0) && !(attributes & request_getObject_template_info))) {
                if(!default_getObjectInstances(peer, object, depth, attributes, req)) {
                    return false;
                }
            }
        } else {
            if((attributes & request_notify_only) == 0) {
                return default_reply_object(peer, object, depth, attributes, req, parameters);
            }
        }
    }

    return true;
}

bool default_reply_object(peer_info_t* peer, object_t* object, uint32_t depth, uint32_t attributes, request_t* req, llist_t* parameters) {
    if((attributes & request_notify_only) == 0) {
        /* update the object */
        local_object_update(object, parameters);
    }
    bool first = true;
    /* Should create a list of functions, now it is added the the pcb_writeObjectBegin */
    uint32_t attr = request_attributes(req);
    request_setAttributes(req, attributes);
    SAH_TRACEZ_INFO("pcb_rh", "Reply object %s - reply attributes are 0x%X - req %p", object_name(object, 0), attributes, req);
    pcb_writeObjectBegin(peer, req, object);
    request_setAttributes(req, attr);
    bool includeTemplate = (object_isTemplate(object) && (attributes & request_getObject_template_info)) || !object_isTemplate(object);
    object_t* child = NULL;
    SAH_TRACEZ_INFO("pcb_rh", "Need to add parameters? %s", (((attributes & request_getObject_parameters) || !llist_isEmpty(parameters)) && includeTemplate) ? "Yes" : "No");
    if(((attributes & request_getObject_parameters) || !llist_isEmpty(parameters)) && includeTemplate) {
        default_replyParameters(peer, object, req);
    }

    if(((attributes & request_getObject_children) && includeTemplate) || (includeTemplate && (depth > 0))) {
        SAH_TRACEZ_INFO("pcb_rh", "Add children");
        first = true;
        pcb_writeObjectListBegin(peer, req, object_list_children);
        object_for_each_child(child, object) {
            if(first) {
                first = false;
            } else {
                pcb_writeObjectListNext(peer, req);
            }
            if(depth == 0) {
                attributes &= ~(request_getObject_template_info | request_getObject_functions | request_getObject_parameters | request_getObject_children | request_getObject_instances);
                SAH_TRACEZ_INFO("pcb_rh", "Depth = 0, modified attributes = 0x%X", attributes);
                request_setAttributes(req, attributes);
                if(!default_getObject_internal(peer, child, 0, attributes, req, NULL)) {
                    return false;
                }
                request_setAttributes(req, attr);
            } else {
                if(!default_getObject_internal(peer, child, (depth == UINT32_MAX) ? depth : (depth - 1), attributes, req, parameters)) {
                    return false;
                }
            }
        }
        pcb_writeObjectListEnd(peer, req);
    }

    if((attributes & request_getObject_instances) || ((depth > 0) && !(attributes & request_getObject_template_info))) {
        SAH_TRACEZ_INFO("pcb_rh", "Add instances");
        first = true;
        pcb_writeObjectListBegin(peer, req, object_list_instances);
        object_for_each_instance(child, object) {
            if(first) {
                first = false;
            } else {
                pcb_writeObjectListNext(peer, req);
            }
            if(depth == 0) {
                attributes &= ~(request_getObject_template_info | request_getObject_functions | request_getObject_parameters | request_getObject_children | request_getObject_instances);
                request_setAttributes(req, attributes);
                if(!default_getObject_internal(peer, child, 0, attributes, req, NULL)) {
                    return false;
                }
                request_setAttributes(req, attr);
            } else {
                if(!default_getObject_internal(peer, child, (depth == UINT32_MAX) ? depth : (depth - 1), attributes, req, parameters)) {
                    return false;
                }
            }
        }
        pcb_writeObjectListEnd(peer, req);
    }
    pcb_writeObjectEnd(peer, req);
    if(!peer_flush(peer)) {
        SAH_TRACEZ_ERROR("pcb_rh", "flushing the data failed");
        peer_destroy(peer);
        return false;
    }

    return true;
}

bool default_getObject(peer_info_t* peer, object_t* object, uint32_t depth, uint32_t attributes, request_t* req) {
    return default_getObject_internal(peer, object, depth, attributes, req, request_parameterList(req));
}

static inline void debug_request(const char* type, request_t* req) {
    (void) type;
    (void) req;
    SAH_TRACEZ_INFO("pcb_rh", "%s request %p", type, req);
    SAH_TRACEZ_INFO("pcb_rh", "    Request path   = [%s]", request_path(req));
    SAH_TRACEZ_INFO("pcb_rh", "    Request attrib = [0x%X]", request_attributes(req));
    SAH_TRACEZ_INFO("pcb_rh", "    User ID        = [%d]", request_userID(req));
    SAH_TRACEZ_INFO("pcb_rh", "    Source PID     = [%d]", request_getPid(req));
    switch(request_type(req)) {
    case request_type_get_object:
        SAH_TRACEZ_INFO("pcb_rh", "    depth          = [%d]", request_depth(req));
        break;
    case request_type_set_object:
        SAH_TRACEZ_INFO("pcb_rh", "    Nr. Of Params  = [%d]", llist_size(request_parameterList(req)));
        break;
    case request_type_create_instance:
        SAH_TRACEZ_INFO("pcb_rh", "    Index          = [%d]", request_instanceIndex(req));
        SAH_TRACEZ_INFO("pcb_rh", "    Key            = [%s]", request_instanceKey(req));
        break;
    case request_type_find_objects:
        SAH_TRACEZ_INFO("pcb_rh", "    depth          = [%d]", request_depth(req));
        SAH_TRACEZ_INFO("pcb_rh", "    Pattern        = [%s]", request_pattern(req));
        break;
    case request_type_exec_function:
        SAH_TRACEZ_INFO("pcb_rh", "    Function name  = [%s]", request_functionName(req));
        SAH_TRACEZ_INFO("pcb_rh", "    Nr. Of Args    = [%d]", llist_size(request_parameterList(req)));
        break;
    default:
        break;
    }
}

static inline object_t* default_fetch_request_object(object_t* root, peer_info_t* peer, request_t* req, uint16_t acl_flags) {
    const char* objectPath = request_path(req);
    uint32_t attributes = request_attributes(req);
    uint32_t uid = request_userID(req);

    bool exact = false;

    pcb_error = 0;

    /* get the object */
    object_t* object = object_getObject(root, objectPath, ((attributes & (path_attr_key_notation | path_attr_slash_seperator | path_attr_alias)) | path_attr_partial_match), &exact);
    if(!object || (object_state(object) == object_state_deleted)) {
        SAH_TRACEZ_NOTICE("pcb_rh", "Object not found");
        if(!(attributes & (request_wait | request_notify_all))) {
            pcb_replyBegin(peer, req, pcb_error_not_found);
            pcb_writeError(peer, req, pcb_error_not_found, error_string(pcb_error_not_found), objectPath);
            pcb_replyEnd(peer, req);
        }
        pcb_error = pcb_error_not_found;
        object = NULL;
        goto exit;
    }

    if(!exact && !((object_attributes(object) & object_attr_linked))) {
        SAH_TRACEZ_NOTICE("pcb_rh", "Object not found");
        if(!(attributes & (request_wait | request_notify_all))) {
            pcb_replyBegin(peer, req, pcb_error_not_found);
            pcb_writeError(peer, req, pcb_error_not_found, error_string(pcb_error_not_found), objectPath);
            pcb_replyEnd(peer, req);
        }
        pcb_error = pcb_error_not_found;
        object = NULL;
        goto exit;
    }

    if(acl_flags && (uid != 0)) {
        bool retval = false;
        bool writeErrorInfo = true;
        if(acl_flags & acl_read) {
            // if userid does not have read access, do not write objectPath
            if(!object_canRead(object, uid)) {
                writeErrorInfo = false;
                goto writereply;
            }
        }

        llist_t ACL;
        llist_initialize(&ACL);
        object_acl(object, &ACL);
        retval = aclCheck(&ACL, uid, acl_flags);
        aclDestroy(&ACL);

writereply:
        if(!retval) {
            object = NULL;
            pcb_replyBegin(peer, req, EACCES);
            pcb_writeError(peer, req, EACCES, error_string(EACCES), writeErrorInfo ? objectPath : "");
            pcb_replyEnd(peer, req);
            pcb_error = EACCES;
        }
    }

exit:
    return object;
}

bool default_getObjectHandler(peer_info_t* peer, datamodel_t* datamodel, request_t* req) {
    object_t* root = datamodel_root(datamodel);
    uint32_t depth = request_depth(req);
    uint32_t attributes = request_attributes(req);
    //const char *objectPath = request_path(req);

    bool retval = true;

    debug_request("Get object", req);

    /* get the object */
    object_t* object = default_fetch_request_object(root, peer, req, acl_read);
    if(!object) {
        if((attributes & (request_wait | request_notify_all)) && (pcb_error == pcb_error_not_found)) {
            SAH_TRACEZ_NOTICE("pcb_rh", "Object is not available, but notification or wait flags are set: store request");
            object_addNotifyRequest(root, req);
            return true;
        }
        return false;
    }

    if(!object_canExecute(object, request_userID(req))) {
        SAH_TRACEZ_NOTICE("pcb_rh", "No execution rights on object, remove function flags");
        attributes &= ~request_getObject_functions;
        request_setAttributes(req, attributes);
    }

    pcb_replyBegin(peer, req, 0);
    retval = default_getObject(peer, object, depth, attributes, req);
    if(retval) {
        if(request_state(req) != request_state_forwarded) {
            pcb_replyEnd(peer, req);
            request_setDone(req);
        }

        if((attributes & request_notify_all) != 0) {
            object_addNotifyRequest(object, req);
        }
        SAH_TRACEZ_NOTICE("pcb_rh", "get object %s OK", request_path(req));
    } else {
        if(request_state(req) != request_state_forwarded) {
            pcb_replyEnd(peer, req);
            request_setDone(req);
            SAH_TRACEZ_NOTICE("pcb_rh", "get object %s FAILED", request_path(req));
        }
    }

    return retval;
}

bool default_setObjectHandler(peer_info_t* peer, datamodel_t* datamodel, request_t* req) {
    object_t* root = datamodel_root(datamodel);

    const char* objectPath = request_path(req);
    uint32_t attributes = request_attributes(req);

    debug_request("Set object", req);

    /* get the object */
    object_t* object = default_fetch_request_object(root, peer, req, 0);
    if(!object) {
        if((attributes & (request_wait | request_notify_all)) && (pcb_error == pcb_error_not_found)) {
            SAH_TRACEZ_NOTICE("pcb_rh", "Object is not available, but notification or wait flags are set: store request");
            object_addNotifyRequest(root, req);
            return true;
        }
        return false;
    }

    if(object_isReadOnly(object)) {
        SAH_TRACEZ_NOTICE("pcb_rh", "Object is read-only");
        pcb_replyBegin(peer, req, pcb_error_read_only);
        pcb_writeError(peer, req, pcb_error_read_only, error_string(pcb_error_read_only), objectPath);
        pcb_replyEnd(peer, req);
        return false;
    }

    if(llist_isEmpty(request_parameterList(req))) {
        SAH_TRACEZ_NOTICE("pcb_rh", "No parameters given");
        pcb_replyBegin(peer, req, pcb_error_list_empty);
        pcb_writeError(peer, req, pcb_error_list_empty, error_string(pcb_error_list_empty), objectPath);
        pcb_replyEnd(peer, req);
        return false;
    }

    if(object->attributes & (object_attr_mapped | object_attr_linked)) {
        if(!default_forward_mapped(req, peer, object, NULL, 0, object_firstDestination(object))) {
            pcb_replyBegin(peer, req, pcb_error_list_empty);
            default_sendErrorList(peer, req);
            pcb_replyEnd(peer, req);
            return false;
        }
        pcb_replyBegin(peer, req, 0);
        SAH_TRACEZ_NOTICE("pcb_rh", "Set object %s is forwarded", objectPath);
        /* Do not mark the request as done here, wait for reply from forwarded request(s) */
        return true;
    }

    if(!default_setValues(peer, object, req)) {
        SAH_TRACEZ_WARNING("pcb_rh", "Failed to set values");
        pcb_replyBegin(peer, req, pcb_error_invalid_request);
        default_sendErrorList(peer, req);
        object_rollback(object);
        pcb_replyEnd(peer, req);
        return false;
    }

    if(request_state(req) == request_state_forwarded) {
        pcb_replyBegin(peer, req, 0);
        SAH_TRACEZ_NOTICE("pcb_rh", "Set object %s is forwarded", objectPath);
        /* Do not mark the request as done here, wait for reply from forwarded request(s) */
        return true;
    }

    if(request_attributes(req) & request_setObject_validate_only) {
        if(!object_validate(object)) {
            pcb_replyBegin(peer, req, pcb_error_invalid_value);
            pcb_writeError(peer, req, pcb_error_invalid_value, error_string(pcb_error_invalid_value), objectPath);
            pcb_replyEnd(peer, req);
            SAH_TRACEZ_WARNING("pcb_rh", "Validation of new values is done and NOT OK");
        } else {
            pcb_replyBegin(peer, req, 0);
            pcb_replyEnd(peer, req);
            SAH_TRACEZ_NOTICE("pcb_rh", "Validation of new values is done and OK");
        }
        object_rollback(object);
        request_setDone(req);
        return true;
    }

    if(!object_commit(object)) {
        object_rollback(object);
        pcb_replyBegin(peer, req, pcb_error_invalid_value);
        pcb_writeError(peer, req, pcb_error_invalid_value, error_string(pcb_error_invalid_value), objectPath);
        pcb_replyEnd(peer, req);
        SAH_TRACEZ_ERROR("pcb_rh", "Failed to set value(s) for object %s", objectPath);
        return false;
    }

    request_parameterListClear(req);
    pcb_replyBegin(peer, req, 0);
    default_reply_object(peer, object, 0, attributes | request_getObject_parameters, req, NULL);
    pcb_replyEnd(peer, req);
    request_setDone(req);

    SAH_TRACEZ_NOTICE("pcb_rh", "Set object %s OK", objectPath);

    return true;
}

bool default_createInstanceHandler(peer_info_t* peer, datamodel_t* datamodel, request_t* req) {
    object_t* root = datamodel_root(datamodel);

    const char* objectPath = request_path(req);
    uint32_t attributes = request_attributes(req);
    uint32_t index = request_instanceIndex(req);
    const char* key = request_instanceKey(req);

    debug_request("Create instance", req);

    /* get the object */
    object_t* object = default_fetch_request_object(root, peer, req, acl_write);
    if(!object) {
        if((attributes & (request_wait | request_notify_all)) && (pcb_error == pcb_error_not_found)) {
            SAH_TRACEZ_NOTICE("pcb_rh", "Object is not available, but notification or wait flags are set: store request");
            object_addNotifyRequest(root, req);
            return true;
        }
        return false;
    }

    if(object_isReadOnly(object)) {
        pcb_replyBegin(peer, req, pcb_error_read_only);
        pcb_writeError(peer, req, pcb_error_read_only, error_string(pcb_error_read_only), objectPath);
        pcb_replyEnd(peer, req);
        return false;
    }

    if(object->attributes & (object_attr_mapped | object_attr_linked)) {
        pcb_replyBegin(peer, req, pcb_error_list_empty);
        if(!default_forward_mapped(req, peer, object, NULL, 0, object_firstDestination(object))) {
            default_sendErrorList(peer, req);
            pcb_replyEnd(peer, req);
            return false;
        }
        pcb_replyBegin(peer, req, 0);
        SAH_TRACEZ_NOTICE("pcb_rh", "Set object %s is forwarded", objectPath);
        /* Do not mark the request as done here, wait for reply from forwarded request(s) */
        return true;
    }

    if(!object_isTemplate(object)) {
        pcb_replyBegin(peer, req, pcb_error_not_template);
        pcb_writeError(peer, req, pcb_error_not_template, error_string(pcb_error_not_template), objectPath);
        pcb_replyEnd(peer, req);
        return false;
    }

    object_t* instance = object_createInstance(object, index, key);
    if(!instance) {
        SAH_TRACEZ_NOTICE("pcb_rh", "Return error 0x%X - %s", pcb_error, error_string(pcb_error));
        uint32_t error = pcb_error;
        pcb_replyBegin(peer, req, error);
        pcb_writeError(peer, req, error, error_string(error), objectPath);
        pcb_replyEnd(peer, req);
        return false;
    }

    if((attributes & request_createObject_persistent) && (attributes & request_createObject_not_persistent)) {
        SAH_TRACEZ_WARNING("pcb_rh", "Create instance request: attribute conflict, persistent and not persistent flags set, ignoring");
    } else {
        if(attributes & request_createObject_persistent) {
            SAH_TRACEZ_INFO("pcb_rh", "Created instance is persistent");
            object_setPersistent(instance, true);
        } else if(attributes & request_createObject_not_persistent) {
            SAH_TRACEZ_INFO("pcb_rh", "Created instance is not persistent");
            object_setPersistent(instance, false);
        } else {
            if(object_attributes(object) & object_attr_persistent) {
                SAH_TRACEZ_INFO("pcb_rh", "Created instance is persistent");
                object_setPersistent(instance, true);
            } else {
                SAH_TRACEZ_INFO("pcb_rh", "Created instance is not persistent");
                object_setPersistent(instance, false);
            }
        }
    }

    if(!default_setValues(peer, instance, req)) {
        SAH_TRACEZ_WARNING("pcb_rh", "Failed to set values");
        pcb_replyBegin(peer, req, pcb_error_invalid_request);
        default_sendErrorList(peer, req);
        object_rollback(object);
        pcb_replyEnd(peer, req);
        return false;
    }

    if(request_attributes(req) & request_setObject_validate_only) {
        if(!object_validate(object)) {
            pcb_replyBegin(peer, req, pcb_error_invalid_value);
            pcb_writeError(peer, req, pcb_error_invalid_value, error_string(pcb_error_invalid_value), objectPath);
            pcb_replyEnd(peer, req);
            SAH_TRACEZ_WARNING("pcb_rh", "Validation of new values is done and NOT OK");
        } else {
            pcb_replyBegin(peer, req, 0);
            pcb_replyEnd(peer, req);
            SAH_TRACEZ_NOTICE("pcb_rh", "Validation of new values is done and OK");
        }
        object_rollback(object);
        request_setDone(req);
        return true;
    }

    if(!object_commit(object)) {
        object_rollback(object);
        pcb_replyBegin(peer, req, pcb_error);
        pcb_writeError(peer, req, pcb_error, error_string(pcb_error), objectPath);
        pcb_replyEnd(peer, req);
        SAH_TRACEZ_ERROR("pcb_rh", "Failed to set value(s) for object %s", objectPath);
        return false;
    }

    request_parameterListClear(req);
    pcb_replyBegin(peer, req, 0);
    default_reply_object(peer, instance, 0, attributes | request_getObject_parameters, req, NULL);
    pcb_replyEnd(peer, req);
    request_setDone(req);

    return true;
}

bool default_deleteInstanceHandler(peer_info_t* peer, datamodel_t* datamodel, request_t* req) {
    object_t* root = datamodel_root(datamodel);

    const char* objectPath = request_path(req);

    debug_request("Delete instance", req);

    /* get the object */
    object_t* object = default_fetch_request_object(root, peer, req, acl_write);
    if(!object) {
        return false;
    }

    if(object_isReadOnly(object_instanceOf(object))) {
        pcb_replyBegin(peer, req, pcb_error_read_only);
        pcb_writeError(peer, req, pcb_error_read_only, error_string(pcb_error_read_only), objectPath);
        pcb_replyEnd(peer, req);
        return false;
    }

    if(object->attributes & (object_attr_mapped | object_attr_linked)) {
        pcb_replyBegin(peer, req, pcb_error_list_empty);
        if(!default_forward_mapped(req, peer, object, NULL, 0, object_firstDestination(object))) {
            default_sendErrorList(peer, req);
            pcb_replyEnd(peer, req);
            return false;
        }
        pcb_replyBegin(peer, req, 0);
        SAH_TRACEZ_NOTICE("pcb_rh", "Set object %s is forwarded", objectPath);
        /* Do not mark the request as done here, wait for reply from forwarded request(s) */
        return true;
    }

    if(!object_isInstance(object)) {
        pcb_replyBegin(peer, req, pcb_error_not_instance);
        pcb_writeError(peer, req, pcb_error_not_instance, error_string(pcb_error_not_instance), objectPath);
        pcb_replyEnd(peer, req);
        return false;
    }

    if(!object_delete(object)) {
        pcb_replyBegin(peer, req, pcb_error);
        pcb_writeError(peer, req, pcb_error_wrong_state, error_string(pcb_error_wrong_state), objectPath);
        pcb_replyEnd(peer, req);
        return false;
    }

    if(!object_commit(object)) {
        pcb_replyBegin(peer, req, pcb_error);
        pcb_writeError(peer, req, pcb_error_wrong_state, error_string(pcb_error_wrong_state), objectPath);
        object_rollback(object);
        pcb_replyEnd(peer, req);
        return false;
    }

    /*
       Not needed to send a notification here, it is done by the commit function !!!!!
       notification_t *notifyDelete = notification_create(notify_object_deleted);
       notification_setObjectPath(notifyDelete,objectPath);
       pcb_writeNotification(peer,req,notifyDelete);
       notification_destroy(notifyDelete);
     */

    pcb_replyBegin(peer, req, 0);
    pcb_replyEnd(peer, req);
    request_setDone(req);

    return true;
}

bool default_findObjectsHandler(peer_info_t* peer, datamodel_t* datamodel, request_t* req) {
    object_t* root = datamodel_root(datamodel);
    const char* objectPath = request_path(req);
    const char* pattern = request_pattern(req);
    uint32_t attributes = request_attributes(req);
    uint32_t depth = request_depth(req);

    debug_request("Find objects", req);

    /* get the object */
    object_t* object = default_fetch_request_object(root, peer, req, acl_read);
    SAH_TRACEZ_INFO("pcb_rh", "Found object %p - %s/%s", object, object_name(object, 0), object_name(object, path_attr_key_notation));
    if(!object) {
        return false;
    }

    if(object->attributes & (object_attr_mapped | object_attr_linked)) {
        if(!default_forward_mapped(req, peer, object, NULL, depth, object_firstDestination(object))) {
            pcb_replyBegin(peer, req, pcb_error_list_empty);
            default_sendErrorList(peer, req);
            pcb_replyEnd(peer, req);
            return false;
        }
        pcb_replyBegin(peer, req, 0);
        SAH_TRACEZ_NOTICE("pcb_rh", "Find objects %s is forwarded", objectPath);
        /* Do not mark the request as done here, wait for reply from forwarded request(s) */
        return true;
    }

    uint32_t findAttr = attributes & (path_attr_key_notation | path_attr_slash_seperator);

    char* modified_pattern = NULL;
    int32_t s = asprintf(&modified_pattern, "%s", pattern);
    if(s == -1) {
        pcb_replyBegin(peer, req, pcb_error_no_memory);
        pcb_writeError(peer, req, pcb_error_no_memory, error_string(pcb_error_no_memory), objectPath);
        pcb_replyEnd(peer, req);
        return false;
    }
    char* parameter = NULL;
    char* value = NULL;
    if(attributes & (request_find_include_parameters | request_find_include_values)) {
        char* pos = strchr(modified_pattern, '=');
        if(pos) {
            value = pos + 1;
            *pos = 0;
            if(attributes & path_attr_slash_seperator) {
                pos = strrchr(modified_pattern, '/');
            } else {
                pos = strrchr(modified_pattern, '.');
            }
            if(pos) {
                parameter = pos + 1;
                *pos = 0;
            } else {
                attributes &= ~(request_find_include_parameters | request_find_include_values);
            }
        } else {
            attributes &= ~(request_find_include_parameters | request_find_include_values);
        }
    }
    SAH_TRACEZ_INFO("pcb_rh", "parameter = %s", parameter);
    SAH_TRACEZ_INFO("pcb_rh", "value = %s", value);

    object_list_t* objects = object_findObjects(object, modified_pattern, depth,
                                                findAttr | ((attributes & request_find_instance_wildcards_only) ? search_attr_instance_wildcards_only : 0));
    if(!objects) {
        SAH_TRACEZ_NOTICE("pcb_rh", "Error upon matching objects: %s", error_string(pcb_error));

        pcb_replyBegin(peer, req, pcb_error);
        pcb_writeError(peer, req, pcb_error, error_string(pcb_error), objectPath);
        pcb_replyEnd(peer, req);

        free(modified_pattern);
        return false;
    }

    pcb_replyBegin(peer, req, 0);
    pcb_writeObjectListBegin(peer, req, object_list);
    object_t* foundObject = NULL;
    string_t* paramValue = NULL;
    bool first = true;
    SAH_TRACEZ_INFO("pcb_rh", "Matching objects found = %s", llist_isEmpty(objects) ? "false" : "true");
    llist_iterator_t* it = NULL;
    llist_for_each(it, objects) {
        foundObject = llist_item_data(it, object_t, it);
        SAH_TRACEZ_INFO("pcb_rh", "Object path matches");
        if(attributes & (request_find_include_parameters | request_find_include_values)) {
            SAH_TRACE_INFO("Checking parameter %s", parameter);
            paramValue = object_parameterStringValue(foundObject, parameter);
            if(!paramValue) {
                SAH_TRACE_INFO("Parameter not found");
                continue;
            }
            SAH_TRACE_INFO("Value = %s", string_buffer(paramValue));
#if defined(FNM_EXTMATCH)
            if(fnmatch(value, string_buffer(paramValue), FNM_CASEFOLD | FNM_EXTMATCH) != 0) {
#else
            if(fnmatch(value, string_buffer(paramValue), FNM_CASEFOLD) != 0) {
#endif
                string_cleanup(paramValue);
                free(paramValue);
                continue;
            }
            string_cleanup(paramValue);
            free(paramValue);
        }
        if(!object_canRead(foundObject, request_userID(req)) &&
           !object_canRead(object_parent(foundObject), request_userID(req))) {
            continue;
        }
        if(!first) {
            pcb_writeObjectListNext(peer, req);
        }
        if(!default_getObject(peer, foundObject, 0, attributes & ~(request_getObject_children | request_getObject_instances), req)) {
            SAH_TRACEZ_NOTICE("pcb_rh", "Failed to get object");
        }
        first = false;
    }
    pcb_writeObjectListEnd(peer, req);
    free(modified_pattern);

    llist_cleanup(objects);
    free(objects);
    if(request_state(req) != request_state_forwarded) {
        pcb_replyEnd(peer, req);
        request_setDone(req);
    }

    return true;
}

static void default_execFunction_ReplyErrorList(peer_info_t* peer, request_t* req) {
    variant_t var;
    variant_initialize(&var, variant_type_unknown);

    pcb_writeFunctionReturnBegin(peer, req);
    pcb_writeReturnValue(peer, req, &var);
    default_sendErrorList(peer, req);
    pcb_writeFunctionReturnEnd(peer, req);

    variant_cleanup(&var);
}

static void default_execFunction_ReplyError(peer_info_t* peer, request_t* req, uint32_t error, const char* info) {
    reply_addError(request_reply(req), error, error_string(error), info);
    pcb_replyBegin(peer, req, 0);
    default_execFunction_ReplyErrorList(peer, req);
    pcb_replyEnd(peer, req);
}

static bool default_execFunction_verifyArgument(request_t* req, function_argument_t* argdef, argument_value_t* arg) {
    if(!arg) {
        reply_addError(request_reply(req), pcb_error_function_argument_missing, "Missing mandatory argument", argument_name(argdef));
        return false;
    }
    return true;
}

static bool default_execFunction_verifyArguments(function_t* function, request_t* req) {
    argument_value_list_t* args = request_parameterList(req);
    uint32_t attributes = request_attributes(req);
    bool retval = true;

    /* loop over all argument definitions */
    function_argument_t* argdef = function_firstArgument(function);
    argument_value_t* arg = NULL;
    if(!(attributes & request_function_args_by_name)) {
        arg = argument_valueFirstArgument(args);
    }
    while(argdef) {
        /* if the argument is mandatory, check if it is in the argument list given */
        if(argument_isMandatory(argdef)) {
            if(attributes & request_function_args_by_name) {
                /* an argument value must be provided in the list with the same name */
                arg = argument_valueByName(args, argument_name(argdef));
                retval &= default_execFunction_verifyArgument(req, argdef, arg);
            } else {
                /* at least the argument value list must contain as much items as the index of this definition */
                retval &= default_execFunction_verifyArgument(req, argdef, arg);
            }
        }
        if(!(attributes & request_function_args_by_name)) {
            arg = argument_valueNextArgument(arg);
        }
        argdef = function_nextArgument(argdef);
    }
    return retval;
}

static bool default_executeFunction(peer_info_t* peer, request_t* req, object_t* object, function_t* func) {
    argument_value_list_t* args = request_parameterList(req);
    function_call_t* fcall = NULL;
    variant_t retval;
    variant_initialize(&retval, variant_type_unknown);
    function_exec_state_t state = function_exec_done;

    /* is the function implemented */
    if(!function_isImplemented(func)) {
        SAH_TRACEZ_NOTICE("pcb_rh", "Function not implemented");
        default_execFunction_ReplyError(peer, req, pcb_error_function_not_implemented, request_functionName(req));
        goto exit_error;
    }

    /* verify all mandatory arguments are present */
    if(!default_execFunction_verifyArguments(func, req)) {
        SAH_TRACEZ_ERROR("pcb_rh", "Function call missing mandatory arguments");
        pcb_replyBegin(peer, req, pcb_error_function_argument_missing);
        default_execFunction_ReplyErrorList(peer, req);
        pcb_replyEnd(peer, req);
        goto exit_error;
    }

    /* create the function call context */
    fcall = fcall_create(peer, req, func, object);
    if(!fcall) {
        default_execFunction_ReplyError(peer, req, pcb_error_no_memory, request_functionName(req));
        goto exit_error;
    }


    /* call the function */
    if(object_isTemplate(object) && (function_attributes(func) & function_attr_template_only)) {
        /* template only function */
        SAH_TRACEZ_INFO("pcb_rh", "Execute function");
        state = fcall_execute(fcall, args, &retval);
    } else if(object_isInstance(object) && !(function_attributes(func) & function_attr_template_only)) {
        /* instance function */
        SAH_TRACEZ_INFO("pcb_rh", "Execute function");
        state = fcall_execute(fcall, args, &retval);
    } else if(!object_isTemplate(object) && !object_isInstance(object)) {
        /* singelton object function */
        SAH_TRACEZ_INFO("pcb_rh", "Execute function");
        state = fcall_execute(fcall, args, &retval);
    } else {
        default_execFunction_ReplyError(peer, req, pcb_error_function_object_mismatch, request_functionName(req));
        goto exit_error_free_fcall;
    }

    if((state != function_exec_done) && (state != function_exec_error)) {
        if((fcall_state(fcall) == function_exec_cancel) ||
           (state == function_exec_cancel)) {
            SAH_TRACEZ_INFO("pcb_rh", "Function execution canceled");
            if(fcall_request(fcall)) {
                default_execFunction_ReplyError(peer, req, pcb_error_canceled, request_functionName(req));
            }
            goto exit_error_free_fcall;
        }
    }
    switch(state) {
    default:
    case function_exec_error:
        SAH_TRACEZ_INFO("pcb_rh", "Function execution error");
        if(!((function_attributes(func) & function_attr_message))) {
            /* add an error to the reply */
            reply_addError(request_reply(req), pcb_error_function_exec_failed, error_string(pcb_error_function_exec_failed), request_functionName(req));
        }
        FALLTHROUGH;
    /* same code to generate a reply when successfull or failure */
    case function_exec_done:
        SAH_TRACEZ_INFO("pcb_rh", "Function execution done");
        if(!((function_attributes(func) & function_attr_message))) {
            /* create reply */
            pcb_replyBegin(peer, req, 0);
            pcb_writeFunctionReturnBegin(peer, req);
            pcb_writeReturnValue(peer, req, &retval);
            if(!llist_isEmpty(args)) {
                pcb_writeReturnArgumentsStart(peer, req);
                pcb_writeReturnArguments(peer, req, args);
            }
            default_sendErrorList(peer, req);
            pcb_writeFunctionReturnEnd(peer, req);
            pcb_replyEnd(peer, req);
        } else {
            pcb_replyBegin(peer, req, 0);
            pcb_replyEnd(peer, req);
        }
        if(request_data(req)) {
            function_call_t* f = (function_call_t*) request_data(req);
            fcall_destroy(f);
        }
        request_setDone(req);
        break;
    case function_exec_executing:
        SAH_TRACEZ_INFO("pcb_rh", "Function execution busy");
        request_setBusy(req);
        break;
    }
    variant_cleanup(&retval);
    return true;

exit_error_free_fcall:
    fcall_destroy(fcall);
    variant_cleanup(&retval);
exit_error:
    return false;
}

function_exec_state_t __getAclInfo(function_call_t* fcall, argument_value_list_t* args, variant_t* retval);
function_exec_state_t __get(function_call_t* fcall, argument_value_list_t* args, variant_t* retval);
function_exec_state_t __set(function_call_t* fcall, argument_value_list_t* args, variant_t* retval);
function_exec_state_t __modifyAcl(function_call_t* fcall, argument_value_list_t* args, variant_t* retval);

static bool default_function_executer(peer_info_t* peer, request_t* req, object_t* object, function_handler_t fn) {
    /* create the function call context */
    function_call_t* fcall = fcall_create(peer, req, NULL, object);
    argument_value_list_t* args = request_parameterList(req);
    variant_t retval;
    function_exec_state_t state = function_exec_done;

    if(!fcall) {
        default_execFunction_ReplyError(peer, req, pcb_error_no_memory, request_functionName(req));
        goto exit_error;
    }

    variant_initialize(&retval, variant_type_unknown);

    /* call the function */
    state = fn(fcall, args, &retval);

    switch(state) {
    default:
    case function_exec_error:
        SAH_TRACEZ_INFO("pcb_rh", "Function execution error");
        reply_addError(request_reply(req), pcb_error_function_exec_failed, error_string(pcb_error_function_exec_failed), request_functionName(req));
        FALLTHROUGH;
    case function_exec_done:
        /* create reply */
        pcb_replyBegin(peer, req, 0);
        pcb_writeFunctionReturnBegin(peer, req);
        pcb_writeReturnValue(peer, req, &retval);
        if(!llist_isEmpty(args)) {
            pcb_writeReturnArgumentsStart(peer, req);
            pcb_writeReturnArguments(peer, req, args);
        }
        default_sendErrorList(peer, req);
        pcb_writeFunctionReturnEnd(peer, req);
        pcb_replyEnd(peer, req);
        if(request_data(req)) {
            function_call_t* f = (function_call_t*) request_data(req);
            fcall_destroy(f);
        }
        request_setDone(req);
        break;
    case function_exec_executing:
        SAH_TRACEZ_INFO("pcb_rh", "Function execution busy");
        request_setBusy(req);
        break;
    }
    variant_cleanup(&retval);
    return true;

exit_error:
    return false;
}

bool default_executeFunctionHandler(peer_info_t* peer, datamodel_t* datamodel, request_t* req) {
    object_t* root = datamodel_root(datamodel);
    const char* objectPath = request_path(req);
    const char* functionName = request_functionName(req);
    uint32_t attributes = request_attributes(req);
    bool retval = false;
    function_t* func = NULL;

    debug_request("Execute function", req);

    /* get the object */
    object_t* object = default_fetch_request_object(root, peer, req, acl_execute);
    if(!object) {
        if((attributes & (request_wait | request_notify_all)) && (pcb_error == pcb_error_not_found)) {
            SAH_TRACEZ_NOTICE("pcb_rh", "Object is not available, but notification or wait flags are set: store request");
            object_addNotifyRequest(root, req);
            return true;
        }
        return false;
    }

    if(object->attributes & (object_attr_mapped | object_attr_linked)) {
        pcb_replyBegin(peer, req, 0);
        if(!default_forward_mapped(req, peer, object, NULL, 0, object_firstDestination(object))) {
            default_execFunction_ReplyErrorList(peer, req);
            pcb_replyEnd(peer, req);
            goto exit;
        } else {
            retval = true;
            goto exit;
        }
    }

    if(strcmp(functionName, "getAclInfo") == 0) {
        if(request_userID(req) != 0) {
            SAH_TRACEZ_ERROR("pcb_rh", "Access Denied");
            default_execFunction_ReplyError(peer, req, EACCES, objectPath);
            goto exit;
        }
        retval = default_function_executer(peer, req, object, __getAclInfo);
        goto exit;
    }

    if(strcmp(functionName, "modifyAcl") == 0) {
        if(request_userID(req) != 0) {
            SAH_TRACEZ_ERROR("pcb_rh", "Access Denied");
            default_execFunction_ReplyError(peer, req, EACCES, objectPath);
            goto exit;
        }
        retval = default_function_executer(peer, req, object, __modifyAcl);
        goto exit;
    }

    /* get the function */
    func = object_getFunction(object, functionName);
    if(!func || !function_isImplemented(func)) {
        if(strcmp(functionName, "get") == 0) {
            retval = default_function_executer(peer, req, object, __get);
            goto exit;
        } else if(strcmp(functionName, "set") == 0) {
            retval = default_function_executer(peer, req, object, __set);
            goto exit;
        } else if(!func) {
            SAH_TRACEZ_NOTICE("pcb_rh", "Function not found");
            default_execFunction_ReplyError(peer, req, pcb_error_not_found, functionName);
            goto exit;
        }
    }

    if(!function_canExecute(func, request_userID(req))) {
        SAH_TRACEZ_NOTICE("pcb_rh", "Access denied");
        default_execFunction_ReplyError(peer, req, EACCES, objectPath);
        goto exit;
    }

    /* is the function mapped? */
    if(func->destination) {
        pcb_replyBegin(peer, req, 0);
        if(!default_forward_mapped(req, peer, object, func, 0, func->destination)) {
            SAH_TRACEZ_ERROR("pcb_rh", "Failed to forward function exec request");
            default_execFunction_ReplyErrorList(peer, req);
            pcb_replyEnd(peer, req);
            goto exit;
        } else {
            retval = true;
            goto exit;
        }
    }

    retval = default_executeFunction(peer, req, object, func);

exit:
    return retval;
}

bool default_closeRequestHandler(peer_info_t* peer, request_t* req) {
    /* search the stored request */
    SAH_TRACEZ_INFO("pcb_rh", "Close request");
    request_t* stored_request = pcb_findNotifyRequest(peer, request_closeRequestId(req));
    if(stored_request) {
        SAH_TRACEZ_INFO("pcb_rh", "Found request, request state = %d", request_state(stored_request));
        pcb_replyBegin(peer, req, 0);
        if((request_state(stored_request) != request_state_destroyed) &&
           (request_state(stored_request) != request_state_destroy) &&
           (request_state(stored_request) != request_state_canceled)) {
            SAH_TRACEZ_INFO("pcb_rh", "Destroying request");
            stored_request->state = request_state_destroy;
            request_destroy(stored_request);
        }
    } else {
        SAH_TRACEZ_NOTICE("pcb_rh", "Request with id = %d not found", request_closeRequestId(req));
        pcb_replyBegin(peer, req, pcb_error);
        pcb_writeError(peer, req, pcb_error, error_string(pcb_error), "");
        pcb_replyEnd(peer, req);
        return false;
    }

    pcb_replyEnd(peer, req);
    request_setDone(req);
    return true;
}

bool default_openSessionRequestHandler(peer_info_t* peer, request_t* req) {
    const char* username = request_username(req);
    pcb_peer_data_t* pcb_data = (pcb_peer_data_t*) peer_getUserData(peer);

#ifdef CONFIG_PCB_ACL_USERMNGT
    const usermngt_user_t* pd = usermngt_userFindByName(username);
    if((pd == NULL) || !usermngt_userEnable(pd)) {
        SAH_TRACEZ_ERROR("pcb_rh", "User not found or user disabled");
        pcb_data->UID = UINT32_MAX;
        pcb_replyBegin(peer, req, EACCES);
        pcb_writeError(peer, req, EACCES, error_string(EACCES), "");
        pcb_replyEnd(peer, req);
        return false;
    }
    pcb_data->UID = usermngt_userID(pd);
#else
    struct passwd* pd;
    if((pd = getpwnam(username)) == NULL) {
        SAH_TRACEZ_ERROR("pcb_rh", "User not found");
        pcb_data->UID = UINT32_MAX;
        pcb_replyBegin(peer, req, EACCES);
        pcb_writeError(peer, req, EACCES, error_string(EACCES), "");
        pcb_replyEnd(peer, req);
        return false;
    }
    pcb_data->UID = pd->pw_uid;
#endif

    pcb_replyBegin(peer, req, 0);
    pcb_replyEnd(peer, req);
    request_setDone(req);
    return true;
}
