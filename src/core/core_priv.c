/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <string.h>
#include <stdlib.h>

#include "core_priv.h"

#include <components.h>

#ifdef CONFIG_PCB_ACL_USERMNGT
#include <usermngt/usermngt.h>
#endif

#include <debug/sahtrace.h>

#include <pcb/common/error.h>
#include <pcb/core/datamodel.h>

/**
   @file
   @brief
   Implementation of core common functions
 */

/**
   @brief
   Checks that a given name (object or parameter) is valid

   @details
   The following names are not valid:
    - Names containing one of the following characters './$?*[]+@!='
    - Full numeric names, numbers are allowed as long as there is at least one non numeric character in the name.
 */

bool isValidName(string_t* name) {
    if((strpbrk(string_buffer(name), "./$?*[]+@!=#") != NULL) ||
       string_isNumeric(name)) {
        pcb_error = pcb_error_invalid_name;
        SAH_TRACEZ_ERROR("pcb", "Name %s is not valid", string_buffer(name));
        return false;
    }

    return true;
}


bool isValidNameChar(const char* name) {
    string_t buffer;
    string_initialize(&buffer, 64);
    string_fromChar(&buffer, name);
    bool retval = isValidName(&buffer);
    string_cleanup(&buffer);

    return retval;
}

ACL_t* aclFind(llist_t* ACL, uint32_t id, uint16_t flags) {
    /* search an ACL for the user/group */
    llist_iterator_t* it = NULL;
    ACL_t* acl = NULL;
    llist_for_each(it, ACL) {
        acl = llist_item_data(it, ACL_t, it);
        if(acl->id == id) {
            if(((flags & acl_group_id) && (acl->acl_flags & acl_group_id)) ||
               (!(flags & acl_group_id) && !(acl->acl_flags & acl_group_id))) {
                break;
            }
        }
        acl = NULL;
    }

    return acl;
}

bool aclSet(llist_t* ACL, uint32_t id, uint16_t flags) {
    ACL_t* acl = aclFind(ACL, id, flags);

    if(!acl) {
        acl = (ACL_t*) calloc(1, sizeof(ACL_t));
        if(acl) {
            llist_iterator_initialize(&acl->it);
            acl->id = id;
        } else {
            pcb_error = pcb_error_no_memory;
            return false;
        }
        if(!llist_append(ACL, &acl->it)) {
            free(acl);
            return false;
        }
    }

    acl->acl_flags = flags;
    return true;
}

#ifdef CONFIG_PCB_IGNORE_ACL
bool aclCheck(llist_t* ACL, uint32_t uid, uint16_t flags) {
    (void) ACL;
    (void) uid;
    (void) flags;

    return true;
}
#else
#ifdef CONFIG_PCB_ACL_USERMNGT
bool aclCheck(llist_t* ACL, uint32_t uid, uint16_t flags) {
    if(!ACL) {
        SAH_TRACEZ_INFO("pcb_acl", "Access denied, no acl available");
        return false;
    }

    bool ret = false;
    bool userfound = false;
    bool groupfound = false;
    bool userallowed = false;
    bool groupallowed = false;
    bool worldallowed = false;

    SAH_TRACEZ_INFO("pcb_acl", "User id = %d", uid);
    llist_iterator_t* it = NULL;
    ACL_t* acl = NULL;
    const usermngt_user_t* user = usermngt_userFindByID(uid);

    llist_for_each(it, ACL) {
        acl = llist_item_data(it, ACL_t, it);
        if(acl->acl_flags & acl_group_id) {
            if(user && usermngt_userIsInGroup(user, acl->id)) {
                SAH_TRACEZ_INFO("pcb_acl", "User in group %d ACL flags = %X", acl->id, acl->acl_flags);
                groupfound = true;
                const usermngt_group_t* grp = usermngt_groupFindByID(acl->id);
                if(!grp) {
                    SAH_TRACEZ_WARNING("pcb_acl", "Group ID %d not found - ignore", acl->id);
                    continue;
                }
                if(((acl->acl_flags & flags) == flags) && usermngt_groupEnable(grp)) {
                    groupallowed = true;
                }
            }
        } else {
            if((acl->id == uid) && (acl->id != UINT32_MAX)) {
                SAH_TRACEZ_INFO("pcb_acl", "User id in ACL flags = %X", acl->acl_flags);
                userfound = true;
                // this is a hack for containers to work. If an ACL check is done from one
                // component in a container to another component in another container, then
                // the user can not be retrieved since the usermngt_userFindByID will return NULL
                // so if no user is found in user management, then the acl_flag rights will be returned
                if(((acl->acl_flags & flags) == flags) && ((user && usermngt_userEnable(user)) | !user)) {
                    userallowed = true;
                }
            }
        }
        if(acl->id == UINT32_MAX) {
            SAH_TRACEZ_INFO("pcb_acl", "Every one in ACL, flags = %x", acl->acl_flags);
            if((acl->acl_flags & flags) == flags) {
                worldallowed = true;
            }
        }
    }

    if(userfound) {
        ret = userallowed;
    } else if(groupfound) {
        ret = groupallowed;
    } else {
        ret = worldallowed;
    }

    if(!ret) {
        pcb_error = pcb_error_access_denied;
    }
    return ret;
}
#else
bool aclCheck(llist_t* ACL, uint32_t uid, uint16_t flags) {
    if(!ACL) {
        SAH_TRACEZ_INFO("pcb", "Access denied, no acl available");
        return false;
    }

    bool ret = false;
    bool userfound = false;
    bool userallowed = false;
    bool worldallowed = false;

    llist_iterator_t* it = NULL;
    ACL_t* acl = NULL;

    llist_for_each(it, ACL) {
        acl = llist_item_data(it, ACL_t, it);
        if(acl->acl_flags & acl_group_id) {
            /* should check the groups that the user is in */
        } else {
            if(acl->id == uid) {
                userfound = true;
                if(acl->acl_flags & flags) {
                    userallowed = true;
                }
            }
        }
        if((acl->id == UINT32_MAX) && (acl->acl_flags & flags)) {
            worldallowed = true;
        }
    }

    if(userfound) {
        ret = userallowed;
    } else {
        ret = worldallowed;
    }

    if(!ret) {
        pcb_error = pcb_error_access_denied;
    }
    return ret;
}
#endif
#endif

void aclDestroy(llist_t* ACL) {
    if(!ACL) {
        return;
    }

    llist_iterator_t* it = llist_takeFirst(ACL);
    ACL_t* acl = NULL;
    while(it) {
        acl = llist_item_data(it, ACL_t, it);
        free(acl);
        it = llist_takeFirst(ACL);
    }
}
