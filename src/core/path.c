/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#define _GNU_SOURCE
#include <stdlib.h>
#include <string.h>

#include <debug/sahtrace.h>
#include <pcb/common/error.h>
#include <pcb/utils/string.h>
#include <pcb/utils/string_list.h>
#include <pcb/core/object.h>

#include "path.h"

/**
   @file
   @brief
   Implementation of path functions
 */

static void exploded_path_descend(exploded_path_t* xpath, object_t* object, const char* name) {
    xpath->object = object;
    string_list_appendChar(&(xpath->exploded_path), name);
    xpath->path_size++;
}

/**
   @brief
   Initialize exploded path structure.

   @details
   Initialize exploded path structure.

   @param xpath Exploded path structure

   @return true if successful, false otherwise
 */
bool exploded_path_initialize(exploded_path_t* xpath) {
    if(!xpath) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    memset(xpath, 0, sizeof(*xpath));

    string_list_initialize(&(xpath->exploded_path));
    string_initialize(&(xpath->param_name), 0);
    string_initialize(&(xpath->param_value), 0);

    return true;
}

/**
   @brief
   Clean up exploded path structure.

   @details
   Clean up exploded path structure.

   @param xpath Exploded path structure

   @return void
 */
void exploded_path_cleanup(exploded_path_t* xpath) {
    if(!xpath) {
        return;
    }

    xpath->object = NULL;
    xpath->path_attributes = 0;
    string_list_cleanup(&(xpath->exploded_path));
    xpath->path_size = 0;
    xpath->parameter = NULL;
    string_cleanup(&(xpath->param_name));
    string_cleanup(&(xpath->param_value));
}

/**
   @brief
   Fill in exploded path structure from object.

   @details
   Fill in exploded path structure from object.
   The 'object' will be available in the structure.
   All parameter data will empty.

   @param xpath Exploded path structure
   @param object The object to take the path from
   @param pathAttributes The path attributes when parsing the object path

   @return true if successful, false otherwise
 */
bool exploded_path_fromObject(exploded_path_t* xpath, object_t* object, const uint32_t pathAttributes) {
    const char* sep = NULL;
    string_t path;

    if(!xpath || !object) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    exploded_path_cleanup(xpath);
    xpath->object = object;
    xpath->path_attributes = pathAttributes;

    sep = (xpath->path_attributes & path_attr_slash_separator) ? "/" : ".";

    string_initialize(&path, 0);
    object_path(xpath->object, &path, xpath->path_attributes);
    xpath->path_size = string_list_split(&(xpath->exploded_path), &path, sep, strlist_skip_empty_parts, string_case_sensitive);
    string_cleanup(&path);

    return true;
}

/**
   @brief
   Fill in exploded path structure from parameter.

   @details
   Fill in exploded path structure from parameter.
   The parameter's 'object' will be available in the structure.
   The 'parameter' and other parameter data will be available.

   @param xpath Exploded path structure
   @param parameter The parameter to take the path from
   @param pathAttributes The path attributes when parsing the parameter path

   @return true if successful, false otherwise
 */
bool exploded_path_fromParameter(exploded_path_t* xpath, parameter_t* parameter, const uint32_t pathAttributes) {
    if(!xpath || !parameter) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(!exploded_path_fromObject(xpath, parameter_owner(parameter), pathAttributes)) {
        return false;
    }

    xpath->path_attributes |= (path_attr_include_parameter | path_attr_include_value);
    xpath->parameter = parameter;
    string_fromChar(&(xpath->param_name), parameter_name(parameter));

    return true;
}

/**
   @brief
   Fill in exploded path structure from a char string.

   @details
   Fill in exploded path structure from a char string.
   The 'object' and 'parameter' will be empty.
   If a '=' character is present, the string is split into
   '<path.to.object>.<parameter_name>=<parameter_value>'
   The parameter_value can be empty. The parameter data
   is filled in as much with what is provided.
   There is no attempt to map the path to any object or parameter.

   @param xpath Exploded path structure
   @param path The path to parse
   @param pathAttributes The path attributes when parsing the path

   @return true if successful, false otherwise
 */
bool exploded_path_fromChar(exploded_path_t* xpath, const char* path, const uint32_t pathAttributes) {
    const char* sep = NULL;
    char* path_dup = NULL, * pn = NULL, * pv = NULL;

    if(!xpath) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    exploded_path_cleanup(xpath);
    xpath->path_attributes = pathAttributes;

    if(!path || !*path) {
        return true;
    }

    sep = (xpath->path_attributes & path_attr_slash_separator) ? "/" : ".";

    // No separator as first or last character
    if((path[0] == sep[0]) || (path[strlen(path) - 1] == sep[0])) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    path_dup = strdup(path);
    if(!path_dup) {
        pcb_error = pcb_error_no_memory;
        return false;
    }

    pv = strchr(path_dup, '=');
    if(pv) {
        *pv = 0;
        pv++;

        pn = strrchr(path_dup, sep[0]);
        if(!pn) {
            free(path_dup);

            pcb_error = pcb_error_invalid_parameter;
            return false;
        }

        *pn = 0;
        pn++;
    }

    xpath->path_size = string_list_splitChar(&(xpath->exploded_path), path_dup, sep, strlist_skip_empty_parts, string_case_sensitive);
    if(pn) {
        string_fromChar(&(xpath->param_name), pn);
        xpath->path_attributes |= path_attr_include_parameter;
    }
    if(pv && *pv) {
        string_fromChar(&(xpath->param_value), pv);
        xpath->path_attributes |= path_attr_include_value;
    }

    free(path_dup);

    return true;
}

/**
   @brief
   Descend a level by object.

   @details
   Descend a level by object.
   This is only possible with exploded path structures filled
   in using @ref exploded_path_fromObject().
   The child object must be a direct descendant of the
   structure's current object.
   The name of the object is derived using the same path
   attributes as given to the _fromObject() function.

   @param xpath Exploded path structure
   @param object The child object to descend into

   @return true if successful, false otherwise
 */
bool exploded_path_descendObject(exploded_path_t* xpath, object_t* object) {
    if(!xpath || !object || !xpath->object) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(!string_isEmpty(&(xpath->param_name))) {
        pcb_error = pcb_error_wrong_state;
        return false;
    }

    if(object_parent(object) != xpath->object) {
        pcb_error = pcb_error_not_found;
        return false;
    }

    exploded_path_descend(xpath, object, object_name(object, xpath->path_attributes));

    return true;
}

/**
   @brief
   Descend a level by char string.

   @details
   Descend a level by char string.
   This is only possible when no parameter data is set.
   The char string should provide the name of a direct descendant and
   should not contain any separators.
   If an 'object' is available, the child's object name is derived using
   the same path attributes as given to the _fromObject() or
   _fromChar() functions, and the child must exist.

   @param xpath Exploded path structure
   @param path The path to descend into

   @return true if successful, false otherwise
 */
bool exploded_path_descendChar(exploded_path_t* xpath, const char* path) {
    object_t* object = NULL;

    if(!xpath) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(!string_isEmpty(&(xpath->param_name))) {
        pcb_error = pcb_error_wrong_state;
        return false;
    }

    if(!path || !*path) {
        return true;
    }

    // No separators allowed
    if(strchr(path, (xpath->path_attributes & path_attr_slash_separator) ? '/' : '.') != NULL) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(xpath->object) {
        object = object_getObject(xpath->object, path, xpath->path_attributes, NULL);
        if(object_parent(object) != xpath->object) {
            pcb_error = pcb_error_not_found;
            return false;
        }
    }

    exploded_path_descend(xpath, object, path);

    return true;
}

/**
   @brief
   Ascend a level.

   @details
   Ascend a level.
   This is only possible when no parameter data is set.

   @param xpath Exploded path structure

   @return true if successful, false otherwise
 */
bool exploded_path_ascend(exploded_path_t* xpath) {
    if(!xpath) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(!string_isEmpty(&(xpath->param_name))) {
        pcb_error = pcb_error_wrong_state;
        return false;
    }

    if(xpath->path_size == 0) {
        return true;
    }

    xpath->object = object_parent(xpath->object);
    string_list_iterator_destroy(string_list_takeLast(&(xpath->exploded_path)));
    xpath->path_size--;

    return true;
}

/**
   @brief
   Get the full path stored in the structure.

   @details
   Get the full path stored in the structure.
   The path is using the separator as indicated by pathAttributes.
   An attribute about key notation is ignored.
   If parameter data is available together with path attributes concerning
   parameters, that data is appened.

   @param xpath Exploded path structure
   @param path The string to store the path
   @param pathAttributes Path attributes to construct the path

   @return true if successful, false otherwise
 */
bool exploded_path_getPath(exploded_path_t* xpath, string_t* path, const uint32_t pathAttributes) {
    return exploded_path_getPartialPath(xpath, path, pathAttributes, 0, UINT32_MAX);
}

/**
   @brief
   Get a partial path stored in the structure.

   @details
   Get a partial path stored in the structure.
   The path is using the separator as indicated by pathAttributes.
   An attribute about key notation is ignored.
   If parameter data is available together with path attributes concerning
   parameters and the last path element is also requested, that data is appened.

   @param xpath Exploded path structure
   @param path The string to store the path
   @param pathAttributes Path attributes to construct the path
   @param start The index of the path to start from
   @param length The length of the path to construct

   @return true if successful, false otherwise
 */
bool exploded_path_getPartialPath(exploded_path_t* xpath, string_t* path, const uint32_t pathAttributes, const uint32_t start, const uint32_t length) {
    bool first = true;
    uint32_t i = 0;
    char* sep = NULL;
    string_list_iterator_t* iter = NULL;

    if(!xpath || !path) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    sep = (pathAttributes & path_attr_slash_separator) ? "/" : ".";

    string_cleanup(path);

    for(i = 0, iter = string_list_at(&(xpath->exploded_path), start);
        i < length && iter != NULL;
        i++, iter = string_list_iterator_next(iter)) {
        if(!first) {
            string_appendChar(path, sep);
        }
        first = false;

        string_append(path, string_list_iterator_data(iter));
    }

    if(((length == UINT32_MAX) || (start + length >= xpath->path_size)) &&
       (pathAttributes & (path_attr_include_parameter | path_attr_include_value)) &&
       !string_isEmpty(&(xpath->param_name))) {
        string_appendChar(path, sep);
        string_append(path, &(xpath->param_name));
        string_appendChar(path, "=");

        if(pathAttributes & path_attr_include_value) {
            if(xpath->parameter) {
                string_append(path, variant_string(parameter_getValue(xpath->parameter)));
            } else {
                string_append(path, &(xpath->param_value));
            }
        }
    }

    return true;
}
