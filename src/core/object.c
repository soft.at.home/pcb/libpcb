/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#define _GNU_SOURCE
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <ctype.h>

#include <debug/sahtrace.h>

#include <pcb/utils/variant_map.h>
#include <pcb/utils/variant_list.h>

#include <pcb/common/error.h>

#include <pcb/core/object.h>
#include <pcb/core/parameter.h>
#include <pcb/core/pcb_main.h>

#include "core_priv.h"
#include "parameter_priv.h"
#include "object_priv.h"
#include "local_object.h"
#include "remote_object.h"
#include "validator_priv.h"
#include "datamodel_priv.h"
#include "request_priv.h"
#include "function_priv.h"
#include "path.h"

/**
   @file
   @brief
   Implementation of object functions
 */

typedef struct searchObjects_data_t {
    bool error;                 // Indicates an error
    object_t* ref;              // The reference object that is mapped
    object_list_t* obj_list;    // Object list containing matching objects
} searchObjects_data_t;

/**
   @brief
   Callback function for matching remote objects.

   @details
   Callback function for matching remote objects.

   @param req The request
   @param item The replied item
   @param pcb The PCB context
   @param from The peer the reply was received from
   @param userdata Custom userdata, in this case a pointer to a searchObjects_data_t.

   @return true if successful, false otherwise
 */
static bool object_searchObjects_mappedHandler(request_t* req, reply_item_t* item, pcb_t* pcb, peer_info_t* from, void* userdata) {
    object_t* object = NULL;
    searchObjects_data_t* data = (searchObjects_data_t*) userdata;

    (void) req;
    (void) pcb;
    (void) from;

    switch(reply_item_type(item)) {
    case reply_type_object:
        object = reply_item_object(item);
        SAH_TRACEZ_INFO("pcb", "Object %s matches", object_name(object, request_attributes(req) & request_common_path_key_notation));
        if(!default_objectTranslatePath(data->ref, object, object_firstDestination(data->ref), request_attributes(req))) {
            SAH_TRACEZ_ERROR("pcb", "Failed to translate object path");
            data->error = true;
        } else {
            llist_append(data->obj_list, &object->it);
        }
        break;
    case reply_type_error:
        SAH_TRACEZ_ERROR("pcb", "Error received : 0x%8.8X %s %s", reply_item_error(item), reply_item_errorDescription(item), reply_item_errorInfo(item));
        data->error = true;
        break;
    default:
        break;
    }

    return true;
}

/**
   @brief
   Adds an object to an object list if the mapped object matches a certain pattern

   @details
   Adds an object to an object list if the mapped object matches a certain pattern.
   Depending on the search attributes the parameters or their values are taken into account.
   This function should only be called for mapped objects. A find_objects request will be created
   and sent towards the destination plug-in.

   @param opath Exploded path for object to match
   @param ppath Exploded path for pattern
   @param depth The depth
   @param searchAttr The search attributes, see @ref search_attributes_t
   @param obj_list Pointer to an initialized object list (linked list), this will contain all matching objects

   @return true if successful, false upon matching error
 */
static bool object_searchObjects_mapped(exploded_path_t* opath, exploded_path_t* ppath, uint32_t depth, const uint32_t searchAttr, object_list_t* obj_list) {
    bool retval = false;
    struct timeval timeout = { 30, 0 }; // 30 second timeout
    string_t object_path, pattern_path;
    object_t* object = opath->object;
    pcb_t* pcb = datamodel_pcb(object_datamodel(object));
    request_t* req = NULL;
    searchObjects_data_t data = { .error = false, .ref = object, .obj_list = obj_list };

    string_initialize(&object_path, 0);
    exploded_path_getPath(opath, &object_path, path_attr_slash_separator);
    string_initialize(&pattern_path, 0);
    exploded_path_getPath(ppath, &pattern_path, path_attr_slash_separator);

    req = request_create_findObjects(string_buffer(&object_path), string_buffer(&pattern_path), depth,
                                     request_getObject_parameters | request_getObject_children | request_getObject_instances |
                                     request_find_include_parameters | request_find_include_values |
                                     (searchAttr & request_common_path_key_notation) | request_common_path_slash_separator |
                                     ((searchAttr & search_attr_instance_wildcards_only) ? request_find_instance_wildcards_only : 0));
    if(!req) {
        SAH_TRACEZ_ERROR("pcb", "Failed to create findObjects request for '%s' with pattern '%s'",
                         string_buffer(&object_path), string_buffer(&pattern_path));
        goto end;
    }

    default_requestTranslatePath(req, request_attributes(req), request_attributes(req), object, object_firstDestination(object));

    request_setReplyItemHandler(req, object_searchObjects_mappedHandler);
    request_setData(req, &data);

    if(!pcb_sendRequest(pcb, pcb_pluginConfig(pcb)->system_bus, req)) {
        SAH_TRACEZ_ERROR("pcb", "Failed to send request");
        goto end;
    }

    if(!pcb_waitForReply(pcb, req, &timeout)) {
        SAH_TRACEZ_ERROR("pcb", "Error waiting for reply");
        goto end;
    }

    retval = !data.error;

end:
    request_destroy(req);
    string_cleanup(&object_path);
    string_cleanup(&pattern_path);

    return retval;
}

/**
   @brief
   Adds an object to an object list if the object matches a certain pattern

   @details
   Adds an object to an object list if the object matches a certain pattern.
   Depending on the search attributes the parameters or theire values are taken into account.

   @param opath Exploded path for object to match
   @param ppath Exploded path for pattern
   @param depth The depth
   @param searchAttr The search attributes, see @ref search_attributes_t
   @param obj_list Pointer to an initialized object list (linked list), this will contain all matching objects
   @param checked Depth in exploded paths already checked

   @return true if successful, false upon matching error
 */
static bool object_searchObjects(exploded_path_t* opath, exploded_path_t* ppath, uint32_t depth, const uint32_t searchAttr, object_list_t* obj_list, uint32_t checked) {
    bool matched = false;
    object_t* object = opath->object, * child = NULL;
    uint32_t to_check = UINT32_MAX;

    // Partial or full path check
    to_check = opath->path_size < ppath->path_size ? opath->path_size - checked : UINT32_MAX;

    // Clear pcb_error to be able to differentiate between "no match" and "error occured"
    // during object_matches()
    pcb_error = pcb_ok;

    if(to_check > 0) {
        matched = object_matches(opath, ppath, searchAttr, to_check == UINT32_MAX ? 0 : checked, to_check);
    } else {
        // Nothing to check, assume match
        // This can happen when trying to match with the root object,
        // which has no name thus opath->path_size would be 0
        matched = true;
    }

    if(pcb_error != pcb_ok) {
        SAH_TRACEZ_ERROR("pcb", "Matching error: %s", error_string(pcb_error));
        return false;
    } else if(!matched) {
        // No match, no error
        return true;
    } else if(to_check == UINT32_MAX) {
        // Full path match
        llist_append(obj_list, &object->it);

        // If the object matches, we don't have to descend into
        // children nor instances, the pattern wouldn't match anymore
        return true;
    }
    // else partial path match ('to_check' should _not_ be UINT32_MAX)

    if(depth == 0) {
        return true;
    }

    if(object->attributes & (object_attr_linked | object_attr_mapped)) {
        SAH_TRACEZ_NOTICE("pcb", "Object %s is mapped/linked, getting objects from destination", object_name(object, searchAttr & search_attr_key_notation));
        object_searchObjects_mapped(opath, ppath, depth, searchAttr, obj_list);
        // Ignore the result. We provide whatever has been found already.
        return true;
    }

    if(!object_isTemplate(object)) {
        object_for_each_child(child, object) {
            exploded_path_descendObject(opath, child);
            if(!object_searchObjects(opath, ppath, (depth == UINT32_MAX) ? depth : (depth - 1), searchAttr, obj_list, checked + to_check)) {
                return false;
            }
            exploded_path_ascend(opath);
        }
    } else {
        object_for_each_instance(child, object) {
            exploded_path_descendObject(opath, child);
            if(!object_searchObjects(opath, ppath, (depth == UINT32_MAX) ? depth : (depth - 1), searchAttr, obj_list, checked + to_check)) {
                return false;
            }
            exploded_path_ascend(opath);
        }
    }

    return true;
}

/**
   @ingroup pcb_core_object
   @brief
   Create an object

   @details
   This function will create an empty object.\n
   The object will be in the created state (@ref object_state_created) until it is committed (@ref object_commit).\n
   This functions allocates memory for the object and initializes the structure.\n
   It also checks that the name is valid.\n
   The object is set in created state.\n

   @param parent The parent object, a newly created object must have a parent
   @param name Name of the object. The name can not contain ".", "/","$" or be numeric and must be unique within the parent object
   @param attributes The object attributes.

   @return
    - pointer to the created object: Creation was successful. The object is in the created state
    - NULL: An error has occurred
        - name is a NULL pointer or an empty string
        - memory allocation failed
        - initialization failed
        - the name is not valid
 */
object_t* object_create(object_t* parent, const char* name, const uint32_t attributes) {
    uint32_t attr = 0;
    object_t* object = NULL;
    if(!parent) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    if(!name || !(*name)) {
        pcb_error = pcb_error_empty_name;
        return NULL;
    }

    if((attributes & object_attr_remote) == object_attr_remote) {
        pcb_error = pcb_error_remote_local_mismatch;
        return NULL;
    }

    /* verify the name
       object_verify_name characters not allowed are : . / $ only numeric
     */
    if(!isValidNameChar(name)) {
        pcb_error = pcb_error_invalid_name;
        goto error;
    }

    /* does the parent object already contains an object with the same name ? */
    if(local_object_getObject(parent, name, 0, NULL) != NULL) {
        pcb_error = pcb_error_not_unique_name;
        goto error;
    }

    attr = object_isMib(parent) ? (attributes | object_attr_mib) : attributes;
    object = object_allocate(name, attr);
    if(!object) {
        pcb_error = pcb_error_no_memory;
        goto error;
    }

    /* add this object to the parent */
    object_setParent(object, parent);

    return object;

error:
    return NULL;
}

/**
   @ingroup pcb_core_object
   @brief
   Delete an object

   @details
   This function will delete an object.\n
   An object can only be deleted if no other operation is busy. (The object must be in the ready state)\n
   The object will be in the deleted state (@ref object_state_deleted) until it is committed (@ref object_commit).\n
   During the commit the object will be physically deleted.\n
   All children and/or instances of this object will be deleted as well.

   This functions marks the object as deleted and moves it to the temporary list of instances if it is an instance.\n
   It will iterate over the child objects and instance objects and mark them for deletion as well.\n
   If deletion of a child fails, nothing is deleted.

   @param object pointer to the object that will be deleted

   @return
    - true: deletion was successful. The object is in deleted state. Use @ref object_commit to really delete the object.
    - false: An error has occurred
        - An other operation is buzy.
        - One of the children failed to delete
 */
bool object_delete(object_t* object) {
    /* the object must be in ready state, if an other operation is buzy we fail immediatly */
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }
    if(!((object->state == object_state_ready) || (object->state == object_state_deleted))) {
        pcb_error = pcb_error_wrong_state;
        return false;
    }

    if(object->state == object_state_deleted) {
        return true;
    }

    if(object_isRemote(object)) {
        return remote_object_delete(object);
    } else {
        return local_object_delete(object);
    }
}

#ifdef PCB_HELP_SUPPORT
/**
   @ingroup pcb_core_object
   @brief
   Set a description for the object

   @details
   Set a description for the object.

   @param object pointer to the object that will be deleted
   @param description text containing the description

   @return
    - true
    - false
 */
bool object_setDescription(object_t* object, const char* description) {
    if(!object || !description) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(object->description) {
        free(object->description);
        object->description = NULL;
    }
    if(*description) {
        object->description = strdup(description);
        if(!object->description) {
            return false;
        }
        SAH_TRACEZ_INFO("pcb", "Set object description: %s", object->description);
    }
    return true;
}

/**
   @ingroup pcb_core_object
   @brief
   Get the description of the object

   @details
   Get the description of the object.

   @param object pointer to the object that will be deleted

   @return
    - string containing the description of the object
 */
const char* object_getDescription(object_t* object) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    return object->description;
}
#endif

/**
   @ingroup pcb_core_object
   @brief
   Get the root object

   @details
   This function will return the root object from a data model. The root object is an invisible object
   and is the absolute parent of all objects in the data model.

   @param object pointer to an object

   @return
    - the root object.
 */
object_t* object_root(const object_t* object) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    object_t* parent = NULL;
    if((object->attributes & object_attr_instance) == object_attr_instance) {
        parent = object_instanceOf(object);
    } else {
        parent = local_object_parent(object);
    }
    return parent ? object_root(parent) : (object_t*) object;
}

/**
   @ingroup pcb_core_object
   @brief
   Get the data model.

   @details
   This function will return the data model pointer.
   Any object is always a membe-r of a data model.

   @param object pointer to an object

   @return
    - the pointer to the data model.
 */
datamodel_t* object_datamodel(const object_t* object) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    object_t* root = object_root(object);
    if(object_isMib(object)) {
        return llist_item_data(root, datamodel_t, mibRoot);
    } else {
        return llist_item_data(root, datamodel_t, root);
    }
}

/**
   @ingroup pcb_core_object
   @brief
   Return the parent object.

   @details
   This function returns the parent object.\n
   If the object given is a remote object and the parent object is not in the connection cache,
   the parent object is retrieved with synchronous requests.

   @param object pointer to the object from which the parent is retreived.

   @return
    - pointer to the parent object.
    - NULL: The object has no parent, this can only be for root objects
 */
object_t* object_parent(const object_t* object) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    object_t* parent = local_object_parent(object);
    if(object_isRemote(object)) {
        pcb_t* pcb = datamodel_pcb(object_datamodel(object));
        if(pcb) {
            datamodel_t* cache = pcb_cache(pcb);
            object_t* cache_root = datamodel_root(cache);
            if(parent == cache_root) {
                remote_object_info_t* ri = object->userData;
                if(string_length(&ri->parentPathIndex) != 0) {
                    parent = remote_object_getObject(cache_root, string_buffer(&ri->parentPathIndex), 0, NULL, ri);
                }
            }
        }
    }
    return parent;
}

/**
   @ingroup pcb_core_object
   @brief
   Return the first child object

   @details
   This function returns the first child object of the specified object\n
   For remote objects, only the children in the cache are taken into account.\n
   This function uses the tree function (@ref tree_item_firstChild) to get the first child of this object.\n
   The object pointer is retreived using the macro @ref tree_item_data

   @param object pointer to the object from which the first child is retreived

   @return
    - pointer to the child object.
    - NULL: The object has no children
 */
object_t* object_firstChild(object_t* object) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    tree_item_t* item = tree_item_firstChild(&object->tree_item);
    if(!item) {
        return NULL;
    }

    return tree_item_data(item, object_t, tree_item);
}

/**
   @ingroup pcb_core_object
   @brief
   Return the last child object

   @details
   This function returns the last child object of the specified object\n
   For remote objects, only the children in the cache are taken into account.\n
   This function uses the tree function (@ref tree_item_lastChild) to get the last child of this object.\n
   The object pointer is retreived using the macro @ref tree_item_data

   @param object pointer to the object from which the last child is retreived

   @return
    - pointer to the child object.
    - NULL: The object has no children
 */
object_t* object_lastChild(object_t* object) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    tree_item_t* item = tree_item_lastChild(&object->tree_item);
    if(!item) {
        return NULL;
    }

    return tree_item_data(item, object_t, tree_item);
}

/**
   @ingroup pcb_core_object
   @brief
   Return the next sibling object

   @details
   This function returns the next sibling object of the specified object.\n
   For remote objects, only the siblins in the cache are taken into account.\n
   This function uses the tree function (@ref tree_item_nextSibling) to get the next child of the parent of this object.\n
   The object pointer is retreived using the macro @ref tree_item_data

   @param object pointer to the object from which the next sibling is retreived

   @return
    - pointer to the child object.
    - NULL: There is no next sibling
 */
object_t* object_nextSibling(const object_t* object) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    tree_item_t* item = tree_item_nextSibling(&object->tree_item);
    if(!item) {
        return NULL;
    }

    return tree_item_data(item, object_t, tree_item);
}

/**
   @ingroup pcb_core_object
   @brief
   Return the previous sibling object

   @details
   This function returns the previous sibling object of the specified object\n
   For remote objects, only the siblins in the cache are taken into account.\n
   This function uses the tree function (@ref tree_item_prevSibling) to get the previous child of the parent of this object.\n
   The object pointer is retreived using the macro @ref tree_item_data

   @param object pointer to the object from which the previous sibling is retreived

   @return
    - pointer to the child object.
    - NULL: There is no previous sibling
 */
object_t* object_prevSibling(const object_t* object) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    tree_item_t* item = tree_item_prevSibling(&object->tree_item);
    if(!item) {
        return NULL;
    }

    return tree_item_data(item, object_t, tree_item);
}

/**
   @ingroup pcb_core_object
   @brief
   Get an object starting from a parent object.

   @details
   This function returns an object starting from a given object.\n
   The path given is relative to the starting object, if in the attributes the @ref path_attr_partial_match flag
   is specified the best matching object is returned. Optionally a pointer to a bool can be given, this will be
   filled with true if the match was exact (the full path matches) or false (on a partial match).

   @param object pointer to the object from which the given path starts.
   @param path the relative path to the object you want to retrieve.
   @param pathAttr path attributes
   @param exactMatch pointer to a bool, will be filled in with true or false.

   @return
    - pointer to found object.
    - NULL: no object is matching the criteria
 */
object_t* object_getObject(object_t* object, const char* path, const uint32_t pathAttr, bool* exactMatch) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    if(!path || !(*path)) {
        if(exactMatch) {
            *exactMatch = true;
        }
        return object;
    }

    object_t* foundObject = NULL;
    if(object_isRemote(object)) {
        foundObject = remote_object_getObject(object, path, pathAttr, exactMatch, NULL);
    } else {
        if((object_parent(object) == NULL) && datamodel_isCache(object_datamodel(object))) {
            foundObject = remote_object_getObject(object, path, pathAttr, exactMatch, NULL);
        } else {
            foundObject = local_object_getObject(object, path, pathAttr, exactMatch);
        }
    }

    return foundObject;
}

/**
   @ingroup pcb_core_object
   @brief
   Find objects matching a pattern.

   @details
   This function returns a list of objects that match a pattern

   @param object pointer to the object from which search has to start.
   @param pattern pattern that the is used for object matching.
   @param depth depth relative to the starting object
   @param searchAttr the search attributes.

   @return
    - pointer to an object list (linked list), the list has to be destroyed when it is not used anymore.
    - the list can contain zero, one or more objects
    - A NULL return value indicates an error
 */
object_list_t* object_findObjects(object_t* object, const char* pattern, uint32_t depth, const uint32_t searchAttr) {
    uint32_t pa = searchAttr & (path_attr_slash_separator | path_attr_key_notation);
    string_t sPattern;
    exploded_path_t opath, ppath;

    if(!object || !pattern) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    object_list_t* obj_list = (object_list_t*) calloc(1, sizeof(object_list_t));
    if(!obj_list) {
        pcb_error = pcb_error_no_memory;
        return NULL;
    }

    if(object_isRemote(object)) {
        return obj_list;
    }

    string_initialize(&sPattern, 50);
    exploded_path_initialize(&opath);
    exploded_path_initialize(&ppath);

    // Fix existing usage due to previously faulty implementation wrt '*'
    if(pattern[0] == '*') {
        object_path(object, &sPattern, pa);
        if(!string_isEmpty(&sPattern)) {
            string_appendChar(&sPattern, (pa & path_attr_slash_separator) ? "/" : ".");
        }
    }
    string_appendChar(&sPattern, pattern);

    if(!exploded_path_fromObject(&opath, object, pa) ||
       !exploded_path_fromChar(&ppath, string_buffer(&sPattern), pa)) {
        SAH_TRACEZ_ERROR("pcb", "Failed to explode path");
        goto end;
    }

    bool search_success = object_searchObjects(&opath, &ppath, depth, searchAttr, obj_list, 0);

    if(!search_success) {
        llist_cleanup(obj_list);
        free(obj_list);
        obj_list = NULL;
    }

end:
    exploded_path_cleanup(&opath);
    exploded_path_cleanup(&ppath);
    string_cleanup(&sPattern);
    return obj_list;
}

/**
   @ingroup pcb_core_object
   @brief
   Converts a list iterator to an object

   @details
   This function will give the pointer to an object using a list iterator.
   The list iterator must be pqrt of an object, otherwhise the returned pointer will be invalid.
   Only use this function on iterators comming from an object list. @ref object_findObjects

   @param it list iterator

   @return
    - pointer to an object
 */
object_t* object_item(llist_iterator_t* it) {
    if(!it) {
        return NULL;
    }

    return llist_item_data(it, object_t, it);
}

/**
   @ingroup pcb_core_object
   @brief
   Check that the object has children

   @details
   Check that the object has children, instances are not taken into acount as a child. (use @ref object_hasInstances)
   This function uses @ref tree_item_hasChildren to check that the object has children.

   @param object pointer to the object that will be checked for children

   @return
    - true: child objects are available
    - false: no child objects are available
 */
bool object_hasChildren(const object_t* object) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(object_isRemote(object)) {
        return remote_object_hasChildren(object);
    } else {
        return local_object_hasChildren(object);
    }
}

/**
   @ingroup pcb_core_object
   @brief
   Returns the number of children

   @details
   Returns the number of children, instances are not taken into acount as a child. (use @ref object_instanceCount)
   This function uses @ref tree_item_childCount to get the number of child objects.

   @param object pointer to the object of which the number of children are returned

   @return
    the number of child objects available.
 */
uint32_t object_childCount(object_t* object) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return 0;
    }

    if(object_isRemote(object)) {
        return remote_object_childCount(object);
    } else {
        return local_object_childCount(object);
    }
}

/**
   @ingroup pcb_core_object
   @brief
   Build the full path of an object

   @details
   This function builds the full path of an object.\n
   This functions uses the internal function object_buildPath which is recursing over the parents of this object.\n
   The path string parameter will contain the path as specified with the path attributes.

   @param object pointer from which the path has to be returned.
   @param path path string variable that will contain the full path
   @param pathAttributes the path attributes

 */
void object_path(const object_t* object, string_t* path, const uint32_t pathAttributes) {
    string_clear(path);

    if(!object || !path) {
        pcb_error = pcb_error_invalid_parameter;
        return;
    }

    if(object_isRemote(object)) {
        remote_object_path(object, path, pathAttributes);
    } else {
        if(pathAttributes & path_attr_parent) {
            local_object_path(object_parent(object), path, pathAttributes & ~path_attr_parent);
        } else {
            local_object_path(object, path, pathAttributes);
        }
    }
}

/**
   @ingroup pcb_core_object
   @brief
   Build the full path of an object

   @details
   This function builds the full path of an object.\n
   This functions behaves exactly the same as @ref object_path

   @param object pointer from which the path has to be returned.
   @param pathAttributes the path attributes

   @return
    - returns the path of the object. The returned buffer must be freed
 */
char* object_pathChar(const object_t* object, const uint32_t pathAttributes) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    string_t path;
    if(!string_initialize(&path, 64)) {
        return NULL;
    }

    object_path(object, &path, pathAttributes);
    if(string_isEmpty(&path)) {
        string_cleanup(&path);
        return NULL;
    }

    /* do not cleanup path, it is on the stack and the buffer is returned */
    return (char*) string_buffer(&path);
}

/**
   @ingroup pcb_core_object
   @brief
   Create an instance of a template object

   @details
   This functions creates an instances of a template object.\n
   The name of the instance will be a numeric index. An instance object starts a new tree within the
   template object.\n
   The new instance will not be available until it is committed using @ref object_commit.\n
   An instance can not be created from a template object that is marked for deletion.\n
   If the template object is a remote object, a synchronous request is send to create an instance\n

   A new object is allocated using calloc. The object is initialized using the private function
   object_initialize. The template object is copied into the new created object, except the
   instances. This is done using the private function object_copy. The instance object does not
   have a parent, but instead the instance_of is filled in.\n

   @param object pointer to the template object
   @param index the index for the newly created instance (if the index is UINT32_MAX, the next available index is used)
   @param key the key for the newly created instance

   @return
    - true: the operation was successful
    - false: an error occured
        - The object is not a template object or object is NULL
        - no more memory available
 */
object_t* object_createInstance(object_t* object, uint32_t index, const char* key) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    if((object->attributes & object_attr_template) != object_attr_template) {
        pcb_error = pcb_error_not_template;
        return NULL;
    }

    /* can not create instances of a deleted object */
    if(object->state == object_state_deleted) {
        pcb_error = pcb_error_wrong_state;
        return NULL;
    }

    /*
       if (!isValidNameChar(key)) {
        pcb_error = pcb_error_invalid_name;
        return NULL;
       }
     */
    /* THE HACK AS MANY PLUGINS USE WRONG KEYS */
    if(key && (strpbrk(key, "./\"") != NULL)) {
        pcb_error = pcb_error_invalid_name;
        SAH_TRACEZ_ERROR("pcb", "Name %s is not valid", key);
        return NULL;
    }

    if(object_isRemote(object)) {
        return remote_object_createInstance(object, index, key);
    } else {
        return local_object_createInstance(object, index, key);
    }
}

/**
   @ingroup pcb_core_object
   @brief
   Returns the template object for this instance

   @details
   This function returns the template object if the given object is an instance object\n
   returns the instance_of member of the object structure

   @param object pointer to an instance object

   @return
    - pointer to the template object
    - NULL
        - The specified object is not an instance object
        - object is NULL
 */
object_t* object_instanceOf(const object_t* object) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }
    if(!(object->attributes & object_attr_instance)) {
        pcb_error = pcb_error_not_instance;
        return NULL;
    }

    return local_object_parent(object);
}

/**
   @ingroup pcb_core_object
   @brief
   Returns the first instance object.

   @details
   If the specified object is a template object, the first instance is returned.\n
   For remote objects only the instances in the cache are taken into account.\n
   Uses @ref llist_first and @ref llist_item_data to get the instance object pointer.
   If the specified object is not a template object the list will be empty, no need
   to check if the given object is a template object.

   @param object pointer to a template object.

   @return
    - pointer to the first instance
    - NULL
        - object is NULL
        - the object does not contain any instances of the requested type.
        - this is not a template object
 */
object_t* object_firstInstance(object_t* object) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    if(!object_isTemplate(object)) {
        pcb_error = pcb_error_not_template;
        return NULL;
    }

    llist_iterator_t* it = llist_first(&object->object_info.template_info->instances);
    if(!it) {
        pcb_error = pcb_error_last_instance;
        return NULL;
    }

    return llist_item_data(it, object_t, instance_it);
}

/**
   @ingroup pcb_core_object
   @brief
   Returns the last instance object.

   @details
   If the specified object is a template object, the last instance is returned.\n
   For remote objects only the instances in the cache are taken into account.\n
   Uses @ref llist_last and @ref llist_item_data to get the instance object pointer.
   If the specified object is not a template object the list will be empty, no need
   to check if the given object is a template object.

   @param object pointer to a template object.

   @return
    - pointer to the template object
    - NULL
        - object is NULL
        - the object does not contain any instances
        - this is not a template object
 */
object_t* object_lastInstance(object_t* object) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    if(!object_isTemplate(object)) {
        pcb_error = pcb_error_not_template;
        return NULL;
    }

    llist_iterator_t* it = llist_last(&object->object_info.template_info->instances);
    if(!it) {
        pcb_error = pcb_error_last_instance;
        return NULL;
    }

    return llist_item_data(it, object_t, instance_it);
}

/**
   @ingroup pcb_core_object
   @brief
   Returns the next instance object.

   @details
   If the specified object is an instance object, the next instance is returned of the same teamplate object.\n
   For remote objects only the instances in the cache are taken into account.\n
   Uses @ref llist_iterator_next and @ref llist_item_data to get the instance object pointer.

   @param object pointer to an instance object

   @return
    - pointer to the instance object
    - NULL
        - object is NULL
        - the object was the last instance object
        - this is not an instance object
 */
object_t* object_nextInstance(const object_t* object) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    if(!object_isInstance(object)) {
        pcb_error = pcb_error_not_template;
        return NULL;
    }

    llist_iterator_t* it = llist_iterator_next(&object->instance_it);
    if(!it) {
        pcb_error = pcb_error_last_instance;
        return NULL;
    }

    return llist_item_data(it, object_t, instance_it);
}

/**
   @ingroup pcb_core_object
   @brief
   Returns the previous instance object.

   @details
   If the specified object is an instance object, the previous instance is returned of the same template object.\n
   For remote objects only the instances in the cache are taken into account.\n
   Uses @ref llist_iterator_prev and @ref llist_item_data to get the instance object pointer.

   @param object pointer to an instance object

   @return
    - pointer to the instance object
    - NULL
        - object is NULL
        - the object was the first instance object
        - this is not an instance object
 */
object_t* object_prevInstance(const object_t* object) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    if(!object_isInstance(object)) {
        pcb_error = pcb_error_not_template;
        return NULL;
    }

    llist_iterator_t* it = llist_iterator_prev(&object->instance_it);
    if(!it) {
        pcb_error = pcb_error_last_instance;
        return NULL;
    }

    return llist_item_data(it, object_t, instance_it);
}

/**
   @ingroup pcb_core_object
   @brief
   Checks to object for instances

   @details
   If the object is a template object, and it has instances, this function will return true.\n
   When the template object is a remote object, the retrieved instance count will be used to check if
   the template object has instances. This number is possible not up to date.
   Uses @ref llist_isEmpty to see if the object has instances or not.

   @param object pointer to a template object

   @return
    - true this object has instances
    - false
        - the object is not a template object
        - the object has no instances
 */
bool object_hasInstances(const object_t* object) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }
    if(!object_isTemplate(object)) {
        pcb_error = pcb_error_not_template;
        return false;
    }

    if(object_isRemote(object)) {
        return remote_object_hasInstances(object);
    } else {
        return local_object_hasInstances(object);
    }
}

/**
   @ingroup pcb_core_object
   @brief
   Returns the number of instances

   @details
   If the object is a template object, this function returns the number of instances\n
   When the template object is a remote object, the retrieved instance count will be used to check if
   the template object has instances. This number is possible not up to date.\n
   Uses @ref llist_size to get the number of instances.

   @param object pointer to a template object

   @return
    - The number of instances
 */
uint32_t object_instanceCount(object_t* object) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return 0;
    }
    if(!object_isTemplate(object)) {
        pcb_error = pcb_error_not_template;
        return 0;
    }

    if(object_isRemote(object)) {
        return remote_object_instanceCount(object);
    } else {
        return local_object_instanceCount(object);
    }
}

/**
   @ingroup pcb_core_object
   @brief
   Returns the first parameter of an object

   @details
   Returns the first parameter of an object.\n
   For remote objects only the parameters in the cache are taken into account.\n
   Uses @ref llist_first and @ref llist_item_data to get the parameter pointer.

   @param object pointer to an object

   @return
    - pointer to the first parameter
    - NULL
        - object is NULL
        - the object does not contain any parameters of the requested type.
 */
parameter_t* object_firstParameter(const object_t* object) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    llist_iterator_t* it = llist_first(&object->parameters);
    if(!it) {
        pcb_error = pcb_error_last_parameter;
        return NULL;
    }
    parameter_t* parameter = llist_item_data(it, parameter_t, it);

    return parameter;
}

/**
   @ingroup pcb_core_object
   @brief
   Returns the last parameter of an object

   @details
   Returns the last parameter of an object.\n
   For remote objects only the parameters in the cache are taken into account.\n
   Uses @ref llist_last and @ref llist_item_data to get the parameter pointer.

   @param object pointer to an object

   @return
    - pointer to the last parameter
    - NULL
        - object is NULL
        - the object does not contain any parameters of the requested type.
 */
parameter_t* object_lastParameter(object_t* object) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    llist_iterator_t* it = llist_last(&object->parameters);
    if(!it) {
        pcb_error = pcb_error_last_parameter;
        return NULL;
    }
    parameter_t* parameter = llist_item_data(it, parameter_t, it);

    return parameter;
}

/**
   @ingroup pcb_core_object
   @brief
   Returns the next parameter of an object

   @details
   Returns the next parameter of an object.\n
   For remote objects only the parameters in the cache are taken into account.\n
   Uses @ref llist_iterator_next and @ref llist_item_data to get the parameter pointer.

   @param parameter pointer to the parameter which is used as a reference.

   @return
    - pointer to the next parameter
    - NULL
        - object is NULL
        - the object does not contain any parameters of the requested type.
 */
parameter_t* object_nextParameter(const parameter_t* parameter) {
    if(!parameter) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    llist_iterator_t* it = parameter->it.next;
    if(!it) {
        pcb_error = pcb_error_last_parameter;
        return NULL;
    }
    parameter_t* param = llist_item_data(it, parameter_t, it);

    return param;
}

/**
   @ingroup pcb_core_object
   @brief
   Returns the prev parameter of an object

   @details
   Returns the prev parameter of an object.\n
   For remote objects only the parameters in the cache are taken into account.\n
   Uses @ref llist_iterator_prev and @ref llist_item_data to get the parameter pointer.

   @param parameter pointer to the parameter which is used as a reference.

   @return
    - pointer to the next parameter
    - NULL
        - object is NULL
        - the object does not contain any parameters of the requested type.
 */
parameter_t* object_prevParameter(const parameter_t* parameter) {
    if(!parameter) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    llist_iterator_t* it = llist_iterator_prev(&parameter->it);
    if(!it) {
        pcb_error = pcb_error_last_parameter;
        return NULL;
    }
    parameter_t* param = llist_item_data(it, parameter_t, it);

    return param;
}

/**
   @ingroup pcb_core_object
   @brief
   Returns a specified parameter from an object

   @details
   Get a parameter by name.\n
   If the object is a remote object, and the parameter is not found in the cache, a synchronous request is created
   to retireve the parameter.

   @param object pointer to the object
   @param parameterName name of the parameter

   @return
    - pointer to the parameter
    - NULL
        - object is NULL
        - the object does not contain a parameter with the specified name
 */
parameter_t* object_getParameter(const object_t* object, const char* parameterName) {
    if(!object || !parameterName) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    parameter_t* parameter = local_object_getParameter(object, parameterName);
    if(!parameter && object_isRemote(object)) {
        return remote_object_getParameter(object, parameterName);
    } else {
        return parameter;
    }
}

/**
   @ingroup pcb_core_object
   @brief
   Verifies that an object contains a parameter.

   @details
   The verification is done on the local object and for remote objects on the cached data.
   If the parameter is not in the cache this functions will return false.

   @param object pointer to the object
   @param parameterName name of the parameter

   @return
    - true: the parameter is available
    - false: parameter not found
 */
bool object_hasParameter(const object_t* object, const char* parameterName) {
    if(!object || !parameterName) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    parameter_t* parameter = local_object_getParameter(object, parameterName);

    return parameter ? true : false;
}

/**
   @ingroup pcb_core_object
   @brief
   Check the object for parameters

   @details
   If this object has at least one parameter this function will return true\n
   If the object is a remote object, the retrieved parameter count will be used to check if the
   object has parameters.

   @param object pointer to an object

   @return
    - true: this object parameters
    - false
        - the object is a NULL pointer
        - this object has no parameters
 */
bool object_hasParameters(const object_t* object) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(object_isRemote(object)) {
        return remote_object_hasParameters(object);
    } else {
        return local_object_hasParameters(object);
    }
}

/**
   @ingroup pcb_core_object
   @brief
   Return the number of parameters

   @details
   This function returns the number of parameters\n
   If the object is a remote object, the retrieved parameter count will be used. This number may not be up to date

   @param object pointer to an object

   @return
    - The number of parameters in this object
 */
uint32_t object_parameterCount(object_t* object) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return 0;
    }

    if(object_isRemote(object)) {
        return remote_object_parameterCount(object);
    } else {
        return local_object_parameterCount(object);
    }
}

/**
   @ingroup pcb_core_object
   @brief
   Returns the first function of an object

   @details
   Returns the first function of an object.\n
   For remote objects only the functions in the cache are taken into account.\n

   Uses @ref llist_first and @ref llist_item_data to get the parameter pointer.

   @param object pointer to an object

   @return
    - pointer to the first function
    - NULL
        - object is NULL
 */
function_t* object_firstFunction(object_t* object) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    llist_iterator_t* it = llist_first(&object->functions);
    if(!it) {
        pcb_error = pcb_error_last_function;
        return NULL;
    }
    function_t* function = llist_item_data(it, function_t, it);

    return function;
}

/**
   @ingroup pcb_core_object
   @brief
   Returns the last function of an object

   @details
   Returns the last function of an object.\n
   For remote objects only the functions in the cache are taken into account.\n

   @param object pointer to an object

   @return
    - pointer to the last function
    - NULL
        - object is NULL
 */
function_t* object_lastFunction(object_t* object) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    llist_iterator_t* it = llist_last(&object->functions);
    if(!it) {
        pcb_error = pcb_error_last_function;
        return NULL;
    }
    function_t* function = llist_item_data(it, function_t, it);

    return function;
}

/**
   @ingroup pcb_core_object
   @brief
   Returns the next function of an object

   @details
   Returns the next function of an object.\n
   For remote objects only the functions in the cache are taken into account.\n

   @param func pointer to a function, used as reference

   @return
    - pointer to the next function
    - NULL
        - object is NULL
 */
function_t* object_nextFunction(const function_t* func) {
    if(!func) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    llist_iterator_t* it = llist_iterator_next(&func->it);
    if(!it) {
        pcb_error = pcb_error_last_function;
        return NULL;
    }
    function_t* function = llist_item_data(it, function_t, it);

    return function;
}

/**
   @ingroup pcb_core_object
   @brief
   Returns the previous function of an object

   @details
   Returns the previous function of an object.\n
   For remote objects only the functions in the cache are taken into account.\n

   @param func pointer to a function, used as reference

   @return
    - pointer to the previous function
    - NULL
        - object is NULL
 */
function_t* object_prevFunction(const function_t* func) {
    if(!func) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    llist_iterator_t* it = llist_iterator_prev(&func->it);
    if(!it) {
        pcb_error = pcb_error_last_function;
        return NULL;
    }
    function_t* function = llist_item_data(it, function_t, it);

    return function;
}

/**
   @ingroup pcb_core_object
   @brief
   Get a function by name

   @details
   Searches the object for a function definition with the given name.\n
   For remote objects first the object cache are taken into account, if the
   function definition is not found in the cache, a request is send to the owner of the object.\n

   @param object pointer to the object
   @param functionName the function name

   @return
    - pointer to the function, if found
    - NULL
        - object is NULL
 */
function_t* object_getFunction(object_t* object, const char* functionName) {
    if(!object || !functionName) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    function_t* func = local_object_getFunction(object, functionName);
    if(!func && object_isRemote(object)) {
        func = remote_object_getFunction(object, functionName);
    }

    return func;
}

/**
   @ingroup pcb_core_object
   @brief
   Has the object functions

   @details
   Check the object to see if it has at least one function definition\n
   For remote objects no request is send. The number of functions availble will be stored in the
   object cahce\n

   @param object pointer to the object

   @return
    - true: the object contains at least one function
    - false no functions available
 */
bool object_hasFunctions(const object_t* object) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(object_isRemote(object)) {
        return remote_object_hasFunctions(object);
    } else {
        return local_object_hasFunctions(object);
    }
}

/**
   @ingroup pcb_core_object
   @brief
   Get the number of functions

   @details
   Get the number of functions of an object\n
   For remote objects no request is send. The number of functions availble will be stored in the
   object cahce\n

   @param object pointer to the object

   @return
    - The number of functions
 */
uint32_t object_functionCount(object_t* object) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return 0;
    }

    if(object_isRemote(object)) {
        return remote_object_functionCount(object);
    } else {
        return local_object_functionCount(object);
    }
}

/**
   @ingroup pcb_core_object
   @brief
   Extend the object using a MIB

   @details
   Extend the object using a MIB\n
   This is only allowed on local objects.

   @param object pointer to the object
   @param mibName name of the mib to use

   @return
    - false in case of error
 */
bool object_addMib(object_t* object, const char* mibName) {
    if(!object || !mibName || !(*mibName)) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(object_isRemote(object)) {
        pcb_error = pcb_error_remote_local_mismatch;
        return false;
    }

    return local_object_addMib(object, mibName);
}

/**
   @ingroup pcb_core_object
   @brief
   Remove MIB memberd from an object

   @details
   object cahce\n

   @param object pointer to the object
   @param mibName name of the MIB to remove

   @return
    - false on error
 */
bool object_removeMib(object_t* object, const char* mibName) {
    if(!object || !mibName || !(*mibName)) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    object_t* mib = datamodel_mib(object_datamodel(object), mibName);
    if(!mib) {
        pcb_error = pcb_error_not_found;
        return false;
    }

    parameter_t* mib_parameter = NULL;
    parameter_t* parameter = NULL;
    object_for_each_parameter(mib_parameter, mib) {
        parameter = object_getParameter(object, parameter_name(mib_parameter));
        if(parameter) {
            parameter_destroy(parameter);
        }
    }

    function_t* mib_function = NULL;
    function_t* function = NULL;
    object_for_each_function(mib_function, mib) {
        function = object_getFunction(object, function_name(mib_function));
        if(function) {
            function_delete(function);
        }
    }

    object_t* mib_object = NULL;
    object_t* subobject = NULL;
    object_for_each_child(mib_object, mib) {
        subobject = object_getObjectByKey(object, object_name(mib_object, path_attr_key_notation));
        if(subobject) {
            object_destroy_recursive(subobject);
        }
    }

    string_list_iterator_t* it = NULL;
    string_list_for_each(it, &object->mibs) {
        if(string_compareChar(string_list_iterator_data(it), mibName, string_case_sensitive) == 0) {
            break;
        }
    }

    if(it) {
        string_list_iterator_take(it);
        string_list_iterator_destroy(it);
    }

    if(object->handlers && object->handlers->mib_deleted) {
        object->handlers->mib_deleted(object, mib);
    }

    return true;
}

bool object_hasMib(object_t* object, const char* mibName) {
    if(!object) {
        return false;
    }

    if(!mibName || !(*mibName)) {
        return false;
    }

    return string_list_containsChar(&object->mibs, mibName);
}

const string_list_t* object_mibs(object_t* object) {
    if(!object) {
        return NULL;
    }

    return &object->mibs;
}

/**
   @ingroup pcb_core_object
   @brief
   Get the value of a parameter of an object

   @details
   Get the value of a parameter of an object as a variant.
   Do not delete the variant pointer returned\n
   For remote objects, if the parameter is not found in the cache a request is send to get the value\n

   @param object pointer to the object
   @param parameterName name of the parameter

   @return
    - The variant representing the value of the parameter
    - NULL if no parameter is found with the given name
 */
const variant_t* object_parameterValue(const object_t* object, const char* parameterName) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    parameter_t* parameter = object_getParameter(object, parameterName);
    if(!parameter) {
        return NULL;
    }

    return parameter_getValue(parameter);
}

/**
   @ingroup pcb_core_object
   @brief
   Set the value of a parameter of an object

   @details
   Set the value of a parameter of an object. If the given variant value does not match
   the parameter type, the value will be converted.\n
   Not all variant types are allowed in this function. The following types will fail:
    - variant_type_array
    - variant_type_map
    - variant_type_double
   \n
   For remote objects, a request is created and sent to set the value\n

   @param object pointer to the object
   @param parameterName name of the parameter
   @param value the new value

   @return
    - true the new value is set.
    - false failed to set the new value
 */
bool object_parameterSetValue(object_t* object, const char* parameterName, const variant_t* value) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if((variant_type(value) == variant_type_array) ||
       (variant_type(value) == variant_type_map) ||
       (variant_type(value) == variant_type_double)) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    parameter_t* parameter = object_getParameter(object, parameterName);
    if(!parameter) {
        return false;
    }

    return parameter_setValue(parameter, value);
}

/**
   @ingroup pcb_core_object
   @brief
   Set the value of a parameter of an object

   @details
   Set the value of a parameter of an object. If the parameter
   is not of the int8 type the value is converted.\n
   For remote objects, a request is created and sent to set the value\n

   @param object pointer to the object
   @param parameterName name of the parameter
   @param value the new value

   @return
    - true the new value is set.
    - false failed to set the new value
 */
bool object_parameterSetInt8Value(object_t* object, const char* parameterName, int8_t value) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    parameter_t* parameter = object_getParameter(object, parameterName);
    if(!parameter) {
        return false;
    }

    variant_t var;
    variant_initialize(&var, variant_type_unknown);
    if(!variant_setInt8(&var, value)) {
        return false;
    }

    return parameter_setValue(parameter, &var);
}

/**
   @ingroup pcb_core_object
   @brief
   Set the value of a parameter of an object

   @details
   Set the value of a parameter of an object. If the parameter
   is not of the int16 type the value is converted.\n
   For remote objects, a request is created and sent to set the value\n

   @param object pointer to the object
   @param parameterName name of the parameter
   @param value the new value

   @return
    - true the new value is set.
    - false failed to set the new value
 */
bool object_parameterSetInt16Value(object_t* object, const char* parameterName, int16_t value) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    parameter_t* parameter = object_getParameter(object, parameterName);
    if(!parameter) {
        return false;
    }

    variant_t var;
    variant_initialize(&var, variant_type_unknown);
    if(!variant_setInt16(&var, value)) {
        return false;
    }

    return parameter_setValue(parameter, &var);
}

/**
   @ingroup pcb_core_object
   @brief
   Set the value of a parameter of an object

   @details
   Set the value of a parameter of an object. If the parameter
   is not of the int32 type the value is converted.\n
   For remote objects, a request is created and sent to set the value\n

   @param object pointer to the object
   @param parameterName name of the parameter
   @param value the new value

   @return
    - true the new value is set.
    - false failed to set the new value
 */
bool object_parameterSetInt32Value(object_t* object, const char* parameterName, int32_t value) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    parameter_t* parameter = object_getParameter(object, parameterName);
    if(!parameter) {
        return false;
    }

    variant_t var;
    variant_initialize(&var, variant_type_unknown);
    if(!variant_setInt32(&var, value)) {
        return false;
    }

    return parameter_setValue(parameter, &var);
}

/**
   @ingroup pcb_core_object
   @brief
   Set the value of a parameter of an object

   @details
   Set the value of a parameter of an object. If the parameter
   is not of the int64 type the value is converted.\n
   For remote objects, a request is created and sent to set the value\n

   @param object pointer to the object
   @param parameterName name of the parameter
   @param value the new value

   @return
    - true the new value is set.
    - false failed to set the new value
 */
bool object_parameterSetInt64Value(object_t* object, const char* parameterName, int64_t value) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    parameter_t* parameter = object_getParameter(object, parameterName);
    if(!parameter) {
        return false;
    }

    variant_t var;
    variant_initialize(&var, variant_type_unknown);
    if(!variant_setInt64(&var, value)) {
        return false;
    }

    return parameter_setValue(parameter, &var);
}

/**
   @ingroup pcb_core_object
   @brief
   Set the value of a parameter of an object

   @details
   Set the value of a parameter of an object. If the parameter
   is not of the uint8 type the value is converted.\n
   For remote objects, a request is created and sent to set the value\n

   @param object pointer to the object
   @param parameterName name of the parameter
   @param value the new value

   @return
    - true the new value is set.
    - false failed to set the new value
 */
bool object_parameterSetUInt8Value(object_t* object, const char* parameterName, uint8_t value) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    parameter_t* parameter = object_getParameter(object, parameterName);
    if(!parameter) {
        return false;
    }

    variant_t var;
    variant_initialize(&var, variant_type_unknown);
    if(!variant_setUInt8(&var, value)) {
        return false;
    }

    return parameter_setValue(parameter, &var);
}

/**
   @ingroup pcb_core_object
   @brief
   Set the value of a parameter of an object

   @details
   Set the value of a parameter of an object. If the parameter
   is not of the uint16 type the value is converted.\n
   For remote objects, a request is created and sent to set the value\n

   @param object pointer to the object
   @param parameterName name of the parameter
   @param value the new value

   @return
    - true the new value is set.
    - false failed to set the new value
 */
bool object_parameterSetUInt16Value(object_t* object, const char* parameterName, uint16_t value) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    parameter_t* parameter = object_getParameter(object, parameterName);
    if(!parameter) {
        return false;
    }

    variant_t var;
    variant_initialize(&var, variant_type_unknown);
    if(!variant_setUInt16(&var, value)) {
        return false;
    }

    return parameter_setValue(parameter, &var);
}

/**
   @ingroup pcb_core_object
   @brief
   Set the value of a parameter of an object

   @details
   Set the value of a parameter of an object. If the parameter
   is not of the uint32 type the value is converted.\n
   For remote objects, a request is created and sent to set the value\n

   @param object pointer to the object
   @param parameterName name of the parameter
   @param value the new value

   @return
    - true the new value is set.
    - false failed to set the new value
 */
bool object_parameterSetUInt32Value(object_t* object, const char* parameterName, uint32_t value) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    parameter_t* parameter = object_getParameter(object, parameterName);
    if(!parameter) {
        return false;
    }

    variant_t var;
    variant_initialize(&var, variant_type_unknown);
    if(!variant_setUInt32(&var, value)) {
        return false;
    }

    return parameter_setValue(parameter, &var);
}

/**
   @ingroup pcb_core_object
   @brief
   Set the value of a parameter of an object

   @details
   Set the value of a parameter of an object. If the parameter
   is not of the uint64 type the value is converted.\n
   For remote objects, a request is created and sent to set the value\n

   @param object pointer to the object
   @param parameterName name of the parameter
   @param value the new value

   @return
    - true the new value is set.
    - false failed to set the new value
 */
bool object_parameterSetUInt64Value(object_t* object, const char* parameterName, uint64_t value) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    parameter_t* parameter = object_getParameter(object, parameterName);
    if(!parameter) {
        return false;
    }

    variant_t var;
    variant_initialize(&var, variant_type_unknown);
    if(!variant_setUInt64(&var, value)) {
        return false;
    }

    return parameter_setValue(parameter, &var);
}

/**
   @ingroup pcb_core_object
   @brief
   Set the value of a parameter of an object

   @details
   Set the value of a parameter of an object. If the parameter
   is not of the string type the value is converted.\n
   For remote objects, a request is created and sent to set the value\n

   @param object pointer to the object
   @param parameterName name of the parameter
   @param value the new value

   @return
    - true the new value is set.
    - false failed to set the new value
 */
bool object_parameterSetStringValue(object_t* object, const char* parameterName, string_t* value) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    parameter_t* parameter = object_getParameter(object, parameterName);
    if(!parameter) {
        return false;
    }

    return parameter_setFromString(parameter, value);
}

/**
   @ingroup pcb_core_object
   @brief
   Set the value of a parameter of an object

   @details
   Set the value of a parameter of an object. If the parameter
   is not of the string type the value is converted.\n
   For remote objects, a request is created and sent to set the value\n

   @param object pointer to the object
   @param parameterName name of the parameter
   @param value the new value

   @return
    - true the new value is set.
    - false failed to set the new value
 */
bool object_parameterSetCharValue(object_t* object, const char* parameterName, const char* value) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    parameter_t* parameter = object_getParameter(object, parameterName);
    if(!parameter) {
        return false;
    }

    return parameter_setFromChar(parameter, value);
}

/**
   @ingroup pcb_core_object
   @brief
   Set the value of a parameter of an object

   @details
   Set the value of a parameter of an object. If the parameter
   is not of the bool type the value is converted.\n
   For remote objects, a request is created and sent to set the value\n

   @param object pointer to the object
   @param parameterName name of the parameter
   @param value the new value

   @return
    - true the new value is set.
    - false failed to set the new value
 */
bool object_parameterSetBoolValue(object_t* object, const char* parameterName, bool value) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    parameter_t* parameter = object_getParameter(object, parameterName);
    if(!parameter) {
        return false;
    }

    variant_t var;
    variant_initialize(&var, variant_type_unknown);
    if(!variant_setBool(&var, value)) {
        return false;
    }

    return parameter_setValue(parameter, &var);
}

/**
   @ingroup pcb_core_object
   @brief
   Set the value of a parameter of an object

   @details
   Set the value of a parameter of an object. If the parameter
   is not of the double type the value is converted.\n
   For remote objects, a request is created and sent to set the value\n

   @param object pointer to the object
   @param parameterName name of the parameter
   @param value the new value

   @return
    - true the new value is set.
    - false failed to set the new value
 */
bool object_parameterSetDoubleValue(object_t* object, const char* parameterName, double value) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    parameter_t* parameter = object_getParameter(object, parameterName);
    if(!parameter) {
        return false;
    }

    variant_t var;
    variant_initialize(&var, variant_type_unknown);
    if(!variant_setDouble(&var, value)) {
        return false;
    }

    return parameter_setValue(parameter, &var);
}

/**
   @ingroup pcb_core_object
   @brief
   Set the value of a parameter of an object

   @details
   Set the value of a parameter of an object. If the parameter
   is not of the datetime type the value is converted.\n
   For remote objects, a request is created and sent to set the value\n

   @param object pointer to the object
   @param parameterName name of the parameter
   @param value the new value

   @return
    - true the new value is set.
    - false failed to set the new value
 */
bool object_parameterSetDateTimeValue(object_t* object, const char* parameterName, const struct tm* value) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    parameter_t* parameter = object_getParameter(object, parameterName);
    if(!parameter) {
        return false;
    }

    variant_t var;
    variant_initialize(&var, variant_type_unknown);
    if(!variant_setDateTime(&var, value)) {
        return false;
    }

    bool retval = parameter_setValue(parameter, &var);
    variant_cleanup(&var);

    return retval;
}

/**
   @ingroup pcb_core_object
   @brief
   Set the value of a parameter of an object

   @details
   Set the value of a parameter of an object. If the parameter
   is not of the datetime type the value is converted.\n
   For remote objects, a request is created and sent to set the value\n

   @param object pointer to the object
   @param parameterName name of the parameter
   @param value the new value

   @return
    - true the new value is set.
    - false failed to set the new value
 */
bool object_parameterSetDateTimeExtendedValue(object_t* object, const char* parameterName, const pcb_datetime_t* value) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    parameter_t* parameter = object_getParameter(object, parameterName);
    if(!parameter) {
        return false;
    }

    variant_t var;
    variant_initialize(&var, variant_type_unknown);
    if(!variant_setDateTimeExtended(&var, value)) {
        return false;
    }

    bool retval = parameter_setValue(parameter, &var);
    variant_cleanup(&var);

    return retval;
}

/**
   @ingroup pcb_core_object
   @brief
   Check that the given userid can read the object's parameter

   @details
   Checks the ACL to see that the given userid can read the object's parameter

   @param object pointer to an object
   @param parameterName the object's parameter name
   @param uid user id

   @return
    - true: the user can read
    - false: the user is not allowed to read
 */
bool object_parameterCanRead(object_t* object, const char* parameterName, uint32_t uid) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    parameter_t* parameter = object_getParameter(object, parameterName);
    if(!parameter) {
        return false;
    }

    return parameter_canRead(parameter, uid);
}

/**
   @ingroup pcb_core_object
   @brief
   Check that the given userid can write the object's parameter

   @details
   Checks the ACL to see that the given userid can write the object's parameter

   @param object pointer to an object
   @param parameterName the object's parameter name
   @param uid user id

   @return
    - true: the user can write
    - false: the user is not allowed to write
 */
bool object_parameterCanWrite(object_t* object, const char* parameterName, uint32_t uid) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    parameter_t* parameter = object_getParameter(object, parameterName);
    if(!parameter) {
        return false;
    }

    return parameter_canWrite(parameter, uid);
}

/**
   @ingroup pcb_core_object
   @brief
   Check that the given userid can read the object's parameter value

   @details
   Checks the ACL to see that the given userid can read the object's parameter value

   @param object pointer to an object
   @param parameterName the object's parameter name
   @param uid user id

   @return
    - true: the user can read the parameter value
    - false: the user is not allowed to read the parameter value
 */
bool object_parameterCanReadValue(object_t* object, const char* parameterName, uint32_t uid) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    parameter_t* parameter = object_getParameter(object, parameterName);
    if(!parameter) {
        return false;
    }

    return parameter_canReadValue(parameter, uid);
}

/**
   @ingroup pcb_core_object
   @brief
   Get parameters of an object using a variant map

   @details
   Get the values of all parameter of an object using a variant map. The given variant map pointer must be valid.
   This function will initialize the variant map.

   @param object pointer to the object
   @param values a variant map

   @return
    - true when successful
    - false otherwhise
 */
bool object_getValues(object_t* object, variant_map_t* values) {
    if(!variant_map_initialize(values)) {
        return false;
    }

    parameter_t* p;
    object_for_each_parameter(p, object) {
        if(!variant_map_add(values, parameter_name(p), parameter_getValue(p))) {
            variant_map_cleanup(values);
            return false;
        }
    }

    return true;
}

/**
   @ingroup pcb_core_object
   @brief
   Get parameters of an object using a variant map, checking user access

   @details
   Get the values of all parameter of an object using a variant map.
   The function will only add parameter for which the user has read access to the parameter.
   The given variant map pointer must be valid.
   This function will initialize the variant map.

   @param object pointer to the object
   @param values a variant map
   @param uid a user id

   @return
    - true when successful
    - false otherwhise
 */
bool object_getValuesWithACL(object_t* object, variant_map_t* values, uint32_t uid) {
    if(!variant_map_initialize(values)) {
        return false;
    }

    parameter_t* p;
    object_for_each_parameter(p, object) {
        if(!parameter_canRead(p, uid) || !parameter_canReadValue(p, uid)) {
            continue;
        }

        if(!variant_map_add(values, parameter_name(p), parameter_getValue(p))) {
            variant_map_cleanup(values);
            return false;
        }
    }

    return true;
}

/**
   @ingroup pcb_core_object
   @brief
   Get parameters of an object using a variant map and convert
   the parameter names to start with a lower case.

   @details
   Get the values of all parameter of an object using a variant map.
   The given variant map pointer must be valid.

   @param object pointer to the object
   @param values an initialized variant map

   @return
    - true when successful
    - false otherwhise
 */
bool object_getLowerCaseValues(object_t* object, variant_map_t* values) {
    parameter_t* p;
    string_t name;
    string_initialize(&name, 256);
    object_for_each_parameter(p, object) {
        const char* c;
        for(c = parameter_name(p); *c && isupper(*c); c++) {
            string_appendFormat(&name, "%c", tolower(*c));
        }
        if(*c) {
            string_appendFormat(&name, "%s", c);
        }
        if(!variant_map_add(values, string_buffer(&name), parameter_getValue(p))) {
            string_cleanup(&name);
            return false;
        }
        string_clear(&name);
    }
    string_cleanup(&name);
    return true;
}

/**
   @ingroup pcb_core_object
   @brief
   Get readable parameters of an object using a variant map and convert
   the parameter names to start with a lower case.

   @details
   Get the values of all parameter of an object using a variant map.
   The function will only add parameter for which the user has read access to the parameter.
   The given variant map pointer must be valid.

   @param object pointer to the object
   @param values an initialized variant map
   @param uid a user id

   @return
    - true when successful
    - false otherwhise
 */
bool object_getLowerCaseValuesWithACL(object_t* object, variant_map_t* values, uint32_t uid) {
    parameter_t* p;
    string_t name;
    string_initialize(&name, 256);
    object_for_each_parameter(p, object) {
        if(!parameter_canRead(p, uid) || !parameter_canReadValue(p, uid)) {
            continue;
        }

        const char* c;
        for(c = parameter_name(p); *c && isupper(*c); c++) {
            string_appendFormat(&name, "%c", tolower(*c));
        }
        if(*c) {
            string_appendFormat(&name, "%s", c);
        }
        if(!variant_map_add(values, string_buffer(&name), parameter_getValue(p))) {
            string_cleanup(&name);
            return false;
        }
        string_clear(&name);
    }
    string_cleanup(&name);
    return true;
}

/**
   @ingroup pcb_core_object
   @brief
   Sets parameters of an object using a variant map

   @details
   Sets the values of an object using a variant map. The variant map should contain for each parameter
   you want to set the value of an entry. This entry must contain the parameter name as the key and
   the new value as the value.
   If one of the sets fails, all sets are considered invalid and no changes are applied.\n
   For remote objects, a request is created and sent to set the value\n

   @param object pointer to the object
   @param values a variant map containing all new values

   @return
    - true the new values are set.
    - false failed to set the new values
 */
bool object_setValues(object_t* object, const variant_map_t* values) {
    if(!object || !values) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    bool retval = true;
    const char* paramName = NULL;
    variant_t* data = NULL;
    parameter_t* parameter = NULL;
    variant_map_iterator_t* it = NULL;
    variant_map_for_each(it, values) {
        paramName = variant_map_iterator_key(it);
        data = variant_map_iterator_data(it);
        parameter = object_getParameter(object, paramName);
        if(!parameter) {
            retval = false;
            break;
        }
        if(!parameter_setValue(parameter, data)) {
            retval = false;
            break;
        }
    }

    if(retval) {
        return true;
    }

    variant_map_for_each(it, values) {
        paramName = variant_map_iterator_key(it);
        parameter = object_getParameter(object, paramName);
        if(!parameter) {
            break;
        }
        parameter_rollback(parameter);
    }

    return false;
}

/**
   @ingroup pcb_core_object
   @brief
   Sets writable parameters of an object using a variant map

   @details
   Sets the values of an object using a variant map. The variant map should contain for each parameter
   you want to set the value of an entry. This entry must contain the parameter name as the key and
   the new value as the value.
   This function checks that the user has write access to the parameter.
   If one of the sets fails, all sets are considered invalid and no changes are applied.\n
   For remote objects, a request is created and sent to set the value\n

   @param object pointer to the object
   @param values a variant map containing all new values
   @param uid a user id

   @return
    - true the new values are set.
    - false failed to set the new values
 */
bool object_setValuesWithACL(object_t* object, const variant_map_t* values, uint32_t uid) {
    if(!object || !values) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    bool retval = true;
    const char* paramName = NULL;
    variant_t* data = NULL;
    parameter_t* parameter = NULL;
    variant_map_iterator_t* it = NULL;
    variant_map_for_each(it, values) {
        paramName = variant_map_iterator_key(it);
        data = variant_map_iterator_data(it);
        parameter = object_getParameter(object, paramName);
        if(!parameter) {
            retval = false;
            break;
        }

        if(!parameter_canWrite(parameter, uid)) {
            retval = false;
            break;
        }

        if(!parameter_setValue(parameter, data)) {
            retval = false;
            break;
        }
    }

    if(retval) {
        return true;
    }

    variant_map_for_each(it, values) {
        paramName = variant_map_iterator_key(it);
        parameter = object_getParameter(object, paramName);
        if(!parameter) {
            break;
        }
        parameter_rollback(parameter);
    }

    return false;
}

/**
   @ingroup pcb_core_object
   @brief
   Sets parameters of an object using a variant map

   @details
   Sets the values of an object using a variant map. The variant map should contain for each parameter
   you want to set the value of an entry. This entry must contain the parameter name as the key and
   the new value as the value.
   The first letter of the parameter name will be uppercased.
   If one of the sets fails, all sets are considered invalid and no changes are applied.\n
   For remote objects, a request is created and sent to set the value\n

   @param object pointer to the object
   @param values a variant map containing all new values

   @return
    - true the new values are set.
    - false failed to set the new values
 */
bool object_setLowerCaseValues(object_t* object, const variant_map_t* values) {
    if(!object || !values) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }


    bool retval = true;
    variant_t* data = NULL;
    parameter_t* parameter = NULL;
    variant_map_iterator_t* it = NULL;
    const char* paramName = NULL;
    variant_map_for_each(it, values) {
        paramName = variant_map_iterator_key(it);
        data = variant_map_iterator_data(it);

        object_for_each_parameter(parameter, object) {
            if(strcasecmp(paramName, parameter_name(parameter)) == 0) {
                if(!parameter_setValue(parameter, data)) {
                    retval = false;
                }
                break;
            }
        }
        if(!retval) {
            break;
        }
    }

    if(retval) {
        return true;
    }

    variant_map_for_each(it, values) {
        paramName = variant_map_iterator_key(it);

        object_for_each_parameter(parameter, object) {
            if(strcasecmp(paramName, parameter_name(parameter)) == 0) {
                break;
            }
        }
        if(parameter) {
            parameter_rollback(parameter);
        }
    }

    return false;
}

/**
   @ingroup pcb_core_object
   @brief
   Sets parameters of an object using a variant map

   @details
   Sets the values of an object using a variant map. The variant map should contain for each parameter
   you want to set the value of an entry. This entry must contain the parameter name as the key and
   the new value as the value.
   The first letter of the parameter name will be uppercased.
   This function checks that the user has write access to the parameter.
   If one of the sets fails, all sets are considered invalid and no changes are applied.\n
   For remote objects, a request is created and sent to set the value\n

   @param object pointer to the object
   @param values a variant map containing all new values
   @param uid a user id

   @return
    - true the new values are set.
    - false failed to set the new values
 */
bool object_setLowerCaseValuesWithACL(object_t* object, const variant_map_t* values, uint32_t uid) {
    if(!object || !values) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }


    bool retval = true;
    variant_t* data = NULL;
    parameter_t* parameter = NULL;
    variant_map_iterator_t* it = NULL;
    const char* paramName = NULL;
    variant_map_for_each(it, values) {
        paramName = variant_map_iterator_key(it);
        data = variant_map_iterator_data(it);

        object_for_each_parameter(parameter, object) {
            if(!parameter_canWrite(parameter, uid)) {
                retval = false;
                break;
            }

            if(strcasecmp(paramName, parameter_name(parameter)) == 0) {
                if(!parameter_setValue(parameter, data)) {
                    retval = false;
                }
                break;
            }
        }
        if(!retval) {
            break;
        }
    }

    if(retval) {
        return true;
    }

    variant_map_for_each(it, values) {
        paramName = variant_map_iterator_key(it);

        object_for_each_parameter(parameter, object) {
            if(strcasecmp(paramName, parameter_name(parameter)) == 0) {
                break;
            }
        }
        if(parameter) {
            parameter_rollback(parameter);
        }
    }

    return false;
}

static void mib_to_variant_map(object_t* object, object_t* mib, variant_map_t* values, bool checkAcl, uint32_t uid) {
    parameter_t* mib_param = NULL;
    parameter_t* param = NULL;
    object_for_each_parameter(mib_param, mib) {
        param = object_getParameter(object, parameter_name(mib_param));
        if(checkAcl) {
            if(!parameter_canRead(param, uid) || !parameter_canReadValue(param, uid)) {
                continue;
            }
        }
        variant_map_add(values, parameter_name(mib_param), parameter_getValue(param));
    }

    /* mib child parameters */
    object_t* mib_child = NULL;
    object_t* subobject = NULL;
    object_for_each_child(mib_child, mib) {
        subobject = object_getObjectByKey(object, object_name(mib_child, path_attr_key_notation));
        if(!subobject) {
            /* subobject not found */
            continue;
        }
        if(!object_isTemplate(mib_child)) {
            /* single instance object, just take the parameters */
            variant_map_iterator_t* it = variant_map_iterator_createMapMove(object_name(mib_child, path_attr_key_notation), NULL);
            mib_to_variant_map(subobject, mib_child, variant_da_map(variant_map_iterator_data(it)), checkAcl, uid);
            variant_map_append(values, it);
        } else {
            /* is a template object, only add the instances */
            variant_map_iterator_t* it = variant_map_iterator_createListMove(object_name(mib_child, path_attr_key_notation), NULL);
            variant_list_t* list = variant_da_list(variant_map_iterator_data(it));
            object_t* instance = NULL;
            object_for_each_instance(instance, subobject) {
                variant_list_iterator_t* lit = variant_list_iterator_createMapMove(NULL);
                mib_to_variant_map(instance, mib_child, variant_da_map(variant_list_iterator_data(lit)), checkAcl, uid);
                variant_list_append(list, lit);
            }
            variant_map_append(values, it);
        }
    }
}

bool object_getMibValues(object_t* object, const char* mibName, variant_map_t* values) {
    if(!string_list_containsChar(&object->mibs, mibName)) {
        return false;
    }

    object_t* mib = datamodel_mib(object_datamodel(object), mibName);
    if(!mib) {
        return false;
    }

    /* convert full mib to a variant map for the given object */
    mib_to_variant_map(object, mib, values, false, 0);

    return true;
}

bool object_getMibValuesWithACL(object_t* object, const char* mibName, variant_map_t* values, uint32_t uid) {
    if(!string_list_containsChar(&object->mibs, mibName)) {
        return false;
    }

    object_t* mib = datamodel_mib(object_datamodel(object), mibName);
    if(!mib) {
        return false;
    }

    /* convert full mib to a variant map for the given object */
    mib_to_variant_map(object, mib, values, true, uid);

    return true;
}

/**
   @ingroup pcb_core_object
   @brief
   Update the object

   @details
   If the object has a read handler function, this function is called. If the read handler is changing the object
   it will be committed. An object can only be updated if it is in the ready state.\n
   If the object is not in the ready state, this function returns false.\n
   The read handler of this object is called, if it fails, the object is rollbacked.\n
   Each parameter is updated, if the update fails, the object is rollbacked. \n
   If after calling the read handler functions the object is not in the ready state, it is validated and committed if validation
   was successful, otherwise a rollback is executed.\n
   If the object is a remote object, a synchronous request will be build to get the object again. This request will update the local
   object cache.

   @param object pointer to an object

   @return
    - true: the operation was successful.
    - false: the read failed
        - the object is a NULL pointer
        - the read handler failed (see reason code)
        - the synchronous request failed
 */
bool object_update(object_t* object) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }
    if(object->state != object_state_ready) {
        pcb_error = pcb_error_wrong_state;
        return false;
    }

    if(object_isRemote(object)) {
        return false;
    }

    return local_object_update(object, NULL);
}

/**
   @ingroup pcb_core_object
   @brief
   Commit changes to the object

   @details
   Any changes to the specified object, any of it's children or instances will be committed.
   Before applying the changes all changed objects will be validated (if this was not done before).
   If validation is successfull the changes will be applied, otherwise the functin returns false and
   no changes are applied (they are not rollbacked either)\n
   If the object is a remote object synchronous requests are created after local validation (only standard
   validation rules can be transferred, custom validation rules are never transferred. So it is possible that
   client side validation is successfull while server side validation fails).
   The commit for remote objects is not yet implemented.

   @param object pointer to an object

   @return
    - true: the operation was successful.
    - false: the commit failed
        - the object is a NULL pointer
        - validation failed, see reason code
        - the synchronous request failed
 */
bool object_commit(object_t* object) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(object_isRemote(object)) {
        return false;
    }

    static int recursive_call = 0;
    /* debugging code, just to detect possible errors in plug-in code */
    recursive_call++;
    if(recursive_call > 5) {
        string_t path;
        string_initialize(&path, 64);
        object_path(object, &path, path_attr_key_notation);
        /* do not use a zone here */
        SAH_TRACE_NOTICE("******************************************");
        SAH_TRACE_NOTICE("**** RECURSIVE OBJECT COMMIT DETECTED ****");
        SAH_TRACE_NOTICE("     This could lead to infinite recursion");
        SAH_TRACE_NOTICE("     Is this the intention ?");
        SAH_TRACE_NOTICE("     Object path = %s", string_buffer(&path));
        SAH_TRACE_NOTICE("     Object name = %s", object_name(object, path_attr_key_notation));
        SAH_TRACE_NOTICE("     Recurse depth = %d", recursive_call);
        SAH_TRACE_NOTICE("******************************************");
        string_cleanup(&path);
    }
    if(recursive_call > 100) {
        string_t path;
        string_initialize(&path, 64);
        object_path(object, &path, path_attr_key_notation);
        /* do not use a zone here */
        SAH_TRACE_ERROR("******************************************");
        SAH_TRACE_ERROR("**** RECURSIVE OBJECT COMMIT DETECTED ****");
        SAH_TRACE_ERROR("****        RECURSE DEPTH > 100       ****");
        SAH_TRACE_ERROR("****           FAILING COMMIT         ****");
        SAH_TRACE_ERROR("******************************************");
        SAH_TRACE_ERROR("     Object path = %s", string_buffer(&path));
        SAH_TRACE_ERROR("     Object name = %s", object_name(object, path_attr_key_notation));
        SAH_TRACE_ERROR("     Recurse depth = %d", recursive_call);
        string_cleanup(&path);
        recursive_call--;
        return false;
    }
    bool retval = local_object_commit(object);
    recursive_call--;
    return retval;
}

/**
   @ingroup pcb_core_object
   @brief
   Rollback changes to the object

   @details
   Any changes to the specified object, any of it's children or instances will be reverted.

   @param object pointer to an object
 */
void object_rollback(object_t* object) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return;
    }

    if((object->state == object_state_ready) && ((object->attributes & object_attr_subtree_changed) != object_attr_subtree_changed)) {
        return;
    }

    return local_object_rollback(object);
}

/**
   @ingroup pcb_core_object
   @brief
   Validate changes to the object

   @details
   Any changes to the specified object, any of it's children or instances will be validated\n
   If the object is a remote object,it is possible that validation is only partially done.
   Only standard validation rules can be transferred. Custom validation rules can only be done at server side.

   @param object pointer to an object

   @return
    - true: the operation was successful.
    - false: the validation failed
        - the object is a NULL pointer
        - validation failed, see reason code
 */
bool object_validate(object_t* object) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    /* only validate created objects and modified objects
       deleted, validated and ready objects do not need to be validated
     */
    if((object->state != object_state_modified) &&
       (object->state != object_state_created) &&
       ((object->attributes & object_attr_subtree_changed) != object_attr_subtree_changed)) {
        return true;
    }

    /* use the local validation function, if the object has to be validate exactly, only the
       owner (plugin) of that object can do that.
     */
    return local_object_validate(object);
}

/**
   @ingroup pcb_core_object
   @brief
   Check that the object contains an access control list

   @details
   This function will check that there is an access control list attached to the object.

   @param object pointer to an object

   @return
    - true an ACL is available
    - false no acl is available
 */
bool object_hasAcl(object_t* object) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    return !llist_isEmpty(&object->ACL);
}

/**
   @ingroup pcb_core_object
   @brief
   Get the access control list from an object

   @details
   This function will get the access control list of an object.

   @param object pointer to a parameter

   @return
    - linked list containing the access control of the object.
    - This list must not be modified or deleted
 */
const llist_t* object_getACL(const object_t* object) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    return &object->ACL;
}

/**
   @ingroup pcb_core_object
   @brief
   Get the calculated access control list from an object

   @details
   This function will get the calculated access control list of an object.

   @param object pointer to an object
   @param ACL pointer to a linked list, this will be filled with the calculated access control information

   @return
    - linked list containing the access control of the object.
    - free the list using @ref acl_list_destroy
 */
void object_getCalculatedACL(const object_t* object, llist_t* ACL) {
    if(!object || !ACL) {
        pcb_error = pcb_error_invalid_parameter;
        return;
    }

    acl_list_destroy(ACL);
    object_acl(object, ACL);
}

/**
   @ingroup pcb_core_object
   @brief
   Apply an access control list on the object

   @details
   This function will apply an access control list on the object.

   @param object pointer to a parameter
   @param acl linked kist containing the access control information

   @return
    - true: the acl is updated.
    - false: failed to update the acl
 */
bool object_setACL(object_t* object, const llist_t* acl) {
    if(!object || !acl) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    ACL_t* a = NULL;
    llist_iterator_t* it = NULL;
    llist_for_each(it, acl) {
        a = llist_item_data(it, ACL_t, it);
        object_aclSet(object, a->id, a->acl_flags);
    }

    return true;
}

/**
   @ingroup pcb_core_object
   @brief
   Modify an access control of the object

   @details
   This function will modify an existing access control that was already added to the object.

   @param object pointer to a parameter
   @param id user or group id
   @param flags identifies that the id is a user or group id  and the access wanted

   @return
    - true: the acl is updated.
    - false: failed to update the acl
 */
bool object_aclSet(object_t* object, uint32_t id, uint16_t flags) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    return aclSet(&object->ACL, id, flags);
}

/**
   @ingroup pcb_core_object
   @brief
   Modify an access control of the object

   @details
   This function will modify an existing access control that was already added to the object.

   @param object pointer to a parameter
   @param id user or group id
   @param flags identifies that the id is a user or group id  and the access wanted

   @return
    - true: the acl is updated.
    - false: failed to update the acl
 */
bool object_aclAdd(object_t* object, uint32_t id, uint16_t flags) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    llist_t ACL;
    llist_initialize(&ACL);
    object_acl(object, &ACL);
    ACL_t* acl = aclFind(&ACL, id, flags);
    if(acl) {
        flags |= acl->acl_flags;
    }
    acl_list_destroy(&ACL);
    return aclSet(&object->ACL, id, flags);
}

/**
   @ingroup pcb_core_object
   @brief
   Modify an access control of the object

   @details
   This function will modify an existing access control that was already added to the object.

   @param object pointer to a parameter
   @param id user or group id
   @param flags identifies that the id is a user or group id  and the access wanted

   @return
    - true: the acl is updated.
    - false: failed to update the acl
 */
bool object_aclDel(object_t* object, uint32_t id, uint16_t flags) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    llist_t ACL;
    llist_initialize(&ACL);
    object_acl(object, &ACL);

    bool group = false;
    if(flags & acl_group_id) {
        group = true;
    }

    ACL_t* acl = aclFind(&ACL, id, flags);
    if(acl) {
        flags = acl->acl_flags & ~flags;
    } else {
        flags = 0;
    }
    if(group) {
        flags |= acl_group_id;
    }
    acl_list_destroy(&ACL);

    return aclSet(&object->ACL, id, flags);
}

/**
   @ingroup pcb_core_object
   @brief
   Check that the given userid can read the object

   @details
   Checks the ACL to see that the given userid can read the object

   @param object pointer to the object
   @param uid user id

   @return
    - true: the user can read
    - false: the user is not allowed to read
 */
bool object_canRead(object_t* object, uint32_t uid) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(object_isRemote(object) || (uid == 0)) {
        return true;
    }

    llist_t ACL;
    llist_initialize(&ACL);
    object_acl(object, &ACL);
    bool retval = aclCheck(&ACL, uid, acl_read);
    aclDestroy(&ACL);
    return retval;
}

/**
   @ingroup pcb_core_object
   @brief
   Check that the given userid can write the object

   @details
   Checks the ACL to see that the given userid can write the object

   @param object pointer to the object
   @param uid user id

   @return
    - true: the user can write
    - false: the user is not allowed to write
 */
bool object_canWrite(object_t* object, uint32_t uid) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(object_isRemote(object) || (uid == 0)) {
        return true;
    }

    llist_t ACL;
    llist_initialize(&ACL);
    object_acl(object, &ACL);
    bool retval = aclCheck(&ACL, uid, acl_write);
    aclDestroy(&ACL);
    return retval;
}

/**
   @ingroup pcb_core_object
   @brief
   Check that the given userid can execute the object

   @details
   Checks the ACL to see that the given userid can execute the object

   @param object pointer to an object
   @param uid user id

   @return
    - true: the user can execute
    - false: the user is not allowed to execute
 */
bool object_canExecute(object_t* object, uint32_t uid) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(object_isRemote(object) || (uid == 0)) {
        return true;
    }

    llist_t ACL;
    llist_initialize(&ACL);
    object_acl(object, &ACL);
    bool retval = aclCheck(&ACL, uid, acl_execute);
    aclDestroy(&ACL);
    return retval;
}

/**
   @ingroup pcb_core_parameter
   @brief
   Set upc attributes of the object

   @details
   This function will modify the upc attributes of an object.

   @param object pointer to an object
   @param attributes the new set of upc attributes

   @return
    - true: the upc attributes are updated.
    - false: failed to update the upc attributes
 */
bool object_upcSet(object_t* object, uint32_t attributes) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(object_isRemote(object)) {
        pcb_error = pcb_error_remote_local_mismatch;
        return false;
    }

    attributes &= (object_attr_upc | object_attr_upc_usersetting | object_attr_upc_overwrite);

    /* Make sure to set as UPC and reboot persistent */
    if(attributes != 0) {
        attributes |= object_attr_upc | object_attr_persistent;
    }

    object->attributes |= attributes;
    return true;
}

/**
   @ingroup pcb_core_parameter
   @brief
   Clear upc attributes of the object

   @details
   This function will modify the upc attributes of an object.

   @param object pointer to an object
   @param attributes the set of upc attributes to clear

   @return
    - true: the upc attributes are updated.
    - false: failed to update the upc attributes
 */
bool object_upcClear(object_t* object, uint32_t attributes) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(object_isRemote(object)) {
        pcb_error = pcb_error_remote_local_mismatch;
        return false;
    }

    attributes &= (object_attr_upc | object_attr_upc_usersetting | object_attr_upc_overwrite);

    object->attributes &= ~attributes;
    return true;
}

/**
   @ingroup pcb_core_object
   @brief
   Returns the object's attributes.

   @details
   Returns the object's attributes.

   @param object pointer to an object

   @return
    - the object's attributes
 */
uint32_t object_attributes(const object_t* object) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return 0;
    }

    return object->attributes;
}

/**
   @ingroup pcb_core_object
   @brief
   Mark this object as read-only

   @details
   This function marks the object as read-only. Objects are alway readable for the owner, but can be made read-only
   for the rest of the world.\n
   Set or remove the object_attr_read_only to or from the object attributes.\n
   This can be done only on local objects.

   @param object pointer to an object
   @param enable set his to true to make the object read-only, to false to make it writable.
 */
void object_setReadOnly(object_t* object, const bool enable) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return;
    }
    if(object_isRemote(object)) {
        pcb_error = pcb_error_remote_local_mismatch;
        return;
    }
    if(enable) {
        object->attributes |= object_attr_read_only;
    } else {
        object->attributes &= ~object_attr_read_only;
    }
}

/**
   @ingroup pcb_core_object
   @brief
   Mark this object as persistent

   @details
   This function marks the object as persistent. This function can be used when creating an instance
   of a template object that is not persistent. The instance created will also not be persistent.\n
   Set or remove the object_attr_persistent to or from the object attributes.\n
   This can be done only on local objects.

   @param object pointer to an object
   @param enable set his to true to make the object persistent, to false to make it non persistent.
 */
void object_setPersistent(object_t* object, const bool enable) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return;
    }
    if(object_isRemote(object)) {
        pcb_error = pcb_error_remote_local_mismatch;
        return;
    }
    if(enable) {
        object->attributes |= object_attr_persistent;
    } else {
        object->attributes &= ~object_attr_persistent;
    }
}

/**
   @ingroup pcb_core_object
   @brief
   Mark this object as upc(Upgrade Persistent Configuration)

   @details
   This function marks the object as UPC.
   Set or remove the object_attr_upc to or from the object attributes.\n
   This can be done only on local objects.

   @param object pointer to an object
   @param enable set his to true to make the object UPC, to false to make it non-UPC.
 */
void object_setUPC(object_t* object, const bool enable) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return;
    }

    if(object_isRemote(object)) {
        pcb_error = pcb_error_remote_local_mismatch;
        return;
    }

    if(enable) {
        object->attributes |= object_attr_upc | object_attr_persistent;
    } else {
        object->attributes &= ~object_attr_upc;
    }
}

/**
   @ingroup pcb_core_object
   @brief
   Mark this object as usersetting

   @details
   This function marks the object as UserSetting,
   Set or remove the object_attr_usersetting to or from the object attributes.\n
   This can be done only on local objects. A UserSetting will be automatically
   marked as UPC.

   @param object pointer to an object
   @param enable set his to true to make the object UserSetting, to false unmark it
   as usersetting. (The UPC flag will not be removed in the latter case.)
 */
void object_setUserSetting(object_t* object, const bool enable) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return;
    }

    if(object_isRemote(object)) {
        pcb_error = pcb_error_remote_local_mismatch;
        return;
    }

    if(enable) {
        object->attributes |= object_attr_upc | object_attr_upc_usersetting | object_attr_persistent;
    } else {
        object->attributes &= ~object_attr_upc_usersetting;
    }
}

/**
   @ingroup pcb_core_object
   @brief
   Mark this object with the upc_overwrite attribute

   @details
   This function marks the object as UPC, and overwrite from the defaults.,
   Set or remove the object_attr_overwrite_upc to or from the object attributes.\n
   This can be done only on local objects.

   @param object pointer to an object
   @param enable set his to true to make the object UPC_Overwrite(Only possible if
   the object is marked UPC, to false unmark it as UPC_Overwrite.
 */
void object_setUPCOverwrite(object_t* object, const bool enable) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return;
    }

    if(object_isRemote(object)) {
        pcb_error = pcb_error_remote_local_mismatch;
        return;
    }

    if(enable) {
        if(object->attributes & object_attr_upc) {
            object->attributes |= object_attr_upc_overwrite;
        }
    } else {
        object->attributes &= ~object_attr_upc_overwrite;
    }
}

/**
   @ingroup pcb_core_object
   @brief
   Mark this object with the sysbus_root attribute

   @details
   This function marks the object as the sysbus root.

   @param object pointer to an object
   @param enable set this to true to make the object sysbus_root.
 */
void object_setSysbusRoot(object_t* object, const bool enable) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return;
    }

    if(object_isRemote(object)) {
        pcb_error = pcb_error_remote_local_mismatch;
        return;
    }

    if(enable) {
        object->attributes |= object_attr_sysbus_root;
    } else {
        object->attributes &= ~object_attr_sysbus_root;
    }
}

/**
   @ingroup pcb_core_object
   @brief
   Checks that the object is a read-only object

   @details
   This function checks that the object is a read-only object
   Checks the attributes member that it contains object_attr_accept_read_only

   @param object pointer to an object

   @return
    - true: the object is a read-only object
    - false:
        - the object is a NULL pointer
        - the object is not a read-only object
 */
bool object_isReadOnly(const object_t* object) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    return ((object->attributes & object_attr_read_only) == object_attr_read_only) ? true : false;
}

/**
   @ingroup pcb_core_object
   @brief
   Checks that the object is a template object

   @details
   This function checks that the object is a template object
   Checks the attributes member that it contains object_attr_accept_template

   @param object pointer to an object

   @return
    - true: the object is a template object
    - false:
        - the object is a NULL pointer
        - the object is not a template object
 */
bool object_isTemplate(const object_t* object) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    return ((object->attributes & object_attr_template) == object_attr_template) ? true : false;
}

/**
   @ingroup pcb_core_object
   @brief
   Checks that the object is an instance object

   @details
   This function checks that the object is an instance object

   @param object pointer to an object

   @return
    - true: the object is an instance object
    - false:
        - the object is a NULL pointer
        - the object is not an instance object
 */
bool object_isInstance(const object_t* object) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    return ((object->attributes & object_attr_instance) == object_attr_instance) ? true : false;
}

/**
   @ingroup pcb_core_object
   @brief
   Checks that the object is a remote object

   @details
   This function checks that the object is a remote object

   @param object pointer to an object

   @return
    - true: the object is a remote object
    - false:
        - the object is a NULL pointer
        - the object is not a remote object
 */
bool object_isRemote(const object_t* object) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    return ((object->attributes & object_attr_remote) == object_attr_remote) ? true : false;
}

/**
   @ingroup pcb_core_object
   @brief
   Checks that the object can accept unknown parameters

   @details
   This function checks that the object can accept unknown parameters
   Checks the attributes member that it contains object_attr_accept_parameters

   @param object pointer to an object

   @return
    - true: the object can accept unknown parameters
    - false:
        - the object is a NULL pointer
        - the object can not accept unknown parameters.
 */
bool object_canAcceptParameters(const object_t* object) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    return ((object->attributes & object_attr_accept_parameters) == object_attr_accept_parameters) ? true : false;
}

/**
   @ingroup pcb_core_object
   @brief
   Checks that the object is a MIB definition

   @details
   Checks that the object is a MIB definition
   Checks the attributes member that it contains object_attr_mib

   @param object pointer to an object

   @return
    - true: the object can accept unknown parameters
    - false:
        - the object is a NULL pointer
        - the object can not accept unknown parameters.
 */
bool object_isMib(const object_t* object) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    return ((object->attributes & object_attr_mib) == object_attr_mib) ? true : false;
}

/**
   @ingroup pcb_core_object
   @brief
   Sets the maximum instances of a template object

   @details
   This function set the maximum instances that can be created of a template object.

   @param object pointer to an object
   @param max the maximum number of instances use UINT32_MAX for unlimited instances

   @return
    - true: operation was successful
    - false:
        - the object is a NULL pointer
        - the object is not a template object
 */
bool object_setMaxInstances(object_t* object, const uint32_t max) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }
    if(object_isRemote(object)) {
        pcb_error = pcb_error_remote_local_mismatch;
        return false;
    }
    if((object->attributes & object_attr_template) != object_attr_template) {
        pcb_error = pcb_error_not_template;
        return false;
    }
    object->object_info.template_info->max_instances = max;
    return true;
}

/**
   @ingroup pcb_core_object
   @brief
   Gets the maximum number of instances for a template object

   @details
   This function gets the maximum number of instances that can be created for a template object.

   @param object pointer to an object

   @return
    - the maximum number of instances
    - UINT32_MAX unlimited number of instances
 */
uint32_t object_getMaxInstances(object_t* object) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return 0;
    }
    if(!(object->attributes & object_attr_template)) {
        pcb_error = pcb_error_not_template;
        return 0;
    }

    return object->object_info.template_info->max_instances;
}

/**
   @ingroup pcb_core_object
   @brief
   Gets the list of deleted instances

   @details
   This function gets the list of deleted instances.
   The type of deleted instances to get can be selected with the index parameter.
   The types persistent, upc and usersetting are supported.

   @param object pointer to an object
   @param index type of deleted instances to get

   @return
    - a pointer to a string_list_t (do not update)
    - NULL upon error
 */
const string_list_t* object_getDeletedInstances(object_t* object, deleted_instance_t index) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }
    if(!(object->attributes & object_attr_template)) {
        pcb_error = pcb_error_not_template;
        return NULL;
    }

    return &(object->object_info.template_info->deleted[index]);
}

/**
   @ingroup pcb_core_object
   @brief
   Sets a counter parameter for a multi-instance object.

   @details
   This function sets the counter parameter for a multi-instance object. The parameter will be automatically updated
   whenever an instance is added or deleted from the object. Events about updates will be sent, too.
   The parameter will be read-only.\n
   The parameter should not yet exist within the object's parent. It will be automatically created by this function.
   The parameter will be put into the object's parent, so it exists next to the multi-instance object.

   @param object pointer to a multi-instance object
   @param parameterName the name of the counter parameter that will be created

   @return
    - a pointer to the created counter parameter
    - NULL:
        - the object is a NULL pointer
        - the object is no multi-instance object
        - a counter parameter was already set for the object
        - a parameter named parameterName already exists in the object's parent
        - creation of the parameter failed
 */
parameter_t* object_setInstanceCounter(object_t* object, const char* parameterName) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }
    if(object_isRemote(object)) {
        pcb_error = pcb_error_remote_local_mismatch;
        return NULL;
    }
    if((object->attributes & object_attr_template) != object_attr_template) {
        pcb_error = pcb_error_not_template;
        return NULL;
    }
    if(object->object_info.template_info->instanceCounter != NULL) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    object_t* parent = object_parent(object);
    if(object_hasParameter(parent, parameterName)) {
        pcb_error = pcb_error_not_unique_name;
        return NULL;
    }

    parameter_t* counter = parameter_create(parent, parameterName, parameter_type_uint32, parameter_attr_read_only);
    if(counter == NULL) {
        return NULL;
    }

    object->object_info.template_info->instanceCounter = counter;
    counter->countedObject = object;

    return counter;
}

/**
   @ingroup pcb_core_object
   @brief
   Gets the counter parameter from a multi-instance object.

   @details
   This function gets the counter parameter from a multi-instance object.

   @param object pointer to a multi-instance object

   @return
    - a pointer to the counter parameter
    - NULL:
        - the object is a NULL pointer
        - the object is no multi-instance object
        - no counter parameter exists
 */
parameter_t* object_getInstanceCounter(object_t* object) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }
    if(object_isRemote(object)) {
        pcb_error = pcb_error_remote_local_mismatch;
        return NULL;
    }
    if((object->attributes & object_attr_template) != object_attr_template) {
        pcb_error = pcb_error_not_template;
        return NULL;
    }

    return object->object_info.template_info->instanceCounter;
}

/**
   @ingroup pcb_core_object
   @brief
   Returns whether the object has an instance counter or not.

   @details
   This function returns whether the object has an instance counter or not.

   @param object pointer to a multi-instance object

   @return
    - true the object has an instance counter
    - false:
        - the object is a NULL pointer
        - the object is no multi-instance object
        - no instance counter exists
 */
bool object_hasInstanceCounter(object_t* object) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }
    if(object_isRemote(object)) {
        pcb_error = pcb_error_remote_local_mismatch;
        return false;
    }
    if((object->attributes & object_attr_template) != object_attr_template) {
        pcb_error = pcb_error_not_template;
        return false;
    }

    if(object->object_info.template_info->instanceCounter != NULL) {
        return true;
    }
    return false;
}

/**
   @ingroup pcb_core_object
   @brief
   Sets the delete handler for an object.

   @details
   This function sets the delete handler for an object. This function will be called when the object is going
   to be marked as deleted. If the handler returns false the object will not be marked as deleted.\n
   Handler functions can only be set on local objects.

   @param object pointer to an object
   @param handler pointer to a @ref object_delete_handler_t function

   @return
    - true: operation was successful
    - false:
        - the object is a NULL pointer
        - the operation failed.
 */
bool object_setDeleteHandler(object_t* object, object_delete_handler_t handler) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }
    if(object_isRemote(object)) {
        pcb_error = pcb_error_remote_local_mismatch;
        return false;
    }

    object_handlers_t* handlers = object_getHandlers(object);
    if(!handlers) {
        return false;
    }

    handlers->del = handler;
    return true;
}

/**
   @ingroup pcb_core_object
   @brief
   Sets the destroy handler for an object.

   @details
   This function sets the destroy handler for an object. This function will be called when the object is going
   to be destroyed.\n
   Handler functions can only be set on local objects.

   @param object pointer to an object
   @param handler pointer to a @ref object_destroy_handler_t function

   @return
    - true: operation was successful
    - false:
        - the object is a NULL pointer
        - the operation failed.
 */
bool object_setDestroyHandler(object_t* object, object_destroy_handler_t handler) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }
    if(object_isRemote(object)) {
        pcb_error = pcb_error_remote_local_mismatch;
        return false;
    }
    object_handlers_t* handlers = object_getHandlers(object);
    if(!handlers) {
        return false;
    }

    handlers->destroy = handler;
    return true;
}

/**
   @ingroup pcb_core_object
   @brief
   Sets the read handler for an object.

   @details
   This function sets the read handler for an object. This function will be called when the object needs to be updated.\n
   Handler functions can only be set on local objects.

   @param object pointer to an object
   @param handler pointer to a @ref object_read_handler_t function

   @return
    - true: operation was successful
    - false:
        - the object is a NULL pointer
        - the operation failed.
 */
bool object_setReadHandler(object_t* object, object_read_handler_t handler) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }
    if(object_isRemote(object)) {
        pcb_error = pcb_error_remote_local_mismatch;
        return false;
    }
    object_handlers_t* handlers = object_getHandlers(object);
    if(!handlers) {
        return false;
    }

    handlers->read = handler;
    return true;
}

/**
   @ingroup pcb_core_object
   @brief
   Sets the write handler for an object.

   @details
   This function sets the write handler for an object. This function will be called during the commit if any of it's parameters has been changed.\n
   Handler functions can only be set on local objects.

   @param object pointer to an object
   @param handler pointer to a @ref object_write_handler_t function

   @return
    - true: operation was successful
    - false:
        - the object is a NULL pointer
        - the operation failed.
 */
bool object_setWriteHandler(object_t* object, object_write_handler_t handler) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }
    if(object_isRemote(object)) {
        pcb_error = pcb_error_remote_local_mismatch;
        return false;
    }
    object_handlers_t* handlers = object_getHandlers(object);
    if(!handlers) {
        return false;
    }

    handlers->write = handler;
    return true;
}

object_write_handler_t object_getWriteHandler(object_t* object) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }
    if(object_isRemote(object)) {
        pcb_error = pcb_error_remote_local_mismatch;
        return NULL;
    }
    object_handlers_t* handlers = object_getHandlers(object);
    if(!handlers) {
        return NULL;
    }

    return handlers->write;
}

/**
   @ingroup pcb_core_object
   @brief
   Sets the add instance handler for an object.

   @details
   This function sets the add instance handler for an object. This function will be called when an instance of a template object is created.\n
   This handler function can only be set on template objects.\n
   Handler functions can only be set on local objects.

   @param object pointer to an object
   @param handler pointer to a @ref object_instance_add_handler_t function

   @return
    - true: operation was successful
    - false:
        - the object is a NULL pointer
        - the operation failed.
 */
bool object_setInstanceAddHandler(object_t* object, object_instance_add_handler_t handler) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }
    if(object_isRemote(object)) {
        pcb_error = pcb_error_remote_local_mismatch;
        return false;
    }
    if(!object_isTemplate(object)) {
        pcb_error = pcb_error_not_template;
        return false;
    }
    object_handlers_t* handlers = object_getHandlers(object);
    if(!handlers) {
        return false;
    }

    handlers->instance_add = handler;
    return true;
}

/**
   @ingroup pcb_core_object
   @brief
   Sets the delete instance handler for an object.

   @details
   This function sets the delete instance handler for an object. This function will be called when an instance of a template object is being deleted.\n
   This handler function can only be set on template objects.\n
   Handler functions can only be set on local objects.

   @param object pointer to an object
   @param handler pointer to a @ref object_instance_add_handler_t function

   @return
    - true: operation was successful
    - false:
        - the object is a NULL pointer
        - the operation failed.
 */
bool object_setInstanceDelHandler(object_t* object, object_instance_del_handler_t handler) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }
    if(object_isRemote(object)) {
        pcb_error = pcb_error_remote_local_mismatch;
        return false;
    }
    if(!object_isTemplate(object)) {
        pcb_error = pcb_error_not_template;
        return false;
    }
    object_handlers_t* handlers = object_getHandlers(object);
    if(!handlers) {
        return false;
    }

    handlers->instance_del = handler;
    return true;
}

bool object_setMibAddHandler(object_t* object, object_mib_add_handler_t handler) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }
    if(object_isRemote(object)) {
        pcb_error = pcb_error_remote_local_mismatch;
        return false;
    }
    object_handlers_t* handlers = object_getHandlers(object);
    if(!handlers) {
        return false;
    }

    handlers->mib_added = handler;
    return true;
}

bool object_setMidDelHandler(object_t* object, object_mib_del_handler_t handler) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }
    if(object_isRemote(object)) {
        pcb_error = pcb_error_remote_local_mismatch;
        return false;
    }
    object_handlers_t* handlers = object_getHandlers(object);
    if(!handlers) {
        return false;
    }

    handlers->mib_deleted = handler;
    return true;
}

/**
   @ingroup pcb_core_object
   @brief
   Sets the validation handler for this object.

   @details
   Each object can have a validation handler. This handler function will be called each time the object needs
   to be validate. By default objects do not have a validation handler.\n
   If the validation handler returns false, the validation of the object has failed and the object can not be commited.\n
   Validation data can be provided, but becarefull do not delete the validation data before the object has
   been deleted.

   @param object pointer to an object
   @param validator pointer to a validator, this is created with @ref object_validator_create

   @return
    - true: operation was successful
    - false: the object is a NULL pointer
 */
bool object_setValidator(object_t* object, object_validator_t* validator) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    object_validator_destroy(object->validator);

    object->validator = validator;
    if(validator) {
        validator->reference++;
    }

    return true;
}

/**
   @ingroup pcb_core_object
   @brief
   Returns the name of the object

   @details
   This functions returns the name of the object according to the path attributes.
   For instances this can be the index or the key, for all other kind of objects it will return the
   name of the object

   @param object pointer to an object
   @param pathAttributes specifys how the name should look like @see path_attributes_t

   @return
    - the name or index of the object.
 */
const char* object_name(const object_t* object, uint32_t pathAttributes) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }
    if((object_isInstance(object) &&
        (pathAttributes & path_attr_key_notation)) &&
       (string_length(&object->object_info.instance_info.key) > 0)) {
        return string_buffer(&object->object_info.instance_info.key);
    } else {
        if(object_isRemote(object)) {
            if(pathAttributes & path_attr_key_notation) {
                remote_object_info_t* ri = (remote_object_info_t*) object->userData;
                return ri ? string_buffer(&ri->key) : NULL;
            } else {
                return object->oname;
            }
        } else {
            // Back up pcb_error, because most objects don't have the "Alias" parameter
            // and we don't want it to pollute the result of this function.
            uint32_t pcb_error_bu = pcb_error;
            const char* alias = object_da_parameterCharValue(object, "Alias");
            pcb_error = pcb_error_bu;
            if(object_parent(object) || (pathAttributes & path_attr_include_namespace)) {
                if(object_isInstance(object) && alias && *alias) {
                    if(!(pathAttributes & path_attr_key_notation) && (pathAttributes & path_attr_alias)) {
                        return alias;
                    } else {
                        return object->oname;
                    }
                } else {
                    return object->oname;
                }
            }
        }
    }

    return NULL;
}

/**
   @ingroup pcb_core_object
   @brief
   Returns the object's state

   @details
   This functions returns the state of the object. See @ref object_state_t for the possible states

   @param object pointer to an object

   @return
    - an object state
    - object_state_invalid is returned if the object is a NULL pointer
 */
object_state_t object_state(const object_t* object) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return object_state_invalid;
    }

    return object->state;
}

/**
   @ingroup pcb_core_object
   @brief
   Set user specific data

   @details
   This functions adds user specific data to an object.\n

   When the user specific data is a pointer which needs to be freed when the object is destroyed, you also need to implement
   a object destroy handler.\n

   @param object pointer to an object
   @param data the user data (as a void pointer)
 */
void object_setUserData(object_t* object, void* data) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return;
    }

    if(object_isRemote(object)) {
        remote_object_info_t* remote_info = object->userData;
        remote_info->userdata = data;
    } else {
        object->userData = data;
    }
}

/**
   @ingroup pcb_core_object
   @brief
   Get user specific data

   @details
   This functions gets the user specific data from an object.\n
   User data can only be set on local objects, for all other kinds of objects this function will return NULL\n

   @param object pointer to an object

   @return
    - the user specific data
    - NULL when
        - a NULL pointer was given
        - the object is a remote object
        - the object is a dispatching object

 */
void* object_getUserData(object_t* object) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    if(object_isRemote(object)) {
        remote_object_info_t* remote_info = object->userData;
        if(remote_info) {
            return remote_info->userdata;
        } else {
            return NULL;
        }
    } else {
        return object->userData;
    }
}

/**
   @ingroup pcb_core_object
   @brief
   Add a notify request to an object.

   @details
   Adding a notify request to an object will send notifications to the sender of the request whenever the object changes.\n
   Normally you do not need to call this function, except if you are writing custom request handlers.\n
   The notify request will be removed automatically when the request has been closed by the sender.\n
   The request must be of the request_type_get_object type.

   @param object pointer to an object
   @param req pointer to the original notify request.

   @return
    - true: the request is added to the obejct,
    - false
        - a NULL pointer was given (object or request)
        - The request has the wrong type.
 */
bool object_addNotifyRequest(object_t* object, request_t* req) {
    bool rv = false;

    if(!object || !req) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    uint32_t attributes = request_attributes(req);
    if(attributes & request_wait) {
        goto store_request;
    }

    if(request_type(req) != request_type_get_object) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

store_request:
    if(req->it.list != &object->notifyRequests) {
        rv = llist_append(&object->notifyRequests, &req->it);
        SAH_TRACEZ_NOTICE("pcb_rh", "Waiting nr of requests are %d for object %p", llist_size(&object->notifyRequests), object);
    } else {
        rv = true; // already in the list
    }
    return rv;
}

/**
   @ingroup pcb_core_object
   @brief
   Get the first notification request for this object.

   @details
   Normally you do not need to call this function, only if you need to implement some custom code
   to send notifications. You can the use this function to start iterating over the notify requests attached to the object

   @param object pointer to an object

   @return
    - a pointer to a request_t,
 */
request_t* object_firstNotifyRequest(object_t* object) {
    if(!object) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    llist_iterator_t* it = llist_first(&object->notifyRequests);
    if(it) {
        return llist_item_data(it, request_t, it);
    }

    return NULL;
}

/**
   @ingroup pcb_core_object
   @brief
   Get the next notification request for an object.

   @details
   Normally you do not need to call this function, only if you need to implement some custom code
   to send notifications. You can the use this function to iterating over the notify requests attached to the object

   @param object pointer to an object
   @param reference pointer to another request, used qs reference

   @return
    - a pointer to a request_t,
 */
request_t* object_nextNotifyRequest(object_t* object, request_t* reference) {
    if(!object || !reference) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    if(reference->it.list != &object->notifyRequests) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    llist_iterator_t* it = llist_iterator_next(&reference->it);
    if(it) {
        return llist_item_data(it, request_t, it);
    }

    return NULL;
}

object_t* object_getObjectByKey(object_t* parent, const char* keypath, ...) {
    va_list ap;
    char* path = NULL;
    object_t* o = NULL;

    va_start(ap, keypath);

    /* declare it here to not polute namespace as we are inlined */
    if(vasprintf(&path, keypath, ap) < 0) {
        goto error;
    }

    o = object_getObject(parent, path, path_attr_key_notation, NULL);

    free(path);

error:
    va_end(ap);
    return o;
}
