/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <debug/sahtrace.h>

#include <pcb/core.h>

static bool default_set_parameter_value(function_call_t* fcall, object_t* object, const char* name, variant_t* value, uint32_t uid) {
    bool rv = false;
    parameter_t* param = object_getParameter(object, name);
    if(!param) {
        fcall_addErrorReply(fcall, pcb_error_not_found, error_string(pcb_error_not_found), name);
        goto exit;
    }
    if((request_userID(fcall_request(fcall)) != 0) && parameter_isReadOnly(param)) {
        fcall_addErrorReply(fcall, pcb_error_read_only, error_string(pcb_error_read_only), name);
        goto exit;
    }
    if(!parameter_canWrite(param, uid)) {
        fcall_addErrorReply(fcall, EACCES, error_string(EACCES), name);
        goto exit;
    }
    if(!parameter_setValue(param, value)) {
        parameter_rollback(param);
        fcall_addErrorReply(fcall, pcb_error, error_string(pcb_error), name);
        goto exit;
    }

    rv = true;

exit:
    return rv;
}

static bool default_set_from_arg_list(function_call_t* fcall, argument_value_list_t* args, uint32_t uid) {
    const char* name = NULL;
    variant_t* value = NULL;
    object_t* object = fcall_object(fcall);
    bool rv = true;

    argument_value_t* arg = argument_valueTakeArgumentFirst(args);
    while(arg) {
        name = argument_valueName(arg);
        value = argument_value(arg);

        if(!name || !(*name)) {
            fcall_addErrorReply(fcall, pcb_error_invalid_name, error_string(pcb_error_invalid_name), name);
            continue;
        }
        rv &= default_set_parameter_value(fcall, object, name, value, uid);

        argument_valueDestroy(arg);
        arg = argument_valueTakeArgumentFirst(args);
    }

    return rv;
}

static bool default_set_from_map(function_call_t* fcall, variant_map_t* parameters, uint32_t uid) {
    bool rv = true;
    variant_map_iterator_t* it = NULL;
    const char* name = NULL;
    variant_t* value = NULL;
    object_t* object = fcall_object(fcall);

    /* loop over all parameters in the variant map */
    variant_map_for_each(it, parameters) {
        name = variant_map_iterator_key(it);
        value = variant_map_iterator_data(it);

        rv &= default_set_parameter_value(fcall, object, name, value, uid);
    }

    return rv;
}

function_exec_state_t __get(function_call_t* fcall, argument_value_list_t* args, variant_t* retval) {
    /* clear the arghument list in case the caller has added arguments
       if the argument list is not cleared, all arguments will be sent back as out arguments
     */
    argument_valueClear(args);

    object_t* object = fcall_object(fcall);
    uint32_t uid = request_userID(fcall_request(fcall));
    variant_initialize(retval, variant_type_map);
    object_update(object);

    if(object_isTemplate(object)) {
        object_t* instance = NULL;
        object_for_each_instance(instance, object) {
            variant_map_iterator_t* instance_map_it = variant_map_iterator_createMapMove(object_name(instance, path_attr_key_notation), NULL);
            variant_t* data = variant_map_iterator_data(instance_map_it);
            object_getValuesWithACL(instance, variant_da_map(data), uid);
            variant_map_append(variant_da_map(retval), instance_map_it);
        }
    } else {
        object_getValuesWithACL(object, variant_da_map(retval), uid);
    }

    /* end the RPC call */
    return function_exec_done;
}

function_exec_state_t __set(function_call_t* fcall, argument_value_list_t* args, variant_t* retval) {
    uint32_t attr = request_attributes(fcall_request(fcall));
    variant_map_t* parameters = NULL;
    bool rv = false;
    variant_t* data = NULL;
    argument_value_t* arg = NULL;

    argument_get(&arg, args, attr, "parameters");
    if(!arg) {
        if(attr & request_function_args_by_name) {
            rv = default_set_from_arg_list(fcall, args, request_userID(fcall_request(fcall)));
            goto exit_commit;
        } else {
            SAH_TRACEZ_ERROR("pcb_def", "Missing argument 'parameters'' - or parameters not given by name");
            /* nothing to be done */
            fcall_addErrorReply(fcall, pcb_error_function_argument_missing, error_string(pcb_error_function_argument_missing), "parameters");
            goto exit;
        }
    }

    data = argument_value(arg);
    if(variant_type(data) != variant_type_map) {
        SAH_TRACEZ_ERROR("pcb_def", "Type mismatch");
        fcall_addErrorReply(fcall, pcb_error_type_mismatch, "Argument has wrong type", "parameters");
        /* nothing to be done */
        goto exit;
    }

    parameters = variant_da_map(data);

    /* clear the argument list in case the caller has added arguments
       if the argument list is not cleared, all arguments will be sent back as out arguments
     */
    argument_valueClear(args);

    rv = default_set_from_map(fcall, parameters, request_userID(fcall_request(fcall)));

exit_commit:
    if(!object_commit(fcall_object(fcall))) {
        fcall_addErrorReply(fcall, pcb_error, error_string(pcb_error), "object");
        rv = false;
        object_rollback(fcall_object(fcall));
    }

exit:
    argument_valueDestroy(arg);
    variant_setBool(retval, rv);

    /* end the RPC call */
    return function_exec_done;
}


static bool modifyAclCheckArgument(variant_map_t* map) {
    variant_t* idData = variant_map_findVariant(map, "id");
    variant_t* flagData = variant_map_findVariant(map, "flag");
    const char* action = variant_map_da_findChar(map, "action");
    if(!idData || !flagData || !action) {
        return false;
    }
    if((strcmp(action, "add") != 0) && (strcmp(action, "del") != 0)) {
        return false;
    }
    return true;
}

static bool modifyAclValidateArgument(function_call_t* fcall, variant_list_t* objectList, variant_map_t* parameterMap, variant_map_t* functionMap) {
    variant_list_iterator_t* lit = NULL;
    variant_map_iterator_t* mit = NULL;
    const char* parameterName = NULL;
    const char* functionName = NULL;

    object_t* myObject = fcall_object(fcall);

    /* Validate arguments */
    if(!objectList && !parameterMap && !functionMap) {
        SAH_TRACEZ_ERROR("pcb_def", "Missing arguments");
        fcall_addErrorReply(fcall, pcb_error_function_argument_missing, "Missing arguments", "object || parameters || functions");
        /* nothing to be done */
        return false;
    }

    if(objectList) {
        variant_list_for_each(lit, objectList) {
            if(modifyAclCheckArgument(variant_da_map(variant_list_iterator_data(lit))) == false) {
                SAH_TRACEZ_ERROR("pcb_def", "Missing arguments");
                fcall_addErrorReply(fcall, pcb_error_function_argument_missing, "Argument is missing", "object id or flag");
                /* nothing to be done */
                return false;
            }
        }
    }

    if(parameterMap) {
        variant_map_for_each(mit, parameterMap) {
            parameterName = variant_map_iterator_key(mit);
            variant_list_for_each(lit, variant_da_list(variant_map_iterator_data(mit))) {
                if(modifyAclCheckArgument(variant_da_map(variant_list_iterator_data(lit))) == false) {
                    SAH_TRACEZ_ERROR("pcb_def", "Missing arguments");
                    fcall_addErrorReply(fcall, pcb_error_function_argument_missing, "Argument is missing", "parameter id or flag");
                    /* nothing to be done */
                    return false;
                }
                if(object_hasParameter(myObject, parameterName) == false) {
                    SAH_TRACEZ_ERROR("pcb_def", "Invalid parameter");
                    fcall_addErrorReply(fcall, pcb_error_invalid_parameter, "Parameter does not exist", parameterName);
                    /* nothing to be done */
                    return false;
                }
            }
        }
    }

    if(functionMap) {
        variant_map_for_each(mit, functionMap) {
            functionName = variant_map_iterator_key(mit);
            variant_list_for_each(lit, variant_da_list(variant_map_iterator_data(mit))) {
                if(modifyAclCheckArgument(variant_da_map(variant_list_iterator_data(lit))) == false) {
                    SAH_TRACEZ_ERROR("pcb_def", "Missing arguments");
                    fcall_addErrorReply(fcall, pcb_error_function_argument_missing, "Argument is missing", "function id or flag");
                    /* nothing to be done */
                    return false;
                }
                if(object_getFunction(myObject, functionName) == NULL) {
                    SAH_TRACEZ_ERROR("pcb_def", "Invalid parameter");
                    fcall_addErrorReply(fcall, pcb_error_invalid_parameter, "Function does not exist", functionName);
                    /* nothing to be done */
                    return false;
                }
            }
        }
    }
    return true;
}


static void modifyAclApply(function_call_t* fcall, variant_list_t* objectList, variant_map_t* parameterMap, variant_map_t* functionMap) {
    uint32_t id = 0;
    uint16_t flags = 0;
    variant_list_iterator_t* lit = NULL;
    variant_map_iterator_t* mit = NULL;
    const char* parameterName = NULL;
    const char* functionName = NULL;
    const char* action = NULL;

    object_t* myObject = fcall_object(fcall);

    if(objectList) {
        variant_list_for_each(lit, objectList) {
            id = variant_map_findUInt32(variant_da_map(variant_list_iterator_data(lit)), "id");
            flags = variant_map_findUInt16(variant_da_map(variant_list_iterator_data(lit)), "flag");
            action = variant_map_da_findChar(variant_da_map(variant_list_iterator_data(lit)), "action");
            if(strcmp(action, "add") == 0) {
                SAH_TRACEZ_INFO("pcb_def", "Adding acl to object: %d %d", id, flags);
                object_aclSet(myObject, id, flags);
            } else if(strcmp(action, "del") == 0) {
                SAH_TRACEZ_INFO("pcb_def", "Removing acl from object: %d %d", id, flags);
                object_aclDel(myObject, id, flags);
            }
        }
    }

    if(parameterMap) {
        variant_map_for_each(mit, parameterMap) {
            parameterName = variant_map_iterator_key(mit);
            variant_list_for_each(lit, variant_da_list(variant_map_iterator_data(mit))) {
                id = variant_map_findUInt32(variant_da_map(variant_list_iterator_data(lit)), "id");
                flags = variant_map_findUInt16(variant_da_map(variant_list_iterator_data(lit)), "flag");
                action = variant_map_da_findChar(variant_da_map(variant_list_iterator_data(lit)), "action");
                parameter_t* param = object_getParameter(myObject, parameterName);
                if(strcmp(action, "add") == 0) {
                    SAH_TRACEZ_INFO("pcb_def", "Adding acl to parameter %s: %d %d", parameterName, id, flags);
                    parameter_aclSet(param, id, flags);
                } else if(strcmp(action, "del") == 0) {
                    SAH_TRACEZ_INFO("pcb_def", "Removing acl from parameter %s: %d %d", parameterName, id, flags);
                    parameter_aclDel(param, id, flags);
                }
            }
        }
    }

    if(functionMap) {
        variant_map_for_each(mit, functionMap) {
            functionName = variant_map_iterator_key(mit);
            variant_list_for_each(lit, variant_da_list(variant_map_iterator_data(mit))) {
                id = variant_map_findUInt32(variant_da_map(variant_list_iterator_data(lit)), "id");
                flags = variant_map_findUInt16(variant_da_map(variant_list_iterator_data(lit)), "flag");
                action = variant_map_da_findChar(variant_da_map(variant_list_iterator_data(lit)), "action");
                function_t* function = object_getFunction(myObject, functionName);
                if(strcmp(action, "add") == 0) {
                    SAH_TRACEZ_INFO("pcb_def", "Adding acl to function %s: %d %d", functionName, id, flags);
                    function_aclSet(function, id, flags);
                } else if(strcmp(action, "del") == 0) {
                    SAH_TRACEZ_INFO("pcb_def", "Removing acl from function %s: %d %d", functionName, id, flags);
                    function_aclDel(function, id, flags);
                }
            }
        }
    }
}

/*
   object.modifyAcl(map object, map parameters, map functions)
   object.modifyAcl(object    : [{id: value, flags:value, action:value}],
                 parameters: { parametername: [{id: value, flags:value, action:value}]},
                 functions : { functionname : [{id: value, flags:value, action:value}]})
 */
function_exec_state_t __modifyAcl(function_call_t* fcall, argument_value_list_t* args, variant_t* retval) {
    uint32_t attr = request_attributes(fcall_request(fcall));
    bool rv = false;
    variant_list_t* objectList = NULL;
    variant_map_t* parameterMap = NULL;
    variant_map_t* functionMap = NULL;

    argument_getList(&objectList, args, attr, "object", NULL);
    argument_getMap(&parameterMap, args, attr, "parameters", NULL);
    argument_getMap(&functionMap, args, attr, "functions", NULL);

    if(modifyAclValidateArgument(fcall, objectList, parameterMap, functionMap) == false) {
        goto exit;
    }

    /* clear the argument list in case the caller has added arguments
       if the argument list is not cleared, all arguments will be sent back as out arguments
     */
    argument_valueClear(args);

    modifyAclApply(fcall, objectList, parameterMap, functionMap);

exit:
    variant_setBool(retval, rv);

    /* cleanup memory */
    variant_list_cleanup(objectList);
    free(objectList);
    variant_map_cleanup(parameterMap);
    free(parameterMap);
    variant_map_cleanup(functionMap);
    free(functionMap);

    /* end the RPC call */
    return function_exec_done;
}

/*

 */


function_exec_state_t __getAclInfo(function_call_t* fcall, argument_value_list_t* args, variant_t* retval) {
    uint32_t attr = request_attributes(fcall_request(fcall));
    object_t* object = fcall_object(fcall);
    int32_t userId = 0;
    bool all = false;

    argument_getInt32(&userId, args, attr, "userID", UINT32_MAX);
    argument_getBool(&all, args, attr, "all", true);
    argument_valueClear(args);

    SAH_TRACE_INFO("USER ID = 0x%8.8X", userId);

    char ar[4] = "---";
    string_t path;
    string_initialize(&path, 64);
    object_path(object, &path, path_attr_key_notation);

    variant_initialize(retval, variant_type_map);
    variant_map_t* data = variant_da_map(retval);

    ar[0] = object_canRead(object, userId) ? 'R' : '-';
    ar[1] = object_canWrite(object, userId) ? 'W' : '-';
    ar[2] = object_canExecute(object, userId) ? 'X' : '-';
    variant_map_addChar(data, string_buffer(&path), ar);

    variant_map_iterator_t* mpit = variant_map_iterator_createMapMove("Parameters", NULL);
    variant_map_t* mp = variant_da_map(variant_map_iterator_data(mpit));

    parameter_t* parameter = NULL;
    object_for_each_parameter(parameter, object) {
        ar[0] = parameter_canRead(parameter, userId) ? 'R' : '-';
        ar[1] = parameter_canWrite(parameter, userId) ? 'W' : '-';
        ar[2] = parameter_canReadValue(parameter, userId) ? 'X' : '-';
        if(all || (strcmp(ar, "---") != 0)) {
            variant_map_addChar(mp, parameter_name(parameter), ar);
        }
    }
    if(variant_map_isEmpty(mp)) {
        variant_map_iterator_destroy(mpit);
        mpit = NULL;
    } else {
        variant_map_append(data, mpit);
    }

    variant_map_iterator_t* mfit = variant_map_iterator_createMapMove("Functions", NULL);
    variant_map_t* mf = variant_da_map(variant_map_iterator_data(mfit));

    function_t* function = NULL;
    object_for_each_function(function, object) {
        ar[0] = function_canRead(function, userId) ? 'R' : '-';
        ar[1] = '-';
        ar[2] = function_canExecute(function, userId) ? 'X' : '-';
        if(all || (strcmp(ar, "---") != 0)) {
            variant_map_addChar(mf, function_name(function), ar);
        }
    }
    if(variant_map_isEmpty(mf)) {
        variant_map_iterator_destroy(mfit);
        mfit = NULL;
    } else {
        variant_map_append(data, mfit);
    }

    if(!all && (variant_map_size(data) == 1)) {
        variant_initialize(retval, variant_type_unknown);
    }
    string_cleanup(&path);
    /* end the RPC call */
    return function_exec_done;
}
