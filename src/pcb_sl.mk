ifndef STAGINGDIR
  $(error STAGINGDIR not defined, stopping build)
endif

include $(STAGINGDIR)/components.config

OBJECTS = utils/connection.o \
          utils/peer.o \
          utils/process.o

TARGET = libpcb_sl

LOCAL_LIBS = -L. -lpcb_utils -lrt

include Common.mk

clean:
	rm -f $(TARGET).so $(OBJECTS) $(TARGET).a
	rm -f utils/*.d
	rm -f utils/*.gcno
	rm -f utils/*.gcda
	rm -f *.gcov

.PHONY: clean
