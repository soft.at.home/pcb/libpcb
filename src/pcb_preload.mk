ifndef STAGINGDIR
  $(error STAGINGDIR not defined, stopping build)
endif

include $(STAGINGDIR)/components.config

OBJECTS = preload/preload.o

TARGET = libpcb_preload

CFLAGS += -Wall -Wextra -Werror -fPIC -std=gnu99

all: $(TARGET).so $(TARGET).a

$(TARGET).so: $(OBJECTS)
	$(CC) -Wl,-soname,$(@) -shared -fPIC -o $(@) $(OBJECTS) $(LDFLAGS)

-include $(OBJECTS:.o=.d)

$(TARGET).a: $(OBJECTS)
	$(AR) rcs $(@) $^

%.o: %.c
	$(CC) $(CFLAGS) -c -o $@ $<
	@$(CC) $(CFLAGS) -MM -MP -MT '$(@) $(@:.o=.d)' -MF $(@:.o=.d) $(<)

clean:
	rm -f $(TARGET).so $(OBJECTS) $(TARGET).a
	rm -f preload/*.d
	rm -f preload/*.gcno
	rm -f preload/*.gcda
	rm -f *.gcov

.PHONY: clean
