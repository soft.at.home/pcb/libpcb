ifndef STAGINGDIR
  $(error STAGINGDIR not defined, stopping build)
endif

include $(STAGINGDIR)/components.config

OBJECTS_COMMON = common/error.o

OBJECTS_UTILS_SENDFILE = utils/sendfile.o

OBJECTS = $(OBJECTS_UTILS_SENDFILE) $(OBJECTS_COMMON)

TARGET = libpcb_utils_sendfile

LOCAL_LIBS = -L. -lrt -lpcb_utils

ifeq ($(CONFIG_PCB_OPEN_SSL_SUPPORT),y)
LOCAL_LIBS += -lpcb_ssl
else
LOCAL_LIBS += -lpcb_sl
endif

include Common.mk

clean:
	rm -f $(TARGET).so $(OBJECTS) $(TARGET).a
	rm -f common/*.d utils/*.d
	rm -f common/*.gcno utils/*.gcno
	rm -f common/*.gcda utils/*.gcda
	rm -f *.gcov

.PHONY: clean
