/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#define _GNU_SOURCE
#include <stdlib.h>
#include <errno.h>
#include <dirent.h>
#include <dlfcn.h>
#include <sys/un.h>
#include <sys/types.h>
#include <fcntl.h>
#include <signal.h>
#include <string.h>

#ifdef OPEN_SSL_SUPPORT
#include <openssl/rand.h>
#include <openssl/err.h>
#include <openssl/opensslv.h>
#include <openssl/bn.h>

#include "connection_dh2048_compatibility.h"
#if OPENSSL_VERSION_NUMBER < 0x30000000L
#include "connection_dh2048.h"
#endif
#endif

#include <debug/sahtrace.h>
#include <components.h>

#include <pcb/common/error.h>
#include <pcb/utils/connection.h>
#include <pcb/utils/timer.h>

#include <pcb/utils/peer.h>
#include <pcb/utils/process.h>

typedef struct connection_runtime_info {
    sigset_t sig_set;
    sigset_t sig_blocked_set;
    sigset_t sig_orig_set;
    uint32_t sig_count;
    int pipefd[2];
    bool terminated;

    /* socket stuff */
    fd_set g_readset;                        /**< The readset */
    fd_set g_writeset;                       /**< The writeset */
    int g_set;                               /**< Keeps track of the number of events */
    bool initialized;
    bool sslEnabled;
    bool blockSignals;
} connection_runtime_info_t;

static connection_runtime_info_t runtime = {
    .pipefd = { -1, -1},
    .initialized = false,
    .sslEnabled = true,
    .blockSignals = true,
};

#ifdef OPEN_SSL_SUPPORT
bool peer_ssl_connect(connection_info_t* con, peer_info_t* peer, const char* node);
bool peer_ssl_setPeer(SSL* ssl, peer_info_t* peer);
peer_info_t* peer_ssl_getPeer(SSL* ssl);
#endif

/**
   @file
   @brief
   Implementation of a connection
 */

/**
   @brief
   Add pipe file descriptor to the set

   @details
   Adds the file descriptor to the set
 */
static void connection_pipe_add_set(int i, fd_set* set, int* maxfd) {
    if((i < 0) || (i > 1)) {
        return;
    }

    if(runtime.pipefd[i] == -1) {
        SAH_TRACEZ_ERROR("pcb_con", "signal pipe not created, library not initialized?");
        return;
    }
    FD_SET(runtime.pipefd[i], set);
    if(runtime.pipefd[i] > *maxfd) {
        *maxfd = runtime.pipefd[i];
    }
}

/**
   @brief
   Copy and reset the signal parameters

   @details
   Make a copy and then reset the signal parameters atomically.

   NOTE: sig_blocked_set must already be filled.

   @param signal_count The location to store the copy of sig_count, if not NULL
   @param signal_set The location to store the copy of sig_set, if not NULL
 */
static void signal_copyAndReset_atomic(int* signal_count, sigset_t* signal_set) {
    sigset_t signal_orig_set;

    /*
     * Temporary block signals, even when blockSignals is false.
     * This allows us to make copies of and reset
     * the global sig_set and sig_count atomically.
     */
    pthread_sigmask(SIG_SETMASK, &runtime.sig_blocked_set, &signal_orig_set);
    if(signal_count) {
        *signal_count = runtime.sig_count;
    }
    if(signal_set) {
        *signal_set = runtime.sig_set;
    }
    runtime.sig_count = 0;
    sigemptyset(&runtime.sig_set);
    pthread_sigmask(SIG_SETMASK, &signal_orig_set, NULL);
}

/**
   @brief
   Global signal handler..

   @details
   Marks a signal as set in a bitmask.

   @param signal The signal number.
 */
static void signalHandler(int signal) {
    if(sigismember(&runtime.sig_set, signal) == 0) {
        runtime.sig_count++;
        sigaddset(&runtime.sig_set, signal);

        /* write character to pipe */
        if(runtime.pipefd[1] != -1) {
            char c = 's';
            if(write(runtime.pipefd[1], &c, 1) == -1) {
                // an error occured
            }
        }
    }
}

/**
   @brief
   Handle the set signals using a bitmask.

   @details
   First of all, all signals will be block during the execution of this function.
   If signals are available, the SIGALRM signal will be checked first.
   If SIGALRM is set all timers will be checked to see if they are expired, if so
   the handler functions for the expired timers are called.
   Last but not least all other signal handlers are checked to see if the signal for
   that handler is set. If so, the handler function is called.

   @param con A pointer to a connection object
 */
static void connection_handle_signals(connection_info_t* con, int signal_count, sigset_t* signal_set) {
    SAH_TRACEZ_INFO("pcb_con", "Signals available, check event handlers");
    /* delete SIGALRM */
    if(sigismember(signal_set, SIGALRM) == 1) {
        signal_count--;
        sigdelset(signal_set, SIGALRM);
    }

    if(sigismember(signal_set, SIGCHLD)) {
        /* SIGCHLD is special.
         * The process code first needs to check this signal was sent because
         * of one of its processes. It must then pass the signal on to the
         * classical signal handlers, because there might be interested
         * handlers.
         * Note that it's possible for multiple children to die at the same
         * time and generate only one signal.
         */
        llist_iterator_t* it = llist_first(&con->activeProcesses);
        llist_iterator_t* prefetch = llist_iterator_next(it);
        while(it) {
            process_info_t* p = llist_item_data(it, process_info_t, it);
            process_handle_sigchld(p);

            it = prefetch;
            prefetch = llist_iterator_next(it);
        }
    }

    /* loop over all signal handlers */
    signal_handler_t* sh = NULL;
    llist_iterator_t* it = llist_first(&con->signalHandlers);
    llist_iterator_t* prefetch = llist_iterator_next(it);
    while(it) {
        sh = llist_item_data(it, signal_handler_t, it);
        if(sigismember(signal_set, sh->signal) && sh->handler) {
            SAH_TRACEZ_NOTICE("pcb_con", "Found signal event handler, calling eventhandler ...");
            sh->handler(sh->signal);
        }
        it = prefetch;
        prefetch = llist_iterator_next(it);
    }
}

#ifdef OPEN_SSL_SUPPORT
/**
   @brief
   Initialize the SSL library.

   @details
   Intialize the SSL library. This function must only be called once in an application.
   This must be called before any connection or peer object is created.

   @return
    - true: initialization was successful.
    - false: an error has occured
 */
static bool connection_initSSL() {
#if OPENSSL_VERSION_NUMBER < 0x10100000L
    if(!SSL_library_init()) {
        SAH_TRACE_ERROR("Failed to initialize OpenSSL library");
        return false;
    }
    SSL_load_error_strings();
    OpenSSL_add_all_algorithms();
#endif

    RAND_load_file("/dev/urandom", 1024);

    return true;
}

static void connection_cleanupSSL() {
#if OPENSSL_VERSION_NUMBER < 0x10100000L
    CONF_modules_free();
#if OPENSSL_VERSION_NUMBER >= 0x10000000L
    ERR_remove_thread_state(NULL);
#else
    ERR_remove_state(0);
#endif
#endif

    CONF_modules_unload(1);

#if OPENSSL_VERSION_NUMBER < 0x10100000L
    ERR_free_strings();
    EVP_cleanup();
    CRYPTO_cleanup_all_ex_data();
#endif
}

/**
   @brief
   Debugging function. Prints reason why a certificate is not accepted.

   @details
   This function is used for debugging purposes only.
   This function will print some messages about the certificate that is not accepted.
   See open ssl documentation for more information

   @param ok Indicates that the cerificate is ok or not.
   @param store certificate store

   @return
    This function returns ok parameter without any modifications.
 */
static int connection_verify_cert_callback(int ok, X509_STORE_CTX* store) {
    char data[256];
    int err = X509_STORE_CTX_get_error(store);
    X509* cert = X509_STORE_CTX_get_current_cert(store);
    SSL* ssl = (SSL*) X509_STORE_CTX_get_ex_data(store, SSL_get_ex_data_X509_STORE_CTX_idx());
    peer_info_t* peer = peer_ssl_getPeer(ssl);
    connection_info_t* con = peer_connection(peer);

    SAH_TRACE_IN();
    if(!ok) {

        switch(err) {
        case X509_V_OK:
            ok = 1;
            /* all ok, nothing to do */
            break;
        case X509_V_ERR_CERT_NOT_YET_VALID:
        case X509_V_ERR_CERT_HAS_EXPIRED:
            if(con->attrib & ssl_attribute_accept_expired) {
                ok = 1;
                SAH_TRACE_WARNING("ignoring certificate warning: %s", X509_verify_cert_error_string(err));
            } else {
                ok = 0;
                SAH_TRACE_WARNING("certificate error: %s", X509_verify_cert_error_string(err));
            }
            break;
        case X509_V_ERR_DEPTH_ZERO_SELF_SIGNED_CERT:
            if(con->attrib & ssl_attribute_accept_self_signed) {
                ok = 1;
                SAH_TRACE_WARNING("ignoring certificate warning: %s", X509_verify_cert_error_string(err));
            } else {
                ok = 0;
                SAH_TRACE_WARNING("certificate error: %s", X509_verify_cert_error_string(err));
            }
            break;
        default:
            SAH_TRACE_ERROR("Error with certificate at depth %i", X509_STORE_CTX_get_error_depth(store));
            X509_NAME_oneline(X509_get_issuer_name(cert), data, 256);
            SAH_TRACE_ERROR("     issuer = %s", data);
            X509_NAME_oneline(X509_get_subject_name(cert), data, 256);
            SAH_TRACE_ERROR("     subject = %s", data);
            SAH_TRACE_ERROR("     Error %i: %s", err, X509_verify_cert_error_string(err));
            ok = 0;
            break;
        }
    }

    if(ok && (X509_STORE_CTX_get_error_depth(store) == 0) && con && (con->attrib & ssl_attribute_check_host_name)) {
        err = connection_checkCertificateHostname(cert, peer->node);
        if(err != X509_V_OK) {
            X509_STORE_CTX_set_error(store, err);
            ok = 0;
        }
    }

    if(con && con->verify_peer) {
        ok = con->verify_peer(ok, store, peer);
    }

    SAH_TRACE_OUT();
    return ok;
}

/**
   @brief
   Implementation to verify the peer's certificate.

   @details
   This implementation checks that the subject's common name or one of
   the alternative DNS names of the certificate is matching the hostname.
   example:
   When connecting to server "mycompany.org" the certificate must contain "mycompany.org".
   \n
   This verification step is performed automatically by the PCB library when
   the flag 'ssl_attribute_check_host_name' is set.
   If the hostname to check for is different than the hostname used to make the connection,
   then a plug-in can leave out this flag and call this function itself with the
   expected hostname, preferably from within a \ref verify_peer_certificate_t callback.

   @param cert the certficate to verify.
   @param hostname hostname we are connecting to

   @return
    - X509_V_OK when the certificate is ok.
    - X509_V_ERR_APPLICATION_VERIFICATION: when the certificate is not valid.
 */
long connection_checkCertificateHostname(X509* cert, const char* hostname) {
    char* peername = NULL;

    SAH_TRACE_IN();

    if(X509_check_host(cert, hostname, 0, X509_CHECK_FLAG_ALWAYS_CHECK_SUBJECT, &peername) == 1) {
        SAH_TRACE_INFO("Certificate accepted: Hostname '%s' matches with peername '%s'", hostname, peername);
        OPENSSL_free(peername);
        SAH_TRACE_OUT();
        return X509_V_OK;
    }

    SAH_TRACE_ERROR("Certificate not accepted: Domain check failed");
    SAH_TRACE_OUT();
    return X509_V_ERR_APPLICATION_VERIFICATION;
}
#endif

static bool connection_setNonBlocking(peer_info_t* peer) {
    int flags = fcntl(peer->socketfd, F_GETFL, 0);
    if(flags < 0) {
        pcb_error = errno;
        SAH_TRACEZ_ERROR("pcb_con", "socket fnctl F_GETFL failed: %s", error_string(pcb_error));
        goto exit_error;
    }
    if(fcntl(peer->socketfd, F_SETFL, flags | O_NONBLOCK) < 0) {
        pcb_error = errno;
        SAH_TRACEZ_ERROR("pcb_con", "socket fnctl F_SETFL failed %s", error_string(pcb_error));
        goto exit_error;
    }

    return true;

exit_error:
    return false;
}

static bool connection_listenSocketClosed(peer_info_t* peer) {
    connection_info_t* cons = peer_connection(peer);
    llist_iterator_t* it = NULL;
    connection_t* con = NULL;

    llist_for_each(it, &cons->connections) {
        con = llist_item_data(it, connection_t, it);
        if(con->info.listensocket == peer) {
            con->info.listensocket = NULL;
        }
    }

    return true;
}

/**
   @brief
   Build the read set from a list of peers

   @details
   Check all peers in a list and see if they have to be monitored for reading.
   If so, add the peer's socket to the read set and update the maxfd if needed.
   A peer has to be monitored for reading when it has a read event handler set or
   when the SSL_ERROR_NEED_READ  error condition has happend:

   @param connections linked list of peers
   @param readset pointer to the read set that will be updated
   @param maxfd pointer to the current maximum file descriptor
 */
void connection_build_readset(llist_t* connections, fd_set* readset, int* maxfd) {
    llist_iterator_t* it = llist_first(connections);
    llist_iterator_t* prefetch = llist_iterator_next(it);
    connection_t* con = NULL;
    while(it) {
        con = llist_item_data(it, connection_t, it);
        if(con->info.socketfd == -1) {
            if(con->info.state == peer_state_deleted) {
                peer_delete(&con->info);
            }
            it = prefetch;
            prefetch = llist_iterator_next(it);
            continue;
        }
        if(peer_needRead(&con->info)) {
            FD_SET(con->info.socketfd, readset);
            if(con->info.socketfd > *maxfd) {
                *maxfd = con->info.socketfd;
            }
        }
        it = prefetch;
        prefetch = llist_iterator_next(it);
    }
}

/**
   @brief
   Build the write set from a list of peers

   @details
   Check all peers in a list and see if they have to be monitored for writing.
   If so, add the peer's socket to the write set and update the maxfd if needed.
   A peer has to be monitored for writing when it has a write event handler set or
   when data is available in the circular buffer or when the SSL_ERROR_NEED_WRITE
   error condition has happended.

   @param connections linked list of peers
   @param writeset pointer to the write set that will be updated
   @param maxfd pointer to the current maximum file descriptor
 */
void connection_build_writeset(llist_t* connections, fd_set* writeset, int* maxfd) {
    llist_iterator_t* it = llist_first(connections);
    llist_iterator_t* prefetch = llist_iterator_next(it);
    connection_t* con = NULL;
    while(it) {
        con = llist_item_data(it, connection_t, it);
        if(con->info.socketfd == -1) {
            if(con->info.state == peer_state_deleted) {
                peer_delete(&con->info);
            }
            it = prefetch;
            prefetch = llist_iterator_next(it);
            continue;
        }
        if(peer_needWrite(&con->info)) {
            FD_SET(con->info.socketfd, writeset);
            if(con->info.socketfd > *maxfd) {
                *maxfd = con->info.socketfd;
            }
        }
        it = prefetch;
        prefetch = llist_iterator_next(it);
    }
}

/**
   @brief
   Check socket for reading or writing.

   @details
   First check all sockets for writing. If there is data to be written do so.
   Second check all sockets for reading. If data is available for reading do so.
   Closed sockets are skipped, sockets marked for deletion or really deleted here.
   If an error occurs during reading or writing the following action is taken:
    - destroy the socket if it is a client connection
    - close the socket if it is a server socket. You can try the reconnect function on this closed socket..

   @param c pointer to an connection_info_t structure
   @param connections list of sockets to check
   @param set number of sockets set
 */
static void connection_check(llist_t* connections, int* set, fd_set* readset, fd_set* writeset) {
    llist_iterator_t* it = NULL;
    llist_iterator_t* prefetch = NULL;
    connection_t* con = NULL;
    SAH_TRACEZ_IN("pcb_con");

    /* do not check in 1 loop the ready read and ready write stuff.
       the client handlers can close/destroy a peer.
       at that point the pointer is not valid any more
       the last call accessing the pointer must always be the custom event handlers.
       except when the custom event handler is returning false, the closing/destroying
       is done here.

       do not use the for_each macros here, the list can be altered
       use while and prefetch the next
     */
    /* check ready write */
    it = llist_first(connections);
    prefetch = llist_iterator_next(it);
    while(it) {
        con = llist_item_data(it, connection_t, it);

        SAH_TRACEZ_INFO("pcb_con", "Checking socket %d for write (is server socket? %s)", con->info.socketfd, peer_isServerConnection(&con->info) ? "yes" : "no");
        if(con->info.socketfd == -1) {
            /* the socket is closed, skip it */
            it = prefetch;
            prefetch = llist_iterator_next(it);
            continue;
        }

        /* check write */
        if(FD_ISSET(con->info.socketfd, writeset)) {
            (*set)--;
            SAH_TRACEZ_NOTICE("pcb_con", "Write data to connection %p (socket %d)", con, con->info.socketfd);
            FD_CLR(con->info.socketfd, writeset);
            if(!peer_handleWrite(&con->info)) {
                if(peer_isClientConnection(&con->info)) {
                    peer_destroy(&con->info);
                } else {
                    if(peer_isServerConnection(&con->info)) {
                        peer_close(&con->info);
                    }
                }
            }
        }

        if(!(*set)) {
            break;
        }

        it = prefetch;
        prefetch = llist_iterator_next(it);
    }

    /* cehck ready read */
    it = llist_first(connections);
    prefetch = llist_iterator_next(it);
    while(it) {
        con = llist_item_data(it, connection_t, it);

        SAH_TRACEZ_INFO("pcb_con", "Checking socket %d for read (is server socket? %s)", con->info.socketfd, peer_isServerConnection(&con->info) ? "yes" : "no");
        if(con->info.socketfd == -1) {
            /* the socket is closed, skip it */
            it = prefetch;
            prefetch = llist_iterator_next(it);
            continue;
        }

        /* check read */
        if(FD_ISSET(con->info.socketfd, readset)) {
            (*set)--;
            SAH_TRACEZ_NOTICE("pcb_con", "Read data from connection %p (socket %d)", con, con->info.socketfd);
            FD_CLR(con->info.socketfd, readset);
            if(!peer_handleRead(&con->info)) {
                if(peer_isClientConnection(&con->info)) {
                    peer_destroy(&con->info);
                } else {
                    if(!peer_isListenSocket(&con->info)) {
                        peer_close(&con->info);
                    }
                }
            }
            SAH_TRACEZ_NOTICE("pcb_con", "read done");
        }

        if(!(*set)) {
            break;
        }

        it = prefetch;
        prefetch = llist_iterator_next(it);
    }

    SAH_TRACEZ_OUT("pcb_con");
}

/**
   @brief
   Build the readset

   @details
   This function builds the readset for all readable sockets.

   @param con A pointer to a connection object, must be created with @ref connection_create
   @param readset Pointer to a readset that has to be filled
   @param maxfd pointer to an integer that will hold the maximum file descriptor
 */
static void connection_readSet(connection_info_t* con, fd_set* readset, int* maxfd) {
    /* add listen sockets to readset */
    connection_build_readset(&con->listenSockets, readset, maxfd);
    /* add sockets to the readset */
    connection_build_readset(&con->connections, readset, maxfd);

    pcb_error = pcb_ok;
}

/**
   @brief
   Build the writeset

   @details
   This function builds the writeset, if no messages or daa has to be send on any socket, the writeset will be empty.

   @param con A pointer to a connection object, must be created with @ref connection_create
   @param writeset Pointer to a readset that has to be filled
   @param maxfd pointer to an integer that will hold the maximum file descriptor
 */
static void connection_writeSet(connection_info_t* con, fd_set* writeset, int* maxfd) {
    /* add server sockets to the write set */
    connection_build_writeset(&con->connections, writeset, maxfd);

    pcb_error = pcb_ok;
}

/**
   @ingroup pcb_socket_layer_connections
   @brief
   Initialize the eventing system.

   @details
   This function initializes the library for event handling.\n
   When need it also initializes the SSL library (see @ref connection_enableSSL).
   Also the global timer structures are initialized in this function.
   When your application exits the @ref connection_exitLibrary must be called.
   This function can only be called once and must be called at the very beginning of your application.

   @return
    - true: initialization was successful.
    - false: an error has occured
 */
bool connection_initLibrary(void) {
    if(runtime.initialized) {
        return true;
    }

    /* Always prepare the sig_blocked_set, we need it when handling events */
    if(sigfillset(&runtime.sig_blocked_set) == -1) {
        return false;
    }

    int32_t i = 0;
    for(i = SIGRTMIN; i <= SIGRTMAX; i++) {
        if(sigdelset(&runtime.sig_blocked_set, i) == -1) {
            return false;
        }
    }

    sigdelset(&runtime.sig_blocked_set, SIGSEGV);
    sigdelset(&runtime.sig_blocked_set, SIGBUS);
    sigdelset(&runtime.sig_blocked_set, SIGFPE);
    sigdelset(&runtime.sig_blocked_set, SIGILL);
    sigdelset(&runtime.sig_blocked_set, SIGABRT);

    if(runtime.blockSignals && (pthread_sigmask(SIG_SETMASK, &runtime.sig_blocked_set, &runtime.sig_orig_set) == -1)) {
        SAH_TRACEZ_ERROR("pcb_con", "Failed to block all signals %d", errno);
        return false;
    }

    signal_copyAndReset_atomic(NULL, NULL);

    if(pipe(runtime.pipefd)) {
        SAH_TRACEZ_ERROR("pcb_con", "Failed to create pipe %d", errno);
        return false;
    }

    /* set to non blocking */
    int flags = fcntl(runtime.pipefd[0], F_GETFL, 0);
    if(flags < 0) {
        pcb_error = errno;
        SAH_TRACEZ_ERROR("pcb_con", "pipe fnctl F_GETFL failed: %s", error_string(pcb_error));
        goto error;
    }
    if(fcntl(runtime.pipefd[0], F_SETFL, flags | O_NONBLOCK) < 0) {
        pcb_error = errno;
        SAH_TRACEZ_ERROR("pcb_con", "pipe fnctl F_SETFL failed %s", error_string(pcb_error));
        goto error;
    }
    flags = fcntl(runtime.pipefd[1], F_GETFL, 0);
    if(flags < 0) {
        pcb_error = errno;
        SAH_TRACEZ_ERROR("pcb_con", "pipe fnctl F_GETFL failed: %s", error_string(pcb_error));
        goto error;
    }
    if(fcntl(runtime.pipefd[1], F_SETFL, flags | O_NONBLOCK) < 0) {
        pcb_error = errno;
        SAH_TRACEZ_ERROR("pcb_con", "pipe fnctl F_SETFL failed %s", error_string(pcb_error));
        goto error;
    }

#ifdef OPEN_SSL_SUPPORT
    if(runtime.sslEnabled) {
        connection_initSSL();
    }
#endif

    pcb_timer_initializeTimers();

    runtime.initialized = true;
    return true;

error:
    close(runtime.pipefd[0]);
    close(runtime.pipefd[1]);
    runtime.pipefd[0] = -1;
    runtime.pipefd[1] = -1;
    return false;
}

/**
   @ingroup pcb_socket_layer_connections
   @brief
   Clean up the eventing system.

   @details
   This function will free up all recources allocated for the eventing system.\n
   It will also cleanup the SSL library and the global timers
 */
void connection_exitLibrary(void) {
    if(!runtime.initialized) {
        return;
    }

    runtime.initialized = false;

    if(pthread_sigmask(SIG_SETMASK, &runtime.sig_orig_set, NULL)) {
        SAH_TRACEZ_ERROR("pcb_con", "Failed to unblock all signals %d", errno);
    }
    close(runtime.pipefd[0]);
    close(runtime.pipefd[1]);
    runtime.pipefd[0] = -1;
    runtime.pipefd[1] = -1;

#ifdef OPEN_SSL_SUPPORT
    if(runtime.sslEnabled) {
        connection_cleanupSSL();
    }
#endif

    pcb_timer_cleanupTimers();
}

/**
   @ingroup pcb_socket_layer_connections
   @brief
   Enables or disables SSL functionality.

   @details
   By default the SSL functionality is enabled (if the library is compiled with ssl support).
   When SSL is not needed call this function before calling @ref connection_initLibrary.
   After the call to @ref connection_initLibrary this function has no effect anymore.

   @param enabled true to enable SSL, false to disable SSL.
 */
void connection_enableSSL(bool enabled) {
    if(!runtime.initialized) {
        runtime.sslEnabled = enabled;
    }
}

/**
   @ingroup pcb_socket_layer_connections
   @brief
   Enables or disables blocking of the signals

   @details
   By default the library blocks all signals except when entering the select.
   When blocking of the signals is unwanted, call this function before calling @ref connection_initLibrary.
   After the call to @ref connection_initLibrary this function has no effect anymore.

   @param block true to enable blocking of the signals, false to disable blocking of the signals.
 */
void connection_blockSignals(bool block) {
    if(!runtime.initialized) {
        runtime.blockSignals = block;
    }
}

/**
   @ingroup pcb_socket_layer_connections
   @brief
   Create a connection object.

   @details
   This function creates and initialize a connection_info_t structure.\n
   There is no need to call the initiliwation function afterwards.

   @param name The name of the connection. This name is used for debugging purposes only and is not mandatory (may be NULL).

   @return
    - A pointer to a connection_info_t structure
    - NULL: an error has occured. See @ref pcb_error to get error details.
 */
connection_info_t* connection_create(const char* name) {
    connection_info_t* con = (connection_info_t*) calloc(1, sizeof(connection_info_t));
    if(!con) {
        pcb_error = pcb_error_no_memory;
        SAH_TRACEZ_ERROR("pcb_con", "%s: con=NULL", error_string(pcb_error));
        return NULL;
    }

    if(!connection_initialize(con, name)) {
        SAH_TRACEZ_ERROR("pcb_con", "connection_initialize failed (%s)", error_string(pcb_error));
        free(con);
        return NULL;
    }

    pcb_error = pcb_ok;
    return con;
}

/**
   @ingroup pcb_socket_layer_connections
   @brief
   Initialize a connection_info_t structure

   @details
   Initializes all elements of an connection_info_t structure.
   Call this function when the object is allocated on the stack.

   @param con  A pointer to a connection structure
   @param name The name of the connection. This name is used for debugging purposes only and is not mandatory (may be NULL).

   @return
    - true: initialization was successful.
    - false: an error has occured
 */
bool connection_initialize(connection_info_t* con, const char* name) {
    if(!con) {
        pcb_error = pcb_error_invalid_parameter;
        SAH_TRACEZ_ERROR("pcb_con", "%s: con=NULL", error_string(pcb_error));
        return false;
    }

    con->name = NULL;
    con->accept = NULL;
    con->eventsProcessedHandler = NULL;
    con->userData = NULL;
    con->ai_family = AF_UNSPEC;
#ifdef OPEN_SSL_SUPPORT
    con->ssl_ctx = NULL;
    con->attrib = 0;
    con->verify_server = NULL;
    con->verify_client = NULL;
    con->ssl_session_pre_hook = NULL;
    con->sni = NULL;
#endif

    /* set the default signal handler for SIGALRM */
    struct sigaction act = {.sa_handler = signalHandler, .sa_flags = SA_RESTART};
    sigaction(SIGALRM, &act, NULL);
    sigaction(SIGCHLD, &act, NULL);

    if(name && strlen(name)) {
        con->name = calloc(1, strlen(name) + 1);
        if(!con->name) {
            pcb_error = pcb_error_no_memory;
            SAH_TRACEZ_ERROR("pcb_con", "%s: con->name=NULL", error_string(pcb_error));
            goto error;
        }
        strcpy(con->name, name);
    }

    if(!llist_initialize(&con->listenSockets)) {
        SAH_TRACEZ_ERROR("pcb_con", "Failed to inialize listen sockets list (%s)", error_string(pcb_error));
        goto error_free_name;
    }

    if(!llist_initialize(&con->connections)) {
        SAH_TRACEZ_ERROR("pcb_con", "Failed to inialize connections list (%s)", error_string(pcb_error));
        goto error_free_name;
    }

    if(!llist_initialize(&con->signalHandlers)) {
        SAH_TRACEZ_ERROR("pcb_con", "Failed to inialize signal handlers list (%s)", error_string(pcb_error));
        goto error_free_name;
    }

    if(!llist_initialize(&con->connectHandlers)) {
        goto error_free_name;
    }

    if(!llist_initialize(&con->activeProcesses)) {
        goto error_free_name;
    }

    llist_iterator_initialize(&con->it);

    pcb_error = pcb_ok;
    return true;

error_free_name:
    free(con->name);
error:
    return false;
}

#ifdef OPEN_SSL_SUPPORT
/**
   @ingroup pcb_socket_layer_connections
   @brief
   Setup a connection object for using SSL functionally

   @details
   Setup a connection object for using SSL/TLS. After calling this function
   ssl enabled sockets can be created.
   The following attibutes are set:
    - trusted list of certificates (caFile), mostly used in clients
    - certificate for the server (mandatory) or client (optionally).
    - certificate verification depth is set to 9
    - SSL2 is disabled
    - cipher list is defined as follows: ALL:!ADH:!LOW:!EXP:!MD5:@@STRENGTH
    - a default server cirtificate verify callback is set
    - the connection name is set as the SSL session id.
    - the environment variable DEFAULT_CIPHER_SUITE can override the default cipher list

   @param con  A pointer to a connection structure
   @param caFile File containing all trusted certificates (mostly CA certificates, others are not recommended)
   @param certFile Certification file used if you are creating a server
   @param attr attributes to set some extra SSL stuff

   @return
    - true: ssl setup was successful..
    - false: an error has occured
 */
bool connection_setupSSL(connection_info_t* con, const char* caFile, const char* certFile, uint32_t attr) {
    if(!con) {
        pcb_error = pcb_error_invalid_parameter;
        SAH_TRACEZ_ERROR("pcb_con", "%s: con=NULL", error_string(pcb_error));
        return false;
    }

    if(!connection_createSSL(con, attr)) {
        goto error;
    }
    if(caFile && *caFile) {
        if(SSL_CTX_load_verify_locations(con->ssl_ctx, caFile, NULL) != 1) {
            SAH_TRACEZ_ERROR("pcb_con", "Error loading CA file %s", caFile);
            SAH_TRACEZ_ERROR("pcb_con", "   SSL error %s", ERR_error_string(ERR_get_error(), NULL));
            goto error_free_ssl;
        }
        if(SSL_CTX_set_default_verify_paths(con->ssl_ctx) != 1) {
            SAH_TRACEZ_ERROR("pcb_con", "Error loading default CA file and/or directory");
            SAH_TRACEZ_ERROR("pcb_con", "   SSL error %s", ERR_error_string(ERR_get_error(), NULL));
            goto error_free_ssl;
        }
    }
    if(certFile && *certFile) {
        if(SSL_CTX_use_certificate_chain_file(con->ssl_ctx, certFile) != 1) {
            SAH_TRACEZ_ERROR("pcb_con", "Error loading certificate from file %s", certFile);
            SAH_TRACEZ_ERROR("pcb_con", "   SSL error %s", ERR_error_string(ERR_get_error(), NULL));
            goto error;
        }
        if(SSL_CTX_use_PrivateKey_file(con->ssl_ctx, certFile, SSL_FILETYPE_PEM) != 1) {
            SAH_TRACEZ_ERROR("pcb_con", "Error loading private key from file %s", certFile);
            SAH_TRACEZ_ERROR("pcb_con", "   SSL error %s", ERR_error_string(ERR_get_error(), NULL));
            goto error_free_ssl;
        }
    }
    return true;

error_free_ssl:
    SSL_CTX_free(con->ssl_ctx);
    ERR_clear_error();
    con->ssl_ctx = NULL;
error:
    return false;
}

/**
   @ingroup pcb_socket_layer_connections
   @brief
   Setup a connection object for using SSL functionally

   @details
   Setup a connection object for using SSL/TLS. After calling this function
   ssl enabled sockets can be created.
   The following attibutes are set:
    - certificate verification depth is set to 9
    - SSL2 is disabled
    - cipher list is defined as follows: ALL:!ADH:!LOW:!EXP:!MD5:@@STRENGTH
    - a default server cirtificate verify callback is set
    - the connection name is set as the SSL session id.
    - the environment variable DEFAULT_CIPHER_SUITE can override the default cipher list

   @param con  A pointer to a connection structure
   @param attr attributes to set some extra SSL stuff

   @return
    An initialized SSL_CTX structure or NULL when an error occured.
 */
SSL_CTX* connection_createSSL(connection_info_t* con, uint32_t attr) {
    const char* cipher_list = CONFIG_DEFAULT_CIPHER_SUITE_LIST;
    long options = 0;


    if(!con) {
        pcb_error = pcb_error_invalid_parameter;
        SAH_TRACEZ_ERROR("pcb_con", "%s: con=NULL", error_string(pcb_error));
        return NULL;
    }

    if(con->ssl_ctx) {
        SSL_CTX_free(con->ssl_ctx);
        ERR_clear_error();
        con->ssl_ctx = NULL;
    }

    con->ssl_ctx = SSL_CTX_new(SSLv23_method());
    if(!con->ssl_ctx) {
        goto error;
    }

    if(attr & ssl_attribute_peer_not_verified) {
        SSL_CTX_set_verify(con->ssl_ctx, SSL_VERIFY_NONE, NULL);
    } else {
        int mode = SSL_VERIFY_PEER;
        if(attr & ssl_attribute_need_client_cert) {
            mode |= SSL_VERIFY_FAIL_IF_NO_PEER_CERT;
        }

        if(attr & ssl_attribute_peer_verified_once) {
            mode |= SSL_VERIFY_CLIENT_ONCE;
        }

        SSL_CTX_set_verify(con->ssl_ctx, mode, connection_verify_cert_callback);
        SSL_CTX_set_verify_depth(con->ssl_ctx, 9);
    }
    con->attrib = attr;

    /* set ssl options */
#ifdef CONFIG_SAH_LIB_OPENSSL_NO_DEPRECATED_TLS
    options = SSL_OP_ALL | SSL_OP_NO_SSLv2 | SSL_OP_NO_SSLv3 | SSL_OP_NO_COMPRESSION | SSL_OP_NO_TLSv1 | SSL_OP_NO_TLSv1_1 | SSL_OP_NO_RENEGOTIATION;
#else
    options = SSL_OP_ALL | SSL_OP_NO_SSLv2 | SSL_OP_NO_SSLv3 | SSL_OP_NO_COMPRESSION;
#endif

    if(attr & ssl_attribute_no_resumption_on_renegotiation) {
        options |= SSL_OP_NO_SESSION_RESUMPTION_ON_RENEGOTIATION;
    }

    SSL_CTX_set_options(con->ssl_ctx, options);

#if OPENSSL_VERSION_NUMBER >= 0x30000000L
    SSL_CTX_set_dh_auto(con->ssl_ctx, 1);
#else
    DH* dh = NULL;
    dh = get_dh2048();
    if(SSL_CTX_set_tmp_dh(con->ssl_ctx, dh) != 1) {
        SAH_TRACEZ_ERROR("pcb_con", "Error setting temporary DH parameters");
    }
    DH_free(dh);

    EC_KEY* key = EC_KEY_new_by_curve_name(NID_X9_62_prime256v1);
    if(!key) {
        SAH_TRACEZ_ERROR("pcb_con", "Error getting new DH EC KEY");
    } else {
        if(SSL_CTX_set_tmp_ecdh(con->ssl_ctx, key) != 1) {
            SAH_TRACEZ_ERROR("pcb_con", "Error setting temporary DH EC KEY");
        }
        EC_KEY_free(key);
    }
#endif
    if(!(attr & ssl_attribute_no_env_cipher_list)) {
        cipher_list = getenv("DEFAULT_CIPHER_SUITE");
        if(cipher_list == NULL) {
            cipher_list = CONFIG_DEFAULT_CIPHER_SUITE_LIST;
        } else {
            SAH_TRACEZ_INFO("pcb_con", "Setting SSL cipher list due to environment variable to '%s'", cipher_list);
        }
    }
    if(SSL_CTX_set_cipher_list(con->ssl_ctx, cipher_list) != 1) {
        SAH_TRACEZ_ERROR("pcb_con", "Error setting cipher list (no valid ciphers)");
        goto error_free_ssl;
    }
    SSL_CTX_set_session_id_context(con->ssl_ctx, (unsigned char*) con->name, strlen(con->name));

    return con->ssl_ctx;

error_free_ssl:
    SSL_CTX_free(con->ssl_ctx);
    ERR_clear_error();
    con->ssl_ctx = NULL;
error:
    return NULL;
}

/**
   @ingroup pcb_socket_layer_connections
   @brief
   Get the SSL_CTX

   @details
   Get the SSL_CTX structure.

   @param con  A pointer to a connection structure

   @return
    The SSL_CTX structure or NULL when SSL was not initialized for this connection.
 */
SSL_CTX* connection_getSSL(connection_info_t* con) {
    if(!con) {
        pcb_error = pcb_error_invalid_parameter;
        SAH_TRACEZ_ERROR("pcb_con", "%s: con=NULL", error_string(pcb_error));
        return NULL;
    }

    return con->ssl_ctx;
}

/**
   @ingroup pcb_socket_layer_connections
   @brief
   Set a callback function to verify the server's certificate

   @details
   Set a callback function to verify the server's certificate.
   This must be done before making a connection to the server.

   @param con  A pointer to a connection structure
   @param fn The callback function

   @return
    - true: function is set.
    - false: failed to set the function
 */
bool connection_setServerVerify(connection_info_t* con, verify_server_certificate_t fn) {
    if(!con) {
        pcb_error = pcb_error_invalid_parameter;
        SAH_TRACEZ_ERROR("pcb_con", "%s: con=NULL", error_string(pcb_error));
        return false;
    }

    con->verify_server = fn;
    return true;
}

/**
   @ingroup pcb_socket_layer_connections
   @brief
   Set a callback function to verify the client's certificate

   @details
   Set a callback function to verify the client's certificate.
   This must be done before accepting a connection from a client.

   @param con  A pointer to a connection structure
   @param fn The callback function

   @return
    - true: function is set.
    - false: failed to set the function
 */
bool connection_setClientVerify(connection_info_t* con, verify_client_certificate_t fn) {
    if(!con) {
        pcb_error = pcb_error_invalid_parameter;
        SAH_TRACEZ_ERROR("pcb_con", "%s: con=NULL", error_string(pcb_error));
        return false;
    }

    con->verify_client = fn;
    return true;
}

/**
   @ingroup pcb_socket_layer_connections
   @brief
   Set a callback function to verify the peer's certificate

   @details
   Set a callback function to verify the peer's certificate.
   This must be done before accepting a connection from a client,
   or connecting to a server.

   @param con  A pointer to a connection structure
   @param fn The callback function

   @return
    - true: function is set.
    - false: failed to set the function
 */
bool connection_setPeerVerify(connection_info_t* con, verify_peer_certificate_t fn) {
    if(!con) {
        pcb_error = pcb_error_invalid_parameter;
        SAH_TRACEZ_ERROR("pcb_con", "%s: con=NULL", error_string(pcb_error));
        return false;
    }

    con->verify_peer = fn;
    return true;
}


/**
   @ingroup pcb_socket_layer_connections
   @brief
   Set server name Indication for certificate

   @details
   sets the field con->sni to contain the value of URL parameter
   this value is used to configure of the server name indication in a ClientHello message
   which tells the server which hostname we're attempting to contact.

   @param con  A pointer to a connection structure
   @param server_name_indication A pointer to string which contains SNI

   @return
    - true: con-> sni field is successfully set
    - false: failed to set the con->sni field
 */
bool connection_setServerNameIndication(connection_info_t* con, const char* server_name_indication) {
    if(!con) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }
    if(!server_name_indication || !(*server_name_indication)) {
        pcb_error = pcb_error_invalid_parameter;
        SAH_TRACEZ_ERROR("pcb_con", "%s: SNI=NULL", error_string(pcb_error));
        return false;
    }

    free(con->sni);
    con->sni = server_name_indication ? strdup(server_name_indication) : NULL;
    return true;
}


/**
   @brief
   Set a callback function to set username after abbreviated ssl handshake

   @details
   Set a callback function to set username after abbreviated ssl handshake.
   The username would be taking from a keystore by looking up the certificate's pubkey.

   @param con  A pointer to a connection structure
   @param fn The callback function

   @return
    - true: function is set.
    - false: failed to set the function
 */
bool connection_setReuseUsername(connection_info_t* con, reuse_username_t fn) {
    if(!con) {
        pcb_error = pcb_error_invalid_parameter;
        SAH_TRACEZ_ERROR("pcb_con", "%s: con=NULL", error_string(pcb_error));
        return false;
    }

    con->reuse_username = fn;
    return true;
}

void connection_addSSLAttribute(connection_info_t* con, uint32_t attr) {
    if(!con) {
        pcb_error = pcb_error_invalid_parameter;
        SAH_TRACEZ_ERROR("pcb_con", "%s: con=NULL", error_string(pcb_error));
        return;
    }

    con->attrib |= attr;
}

void connection_delSSLAttribute(connection_info_t* con, uint32_t attr) {
    if(!con) {
        pcb_error = pcb_error_invalid_parameter;
        SAH_TRACEZ_ERROR("pcb_con", "%s: con=NULL", error_string(pcb_error));
        return;
    }

    con->attrib &= ~attr;
}

/**
   @ingroup pcb_socket_layer_connections
   @brief
   Set the SSL cipher list for this connection

   @details
   Sets the SSL cipher list used by the SSL library
   to identify acceptable cipher suites for when negotiating
   a common cipher suite with the remote end.

   @param con  A pointer to a connection structure
   @param cipher_list The cipher list (as defined by 'man ciphers(1)')

   @return
    - true: cipher list is successfully set
    - false: failed to set the cipher list
 */
bool connection_setCipherList(connection_info_t* con, const char* cipher_list) {
    if(!con) {
        pcb_error = pcb_error_invalid_parameter;
        SAH_TRACEZ_ERROR("pcb_con", "%s: con=NULL", error_string(pcb_error));
        return false;
    }
    if(!con->ssl_ctx) {
        pcb_error = pcb_error_invalid_parameter;
        SAH_TRACEZ_ERROR("pcb_con", "%s: No SSL CTX for connection", error_string(pcb_error));
        return false;
    }
    if(!cipher_list) {
        pcb_error = pcb_error_invalid_parameter;
        SAH_TRACEZ_ERROR("pcb_con", "%s: cipher_list=NULL", error_string(pcb_error));
        return false;
    }

    if(SSL_CTX_set_cipher_list(con->ssl_ctx, cipher_list) != 1) {
        pcb_error = pcb_error_invalid_parameter;
        SAH_TRACEZ_ERROR("pcb_con", "%s: Error setting cipher list (no valid ciphers)", error_string(pcb_error));
        return false;
    }

    return true;
}

/**
   @ingroup pcb_socket_layer_connections
   @brief
   Set the pre SSL connection hook for this connection

   @details
   Sets the pre SSL connection hook to be able to manipulate the SSL object
   before any connection to a server is made.

   @param con A pointer to a connection structure
   @param fn The callback function

   @return
    - true: function is set.
    - false: failed to set the function
 */
bool connection_setPreSSLConnectionHook(connection_info_t* con, connection_ssl_session_hook_t fn) {
    if(!con) {
        pcb_error = pcb_error_invalid_parameter;
        SAH_TRACEZ_ERROR("pcb_con", "%s: con=NULL", error_string(pcb_error));
        return false;
    }

    con->ssl_session_pre_hook = fn;

    return true;
}
#endif

/**
   @ingroup pcb_socket_layer_connections
   @brief
   Clean up of a connection_info_t structure

   @details
   Clean up all elements of an connection_info_t structure

   @param con  A pointer to a connection structure
 */
void connection_cleanup(connection_info_t* con) {
    if(!con) {
        pcb_error = pcb_error_invalid_parameter;
        SAH_TRACEZ_ERROR("pcb_con", "%s: con=NULL", error_string(pcb_error));
        return;
    }

    /* remove this connection from any list it is in. */
    llist_iterator_take(&con->it);

    /* delete all listen sockets */
    connection_t* c = NULL;
    llist_iterator_t* it = llist_first(&con->listenSockets);
    llist_iterator_t* prefetch = llist_iterator_next(it);
    while(it != NULL) {
        c = llist_item_data(it, connection_t, it);
        peer_destroy(&c->info);
        peer_delete(&c->info);
        it = prefetch;
        prefetch = llist_iterator_next(it);
    }

    /* delete all sockets */
    it = llist_first(&con->connections);
    prefetch = llist_iterator_next(it);
    while(it != NULL) {
        c = llist_item_data(it, connection_t, it);
        peer_destroy(&c->info);
        peer_delete(&c->info);
        it = prefetch;
        prefetch = llist_iterator_next(it);
    }

    /* delete all signal handlers */
    it = llist_takeFirst(&con->signalHandlers);
    signal_handler_t* sh = NULL;
    while(it) {
        sh = llist_item_data(it, signal_handler_t, it);
        free(sh);
        it = llist_takeFirst(&con->signalHandlers);
    }

    free(con->name);

    /* remove all connect handlers */
    it = llist_takeFirst(&con->connectHandlers);
    peer_handler_list_t* ch = NULL;
    while(it) {
        ch = llist_item_data(it, peer_handler_list_t, it);
        free(ch);
        it = llist_takeFirst(&con->connectHandlers);
    }

#ifdef OPEN_SSL_SUPPORT
    if(con->ssl_ctx) {
        SSL_CTX_free(con->ssl_ctx);
    }
    free(con->sni);
#endif

    pcb_error = pcb_ok;
    return;
}

/**
   @ingroup pcb_socket_layer_connections
   @brief
   Destroy the connection object

   @details
   This function will close all open sockets and free up the memory used by the connection structure.

   @param con A pointer to a connection structure
 */
void connection_destroy(connection_info_t* con) {
    if(!con) {
        pcb_error = pcb_error_invalid_parameter;
        SAH_TRACEZ_ERROR("pcb_con", "%s: con=NULL", error_string(pcb_error));
        return;
    }

    connection_cleanup(con);
    free(con);
    pcb_error = pcb_ok;
}

/**
   @ingroup pcb_socket_layer_connections
   @brief
   Get the connection name.

   @details
   Returns the name of the.connection

   @param con A pointer to a connection structure

   @return
    - The name of the connection.
 */
const char* connection_name(connection_info_t* con) {
    if(!con) {
        pcb_error = pcb_error_invalid_parameter;
        SAH_TRACEZ_ERROR("pcb_con", "%s: con=NULL", error_string(pcb_error));
        return NULL;
    }

    return con->name;
}

/**
   @ingroup pcb_socket_layer_connections
   @brief
   Set the address family for the DNS resolution.

   @details
   Returns true if success else return false

   @param con A pointer to a connection structure
   @param con A pointer to a connection structure

   @return
    - true : the address family is correclty set
    - false : the set of the address family has failed.
 */
bool connection_setAddressFamily(connection_info_t* con, int ai_family) {
    if(!con) {
        pcb_error = pcb_error_invalid_parameter;
        SAH_TRACEZ_ERROR("pcb_con", "%s: con=NULL", error_string(pcb_error));
        return false;
    }

    con->ai_family = ai_family;
    return true;
}

/**
   @ingroup pcb_socket_layer_connections
   @brief
   Create the listen socket of IPC

   @details
   Creates the listen socket to accept IPC connections.

   @param con A pointer to a connection structure
   @param path The IPC socket name This is a file name on your local system and can be an absolute path or a relative path.

   @return
    - true: the socket has been created
    - false: an error has occured. See @ref pcb_error to get error details.
 */
peer_info_t* connection_listenOnIPC(connection_info_t* con, const char* path) {
    if(!con || !path || !(*path)) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    struct sockaddr_un local;
    local.sun_family = AF_UNIX;
    strncpy(local.sun_path, path, 107);
    int len = strlen(local.sun_path) + sizeof(local.sun_family);

    llist_iterator_t* it = NULL;
    llist_for_each(it, &con->listenSockets) {
        connection_t* c = llist_item_data(it, connection_t, it);
        if(c->info.addrInfo.ai_addr && (memcmp(c->info.addrInfo.ai_addr, &local, len) == 0)) {
            SAH_TRACEZ_ERROR("pcb_con", "Already listening on %s", path);
            return NULL;
        }
    }

    connection_t* ep = (connection_t*) calloc(1, sizeof(connection_t));
    if(!ep) {
        pcb_error = pcb_error_no_memory;
        return NULL;
    }

    peer_info_t* peer = &ep->info;
    if(!peer_initialize(peer)) {
        goto error_initialize;
    }

    peer->socketfd = socket(AF_UNIX, SOCK_STREAM, 0);
    if(peer->socketfd == -1) {
        pcb_error = errno;
        SAH_TRACEZ_ERROR("pcb_con", "socket creation failed (%s)", error_string(pcb_error));
        goto error_delete_socket;
    }

    unlink(local.sun_path);

    if(bind(peer->socketfd, (struct sockaddr*) &local, len) == -1) {
        pcb_error = errno;
        SAH_TRACEZ_ERROR("pcb_con", "socket bind failed (%s)", error_string(pcb_error));
        goto error_close_socket;
    }

    if(listen(peer->socketfd, SOMAXCONN) == -1) {
        pcb_error = errno;
        SAH_TRACEZ_ERROR("pcb_con", "socket listen failed (%s)", error_string(pcb_error));
        goto error_close_socket;
    }

    /* store address info */
    memset(&peer->addrInfo, 0, sizeof(struct addrinfo));
    peer->addrInfo.ai_addrlen = len;
    peer->addrInfo.ai_addr = (struct sockaddr*) calloc(1, len + 1);
    if(peer->addrInfo.ai_addr) {
        memcpy(peer->addrInfo.ai_addr, &local, len);
    }

    peer->type = peer_type_listen;
    peer->state = peer_state_listening;

    peer_setEventHandler(peer, peer_event_accepted, 0, con->accept);
    llist_append(&con->listenSockets, &ep->it);

    pcb_error = pcb_ok;
    return peer;

error_close_socket:
    close(peer->socketfd);
error_delete_socket:
    peer_delete(peer);
    return NULL;

error_initialize:
    free(ep);
    return NULL;
}

/**
   @ingroup pcb_socket_layer_connections
   @brief
   Create the listen socket of TCP, calling a possible callback

   @details
   Creates the listen socket to accept TCP connections. If the callback argument is given, it will
   be called with the created socket between the socket() and the bind() call so that you can set
   some options on the socket (most notably the SO_BINDTODEVICE option).

   @param con A pointer to a connection structure
   @param host The IP address of the interface, from where to accept new connections or an empty string or NULL to accept connections on any interface.
   @param service Port on which to accept new connections.
   @param attr one or more flags from @ref connection_attribute_t
   @param handler A pointer to a function that will be called with the socket created between the socket() and bind() calls.

   @return
    - true: the socket has been created
    - false: an error has occured. See @ref pcb_error to get error details.
 */
peer_info_t* connection_listenOnTCPCallback(connection_info_t* con, const char* host, const char* service, uint32_t attr, connection_socket_handler_t handler) {
    if(!con) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    connection_t* ep = (connection_t*) calloc(1, sizeof(connection_t));
    if(!ep) {
        pcb_error = pcb_error_no_memory;
        return NULL;
    }

    pcb_error_t olderr;
    int status;
    struct addrinfo hints;
    struct addrinfo* servinfo; /* will point to the results */
    struct addrinfo* p;
    int sockfd = -1;
    int yes = 1;

    peer_info_t* peer = &ep->info;
    if(!peer_initialize(peer)) {
        goto error_initialize;
    }

    memset(&hints, 0, sizeof(hints)); /* make sure the struct is empty */
    hints.ai_family = con->ai_family;
    hints.ai_socktype = SOCK_STREAM;  /* TCP stream sockets */
    hints.ai_flags = AI_PASSIVE;      /* fill in my IP for me */
    status = getaddrinfo(host, service, &hints, &servinfo);
    if(status != 0) {
        pcb_error = (uint32_t) (((uint16_t) (-status) & 0x0000FFFF) | PCB_GETADDRINFO_BASE);
        SAH_TRACEZ_ERROR("pcb_con", "getaddrinfo failed %s", error_string(pcb_error));
        goto error_delete_socket;
    }

    /* loop through all the results and bind to the first we can */
    for(p = servinfo; p != NULL; p = p->ai_next) {
        sockfd = socket(p->ai_family, p->ai_socktype, p->ai_protocol);
        if(sockfd == -1) {
            continue;
        }
        if(setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1) {
            close(sockfd);
            break;
        }

        if(handler && !handler(sockfd)) {
            close(sockfd);
            break;
        }

        if(bind(sockfd, p->ai_addr, p->ai_addrlen) == -1) {
            pcb_error = errno;
            SAH_TRACEZ_ERROR("pcb_con", "socket bind failed (%s)", error_string(pcb_error));
            close(sockfd);
            continue;
        }
        peer->socketfd = sockfd;
        break;
    }
    freeaddrinfo(servinfo);
    if(peer->socketfd == -1) {
        SAH_TRACEZ_ERROR("pcb_con", "no socket bound (%s)", error_string(pcb_error));
        goto error_close_socket;
    }

    if(listen(peer->socketfd, 5) == -1) {
        pcb_error = errno;
        SAH_TRACEZ_ERROR("pcb_con", "socket listen failed (%s)", error_string(pcb_error));
        close(peer->socketfd);
        peer->socketfd = -1;
        goto error_close_socket;
    }

    peer->type = peer_type_listen;
    peer->state = peer_state_listening;

    peer_setEventHandler(peer, peer_event_accepted, 0, con->accept);
    peer_addCloseHandler(peer, connection_listenSocketClosed);

    llist_append(&con->listenSockets, &ep->it);

#ifdef OPEN_SSL_SUPPORT
    if(attr & connection_attr_ssl) {
        peer->ssl_bio = BIO_new(BIO_s_accept());
        if(!peer->ssl_bio) {
            SAH_TRACEZ_ERROR("pcb_con", "Error creating server bio");
            goto error_initialize;
        }
        BIO_set_fd(peer->ssl_bio, peer->socketfd, 1);
    }
#else
    (void) attr; /* to remove compile warning */
#endif

    return peer;

error_close_socket:
    if(peer->socketfd >= 0) {
        close(peer->socketfd);
    }
error_delete_socket:
    olderr = pcb_error;
    peer_delete(peer);
    pcb_error = olderr;
    return NULL;

error_initialize:
    free(ep);
    return NULL;
}

/**
   @ingroup pcb_socket_layer_connections
   @brief
   Create the listen socket of TCP

   @details
   Creates the listen socket to accept TCP connections.

   @param con A pointer to a connection structure
   @param host The IP address of the interface, from where to accept new connections or an empty string or NULL to accept connections on any interface.
   @param service Port on which to accept new connections.
   @param attr one or more flags from @ref connection_attribute_t

   @return
    - true: the socket has been created
    - false: an error has occured. See @ref pcb_error to get error details.
 */
peer_info_t* connection_listenOnTCP(connection_info_t* con, const char* host, const char* service, uint32_t attr) {
    if(!con) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }
    return connection_listenOnTCPCallback(con, host, service, attr, NULL);
}

/**
   @ingroup pcb_socket_layer_connections
   @brief
   Create a IPC connection to a server

   @details
   Connect to a server accepting IPC connections.

   @param con A pointer to a connection structure
   @param path The IPC socket path This is a file name on your local system and can be an absolute path or a relative path.

   @return
    - true: the socket has been created
    - false: an error has occured. See @ref pcb_error to get error details.
 */
peer_info_t* connection_connectToIPC(connection_info_t* con, const char* path) {
    int len;
    struct sockaddr_un remote;
    connection_t* ep = NULL;
    llist_iterator_t* it = NULL;
    peer_handler_list_t* connectHandler = NULL;
    peer_info_t* peer = NULL;

    if(!con || !path) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    ep = (connection_t*) calloc(1, sizeof(connection_t));
    if(!ep) {
        pcb_error = pcb_error_no_memory;
        return NULL;
    }

    peer = &ep->info;
    if(!peer_initialize(peer)) {
        goto error_initialize;
    }

    /* create socket */
    peer->socketfd = socket(AF_UNIX, SOCK_STREAM, 0);
    if(peer->socketfd == -1) {
        pcb_error = errno;
        SAH_TRACEZ_ERROR("pcb_con", "socket creation failed %s", error_string(pcb_error));
        goto error_cleanup_peer;
    }

    /* set to non blocking */
    if(!connection_setNonBlocking(peer)) {
        pcb_error = errno;
        goto error_close_peer;
    }

    /* try to connect */
    remote.sun_family = AF_UNIX;
    strncpy(remote.sun_path, path, 107);
    len = strlen(remote.sun_path) + sizeof(remote.sun_family);
    if(connect(peer->socketfd, (struct sockaddr*) &remote, len) == -1) {
        pcb_error = errno;
        SAH_TRACEZ_NOTICE("pcb_con", "socket connect failed %s", error_string(pcb_error));
        goto error_close_peer;
    }

    /* store address info */
    memset(&peer->addrInfo, 0, sizeof(struct addrinfo));
    peer->addrInfo.ai_addrlen = len;
    peer->addrInfo.ai_addr = (struct sockaddr*) calloc(1, len + 1);
    if(peer->addrInfo.ai_addr) {
        memcpy(peer->addrInfo.ai_addr, &remote, len);
    }

    peer->type = peer_type_server_connection;
    peer->state = peer_state_connected;

    llist_append(&con->connections, &ep->it);

    llist_for_each(it, &con->connectHandlers) {
        connectHandler = llist_item_data(it, peer_handler_list_t, it);
        if(connectHandler->handler_function) {
            connectHandler->handler_function(peer);
        }
    }

    pcb_error = pcb_ok;
    return peer;

error_close_peer:
    close(peer->socketfd);
error_cleanup_peer:
    peer_delete(peer);
    return NULL;

error_initialize:
    free(ep);

    return NULL;
}

/**
   @ingroup pcb_socket_layer_connections
   @brief
   Create a TCP connection to a server

   @details
   Connect to a server accepting TCP connections.

   @param con A pointer to a connection structure
   @param host The IP address or a resolvable network name of the server
   @param service The port
   @param attr one or more flags from @ref connection_attribute_t
   @param handler A pointer to a function that will be called with the socket created between the socket() and connect() calls.

   @return
    - true: the socket has been created
    - false: an error has occured. See @ref pcb_error to get error details.
 */
peer_info_t* connection_connectToTCPCallback(connection_info_t* con, const char* host, const char* service, uint32_t attr, connection_socket_handler_t handler) {
    llist_iterator_t* it = NULL;
    peer_handler_list_t* connectHandler = NULL;
    peer_info_t* peer = NULL;
    int status;
    struct addrinfo hints;
    struct addrinfo* res = NULL;
    struct addrinfo* p;
    connection_t* ep = NULL;

    if(!con) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    ep = (connection_t*) calloc(1, sizeof(connection_t));
    if(!ep) {
        pcb_error = pcb_error_no_memory;
        return NULL;
    }

    peer = &ep->info;
    if(!peer_initialize(peer)) {
        goto error_socket;
    }

    memset(&hints, 0, sizeof hints);
    hints.ai_family = con->ai_family;
    hints.ai_socktype = SOCK_STREAM;

    status = getaddrinfo(host, service, &hints, &res);
    if((status == -1) || (res == NULL)) {
        pcb_error = (uint32_t) ((uint16_t) status & 0x0000FFFF);
        goto error_delete_socket;
    }

    /* loop through all the results and connect to the first we can */
    for(p = res; p != NULL; p = p->ai_next) {
        peer->socketfd = socket(p->ai_family, p->ai_socktype, p->ai_protocol);
        if(peer->socketfd == -1) {
            continue;
        }
        if((attr & connection_attr_async_connect) && !connection_setNonBlocking(peer)) {
            pcb_error = errno;
            goto error_free_res;
        }
        if(handler && !handler(peer->socketfd)) {
            close(peer->socketfd);
            peer->socketfd = -1;
            break;
        }

        if(connect(peer->socketfd, p->ai_addr, p->ai_addrlen) == -1) {
            if(errno == EINPROGRESS) {
                if(attr & connection_attr_async_connect) {
                    peer->type = peer_type_server_connection;
                    peer->state = peer_state_connecting;
                    /* connection is in progress */
                    break;
                } else {
                    pcb_error = errno;
                    SAH_TRACEZ_ERROR("pcb_con", "socket connect failed %s", error_string(pcb_error));
                    close(peer->socketfd);
                    peer->socketfd = -1;
                    continue;
                }
            } else {
                pcb_error = errno;
                SAH_TRACEZ_ERROR("pcb_con", "socket connect failed %s", error_string(pcb_error));
                close(peer->socketfd);
                peer->socketfd = -1;
                continue;
            }
        } else {
            peer->type = peer_type_server_connection;
            peer->state = peer_state_connected;
        }
        if(!(attr & connection_attr_async_connect) && !connection_setNonBlocking(peer)) {
            pcb_error = errno;
            goto error_free_res;
        }
        break;
    }
    if(peer->socketfd == -1) {
        goto error_free_res;
    }

    memcpy(&peer->addrInfo, p, sizeof(struct addrinfo));
    peer->addrInfo.ai_addr = (struct sockaddr*) calloc(1, peer->addrInfo.ai_addrlen);
    if(peer->addrInfo.ai_addr) {
        memcpy(peer->addrInfo.ai_addr, p->ai_addr, peer->addrInfo.ai_addrlen);
    }

    llist_append(&con->connections, &ep->it);

#ifdef OPEN_SSL_SUPPORT
    if(attr & connection_attr_ssl) {
        peer->ssl_bio = BIO_new_fd(peer->socketfd, BIO_NOCLOSE);
        if(!peer->ssl_bio) {
            SAH_TRACEZ_ERROR("pcb_con", "Failed to create connectiong BIO");
            goto error_take_connection;
        }
        peer->ssl = SSL_new(con->ssl_ctx);
        SAH_TRACEZ_INFO("ssl", "Allocate SSL %p for peer %p", peer->ssl, peer);
        if(!peer->ssl) {
            SAH_TRACEZ_ERROR("pcb_con", "Error creating an SSL context");
            goto error_take_connection;
        }
        if(con->sni) {
            SAH_TRACEZ_INFO("ssl", "Set SNI %s for peer %p", con->sni, peer);
            if(SSL_set_tlsext_host_name(peer->ssl, con->sni) != 1) {
                pcb_error = pcb_error_communication;
                SAH_TRACEZ_ERROR("pcb_con", "%s: Error setting server name indication", error_string(pcb_error));
                goto error_take_connection;
            }
        }
        peer_ssl_setPeer(peer->ssl, peer);
        SSL_set_bio(peer->ssl, peer->ssl_bio, peer->ssl_bio);
        if(con->ssl_session_pre_hook) {
            con->ssl_session_pre_hook(con, peer, host);
        }
        SAH_TRACEZ_NOTICE("pcb_con", "SSL CONNECT");
        if(!peer_ssl_connect(con, peer, host)) {
            SAH_TRACEZ_ERROR("pcb_con", "Error connecting ssl session");
            goto error_take_connection;
        }
    }
#else
    (void) attr; /* to remove compile warning */
#endif

    llist_for_each(it, &con->connectHandlers) {
        connectHandler = llist_item_data(it, peer_handler_list_t, it);
        if(connectHandler->handler_function) {
            connectHandler->handler_function(peer);
        }
    }

    freeaddrinfo(res);
    pcb_error = pcb_ok;
    return peer;

#ifdef OPEN_SSL_SUPPORT
error_take_connection:
    if(ep) {
        llist_iterator_take(&ep->it);
    }
#endif
error_free_res:
    freeaddrinfo(res);
    if(peer->socketfd >= 0) {
        close(peer->socketfd);
    }
error_delete_socket:
    peer_delete(peer);
    return NULL;

error_socket:
    free(ep);
    return NULL;
}

/**
   @ingroup pcb_socket_layer_connections
   @brief
   Create a TCP connection to a server

   @details
   Connect to a server accepting TCP connections.

   @param con A pointer to a connection structure
   @param host The IP address or a resolvable network name of the server
   @param service The port
   @param attr one or more flags from @ref connection_attribute_t

   @return
    - true: the socket has been created
    - false: an error has occured. See @ref pcb_error to get error details.
 */
peer_info_t* connection_connectToTCP(connection_info_t* con, const char* host, const char* service, uint32_t attr) {
    if(!con) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }
    return connection_connectToTCPCallback(con, host, service, attr, NULL);
}

/**
   @ingroup pcb_socket_layer_connections
   @brief
   Create a pair of connected peers

   @details
   The connection_connectPair call creates an unnamed pair of connected IPC peers

   @param con A pointer to a connection structure
   @param peers An array of 2 peer structure that will hold the two connected peers

   @return
    - true: the socket has been created
    - false: an error has occured. See @ref pcb_error to get error details.
 */
bool connection_connectPair(connection_info_t* con, peer_info_t* peers[2]) {
    int len;
    struct sockaddr_un remote;
    int fd[2];
    int i = 0;

    connection_t* ep[2] = { NULL, NULL };
    ep[0] = (connection_t*) calloc(1, sizeof(connection_t));
    if(!ep[0]) {
        pcb_error = pcb_error_no_memory;
        return NULL;
    }

    ep[1] = (connection_t*) calloc(1, sizeof(connection_t));
    if(!ep[1]) {
        pcb_error = pcb_error_no_memory;
        goto error_initialize;
    }

    peers[0] = &(ep[0]->info);
    peers[1] = &(ep[1]->info);
    if(!peer_initialize(peers[0]) || !peer_initialize(peers[1])) {
        goto error_initialize;
    }

    remote.sun_family = AF_UNIX;
    *remote.sun_path = '\0';
    len = 1 + sizeof(remote.sun_family);

    /* create socket */
    if(socketpair(AF_UNIX, SOCK_STREAM, 0, fd) == -1) {
        pcb_error = errno;
        SAH_TRACEZ_ERROR("pcb_con", "socket creation failed %s", error_string(pcb_error));
        goto error_cleanup_peer;
    }

    peers[0]->socketfd = fd[0];
    peers[1]->socketfd = fd[1];

    for(i = 0; i < 2; i++) {
        /* set to non blocking */
        if(!connection_setNonBlocking(peers[i])) {
            pcb_error = errno;
            goto error_cleanup_peer;
        }

        /* store address info */
        memset(&peers[i]->addrInfo, 0, sizeof(struct addrinfo));
        peers[i]->addrInfo.ai_addrlen = len;
        peers[i]->addrInfo.ai_addr = (struct sockaddr*) calloc(1, len + 1);
        if(peers[i]->addrInfo.ai_addr) {
            memcpy(peers[i]->addrInfo.ai_addr, &remote, len);
        }

        peers[i]->type = peer_type_pair;
        peers[i]->state = peer_state_connected;
        llist_append(&con->connections, &ep[i]->it);
    }

    pcb_error = pcb_ok;
    return true;

error_cleanup_peer:
    peer_delete(peers[0]);
    peer_delete(peers[1]);
    return false;

error_initialize:
    free(ep[1]);
    free(ep[0]);

    return false;

}

/**
   @ingroup pcb_socket_layer_connections
   @brief
   Create a connection to a server using a URI

   @details
   Connect to a server using a URI. All infor,ation to setup the connection is retrieved fromt the URI string.
   The parsed URI is returned in the uri argument. You need to free the memory allocated for this structure
   using @ref uri_destroy.

   @param con A pointer to a connection structure
   @param URI String containing the URI
   @param uri pointer that will contain the parsed URI in a uri structure

   @return
    - true: the socket has been created
    - false: an error has occured. See @ref pcb_error to get error details.
 */
peer_info_t* connection_connect(connection_info_t* con, const char* URI, uri_t** uri) {
    peer_info_t* dest = NULL;
    if(!con || !uri || !URI || !(*URI)) {
        SAH_TRACEZ_ERROR("pcb_con", "%s: one of the mandatory parameters is NULL or an empty string", error_string(pcb_error));
        pcb_error = pcb_error_invalid_parameter;
        goto error;
    }

    *uri = uri_parse(URI);
    if(!*uri) {
        SAH_TRACEZ_ERROR("pcb_con", "uri parsing failed");
        goto error;
    }

    /* connect */
    if(strcmp(uri_getHost(*uri), "ipc") == 0) {
        SAH_TRACEZ_INFO("pcb_con", "Connecting using ipc socket %s", uri_getPort(*uri));
        dest = connection_connectToIPC(con, uri_getPort(*uri));
    } else {
        SAH_TRACEZ_INFO("pcb_con", "Connecting using tcp socket %s:%s", uri_getHost(*uri), uri_getPort(*uri));
        dest = connection_connectToTCP(con, uri_getHost(*uri), uri_getPort(*uri), connection_attr_default);
    }

    return dest;

error:
    return NULL;
}

/**
   @ingroup pcb_socket_layer_connections
   @brief
   Close all open sockets

   @details
   Close all open sockets.

   @param connection A pointer to a connection structure

   @return
    - true: all sockets are closed
    - false: an error has occured. See @ref pcb_error to get error details.
 */
bool connection_closeAll(connection_info_t* connection) {
    if(!connection) {
        SAH_TRACEZ_ERROR("pcb_con", "%s: one of the mandatory parameters is NULL or an empty string", error_string(pcb_error));
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(!connection_closeListenSockets(connection)) {
        SAH_TRACEZ_ERROR("pcb_con", "Failed to close listen sockets");
    }

    connection_t* con = NULL;
    llist_iterator_t* it = NULL;
    llist_for_each(it, &connection->connections) {
        con = llist_item_data(it, connection_t, it);
        peer_destroy(&con->info);
    }

    pcb_error = pcb_ok;
    return true;
}

/**
   @ingroup pcb_socket_layer_connections
   @brief
   Close the IPC listen socket

   @details
   After calling this function no more IPC connection can be accepted.

   @param connection A pointer to a connection structure

   @return
    - true: IPC listen socket is closed
    - false: an error has occured. See @ref pcb_error to get error details.
 */
bool connection_closeListenSockets(connection_info_t* connection) {
    if(!connection) {
        SAH_TRACEZ_ERROR("pcb_con", "%s: one of the mandatory parameters is NULL or an empty string", error_string(pcb_error));
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    connection_t* con = NULL;
    llist_iterator_t* it = NULL;
    llist_for_each(it, &connection->listenSockets) {
        con = llist_item_data(it, connection_t, it);
        peer_destroy(&con->info);
    }

    pcb_error = pcb_ok;
    return true;
}

/**
   @ingroup pcb_socket_layer_connections
   @brief
   Close server socket

   @details
   Closes and removes all server connections

   @param con A pointer to a connection object, must be created with @ref connection_create

   @return
    - true: connection to the server is closed
    - false: an error has occured. See @ref pcb_error to get error details.
 */
bool connection_closeServers(connection_info_t* con) {
    if(!con) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    connection_t* c = NULL;
    llist_iterator_t* it = NULL;
    llist_for_each(it, &con->connections) {
        c = llist_item_data(it, connection_t, it);
        if(peer_isServerConnection(&c->info)) {
            SAH_TRACEZ_INFO("pcb_con", "Removing server socket");
            peer_destroy(&c->info);
        }
    }

    pcb_error = pcb_ok;
    return true;
}

/**
   @brief
   Verify a list of peers

   @details
   This function verifies all file descriptors and closes the peers with bad ones.

   @param cons linked list of peers
   @return The number of closed file descriptors
 */
static int connection_verify_connections(llist_t* cons) {
    int closed = 0;
    llist_iterator_t* iter = NULL, * iter_next = NULL;
    connection_t* con = NULL;

    for(iter = llist_first(cons); iter; iter = iter_next) {
        iter_next = llist_iterator_next(iter);
        con = llist_item_data(iter, connection_t, it);

        if(con->info.socketfd != -1) {
            if(fcntl(con->info.socketfd, F_GETFL) == -1) {
                switch(errno) {
                case EBADF:
                    SAH_TRACEZ_ERROR("pcb_con", "File descriptor %d is bad", con->info.socketfd);
                    peer_close(&(con->info));
                    closed++;
                    break;
                default:
                    SAH_TRACEZ_ERROR("pcb_con", "File descriptor %d: (%d) %s", con->info.socketfd, errno, error_string(errno));
                    break;
                }
            }
        }
    }

    return closed;
}

/**
   @brief
   Verify the peers of a connection list

   @details
   This function verifies all file descriptors and closes the peers with bad ones.

   @param cons A pointer to a list of connection_info_t structures
   @return The number of closed file descriptors
 */
static int connection_verify_connectionList(connection_list_t* cons) {
    int closed = 0;
    llist_iterator_t* it = NULL;
    connection_info_t* con = NULL;

    llist_for_each(it, cons) {
        con = llist_item_data(it, connection_info_t, it);

        closed += connection_verify_connections(&con->listenSockets);
        closed += connection_verify_connections(&con->connections);
    }

    return closed;
}

/**
   @ingroup pcb_socket_layer_connections
   @brief
   Close all client connections

   @details
   Closes and removes all client connections.

   @param con A pointer to a connection object, must be created with @ref connection_create

   @return
    - true: all client connections are closed
    - false: an error has occured. See @ref pcb_error to get error details.
 */
bool connection_closeClients(connection_info_t* con) {
    if(!con) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    connection_t* c = NULL;
    llist_iterator_t* it = NULL;
    llist_for_each(it, &con->connections) {
        c = llist_item_data(it, connection_t, it);
        if(peer_isClientConnection(&c->info)) {
            SAH_TRACEZ_INFO("pcb_con", "Removing client socket");
            peer_destroy(&c->info);
        }
    }

    pcb_error = pcb_ok;
    return true;
}

/**
   @ingroup pcb_socket_layer_connections
   @brief
   Wait for events

   @details
   This function is doing a simple select using a read set and a write set.
   Sockets are added to the readset if the read event handler has been set.
   Sockets are added to the writeset if the write event handler has been set or when
   data is available in the circular buffer..

   @param cons A pointer to a list of connection_info_t structures
   @param timeout Maximum idle time or NULL for no timeout

   @return
    - The number of filedescriptors set
    - -1 if an error occured
 */
int connection_waitForEvents(connection_list_t* cons, struct timeval* timeout) {
    return connection_waitForEvents_r(cons, timeout, &runtime.g_readset, &runtime.g_writeset);
}

/**
   @ingroup pcb_socket_layer_connections
   @brief
   Wait for events reentrant

   @details
   This function is doing a simple select using a read set and a write set.
   Sockets are added to the readset if the read event handler has been set.
   Sockets are added to the writeset if the write event handler has been set or when
   data is available in the circular buffer..

   @param cons A pointer to a list of connection_info_t structures
   @param readset A pointer to a read set
   @param writeset A pointer to a write set
   @param timeout Maximum idle time or NULL for no timeout

   @return
    - The number of filedescriptors set
    - -1 if an error occured
 */
int connection_waitForEvents_r(connection_list_t* cons, struct timeval* timeout, fd_set* readset, fd_set* writeset) {
    if(!cons || llist_isEmpty(cons)) {
        pcb_error = pcb_error_invalid_parameter;
        return -1;
    }
    llist_iterator_t* it = NULL;
    connection_info_t* con = NULL;
    int maxfd = -1, retval = -1;

    FD_ZERO(readset);
    FD_ZERO(writeset);

    llist_for_each(it, cons) {
        con = llist_item_data(it, connection_info_t, it);

        /* calculate the timers */
        pcb_timer_calculateTimers();
        pcb_timer_checkTimers();

        /* build read and write sets */
        connection_readSet(con, readset, &maxfd);
        connection_writeSet(con, writeset, &maxfd);
    }

    retval = connection_select(maxfd, readset, writeset, timeout);

    if((retval == -1) && (pcb_error == EBADF)) {
        SAH_TRACEZ_ERROR("pcb_con", "Bad file descriptor detected, searching and closing peer");

        if(connection_verify_connectionList(cons)) {
            /* Try again with bad file descriptors closed */
            retval = connection_waitForEvents_r(cons, timeout, readset, writeset);
        }
    }

    return retval;
}

int connection_select(int maxfd, fd_set* readset, fd_set* writeset, struct timeval* timeout) {
    return connection_select_r(maxfd, readset, writeset, timeout, runtime.blockSignals);
}

int connection_select_r(int maxfd, fd_set* readset, fd_set* writeset, struct timeval* timeout, bool unblockSignals) {
    /* app pipe file descriptor to the readset */
    connection_pipe_add_set(0, readset, &maxfd);

    if(unblockSignals && pthread_sigmask(SIG_SETMASK, &runtime.sig_orig_set, NULL)) {
        /* unblock signals */
        // coverity does not like %m
        //SAH_TRACEZ_WARNING("pcb_con","Failed to unblock all signals %m");
        SAH_TRACEZ_WARNING("pcb_con", "Failed to unblock all signals");
    }

    if(maxfd + 1 <= 0) {
        SAH_TRACE_ERROR("No file descriptors given, leave");
        return -1;
    }
    /* signals received right now will be missed but we catch them anyways thanks to the pipe mechanism */

    SAH_TRACEZ_INFO("pcb", "Entering select");
    /* wait until something happens */
    errno = 0;
    int retval = select(maxfd + 1, readset, writeset, NULL, timeout);
    pcb_error = errno;
    // coverity does not like %m
    //SAH_TRACEZ_INFO("pcb","Select done retval = %d, errno =%d (%m)",retval, errno);
    SAH_TRACEZ_INFO("pcb", "Select done retval = %d, errno =%d", retval, errno);

    if(unblockSignals && pthread_sigmask(SIG_SETMASK, &runtime.sig_blocked_set, NULL)) {
        /* block signals again */
        SAH_TRACEZ_WARNING("pcb_con", "Failed to block all signals %d", errno);
    }

    if(retval == -1) {
        switch(pcb_error) {
        case EINTR:
            if(runtime.sig_count == 0) {
                SAH_TRACEZ_ERROR("pcb_con", "Unregistered signal detected during select");
                /*
                 * Because we detected the signal, but it appears to be unregistered,
                 * we pretend to have registered it and return 1.
                 * The application will then hopefully handle the unregistered signal anyways.
                 * Returning 0 here would mean we had a timeout, which is incorrect in any case.
                 */
                return 1;
            }

            SAH_TRACEZ_INFO("pcb_con", "signal count = %d", runtime.sig_count);
            return runtime.sig_count;
            break;
        default:
            SAH_TRACEZ_ERROR("pcb_con", "select failed %s", error_string(pcb_error));
            return -1;
            break;
        }
    }
    if((retval >= 0) && (runtime.pipefd[0] != -1) && FD_ISSET(runtime.pipefd[0], readset)) {
        /* remove pipe file descriptor from readset */
        SAH_TRACEZ_INFO("pcb_con", "Reading signal pipe");
        connection_clearEventFd();
        FD_CLR(runtime.pipefd[0], readset);
        retval += runtime.sig_count;
    }

    runtime.g_set = retval;

    pcb_error = pcb_ok;
    SAH_TRACEZ_INFO("pcb_con", "signal count = %d", runtime.sig_count);
    return retval;
}

/**
   @ingroup pcb_socket_layer_connections
   @brief
   Handle pending events

   @details
   This function checks all socket to see if there are any pending events for any of the
   open sockets or custom file descriptors.

   @param cons A pointer to a list of connection_info_t structures

   @return
    - true: No errors occured
    - false: an error has occured, see @ref pcb_error for more detailed information
 */
bool connection_handleEvents(connection_list_t* cons) {
    return connection_handleEvents_r(cons, &runtime.g_readset, &runtime.g_writeset);
}

/**
   @ingroup pcb_socket_layer_connections
   @brief
   Handle pending events (reentrant)

   @details
   This function checks all socket to see if there are any pending events for any of the
   open sockets or custom file descriptors.. This function can be used when writing a custom
   event loop.

   @param cons A pointer to a list of connection_info_t structures
   @param readset A pointer to a read set
   @param writeset A pointer to a write set

   @return
    - true: No errors occured
    - false: an error has occured, see @ref pcb_error for more detailed information
 */
bool connection_handleEvents_r(connection_list_t* cons, fd_set* readset, fd_set* writeset) {
    if(!cons || llist_isEmpty(cons)) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    llist_iterator_t* it = NULL;
    connection_info_t* con = NULL;

    int set = runtime.g_set;
    runtime.g_set = 0;

    int signal_count = 0;
    sigset_t signal_set;
    signal_copyAndReset_atomic(&signal_count, &signal_set);

    /* calculate timers */
    pcb_timer_calculateTimers();

    pcb_timer_checkTimers();

    llist_for_each(it, cons) {
        con = llist_item_data(it, connection_info_t, it);

        /* if signals are available, handle them here */
        if(signal_count) {
            connection_handle_signals(con, signal_count, &signal_set);
        }

        /* 1. Handle new connections */
        if(set) {
            connection_check(&con->listenSockets, &set, readset, writeset);
        }

        /* 2. Handle read and write events */
        if(set) {
            connection_check(&con->connections, &set, readset, writeset);
        }

        /* events where processed */
        if(con->eventsProcessedHandler) {
            con->eventsProcessedHandler(con);
        }
    }

    pcb_error = pcb_ok;
    return true;
}

/**
   @ingroup pcb_socket_layer_connections
   @brief
   Mark that events are available

   @details
   By calling this function you force the event loop to fall through on the next run.

   @return
    - true: No errors occured
    - false: an error has occured, see @ref pcb_error for more detailed information
 */
bool connection_setEventsAvailable(void) {
    /* write character to pipe */
    if(runtime.pipefd[1] != -1) {
        char c = 's';
        ssize_t ret;
        ret = write(runtime.pipefd[1], &c, 1);
        if(ret) {
            SAH_TRACEZ_NOTICE("pcb_con", "Events available set, write returned %zd", ret);
        }
    }

    return true;
}

/**
   @ingroup pcb_socket_layer_connections
   @brief
   Get the event queue file descriptor

   @details
   Data will be available when unhandled events are available.

   @return
    - a file descriptor
    - -1 if no such file descriptor is available
 */
int connection_eventFd(void) {
    return runtime.pipefd[0];
}

/**
   @ingroup pcb_socket_layer_connections
   @brief
   Clear the event file descriptor

   @details
   Clear and reset the event file descriptor. This file descriptor can be used to indicate that events has happend.
   When writing a custom event loop and when adding this file descriptor to the readset, this function must be
   called before entering the select.
 */
void connection_clearEventFd(void) {
    if(runtime.pipefd[0] == -1) {
        return;
    }
    char c;
    SAH_TRACEZ_INFO("pcb_con", "Read signal pipe");

    ssize_t bytes = 0;
    do {
        bytes = read(runtime.pipefd[0], &c, 1);
    } while(bytes > 0);
    SAH_TRACEZ_INFO("pcb_con", "Read signal pipe done");
}

/**
   @ingroup pcb_socket_layer_connections
   @brief
   Sets the terminated flag

   @details
   By calling this function ther terminated flag is set. Eventloops can use
   @ref connection_isTerminated to decide to coninue or stop.

   @param terminate

   @return
    - true: flag is set.
 */
bool connection_setTerminated(bool terminate) {
    runtime.terminated = terminate;
    return true;
}

/**
   @ingroup pcb_socket_layer_connections
   @brief
   If the terminated flag is set, this function will return true

   @details
   Check that the terminate flag is set. When the flag is set clean-up and stop the application.
   This function can be used when writing a custom event loop.

   @return
    - true: terminated flag is set
    - false: terminated flag is not set
 */
bool connection_isTerminated(void) {
    return runtime.terminated;
}

/**
   @ingroup pcb_socket_layer_connections
   @brief
   Add a file descriptor.

   @details
   This function adds a file descriptor to the list of file descriptors to be checked.
   From the moment the file descriptor is added, the event handlers can be used for
   reading and/or writing..

   @param con A pointer to a connection info structure
   @param fd the file descriptor

   @return
    - pointer to a peer info stucture.
    - NULL: an error has occured
 */
peer_info_t* connection_addCustom(connection_info_t* con, int fd) {
    if(!con) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    connection_t* ep = (connection_t*) calloc(1, sizeof(connection_t));
    if(!ep) {
        pcb_error = pcb_error_no_memory;
        return NULL;
    }

    peer_info_t* peer = &ep->info;
    if(!peer_initialize(peer)) {
        free(ep);
        return NULL;
    }

    peer->socketfd = fd;

    memset(&peer->addrInfo, 0, sizeof(struct addrinfo));

    peer->type = peer_type_custom_fd;
    peer->state = peer_state_connected;
    llist_append(&con->connections, &ep->it);

    pcb_error = pcb_ok;
    return peer;
}

static peer_info_t* connection_find_internal(llist_t* connections, int fd) {
    llist_iterator_t* it = llist_first(connections);
    connection_t* c = NULL;
    while(it) {
        c = llist_item_data(it, connection_t, it);
        if(c->info.socketfd == fd) {
            break;
        }
        c = NULL;
        it = llist_iterator_next(it);
    }
    if(c && (c->info.state != peer_state_deleted)) {
        return &c->info;
    }

    return NULL;
}

/**
   @ingroup pcb_socket_layer_connections
   @brief
   Get a peer info structure from a fd.

   @details
   Searches a peer info structure that encapsulates the given file descriptor.

   @param con A pointer to a connection info structure
   @param fd the file descriptor

   @return
    - pointer to a peer info stucture.
    - NULL: no peer info structure found.
 */
peer_info_t* connection_find(connection_info_t* con, int fd) {
    if(!con) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    peer_info_t* peer = connection_find_internal(&con->listenSockets, fd);
    return peer ? peer : connection_find_internal(&con->connections, fd);
}

/**
   @ingroup pcb_socket_layer_connections
   @brief
   Find a IPC socket.

   @details
   Searches a peer info structure that encapsulates the given IPC socket path.

   @param con A pointer to a connection info structure
   @param path the socket path

   @return
    - pointer to a peer info stucture.
    - NULL: no peer info structure found.
 */
peer_info_t* connection_findIPC(connection_info_t* con, const char* path) {
    if(!con) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    llist_iterator_t* it = llist_first(&con->connections);
    connection_t* c = NULL;
    struct sockaddr_un* remote = NULL;
    while(it) {
        c = llist_item_data(it, connection_t, it);
        if(!peer_isIPCSocket(&c->info)) {
            c = NULL;
            it = llist_iterator_next(it);
            continue;
        }
        remote = (struct sockaddr_un*) c->info.addrInfo.ai_addr;
        if(remote->sun_path[0] && (strcmp(remote->sun_path, path) == 0)) {
            break;
        }
        c = NULL;
        it = llist_iterator_next(it);
    }
    if(c && (c->info.state != peer_state_deleted)) {
        return &c->info;
    } else {
        return NULL;
    }
}

/**
   @ingroup pcb_socket_layer_connections
   @brief
   Find a IPC socket.

   @details
   Searches a peer info structure that encapsulates the given host and service..

   @param con A pointer to a connection info structure
   @param host the host address or name
   @param service the port or service name

   @return
    - pointer to a peer info stucture.
    - NULL: no peer info structure found.
 */
peer_info_t* connection_findTCP(connection_info_t* con, const char* host, const char* service) {
    int status;
    struct addrinfo hints;
    struct addrinfo* res = NULL;
    struct addrinfo* p;
    llist_iterator_t* it;
    connection_t* c = NULL;

    if(!con) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    memset(&hints, 0, sizeof hints);
    hints.ai_family = con->ai_family;
    hints.ai_socktype = SOCK_STREAM;

    status = getaddrinfo(host, service, &hints, &res);
    if((status == -1) || (res == NULL)) {
        pcb_error = (uint32_t) ((uint16_t) status & 0x0000FFFF);
        goto error;
    }

    /* loop through all the results and compare with the TCP sockets we already have */
    for(p = res; p != NULL; p = p->ai_next) {
        c = NULL;
        it = llist_first(&con->connections);
        while(it) {
            c = llist_item_data(it, connection_t, it);
            if(!peer_isTCPSocket(&c->info)) {
                c = NULL;
                it = llist_iterator_next(it);
                continue;
            }
            if(c->info.addrInfo.ai_addrlen != p->ai_addrlen) {
                c = NULL;
                it = llist_iterator_next(it);
                continue;
            }
            if(memcmp(p->ai_addr, c->info.addrInfo.ai_addr, c->info.addrInfo.ai_addrlen) == 0) {
                break;
            }
            c = NULL;
            it = llist_iterator_next(it);
        }
        if(c) {
            break;
        }
    }
    free(res);
    if(c && (c->info.state != peer_state_deleted)) {
        return &c->info;
    } else {
        return NULL;
    }

error:
    free(res);
    return NULL;
}

bool connection_addRunningProcess(connection_info_t* con, process_info_t* process) {
    if(!con || !process) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    llist_append(&con->activeProcesses, &process->it);
    return true;
}

bool connection_removeRunningProcess(connection_info_t* con, process_info_t* process) {
    if(!con || !process) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    llist_iterator_t* it = NULL;
    llist_for_each(it, &con->activeProcesses) {
        process_info_t* item = llist_item_data(it, process_info_t, it);
        if(item == process) {
            llist_iterator_take(it);
            return true;
        }
    }
    return false;
}

/**
   @ingroup pcb_socket_layer_connections
   @brief
   Set the a connection's event handler.

   @details
   For a connection info structure you can add two event handlers:
    - peer_event_connected: will be called whenever the peer is connected (server connections)
    - peer_event_accepted: will be called whenever an incoming connection is accepted (client connections)
   When the accepted event handler returns false, the connection is closed immediatly.
   An event handler can be removed by setting the event handler to NULL.

   @param con A pointer to a connection object, must be created with @ref connection_create
   @param event The event type you want to add a handler for. This can only bee peer_event_connected, peer_event_accepted
   @param handlerfn A pointer to a accept handler function

   @return
    - true: OK
    - false: An error occured
 */
bool connection_setEventHandler(connection_info_t* con, peer_event_type_t event, peer_event_handler_t handlerfn) {
    if(!con) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    switch(event) {
    case peer_event_connected:
        connection_addConnectedHandler(con, handlerfn);
        break;
    case peer_event_accepted:
        con->accept = handlerfn;
        break;
    default:
        /* invalid event type provided */
        SAH_TRACEZ_ERROR("pcb_con", "Invalid event type specified %d", event);
        return false;
        break;
    }

    return true;
}

/**
   @ingroup pcb_socket_layer_connections
   @brief
   Set a signal handler

   @details
   Set a signal handler for a certain signal

   @param con A pointer to a connection object, must be created with @ref connection_create
   @param sig the signal type
   @param handlerfn The signal handler function

   @return
    - true: OK
    - false: An error occured
 */
bool connection_setSignalEventHandler(connection_info_t* con, int sig, signal_event_handler_t handlerfn) {
    if(!con) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(sig == SIGALRM) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    llist_iterator_t* it = NULL;
    signal_handler_t* sh = NULL;
    llist_for_each(it, &con->signalHandlers) {
        sh = llist_item_data(it, signal_handler_t, it);
        if(sh->signal == sig) {
            break;
        }
        sh = NULL;
    }

    if(!sh) {
        sh = (signal_handler_t*) calloc(1, sizeof(signal_handler_t));
        if(!sh) {
            return false;
        } else {
            llist_iterator_initialize(&sh->it);
            llist_append(&con->signalHandlers, &sh->it);
        }
    }
    sh->handler = handlerfn;
    sh->signal = sig;

    struct sigaction act = {.sa_handler = signalHandler, .sa_flags = SA_RESTART};
    if(!handlerfn && (sig != SIGCHLD)) {
        /* Directly ignore the signal when nothing will be done with it. */
        act.sa_handler = SIG_IGN;
    }
    sigaction(sig, &act, NULL);

    return true;
}

/**
   @ingroup pcb_socket_layer_connections
   @brief
   Add a connected handler

   @details
   Add a connect handler to the connection_info_t structure. This handler will be called
   each time a new connection is setup.
   It will only be called for connections initiated by the application itself, not for incomming
   connections.

   @param con A pointer to a connection object, must be created with @ref connection_create
   @param handlerfn The connected handler function

   @return
    - true: OK
    - false: An error occured
 */
bool connection_addConnectedHandler(connection_info_t* con, peer_event_handler_t handlerfn) {
    if(!con) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    llist_iterator_t* it = NULL;
    peer_handler_list_t* connectHandler = NULL;
    llist_for_each(it, &con->connectHandlers) {
        connectHandler = llist_item_data(it, peer_handler_list_t, it);
        if(connectHandler->handler_function == handlerfn) {
            return true;
        }
    }

    peer_handler_list_t* handler = (peer_handler_list_t*) calloc(1, sizeof(peer_handler_list_t));
    if(!handler) {
        return false;
    }

    handler->handler_function = handlerfn;
    llist_append(&con->connectHandlers, &handler->it);

    return true;
}

/**
   @ingroup pcb_socket_layer_connections
   @brief
   Set a common event handler

   @details
   Sets a common event handler to the connection_info_t structure.
   This handler will be called wheneverat least one event happended and was handled.
   This function allows you to perform small common tasks after one or more events where handled.

   @param con A pointer to a connection object, must be created with @ref connection_create
   @param handlerfn The connected handler function

   @return
    - true: OK
    - false: An error occured
 */
bool connection_setEventsProcessedHandler(connection_info_t* con, events_processed_handler_t handlerfn) {
    if(!con) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    con->eventsProcessedHandler = handlerfn;
    return true;
}
