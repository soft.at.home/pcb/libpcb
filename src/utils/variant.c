/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#define _DEFAULT_SOURCE
#define _BSD_SOURCE
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <stdio.h>
#include <ctype.h>
#include <ctype.h>
#include <wctype.h>
#include <wchar.h>

#include <debug/sahtrace.h>

#include <pcb/common/error.h>
#include <pcb/utils/variant_list.h>
#include <pcb/utils/variant_map.h>
#include <pcb/utils/string.h>

/* define llabs: cross-compiling for mips using uclibc gives a warning on this function */
long long int llabs (long long int __x);

static variant_t local_variant_true = { .type = variant_type_bool, .data = { .b = true } };
static variant_t local_variant_false = { .type = variant_type_bool, .data = { .b = false } };
variant_t* variant_true = &local_variant_true;
variant_t* variant_false = &local_variant_false;



/**
   @ingroup pcb_utils_variant
   @brief
   Initialize a variant.

   @details
   This function <b>initializes</b> a <b>variant</b>. \n
   It is mandatory to initialize a variant (variant_t) before using it. \n
   Every @ref variant_initialize() call should have a corresponding @ref variant_cleanup() call to free the memory\n
   Depending on the variant type, this function call allocates memory for storing its data type.\n

   @param variant the variant to be initialized
   @param type the variant type

   @return
    - true: Initialization was succesfull, the variant_t is ready to be used
    - false: An error has occurred, pcb_error contains the reason why the initialization failed
        - an invalid variant was provided
        - variant type is invalid
        - memory allocation failed
 */
bool variant_initialize(variant_t* variant, variant_type_t type) {
    if(!variant) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    bool retVal = true;
    memset(&variant->data, 0x00, sizeof(variant->data));
    variant->type = type;

    switch(variant->type) {
    case variant_type_date_time:
        variant->data.dt = calloc(1, sizeof(pcb_datetime_t));
        if(!variant->data.dt) {
            pcb_error = pcb_error_no_memory;
            return false;
        }
        pcb_datetime_initialize(variant->data.dt, PCB_EPOCH_TR098);
        break;
    case variant_type_array:
        variant->data.vl = calloc(1, sizeof(variant_list_t));
        if(!variant->data.vl) {
            pcb_error = pcb_error_no_memory;
            return false;
        }
        break;
    case variant_type_map:
        variant->data.vm = calloc(1, sizeof(variant_map_t));
        if(!variant->data.vm) {
            pcb_error = pcb_error_no_memory;
            return false;
        }
        break;
    case variant_type_file_descriptor:
        variant->data.fd = -1;
        break;
    case variant_type_byte_array:
        variant->data.ba = calloc(1, sizeof(byte_array_t));
        if(!variant->data.ba) {
            pcb_error = pcb_error_no_memory;
            return false;
        }
        break;
    default:
        break;
    }
    return retVal;
}

/**
   @ingroup pcb_utils_variant
   @brief
   Copy a variant.

   @details
   This function creates a <b>copy</b> of an existing variant.\n
   If the destination variant contains data, it is automatically freed before copying the new data in it.\n

   @param dest the destination variant
   @param src the source variant
 */
void variant_copy(variant_t* dest, const variant_t* src) {
    if(!dest || !src) {
        pcb_error = pcb_error_invalid_parameter;
        return;
    }

    variant_cleanup(dest);
    variant_initialize(dest, src->type);
    switch(src->type) {
    case variant_type_string:
        string_copy(&dest->data.str, &src->data.str);
        break;
    case variant_type_int8:
        dest->data.i8 = src->data.i8;
        break;
    case variant_type_int16:
        dest->data.i16 = src->data.i16;
        break;
    case variant_type_int32:
        dest->data.i32 = src->data.i32;
        break;
    case variant_type_int64:
        dest->data.i64 = src->data.i64;
        break;
    case variant_type_uint8:
        dest->data.ui8 = src->data.ui8;
        break;
    case variant_type_uint16:
        dest->data.ui16 = src->data.ui16;
        break;
    case variant_type_uint32:
        dest->data.ui32 = src->data.ui32;
        break;
    case variant_type_uint64:
        dest->data.ui64 = src->data.ui64;
        break;
    case variant_type_bool:
        dest->data.b = src->data.b;
        break;
    case variant_type_double:
        dest->data.d = src->data.d;
        break;
    case variant_type_date_time:
        if(dest->data.dt && src->data.dt) {
            *(dest->data.dt) = *(src->data.dt);
        }
        break;
    case variant_type_array: {
        variant_list_iterator_t* it = NULL;
        variant_list_iterator_t* copy = NULL;
        variant_list_for_each(it, src->data.vl) {
            copy = variant_list_iterator_create(&it->variant);
            if(copy) {
                variant_list_append(dest->data.vl, copy);
            }
        }
    }
    break;
    case variant_type_map: {
        variant_map_iterator_t* it = NULL;
        variant_map_iterator_t* copy = NULL;
        variant_map_for_each(it, src->data.vm) {
            copy = variant_map_iterator_create(it->key, &it->variant);
            if(copy) {
                variant_map_append(dest->data.vm, copy);
            }
        }
    }
    break;
    case variant_type_reference:
        dest->data.v = src->data.v;
        break;
    case variant_type_file_descriptor:
        dest->data.fd = src->data.fd;
        break;
    case variant_type_byte_array:
        dest->data.ba->data = calloc(1, src->data.ba->size);
        if(dest->data.ba->data) {
            memcpy(dest->data.ba->data, src->data.ba->data, src->data.ba->size);
            dest->data.ba->size = src->data.ba->size;
        } else {
            dest->data.ba->size = 0;
        }
        break;
    default:
        break;
    }
}

void variant_move(variant_t* dest, variant_t* src) {
    if(!dest || !src) {
        pcb_error = pcb_error_invalid_parameter;
        return;
    }

    variant_cleanup(dest);
    dest->type = src->type;
    switch(src->type) {
    case variant_type_string:
        dest->data.str.buffer = src->data.str.buffer;
        dest->data.str.bufferSize = src->data.str.bufferSize;
        src->data.str.buffer = NULL;
        src->data.str.bufferSize = 0;
        break;
    case variant_type_int8:
        dest->data.i8 = src->data.i8;
        break;
    case variant_type_int16:
        dest->data.i16 = src->data.i16;
        break;
    case variant_type_int32:
        dest->data.i32 = src->data.i32;
        break;
    case variant_type_int64:
        dest->data.i64 = src->data.i64;
        break;
    case variant_type_uint8:
        dest->data.ui8 = src->data.ui8;
        break;
    case variant_type_uint16:
        dest->data.ui16 = src->data.ui16;
        break;
    case variant_type_uint32:
        dest->data.ui32 = src->data.ui32;
        break;
    case variant_type_uint64:
        dest->data.ui64 = src->data.ui64;
        break;
    case variant_type_bool:
        dest->data.b = src->data.b;
        break;
    case variant_type_double:
        dest->data.d = src->data.d;
        break;
    case variant_type_date_time:
        dest->data.dt = src->data.dt;
        src->data.dt = NULL;
        break;
    case variant_type_array:
        dest->data.vl = src->data.vl;
        src->data.vl = NULL;
        break;
    case variant_type_map:
        dest->data.vm = src->data.vm;
        src->data.vm = NULL;
        break;
    case variant_type_reference:
        dest->data.v = src->data.v;
        break;
    case variant_type_file_descriptor:
        dest->data.fd = src->data.fd;
        break;
    case variant_type_byte_array:
        dest->data.ba = src->data.ba;
        src->data.ba = NULL;
        break;
    default:
        break;
    }
    src->type = variant_type_unknown;
    src->data.ui64 = 0;
}

bool variant_convert(variant_t* dst, const variant_t* src, variant_type_t type) {
    switch(type) {
    case variant_type_unknown:
        return false;
        break;
    case variant_type_string:
    case variant_type_reference: {
        string_t temp;
        string_initialize(&temp, 0);
        if(variant_toString(&temp, src)) {
            variant_setString(dst, &temp);
            string_cleanup(&temp);
        } else {
            string_cleanup(&temp);
            return false;
        }
    }
    break;
    case variant_type_int8: {
        int8_t temp;
        if(variant_toInt8(&temp, src)) {
            variant_setInt8(dst, temp);
        } else {
            return false;
        }
    }
    break;
    case variant_type_int16: {
        int16_t temp;
        if(variant_toInt16(&temp, src)) {
            variant_setInt16(dst, temp);
        } else {
            return false;
        }
    }
    break;
    case variant_type_int32: {
        int32_t temp;
        if(variant_toInt32(&temp, src)) {
            variant_setInt32(dst, temp);
        } else {
            return false;
        }
    }
    break;
    case variant_type_int64: {
        int64_t temp;
        if(variant_toInt64(&temp, src)) {
            variant_setInt64(dst, temp);
        } else {
            return false;
        }
    }
    break;
    case variant_type_uint8: {
        uint8_t temp;
        if(variant_toUInt8(&temp, src)) {
            variant_setUInt8(dst, temp);
        } else {
            return false;
        }
    }
    break;
    case variant_type_uint16: {
        uint16_t temp;
        if(variant_toUInt16(&temp, src)) {
            variant_setUInt16(dst, temp);
        } else {
            return false;
        }
    }
    break;
    case variant_type_uint32: {
        uint32_t temp;
        if(variant_toUInt32(&temp, src)) {
            variant_setUInt32(dst, temp);
        } else {
            return false;
        }
    }
    break;
    case variant_type_uint64: {
        uint64_t temp;
        if(variant_toUInt64(&temp, src)) {
            variant_setUInt64(dst, temp);
        } else {
            return false;
        }
    }
    break;
    case variant_type_bool: {
        bool temp;
        if(variant_toBool(&temp, src)) {
            variant_setBool(dst, temp);
        } else {
            return false;
        }
    }
    break;
    case variant_type_double:
    {
        double temp;
        if(variant_toDouble(&temp, src)) {
            variant_setDouble(dst, temp);
        } else {
            return false;
        }
    }
    break;
    case variant_type_date_time:
    {
        pcb_datetime_t temp;
        if(variant_toDateTimeExtended(&temp, src)) {
            variant_setDateTimeExtended(dst, &temp);
        } else {
            return false;
        }
    }
    break;
    default:
        pcb_error = pcb_error_unknown_type;
        return false;
        break;
    }

    return true;
}

/**
   @ingroup pcb_utils_variant
   @brief
   Cleanup a variant.

   @details
   This function <b>frees</b> the allocated memory for storing its data\n

   @param variant the variant that has to be cleaned up
 */
void variant_cleanup(variant_t* variant) {
    if(!variant) {
        pcb_error = pcb_error_invalid_parameter;
        return;
    }

    switch(variant->type) {
    case variant_type_unknown:
    case variant_type_string:
        string_cleanup(&variant->data.str);
        break;
    case variant_type_date_time:
        free(variant->data.dt);
        variant->data.dt = NULL;
        break;
    case variant_type_array:
        variant_list_cleanup(variant->data.vl);
        free(variant->data.vl);
        variant->data.vl = NULL;
        break;
    case variant_type_map:
        variant_map_cleanup(variant->data.vm);
        free(variant->data.vm);
        variant->data.vm = NULL;
        break;
    case variant_type_byte_array:
        free(variant->data.ba->data);
        free(variant->data.ba);
        variant->data.ba = NULL;
        break;
    default:
        break;
    }
}

/**
   @ingroup pcb_utils_variant
   @brief
   Create a int8_t variant using the specified int8_t as initialization data.

   @details
   This function cleans up the existing variant value, (re-)initializes the variant to the int8_t type,
   and initializes the value with the provided data.\n

   @param variant the variant we want to set
   @param data the initial value

   @return
    - true: the variant_t is ready to be used
    - false: An error has occurred, pcb_error contains the reason why the set failed
        - an invalid variant was provided
 */
bool variant_setInt8(variant_t* variant, int8_t data) {
    if(!variant) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }
    variant_cleanup(variant);

    if(variant_initialize(variant, variant_type_int8)) {
        variant->data.i8 = data;
        return true;
    }

    return false;
}

/**
   @ingroup pcb_utils_variant
   @brief
   Create a int16_t variant using the specified int16_t as initialization data.

   @details
   This function cleans up the existing variant value, (re-)initializes the variant to the int16_t type,
   and initializes the value with the provided data.\n

   @param variant the variant we want to set
   @param data the initial value

   @return
    - true: the variant_t is ready to be used
    - false: An error has occurred, pcb_error contains the reason why the set failed
        - an invalid variant was provided
 */
bool variant_setInt16(variant_t* variant, int16_t data) {
    if(!variant) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }
    variant_cleanup(variant);

    if(variant_initialize(variant, variant_type_int16)) {
        variant->data.i16 = data;
        return true;
    }

    return false;
}

/**
   @ingroup pcb_utils_variant
   @brief
   Create a int32_t variant using the specified int32_t as initialization data.

   @details
   This function cleans up the existing variant value, (re-)initializes the variant to the int32_t type,
   and initializes the value with the provided data.

   @param variant the variant we want to set
   @param data the initial value

   @return
    - true: the variant_t is ready to be used
    - false: An error has occurred, pcb_error contains the reason why the set failed
        - an invalid variant was provided
 */
bool variant_setInt32(variant_t* variant, int32_t data) {
    if(!variant) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }
    variant_cleanup(variant);

    if(variant_initialize(variant, variant_type_int32)) {
        variant->data.i32 = data;
        return true;
    }

    return false;
}

/**
   @ingroup pcb_utils_variant
   @brief
   Create a int64_t variant using the specified int64_t as initialization data.

   @details
   This function cleans up the existing variant value, (re-)initializes the variant to the int64_t type,
   and initializes the value with the provided data.\n

   @param variant the variant we want to set
   @param data the initial value

   @return
    - true: the variant_t is ready to be used
    - false: An error has occurred, pcb_error contains the reason why the set failed
        - an invalid variant was provided
 */
bool variant_setInt64(variant_t* variant, int64_t data) {
    if(!variant) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }
    variant_cleanup(variant);

    if(variant_initialize(variant, variant_type_int64)) {
        variant->data.i64 = data;
        return true;
    }

    return false;
}

/**
   @ingroup pcb_utils_variant
   @brief
   Create a uint8_t variant using the specified uint8_t as initialization data.

   @details
   This function cleans up the existing variant value, (re-)initializes the variant to the uint8_t type,
   and initializes the value with the provided data.\n

   @param variant the variant we want to set
   @param data the initial value

   @return
    - true: the variant_t is ready to be used
    - false: An error has occurred, pcb_error contains the reason why the set failed
        - an invalid variant was provided
 */
bool variant_setUInt8(variant_t* variant, uint8_t data) {
    if(!variant) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }
    variant_cleanup(variant);

    if(variant_initialize(variant, variant_type_uint8)) {
        variant->data.ui8 = data;
        return true;
    }

    return false;
}

/**
   @ingroup pcb_utils_variant
   @brief
   Create a uint16_t variant using the specified uint16_t as initialization data.

   @details
   This function cleans up the existing variant value, (re-)initializes the variant to the uint16_t type,
   and initializes the value with the provided data.\n

   @param variant the variant we want to set
   @param data the initial value

   @return
    - true: the variant_t is ready to be used
    - false: An error has occurred, pcb_error contains the reason why the set failed
        - an invalid variant was provided
 */
bool variant_setUInt16(variant_t* variant, uint16_t data) {
    if(!variant) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }
    variant_cleanup(variant);

    if(variant_initialize(variant, variant_type_uint16)) {
        variant->data.ui16 = data;
        return true;
    }

    return false;
}

/**
   @ingroup pcb_utils_variant
   @brief
   Create a uint32_t variant using the specified uint32_t as initialization data.

   @details
   This function cleans up the existing variant value, (re-)initializes the variant to the uint32_t type,
   and initializes the value with the provided data.\n

   @param variant the variant we want to set
   @param data the initial value

   @return
    - true: the variant_t is ready to be used
    - false: An error has occurred, pcb_error contains the reason why the set failed
        - an invalid variant was provided
 */
bool variant_setUInt32(variant_t* variant, uint32_t data) {
    if(!variant) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }
    variant_cleanup(variant);

    if(variant_initialize(variant, variant_type_uint32)) {
        variant->data.ui32 = data;
        return true;
    }

    return false;
}

/**
   @ingroup pcb_utils_variant
   @brief
   Create a uint64_t variant using the specified uint64_t as initialization data.

   @details
   This function cleans up the existing variant value, (re-)initializes the variant to the uint64_t type,
   and initializes the value with the provided data.\n

   @param variant the variant we want to set
   @param data the initial value

   @return
    - true: the variant_t is ready to be used
    - false: An error has occurred, pcb_error contains the reason why the set failed
        - an invalid variant was provided
 */
bool variant_setUInt64(variant_t* variant, uint64_t data) {
    if(!variant) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }
    variant_cleanup(variant);

    if(variant_initialize(variant, variant_type_uint64)) {
        variant->data.ui64 = data;
        return true;
    }

    return false;
}

/**
   @ingroup pcb_utils_variant
   @brief
   Create a boolean variant using the specified bool as initialization data.

   @details
   This function cleans up the existing variant value, (re-)initializes the variant to the bool type,
   and initializes the value with the provided data.\n

   @param variant the variant we want to set
   @param data the initial value

   @return
    - true: the variant_t is ready to be used
    - false: An error has occurred, pcb_error contains the reason why the set failed
        - an invalid variant was provided
 */
bool variant_setBool(variant_t* variant, bool data) {
    if(!variant) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }
    variant_cleanup(variant);

    if(variant_initialize(variant, variant_type_bool)) {
        variant->data.b = data;
        return true;
    }

    return false;
}

/**
   @ingroup pcb_utils_variant
   @brief
   Create a double variant using the specified double as initialization data.

   @details
   This function cleans up the existing variant value, (re-)initializes the variant to the double type,
   and initializes the value with the provided data.
   \n

   @param variant the variant we want to set
   @param data the initial value

   @return
    - true: the variant_t is ready to be used
    - false: An error has occurred, pcb_error contains the reason why the set failed
        - an invalid variant was provided
 */
bool variant_setDouble(variant_t* variant, double data) {
    if(!variant) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }
    variant_cleanup(variant);

    if(variant_initialize(variant, variant_type_double)) {
        variant->data.d = data;
        return true;
    }

    return false;
}
/**
   @ingroup pcb_utils_variant
   @brief
   Create a datetime variant using the specified date time as initialization data.

   @details
   This function cleans up the existing variant value, (re-)initializes the variant to the datetime type,
   and initializes the value with the provided data.\n

   @param variant the variant we want to set
   @param data the initial value

   @return
    - true: the variant_t is ready to be used
    - false: An error has occurred, pcb_error contains the reason why the set failed
        - an invalid variant was provided
        - an invalid struct tm pointer was provided
 */
bool variant_setDateTimeExtended(variant_t* variant, const pcb_datetime_t* data) {
    if(!variant || !data) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }
    variant_cleanup(variant);

    if(variant_initialize(variant, variant_type_date_time)) {
        *variant->data.dt = *data;
        return true;
    }

    return false;
}
/**
   @ingroup pcb_utils_variant
   @brief
   Create a datetime variant using the specified date time as initialization data.

   @details
   This function cleans up the existing variant value, (re-)initializes the variant to the datetime type,
   and initializes the value with the provided data.\n

   @param variant the variant we want to set
   @param data the initial value

   @return
    - true: the variant_t is ready to be used
    - false: An error has occurred, pcb_error contains the reason why the set failed
        - an invalid variant was provided
        - an invalid struct tm pointer was provided
 */
bool variant_setDateTime(variant_t* variant, const struct tm* data) {
    if(!variant || !data) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }
    variant_cleanup(variant);

    if(variant_initialize(variant, variant_type_date_time)) {
        variant->data.dt->datetime = *data;
        variant->data.dt->nanoseconds = 0;
        return true;
    }

    return false;
}

/**
   @ingroup pcb_utils_variant
   @brief
   Create a variant list variant using the specified variant list as initialization data.

   @details
   This function cleans up the existing variant value, (re-)initializes the variant to the variant list type,
   and initializes the value with the provided data.\n
   All variants in the variant list will be moved to the variant. The original variant list will be empty
   after calling this function\n

   @param variant the variant we want to set
   @param data the initial value

   @return
    - true: the variant_t is ready to be used
    - false: An error has occurred, pcb_error contains the reason why the set failed
        - an invalid variant was provided
        - an invalid struct tm pointer was provided
 */
bool variant_setListMove(variant_t* variant, variant_list_t* data) {
    if(!variant || !data) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }
    variant_cleanup(variant);

    if(variant_initialize(variant, variant_type_array)) {
        variant_list_iterator_t* it = variant_list_takeFirst(data);
        while(it) {
            variant_list_append(variant->data.vl, it);
            it = variant_list_takeFirst(data);
        }
        return true;
    }

    return false;
}

/**
   @ingroup pcb_utils_variant
   @brief
   Create a variant list variant using the specified variant list as initialization data.

   @details
   This function cleans up the existing variant value, (re-)initializes the variant to the variant list type,
   and initializes the value with the provided data.\n
   All variants in the variant list will be copied to the variant.\n

   @param variant the variant we want to set
   @param data the initial value

   @return
    - true: the variant_t is ready to be used
    - false: An error has occurred, pcb_error contains the reason why the set failed
        - an invalid variant was provided
        - an invalid struct tm pointer was provided
 */
bool variant_setListCopy(variant_t* variant, const variant_list_t* data) {
    if(!variant || !data) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }
    variant_cleanup(variant);

    if(variant_initialize(variant, variant_type_array)) {
        variant_list_iterator_t* it = variant_list_first(data);
        while(it) {
            variant_list_add(variant->data.vl, variant_list_iterator_data(it));
            it = variant_list_iterator_next(it);
        }
        return true;
    }

    return false;
}

/**
   @ingroup pcb_utils_variant
   @brief
   Create a variant list variant using the specified variant list as initialization data.

   @details
   This function cleans up the existing variant value, (re-)initializes the variant to the variant list reference type,
   and initializes the value with the provided data.\n
   This variant will keep a reference to the original list. If the original list changes the content of the variant is changed as well.
   If the original variant list is destroyed, this variant becomes invalid.\n

   @param variant the variant we want to set
   @param data the initial value

   @return
    - true: the variant_t is ready to be used
    - false: An error has occurred, pcb_error contains the reason why the set failed
        - an invalid variant was provided
        - an invalid struct tm pointer was provided
 */
bool variant_setListRef(variant_t* variant, variant_list_t* data) {
    if(!variant || !data) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }
    variant_cleanup(variant);

    if(variant_initialize(variant, variant_type_array_reference)) {
        variant->data.vl = data;
        return true;
    }

    return false;
}

/**
   @ingroup pcb_utils_variant
   @brief
   Create a variant map variant using the specified variant map as initialization data.

   @details
   This function cleans up the existing variant value, (re-)initializes the variant to the variant map type,
   and initializes the value with the provided data.\n
   All variants in the variant map will be moved to the variant. The original variant map will be empty
   after calling this function\n

   @param variant the variant we want to set
   @param data the initial value

   @return
    - true: the variant_t is ready to be used
    - false: An error has occurred, pcb_error contains the reason why the set failed
        - an invalid variant was provided
        - an invalid struct tm pointer was provided
 */
bool variant_setMapMove(variant_t* variant, variant_map_t* data) {
    if(!variant || !data) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }
    variant_cleanup(variant);

    if(variant_initialize(variant, variant_type_map)) {
        variant_map_iterator_t* it = variant_map_takeFirst(data);
        while(it) {
            variant_map_append(variant->data.vl, it);
            it = variant_map_takeFirst(data);
        }
        return true;
    }

    return false;
}

/**
   @ingroup pcb_utils_variant
   @brief
   Create a variant map variant using the specified variant map as initialization data.

   @details
   This function cleans up the existing variant value, (re-)initializes the variant to the variant map type,
   and initializes the value with the provided data.\n
   All variants in the variant map will be copied to the variant.\n

   @param variant the variant we want to set
   @param data the initial value

   @return
    - true: the variant_t is ready to be used
    - false: An error has occurred, pcb_error contains the reason why the set failed
        - an invalid variant was provided
        - an invalid struct tm pointer was provided
 */
bool variant_setMapCopy(variant_t* variant, const variant_map_t* data) {
    if(!variant || !data) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }
    variant_cleanup(variant);

    if(variant_initialize(variant, variant_type_map)) {
        variant_map_iterator_t* it = variant_map_first(data);
        variant_map_iterator_t* nit = NULL;
        while(it) {
            nit = variant_map_iterator_create(variant_map_iterator_key(it), variant_map_iterator_data(it));
            variant_map_append(variant->data.vm, nit);
            it = variant_map_iterator_next(it);
        }
        return true;
    }

    return false;
}

/**
   @ingroup pcb_utils_variant
   @brief
   Create a variant map variant using the specified variant list as initialization data.

   @details
   This function cleans up the existing variant value, (re-)initializes the variant to the variant map reference type,
   and initializes the value with the provided data.\n
   This variant will keep a reference to the original map. If the original map changes the content of the variant is changed as well.
   If the original variant map is destroyed, this variant becomes invalid.\n

   @param variant the variant we want to set
   @param data the initial value

   @return
    - true: the variant_t is ready to be used
    - false: An error has occurred, pcb_error contains the reason why the set failed
        - an invalid variant was provided
        - an invalid struct tm pointer was provided
 */
bool variant_setMapRef(variant_t* variant, variant_map_t* data) {
    if(!variant || !data) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }
    variant_cleanup(variant);

    if(variant_initialize(variant, variant_type_map_reference)) {
        variant->data.vm = data;
        return true;
    }

    return false;
}

/**
   @ingroup pcb_utils_variant
   @brief
   Create a variant containing a reference to another variant

   @details
   This function cleans up the existing variant value, (re-)initializes the variant to the reference type
   and initializes the value with the provided reference variant.\n

   @param variant the variant we want to set
   @param ref the variant that will be referenced

   @return
    - true: the variant_t is ready to be used
    - false: An error has occurred, pcb_error contains the reason why the set failed
        - an invalid variant was provided
        - an invalid struct tm pointer was provided
 */
bool variant_setReference(variant_t* variant, variant_t* ref) {
    if(!variant || !ref) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }
    variant_cleanup(variant);

    if(variant_type(ref) == variant_type_reference) {
        ref = variant_da_variant(ref);
    }

    if(variant_initialize(variant, variant_type_reference)) {
        variant->data.v = ref;
        return true;
    }

    return false;
}

/**
   @ingroup pcb_utils_variant
   @brief
   Create a variant containing a file descriptor

   @details
   This function cleans up the existing variant value, (re-)initializes the variant to the file descriptor type
   and initializes the value with the provided file descriptor.\n

   @param variant the variant we want to set
   @param fd the file descriptor

   @return
    - true: the variant_t is ready to be used
    - false: An error has occurred, pcb_error contains the reason why the set failed
 */
bool variant_setFd(variant_t* variant, int fd) {
    if(!variant) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }
    variant_cleanup(variant);
    if(variant_initialize(variant, variant_type_file_descriptor)) {
        if((fcntl(fd, F_GETFD) == -1) && (errno == EBADF)) {
            pcb_error = errno;
            return false;
        }

        variant->data.fd = fd;
        return true;
    }

    return false;
}

/**
   @ingroup pcb_utils_variant
   @brief
   Create a variant containing a byte array

   @details
   This function cleans up the existing variant value, (re-)initializes the variant to the byte array type
   and initializes the value with the provided data.\n

   @param variant the variant we want to set
   @param data pointer to the data
   @param size size of the data

   @return
    - true: the variant_t is ready to be used
    - false: An error has occurred, pcb_error contains the reason why the set failed
 */
bool variant_setByteArray(variant_t* variant, const void* data, uint32_t size) {
    if(!variant) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }
    variant_cleanup(variant);
    if(variant_initialize(variant, variant_type_byte_array)) {
        if(size && data) {
            variant->data.ba->data = calloc(1, size);
            if(variant->data.ba->data) {
                variant->data.ba->size = size;
                memcpy(variant->data.ba->data, data, size);
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }

    return false;
}

/**
   @ingroup pcb_utils_variant
   @brief
   Get the value of the specified variant and copy it into the specified int8_t.

   @details
   This function converts the variant value to the int8_t type and copies it into the provided
   int8_t. The conversion type depends on the variant type. \n

   Supported conversions:\n
   @li string
   @li int8
   @li int16
   @li int32
   @li int64
   @li uint8
   @li uint16
   @li uint32
   @li uint64
   @li bool
   All other variant types can NOT be converted to a int8_t.
   If the conversion fails, pcb_error will be set to pcb_error_invalid_variant_conversion.\n

   @param data int8_t that will contain the result
   @param variant the variant of interest

   @return
    - true: the resulting int8_t is ready to be used
    - false: An error has occurred, pcb_error contains the reason why the set failed
        - an invalid variant was provided
        - an invalid data pointer was provided
        - memory allocation failed
 */
bool variant_toInt8(int8_t* data, const variant_t* variant) {
    if(!variant || !data) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    bool success = true;
    switch(variant->type) {
    case variant_type_string:
        *data = string_convertToInt8(&variant->data.str, &success);
        break;
    case variant_type_int8:
        *data = (int8_t) variant->data.i8;
        break;
    case variant_type_int16:
        *data = (variant->data.i16 < 0) ? (int8_t) variant->data.i16 : (int8_t) (variant->data.i16 & INT8_MAX);
        break;
    case variant_type_int32:
        *data = (variant->data.i32 < 0) ? (int8_t) variant->data.i32 : (int8_t) (variant->data.i32 & INT8_MAX);
        break;
    case variant_type_int64:
        *data = (variant->data.i64 < 0) ? (int8_t) variant->data.i64 : (int8_t) (variant->data.i64 & INT8_MAX);
        break;
    case variant_type_uint8:
        *data = (int8_t) variant->data.ui8 & INT8_MAX;
        break;
    case variant_type_uint16:
        *data = (int8_t) variant->data.ui16 & INT8_MAX;
        break;
    case variant_type_uint32:
        *data = (int8_t) variant->data.ui32 & INT8_MAX;
        break;
    case variant_type_uint64:
        *data = (int8_t) variant->data.ui64 & INT8_MAX;
        break;
    case variant_type_bool:
        *data = variant->data.b ? 1 : 0;
        break;
    case variant_type_reference:
        return variant_toInt8(data, variant->data.v);
        break;
    case variant_type_file_descriptor:
        *data = -1;
        break;
    case variant_type_double:
    case variant_type_date_time:
    case variant_type_array_reference:
    case variant_type_array:
    case variant_type_map_reference:
    case variant_type_map:
    case variant_type_byte_array:
    default:
        pcb_error = pcb_error_invalid_variant_conversion;
        success = false;
        break;
    }

    return success;
}

/**
   @ingroup pcb_utils_variant
   @brief
   Get the value of the specified variant and copy it into the specified int16_t.

   @details
   This function converts the variant value to the int16_t type and copies it into the provided
   int16_t. The conversion type depends on the variant type. \n

   Supported conversions:\n
   @li string
   @li int8
   @li int16
   @li int32
   @li int64
   @li uint8
   @li uint16
   @li uint32
   @li uint64
   @li bool
   All other variant types can NOT be converted to a int16_t.
   If the conversion fails, pcb_error will be set to pcb_error_invalid_variant_conversion.\n

   @param data int16_t that will contain the result
   @param variant the variant of interest

   @return
    - true: the resulting int16_t is ready to be used
    - false: An error has occurred, pcb_error contains the reason why the set failed
        - an invalid variant was provided
        - an invalid data pointer was provided
        - memory allocation failed
 */
bool variant_toInt16(int16_t* data, const variant_t* variant) {
    if(!variant || !data) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    bool success = true;
    switch(variant->type) {
    case variant_type_string:
        *data = string_convertToInt16(&variant->data.str, &success);
        break;
    case variant_type_int8:
        *data = (int16_t) variant->data.i8;
        break;
    case variant_type_int16:
        *data = (int16_t) variant->data.i16;
        break;
    case variant_type_int32:
        *data = (variant->data.i32 < 0) ? (int16_t) variant->data.i32 : (int16_t) (variant->data.i32 & INT16_MAX);
        break;
    case variant_type_int64:
        *data = (variant->data.i64 < 0) ? (int16_t) variant->data.i64 : (int16_t) (variant->data.i64 & INT16_MAX);
        break;
    case variant_type_uint8:
        *data = abs((int16_t) variant->data.ui8);
        break;
    case variant_type_uint16:
        *data = (int16_t) variant->data.ui16 & INT16_MAX;
        break;
    case variant_type_uint32:
        *data = (int16_t) variant->data.ui32 & INT16_MAX;
        break;
    case variant_type_uint64:
        *data = (int16_t) variant->data.ui64 & INT16_MAX;
        break;
    case variant_type_bool:
        *data = variant->data.b ? 1 : 0;
        break;
    case variant_type_reference:
        return variant_toInt16(data, variant->data.v);
        break;
    case variant_type_file_descriptor:
        *data = -1;
        break;
    case variant_type_double:
    case variant_type_date_time:
    case variant_type_array_reference:
    case variant_type_array:
    case variant_type_map_reference:
    case variant_type_map:
    case variant_type_byte_array:
    default:
        pcb_error = pcb_error_invalid_variant_conversion;
        success = false;
        break;
    }

    return success;
}

/**
   @ingroup pcb_utils_variant
   @brief
   Get the value of the specified variant and copy it into the specified int32_t.

   @details
   This function converts the variant value to the int32_t type and copies it into the provided
   int32_t. The conversion type depends on the variant type. \n

   Supported conversions:\n
   @li string
   @li int8
   @li int16
   @li int32
   @li int64
   @li uint8
   @li uint16
   @li uint32
   @li uint64
   @li bool
   All other variant types can NOT be converted to a int32_t.
   If the conversion fails, pcb_error will be set to pcb_error_invalid_variant_conversion.\n

   @param data int32_t that will contain the result
   @param variant the variant of interest

   @return
    - true: the resulting int32_t is ready to be used
    - false: An error has occurred, pcb_error contains the reason why the set failed
        - an invalid variant was provided
        - an invalid data pointer was provided
        - memory allocation failed
 */
bool variant_toInt32(int32_t* data, const variant_t* variant) {
    if(!variant || !data) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    bool success = true;
    switch(variant->type) {
    case variant_type_string:
        *data = string_convertToInt32(&variant->data.str, &success);
        break;
    case variant_type_int8:
        *data = (int32_t) variant->data.i8;
        break;
    case variant_type_int16:
        *data = (int32_t) variant->data.i16;
        break;
    case variant_type_int32:
        *data = (int32_t) variant->data.i32;
        break;
    case variant_type_int64:
        *data = (variant->data.i64 < 0) ? (int32_t) variant->data.i64 : (int32_t) (variant->data.i64 & INT32_MAX);
        break;
    case variant_type_uint8:
        *data = (int32_t) variant->data.ui8;
        break;
    case variant_type_uint16:
        *data = (int32_t) variant->data.ui16;
        break;
    case variant_type_uint32:
        *data = (int32_t) variant->data.ui32 & INT32_MAX;
        break;
    case variant_type_uint64:
        *data = (int32_t) variant->data.ui64 & INT32_MAX;
        break;
    case variant_type_bool:
        *data = variant->data.b ? 1 : 0;
        break;
    case variant_type_reference:
        return variant_toInt32(data, variant->data.v);
        break;
    case variant_type_file_descriptor:
        *data = variant->data.fd;
        break;
    case variant_type_double:
    case variant_type_date_time:
    case variant_type_array_reference:
    case variant_type_array:
    case variant_type_map_reference:
    case variant_type_map:
    case variant_type_byte_array:
    default:
        pcb_error = pcb_error_invalid_variant_conversion;
        success = false;
        break;
    }

    return success;
}

/**
   @ingroup pcb_utils_variant
   @brief
   Get the value of the specified variant and copy it into the specified int64_t.

   @details
   This function converts the variant value to the int64_t type and copies it into the provided
   int64_t. The conversion type depends on the variant type. \n

   Supported conversions:\n
   @li string
   @li int8
   @li int16
   @li int32
   @li int64
   @li uint8
   @li uint16
   @li uint32
   @li uint64
   @li bool
   All other variant types can NOT be converted to a int64_t.
   If the conversion fails, pcb_error will be set to pcb_error_invalid_variant_conversion.\n

   @param data int64_t that will contain the result
   @param variant the variant of interest

   @return
    - true: the resulting int64_t is ready to be used
    - false: An error has occurred, pcb_error contains the reason why the set failed
        - an invalid variant was provided
        - an invalid data pointer was provided
        - memory allocation failed
 */
bool variant_toInt64(int64_t* data, const variant_t* variant) {
    if(!variant || !data) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    bool success = true;
    switch(variant->type) {
    case variant_type_string:
        *data = string_convertToInt64(&variant->data.str, &success);
        break;
    case variant_type_int8:
        *data = (int64_t) variant->data.i8;
        break;
    case variant_type_int16:
        *data = (int64_t) variant->data.i16;
        break;
    case variant_type_int32:
        *data = (int64_t) variant->data.i32;
        break;
    case variant_type_int64:
        *data = (int64_t) variant->data.i64;
        break;
    case variant_type_uint8:
        *data = (int64_t) variant->data.ui8;
        break;
    case variant_type_uint16:
        *data = (int64_t) variant->data.ui16;
        break;
    case variant_type_uint32:
        *data = (int64_t) variant->data.ui32;
        break;
    case variant_type_uint64:
        *data = (int64_t) variant->data.ui64 & INT64_MAX;
        break;
    case variant_type_bool:
        *data = variant->data.b ? 1 : 0;
        break;
    case variant_type_reference:
        return variant_toInt64(data, variant->data.v);
        break;
    case variant_type_file_descriptor:
        *data = -1;
        break;
    case variant_type_double:
    case variant_type_date_time:
    case variant_type_array_reference:
    case variant_type_array:
    case variant_type_map_reference:
    case variant_type_map:
    case variant_type_byte_array:
    default:
        pcb_error = pcb_error_invalid_variant_conversion;
        success = false;
        break;
    }

    return success;
}

/**
   @ingroup pcb_utils_variant
   @brief
   Get the value of the specified variant and copy it into the specified uint8_t.

   @details
   This function converts the variant value to the uint8_t type and copies it into the provided
   uint8_t. The conversion type depends on the variant type. \n

   Supported conversions:\n
   @li string
   @li int8
   @li uint8
   @li bool
   All other variant types can NOT be converted to a uint8_t.
   If the conversion fails, pcb_error will be set to pcb_error_invalid_variant_conversion.\n

   @param data uint8_t that will contain the result
   @param variant the variant of interest

   @return
    - true: the resulting uint8_t is ready to be used
    - false: An error has occurred, pcb_error contains the reason why the set failed
        - an invalid variant was provided
        - an invalid data pointer was provided
        - memory allocation failed
 */
bool variant_toUInt8(uint8_t* data, const variant_t* variant) {
    if(!variant || !data) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    bool success = true;
    switch(variant->type) {
    case variant_type_string:
        *data = string_convertToUInt8(&variant->data.str, &success);
        break;
    case variant_type_int8:
        *data = (uint8_t) abs(variant->data.i8);
        break;
    case variant_type_int16:
        *data = (uint8_t) abs(variant->data.i16);
        break;
    case variant_type_int32:
        *data = (uint8_t) labs(variant->data.i32);
        break;
    case variant_type_int64:
        *data = (uint8_t) llabs(variant->data.i64);
        break;
    case variant_type_uint8:
        *data = variant->data.ui8;
        break;
    case variant_type_uint16:
        *data = (uint8_t) variant->data.ui16;
        break;
    case variant_type_uint32:
        *data = (uint8_t) variant->data.ui32;
        break;
    case variant_type_uint64:
        *data = (uint8_t) variant->data.ui64;
        break;
    case variant_type_bool:
        *data = variant->data.b ? 1 : 0;
        break;
    case variant_type_reference:
        return variant_toUInt8(data, variant->data.v);
        break;
    case variant_type_file_descriptor:
        *data = -1;
        break;
    case variant_type_double:
    case variant_type_date_time:
    case variant_type_array_reference:
    case variant_type_array:
    case variant_type_map_reference:
    case variant_type_map:
    case variant_type_byte_array:
    default:
        pcb_error = pcb_error_invalid_variant_conversion;
        success = false;
        break;
    }

    return success;
}

/**
   @ingroup pcb_utils_variant
   @brief
   Get the value of the specified variant and copy it into the specified uint16_t.

   @details
   This function converts the variant value to the uint16_t type and copies it into the provided
   uint16_t. The conversion type depends on the variant type. \n

   Supported conversions:\n
   @li string
   @li int8
   @li int16
   @li uint8
   @li uint16
   @li bool
   All other variant types can NOT be converted to a uint16_t.
   If the conversion fails, pcb_error will be set to pcb_error_invalid_variant_conversion.\n

   @param data uint16_t that will contain the result
   @param variant the variant of interest

   @return
    - true: the resulting uint16_t is ready to be used
    - false: An error has occurred, pcb_error contains the reason why the set failed
        - an invalid variant was provided
        - an invalid data pointer was provided
        - memory allocation failed
 */
bool variant_toUInt16(uint16_t* data, const variant_t* variant) {
    if(!variant || !data) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    bool success = true;
    switch(variant->type) {
    case variant_type_string:
        *data = string_convertToUInt16(&variant->data.str, &success);
        break;
    case variant_type_int8:
        *data = (uint16_t) abs(variant->data.i8);
        break;
    case variant_type_int16:
        *data = (uint16_t) abs(variant->data.i16);
        break;
    case variant_type_int32:
        *data = (uint16_t) labs(variant->data.i32);
        break;
    case variant_type_int64:
        *data = (uint16_t) llabs(variant->data.i64);
        break;
    case variant_type_uint8:
        *data = (uint16_t) variant->data.ui8;
        break;
    case variant_type_uint16:
        *data = (uint16_t) variant->data.ui16;
        break;
    case variant_type_uint32:
        *data = (uint16_t) variant->data.ui32;
        break;
    case variant_type_uint64:
        *data = (uint16_t) variant->data.ui64;
        break;
    case variant_type_bool:
        *data = variant->data.b ? 1 : 0;
        break;
    case variant_type_reference:
        return variant_toUInt16(data, variant->data.v);
        break;
    case variant_type_file_descriptor:
        *data = -1;
        break;
    case variant_type_double:
    case variant_type_date_time:
    case variant_type_array_reference:
    case variant_type_array:
    case variant_type_map_reference:
    case variant_type_map:
    case variant_type_byte_array:
    default:
        pcb_error = pcb_error_invalid_variant_conversion;
        success = false;
        break;
    }

    return success;
}

/**
   @ingroup pcb_utils_variant
   @brief
   Get the value of the specified variant and copy it into the specified uint32_t.

   @details
   This function converts the variant value to the uint32_t type and copies it into the provided
   uint32_t. The conversion type depends on the variant type. \n

   Supported conversions:\n
   @li string
   @li int8
   @li int16
   @li int32
   @li uint8
   @li uint16
   @li uint32
   @li bool
   All other variant types can NOT be converted to a uint32_t.
   If the conversion fails, pcb_error will be set to pcb_error_invalid_variant_conversion.\n

   @param data uint32_t that will contain the result
   @param variant the variant of interest

   @return
    - true: the resulting uint32_t is ready to be used
    - false: An error has occurred, pcb_error contains the reason why the set failed
        - an invalid variant was provided
        - an invalid data pointer was provided
        - memory allocation failed
 */
bool variant_toUInt32(uint32_t* data, const variant_t* variant) {
    if(!variant || !data) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    bool success = true;
    switch(variant->type) {
    case variant_type_string:
        *data = string_convertToUInt32(&variant->data.str, &success);
        break;
    case variant_type_int8:
        *data = (uint32_t) abs(variant->data.i8);
        break;
    case variant_type_int16:
        *data = (uint32_t) abs(variant->data.i16);
        break;
    case variant_type_int32:
        *data = (uint32_t) labs(variant->data.i32);
        break;
    case variant_type_int64:
        *data = (uint32_t) llabs(variant->data.i64);
        break;
    case variant_type_uint8:
        *data = (uint32_t) variant->data.ui8;
        break;
    case variant_type_uint16:
        *data = (uint32_t) variant->data.ui16;
        break;
    case variant_type_uint32:
        *data = (uint32_t) variant->data.ui32;
        break;
    case variant_type_uint64:
        *data = (uint32_t) variant->data.ui64;
        break;
    case variant_type_bool:
        *data = variant->data.b ? 1 : 0;
        break;
    case variant_type_reference:
        return variant_toUInt32(data, variant->data.v);
        break;
    case variant_type_file_descriptor:
        *data = -1;
        break;
    case variant_type_double:
    case variant_type_date_time:
    case variant_type_array_reference:
    case variant_type_array:
    case variant_type_map_reference:
    case variant_type_map:
    case variant_type_byte_array:
    default:
        pcb_error = pcb_error_invalid_variant_conversion;
        success = false;
        break;
    }

    return success;
}

/**
   @ingroup pcb_utils_variant
   @brief
   Get the value of the specified variant and copy it into the specified uint64_t.

   @details
   This function converts the variant value to the uint64_t type and copies it into the provided
   uint64_t. The conversion type depends on the variant type. \n

   Supported conversions:\n
   @li string
   @li int8
   @li int16
   @li int32
   @li int64
   @li uint8
   @li uint16
   @li uint32
   @li uint64
   @li bool
   All other variant types can NOT be converted to a uint32_t.
   If the conversion fails, pcb_error will be set to pcb_error_invalid_variant_conversion.\n

   @param data uint32_t that will contain the result
   @param variant the variant of interest

   @return
    - true: the resulting uint32_t is ready to be used
    - false: An error has occurred, pcb_error contains the reason why the set failed
        - an invalid variant was provided
        - an invalid data pointer was provided
        - memory allocation failed
 */
bool variant_toUInt64(uint64_t* data, const variant_t* variant) {
    if(!variant || !data) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    bool success = true;
    switch(variant->type) {
    case variant_type_string:
        *data = string_convertToUInt64(&variant->data.str, &success);
        break;
    case variant_type_int8:
        *data = (uint64_t) abs(variant->data.i8);
        break;
    case variant_type_int16:
        *data = (uint64_t) abs(variant->data.i16);
        break;
    case variant_type_int32:
        *data = (uint64_t) labs(variant->data.i32);
        break;
    case variant_type_int64:
        *data = (uint64_t) llabs(variant->data.i64);
        break;
    case variant_type_uint8:
        *data = (uint64_t) variant->data.ui8;
        break;
    case variant_type_uint16:
        *data = (uint64_t) variant->data.ui16;
        break;
    case variant_type_uint32:
        *data = (uint64_t) variant->data.ui32;
        break;
    case variant_type_uint64:
        *data = (uint64_t) variant->data.ui64;
        break;
    case variant_type_bool:
        *data = variant->data.b ? 1 : 0;
        break;
    case variant_type_reference:
        return variant_toUInt64(data, variant->data.v);
        break;
    case variant_type_file_descriptor:
        *data = -1;
        break;
    case variant_type_double:
    case variant_type_date_time:
    case variant_type_array_reference:
    case variant_type_array:
    case variant_type_map_reference:
    case variant_type_map:
    case variant_type_byte_array:
    default:
        pcb_error = pcb_error_invalid_variant_conversion;
        success = false;
        break;
    }

    return success;
}

/**
   @ingroup pcb_utils_variant
   @brief
   Get the value of the specified variant and copy it into the specified bool.

   @details
   This function converts the variant value to the bool type and copies it into the provided
   bool. The conversion type depends on the variant type. \n

   All variant types (except the datetime type) can be converted to a bool.\n

   @param data bool that will contain the result
   @param variant the variant of interest

   @return
    - true: the resulting bool is ready to be used
    - false: An error has occurred, pcb_error contains the reason why the set failed
        - an invalid variant was provided
        - an invalid data pointer was provided
        - memory allocation failed
 */
bool variant_toBool(bool* data, const variant_t* variant) {
    if(!variant || !data) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    bool success = true;
    switch(variant->type) {
    case variant_type_string:
        *data = string_convertToBool(&variant->data.str, &success);
        break;
    case variant_type_int8:
        *data = (variant->data.i8) ? true : false;
        break;
    case variant_type_int16:
        *data = (variant->data.i16) ? true : false;
        break;
    case variant_type_int32:
        *data = (variant->data.i32) ? true : false;
        break;
    case variant_type_int64:
        *data = (variant->data.i64) ? true : false;
        break;
    case variant_type_uint8:
        *data = (variant->data.ui8) ? true : false;
        break;
    case variant_type_uint16:
        *data = (variant->data.ui16) ? true : false;
        break;
    case variant_type_uint32:
        *data = (variant->data.ui32) ? true : false;
        break;
    case variant_type_uint64:
        *data = (variant->data.ui64) ? true : false;
        break;
    case variant_type_bool:
        *data = variant->data.b;
        break;
    case variant_type_double:
        *data = (variant->data.d) ? true : false;
        break;
    case variant_type_reference:
        return variant_toBool(data, variant->data.v);
        break;
    case variant_type_file_descriptor:
        if((fcntl(variant->data.fd, F_GETFD) == -1) && (errno == EBADF)) {
            *data = false;
        } else {
            *data = true;
        }
        break;
    case variant_type_date_time:
    case variant_type_array_reference:
    case variant_type_array:
    case variant_type_map_reference:
    case variant_type_map:
    case variant_type_byte_array:
    default:
        pcb_error = pcb_error_invalid_variant_conversion;
        success = false;
        break;
    }

    return success;
}

/**
   @ingroup pcb_utils_variant
   @brief
   Get the value of the specified variant and copy it into the specified double.

   @details
   This function converts the variant value to the double type and copies it into the provided
   double. The conversion type depends on the variant type. \n

   All variant types (except the datetime type) can be converted to a double.\n

   @param data double that will contain the result
   @param variant the variant of interest

   @return
    - true: the resulting double is ready to be used
    - false: An error has occurred, pcb_error contains the reason why the set failed
        - an invalid variant was provided
        - an invalid data pointer was provided
        - memory allocation failed
 */
bool variant_toDouble(double* data, const variant_t* variant) {
    if(!variant || !data) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    bool success = true;
    switch(variant->type) {
    case variant_type_string:
        *data = string_convertToDouble(&variant->data.str, &success);
        break;
    case variant_type_int8:
        *data = (double) variant->data.i8;
        break;
    case variant_type_int16:
        *data = (double) variant->data.i16;
        break;
    case variant_type_int32:
        *data = (double) variant->data.i32;
        break;
    case variant_type_int64:
        *data = (double) variant->data.i64;
        break;
    case variant_type_uint8:
        *data = (double) variant->data.ui8;
        break;
    case variant_type_uint16:
        *data = (double) variant->data.ui16;
        break;
    case variant_type_uint32:
        *data = (double) variant->data.ui32;
        break;
    case variant_type_uint64:
        *data = (double) variant->data.ui64;
        break;
    case variant_type_bool:
        *data = (variant->data.b) ? 1 : 0;
        break;
    case variant_type_double:
        *data = variant->data.d;
        break;
    case variant_type_reference:
        return variant_toDouble(data, variant->data.v);
        break;
    case variant_type_file_descriptor:
        *data = -1;
        break;
    case variant_type_date_time:
    case variant_type_array_reference:
    case variant_type_array:
    case variant_type_map_reference:
    case variant_type_map:
    case variant_type_byte_array:
    default:
        pcb_error = pcb_error_invalid_variant_conversion;
        return false;
        break;
    }

    return success;
}

/**
   @ingroup pcb_utils_variant
   @brief
   Get the value of the specified variant and copy it into the specified datetime structure.

   @details
   This function converts the variant value to the struct tm  type and copies it into the provided
   struct tm  *. The conversion type depends on the variant type. \n

   The struct tm data must be initialized before calling this function. The user is responsible for
   cleaning up the resulting structure.\n
   Only strings and datetime variants can be converted to a struct tm.\n

   @param data the initialized pcb_datetime_t that will contain the result
   @param variant the variant of interest

   @return
   - true: the resulting struct tm is ready to be used
   - false: An error has occurred, pcb_error contains the reason why the set failed
   - an invalid variant was provided
   - an invalid struct tm was provided
   - memory allocation failed
 */
bool variant_toDateTimeExtended(pcb_datetime_t* data, const variant_t* variant) {
    pcb_datetime_t* tempData = NULL;
    if(!variant || !data) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    switch(variant->type) {
    case variant_type_string:
        // using string_toTimeExtended on the input string (when the input string is a number, e.g. "1234")
        //      will parse it as a year, (e.g. resulting in a datetime of "1234-01-01T00:00:00Z" (1st of january, 1234))
        //      which is usually not desired. Explicitly check for the correct format first
        //      Only exception is "0", which yields the default datetime ("0001-01-01T00:00:00Z")
        if(string_compareChar(&variant->data.str, "0", string_case_sensitive) == 0) {
            pcb_datetime_initialize(data, PCB_EPOCH_TR098);
            break;
        } else if(string_containsChar(&variant->data.str, "-", string_case_sensitive)) {
            tempData = string_toTimeExtended(&variant->data.str, NULL);
            if(tempData) {
                memcpy(data, tempData, sizeof(pcb_datetime_t));
                free(tempData);
                break;
            }
        }
        memset(data, 0, sizeof(pcb_datetime_t));
        pcb_error = pcb_error_invalid_variant_conversion;
        return false;
        break;
    case variant_type_date_time:
        if(variant->data.dt) {
            *data = *variant->data.dt;
        } else {
            memset(data, 0, sizeof(pcb_datetime_t));
            pcb_error = pcb_error_invalid_parameter;
            return false;
        }
        break;
    case variant_type_reference:
        return variant_toDateTimeExtended(data, variant->data.v);
        break;
    case variant_type_int8:
    case variant_type_int16:
    case variant_type_int32:
    case variant_type_int64:
    case variant_type_uint8:
    case variant_type_uint16:
    case variant_type_uint32:
    case variant_type_uint64:
    case variant_type_bool:
    case variant_type_double:
    case variant_type_array_reference:
    case variant_type_array:
    case variant_type_map_reference:
    case variant_type_map:
    case variant_type_file_descriptor:
    case variant_type_byte_array:
    default:
        pcb_error = pcb_error_invalid_variant_conversion;
        return false;
        break;
    }

    return true;
}

/**
   @ingroup pcb_utils_variant
   @brief
   Get the value of the specified variant and copy it into the specified datetime structure.

   @details
   This function converts the variant value to the struct tm  type and copies it into the provided
   struct tm  *. The conversion type depends on the variant type. \n

   The struct tm data must be initialized before calling this function. The user is responsible for
   cleaning up the resulting structure.\n
   Only strings and datetime variants can be converted to a struct tm.\n

   @param data the initialized struct tm that will contain the result
   @param variant the variant of interest

   @return
    - true: the resulting struct tm is ready to be used
    - false: An error has occurred, pcb_error contains the reason why the set failed
        - an invalid variant was provided
        - an invalid struct tm was provided
        - memory allocation failed
 */
bool variant_toDateTime(struct tm* data, const variant_t* variant) {
    pcb_datetime_t temp;
    bool rv = variant_toDateTimeExtended(&temp, variant);
    if(!rv) {
        return false;
    }
    *data = temp.datetime;

    return true;
}

/**
   @ingroup pcb_utils_variant
   @brief
   Converts a variant to a variant list.

   @details
   This function converts the variant value to a variant list.\n
   The conversion depends on the type of the variant given.\n
   Simple types are added to the variant list.\n
   If the given variant is a variant list or a variant map, all elements of the given
   list or map are added the to variant list.

   The variant list provided must be initialized.\n

   @param data the initialized variant list that will contain the result
   @param variant the variant of interest

   @return
    - true: the resulting struct tm is ready to be used
    - false: An error has occurred, pcb_error contains the reason why the set failed
        - an invalid variant was provided
        - an invalid variant list was provided
        - memory allocation failed
 */
bool variant_toList(variant_list_t* data, const variant_t* variant) {
    if(!variant || !data) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    variant_list_clear(data);
    variant_list_iterator_t* it = NULL;

    switch(variant->type) {
    default:
    case variant_type_string:
    case variant_type_int8:
    case variant_type_int16:
    case variant_type_int32:
    case variant_type_int64:
    case variant_type_uint8:
    case variant_type_uint16:
    case variant_type_uint32:
    case variant_type_uint64:
    case variant_type_bool:
    case variant_type_double:
    case variant_type_date_time:
    case variant_type_file_descriptor:
    case variant_type_byte_array:
        it = variant_list_iterator_create(variant);
        variant_list_append(data, it);
        break;
    case variant_type_array_reference:
    case variant_type_array: {
        variant_list_iterator_t* ref_it = NULL;
        variant_list_iterator_t* copy = NULL;
        variant_list_for_each(ref_it, variant->data.vl) {
            copy = variant_list_iterator_create(&ref_it->variant);
            if(copy) {
                variant_list_append(data, copy);
            }
        }
    }
    break;
    case variant_type_map_reference:
    case variant_type_map: {
        variant_map_iterator_t* ref_it = NULL;
        variant_list_iterator_t* copy = NULL;
        variant_map_for_each(ref_it, variant->data.vm) {
            copy = variant_list_iterator_create(&ref_it->variant);
            if(copy) {
                variant_list_append(data, copy);
            }
        }
    }
    break;
    case variant_type_reference:
        return variant_toList(data, variant->data.v);
        break;
    }

    return true;
}

/**
   @ingroup pcb_utils_variant
   @brief
   Converts a variant to a variant map.

   @details
   This function converts the variant value to a variant map.\n
   The conversion depends on the type of the variant given.\n
   Simple types and variant lists can not be converted to a variant map.\n
   If the given variant is a variant map, it is added to the specified variant map.

   The variant map provided must be initialized.\n

   @param data the initialized variant map that will contain the result
   @param variant the variant of interest

   @return
    - true: the resulting struct tm is ready to be used
    - false: An error has occurred, pcb_error contains the reason why the set failed
        - an invalid variant was provided
        - an invalid variant map was provided
        - memory allocation failed
 */
bool variant_toMap(variant_map_t* data, const variant_t* variant) {
    if(!variant || !data) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    variant_map_clear(data);

    switch(variant->type) {
    default:
    case variant_type_string:
    case variant_type_int8:
    case variant_type_int16:
    case variant_type_int32:
    case variant_type_int64:
    case variant_type_uint8:
    case variant_type_uint16:
    case variant_type_uint32:
    case variant_type_uint64:
    case variant_type_bool:
    case variant_type_double:
    case variant_type_date_time:
    case variant_type_array_reference:
    case variant_type_array:
    case variant_type_file_descriptor:
    case variant_type_byte_array:
        pcb_error = pcb_error_invalid_variant_conversion;
        return false;
        break;
    case variant_type_map_reference:
    case variant_type_map: {
        variant_map_iterator_t* it = NULL;
        variant_map_iterator_t* copy = NULL;
        variant_map_for_each(it, variant->data.vm) {
            copy = variant_map_iterator_create(it->key, &it->variant);
            if(copy) {
                variant_map_append(data, copy);
            }
        }
    }
    break;
    case variant_type_reference:
        return variant_toMap(data, variant->data.v);
        break;
    }

    return true;
}

/**
   @ingroup pcb_utils_variant
   @brief
   Converts a variant to a file descriptor.

   @details
   This function converts the variant value to a file descriptor.\n
   Only variants of the type file descriptor can return a valid file descriptor.
   Variants of another type will always return false.\n

   @param data the file descriptor
   @param variant the variant of interest

   @return
    - true:
    - false:
 */
bool variant_toFd(int* data, const variant_t* variant) {
    if(!variant || !data) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    switch(variant->type) {
    case variant_type_string:
    case variant_type_int8:
    case variant_type_int16:
    case variant_type_int32:
    case variant_type_int64:
    case variant_type_uint8:
    case variant_type_uint16:
    case variant_type_uint32:
    case variant_type_uint64:
    case variant_type_bool:
    case variant_type_double:
    case variant_type_date_time:
    case variant_type_array_reference:
    case variant_type_array:
    case variant_type_map_reference:
    case variant_type_map:
    case variant_type_byte_array:
        pcb_error = pcb_error_invalid_variant_conversion;
        return false;
        break;
    case variant_type_file_descriptor:
        *data = variant->data.fd;
        break;
    case variant_type_reference:
        return variant_toFd(data, variant->data.v);
        break;
    default:
        pcb_error = pcb_error_invalid_variant_conversion;
        return false;
        break;
    }

    return true;
}

/**
   @ingroup pcb_utils_variant
   @brief
   Converts a variant to a binary byte array.

   @details
   This function converts the variant value to a binary byte array.\n
   Only variants of the type byte array can return byte array.\n
   Variants of another type will always return false.\n

   @param data the file descriptor
   @param size the file descriptor
   @param variant the variant of interest

   @return
    - true:
    - false:
 */
bool variant_toByteArray(void** data, uint32_t* size, const variant_t* variant) {
    if(!variant || !data) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    switch(variant->type) {
    case variant_type_string:
    case variant_type_int8:
    case variant_type_int16:
    case variant_type_int32:
    case variant_type_int64:
    case variant_type_uint8:
    case variant_type_uint16:
    case variant_type_uint32:
    case variant_type_uint64:
    case variant_type_bool:
    case variant_type_double:
    case variant_type_date_time:
    case variant_type_array_reference:
    case variant_type_array:
    case variant_type_map_reference:
    case variant_type_map:
    case variant_type_file_descriptor:
        pcb_error = pcb_error_invalid_variant_conversion;
        return false;
        break;
    case variant_type_reference:
        return variant_toByteArray(data, size, variant->data.v);
        break;
    case variant_type_byte_array: {
        uint32_t length = variant->data.ba->size;
        if(length) {
            *data = malloc(length);
            if(*data) {
                memcpy(*data, variant->data.ba->data, length);
                *size = length;
            } else {
                *size = 0;
                return false;
            }
        } else {
            *data = NULL;
            *size = 0;
        }
    }
    break;
    default:
        break;
    }

    return true;
}

/**
   @ingroup pcb_utils_variant
   @brief
   Converts a variant to a char array.

   @details
   This function is provided for convience and is doing the same as @ref variant_toChar.
   The returned char* must be freed.\n

   @param variant the variant of interest

   @return
    - a char pointer,
    - NULL if the conversion was not successful.
 */
char* variant_char(const variant_t* variant) {
    if(!variant) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    char* data = NULL;
    variant_toChar(&data, variant);

    return data;
}

/**
   @ingroup pcb_utils_variant
   @brief
   Converts a variant to an int8.

   @details
   This function is provided for convience and is doing the same as @ref variant_toInt8.\n

   @param variant the variant of interest

   @return
    - The converted value
 */
int8_t variant_int8(const variant_t* variant) {
    if(!variant) {
        pcb_error = pcb_error_invalid_parameter;
        return 0;
    }

    int8_t data = 0;
    variant_toInt8(&data, variant);

    return data;
}

/**
   @ingroup pcb_utils_variant
   @brief
   Converts a variant to an int16.

   @details
   This function is provided for convience and is doing the same as @ref variant_toInt16.\n

   @param variant the variant of interest

   @return
    - The converted value
 */
int16_t variant_int16(const variant_t* variant) {
    if(!variant) {
        pcb_error = pcb_error_invalid_parameter;
        return 0;
    }

    int16_t data = 0;
    variant_toInt16(&data, variant);

    return data;
}

/**
   @ingroup pcb_utils_variant
   @brief
   Converts a variant to an int32.

   @details
   This function is provided for convience and is doing the same as @ref variant_toInt32.\n

   @param variant the variant of interest

   @return
    - The converted value
 */
int32_t variant_int32(const variant_t* variant) {
    if(!variant) {
        pcb_error = pcb_error_invalid_parameter;
        return 0;
    }

    int32_t data = 0;
    variant_toInt32(&data, variant);

    return data;
}

/**
   @ingroup pcb_utils_variant
   @brief
   Converts a variant to an int64.

   @details
   This function is provided for convience and is doing the same as @ref variant_toInt64.
   \n

   @param variant the variant of interest

   @return
    - The converted value
 */
int64_t variant_int64(const variant_t* variant) {
    if(!variant) {
        pcb_error = pcb_error_invalid_parameter;
        return 0;
    }

    int64_t data = 0;
    variant_toInt64(&data, variant);

    return data;
}

/**
   @ingroup pcb_utils_variant
   @brief
   Converts a variant to an unsigned int8.

   @details
   This function is provided for convience and is doing the same as @ref variant_toUInt8.\n

   @param variant the variant of interest

   @return
    - The converted value
 */
uint8_t variant_uint8(const variant_t* variant) {
    if(!variant) {
        pcb_error = pcb_error_invalid_parameter;
        return 0;
    }

    uint8_t data = 0;
    variant_toUInt8(&data, variant);

    return data;
}

/**
   @ingroup pcb_utils_variant
   @brief
   Converts a variant to an unsigned int16.

   @details
   This function is provided for convience and is doing the same as @ref variant_toUInt16.\n

   @param variant the variant of interest

   @return
    - The converted value
 */
uint16_t variant_uint16(const variant_t* variant) {
    if(!variant) {
        pcb_error = pcb_error_invalid_parameter;
        return 0;
    }

    uint16_t data = 0;
    variant_toUInt16(&data, variant);

    return data;
}

/**
   @ingroup pcb_utils_variant
   @brief
   Converts a variant to an unsigned int32.

   @details
   This function is provided for convience and is doing the same as @ref variant_toUInt32.\n

   @param variant the variant of interest

   @return
    - The converted value
 */
uint32_t variant_uint32(const variant_t* variant) {
    if(!variant) {
        pcb_error = pcb_error_invalid_parameter;
        return 0;
    }

    uint32_t data = 0;
    variant_toUInt32(&data, variant);

    return data;
}

/**
   @ingroup pcb_utils_variant
   @brief
   Converts a variant to an unsigned int64.

   @details
   This function is provided for convience and is doing the same as @ref variant_toUInt64.\n

   @param variant the variant of interest

   @return
    - The converted value
 */
uint64_t variant_uint64(const variant_t* variant) {
    if(!variant) {
        pcb_error = pcb_error_invalid_parameter;
        return 0;
    }

    uint64_t data = 0;
    variant_toUInt64(&data, variant);

    return data;
}

/**
   @ingroup pcb_utils_variant
   @brief
   Converts a variant to a boolean.

   @details
   This function is provided for convience and is doing the same as @ref variant_toBool.\n

   @param variant the variant of interest

   @return
    - The converted value
 */
bool variant_bool(const variant_t* variant) {
    if(!variant) {
        pcb_error = pcb_error_invalid_parameter;
        return 0;
    }

    bool data = false;
    variant_toBool(&data, variant);

    return data;
}

/**
   @ingroup pcb_utils_variant
   @brief
   Converts a variant to a double.

   @details
   This function is provided for convience and is doing the same as @ref variant_toDouble.\n

   @param variant the variant of interest

   @return
    - The converted value
 */
double variant_double(const variant_t* variant) {
    if(!variant) {
        pcb_error = pcb_error_invalid_parameter;
        return 0;
    }

    double data = 0;
    variant_toDouble(&data, variant);

    return data;
}

/**
   @ingroup pcb_utils_variant
   @brief
   Converts a variant to a date time.

   @details
   This function is provided for convience and is doing the same as @ref variant_toDateTime.
   The returned pointer must be freed.\n

   @param variant the variant of interest

   @return
    - The converted value
 */
struct tm* variant_dateTime(const variant_t* variant) {
    if(!variant) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    struct tm* data = (struct tm*) calloc(1, sizeof(struct tm));
    if(!data) {
        return NULL;
    }

    if(!variant_toDateTime(data, variant)) {
        free(data);
        return NULL;
    }

    return data;
}

/**
   @ingroup pcb_utils_variant
   @brief
   Converts a variant to a date time, with nanosecond precision (pcb_datetime_t).

   @details
   This function is provided for convience and is doing the same as @ref variant_toDateTimeExtended.
   The returned pointer must be freed.\n

   @param variant the variant of interest

   @return
   - The converted value
 */
pcb_datetime_t* variant_dateTimeExtended(const variant_t* variant) {
    if(!variant) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    pcb_datetime_t* data = calloc(1, sizeof(pcb_datetime_t));
    if(!data) {
        return NULL;
    }

    if(!variant_toDateTimeExtended(data, variant)) {
        free(data);
        return NULL;
    }

    return data;
}

variant_list_t* variant_list(const variant_t* variant) {
    if(!variant) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    variant_list_t* data = (variant_list_t*) calloc(1, sizeof(variant_list_t));
    if(!data) {
        return NULL;
    }

    if(!variant_list_initialize(data)) {
        free(data);
        return NULL;
    }

    if(!variant_toList(data, variant)) {
        variant_list_cleanup(data);
        free(data);
        return NULL;
    }

    return data;
}

/**
   @ingroup pcb_utils_variant
   @brief
   Converts a variant to a variant map.

   @details
   This function is provided for convience and is doing the same as @ref variant_toMap.
   The returned pointer must be freed.\n

   @param variant the variant of interest

   @return
    - The converted value
 */
variant_map_t* variant_map(const variant_t* variant) {
    if(!variant) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    variant_map_t* data = (variant_map_t*) calloc(1, sizeof(variant_map_t));
    if(!data) {
        return NULL;
    }

    if(!variant_map_initialize(data)) {
        free(data);
        return NULL;
    }

    if(!variant_toMap(data, variant)) {
        variant_map_cleanup(data);
        free(data);
        return NULL;
    }

    return data;
}

/**
   @ingroup pcb_utils_variant
   @brief
   Converts a variant to a file descriptor

   @details
   This function is provided for convience and is doing the same as @ref variant_toFd.\n

   @param variant the variant of interest

   @return
    - The converted value
 */
int variant_fd(const variant_t* variant) {
    if(!variant) {
        pcb_error = pcb_error_invalid_parameter;
        return -1;
    }

    int data = -1;

    if(!variant_toFd(&data, variant)) {
        return -1;
    }

    return data;
}

/**
   @ingroup pcb_utils_variant
   @brief
   Converts a variant to a byte array

   @details
   This function is provided for convience and is doing the same as @ref variant_toByteArray.\n

   @param variant the variant of interest
   @param size pointer to an uint32 that will contain the size of the returned buffer

   @return
    - The converted value
 */
void* variant_byteArray(const variant_t* variant, uint32_t* size) {
    if(!variant) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    void* data = NULL;

    if(!variant_toByteArray(&data, size, variant)) {
        return NULL;
    }

    return data;
}

/**
   @ingroup pcb_utils_variant
   @brief
   Get a struct tm pointer directly to the date time data of a date time variant.

   @details
   This function returns a pointer directly to the date time data contained within the variant.\n
   The resulting date time may only be used to READ, NOT WRITE. This function only works when
   the variant type is a date time.\n
   The returned pointer MUST not be freed.\n

   @param variant the variant of interest

   @return
   A pointer to a struct tm, NULL if the variant is invalid or not of the date time type.
 */
const struct tm* variant_da_dateTime(const variant_t* variant) {
    if(!variant) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    if(variant->type == variant_type_reference) {
        variant = variant->data.v;
    }

    if(variant->type != variant_type_date_time) {
        pcb_error = pcb_error_invalid_variant_type;
        return NULL;
    }

    return &variant->data.dt->datetime;
}

/**
   @ingroup pcb_utils_variant
   @brief
   Get a pcb_datetime_t pointer directly to the date time data of a date time variant.

   @details
   This function returns a pointer directly to the date time data contained within the variant.\n
   The resulting date time may only be used to READ, NOT WRITE. This function only works when
   the variant type is a date time.\n
   The returned pointer MUST NOT be freed.\n

   @param variant the variant of interest

   @return
   A pointer to a pcb_datetime_t, NULL if the variant is invalid or not of the date time type.
 */
const pcb_datetime_t* variant_da_dateTimeExtended(const variant_t* variant) {
    if(!variant) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    if(variant->type == variant_type_reference) {
        variant = variant->data.v;
    }

    if(variant->type != variant_type_date_time) {
        pcb_error = pcb_error_invalid_variant_type;
        return NULL;
    }

    return variant->data.dt;
}

variant_list_t* variant_da_list(const variant_t* variant) {
    if(!variant) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    if(variant->type == variant_type_reference) {
        variant = variant->data.v;
    }

    if((variant->type != variant_type_array) &&
       (variant->type != variant_type_array_reference)) {
        pcb_error = pcb_error_invalid_variant_type;
        return NULL;
    }

    return variant->data.vl;
}

variant_map_t* variant_da_map(const variant_t* variant) {
    if(!variant) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    if(variant->type == variant_type_reference) {
        variant = variant->data.v;
    }

    if((variant->type != variant_type_map) &&
       (variant->type != variant_type_map_reference)) {
        pcb_error = pcb_error_invalid_variant_type;
        return NULL;
    }

    return variant->data.vm;
}

/**
   @ingroup pcb_utils_variant
   @brief
   Get a variant pointer directly to the variant data of a variant reference.

   @details
   This function returns a variant pointer directly to the variant data contained within the variant reference.\n
   The resulting variant may be used and modified, but NOT FREED. The ownership of the pointer is kept by the variant\n
   This function only works when the variant type is a reference.\n
   The returned pointer MUST not be freed.
   If the variant is not a reference variant, this function will return a pointer to the variant itself.\n

   @param variant the variant of interest

   @return
   The resulting variant pointer, NULL if the variant is invalid.
 */
variant_t* variant_da_variant(const variant_t* variant) {
    if(!variant) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    if(variant->type != variant_type_reference) {
        return (variant_t*) variant;
    }

    return variant->data.v;
}

/**
   @ingroup pcb_utils_variant
   @brief
   Get a pointer to a memory block directly of a byte array variant.

   @details
   This function returns a variant pointer directly to the memory block contained within the variant.\n
   The returned pointer may be used to modify the data, but NOT FREED or realloced. The ownership of the pointer is kept by the variant\n
   This function only works when the variant type is a byte array.\n
   The returned pointer MUST not be freed.
   If the variant is not a byte array, this function will return a NULL pointer.\n

   @param variant the variant of interest
   @param size pointer to a uint32, that will contain the size of the memory block.

   @return
   A pointer to a memory block, or NULL if the variant is not of the byte array type.
 */
const void* variant_da_byteArray(const variant_t* variant, uint32_t* size) {
    if(!variant || !size) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    if(variant->type == variant_type_reference) {
        variant = variant->data.v;
    }

    if(variant->type != variant_type_byte_array) {
        *size = 0;
        return NULL;
    }

    *size = variant->data.ba->size;
    return variant->data.ba->data;
}

/**
   @ingroup pcb_utils_variant
   @brief
   Get a char pointer directly to the variant data of a variant string.

   @details
   This function returns a char pointer directly to the variant data contained within the variant string.\n
   This function only works when the variant type is a string.\n
   The returned pointer MUST not be freed.\n

   @param variant the variant of interest

   @return
   The resulting char pointer, NULL if the variant is invalid.
 */
const char* variant_da_char(const variant_t* variant) {
    if(!variant) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    if(variant->type == variant_type_reference) {
        variant = variant->data.v;
    }

    if(variant->type != variant_type_string) {
        pcb_error = pcb_error_invalid_variant_type;
        return NULL;
    }

    return string_buffer(&variant->data.str);
}

/**
   @ingroup pcb_utils_variant
   @brief
   Get the variant type.

   @details
   This function returns type of the specified variant.\n

   @param variant the variant of interest

   @return
   The resulting variant type, 'unknown' if an invalid variant pointer was provided
 */
variant_type_t variant_type(const variant_t* variant) {
    if(!variant) {
        pcb_error = pcb_error_invalid_parameter;
        return variant_type_unknown;
    }

    return variant->type;
}

/**
   @ingroup pcb_utils_variant
   @brief
   Change the type of a variant.

   @details
   If the variant was containing data, changing the type will clear the data and reinitializes the variant to the new type.\n

   @param variant the variant of interest
   @param type the new variant type

   @return
    - true when successful.
 */
bool variant_setType(variant_t* variant, variant_type_t type) {
    if(!variant) {
        pcb_error = pcb_error_invalid_parameter;
        return variant_type_unknown;
    }

    variant_cleanup(variant);
    return variant_initialize(variant, type);
}

typedef bool (* variant_compare_t) (const variant_t* var1, const variant_t* var2, int* result);

static bool variant_compare_unknown(const variant_t* var1 __attribute__((unused)), const variant_t* var2, int* result) {
    if(var2->type == variant_type_unknown) {
        *result = 0;
        return true;
    } else {
        *result = -1;
        return true;
    }
    return false;
}

static bool variant_compare_string(const variant_t* var1, const variant_t* var2, int* result) {
    string_t s2;
    string_initialize(&s2, 64);
    if(!variant_toString(&s2, var2)) {
        string_cleanup(&s2);
        return false;
    }
    *result = string_compare(&var1->data.str, &s2, string_case_sensitive);
    string_cleanup(&s2);
    return true;
}

static bool variant_compare_int8(const variant_t* var1, const variant_t* var2, int* result) {
    int8_t val2;
    if(!variant_toInt8(&val2, var2)) {
        return false;
    }
    if(var1->data.i8 < val2) {
        *result = -1;
    } else if(var1->data.i8 > val2) {
        *result = 1;
    } else {
        *result = 0;
    }

    return true;
}

static bool variant_compare_int16(const variant_t* var1, const variant_t* var2, int* result) {
    int16_t val2;
    if(!variant_toInt16(&val2, var2)) {
        return false;
    }
    if(var1->data.i16 < val2) {
        *result = -1;
    } else if(var1->data.i16 > val2) {
        *result = 1;
    } else {
        *result = 0;
    }

    return true;
}

static bool variant_compare_int32(const variant_t* var1, const variant_t* var2, int* result) {
    int32_t val2;
    if(!variant_toInt32(&val2, var2)) {
        return false;
    }
    if(var1->data.i32 < val2) {
        *result = -1;
    } else if(var1->data.i32 > val2) {
        *result = 1;
    } else {
        *result = 0;
    }

    return true;
}

static bool variant_compare_int64(const variant_t* var1, const variant_t* var2, int* result) {
    int64_t val2;
    if(!variant_toInt64(&val2, var2)) {
        return false;
    }
    if(var1->data.i64 < val2) {
        *result = -1;
    } else if(var1->data.i64 > val2) {
        *result = 1;
    } else {
        *result = 0;
    }

    return true;
}

static bool variant_compare_uint8(const variant_t* var1, const variant_t* var2, int* result) {
    uint8_t val2;
    if(!variant_toUInt8(&val2, var2)) {
        return false;
    }
    if(var1->data.ui8 < val2) {
        *result = -1;
    } else if(var1->data.ui8 > val2) {
        *result = 1;
    } else {
        *result = 0;
    }

    return true;
}

static bool variant_compare_uint16(const variant_t* var1, const variant_t* var2, int* result) {
    uint16_t val2;
    if(!variant_toUInt16(&val2, var2)) {
        return false;
    }
    if(var1->data.ui16 < val2) {
        *result = -1;
    } else if(var1->data.ui16 > val2) {
        *result = 1;
    } else {
        *result = 0;
    }

    return true;
}

static bool variant_compare_uint32(const variant_t* var1, const variant_t* var2, int* result) {
    uint32_t val2;
    if(!variant_toUInt32(&val2, var2)) {
        return false;
    }
    if(var1->data.ui32 < val2) {
        *result = -1;
    } else if(var1->data.ui32 > val2) {
        *result = 1;
    } else {
        *result = 0;
    }

    return true;
}

static bool variant_compare_uint64(const variant_t* var1, const variant_t* var2, int* result) {
    uint64_t val2;
    if(!variant_toUInt64(&val2, var2)) {
        return false;
    }
    if(var1->data.ui64 < val2) {
        *result = -1;
    } else if(var1->data.ui64 > val2) {
        *result = 1;
    } else {
        *result = 0;
    }

    return true;
}

static bool variant_compare_bool(const variant_t* var1, const variant_t* var2, int* result) {
    bool val2;
    if(!variant_toBool(&val2, var2)) {
        return false;
    }
    if(var1->data.b == val2) {
        *result = 0;
    } else if(var1->data.b == false) {
        *result = -1;
    } else {
        *result = 1;
    }

    return true;
}

static bool variant_compare_double(const variant_t* var1, const variant_t* var2, int* result) {
    double val2;
    if(!variant_toDouble(&val2, var2)) {
        return false;
    }
    if(var1->data.d < val2) {
        *result = -1;
    } else if(var1->data.d > val2) {
        *result = 1;
    } else {
        *result = 0;
    }

    return true;
}

static bool variant_compare_array(const variant_t* var1, const variant_t* var2, int* result) {
    if(var2->type != variant_type_array) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }
    return variant_list_compare(var1->data.vl, var2->data.vl, result);
}

static bool variant_compare_map(const variant_t* var1, const variant_t* var2, int* result) {
    if(var2->type != variant_type_map) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }
    return variant_map_compare(var1->data.vm, var2->data.vm, result);
}

static bool variant_compare_file_descriptor(const variant_t* var1, const variant_t* var2, int* result) {
    int val2;
    if(!variant_toFd(&val2, var2)) {
        return false;
    }
    if(var1->data.fd < val2) {
        *result = -1;
    } else if(var1->data.fd > val2) {
        *result = 1;
    } else {
        *result = 0;
    }

    return true;
}

/**
   @ingroup pcb_utils_variant
   @brief
   Compare two variants.

   @details
   This function Compares two variants.\n
 *
   First it does a comparison on pointer level:\n
   @li both variants are invalid pointers: result = 0
   @li only first variant is invalid pointer: result = -1
   @li only second variant is invalid pointer: result = 1

   If none of the variant pointers is NULL, the variant data is compared:\n
   Take the variant type of var1 and compare it to the variant type of var2.\n
   @li If they are equal, a direct comparison of both variants is performed, result has the same behavior as strcmp or integers
   @li If they are not equal, var2 will be converted to the var1 variant type.
     - if the conversion is succesfull, both variables are compared, result has the same behavior as strcmp or integers
     - if the conversion is unsuccesfull, an error is returned

   The supported conversions are documented in the variant_to* functions.

   @param var1 variant 1
   @param var2 variant 2
   @param result the result of the comparison

   @return
    - true: the comparison was a succes, result contains the resulting value
    - false: An error has occurred, pcb_error contains the reason why the set failed
        - an invalid result pointer was provided
        - an invalid comparison
 */
bool variant_compare(const variant_t* var1, const variant_t* var2, int* result) {
    if(!result) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(!var1 && !var2) {
        *result = 0;
        return true;
    }
    if(!var1) {
        *result = -1;
        return true;
    }
    if(!var2) {
        *result = 1;
        return true;
    }

    if(variant_type(var1) == variant_type_reference) {
        var1 = variant_da_variant(var1);
    }

    if(variant_type(var2) == variant_type_reference) {
        var2 = variant_da_variant(var2);
    }

    static variant_compare_t compare[] = {
        variant_compare_unknown,
        variant_compare_string,
        variant_compare_int8,
        variant_compare_int16,
        variant_compare_int32,
        variant_compare_int64,
        variant_compare_uint8,
        variant_compare_uint16,
        variant_compare_uint32,
        variant_compare_uint64,
        variant_compare_bool,
        variant_compare_double,
        NULL,                  /* date time */
        variant_compare_array,
        variant_compare_map,
        NULL,                  /* reference - should take the referenced variant */
        variant_compare_array, /* array reference */
        variant_compare_map,   /* map reference */
        variant_compare_file_descriptor,
        NULL,                  /* byte array */
    };

    if(compare[var1->type] == NULL) {
        *result = 0;
        return false;
    }

    return compare[var1->type](var1, var2, result);
}

variant_type_t variant_parseVariantType(const char* type_string) {
    variant_type_t result;

    if((type_string == NULL) || (*type_string == 0)) {
        result = variant_type_unknown;
    } else if(!strcmp(type_string, "string")) {
        result = variant_type_string;
    } else if(!strcmp(type_string, "int8")) {
        result = variant_type_int8;
    } else if(!strcmp(type_string, "int16")) {
        result = variant_type_int16;
    } else if(!strcmp(type_string, "int32")) {
        result = variant_type_int32;
    } else if(!strcmp(type_string, "int64")) {
        result = variant_type_int64;
    } else if(!strcmp(type_string, "uint8")) {
        result = variant_type_uint8;
    } else if(!strcmp(type_string, "uint16")) {
        result = variant_type_uint16;
    } else if(!strcmp(type_string, "uint32")) {
        result = variant_type_uint32;
    } else if(!strcmp(type_string, "uint64")) {
        result = variant_type_uint64;
    } else if(!strcmp(type_string, "bool")) {
        result = variant_type_bool;
    } else if(!strcmp(type_string, "double")) {
        result = variant_type_double;
    } else if(!strcmp(type_string, "array")) {
        result = variant_type_array;
    } else if(!strcmp(type_string, "map")) {
        result = variant_type_map;
    } else if(!strcmp(type_string, "byte_array")) {
        result = variant_type_byte_array;
    } else if(!strcmp(type_string, "reference")) {
        result = variant_type_reference;
    } else if(!strcmp(type_string, "datetime")) {
        result = variant_type_date_time;
    } else {
        result = variant_type_unknown;
    }

    return result;
}

/**
   @}
 */
