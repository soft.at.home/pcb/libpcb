/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#define _GNU_SOURCE
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <stdio.h>
#include <ctype.h>
#include <inttypes.h>
#include <wctype.h>
#include <wchar.h>

#include <debug/sahtrace.h>

#include <pcb/common/error.h>
#include <pcb/utils/variant_list.h>
#include <pcb/utils/variant_map.h>
#include <pcb/utils/string.h>

/**
   @file
   @brief
   Implementation of the variant functions
 */
static uint32_t utf8Length(const char* pos) {
    uint32_t i = 0;
    uint32_t len = 1;
    if((pos[0] & 0xF8) == 0xF0) {
        len = 4;
    }
    if((pos[0] & 0xF0) == 0xE0) {
        len = 3;
    }
    if((pos[0] & 0xE0) == 0xC0) {
        len = 2;
    }
    for(i = 1; i < len; i++) {
        if((pos[i] & 0xC0) != 0x80) {
            SAH_TRACE_WARNING("Invalid UTF-8 encoding detected [%2x][%u/%u]", pos[0], i, len);
            len = i;
            break;
        }
    }
    return len;
}

static void variant_linefeed(FILE* stream, int indent) {
    int i;
    fputs("\n", stream);
    for(i = 0; i < indent; i++) {
        fputs("    ", stream);
    }
}

static void fprintfstr(FILE* stream, const string_t* string) {
    const char* buff = string_buffer(string);
    if(buff) {
        fputs(buff, stream);
    }
}

static const char* variant_print_default_delimiters[] = { "{", ":", ",", "}", "[", ",", "]", NULL };

static bool variant_printPriv(FILE* stream, const variant_t* variant, uint32_t attributes,
                              const char** delimiters, int indent);

static void variant_printPrivQuotedString(FILE* stream, string_t* temp, const variant_t* variant) {
    string_initialize(temp, string_length(&variant->data.str) * 2);
    fputs("\"", stream);
    /* copy string to temp and escape special characters */
    string_copy(temp, &variant->data.str);
    string_replaceChar(temp, "\\", "\\\\");
    /* escape special characters with \uxxxx */
    char* pos = variant->data.str.buffer;
    char needle[2] = {0, 0};
    char escaped[10] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
    uint32_t flags = 0;
    for(; *pos;) {
        if((*pos == '"') && !(flags & 0x01)) {
            string_replaceChar(temp, "\"", "\\\"");
            flags |= 0x01;
            pos++;
            continue;
        }
        if((*pos == '/') && !(flags & 0x02)) {
            string_replaceChar(temp, "/", "\\/");
            flags |= 0x02;
            pos++;
            continue;
        }
        if((*pos == '\t') && !(flags & 0x04)) {
            string_replaceChar(temp, "\t", "\\t");
            flags |= 0x04;
            pos++;
            continue;
        }
        if((*pos == '\r') && !(flags & 0x08)) {
            string_replaceChar(temp, "\r", "\\r");
            flags |= 0x08;
            pos++;
            continue;
        }
        if((*pos == '\n') && !(flags & 0x10)) {
            string_replaceChar(temp, "\n", "\\n");
            flags |= 0x10;
            pos++;
            continue;
        }
        if((*pos == '\f') && !(flags & 0x20)) {
            string_replaceChar(temp, "\f", "\\f");
            flags |= 0x20;
            pos++;
            continue;
        }
        if((*pos == '\b') && !(flags & 0x40)) {
            string_replaceChar(temp, "\b", "\\b");
            flags |= 0x40;
            pos++;
            continue;
        }

        uint32_t length = utf8Length(pos);
        if(length > 1) {
            pos += length;
            continue;
        }
        needle[0] = *pos;
        if(!isprint(*needle)) {
            snprintf(escaped, 9, "\\u00%2.2x", (unsigned char) (*needle));
            string_replaceChar(temp, needle, escaped);
        }
        pos++;
    }
    fprintfstr(stream, temp);
    fputs("\"", stream);
}

static void variant_printPrivList(FILE* stream, const variant_t* variant, uint32_t attributes,
                                  const char** delimiters, int indent) {
    if((attributes & variant_print_multiline) && (attributes & variant_print_need_newline) &&
       !variant_list_isEmpty(variant->data.vl)) {
        variant_linefeed(stream, indent);
    }
    attributes &= ~variant_print_need_newline;
    fputs(delimiters[variant_print_delimiter_list_open], stream);
    if((attributes & variant_print_spaces) && !(attributes & variant_print_multiline)) {
        fputs(" ", stream);
    }
    indent++;
    variant_list_iterator_t* it = NULL;
    variant_list_for_each(it, variant->data.vl) {
        if(attributes & variant_print_multiline) {
            variant_linefeed(stream, indent);
        }
        if(!variant_printPriv(stream, &it->variant, attributes, delimiters, indent)) {
            fputs("\"#ERROR#\"", stream);
        }
        if(variant_list_iterator_next(it)) {
            fputs(delimiters[variant_print_delimiter_list_iterate], stream);
            if((attributes & variant_print_spaces) && !(attributes & variant_print_multiline)) {
                fputs(" ", stream);
            }
        }
    }
    indent--;
    if((attributes & variant_print_multiline) && !variant_list_isEmpty(variant->data.vl)) {
        variant_linefeed(stream, indent);
    }
    if((attributes & variant_print_spaces) && !(attributes & variant_print_multiline)) {
        fputs(" ", stream);
    }
    fputs(delimiters[variant_print_delimiter_list_close], stream);
}

static void variant_printPrivMap(FILE* stream, const variant_t* variant, uint32_t attributes,
                                 const char** delimiters, int indent) {
    if((attributes & variant_print_multiline) && (attributes & variant_print_need_newline) &&
       !variant_map_isEmpty(variant->data.vm)) {
        variant_linefeed(stream, indent);
    }
    attributes |= variant_print_need_newline;
    fputs(delimiters[variant_print_delimiter_map_open], stream);
    if((attributes & variant_print_spaces) && !(attributes & variant_print_multiline)) {
        fputs(" ", stream);
    }
    indent++;
    variant_map_iterator_t* it = NULL;
    int i, n = 0;
    if(attributes & variant_print_outline_colons) {
        variant_map_for_each(it, variant->data.vm) {
            i = strlen(it->key ? it->key : "");
            if(i > n) {
                n = i;
            }
        }
    }
    variant_map_for_each(it, variant->data.vm) {
        if(attributes & variant_print_multiline) {
            variant_linefeed(stream, indent);
        }
        if(attributes & variant_print_quote_keys) {
            fputs("\"", stream);
        }
        fputs(it->key, stream);
        if(attributes & variant_print_quote_keys) {
            fputs("\"", stream);
        }
        if(attributes & variant_print_outline_colons) {
            for(i = n - strlen(it->key ? it->key : ""); i; i--) {
                fputs(" ", stream);
            }
        }
        if(attributes & variant_print_spaces) {
            fputs(" ", stream);
        }
        fputs(delimiters[variant_print_delimiter_map_assign], stream);
        if(attributes & variant_print_spaces) {
            fputs(" ", stream);
        }
        if(!variant_printPriv(stream, &it->variant, attributes, delimiters, indent)) {
            fputs("\"#ERROR#\"", stream);
        }
        if(variant_map_iterator_next(it)) {
            fputs(delimiters[variant_print_delimiter_map_iterate], stream);
            if((attributes & variant_print_spaces) && !(attributes & variant_print_multiline)) {
                fputs(" ", stream);
            }
        }
    }
    indent--;
    if((attributes & variant_print_multiline) && !variant_map_isEmpty(variant->data.vm)) {
        variant_linefeed(stream, indent);
    }
    if((attributes & variant_print_spaces) && !(attributes & variant_print_multiline)) {
        fputs(" ", stream);
    }
    fputs(delimiters[variant_print_delimiter_map_close], stream);
}

static bool variant_printPriv(FILE* stream, const variant_t* variant, uint32_t attributes,
                              const char** delimiters, int indent) {
    if(!variant || !stream) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    string_t temp;
    string_initialize(&temp, 0);
    switch(variant->type) {
    case variant_type_unknown:
        if(attributes & variant_print_quote_strings) {
            fputs("null", stream);
        } else {
            return false;
        }
        break;
    case variant_type_string:
        if(attributes & variant_print_quote_strings) {
            if(string_isNull(&variant->data.str)) {
                fputs("null", stream);
            } else {
                variant_printPrivQuotedString(stream, &temp, variant);
            }
        } else {
            fprintfstr(stream, &variant->data.str);
        }
        break;
    case variant_type_int8:
        fprintf(stream, "%" PRId8, variant->data.i8);
        break;
    case variant_type_int16:
        fprintf(stream, "%" PRId16, variant->data.i16);
        break;
    case variant_type_int32:
        fprintf(stream, "%" PRId32, variant->data.i32);
        break;
    case variant_type_int64:
        fprintf(stream, "%" PRId64, variant->data.i64);
        break;
    case variant_type_uint8:
        fprintf(stream, "%" PRIu8, variant->data.ui8);
        break;
    case variant_type_uint16:
        fprintf(stream, "%" PRIu16, variant->data.ui16);
        break;
    case variant_type_uint32:
        fprintf(stream, "%" PRIu32, variant->data.ui32);
        break;
    case variant_type_uint64:
        fprintf(stream, "%" PRIu64, variant->data.ui64);
        break;
    case variant_type_bool:
        if(attributes & variant_print_bool_to_num) {
            if(variant->data.b) {
                fputs("1", stream);
            } else {
                fputs("0", stream);
            }
        } else {
            if(variant->data.b) {
                fputs("true", stream);
            } else {
                fputs("false", stream);
            }
        }
        break;
    case variant_type_double:
        fprintf(stream, "%e", variant->data.d);
        break;
    case variant_type_date_time:
    {
        pcb_datetime_t tmValue;
        if(variant_toDateTimeExtended(&tmValue, variant)) {
            string_fromTimeExtended(&temp, &tmValue, NULL);
            if(attributes & variant_print_quote_strings) {
                fprintf(stream, "\"%s\"", string_buffer(&temp));
            } else {
                fprintfstr(stream, &temp);
            }
        } else {
            string_cleanup(&temp);
            return false;
        }
    }
    break;
    case variant_type_array_reference:
    case variant_type_array:
        variant_printPrivList(stream, variant, attributes, delimiters, indent);
        break;
    case variant_type_map_reference:
    case variant_type_map:
        variant_printPrivMap(stream, variant, attributes, delimiters, indent);
        break;
    case variant_type_reference:
        variant_printPriv(stream, variant->data.v, attributes, delimiters, indent);
        break;
    case variant_type_file_descriptor:
        fprintf(stream, "%d", variant->data.fd);
        break;
    case variant_type_byte_array:
        pcb_error = pcb_error_invalid_variant_conversion;
        return false;
        break;
    default:
        break;
    }

    string_cleanup(&temp);
    return true;
}

/**
   @ingroup pcb_utils_variant
   @brief
   Create a string variant using the specified character array as initialization data.

   @details
   This function cleans up the existing variant value, (re-)initializes the variant to the string type,
   and initializes the string value with the provided char array.\n

   @param variant the variant we want to set
   @param data the initial value

   @return
    - true: the variant_t is ready to be used
    - false: An error has occurred, pcb_error contains the reason why the set failed
        - an invalid variant was provided
        - an invalid character array was provided
 */
bool variant_setChar(variant_t* variant, const char* data) {
    if(!variant) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }
    variant_cleanup(variant);

    if(variant_initialize(variant, variant_type_string)) {
        string_fromChar(&variant->data.str, data);
        return true;
    }

    return false;
}

/**
   @ingroup pcb_utils_variant
   @brief
   Create a string variant using the specified string as initialization data.

   @details
   This function cleans up the existing variant value, (re-)initializes the variant to the string type,
   and initializes the string value with the provided data string.\n

   @param variant the variant we want to set
   @param data the initial value

   @return
    - true: the variant_t is ready to be used
    - false: An error has occurred, pcb_error contains the reason why the set failed
        - an invalid variant was provided
        - an invalid character array was provided
 */
bool variant_setString(variant_t* variant, const string_t* data) {
    if(!variant) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }
    variant_cleanup(variant);

    if(variant_initialize(variant, variant_type_string)) {
        if(data) {
            string_copy(&variant->data.str, data);
        }
        return true;
    }

    return false;
}

/**
   @ingroup pcb_utils_variant
   @brief
   Create a variant containing a string list

   @details
   This function cleans up the existing variant value, (re-)initializes the variant to the string type
   and initializes the value with the provided string list joined using the provided separator.\n

   @param variant the variant we want to set
   @param list pointer to the string list
   @param separator the separator use to join the string list

   @return
    - true: the variant_t is ready to be used
    - false: An error has occurred, pcb_error contains the reason why the set failed
 */
bool variant_setStringList(variant_t* variant, const string_list_t* list, const char* separator) {
    if(!variant || !list || !separator) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    string_t data;
    string_initialize(&data, 256);
    string_join(&data, list, separator);
    bool rc = variant_setString(variant, &data);
    string_cleanup(&data);
    return rc;
}

/**
   @ingroup pcb_utils_variant
   @brief
   Get the value of the specified variant and copy it into the char array.

   @details
   This function converts the variant value to a string and copies it into the a newly allocated
   char array. The conversion type depends on the variant type. \n
   The char pointer must be freed.
   All variant types can be converted to a string_t.\n

   @param data pointer to a char array
   @param variant the variant of interest

   @return
    - true: the resulting string_t is ready to be used
    - false: An error has occurred, pcb_error contains the reason why the set failed
        - an invalid variant was provided
        - an invalid/un-initialized string_t was provided
        - memory allocation failed
 */
bool variant_toChar(char** data, const variant_t* variant) {
    if(!variant || !data) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    string_t string;
    string_initialize(&string, 0);
    if(!variant_toString(&string, variant)) {
        string_cleanup(&string);
        return false;
    }

    if(string.buffer) {
        *data = string.buffer;
    } else {
        *data = strdup("");
    }
    return true;
}

/**
   @ingroup pcb_utils_variant
   @brief
   Get the value of the specified variant and copy it into the specified string.

   @details
   This function converts the variant value to the string_t type and copies it into the provided
   string_t. The conversion type depends on the variant type. \n

   The string_t data must be initialized before calling this function. The user is responsible for
   cleaning up the resulting string.\n
   All variant types can be converted to a string_t.\n

   @param data the initialized sting_t that will contain the result
   @param variant the variant of interest

   @return
    - true: the resulting string_t is ready to be used
    - false: An error has occurred, pcb_error contains the reason why the set failed
        - an invalid variant was provided
        - an invalid/un-initialized string_t was provided
        - memory allocation failed
 */
bool variant_toString(string_t* data, const variant_t* variant) {
    return variant_print(data, variant, variant_print_bool_to_num, NULL);
}


/**
   @ingroup pcb_utils_variant
   @brief
   Get the value of the specified variant and copy it into the specified string as a JSON string.

   @details
   This function converts the variant value to the string_t type and copies it into the provided
   string_t. The conversion type depends on the variant type. \n

   The string_t data must be initialized before calling this function. The user is responsible for
   cleaning up the resulting string.\n
   All variant types can be converted to a string_t.\n

   @param data the initialized sting_t that will contain the result
   @param variant the variant of interest

   @return
    - true: the resulting string_t is ready to be used
    - false: An error has occurred, pcb_error contains the reason why the set failed
        - an invalid variant was provided
        - an invalid/un-initialized string_t was provided
        - memory allocation failed
 */
bool variant_toJSON(string_t* data, const variant_t* variant) {
    return variant_print(data, variant, variant_print_json, NULL);
}

/**
   @ingroup pcb_utils_variant
   @brief
   Customized conversion of a variant to a string.

   @details
   This function converts the variant value to the string_t type and copies it into the provided
   string_t. The format of the resulting string is customizable through the function arguments
   attributes and delimiters. Internally, the function opens a memory buffer stream and calls variant_fprint.\n

   The string_t data must be initialized before calling this function. The user is responsible for
   cleaning up the resulting string.\n
   All variant types can be converted to a string_t.\n

   @param data the initialized sting_t that will contain the result
   @param variant the variant of interest
   @param attributes Attributes that affect the format of the resulting string. A bitwise OR of
                  zero or more of the following:
                  - @ref variant_print_multiline
                  - @ref variant_print_spaces
                  - @ref variant_print_outline_colons
                  - @ref variant_print_quote_keys
                  - @ref variant_print_quote_strings
                  - @ref variant_print_bool_to_num
                  - @ref variant_print_need_newline
                  - @ref variant_print_json
                  - @ref variant_print_beautify
   @param delimiters Array of strings that specify the delimiters to be used for maps and lists.
                  The exact order in which delimiter types should appear in the array,
                  is defined by the enumerator @ref variant_print_delimiter_t.\n
                  If a NULL pointer is provided, default delimiters are used.\n
                  Remark: if this argument is provided, it MUST be a NULL-terminated array.
   @return
    - true: the resulting string_t is ready to be used
    - false: An error has occurred, pcb_error contains the reason why the set failed
        - an invalid variant was provided
        - an invalid/un-initialized string_t was provided
        - memory allocation failed
 */
bool variant_print(string_t* data, const variant_t* variant, uint32_t attributes, const char** delimiters) {
    bool ret = false;
    if(!data) {
        pcb_error = pcb_error_invalid_parameter;
        return ret;
    }

    char* str = NULL;
    size_t len = 0;

    FILE* stream = open_memstream(&str, &len);
    if(stream) {
        ret = variant_fprint(stream, variant, attributes, delimiters);
        fclose(stream);
    }

    if(ret) {
        size_t bufferLength = str ? len + 1 : 0;
        string_attachBuffer(data, str, bufferLength);
    } else {
        string_clear(data);
        free(str);
        pcb_error = pcb_error_no_memory;
    }

    return ret;
}

/**
   @ingroup pcb_utils_variant
   @brief
   Write a variant to a FILE stream.

   @details
   This function write the variant value into a FILE stream.
   The string format is customizable through the function arguments attributes and delimiters.\n

   The user is responsible for cleaning up the resulting string.\n
   All variant types can be written to a stream.\n

   @param stream the FILE stream that will contain the result
   @param variant the variant of interest
   @param attributes Attributes that affect the format of the resulting string. A bitwise OR of
                  zero or more of the following:
                  - @ref variant_print_multiline
                  - @ref variant_print_spaces
                  - @ref variant_print_outline_colons
                  - @ref variant_print_quote_keys
                  - @ref variant_print_quote_strings
                  - @ref variant_print_bool_to_num
                  - @ref variant_print_need_newline
                  - @ref variant_print_json
                  - @ref variant_print_beautify
   @param delimiters Array of strings that specify the delimiters to be used for maps and lists.
                  The exact order in which delimiter types should appear in the array,
                  is defined by the enumerator @ref variant_print_delimiter_t.\n
                  If a NULL pointer is provided, default delimiters are used.\n
                  Remark: if this argument is provided, it MUST be a NULL-terminated array.
   @return
    - true: the variant was succesfully written into stream
    - false: An error has occurred, pcb_error contains the reason why the set failed
        - an invalid variant was provided
        - an invalid/un-initialized string_t was provided
        - memory allocation failed
 */
bool variant_fprint(FILE* stream, const variant_t* variant, uint32_t attributes, const char** delimiters) {
    if(delimiters) {
        const char** delimiter = delimiters;
        int i;
        for(i = 0; i < variant_print_delimiter_count && *delimiter; i++) {
            delimiter++;
        }
        if(i < variant_print_delimiter_count) {
            pcb_error = pcb_error_invalid_value;
            return false;
        }
    } else {
        delimiters = variant_print_default_delimiters;
    }
    return variant_printPriv(stream, variant, attributes, delimiters, 0);
}

/**
   @ingroup pcb_utils_variant
   @brief
   Get a string pointer directly to the string data of a string variant.

   @details
   This function returns a string pointer directly to the string data contained within the variant.\n
   The resulting string may only be used to READ, NOT WRITE. This function only works when
   the variant type is a string.\n
   The returned pointer MUST not be freed.\n

   @param variant the variant of interest

   @return
   The resulting string pointer, NULL if the variant is invalid or not of the string type.
 */
const string_t* variant_da_string(const variant_t* variant) {
    if(!variant) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    if(variant->type == variant_type_reference) {
        variant = variant->data.v;
    }

    if(variant->type != variant_type_string) {
        pcb_error = pcb_error_invalid_variant_type;
        return NULL;
    }

    return &variant->data.str;
}

/**
   @ingroup pcb_utils_variant
   @brief
   Converts a variant to a string list.

   @details
   This function converts the variant value to a string list.\n
   The conversion depends on the type of the variant given.\n
   Simple types are added to the string list.\n
   String is split using the separator.\n
   If the given variant is a variant list or a variant map, all elements of the given
   list or map are added the to the string list.

   The string list provided must be initialized.\n

   @param data the initialized string list that will contain the result
   @param variant the variant of interest
   @param separator the separator to use if the variant type is a string

   @return
    - true: the resulting string list is ready to be used
    - false: An error has occurred, pcb_error contains the reason why the set failed
        - an invalid variant was provided
        - an invalid variant list was provided
        - memory allocation failed
 */
bool variant_toStringList(string_list_t* data, const variant_t* variant, const char* separator) {
    if(!variant || !data) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    string_list_clear(data);

    string_t str;
    string_initialize(&str, 64);

    switch(variant->type) {
    case variant_type_string:
        variant_toString(&str, variant);
        string_list_split(data, &str, separator, strlist_skip_empty_parts, string_case_sensitive);
        break;
    case variant_type_int8:
    case variant_type_int16:
    case variant_type_int32:
    case variant_type_int64:
    case variant_type_uint8:
    case variant_type_uint16:
    case variant_type_uint32:
    case variant_type_uint64:
    case variant_type_bool:
    case variant_type_double:
    case variant_type_date_time:
    case variant_type_file_descriptor:
    case variant_type_byte_array:
        variant_toString(&str, variant);
        string_list_append(data, string_list_iterator_create(&str));
        break;
    case variant_type_array_reference:
    case variant_type_array: {
        variant_list_iterator_t* it = NULL;
        variant_list_for_each(it, variant->data.vl) {
            variant_toString(&str, &it->variant);
            string_list_append(data, string_list_iterator_create(&str));
        }
    }
    break;
    case variant_type_map_reference:
    case variant_type_map: {
        variant_map_iterator_t* it = NULL;
        variant_map_for_each(it, variant->data.vm) {
            variant_toString(&str, &it->variant);
            string_list_append(data, string_list_iterator_create(&str));
        }
    }
    break;
    case variant_type_reference:
        return variant_toStringList(data, variant->data.v, separator);
        break;
    default:
        break;
    }

    string_cleanup(&str);

    return true;
}

/**
   @ingroup pcb_utils_variant
   @brief
   Converts a variant to a string.

   @details
   This function is provided for convience and is doing the same as @ref variant_toString.
   The returned pointer must be freed.\n

   @param variant the variant of interest

   @return
    - The converted value
 */
string_t* variant_string(const variant_t* variant) {
    if(!variant) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    string_t* data = (string_t*) calloc(1, sizeof(string_t));
    if(!data) {
        return NULL;
    }

    if(!variant_toString(data, variant)) {
        string_cleanup(data);
        free(data);
        return NULL;
    }

    return data;
}

/**
   @ingroup pcb_utils_variant
   @brief
   Converts a variant to a string list.

   @details
   This function is provided for convience and is doing the same as @ref variant_toStringList.
   The returned pointer must be freed.\n

   @param variant the variant of interest
   @param separator separator string to insert in the string

   @return
    - The converted value
 */
string_list_t* variant_stringList(const variant_t* variant, const char* separator) {
    if(!variant) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    string_list_t* data = (string_list_t*) calloc(1, sizeof(string_list_t));
    if(!data) {
        return NULL;
    }

    if(!string_list_initialize(data)) {
        free(data);
        return NULL;
    }

    if(!variant_toStringList(data, variant, separator)) {
        string_list_cleanup(data);
        free(data);
        return NULL;
    }

    return data;
}
