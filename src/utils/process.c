/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

/* -*- c-basic-offset: 4 -*- */
#define _GNU_SOURCE
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <syscall.h>
#include <sys/ioctl.h>
#include <sys/wait.h>
#include <sys/prctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/resource.h>

#include <debug/sahtrace.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <pcb/utils/peer.h>
#include <pcb/utils/process.h>

enum {
    PIPE_PARENT_END = 0,
    PIPE_CHILD_END = 1
};

/**
 * We support one toolchain (namely vx160-linux-uclibc-gcc3.4.3-uclibc0.9.28)
 * which declares waitid() in its headers, but doesn't actually implement it in
 * libc.so.  It can't be updated, so we have to work around that issue.
 *
 * Luckily all supported kernel version have waitid().
 */
int waitid(idtype_t idtype, id_t id, siginfo_t* info, int options) {
    return syscall(SYS_waitid, idtype, id, info, options, NULL);
}

process_info_t* process_create(connection_info_t* con) {
    if(!con) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    process_info_t* process = (process_info_t*) calloc(1, sizeof(process_info_t));
    if(!process) {
        pcb_error = pcb_error_no_memory;
        return NULL;
    }

    process->con = con;

    for(int i = 0; i < 3; i++) {
        process->fd[i][0] = -1;
        process->fd[i][1] = -1;
    }
    return process;
}

bool process_destroy(process_info_t* process) {
    if(!process || process->running) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }
    for(int i = 0; i < 3; i++) {
        if(process->fd[i][PIPE_PARENT_END] >= 0) {
            close(process->fd[i][PIPE_PARENT_END]);
        }
        if(process->fd[i][PIPE_CHILD_END] >= 0) {
            close(process->fd[i][PIPE_CHILD_END]);
        }
    }

    llist_iterator_take(&process->it);

    free(process);
    return true;
}

int process_getFd(process_info_t* process, int requested) {
    if(!process) {
        pcb_error = pcb_error_invalid_parameter;
        errno = 0;
        return -1;
    }

    if(process_isRunning(process)) {
        SAH_TRACEZ_WARNING("pcb", "process_getFd might not work when process is started");
    }

    if((requested < STDIN_FILENO) || (requested > STDERR_FILENO)) {
        pcb_error = pcb_error_invalid_parameter;
        return -1;
    }

    if(process->fd[requested][PIPE_PARENT_END] < 0) {
        if(pipe2(process->fd[requested], O_NONBLOCK | O_CLOEXEC) < 0) {
            return -1;
        }
        if(requested == STDIN_FILENO) {
            /* the parent end is the write end */
            int swap = process->fd[STDIN_FILENO][0];
            process->fd[STDIN_FILENO][0] = process->fd[STDIN_FILENO][1];
            process->fd[STDIN_FILENO][1] = swap;
        }
    }
    return process->fd[requested][PIPE_PARENT_END];
}

int process_closeFd(process_info_t* process, int requested) {
    if(!process || process->running) {
        pcb_error = pcb_error_invalid_parameter;
        return -1;
    }

    if((requested < STDIN_FILENO) || (requested > STDERR_FILENO)) {
        pcb_error = pcb_error_invalid_parameter;
        return -1;
    }

    close(process->fd[requested][0]);
    close(process->fd[requested][1]);
    process->fd[requested][0] = -1;
    process->fd[requested][1] = -1;

    return 0;
}

static void process_closeFDs() {
    struct rlimit limit;
    int max_files = 1024;

    int ret = getrlimit(RLIMIT_NOFILE, &limit);
    if(ret == 0) {
        max_files = limit.rlim_cur;
    }

    for(int i = 3; i < max_files; i++) {
        close(i);
    }
}

/* NOTE: First argument must be the command to execute */
bool process_vstart(process_info_t* process, char** argv) {
    if(!process || !argv || process->running) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(!connection_addRunningProcess(process->con, process)) {
        pcb_error = pcb_error_system_error;
        return false;
    }

    pid_t pid = fork();
    if(pid > 0) {
        process->pid = pid;
        process->running = true;
        return true;
    } else if(pid == -1) {
        pcb_error = pcb_error_system_error;
        return false;
    }
    /* Ensure the child process doesn't survive the parent. */
    prctl(PR_SET_PDEATHSIG, SIGKILL);

    int fd_null = open("/dev/null", O_RDWR);
    /* dup pipes dev/null to stdin/out/error */
    for(int i = 0; i < 3; i++) {
        if(process->fd[i][PIPE_CHILD_END] >= 0) {
            if(i != process->fd[i][PIPE_CHILD_END]) {
                close(i);
                dup2(process->fd[i][PIPE_CHILD_END], i);
            }
        } else {
            dup2(fd_null, i);
        }
    }

    /* Close all other open file descriptors. . */
    process_closeFDs();

    /* Unblock all signals */
    sigset_t sig_set;
    pthread_sigmask(SIG_SETMASK, NULL, &sig_set);
    pthread_sigmask(SIG_UNBLOCK, &sig_set, NULL);

    /* Preload pcb_preload.so. This ensures that stdout/stderr are line
     * buffered so that we get any output without incurring significant
     * buffering delays. */
    putenv("LD_PRELOAD=/lib/libpcb_preload.so");

    execvp(argv[0], argv);

    /* We failed to start the process. We need to let out caller know, but
     * we've already forked.  Just exit with a failure so our parent gets
     * SIGCHLD.
     */
    exit(EXIT_FAILURE);
}

/* NOTE: Last argument must be NULL */
bool process_start(process_info_t* process, char* cmd, ...) {
    if(!process || !cmd || process->running) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    va_list ap;
    int argc = 0;

    /* Count the number of arguments */
    va_start(ap, cmd);
    while(va_arg(ap, char*)) {
        argc++;
    }
    va_end(ap);

    char** argv = calloc(argc + 2, sizeof(char*));
    if(!argv) {
        pcb_error = pcb_error_no_memory;
        return false;
    }

    va_start(ap, cmd);
    /* By convention, the first argument is always the executable name */
    argv[0] = cmd;
    for(int i = 1; i <= argc; i++) {
        argv[i] = va_arg(ap, char*);
    }
    va_end(ap);

    bool retval = process_vstart(process, argv);
    free(argv);
    return retval;
}

bool process_kill(process_info_t* process, int sig) {
    if(!process || !process->running) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    int ret = kill(process->pid, sig);
    if(ret < 0) {
        SAH_TRACEZ_ERROR("pcb", "Failed to send signal to PID %d: %s",
                         process->pid, strerror(errno));
        return false;
    }

    return true;
}

static bool process_block_sigchld_if_needed(sigset_t* oldset) {
    sigset_t sigchld_set;
    int ret;

    ret = sigemptyset(&sigchld_set);
    if(ret < 0) {
        return false;
    }
    ret = sigaddset(&sigchld_set, SIGCHLD);
    if(ret < 0) {
        return false;
    }

    ret = pthread_sigmask(SIG_BLOCK, &sigchld_set, oldset);
    if(ret < 0) {
        return false;
    }
    return true;
}

static bool process_unblock_sigchld_if_needed(const sigset_t* oldset) {
    int ret;

    ret = pthread_sigmask(SIG_SETMASK, oldset, NULL);
    if(ret < 0) {
        return false;
    }
    return true;
}

// This function MUST have the sigchild blocked before being executed.
// You must call process_handle_sigchld if this call was succesfull.
// @return true if the process stopped, false if timeout or error
static bool process_wait_timeout_internal(process_info_t* process, uint32_t timeout) {
    sigset_t sigset;
    siginfo_t info;
    struct timespec ts_timeout, ts_tmp;
    struct timeval tv_timeout, tv_start, tv_now, tv_diff;
    int ret;

    sigemptyset(&sigset);
    sigaddset(&sigset, SIGCHLD);

    ret = clock_gettime(CLOCK_MONOTONIC, &ts_tmp);
    if(ret < 0) {
        pcb_error = pcb_error_system_error;
        return false;
    }
    TIMESPEC_TO_TIMEVAL(&tv_start, &ts_tmp);

wait:
    /* Calculate the remaining wait time
     * Note CLOCK_MONOTONIC, because we care about intervals, not the correct
     * time of day. This ensures we don't have problems if the clock is changes
     * (e.g. NTP)
     */
    ret = clock_gettime(CLOCK_MONOTONIC, &ts_tmp);
    if(ret < 0) {
        pcb_error = pcb_error_system_error;
        return false;
    }
    TIMESPEC_TO_TIMEVAL(&tv_now, &ts_tmp);

    tv_timeout.tv_sec = timeout / 1000;
    tv_timeout.tv_usec = (timeout % 1000) * 1000;

    timersub(&tv_now, &tv_start, &tv_diff);
    // *INDENT-OFF*
    if(timercmp(&tv_diff, &tv_timeout, >=)) {
        pcb_error = pcb_error_canceled;
        return false;
    }
    // *INDENT-ON*

    timersub(&tv_timeout, &tv_diff, &tv_timeout);

    TIMEVAL_TO_TIMESPEC(&tv_timeout, &ts_timeout);

    ret = sigtimedwait(&sigset, &info, &ts_timeout);
    if(ret != SIGCHLD) {
        goto wait;
    }

    /* Set again SIGCHLD signal before waitid().
     * This is needed so that :
     * A) if there were multiple SIGCHLD event merged, SIGCHLD will still
     * be set after this function (needed for main pcb loop).
     * B) if there were no other SIGCHLD event, it will be cleaned up after waitid().
     * see man 3p wait for the details.
     */
    ret = raise(SIGCHLD);
    if(ret) {
        SAH_TRACEZ_ERROR("pcb", "cannot raise SIGCHLD: %s (%d)", strerror(errno), errno);
        pcb_error = pcb_error_system_error;
        return false;
    }

    /* Check that this is really the process we're interested in. */
    memset(&info, 0, sizeof(info));
    ret = waitid(P_PID, process->pid, &info, WEXITED | WNOWAIT | WNOHANG);
    if(ret < 0) {
        SAH_TRACEZ_ERROR("pcb", "waitid(P_PID, %d, NULL, WEXITED | WNOWAIT | WNOHANG) failed: %s (%d)",
                         process->pid, strerror(errno), errno);
        pcb_error = pcb_error_system_error;
        return false;
    }

    if(info.si_pid == 0) {
        goto wait;
    }

    return true;
}

static bool process_kill_wait_timeout_internal(process_info_t* process, bool sendsig, int sig, uint32_t timeout) {
    sigset_t orig_sigset;
    bool ret;

    ret = process_block_sigchld_if_needed(&orig_sigset);
    if(!ret) {
        SAH_TRACEZ_ERROR("pcb", "cannot block SIGCHLD: %s (%d)", strerror(errno), errno);
        pcb_error = pcb_error_system_error;
        return false;
    }

    if(sendsig) {
        ret = process_kill(process, sig);
        if(!ret) {
            if(!process_unblock_sigchld_if_needed(&orig_sigset)) {
                SAH_TRACEZ_ERROR("pcb", "cannot unblock SIGCHLD: %s (%d)", strerror(errno), errno);
                pcb_error = pcb_error_system_error;
                return false;
            }
            return false;
        }
    }

    ret = process_wait_timeout_internal(process, timeout);

    if(!process_unblock_sigchld_if_needed(&orig_sigset)) {
        SAH_TRACEZ_ERROR("pcb", "cannot unblock SIGCHLD: %s (%d)", strerror(errno), errno);
        pcb_error = pcb_error_system_error;
        return false;
    }

    if(ret) {
        //process did stop, we can continue processing its state
        return process_handle_sigchld(process);
    } else {
        return false;
    }
}

bool process_kill_wait_timeout(process_info_t* process, int sig, uint32_t timeout) {
    return process_kill_wait_timeout_internal(process, true, sig, timeout);
}

bool process_wait_timeout(process_info_t* process, uint32_t timeout) {
    return process_kill_wait_timeout_internal(process, false, 0, timeout);
}

bool process_wait(process_info_t* process) {
    if(!process) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    /* Block, but don't remove the process exit information.
     * That'll be done by process_handle_sigchld_internal.
     */
    siginfo_t dummy;
    int ret = waitid(P_PID, process->pid, &dummy, WEXITED | WNOWAIT);
    if(ret < 0) {
        SAH_TRACEZ_ERROR("pcb", "waitid(P_PID, %d, NULL, WEXITED | WNOWAIT) failed: %s (%d)",
                         process->pid, strerror(errno), errno);
        pcb_error = pcb_error_system_error;
        return false;
    }

    return process_handle_sigchld(process);
}

bool process_setUserData(process_info_t* process, void* data) {
    if(!process) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    process->data = data;

    return true;
}

void* process_getUserData(process_info_t* process) {
    if(!process) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    return process->data;
}

bool process_setExitHandler(process_info_t* process, process_event_handler_t handler) {
    if(!process) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    process->exit = handler;

    return true;
}

pid_t process_getPid(process_info_t* process) {
    if(!process) {
        pcb_error = pcb_error_invalid_parameter;
        return 0;
    }

    return process->pid;
}

bool process_isRunning(process_info_t* process) {
    if(!process) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    return process->running;
}

bool process_handle_sigchld(process_info_t* process) {
    if(!process || !process->running) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    int status;
    int ret = waitpid(process->pid, &status, WNOHANG);

    if(ret < 0) {
        pcb_error = pcb_error_system_error;
        return false;
    }

    if(ret == 0) {
        /* This process is not the one which generated SIGCHLD */
        return true;
    }

    return process_handle_sigchld_status(process, status);
}

bool process_handle_sigchld_status(process_info_t* process, int status) {
    if(!process || !process->running) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    /* child process may have exited before we actually read
     * all the datas it sent. ensure that we read it all.
     * We should check stdout and stderr. */
    int type = STDOUT_FILENO;
    while(type <= STDERR_FILENO) {
        int fd = process->fd[type][PIPE_PARENT_END];
        int len = 0;
        if((fd >= 0) && (ioctl(fd, FIONREAD, &len) >= 0) && (len > 0)) {
            peer_info_t* peer = connection_find(process->con, fd);
            if(peer) {
                peer_handleRead(peer);
            }
        }
        ++type;
    }

    /* The process is no longer running */
    process->running = false;
    connection_removeRunningProcess(process->con, process);

    process_exit_info_t exit_info;
    memset(&exit_info, 0, sizeof(process_exit_info_t));

    if(WIFEXITED(status)) {
        exit_info.exit_code = WEXITSTATUS(status);
    } else if(WIFSIGNALED(status)) {
#ifdef WCOREDUMP
        if(WCOREDUMP(status)) {
            exit_info.core_dump = true;
        }
#endif
        exit_info.signalled = true;
        exit_info.signal = WTERMSIG(status);
    } else {
        SAH_TRACEZ_ERROR("pcb", "Illegal process status %d for pid %d", status, process->pid);
        pcb_error = pcb_error_system_error;
        return false;
    }

    if(process->exit) {
        process->exit(process, &exit_info);
    }

    return true;
}
