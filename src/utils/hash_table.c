/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <pcb/common/error.h>
#include <pcb/utils/linked_list.h>
#include <pcb/utils/hash_table.h>

struct _hash_table {
    size_t size;
    hash_table_bucket_t* buckets;
    hash_table_destructor_t destructor;
};

struct _hash_table_bucket {
    llist_t entries;
};

struct _hash_table_entry {
    llist_iterator_t it;
    hash_table_t* table;
    char* key;
    void* value;
};


/**
   @ingroup pcb_utils_hash_table
   @brief
   Hash the key with djb2 algorithm
 */
static uint32_t hash_key(hash_table_t* table, const char* key) {
    uint32_t hash = 5381;
    int c;

    while((c = *key++)) {
        hash = ((hash << 5) + hash) + c; /* hash * 33 + c */
    }

    return hash % table->size;
}

static hash_table_entry_t* hash_table_bucket_find(hash_table_bucket_t* bucket, const char* key) {
    llist_iterator_t* it = NULL;

    llist_for_each(it, &bucket->entries) {
        hash_table_entry_t* entry = llist_item_data(it, hash_table_entry_t, it);
        if(!strcmp(entry->key, key)) {
            return entry;
        }
    }

    return NULL;
}

static hash_table_entry_t* hash_table_bucket_add(hash_table_t* table, hash_table_bucket_t* bucket, const char* key) {
    hash_table_entry_t* entry = calloc(1, sizeof(hash_table_entry_t));
    if(!entry) {
        return NULL;
    }

    entry->table = table;
    entry->key = strdup(key);
    if(!entry->key) {
        free(entry);
        return NULL;
    }

    llist_append(&bucket->entries, &entry->it);

    return entry;
}

static void hash_table_bucket_cleanup(hash_table_bucket_t* bucket) {
    llist_iterator_t* it = NULL;

    while((it = llist_first(&bucket->entries))) {
        hash_table_entry_t* entry = llist_item_data(it, hash_table_entry_t, it);
        hash_table_entry_destroy(entry);
    }
}

hash_table_t* hash_table_create(uint32_t size) {
    hash_table_t* table = NULL;

    if(!size) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    table = calloc(1, sizeof(hash_table_t));
    if(!table) {
        pcb_error = pcb_error_no_memory;
        return NULL;
    }

    table->buckets = calloc(size, sizeof(hash_table_bucket_t));
    if(!table->buckets) {
        free(table);
        pcb_error = pcb_error_no_memory;
        return NULL;
    }
    table->size = size;

    return table;
}

void hash_table_destroy(hash_table_t* table) {
    uint32_t i = 0;
    if(!table) {
        return;
    }

    for(i = 0; i < table->size; i++) {
        hash_table_bucket_t* bucket = &table->buckets[i];
        hash_table_bucket_cleanup(bucket);
    }

    free(table->buckets);
    free(table);
}

bool hash_table_setDestructor(hash_table_t* table, hash_table_destructor_t destructor) {
    if(!table) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    table->destructor = destructor;

    return true;
}

hash_table_entry_t* hash_table_find(hash_table_t* table, const char* key) {
    if(!table || !key) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    uint32_t index = hash_key(table, key);
    hash_table_bucket_t* bucket = &table->buckets[index];

    hash_table_entry_t* entry = hash_table_bucket_find(bucket, key);
    if(!entry) {
        pcb_error = pcb_error_not_found;
        return NULL;
    }

    return entry;
}

hash_table_entry_t* hash_table_find_or_add(hash_table_t* table, const char* key) {
    if(!table || !key) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    uint32_t index = hash_key(table, key);
    hash_table_bucket_t* bucket = &table->buckets[index];
    hash_table_entry_t* entry = hash_table_bucket_find(bucket, key);

    if(!entry) {
        entry = hash_table_bucket_add(table, bucket, key);
        if(!entry) {
            pcb_error = pcb_error_no_memory;
            return NULL;
        }
    }

    return entry;
}

void* hash_table_entry_getValue(hash_table_entry_t* entry) {
    return entry ? entry->value : NULL;
}

bool hash_table_entry_setValue(hash_table_entry_t* entry, void* value) {
    if(!entry) {
        return false;
    }

    if(value != entry->value) {
        hash_table_destructor_t destructor = entry->table->destructor;
        if(destructor && entry->value) {
            destructor(entry->key, entry->value);
        }
        entry->value = value;
    }
    return true;
}

bool hash_table_entry_destroy(hash_table_entry_t* entry) {
    if(!entry || !entry->table) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    hash_table_entry_setValue(entry, NULL);
    llist_iterator_take(&entry->it);
    free(entry->key);
    free(entry);
    return true;
}

