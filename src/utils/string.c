/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#define _GNU_SOURCE
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <stdarg.h>
#include <inttypes.h>

#include <debug/sahtrace.h>

#include <pcb/common/error.h>
#include <pcb/utils/string.h>


/**
   @file
   @brief
   Implementation of the public string functions
 */

typedef char* (* find_fn) (const char* s1, const char* s2);
typedef int (* compare_fn) (const char* s1, const char* s2);

/**
   @ingroup pcb_utils_string
   @brief
   Initialize a string.

   @details
   This function <b>initializes</b> a <b>string</b>. \n
   It is mandatory to initialize a string (string_t) before using it. \n
   Every @ref string_initialize() call should have a corresponding @ref string_cleanup() call to free the memory.\n
   This function call allocates a buffer of a size specified with buffersize. If the specified buffersize is 0
   the string structure is initialized to a NULL string\n

   @param string the string to be initialized
   @param bufferSize the initial buffersize

   @return
    - true: Initialization was succesfull, the string_t is ready to be used
    - false: An error has occurred
        - memory allocation failed
 */
bool string_initialize(string_t* string, size_t bufferSize) {
    if(!string) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(bufferSize) {
        string->buffer = calloc(1, bufferSize);
        if(!string->buffer) {
            pcb_error = pcb_error_no_memory;
            string->bufferSize = 0;
            return false;
        }
        string->bufferSize = bufferSize;
    } else {
        string->buffer = NULL;
        string->bufferSize = 0;
    }

    return true;
}

/**
   @ingroup pcb_utils_string
   @brief
   Attach a buffer to a string.

   @details
   Cleanup the old string buffer and attach a new null-terminated buffer to the string.\n
   The ownership of the new buffer is transmitted to the string. Do not use this pointer.

   @param string the string to attach the buffer
   @param buffer a memory buffer with a null-terminated string.  May be NULL iff size is zero.
              Note: the new buffer must not overlap the current buffer.
   @param bufferSize the allocated buffer size (with malloc)

   @return
    - true: Buffer is attached to the string and its ownership was moved.
    - false: An error has occurred
        - string was NULL
 */

bool string_attachBuffer(string_t* string, char* buffer, size_t bufferSize) {
    if(!string) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if((buffer == NULL) != (bufferSize == 0)) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    string_cleanup(string);

    string->buffer = buffer;
    string->bufferSize = bufferSize;

    return true;
}

/**
   @ingroup pcb_utils_string
   @brief
   Cleanup a string.

   @details
   This function <b>frees</b> the buffer alocated for containing the string\n
   After calling this function, the string structure contains a null string\n

   This function frees the alocated string buffer.\n

   @param string the string that has to be cleaned up
 */
void string_cleanup(string_t* string) {
    if(!string_isNull(string)) {
        free(string->buffer);
        string->buffer = NULL;
        string->bufferSize = 0;
    }
}

/**
   @ingroup pcb_utils_string
   @brief
   Clear a string.

   @details
   This function <b>clears</b> the string buffer.\n
   After calling this function, the string structure contains an empty string, the buffer is not re-allocated\n

   @param string the string that has to be cleared
 */
void string_clear(string_t* string) {
    if(string_isNull(string)) {
        return;
    }

    strncpy(string->buffer, "", string->bufferSize);
}

const char* string_buffer(const string_t* string) {
    return (!string_isNull(string)) ? string->buffer : NULL;
}

/**
   @ingroup pcb_utils_string
   @brief
   Returns a not-NULL pointer to a null-terminated string.

   @details
   This function returns a pointer different from NULL to a null-terminated string with the
   same text as the text represented by the given string_t.\n
   Do not store this pointer for later use.\n
   The returned pointer must not be freed.

   @param string the string_t for which a pointer to a string is returned

   @return
    pointer to a null-terminated string. Guaranteed not a NULL pointer.
    In case the given string_t is a NULL pointer, or the given string_t's buffer
    is NULL, returns "" (an empty string, i.e. a pointer to '\0').
 */
const char* string_safeBuffer(const string_t* string) {
    return (!string_isNull(string)) ? string->buffer : "";
}

/**
   @ingroup pcb_utils_string
   @brief
   Convert the string to upper case

   @details
   Converts the complete string into upper case\n

   @param string the string that to be set in upper case
 */
void string_toUpper(string_t* string) {
    if(string_isNull(string)) {
        return;
    }

    size_t index = 0;
    size_t size = strlen(string->buffer);
    char diff = 'A' - 'a';
    for(index = 0; index < size; index++) {
        if((string->buffer[index] >= 'a') && (string->buffer[index] <= 'z')) {
            string->buffer[index] += diff;
        }
    }
}

/**
   @ingroup pcb_utils_string
   @brief
   Convert the string to lower case

   @details
   Converts the complete string into lower case\n

   @param string the string that to be set in lower case
 */
void string_toLower(string_t* string) {
    if(string_isNull(string)) {
        return;
    }

    size_t index = 0;
    size_t size = strlen(string->buffer);
    char diff = 'a' - 'A';
    for(index = 0; index < size; index++) {
        if((string->buffer[index] >= 'A') && (string->buffer[index] <= 'Z')) {
            string->buffer[index] += diff;
        }
    }
}

/**
   @ingroup pcb_utils_string
   @brief
   Appends a const char to the string buffer

   @details
   Appends a const char to the string buffer. If the string buffer is to small, the buffer is re-allocated\n

   @param dest the string to which another string will be attached
   @param string the string that will be attached

   @return
    - the new string length
 */
size_t string_appendChar(string_t* dest, const char* string) {
    if(!dest) {
        pcb_error = pcb_error_invalid_parameter;
        return 0;
    }

    if(!string || !(*string)) {
        return dest->buffer ? strlen(dest->buffer) : 0;
    }

    size_t sizeNeeded = strlen(string) + 1;
    if(!string_isNull(dest)) {
        sizeNeeded += strlen(dest->buffer);
    }

    if(dest->bufferSize < sizeNeeded) {
        char* newBuffer = NULL;
        if(!string_isNull(dest)) {
            newBuffer = realloc(dest->buffer, sizeNeeded * 2);
        } else {
            newBuffer = calloc(1, sizeNeeded * 2);
        }
        if(!newBuffer) {
            pcb_error = pcb_error_no_memory;
            return string_length(dest);
        }
        dest->buffer = newBuffer;
        dest->bufferSize = sizeNeeded * 2;
    }
    strncat(dest->buffer, string, dest->bufferSize);

    return sizeNeeded - 1;

}

/**
   @ingroup pcb_utils_string
   @brief
   Appends a string to a string

   @details
   This function is provided for convenience, and is doing exactly the same as @ref string_appendChar

   @param dest the string to which another string will be attached
   @param string the string that will be attached

   @return
    - the new string length
 */
size_t string_append(string_t* dest, const string_t* string) {
    if(string_isNull(string)) {
        return string_length(dest);
    }

    return string_appendChar(dest, string->buffer);
}

/**
   @ingroup pcb_utils_string
   @brief
   Appends a formatted string to the end of the sring

   @details
   Appends a formatted string to the end of the sring

   @param dest the string to which another string will be attached
   @param format the format string

   @return
    - the new string length
 */
size_t string_appendFormat(string_t* dest, const char* format, ...) {
    if(!dest) {
        pcb_error = pcb_error_invalid_parameter;
        return 0;
    }

    if(!format) {
        return string_length(dest);
    }

    /* try to write and see if we are writing too much */
    va_list arg;
    ssize_t len = 0, left = 0, writen = 0;

    if(!dest->buffer) {
        va_start(arg, format);
        writen = vasprintf(&dest->buffer, format, arg);
        va_end(arg);
        dest->bufferSize = writen + 1;
        return writen;
    }

    len = strlen(dest->buffer);
    left = dest->bufferSize - len - 1;

    va_start(arg, format);
    writen = vsnprintf(dest->buffer + len, left, format, arg);
    va_end(arg);
    if(writen >= left) {
        /* leave some room for the next append */
        size_t sizeNeeded = len + (len > writen ? len * 2 : writen * 2);
        char* newBuffer = realloc(dest->buffer, sizeNeeded);
        if(!newBuffer) {
            pcb_error = pcb_error_no_memory;
            dest->buffer[len] = '\0';
            return string_length(dest);
        }
        dest->buffer = newBuffer;
        dest->bufferSize = sizeNeeded;
        left = dest->bufferSize - len - 1;

        /* this should not fail any more */
        va_start(arg, format);
        vsnprintf(dest->buffer + len, left, format, arg);
        va_end(arg);
    }

    return len + writen;
}

/**
   @ingroup pcb_utils_string
   @brief
   Prepends a const char to the string buffer

   @details
   Prepends a const char to the string buffer. If the string buffer is to small, the buffer is re-allocated\n

   @param dest the string to which another string will be attached
   @param string the string that will be attached

   @return
    - the new string length
 */
size_t string_prependChar(string_t* dest, const char* string) {
    if(!dest) {
        pcb_error = pcb_error_invalid_parameter;
        return 0;
    }

    if(!string || !(*string)) {
        return dest->buffer ? strlen(dest->buffer) : 0;
    }

    size_t sizeNeeded = strlen(string) + 1;
    if(!string_isNull(dest)) {
        sizeNeeded += strlen(dest->buffer);
    }

    if(dest->bufferSize < sizeNeeded) {
        char* newBuffer = calloc(1, sizeNeeded + 1); // 0 terminating
        if(!newBuffer) {
            pcb_error = pcb_error_no_memory;
            return string_length(dest);
        }
        strncpy(newBuffer, string, sizeNeeded);
        if(!string_isNull(dest)) {
            strncat(newBuffer, dest->buffer, sizeNeeded);
            free(dest->buffer);
        }
        dest->buffer = newBuffer;
        dest->bufferSize = sizeNeeded;
    } else {
        /* using memmove and memmcpy here, do not need the zero terminated 0
           memmove can handle overlapping
         */
        memmove(dest->buffer + strlen(string), dest->buffer, strlen(dest->buffer) + 1);
        memcpy(dest->buffer, string, strlen(string));
    }

    return strlen(dest->buffer);
}

/**
   @ingroup pcb_utils_string
   @brief
   Prepends a string to a string

   @details
   This function is provided for convenience, and is doing exactly the same as @ref string_prependChar

   @param dest the string to which another string will be prepended
   @param string the string that will be prepended

   @return
    - the new string length
 */
size_t string_prepend(string_t* dest, const string_t* string) {
    if(string_isNull(string)) {
        return string_length(dest);
    }

    return string_prependChar(dest, string->buffer);
}

/**
   @ingroup pcb_utils_string
   @brief
   Create a sub string from a string

   @details
   This function creates a sub string from a string starting from a certain position and with a certain length.

   @param dest the string which will contain the sub-string
   @param string the string from which a sub-string is taken.
   @param start the start position in string
   @param length the length of the sub-string, if this is UINT32_MAX a substring until the end is taken

   @return
    - the length of the substring
 */
size_t string_mid(string_t* dest, const string_t* string, unsigned int start, size_t length) {
    if(!dest) {
        pcb_error = pcb_error_invalid_parameter;
        return 0;
    }

    if(string_isNull(string)) {
        string_cleanup(dest);
        return 0;
    }

    size_t size = strlen(string->buffer);
    char* startPos = NULL;

    if(start > size) {
        pcb_error = pcb_error_out_of_boundaries;
        string_cleanup(dest);
        return 0;
    }

    if(((start + length) > size) || (length == UINT32_MAX)) {
        length = size - start;
    }

    if(dest->bufferSize < length + 1) {
        string_cleanup(dest);
        string_initialize(dest, length + 1);
    } else {
        memset(dest->buffer, 0, dest->bufferSize);
    }

    startPos = string->buffer + start;
    memcpy(dest->buffer, startPos, length);

    return strlen(dest->buffer);
}

/**
   @ingroup pcb_utils_string
   @brief
   Create a sub string from a string

   @details
   This function creates a sub string from a string starting from position 0 with a certain length.

   @param dest the string which will contain the sub-string
   @param string the string from which a sub-string is taken.
   @param length the length of the sub-string, if this is UINT32_MAX a substring until the end is taken

   @return
    - the length of the substring
 */
size_t string_left(string_t* dest, const string_t* string, size_t length) {
    return string_mid(dest, string, 0, length);
}

/**
   @ingroup pcb_utils_string
   @brief
   Create a sub string from a string

   @details
   This function creates a sub-string starting from the right with a certain length.\n

   @param dest the string which will contain the sub-string
   @param string the string from which a sub-string is taken.
   @param length the length of the sub-string, if this is UINT32_MAX a substring until the end is taken

   @return
    - the length of the substring
 */
size_t string_right(string_t* dest, const string_t* string, size_t length) {
    if(!dest) {
        pcb_error = pcb_error_invalid_parameter;
        return 0;
    }

    if(string_isNull(string)) {
        string_cleanup(dest);
        return 0;
    }
    if(length > strlen(string->buffer)) {
        length = strlen(string->buffer);
    }

    return string_mid(dest, string, strlen(string->buffer) - length, length);
}

/**
   @ingroup pcb_utils_string
   @brief
   Locate a string within a string.

   @details
   This functions locates a string within a string\n

   @param haystack the string that will be searched
   @param start the start position in the string that will be searched.
   @param needle the string to search for.
   @param cs Case sensitivie or case insensitive search

   @return
    - the location of the needle
    - if the needle is not found UINT32_MAX is returned
 */
unsigned int string_findChar(const string_t* haystack, unsigned int start, const char* needle, string_case_t cs) {
    if(string_isNull(haystack)) {
        return UINT32_MAX;
    }

    if(!needle) {
        pcb_error = pcb_error_invalid_parameter;
        return 0;
    }

    find_fn mystrstr = NULL;
    char* pos = NULL;

    if(start > strlen(haystack->buffer)) {
        pcb_error = pcb_error_out_of_boundaries;
        return UINT32_MAX;
    }

    if(cs == string_case_sensitive) {
        mystrstr = strstr;
    } else {
        mystrstr = strcasestr;
    }

    pos = mystrstr(haystack->buffer + start, needle);
    if(pos == NULL) {
        pcb_error = pcb_error_substring_not_found;
        return UINT32_MAX;
    }

    return pos - haystack->buffer;
}

/**
   @ingroup pcb_utils_string
   @brief
   Locate a string within a string.

   @details
   This function is provided for convenience, and is doing exactly the same as @ref string_findChar

   @param haystack the string that will be searched
   @param start the start position in the string that will be searched.
   @param needle the string to search for.
   @param cs Case sensitivie or case insensitive search

   @return
    - the location of the needle
    - if the needle is not found UINT32_MAX is returned
 */
unsigned int string_find(const string_t* haystack, unsigned int start, const string_t* needle, string_case_t cs) {
    if(string_isNull(needle)) {
        return 0;
    }

    return string_findChar(haystack, start, needle->buffer, cs);
}

/**
   @ingroup pcb_utils_string
   @brief
   Checks that a string is contained within a string

   @details
   If the specified string is contained within the search string this function returns true.

   @param haystack the string that will be searched
   @param needle the string to search for.
   @param cs Case sensitivie or case insensitive search

   @return
    - true if the needle is contained within the haystack
    - false if the needle is not found
 */
bool string_containsChar(const string_t* haystack, const char* needle, string_case_t cs) {
    unsigned int pos = string_findChar(haystack, 0, needle, cs);

    return (pos != UINT32_MAX) ? true : false;
}

/**
   @ingroup pcb_utils_string
   @brief
   Checks that a string is contained within a string

   @details
   If the specified string is contained within the search string this function returns true.

   @param haystack the string that will be searched
   @param needle the string to search for.
   @param cs Case sensitivie or case insensitive search

   @return
    - true if the needle is contained within the haystack
    - false if the needle is not found
 */
bool string_contains(const string_t* haystack, const string_t* needle, string_case_t cs) {
    if(string_isNull(needle)) {
        return true;
    }

    unsigned int pos = string_findChar(haystack, 0, needle->buffer, cs);

    return (pos != UINT32_MAX) ? true : false;
}

/**
   @ingroup pcb_utils_string
   @brief
   Replaces all occurences of one string in the search string with another string

   @details
   This function replaces all occurences of one string in the search string with another string.\n
   The search and replace operation is always case sensitive\n

   @param string the string that will be searched
   @param search the string to search for.
   @param replace the replace string
 */
void string_replaceChar(string_t* string, const char* search, const char* replace) {
    if(string_isNull(string) || !search || !replace) {
        return;
    }

    unsigned int length = string_findChar(string, 0, search, string_case_sensitive);

    if(length == UINT32_MAX) {
        return;
    }

    char* pos = string->buffer;
    string_t tempString;
    string_initialize(&tempString, string->bufferSize + 5 * strlen(search));

    while(length != UINT32_MAX) {
        string->buffer[length] = 0;
        string_appendChar(&tempString, pos);
        string->buffer[length] = search[0];
        string_appendChar(&tempString, replace);
        pos = string->buffer + length + strlen(search);
        length = string_findChar(string, length + strlen(search), search, string_case_sensitive);
    }

    string_appendChar(&tempString, pos);
    free(string->buffer);
    string->buffer = tempString.buffer;
    string->bufferSize = tempString.bufferSize;
}

/**
   @ingroup pcb_utils_string
   @brief
   Replaces all occurences of one string in the search string with another string

   @details
   This function is provided for convenience, and is doing exactly the same as @ref string_replaceChar

   @param string the string that will be searched
   @param search the string to search for.
   @param replace the replace string
 */
void string_replace(string_t* string, const string_t* search, const string_t* replace) {
    if(string_isNull(search) || string_isNull(replace)) {
        return;
    }
    string_replaceChar(string, search->buffer, replace->buffer);
}

/**
   @ingroup pcb_utils_string
   @brief
   Compare two strings.

   @details
   Compare two strings

   calls strcmp or strcasecmp depending on cs.

   @param string1 The first string
   @param string2 The second string
   @param cs Case sensitivie or case insensitive search

   @return
    - <0  string1 < string2
    - 0 string1 == string2
    - >0 string1 > string2
 */
int32_t string_compareChar(const string_t* string1, const char* string2, string_case_t cs) {
    if((string_isEmpty(string1)) && (!string2 || !*string2)) {
        return 0;
    }
    if(string_isEmpty(string1)) {
        return -1;
    }
    if(!string2) {
        return 1;
    }

    compare_fn mystrcmp = NULL;

    if(cs == string_case_sensitive) {
        mystrcmp = strcmp;
    } else {
        mystrcmp = strcasecmp;
    }

    return mystrcmp(string1->buffer, string2);
}

/**
   @ingroup pcb_utils_string
   @brief
   Compare two strings.

   @details
   Compare two strings

   @param string1 The first string
   @param string2 The second string
   @param cs Case sensitivie or case insensitive search

   @return
    - <0  string1 < string2
    - 0 string1 == string2
    - >0 string1 > string2
 */
int32_t string_compare(const string_t* string1, const string_t* string2, string_case_t cs) {
    if(string_isEmpty(string1) && string_isEmpty(string2)) {
        return 0;
    }
    if(string_isEmpty(string1)) {
        return -1;
    }
    if(string_isEmpty(string2)) {
        return 1;
    }

    return string_compareChar(string1, string2->buffer, cs);
}

/**
   @ingroup pcb_utils_string
   @brief
   Check the string buffer

   @details
   Checks the string buffer

   @param string The string for which the string buffer is checked

   @return
    - true  string buffer is allocated
    - false
        - string is NULL
        - the buffer is NULL (null string)
 */
bool string_isNull(const string_t* string) {
    if(!string) {
        pcb_error = pcb_error_invalid_parameter;
        return true;
    }
    return (string->buffer == NULL) ? true : false;
}

/**
   @ingroup pcb_utils_string
   @brief
   Check the string size

   @details
   Check the string size

   @param string The string for which the string size is checked

   @return
    - true  string buffer is allocated
    - false
        - string is NULL
        - the buffer is NULL
        - the string buffer contains an empty string
 */
bool string_isEmpty(const string_t* string) {
    return (string_isNull(string) || strlen(string->buffer) == 0) ? true : false;
}

/**
   @ingroup pcb_utils_string
   @brief
   Returns the length of the string

   @details
   This function returns the length of the string

   @param string The string for which the size is returned

   @return
    - the length of the string.
 */
size_t string_length(const string_t* string) {
    return string_isEmpty(string) ? 0 : strlen(string->buffer);
}

/**
   @ingroup pcb_utils_string
   @brief
   Trim whitespaces at the beginning of the string

   @details
   This function trims away spaces, tab, vertical tab, NL, CR at the beginning of the string

   @param string The string that has to be trimmed
 */
void string_trimLeft(string_t* string) {
    if(string_isNull(string)) {
        return;
    }

    char* data = string->buffer;
    int32_t i = 0;

    while((*data == ' ') || (*data == '\t') || (*data == '\v') || (*data == '\n') || (*data == '\r')) {
        data++;
    }

    if(data == string->buffer) {
        return;
    }

    do {
        string->buffer[i] = data[i];
    } while(data[i++]);
}

/**
   @ingroup pcb_utils_string
   @brief
   Trim whitespaces at the end of the string

   @details
   This function trims away spaces, tab, vertical tab, NL, CR at the end of the string

   @param string The string that has to be trimmed
 */
void string_trimRight(string_t* string) {
    if(string_isNull(string)) {
        return;
    }

    int32_t l = strlen(string->buffer) - 1;
    char* data = &string->buffer[l];

    while((l-- >= 0) && ((*data == ' ') || (*data == '\t') || (*data == '\v') || (*data == '\n') || (*data == '\r'))) {
        *data-- = 0;
    }
}

/**
   @ingroup pcb_utils_string
   @brief
   Trim whitespaces at the beginning of the string

   @details
   This function trims away spaces, tab, vertical tab, NL, CR at the beginning and end of the string

   @param string The string that has to be trimmed
 */
void string_trim(string_t* string) {
    string_trimLeft(string);
    string_trimRight(string);
}

/**
   @ingroup pcb_utils_string
   @brief
   Check whether this string only contains numbers

   @details
   This function checks if the string only contains numeric values.\n

   @param string The string that has to be trimmed

   @return
   - true: the string is numeric (or empty)
   - false: there are non-numeric characters in the string or string is null
 */
bool string_isNumeric(string_t* string) {
    char* data = NULL;
    if(string_isNull(string)) {
        return false;
    }

    data = string->buffer;
    do {
        if(((*data < '0') || (*data > '9')) && (*data != '\0')) {
            return false;
        }
    } while(*data++);

    return true;
}

/**
   @ingroup pcb_utils_string
   @brief
   Replaces environment variables in a string.

   @details
   This functions searches the string for \$(ENV) (ENV is any environment variable) and replaces these with the content
   of the environment variable. If the environment variable was not found it is replaced with an empty string.

   @param string The string that contains environment variables

   @return
   - true: replacing was successful.
   - false: a nerror has occured
    - A closing ')' was missing
 */
bool string_resolveEnvVar(string_t* string) {
    if(string_isNull(string)) {
        return false;
    }

    SAH_TRACEZ_INFO("pcb_string", "start resolve env vars: %s", string_buffer(string));
    bool retval = true;
    char* value = NULL;
    unsigned int start = string_findChar(string, 0, "$(", string_case_sensitive);
    unsigned int end = 0;
    string_t envvar;
    string_initialize(&envvar, 64);
    while(start != UINT32_MAX) {
        end = string_findChar(string, start, ")", string_case_sensitive);
        if(end == UINT32_MAX) {
            retval = false;
            break;
        }
        string_mid(&envvar, string, start + 2, end - (start + 2));
        value = getenv(string_buffer(&envvar));
        string_mid(&envvar, string, start, end - start + 1);
        SAH_TRACEZ_INFO("pcb_string", "replace %s with %s", string_buffer(&envvar), value);
        if(value != NULL) {
            string_replaceChar(string, string_buffer(&envvar), value);
        } else {
            string_replaceChar(string, string_buffer(&envvar), "");
        }
        start = string_findChar(string, 0, "$(", string_case_sensitive);
    }
    string_cleanup(&envvar);

    SAH_TRACEZ_INFO("pcb_string", "end resolve env vars: %s", string_buffer(string));
    return retval;
}

/**
   @ingroup pcb_utils_string
   @brief
   Replaces environment variables in a string.

   @details
   This functions searches the string for \$(ENV) (ENV is any environment variable) and replaces these with the content
   of the environment variable. If the environment variable was not found it is replaced with an empty string.

   @param string The string that contains environment variables

   @return
   - true: replacing was successful.
   - false: a nerror has occured
    - A closing ')' was missing
 */
char* string_resolveEnvVarChar(const char* string) {
    if(!string) {
        return NULL;
    }

    if(strstr(string, "$(") == NULL) {
        return strdup(string);
    }
    string_t temp;
    string_initialize(&temp, 0);
    string_fromChar(&temp, string);
    if(string_resolveEnvVar(&temp)) {
        return temp.buffer;
    }
    string_cleanup(&temp);
    return NULL;
}
