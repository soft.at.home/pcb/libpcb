/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>

#include <debug/sahtrace.h>

#include <pcb/common/error.h>
#include <pcb/utils/linked_list.h>

/**
   @file
   @brief
   Implementation of the linked list functions
 */

/**
   @ingroup pcb_utils_linked_list
   @brief
   Initialize a linked list.

   @details
   This function <b>initializes</b> a <b>linked list</b>. \n
   It is mandatory to initialize a linked list (llist_t) before using it.\n

   @param list the list to be initialized

   @return
    - true: Initialization was succesfull, the llist_t item is ready to be used
    - false: An error has occurred
        - the list item is a NULL pointer
 */
bool llist_initialize(llist_t* list) {
    if(!list) {
        return false;
    }

    list->head = NULL;
    list->tail = NULL;

    return true;
}

/**
   @ingroup pcb_utils_linked_list
   @brief
   Initialize a <b>linked list iterator</b> (=list item)

   @details
   This function <b>initializes</b> a <b>linked list iterator</b>. \n
   This function call initializes the next, previous and list pointer of a linked list iterator to NULL. \n

   @param it the linked list iterator that has to be initialized

   @return
    - true: Initialization was succesfull, the llist_t_operator_t item is ready to be used
    - false: An error has occurred
        - the linked list iterator is a NULL pointer
 */
bool llist_iterator_initialize(llist_iterator_t* it) {
    if(!it) {
        return false;
    }

    it->next = NULL;
    it->prev = NULL;
    it->list = NULL;

    return true;
}

/**
   @ingroup pcb_utils_linked_list
   @brief
   Cleanup a linked list.

   @details
   This funtion removes all iterators from a list.\n
   After calling this function, the linked list will be empty and can be re-used.\n

   @warning
   The elements in the list are not deleted, freeing up the memory used by the iterators must be done
   manually.\n
   The memory taken by the linked list itself is not freed, freeing up the memory taken by the linked list
   must be done manually.

   @param list the list that has to be cleaned up
 */
void llist_cleanup(llist_t* list) {
    if(!list) {
        return;
    }

    llist_iterator_t* it = llist_takeFirst(list);
    while(it) {
        it = llist_takeFirst(list);
    }
}

/**
   @ingroup pcb_utils_linked_list
   @brief
   Get a pointer to the specified list element.

   @details
   This function returns a pointer to the list element at the specified index of the linked list. \n
   Due to the nature of a linked list the items are not garanteed to be at the same place all the time.\n
   Use this function with care, it can have performance impact, the list is iterated to find the correct element.\n

   @warning
   Do not free the returned pointer before removing it from the linked list. Doing so will corrupt the linked list
   itself. Removing the iterator from the linked list can be done by using the function @ref llist_iterator_take.

   @param list the linked list
   @param index the item # in which you are interested (start counting from 0)

   @return
    - a <b>pointer</b> to the specified list element in the linked list.
    - NULL if the index is bigger then the number of elements in the list
    - NULL if the list parameter is NULL.
 */
llist_iterator_t* llist_at(const llist_t* list, unsigned int index) {
    if(!list) {
        return NULL;
    }

    unsigned int pos = 0;
    llist_iterator_t* it = llist_first(list);

    while(it && pos < index) {
        pos++;
        it = llist_iterator_next(it);
    }
    if(pos == index) {
        return it;
    } else {
        pcb_error = pcb_error_out_of_boundaries;
        return NULL;
    }
}

/**
   @ingroup pcb_utils_linked_list
   @brief
   Insert a list iterator before the reference iterator.

   @details
   This function inserts the specified list iterator before the reference iterator in the list. If the
   element was part of another list then the element will be automatically removed from that list.\n
   If the reference is a NULL pointer the insert iterator will be added before the first element in
   the list, the same behavior can be achieved with @ref llist_prepend

   @warning
   The iterator pointer ownership is moved to the linked list. Never free the iterator before removing it from
   the linked list. Removing an interator can be done by using @ref llist_iterator_take.

   @param list the list the iterator must be added to
   @param reference the reference iterator
   @param insert the iterator that has to be inserted in the list

   @return
    - true: The list element was inserted succesfully before the reference object
    - false: An error has occurred
        - list or insert is a NULL pointer
        - the reference iterator is not part of the list
 */
bool llist_insertBefore(llist_t* list, llist_iterator_t* reference, llist_iterator_t* insert) {
    if(!list || !insert) {
        return false;
    }

    if(reference && (reference->list != list)) {
        return false;
    }

    if(reference == insert) {
        return true;
    }

    insert = llist_iterator_take(insert);
    insert->list = list;

    if(!reference) {
        reference = list->head;
    }

    llist_iterator_t* before = NULL;
    llist_iterator_t* after = NULL;

    if(reference) {
        before = reference->prev;
        after = reference;
    }

    insert->next = after;
    insert->prev = before;
    if(after) {
        after->prev = insert;
    } else {
        list->tail = insert;
    }
    if(before) {
        before->next = insert;
    } else {
        list->head = insert;
    }

    return true;
}

/**
   @ingroup pcb_utils_linked_list
   @brief
   Insert a list element after the reference element.

   @details
   This function inserts the specified list iterator after the reference iterator in the list. If the
   element was part of another list then the element will be automatically removed from that list.\n
   If the reference is a NULL pointer the insert iterator will be added after the last element in
   the list, the same behavior can be achieved with @ref llist_append

   @warning
   The iterator pointer ownership is moved to the linked list. Never free the iterator before removing it from
   the linked list. Removing an interator can be done by using @ref llist_iterator_take.

   @param list the list the iterator must be added to
   @param reference the reference iterator
   @param insert the iterator that has to be inserted in the list

   @return
    - true: The list element was inserted succesfully after the reference object
    - false: An error has occurred
        - list or insert is a NULL pointer
        - the reference iterator is not part of the list
 */
bool llist_insertAfter(llist_t* list, llist_iterator_t* reference, llist_iterator_t* insert) {
    if(!list || !insert) {
        return false;
    }

    if(reference && (reference->list != list)) {
        return false;
    }

    if(reference == insert) {
        return true;
    }

    insert = llist_iterator_take(insert);
    insert->list = list;

    if(!reference) {
        reference = list->tail;
    }

    llist_iterator_t* before = NULL;
    llist_iterator_t* after = NULL;

    if(reference) {
        before = reference;
        after = reference->next;
    }

    insert->next = after;
    insert->prev = before;
    if(after) {
        after->prev = insert;
    } else {
        list->tail = insert;
    }
    if(before) {
        before->next = insert;
    } else {
        list->head = insert;
    }

    return true;
}

/**
   @ingroup pcb_utils_linked_list
   @brief
   Append the element to a list.

   @details
   This function appends the specified iterator to an existing list. If the element was part of
   another list then the element will be automatically removed from that list.\n
   The iterator will become the last element in the list.

   @warning
   The iterator pointer ownership is moved to the linked list. Never free the iterator before removing it from
   the linked list. Removing an interator can be done by using @ref llist_iterator_take.

   @param list the list to which the iterator has to be added
   @param it the iterator that has to be attached to the list

   @return
    - true: The list element was added succesfully appended to the list
    - false: An error has occurred
        - list or it is a NULL pointer
 */
bool llist_append(llist_t* list, llist_iterator_t* it) {
    if(!list || !it) {
        return false;
    }

    if(it->list) {
        llist_iterator_take(it);
    }

    llist_iterator_t* before = list->tail;
    it->list = list;
    it->next = NULL;
    it->prev = before;

    if(before) {
        before->next = it;
    } else {
        list->head = it;
    }
    list->tail = it;

    return true;
}

/**
   @ingroup pcb_utils_linked_list
   @brief
   Prepend the element to a list.

   @details
   This function prepends the specified iterator to an existing list. If the element was part of
   another list then the element will be automatically removed from that list.\n
   The iterator will become the first element in the list.

   @warning
   The iterator pointer ownership is moved to the linked list. Never free the iterator before removing it from
   the linked list. Removing an interator can be done by using @ref llist_iterator_take.

   @param list the list to which the element has to be added
   @param it the iterator that has to be attached to the list

   @return
    - true: The list element was added succesfully prepended to the list
    - false: An error has occurred
        - list or it is a NULL pointer
 */
bool llist_prepend(llist_t* list, llist_iterator_t* it) {
    if(!list || !it) {
        return false;
    }

    if(it->list) {
        llist_iterator_take(it);
    }

    llist_iterator_t* after = list->head;
    it->list = list;
    it->next = after;
    it->prev = NULL;

    if(after) {
        after->prev = it;
    } else {
        list->tail = it;
    }
    list->head = it;

    return true;
}

/**
   @ingroup pcb_utils_linked_list
   @brief
   Attach the element to a list at a certain index.

   @details
   This function attaches the specified iterator to an existing list at the given index.
   If the element is part of another list then the element will be automatically removed from that list.\n
   If the index is higher then the number of elements in the list, it will be appended after the last element.\n

   @warning
   The iterator pointer ownership is moved to the linked list. Never free the iterator before removing it from
   the linked list. Removing an interator can be done by using @ref llist_iterator_take.

   @param list the list to which the element has to be added
   @param index the item # where to add the element to the list (start counting from 0)
   @param it the list element that has to be attached to the list

   @return
    - true: The list element was succesfully inserted into the list
    - false: An error has occurred
        - list or it is a NULL pointer
 */
bool llist_insertAt(llist_t* list, unsigned int index, llist_iterator_t* it) {
    llist_iterator_t* pos = llist_at(list, index);
    if(!pos) {
        return llist_insertAfter(list, NULL, it);
    }
    return llist_insertBefore(list, pos, it);
}

/**
   @ingroup pcb_utils_linked_list
   @brief
   Detach the element from a list.

   @details
   This function detaches the specified list element from an existing list.\n

   @param it the list element that has to be detached from the list

   @return
    - a <b>pointer</b> to the detached list element.
    - NULL if an error occured
        - it is NULL
 */
llist_iterator_t* llist_iterator_take(llist_iterator_t* it) {
    if(!it) {
        return NULL;
    }

    if(!it->list) {
        return it;
    }

    llist_iterator_t* before = it->prev;
    llist_iterator_t* after = it->next;

    if(before) {
        before->next = after;
    } else {
        it->list->head = after;
    }
    if(after) {
        after->prev = before;
    } else {
        it->list->tail = before;
    }

    it->prev = NULL;
    it->next = NULL;
    it->list = NULL;

    return it;
}

/**
   @ingroup pcb_utils_linked_list
   @brief
   Get the size of the list.

   @details
   This function returns the size of the list.\n
   This function will iterate over all elements in the list and count them, this can have performance impact.
   If you need to check if a list contains elements or is empty call @ref llist_isEmpty.

   @param list the list in wich you are interested

   @return
    - the number of elements in the list.
    - 0 if list is a NULL pointer ot the list is empty.
 */
unsigned int llist_size(const llist_t* list) {
    if(!list) {
        return 0;
    }
    unsigned int size = 0;
    llist_iterator_t* it;
    llist_for_each(it, list) {
        size++;
    }
    return size;
}

/**
   @ingroup pcb_utils_linked_list
   @brief
   Determine if the list is empty.

   @details
   This function checks wether the list is empty or not.\n

   @param list the list in wich you are interested

   @return
    - true:
        - The list is empty
        - list == NULL
    - false: The list is not empty

 */
bool llist_isEmpty(const llist_t* list) {
    return !(list && list->head);
}

/**
   @ingroup pcb_utils_linked_list
   @brief
   Get the first list element.

   @details
   This function returns the first list iterator of the linked list.\n

   @warning
   Do not free the returned pointer before removing it from the linked list. Doing so will corrupt the linked list
   itself. Removing the iterator from the linked list can be done by using the function @ref llist_iterator_take.

   @param list the linked list

   @return
    - a <b>pointer</b> to the first list iterator of the linked list.
    - NULL if the list parameter is NULL or the list is empty.
 */

llist_iterator_t* llist_first(const llist_t* list) {
    return list ? list->head : NULL;
}

/**
   @ingroup pcb_utils_linked_list
   @brief
   Get the last list element.

   @details
   This function returns the last list iterator of the linked list.\n

   @warning
   Do not free the returned pointer before removing it from the linked list. Doing so will corrupt the linked list
   itself. Removing the iterator from the linked list can be done by using the function @ref llist_iterator_take.

   @param list the linked list

   @return
    - a <b>pointer</b> to the last list iterator of the linked list.
    - NULL if the list parameter is NULL or the list is empty.

 */
llist_iterator_t* llist_last(const llist_t* list) {
    return list ? list->tail : NULL;
}

/**
   @ingroup pcb_utils_linked_list
   @brief
   Get a pointer to the next list iterator.

   @details
   This function returns a pointer to the next list iterator.\n

   @warning
   Do not free the returned pointer before removing it from the linked list. Doing so will corrupt the linked list
   itself. Removing the iterator from the linked list can be done by using the function @ref llist_iterator_take.

   @param it the reference iterator

   @return
    - a <b>pointer</b> to the next list iterator in the linked list.
    - NULL if the reference iterator is the last in the list.
    - NULL if the reference iterator is NULL.
 */
llist_iterator_t* llist_iterator_next(const llist_iterator_t* it) {
    return (it) ? it->next : NULL;
}

/**
   @ingroup pcb_utils_linked_list
   @brief
   Get a pointer to the previous list iterator.

   @details
   This function returns a pointer to the previous list iterator. \n

   @warning
   Do not free the returned pointer before removing it from the linked list. Doing so will corrupt the linked list
   itself. Removing the iterator from the linked list can be done by using the function @ref llist_iterator_take.

   @param it the reference iterator

   @return
    - a <b>pointer</b> to the previous list iterator in the linked list.
    - NULL if the reference iterator is the last in the list.
    - NULL if the reference iterator is NULL.
 */
llist_iterator_t* llist_iterator_prev(const llist_iterator_t* it) {
    return (it) ? it->prev : NULL;
}

/**
   @ingroup pcb_utils_linked_list
   @brief
   Detach the first element from a list.

   @details
   This function detaches the first list element from an existing list.\n

   @param list the list from which the first element has to be detached

   @return
    - a <b>pointer</b> to the detached list element.
    - NULL if an error occured
        - list is NULL
        - the list is empty
 */
llist_iterator_t* llist_takeFirst(llist_t* list) {
    return llist_iterator_take(llist_first(list));
}

/**
   @ingroup pcb_utils_linked_list
   @brief
   Detach the last element from a list.

   @details
   This function detaches the last list element from an existing list.\n

   @param list the list from which the last element has to be detached

   @return
    - a <b>pointer</b> to the detached list element.
    - NULL if an error occured
        - list is NULL
        - the list is empty
 */
llist_iterator_t* llist_takeLast(llist_t* list) {
    return llist_iterator_take(llist_last(list));
}

typedef struct _llist_qsort_data {
    llist_qsort_compar compar;
    void* userdata;
} llist_qsort_data_t;

#ifdef HAVE_QSORT_R
static int llist_qsort_compare_r(const void* a, const void* b, void* d) {
    llist_qsort_data_t* data = (llist_qsort_data_t*) d;

    return data->compar(*((llist_iterator_t**) a), *((llist_iterator_t**) b), data->userdata);
}
#else
static llist_qsort_data_t* global_qsort_data = NULL;
static int llist_qsort_compare(const void* a, const void* b) {
    if(!global_qsort_data) {
        return 0;
    }

    return global_qsort_data->compar(*((llist_iterator_t**) a), *((llist_iterator_t**) b), global_qsort_data->userdata);
}
#endif

/**
   @ingroup pcb_utils_linked_list
   @brief
   Sort a linked list using the provided comparison function.\n
   The contents of the list are sorted in ascending order according to a comparison function pointed to
   by <b>compar</b>, which is called with two arguments that point to the llist iterators being compared.
   The comparison function must return an integer less than, equal to, or greater than zero if the first
   argument is considered to be respectively less than, equal to, or greater than the second.
   If two members compare as equal, their order in the sorted list is undefined.
   If <b>reverse</b> is set, the sorting order is reversed.

   @warning
   This function uses a trampoline function to call the provided comparison function.
   When the qsort_r() function is not available by the toolchain, the sorting function is not reentrant
   nor safe to use in threads.

   @param list the list to sort
   @param compar the comparison function
   @param reverse reverse the sorting order
   @param userdata userdata passed on to comparison function

   @return
    - true when sorting is done.
    - false if an error occured, pcb_error will be set
        - pcb_error_invalid_parameter when list or compar is NULL
        - pcb_error_no_memory when out of memory
 */
bool llist_qsort(llist_t* list, llist_qsort_compar compar, bool reverse, void* userdata) {
    size_t size = 0;
    llist_iterator_t** iter = NULL, ** array = NULL;
    bool (* llist_action)(llist_t*, llist_iterator_t*) = llist_append;
    llist_qsort_data_t data;

    if(!list || !compar) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    size = (size_t) llist_size(list);
    if(!size) {
        return true;
    }

    /* Add 1 extra that will act as an end-of-the-array indicator */
    array = calloc(size + 1, sizeof(llist_iterator_t*));
    if(!array) {
        pcb_error = pcb_error_no_memory;
        return false;
    }

    for(iter = array; !llist_isEmpty(list); iter++) {
        *iter = llist_takeFirst(list);
    }

    data.compar = compar;
    data.userdata = userdata;

#ifdef HAVE_QSORT_R
    qsort_r(array, size, sizeof(llist_iterator_t*), llist_qsort_compare_r, (void*) &data);
#else
    global_qsort_data = &data;
    qsort(array, size, sizeof(llist_iterator_t*), llist_qsort_compare);
    global_qsort_data = NULL;
#endif

    if(reverse) {
        llist_action = llist_prepend;
    }

    for(iter = array; *iter; iter++) {
        llist_action(list, *iter);
    }

    free(array);

    return true;
}
