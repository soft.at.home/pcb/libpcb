/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>

#include <debug/sahtrace.h>

#include <pcb/common/error.h>
#include <pcb/utils/timer.h>



typedef struct _deferred_function_data {
    pcb_timer_deferred_function_t fn;
    void* userdata;
} deferred_function_data_t;

static llist_t timers;
static struct timeval g_current;                /**< Current timer */
static bool timers_enabled = true;

static struct timeval pcb_timer_getElapsedTime(void) {
    struct itimerval ti;
    struct timeval elapsed = { 0, 0 };

    /* get the elapsed period */
    if(!timerisset(&g_current)) {
        return elapsed;
    }

    getitimer(ITIMER_REAL, &ti);
    if(timerisset(&ti.it_value)) {
        timersub(&g_current, &ti.it_value, &elapsed);
    } else {
        elapsed = g_current;
    }

    return elapsed;
}

static void pcb_timer_updateCurrent(struct timeval* elapsed) {
    timersub(&g_current, elapsed, &g_current);

    if(g_current.tv_sec < 0) {
        timerclear(&g_current);
    }
}

static bool pcb_timer_startTimer(pcb_timer_t* timer) {
    timer->state = pcb_timer_running;
    // *INDENT-OFF*
    if(!timerisset(&g_current) || timercmp(&g_current, &timer->timer.it_value, >)) {
        g_current = timer->timer.it_value;
    }
    // *INDENT-ON*
    SAH_TRACEZ_INFO("pcb_timer", "Timer %p running: timeout value = %d.%d seconds", timer, (int) timer->timer.it_value.tv_sec, (int) (timer->timer.it_value.tv_usec / 1000));
    return true;
}

static bool pcb_timer_updateTimer(pcb_timer_t* timer, struct timeval* elapsed) {
    /* update the timer */
    timersub(&timer->timer.it_value, elapsed, &timer->timer.it_value);
    if(!((timer->timer.it_value.tv_sec < 0) || !timerisset(&timer->timer.it_value))) {
        /* timer is still running
           update current if the timer is smaller then current
         */
        // *INDENT-OFF*
        if(!timerisset(&g_current) || timercmp(&g_current, &timer->timer.it_value, >)) {
            g_current = timer->timer.it_value;
        }
        // *INDENT-ON*
        return true;
    }

    /* timer has expired */
    if(timer->state != pcb_timer_expired) {
        SAH_TRACEZ_INFO("pcb_timer", "Timer %p has expired", timer);
        timer->state = pcb_timer_expired;
    }

    /* if an interval is set, reset the timer to the interval */
    if(timerisset(&timer->timer.it_interval)) {
        timer->timer.it_value = timer->timer.it_interval;
        SAH_TRACEZ_INFO("pcb_timer", "Reset timer %p to interval, timeout value = %d.%d seconds", timer, (int) timer->timer.it_value.tv_sec, (int) (timer->timer.it_value.tv_usec / 1000));
        /* update current if the timer is smaller then current */
        // *INDENT-OFF*
        if(!timerisset(&g_current) || timercmp(&g_current, &timer->timer.it_value, >)) {
            g_current = timer->timer.it_value;
        }
        // *INDENT-ON*
        return true;
    }

    return false;
}

static void pcb_timer_call_deferred_function(pcb_timer_t* timer, void* userdata) {
    deferred_function_data_t* data = (deferred_function_data_t*) userdata;

    data->fn(data->userdata);

    free(userdata);
    timer->userdata = NULL;
    pcb_timer_destroy(timer);
}

/**
   @file
   @brief
   Implementation of the public timer functions
 */

/**
   @ingroup pcb_utils_timers
   @brief
   Initialize the global timers list

   @details
   Initializes the global timers list.\n
   Timers can only be used after calling this function.

   @return
    - true when initialization is successful
    - false: an error occured.
 */
bool pcb_timer_initializeTimers(void) {
    llist_initialize(&timers);
    return true;
}

/**
   @ingroup pcb_utils_timers
   @brief
   Clanup the global timers list

   @details
   Cleanup the global timers list, also destroys all timers in the list.\n
   After calling this function, timers can not be used anymore.
 */
void pcb_timer_cleanupTimers(void) {
    llist_iterator_t* it = NULL;
    pcb_timer_t* timer = NULL;
    /* delete all timers */
    it = llist_takeFirst(&timers);
    while(it) {
        SAH_TRACEZ_INFO("pcb_timer", "Removing timer");
        timer = llist_item_data(it, pcb_timer_t, it);
        pcb_timer_destroy(timer);
        free(timer);
        it = llist_takeFirst(&timers);
    }
}

/**
   @ingroup pcb_utils_timers
   @brief
   Caclulate the remaining time of all timers.

   @details
   Updates all timers with the time passed since the last update. (Subtract the passed time from each timer).
   If a timer reaches zero or becomes negative it is expired. The timer is reset if an interval
   was set. The smallest remaining time is used to set the SIGALRM signal.
 */
void pcb_timer_calculateTimers(void) {
    struct timeval elapsed = { 0, 0 };
    bool activeTimer = false;

    elapsed = pcb_timer_getElapsedTime();

    pcb_timer_updateCurrent(&elapsed);

    /* iterate over all timers */
    llist_iterator_t* it = NULL;
    pcb_timer_t* timer = NULL;
    llist_for_each(it, &timers) {
        timer = llist_item_data(it, pcb_timer_t, it);
        /* this timer is off, continue */
        if((timer->state == pcb_timer_off) || (timer->state == pcb_timer_destroyed)) {
            continue;
        }

        /* just started, set state to running */
        if(timer->state == pcb_timer_started) {
            activeTimer |= pcb_timer_startTimer(timer);
            continue;
        }

        activeTimer |= pcb_timer_updateTimer(timer, &elapsed);
    }

    /* if there is an active timer, set it */
    if(activeTimer) {
        struct itimerval ti;
        if(!timerisset(&g_current)) {
            g_current.tv_usec = 100000; /* 100 ms */
        }
        timerclear(&ti.it_interval);
        ti.it_value = g_current;
        setitimer(ITIMER_REAL, &ti, NULL);
    }
}

/**
   @ingroup pcb_utils_timers
   @brief
   Check all timers and call handler when the timer is expired.

   @details
   Loops over the global list of timers and for each expired timer the timeout handler is called.
   If the timer has an interval, the state is reset to @ref pcb_timer_running, if no interval is availble
   the timer state is reset to @ref pcb_timer_off.
 */
void pcb_timer_checkTimers(void) {
    if(!timers_enabled) {
        return;
    }

    llist_iterator_t* it = llist_first(&timers);
    llist_iterator_t* prefetch = llist_iterator_next(it);
    /* loop over all timers, and handle the expired ones */
    pcb_timer_t* timer = NULL;
    while(it) {
        timer = llist_item_data(it, pcb_timer_t, it);
        switch(timer->state) {
        case pcb_timer_destroyed:
            llist_iterator_take(&timer->it);
            free(timer);
            break;
        case pcb_timer_expired:
            if(timerisset(&timer->timer.it_interval)) {
                timer->state = pcb_timer_running;
            } else {
                timer->state = pcb_timer_off;
            }
            if(timer->handler) {
                pcb_timer_enableTimers(false);
                SAH_TRACEZ_NOTICE("pcb_timer", "Calling timeout handler for timer %p", timer);
                timer->handler(timer, timer->userdata);
                pcb_timer_enableTimers(true);
            }
            break;
        default:
            /* do nothing */
            break;
        }
        it = prefetch;
        prefetch = llist_iterator_next(it);
    }
}

/**
   @ingroup pcb_utils_timers
   @brief
   Enable or disable all timers.

   @details
   With this function all timers can be disable or enabled. When the timers
   are disabled they can still expire, but the handler functions are not called.
   The next time the timers are enabled all handlers of the expired timers will be called in the next
   iteration in the event loop.

   @param enable True to enable timer handling or false to disable timer handling

   @return
    - true when initialization is successful
    - false: an error occured.
 */
bool pcb_timer_enableTimers(bool enable) {
    timers_enabled = enable;

    return true;
}

/**
   @ingroup pcb_utils_timers
   @brief
   Create a new timer

   @details
   Allocate, initialize a new timer structure. The new timer will be added to the global list of timers.

   @return
    - pointer to the new timer
    - NULL: memory allocation failed
 */
pcb_timer_t* pcb_timer_create(void) {
    pcb_timer_t* timer = (pcb_timer_t*) calloc(1, sizeof(pcb_timer_t));
    if(timer) {
        llist_append(&timers, &timer->it);
    }
    return timer;
}

/**
   @ingroup pcb_utils_timers
   @brief
   Destroy a timer

   @details
   Stops the timer and marks it as destroyed.
   Eventually, the timer will be removed from the global timers list and allocated memory will be
   freed, so do not use this timer after calling this function.

   @param timer pointer to the timer.
 */
void pcb_timer_destroy(pcb_timer_t* timer) {
    if(!timer) {
        pcb_error = pcb_error_invalid_parameter;
        return;
    }

    pcb_timer_stop(timer);

    if(timer->handler == pcb_timer_call_deferred_function) {
        free(timer->userdata);
        timer->userdata = NULL;
    }

    timer->state = pcb_timer_destroyed;
    return;
}

/**
   @ingroup pcb_utils_timers
   @brief
   Set the interval of a timer

   @details
   Assigns an interval to the timer. When the initial timeout of the timer occurs, this
   interval time will be used to restart the timer.

   @param timer pointer to the timer.
   @param msec Time of the interval in milliseconds

   @return
    - true when interval was set
    - false: an error occured.
 */
bool pcb_timer_setInterval(pcb_timer_t* timer, unsigned int msec) {
    if(!timer || (timer->state == pcb_timer_destroyed)) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    timer->timer.it_interval.tv_sec = msec / 1000;
    if(msec < 100) {
        timer->timer.it_interval.tv_usec = 100000; /* 100 ms */
    } else {
        timer->timer.it_interval.tv_usec = (msec % 1000) * 1000;
    }
    return true;
}

/**
   @ingroup pcb_utils_timers
   @brief
   Set the timeout handler

   @details
   Adds a function to the timer, which will be called when the timer expires.
   A timer can have only one handler.

   @param timer pointer to the timer.
   @param handler Pointer to the handler function.

   @return
    - true when the handler is set.
    - false: an error occured.
 */
bool pcb_timer_setHandler(pcb_timer_t* timer, pcb_timer_timeout_handler_t handler) {
    if(!timer || (timer->state == pcb_timer_destroyed)) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    timer->handler = handler;
    return true;
}

/**
   @ingroup pcb_utils_timers
   @brief
   Set user data

   @details
   Adds some user specified data to the timer.
   It is the responsibility of the caller to this function, to also free the memory of that data part (if needed).
   The timer itself will not free up the memory.  If the timer already contained some user data, it will be overwritten.

   The user data is passed to the timeout handler.

   @param timer pointer to the timer.
   @param userData pointer to user data

   @return
    - true when the data is set.
    - false: an error occured.
 */
bool pcb_timer_setUserData(pcb_timer_t* timer, void* userData) {
    if(!timer || (timer->state == pcb_timer_destroyed)) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    timer->userdata = userData;
    return true;
}

/**
   @ingroup pcb_utils_timers
   @brief
   Get user data

   @details
   Returns a pointer to the user data

   @param timer pointer to the timer.

   @return
    - The user data
 */
void* pcb_timer_getUserData(pcb_timer_t* timer) {
    if(!timer) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    return timer->userdata;
}

/**
   @ingroup pcb_utils_timers
   @brief
   Get the remaining time of the timer

   @details
   Returns the remaining time of the timer in milliseconds.
   If a timer is not running, this function will always return 0.

   @param timer pointer to the timer.

   @return
    - The remaining timer
 */
unsigned int pcb_timer_remainingTime(pcb_timer_t* timer) {
    if(!timer || (timer->state == pcb_timer_destroyed)) {
        pcb_error = pcb_error_invalid_parameter;
        return 0;
    }

    if((timer->state != pcb_timer_running) &&
       (timer->state != pcb_timer_started)) {
        return 0;
    }

    return timer->timer.it_value.tv_sec * 1000 + timer->timer.it_value.tv_usec / 1000;
}

/**
   @ingroup pcb_utils_timers
   @brief
   Start or reset a timer

   @details
   This function will start the timer, the initial time out value is also given with this function.
   If an interval time was set, the interval will start after the initial timeout.

   If the timer was already started, the timer will be reset and restarted using the new timeout value.

   @param timer pointer to the timer.
   @param timeout initial timeout value.

   @return
    - true the timer is started
    - false: an error occured.
 */
bool pcb_timer_start(pcb_timer_t* timer, unsigned int timeout) {
    if(!timer || (timer->state == pcb_timer_destroyed)) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    timer->timer.it_value.tv_sec = timeout / 1000;
    if(timeout < 100) {
        timer->timer.it_value.tv_usec = 100000; /* 100 ms */
    } else {
        timer->timer.it_value.tv_usec = (timeout % 1000) * 1000;
    }

    SAH_TRACEZ_INFO("pcb_timer", "Timer %p set: timeout value = %d.%d seconds", timer, (int) timer->timer.it_value.tv_sec, (int) (timer->timer.it_value.tv_usec / 1000));
    timer->state = pcb_timer_started;
    return true;
}

/**
   @ingroup pcb_utils_timers
   @brief
   Stops the timer

   @details
   This function will stop the timer. After calling this function the timeout handler of the timer
   will never be called again until the timer is restarted using @ref pcb_timer_start

   @param timer pointer to the timer.

   @return
    - true the timer is stopped
    - false: an error occured.
 */
bool pcb_timer_stop(pcb_timer_t* timer) {
    if(!timer || (timer->state == pcb_timer_destroyed)) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    timer->state = pcb_timer_off;
    return true;
}

/**
   @ingroup pcb_utils_timers
   @brief
   Get the timer's state.

   @details
   Return the state of the timer.

   @param timer pointer to the timer.

   @return
    - The current state of the timer
 */
pcb_timer_state_t pcb_timer_getState(pcb_timer_t* timer) {
    if(!timer || (timer->state == pcb_timer_destroyed)) {
        pcb_error = pcb_error_invalid_parameter;
        return pcb_timer_off;
    }

    return timer->state;
}

/**
   @ingroup pcb_utils_timers
   @brief
   Call a function at a later time.

   @details
   This function will create a timer to call a function at a later timer. When the timer timesout,
   the specified function is called and the created timer will be destroyed.

   If the userdata is destroyed, there is no way to destroy the timer
   (without destroying all other timers), which can cause the timer handler to access the userdata
   (use-after-free). If this situation can happen, consider using @ref pcb_timer_createAndStart.

   @param fn the function that must be called when the timer times out
   @param userdata pointer to data that will be passed to the deferred function
   @param time time in milliseconds that must be wait before calling the function

   @return
    - true when the timer was successfully started
    - false an error occured
 */
bool pcb_timer_defer(pcb_timer_deferred_function_t fn, void* userdata, unsigned int time) {
    if(!fn) {
        return false;
    }

    pcb_timer_t* timer = pcb_timer_create();
    if(!timer) {
        return false;
    }

    deferred_function_data_t* data = (deferred_function_data_t*) calloc(1, sizeof(deferred_function_data_t));
    if(!data) {
        pcb_timer_destroy(timer);
        return false;
    }

    data->fn = fn;
    data->userdata = userdata;
    pcb_timer_setUserData(timer, data);
    pcb_timer_setHandler(timer, pcb_timer_call_deferred_function);

    return pcb_timer_start(timer, time);
}

/**
   @ingroup pcb_utils_timers
   @brief
   Convenience function to call a function at a later time, while allowing cleanup.

   @details
   Similar to @ref pcb_timer_defer but can be cleaned up on destruction.

   This function makes sure `handler` is called after `timeInMs` milliseconds has passed.

   When called a second time but before `handler` has been executed, the timer is put at
   the back of the queue of timers.

   Cleaning up the timer is the responsibility of the caller (also if `handler` does get called).

   @param timer: If points to existing timer, it's reused, if points to a nullpointer, a new
   timer is created and the pointer is updated, allowing you to destroy the timer.
   @param handler: callback that will be called when `timeInMs` milliseconds has passed.
   @param userData: will be passed to `handler`
   @param timeinMs: amount of time that should pass before `handler` is called.
   @return true on success, false on failure:
   - timer == NULL
   - timer cannot be created, configured, or started.
 */
bool pcb_timer_createAndStart(pcb_timer_t** timer, pcb_timer_timeout_handler_t handler,
                              void* userData, unsigned int timeInMs) {

    bool timer_locally_created = false;
    if(!timer) {
        return false;
    }
    if(!*timer) {
        *timer = pcb_timer_create();
        if(!*timer) {
            return false;
        }
        timer_locally_created = true;
    }
    bool ok = pcb_timer_setUserData(*timer, userData);
    ok &= pcb_timer_setHandler(*timer, handler);
    if(!ok) {
        goto cleanup;
    }
    ok = pcb_timer_start(*timer, timeInMs);
    if(!ok) {
        goto cleanup;
    }

    return true;

cleanup:
    if(timer_locally_created) {
        pcb_timer_destroy(*timer);
        *timer = NULL;
    }
    return false;
}
