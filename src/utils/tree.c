/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <pcb/common/error.h>
#include <pcb/utils/tree.h>

/**
   @file
   @brief
   Implementation of the public tree functions
 */

/**
   @ingroup pcb_utils_tree
   @brief
   Initialize a tree.

   @details
   This function <b>initializes</b> a <b>tree</b>. \n
   It is mandatory to initialize a tree (tree_t) before using it. \n
   Every @ref tree_initialize() call should have a corresponding  @ref tree_cleanup() call to free the memory,\
   This function calls  @ref tree_item_initialize().\n

   @param tree the tree to be initialized

   @return
    - true: Initialization was succesfull, the tree_t item is ready to be used
    - false: An error has occurred
        - the tree is a NULL pointer
        - memory allocation failed
 */
bool tree_initialize(tree_t* tree) {
    if(!tree) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    return tree_item_initialize(&tree->root);
}

/**
   @ingroup pcb_utils_tree
   @brief
   Initialize a tree item.

   @details
   This function <b>initializes</b> a <b>tree item</b>. \n
   It is mandatory to initialize a tree item (tree_item_t) before using it. \n
   Every @ref tree_item_initialize() call should have a corresponding  @ref tree_item_cleanup() call to free the memory,
   even if the @ref tree_item_initialize() function returned an error. \n
   This function initializes it's own set of linked list pointers using @ref llist_iterator_initialize().
   Then it initializes it's pointer to the parent with NULL. Finaly the child list pointers are initialized
   with @ref llist_initialize().\n

   @param item the tree item to be initialized

   @return
    - true: Initialization was succesfull, the tree_item_t is ready to be used
    - false: An error has occurred
        - the tree item is a NULL pointer
        - memory allocation failed
 */
bool tree_item_initialize(tree_item_t* item) {
    if(!item) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    llist_iterator_initialize(&item->listItem);
    return llist_initialize(&item->children);
}

/**
   @ingroup pcb_utils_tree
   @brief
   Cleanup a tree.

   @details
   This function <b>frees</b> the tree. \n
   It is mandatory to free all tree items belonging to this tree before calling this function. \n
   This function calls @ref tree_item_cleanup() to free it's linked list containing the children. \n

   @param tree the tree that has to be cleaned up
 */
void tree_cleanup(tree_t* tree) {
    if(!tree) {
        pcb_error = pcb_error_invalid_parameter;
        return;
    }

    tree_item_cleanup(&tree->root);
}

/**
   @ingroup pcb_utils_tree
   @brief
   Cleanup a tree item.

   @details
   This function <b>frees</b> the children of this tree item. \n
   This function calls @ref llist_cleanup() to free it's linked list containing the children. \n

   @param item the tree item that has to be cleaned up
 */
void tree_item_cleanup(tree_item_t* item) {
    if(!item) {
        pcb_error = pcb_error_invalid_parameter;
        return;
    }

    llist_cleanup(&item->children);
}

/**
   @ingroup pcb_utils_tree
   @brief
   Get the tree root item.

   @details
   This function returns a <b>pointer</b> to the tree root item. \n
   This function returns tree->root\n

   @param tree the tree you are interested in

   @return
    - <b>pointer</b> to the tree root item
    - NULL if the tree pointer is NULL
 */
tree_item_t* tree_root(const tree_t* tree) {
    if(!tree) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }
    return (tree_item_t*) &tree->root;
}

/**
   @ingroup pcb_utils_tree
   @brief
   Get the tree item's parent.

   @details
   This function returns a <b>pointer</b> to the tree item's parent. \n
   This function returns item->parent\n

   @param item the tree item you are interested in

   @return
    - <b>pointer</b> to the tree item's parent
    - NULL if item is NULL
 */
tree_item_t* tree_item_parent(const tree_item_t* item) {
    if(!item) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }
    tree_item_t* parent = NULL;
    if(item->listItem.list) {
        parent = llist_item_data(item->listItem.list, tree_item_t, children);
    }

    return parent;
}

/**
   @ingroup pcb_utils_tree
   @brief
   Get the tree item's first child.

   @details
   This function returns a <b>pointer</b> to the first child of this tree item. \n
   This function calls @ref llist_first() to get the first child item, then it calls
   @ref llist_item_data() to get the pointer to the tree_item_t structure \n

   @param item the tree item you are interested in

   @return
    - a <b>pointer</b> to the tree item's first child,
    - NULL if an error occured
        - item is NULL
        - item has no children
 */
tree_item_t* tree_item_firstChild(tree_item_t* item) {
    if(!item) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    llist_iterator_t* ll_item = llist_first(&item->children);
    if(!ll_item) {
        return NULL;
    }
    return llist_item_data(ll_item, tree_item_t, listItem);
}

/**
   @ingroup pcb_utils_tree
   @brief
   Get a pointer to the specified tree item's child.

   @details
   This function returns a <b>pointer</b> to the specifiedtree item child . \n
   This function calls @ref llist_at() to get the specified child item, then it calls
   @ref llist_item_data() to get the pointer to the tree_item_t structure \n

   @param item the tree item you are interested in
   @param index the item # in which you are interested (start counting from 0)

   @return
    - a <b>pointer</b> to the specified item's first child,
    - NULL is returned if an error occured
        - item is NULL
        - item does not exist
 */
tree_item_t* tree_item_childAt(tree_item_t* item, unsigned int index) {
    if(!item) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    llist_iterator_t* ll_item = llist_at(&item->children, index);
    if(!ll_item) {
        return NULL;
    }
    return llist_item_data(ll_item, tree_item_t, listItem);
}

/**
   @ingroup pcb_utils_tree
   @brief
   Get the tree item's last child.

   @details
   This function returns a <b>pointer</b> to the last child of this tree item. \n
   This function calls @ref llist_last() to get the last child item, then it calls
   @ref llist_item_data() to get the pointer to the tree_item_t structure \n

   @param item the tree item you are interested in

   @return
    - a <b>pointer</b> to the tree item's last child,
    - NULL is returned if an error occured
        - item is NULL
        - item has no children
 */
tree_item_t* tree_item_lastChild(tree_item_t* item) {
    if(!item) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    llist_iterator_t* ll_item = llist_last(&item->children);
    if(!ll_item) {
        return NULL;
    }
    return llist_item_data(ll_item, tree_item_t, listItem);
}

/**
   @ingroup pcb_utils_tree
   @brief
   Get the next tree item in the list.

   @details
   This function returns a <b>pointer</b> to the next tree item in the list. \n
   This function calls @ref llist_iterator_next() to get the next tree item,  then it calls
   @ref llist_item_data() to get the pointer to the tree_item_t structure \n

   @param item the tree item you are interested in

   @return
    - a <b>pointer</b> to the next tree item,
    - NULL is returned if an error occured
        - item is NULL
        - the end of the list is reached
 */
tree_item_t* tree_item_nextSibling(const tree_item_t* item) {
    if(!item) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    llist_iterator_t* ll_item = llist_iterator_next(&item->listItem);
    if(!ll_item) {
        return NULL;
    }
    return llist_item_data(ll_item, tree_item_t, listItem);
}

/**
   @ingroup pcb_utils_tree
   @brief
   Get the previous tree item in the list.

   @details
   This function returns a <b>pointer</b> to the previous tree item in the list. \n
   This function calls @ref llist_iterator_prev() to get the previous tree item, then it calls
   @ref llist_item_data() to get the pointer to the tree_item_t structure \n

   @param item the tree item you are interested in

   @return
    - a <b>pointer</b> to the previous tree item,
    - NULL is returned if an error occured
        - item is NULL
        - the beginning of the list is reached
 */
tree_item_t* tree_item_prevSibling(const tree_item_t* item) {
    if(!item) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    llist_iterator_t* ll_item = llist_iterator_prev(&item->listItem);
    if(!ll_item) {
        return NULL;
    }
    return llist_item_data(ll_item, tree_item_t, listItem);
}

/**
   @ingroup pcb_utils_tree
   @brief
   Append a tree item to the child list.

   @details
   This function appends an existing tree item to an existing tree. If the
   tree item was allready in another tree, it will be removed.  If the item has children they are also
   moved.\n
   This function calls @ref llist_append() to add the tree item to the tree. \n
   If the item was inserted corectly the parent pointer is set, otherwise the parent pointer is reset to NULL..

   @param parent the parent tree item, to which you want to append the new child
   @param child the tree item you want to insert

   @return
    - true: The tree item was correctly inserted into the tree
    - false: An error has occurred
        - parent or child is a NULL pointer
        - child is a head or tail list element.
 */
bool tree_item_appendChild(tree_item_t* parent, tree_item_t* child) {
    if(!parent || !child) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    bool retVal = llist_append(&parent->children, &child->listItem);

    return retVal;
}

/**
   @ingroup pcb_utils_tree
   @brief
   Prepend a tree item to the child list.

   @details
   This function prepends an existing tree item to an existing tree. If the
   tree item was allready in another tree, it will be removed. If the item has children they are also
   moved.\n
   This function calls @ref llist_prepend() to add the tree item to the tree. \n
   If the item was inserted corectly the parent pointer is set, otherwise the parent pointer is reset to NULL..

   @param parent the parent tree item, to which you want to prepend the new child
   @param child the tree item you want to insert

   @return
    - true: The tree item was correctly inserted into the tree
    - false: An error has occurred
        - parent or child is a NULL pointer
        - child is a head or tail list element.
 */
bool tree_item_prependChild(tree_item_t* parent, tree_item_t* child) {
    if(!parent || !child) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    bool retVal = llist_prepend(&parent->children, &child->listItem);

    return retVal;
}

/**
   @ingroup pcb_utils_tree
   @brief
   Insert a tree item at a specific location into the child list.

   @details
   This function inserts an existing tree item at a specific location into an existing tree. If the
   tree item was allready in another tree, it will be removed.\n
   This function calls @ref llist_insertAt() to add the tree item to the tree. \n
   If the item was inserted corectly the parent pointer is set, otherwise the parent pointer is reset to NULL..

   @param parent the parent tree item, to which you want to prepend the new child
   @param index the item # where you want to insert the item (start counting from 0)
   @param child the tree item you want to insert

   @return
    - true: The tree item was correctly inserted into the tree
    - false: An error has occurred
        - parent or child is a NULL pointer
        - index does not exist
        - child is a head or tail list element.
 */
bool tree_item_insertChildAt(tree_item_t* parent, unsigned int index, tree_item_t* child) {
    if(!parent || !child) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    bool retVal = llist_insertAt(&parent->children, index, &child->listItem);

    return retVal;
}

/**
   @ingroup pcb_utils_tree
   @brief
   Detach the tree item from a tree.

   @details
   This function detaches an existing tree item from it's parent tree.\n
   All children of the item are detached from the tree as well, but are still reachable
   by using the detached item.
   This function calls @ref llist_iterator_take to remove the item from the child list. \n
   If the item is successfully detached from the list, the parent pointer is reset to NULL.\n
   Then @ref llist_item_data is called to get the tree item.

   @param child the tree item you want to detach

   @return
    - a pointer to the tree item that is detached
    - NULL if
        - child is NULL.
        - child is a head or tail of a list
 */
tree_item_t* tree_item_takeChild(tree_item_t* child) {
    if(!child) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    llist_iterator_t* ll_item = llist_iterator_take(&child->listItem);

    return llist_item_data(ll_item, tree_item_t, listItem);
}

/**
   @ingroup pcb_utils_tree
   @brief
   Detach the tree item from a tree.

   @details
   This function detaches the first tree item from it's parent tree.\n
   All children of the item are detached from the tree as well, but are still reachable
   by using the detached item.
   This function calls @ref llist_takeFirst to remove the item from the child list. \n
   If the item is successfully detached from the list, the parent pointer is reset to NULL.\n
   Then @ref llist_item_data is called to get the tree item.

   @param parent the tree item from witch the first child will be detached

   @return
    - a pointer to the tree item that is detached
    - NULL if
        - parent is NULL.
        - parent has no children
 */
tree_item_t* tree_item_takeFirstChild(tree_item_t* parent) {
    if(!parent) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    llist_iterator_t* ll_item = llist_takeFirst(&parent->children);

    tree_item_t* child = llist_item_data(ll_item, tree_item_t, listItem);
    return child;
}

/**
   @ingroup pcb_utils_tree
   @brief
   Detach the tree item from a tree.

   @details
   This function detaches the last tree item from it's parent tree.\n
   All children of the item are detached from the tree as well, but are still reachable
   by using the detached item.
   This function calls @ref llist_takeLast to remove the item from the child list. \n
   If the item is successfully detached from the list, the parent pointer is reset to NULL.\n
   Then @ref llist_item_data is called to get the tree item.

   @param parent the tree item from witch the last child will be detached

   @return
    - a pointer to the tree item that is detached
    - NULL if
        - parent is NULL.
        - parent has no children
 */
tree_item_t* tree_item_takeLastChild(tree_item_t* parent) {
    if(!parent) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    llist_iterator_t* ll_item = llist_takeLast(&parent->children);

    tree_item_t* child = llist_item_data(ll_item, tree_item_t, listItem);
    return child;
}

/**
   @ingroup pcb_utils_tree
   @brief
   The number of children contained in the tree.

   @details
   This function returns the number of children in a tree.\n
   This function calls @ref llist_size\n

   @param parent the tree item from witch the number of children is returned

   @return
    - the number of children.
    - 0 when the tree item has no children
    - 0 when the parent is NULL.
 */
unsigned int tree_item_childCount(tree_item_t* parent) {
    if(!parent) {
        pcb_error = pcb_error_invalid_parameter;
        return 0;
    }
    return llist_size(&parent->children);
}

/**
   @ingroup pcb_utils_tree
   @brief
   Check the a tree item has children..

   @details
   This function checks that the tree item has children or not.\n
   This function calls @ref llist_isEmpty\n

   @param parent the parent tree item

   @return
    - true: the tree item has children
    - false
        - the tree item has no children
        - parent is NULL.
 */
bool tree_item_hasChildren(const tree_item_t* parent) {
    if(!parent) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }
    return !llist_isEmpty(&parent->children);
}
