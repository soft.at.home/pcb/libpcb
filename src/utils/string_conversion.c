/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#define _DEFAULT_SOURCE /* Needed to stop glibc>=2.19 from warning about _BSD_SOURCE */
#define _BSD_SOURCE     /* needed for timegm() */
#define _GNU_SOURCE
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <stdarg.h>
#include <inttypes.h>
#include <ctype.h>
#include <limits.h>

#include <debug/sahtrace.h>

#include <pcb/common/error.h>
#include <pcb/utils/string.h>
#include <pcb/utils/datetime.h>

static inline char* string_resize(string_t* data, size_t newsize) {
    if(data->bufferSize > newsize) {
        return data->buffer;
    }

    char* temp = (char*) realloc(data->buffer, newsize);
    if(!temp) {
        return NULL;
    }

    data->buffer = temp;
    return temp;
}

static bool string_convert_signed(const string_t* string, long long* converted) {
    *converted = 0;
    pcb_error = pcb_error_invalid_value;

    if(string_isNull(string)) {
        return false;
    }

    char* buffer = string->buffer;
    char* endptr = NULL;
    long long result = 0;

    while(isspace(*buffer)) {
        buffer += 1;
    }

    if(!isdigit(*buffer) && (*buffer != '+') && (*buffer != '-')) {
        return false;
    }

    errno = 0;
    result = strtoll(buffer, &endptr, 0);

    if(((errno == ERANGE) && ((result == LLONG_MIN) || (result == LLONG_MAX))) ||
       ((errno != 0) && (result == 0))) {
        return false;
    }
    if(endptr == buffer) {
        return false;
    }
    if(*endptr != '\0') {
        return false;
    }

    SAH_TRACE_INFO("Conversion = %llu", result);
    *converted = result;
    pcb_error = pcb_ok;
    return true;
}

static bool string_convert_unsigned(const string_t* string, unsigned long long* converted) {
    *converted = 0;
    pcb_error = pcb_error_invalid_value;

    if(string_isNull(string)) {
        return false;
    }

    char* buffer = string->buffer;
    char* endptr = NULL;
    unsigned long long result = 0;

    while(isspace(*buffer)) {
        buffer += 1;
    }

    if(!isdigit(*buffer) && (*buffer != '-')) {
        return false;
    }

    errno = 0;
    result = strtoull(buffer, &endptr, 0);

    if(((errno == ERANGE) && ((result == 0) || (result == ULLONG_MAX))) ||
       ((errno != 0) && (result == 0))) {
        return false;
    }
    if(endptr == buffer) {
        return false;
    }
    if(*endptr != '\0') {
        return false;
    }

    *converted = result;
    pcb_error = pcb_ok;
    return true;
}

static bool string_convert_double(const string_t* string, double* converted) {
    *converted = 0;
    pcb_error = pcb_error_invalid_value;

    if(string_isNull(string)) {
        return false;
    }

    char* buffer = string->buffer;
    char* endptr = NULL;
    double result = 0;

    while(isspace(*buffer)) {
        buffer += 1;
    }

    if(!isdigit(*buffer) && (*buffer != '-')) {
        return false;
    }

    errno = 0;
    result = strtod(buffer, &endptr);

    if(((errno == ERANGE) && (result == 0)) ||
       ((errno != 0) && (result == 0))) {
        return false;
    }
    if(endptr == buffer) {
        return false;
    }
    if(*endptr != '\0') {
        return false;
    }

    *converted = result;
    pcb_error = pcb_ok;
    return true;
}

/**
   @ingroup pcb_utils_string
   @brief
   Copy a string.

   @details
   This function creates a copy of the string\n

   If the destination buffer size can not contain the full string, the buffer is re-allocated\n

   @param dest the string will contain the copy
   @param data the source string that will be copied

   @return
    - Number of bytes copied.
 */
size_t string_copy(string_t* dest, const string_t* data) {
    if(!data) {
        string_clear(dest);
        return 0;
    }
    return string_fromChar(dest, data->buffer);
}

/**
   @ingroup pcb_utils_string
   @brief
   Create a string.from a bool

   @details
   This function creates string from a bool. The string buffer is alway re-allocated\n

   @param string the string will contain the bool (="1" for true, "0" for false)
   @param data the bool that has to be converted to a string

   @return
    - length of the new string
 */
size_t string_fromBool(string_t* string, bool data) {
    if(!string) {
        pcb_error = pcb_error_invalid_parameter;
        return 0;
    }

    string_cleanup(string);
    string_initialize(string, 2);
    return sprintf(string->buffer, "%i", data ? 1 : 0);
}

/**
   @ingroup pcb_utils_string
   @brief
   Create a string from a char array

   @details
   This function creates string from a char array. A copy is taken from the char array\n

   @param string the string will contain the copy
   @param data the char array that will be copied

   @return
    - Number of bytes copied.
 */
size_t string_fromChar(string_t* string, const char* data) {
    if(!string) {
        pcb_error = pcb_error_invalid_parameter;
        return 0;
    }

    if(!data) {
        pcb_error = pcb_error_invalid_parameter;
        string_clear(string);
        return 0;
    } else {
        size_t neededSize = strlen(data) + 1;
        if(string->bufferSize < neededSize) {
            char* temp = (char*) realloc(string->buffer, neededSize);
            if(!temp) {
                string_clear(string);
                return 0;
            }
            string->buffer = temp;
            string->bufferSize = neededSize;
        }
        strncpy(string->buffer, data, string->bufferSize);
        return neededSize - 1;
    }

    return 0;
}

/**
   @ingroup pcb_utils_string
   @brief
   Create a string.from an int8

   @details
   This function creates string from an int8.\n

   @param string the string will contain the converted number
   @param data the number that has to be converted to a string

   @return
    - length of the new string
 */
size_t string_fromInt8(string_t* string, int8_t data) {
    if(!string) {
        pcb_error = pcb_error_invalid_parameter;
        return 0;
    }

    string_cleanup(string);
    string_initialize(string, 5);
    return sprintf(string->buffer, "%i", data);
}

/**
   @ingroup pcb_utils_string
   @brief
   Create a string.from an int16

   @details
   This function creates string from an int16.\n

   @param string the string will contain the converted number
   @param data the number that has to be converted to a string

   @return
    - length of the new string
 */
size_t string_fromInt16(string_t* string, int16_t data) {
    if(!string) {
        pcb_error = pcb_error_invalid_parameter;
        return 0;
    }

    string_cleanup(string);
    string_initialize(string, 7);
    return sprintf(string->buffer, "%i", data);
}

/**
   @ingroup pcb_utils_string
   @brief
   Create a string.from an int32

   @details
   This function creates string from an int32.\n

   @param string the string will contain the converted number
   @param data the number that has to be converted to a string

   @return
    - length of the new string
 */
size_t string_fromInt32(string_t* string, int32_t data) {
    if(!string) {
        pcb_error = pcb_error_invalid_parameter;
        return 0;
    }

    string_cleanup(string);
    string_initialize(string, 12);
    return sprintf(string->buffer, "%i", data);
}

/**
   @ingroup pcb_utils_string
   @brief
   Create a string.from an unsigned int64

   @details
   This function creates string from an int64.\n

   @param string the string will contain the converted number
   @param data the number that has to be converted to a string

   @return
    - length of the new string
 */
size_t string_fromInt64(string_t* string, int64_t data) {
    if(!string) {
        pcb_error = pcb_error_invalid_parameter;
        return 0;
    }

    string_cleanup(string);
    string_initialize(string, 22);
    return sprintf(string->buffer, "%" PRId64, data);
}

/**
   @ingroup pcb_utils_string
   @brief
   Create a string.from an unsigned int8

   @details
   This function creates string from an unsigned int8. \n

   @param string the string will contain the converted number
   @param data the number that has to be converted to a string

   @return
    - length of the new string
 */
size_t string_fromUInt8(string_t* string, uint8_t data) {
    if(!string) {
        pcb_error = pcb_error_invalid_parameter;
        return 0;
    }

    if(!string_resize(string, 5)) {
        return 0;
    }

    return sprintf(string->buffer, "%u", data);
}

/**
   @ingroup pcb_utils_string
   @brief
   Create a string.from an unsigned int16

   @details
   This function creates string from an unsigned int16.\n

   @param string the string will contain the converted number
   @param data the number that has to be converted to a string

   @return
    - length of the new string
 */
size_t string_fromUInt16(string_t* string, uint16_t data) {
    if(!string) {
        pcb_error = pcb_error_invalid_parameter;
        return 0;
    }

    if(!string_resize(string, 7)) {
        return 0;
    }

    return sprintf(string->buffer, "%u", data);
}

/**
   @ingroup pcb_utils_string
   @brief
   Create a string.from an unsigned int32

   @details
   This function creates string from an unsigned int32.\n

   @param string the string will contain the converted number
   @param data the number that has to be converted to a string

   @return
    - length of the new string
 */
size_t string_fromUInt32(string_t* string, uint32_t data) {
    if(!string) {
        pcb_error = pcb_error_invalid_parameter;
        return 0;
    }

    if(!string_resize(string, 12)) {
        return 0;
    }

    return sprintf(string->buffer, "%u", data);
}

/**
   @ingroup pcb_utils_string
   @brief
   Create a string.from an unsigned int64

   @details
   This function creates string from an unsigned int64.\n

   @param string the string will contain the converted number
   @param data the number that has to be converted to a string

   @return
    - length of the new string
 */
size_t string_fromUInt64(string_t* string, uint64_t data) {
    if(!string) {
        pcb_error = pcb_error_invalid_parameter;
        return 0;
    }

    if(!string_resize(string, 22)) {
        return 0;
    }

    return sprintf(string->buffer, "%" PRId64, data);
}

/**
   @ingroup pcb_utils_string
   @brief
   Create a string.from a double

   @details
   This function creates string from a double.\n

   @param string the string will contain the converted number
   @param data the number that has to be converted to a string

   @return
    - length of the new string
 */
size_t string_fromDouble(string_t* string, double data) {
    if(!string) {
        pcb_error = pcb_error_invalid_parameter;
        return 0;
    }

    if(!string_resize(string, 22)) {
        return 0;
    }

    return sprintf(string->buffer, "%e", data);
}
/**
   @ingroup pcb_utils_string
   @brief
   Create a string.from an time date structure

   @details
   This function creates string from time_t structure.\n

   @param string the string will contain the converted time/date
   @param data the structure containing the time/date data
   @param format the string format to which the time/date has to be converted,
   more information can about this can be found in datetime.h. If this parameter is null, a default
   datetimeformat will be used ("%Y-%m-%dT%H:%M:%SZ").
   @see string_fromTimeExtended
   @see pcb_strftime

   @return
    - length of the new string
    - 0 if string or data are NULL pointers
    - 0 if memory allocation failed
 */
size_t string_fromTime(string_t* string, const struct tm* data, const char* format) {
    if(!string || !data) {
        pcb_error = pcb_error_invalid_parameter;
        return 0;
    }

    pcb_datetime_t dt;
    memcpy(&dt.datetime, data, sizeof dt.datetime);
    dt.nanoseconds = 0;

    return string_fromTimeExtended(string, &dt, format);
}

/**
   @ingroup pcb_utils_string
   @brief
   Create a string from an time date structure

   @details
   This function creates a string from a pcb_datetime_t structure.\n
   The default string format generated will conform to the ISO8601 format, with 6 decimals for the second (i.e. microsecond precision),
    unless the subsecond part of the timestamp is zero in which case the seconds part of the string will be an integer.
   Example output format: 2019-03-03T06:48:12.371052Z

   @param string the string will contain the converted time/date
   @param data the structure containing the time/date data
   @param format the format to use. See strftime(3) for most specifiers.
    On top of those, we also support %f (microseconds), %N (nanoseconds) and %i (fractional seconds)
    @see pcb_strftime

   @return
    - length of the new string
    - 0 if string or data are NULL pointers
    - 0 if memory allocation failed
    - 0 if no part of the input string could be matched to the given format (pcb_error is set to pcb_error_invalid_value in that case)
 */
size_t string_fromTimeExtended(string_t* string, const pcb_datetime_t* data, const char* format) {
    char tempBuffer[256] = "";
    size_t size = 0;

    if(!string || !data) {
        pcb_error = pcb_error_invalid_parameter;
        return 0;
    }
    if(!format) {
        format = data->nanoseconds != 0 ? ISO8601_DATE_TIME_FMT_US : ISO8601_DATE_TIME_FMT;
    }

    size = pcb_strftime(tempBuffer, sizeof tempBuffer, format, data);
    if((size == 0) && *format) {
        pcb_error = pcb_error_invalid_value;
        return 0;
    }

    string_cleanup(string);
    if(!string_initialize(string, size + 1)) {
        return 0;
    }

    return string_fromChar(string, tempBuffer);

}

/**
   @ingroup pcb_utils_string
   @brief
   Convert the string to a bool.

   @details
   Parses the string buffer interpreting its content as a bool.\n
   If the string is equal to one of the following strings: (not case sensitive)
    - "1"
    - "yes"
    - "true"
   the function will convert it to true.

   If the string is equal to one of the following strings:  (not case sensitive)
    - "0"
    - "no"
    - "false"
   the function will convert it to false

   If the conversion fails the function will return the default conversion value: false.

   If a pointer to a bool is provided, it will set to true or false depending on the success
   of the value conversion.

   If the conversion fails, pcb_error is set to pcb_error_invalid_value

   @param string the string that has to be converted
   @param success (optional) pointer to a bool, that will be set to true or false, depending the success of the conversion

   @return
   - true if the string contains one of the following:\n
      @li true (case insensitive)
      @li yes (case insensitive)
      @li 1
   - false if the string contains one of the following:\n
      @li false (case insensitive)
      @li no (case insensitive)
      @li 1
   - if the conversion fails, false is returned
 */
bool string_convertToBool(const string_t* string, bool* success) {
    bool value = false;

    if(string_isNull(string)) {
        pcb_error = pcb_error_invalid_value;
        goto exit;
    }

    if(!string_compareChar(string, "1", string_case_insensitive) ||
       !string_compareChar(string, "yes", string_case_insensitive) ||
       !string_compareChar(string, "true", string_case_insensitive)) {
        value = true;
        pcb_error = pcb_ok;
    } else if(!string_compareChar(string, "0", string_case_insensitive) ||
              !string_compareChar(string, "no", string_case_insensitive) ||
              !string_compareChar(string, "false", string_case_insensitive)) {
        pcb_error = pcb_ok;
    } else {
        pcb_error = pcb_error_invalid_value;
    }

exit:
    if(success) {
        *success = (pcb_error == pcb_ok) ? true : false;
    }

    /* default conversion: false */
    return value;
}

char* string_toChar(const string_t* string) {
    if(string_isNull(string)) {
        return NULL;
    }

    size_t size = strlen(string->buffer) + 1;
    char* retVal = calloc(1, size);
    if(retVal) {
        strcpy(retVal, string->buffer);
    } else {
        pcb_error = pcb_error_no_memory;
    }
    return retVal;
}

/**
   @ingroup pcb_utils_string
   @brief
   Convert the string to an int8.

   @details
   Parses the string buffer interpreting its content as an integral number (8 bit)\n
   The string may begin with an arbitrary amount of white space (as determined by isspace(3)) followed by a
   single optional '+' or '-' sign.

   @param string the string that has to be converted
   @param success (optional) pointer to a bool, that will be set to true or false, depending the success of the conversion

   @return
    - The int8 representation of the string
    - If the conversion fails 0 is returned and success is set to false (if a pointer was provided)
 */
int8_t string_convertToInt8(const string_t* string, bool* success) {
    int8_t value = 0;
    long long converted_value = 0;

    /* convert the string to a signed long */
    bool converted = string_convert_signed(string, &converted_value);

    /* verify overflow or underflow */
    if((converted_value > INT8_MAX) ||
       (converted_value < INT8_MIN)) {
        converted = false;
    }

    /* set the success value if a not null pointer is provided */
    if(success) {
        *success = converted;
    }

    /* cast to 8 bit integer, when conversion was successfull */
    if(converted) {
        value = (int8_t) converted_value;
    }

    return value;
}

/**
   @ingroup pcb_utils_string
   @brief
   Convert the string to an int16.

   @details
   Parses the string buffer interpreting its content as an integral number (16 bit)\n
   The string may begin with an arbitrary amount of white space (as determined by isspace(3)) followed by a
   single optional '+' or '-' sign.

   @param string the string that has to be converted
   @param success (optional) pointer to a bool, that will be set to true or false, depending the success of the conversion

   @return
    - The int16 representation of the string
    - If the conversion fails 0 is returned and success is set to false (if a pointer was provided)
 */
int16_t string_convertToInt16(const string_t* string, bool* success) {
    int16_t value = 0;
    long long converted_value = 0;

    /* convert the string to a signed ling */
    bool converted = string_convert_signed(string, &converted_value);

    /* verify overflow or underflow */
    if((converted_value > INT16_MAX) ||
       (converted_value < INT16_MIN)) {
        converted = false;
    }

    /* set the success value if a not null pointer is provided */
    if(success) {
        *success = converted;
    }

    /* cast to 16 bit integer, when conversion was successfull */
    if(converted) {
        value = (int16_t) converted_value;
    }

    return value;
}

/**
   @ingroup pcb_utils_string
   @brief
   Convert the string to an int32.

   @details
   Parses the string buffer interpreting its content as an integral number (32 bit)\n
   The string may begin with an arbitrary amount of white space (as determined by isspace(3)) followed by a
   single optional '+' or '-' sign.

   @param string the string that has to be converted
   @param success (optional) pointer to a bool, that will be set to true or false, depending the success of the conversion

   @return
    - The int32 representation of the string
    - If the conversion fails 0 is returned and success is set to false (if a pointer was provided)
 */
int32_t string_convertToInt32(const string_t* string, bool* success) {
    int32_t value = 0;
    long long converted_value = 0;

    /* convert the string to a signed ling */
    bool converted = string_convert_signed(string, &converted_value);

    /* verify overflow or underflow */
    if((converted_value > INT32_MAX) ||
       (converted_value < INT32_MIN)) {
        converted = false;
    }

    /* set the success value if a not null pointer is provided */
    if(success) {
        *success = converted;
    }

    /* cast to 32 bit integer, when conversion was successfull */
    if(converted) {
        value = (int32_t) converted_value;
    }

    return value;
}

/**
   @ingroup pcb_utils_string
   @brief
   Convert the string to an int64

   @details
   Parses the string buffer interpreting its content as an integral number (64 bit)\n
   The string may begin with an arbitrary amount of white space (as determined by isspace(3)) followed by a
   single optional '+' or '-' sign.

   @param string the string that has to be converted
   @param success (optional) pointer to a bool, that will be set to true or false, depending the success of the conversion

   @return
    - The int64 representation of the string
    - If the conversion fails 0 is returned and success is set to false (if a pointer was provided)
 */
int64_t string_convertToInt64(const string_t* string, bool* success) {
    int64_t value = 0;
    long long converted_value = 0;

    /* convert the string to a signed ling */
    bool converted = string_convert_signed(string, &converted_value);

    /* verify overflow or underflow */
    if((converted_value > INT64_MAX) ||
       (converted_value < INT64_MIN)) {
        converted = false;
    }

    /* set the success value if a not null pointer is provided */
    if(success) {
        *success = converted;
    }

    /* cast to 64 bit integer, when conversion was successfull */
    if(converted) {
        value = (int64_t) converted_value;
    }

    return value;
}

/**
   @ingroup pcb_utils_string
   @brief
   Convert the string to an uint8.

   @details
   Parses the string buffer interpreting its content as an unsigned integral number (8 bit)\n
   The string may begin with an arbitrary amount of white space (as determined by isspace(3)) followed by a
   single optional '+' sign.

   @param string the string that has to be converted
   @param success (optional) pointer to a bool, that will be set to true or false, depending the success of the conversion

   @return
    - The uint8 representation of the string
    - If the conversion fails 0 is returned and success is set to false (if a pointer was provided)
 */
uint8_t string_convertToUInt8(const string_t* string, bool* success) {
    uint8_t value = 0;
    unsigned long long converted_value = 0;

    /* convert the string to a signed long */
    bool converted = string_convert_unsigned(string, &converted_value);

    /* verify overflow */
    if(converted_value > UINT8_MAX) {
        converted = false;
    }

    /* set the success value if a not null pointer is provided */
    if(success) {
        *success = converted;
    }

    /* cast to 8 bit integer, when conversion was successfull */
    if(converted) {
        value = (uint8_t) converted_value;
    }

    return value;
}

/**
   @ingroup pcb_utils_string
   @brief
   Convert the string to an uint16.

   @details
   Parses the string buffer interpreting its content as an unsigned integral number (16 bit)\n
   The string may begin with an arbitrary amount of white space (as determined by isspace(3)) followed by a
   single optional '+' sign.

   @param string the string that has to be converted
   @param success (optional) pointer to a bool, that will be set to true or false, depending the success of the conversion

   @return
    - The uint16 representation of the string
    - If the conversion fails 0 is returned and success is set to false (if a pointer was provided)
 */
uint16_t string_convertToUInt16(const string_t* string, bool* success) {
    uint16_t value = 0;
    unsigned long long converted_value = 0;

    /* convert the string to a signed ling */
    bool converted = string_convert_unsigned(string, &converted_value);

    /* verify overflow */
    if(converted_value > UINT16_MAX) {
        converted = false;
    }

    /* set the success value if a not null pointer is provided */
    if(success) {
        *success = converted;
    }

    /* cast to 16 bit integer, when conversion was successfull */
    if(converted) {
        value = (uint16_t) converted_value;
    }

    return value;
}

/**
   @ingroup pcb_utils_string
   @brief
   Convert the string to an uint32.

   @details
   Parses the string buffer interpreting its content as an unsigned integral number (32 bit)\n
   The string may begin with an arbitrary amount of white space (as determined by isspace(3)) followed by a
   single optional '+' sign.

   @param string the string that has to be converted
   @param success (optional) pointer to a bool, that will be set to true or false, depending the success of the conversion

   @return
    - The uint32 representation of the string
    - If the conversion fails 0 is returned and success is set to false (if a pointer was provided)
 */
uint32_t string_convertToUInt32(const string_t* string, bool* success) {
    uint32_t value = 0;
    unsigned long long converted_value = 0;

    /* convert the string to a signed ling */
    bool converted = string_convert_unsigned(string, &converted_value);

    /* verify overflow */
    if(converted_value > UINT32_MAX) {
        converted = false;
    }

    /* set the success value if a not null pointer is provided */
    if(success) {
        *success = converted;
    }

    /* cast to 32 bit integer, when conversion was successfull */
    if(converted) {
        value = (uint32_t) converted_value;
    }

    return value;
}

/**
   @ingroup pcb_utils_string
   @brief
   Convert the string to an uint64.

   @details
   Parses the string buffer interpreting its content as an unsigned integral number (64 bit)\n
   The string may begin with an arbitrary amount of white space (as determined by isspace(3)) followed by a
   single optional '+' sign.

   @param string the string that has to be converted
   @param success (optional) pointer to a bool, that will be set to true or false, depending the success of the conversion

   @return
    - The uint64 representation of the string
    - If the conversion fails 0 is returned and success is set to false (if a pointer was provided)
 */
uint64_t string_convertToUInt64(const string_t* string, bool* success) {
    uint64_t value = 0;
    unsigned long long converted_value = 0;

    /* convert the string to a signed ling */
    bool converted = string_convert_unsigned(string, &converted_value);

    /* verify overflow or underflow */
    if(converted_value > INT64_MAX) {
        converted = false;
    }

    /* set the success value if a not null pointer is provided */
    if(success) {
        *success = converted;
    }

    /* cast to 64 bit integer, when conversion was successfull */
    if(converted) {
        value = (uint64_t) converted_value;
    }

    return value;
}

double string_convertToDouble(const string_t* string, bool* success) {
    double converted_value = 0;

    /* convert the string to a signed ling */
    bool converted = string_convert_double(string, &converted_value);

    /* set the success value if a not null pointer is provided */
    if(success) {
        *success = converted;
    }

    return converted_value;
}



static bool string_toTime_internal(const string_t* string, const char* format, pcb_datetime_t* dateTime) {
    if(string_isNull(string)) {
        return false;
    }

    if(!format) {
        format = string_containsChar(string, ".", string_case_sensitive) ? ISO8601_DATE_TIME_FMT_US : ISO8601_DATE_TIME_FMT;
    }

    pcb_datetime_initialize(dateTime, PCB_EPOCH_TR098);

    // Special case "0" -- return default datetime
    if(string_compareChar(string, "0", string_case_sensitive) == 0) {
        return true;
    }

    char* rv = pcb_strptime(string->buffer, format, dateTime);

    if(rv == NULL) {
        // Format not matched
        return false;
    }

    return true;
}
/**
   @ingroup pcb_utils_string
   @brief
   Convert the string to a time_t struct

   @details
   Parses the string buffer interpreting its content as a data\n
   The caller of this routine becomes owner of the returned tm pointer and is responsible for freeing the memory.

   @param string the string that has to be converted
   @param format the time/date format in that string,
   more information can about this can be found in datetime.h. If this parameter is null, a default
   datetimeformat will be used ("%Y-%m-%dT%H:%M:%SZ").

   @return
   - NULL if string is NULL or no more memory free
   - a pointer to a correct time_t structure
 */
struct tm* string_toTime(const string_t* string, const char* format) {
    pcb_datetime_t tmp;
    struct tm* dateTime = NULL;

    dateTime = calloc(1, sizeof(struct tm));
    if(dateTime == NULL) {
        pcb_error = pcb_error_no_memory;
        return NULL;
    }

    if(!string_toTime_internal(string, format, &tmp)) {
        free(dateTime);
        return NULL;
    }
    *dateTime = tmp.datetime;

    return dateTime;
}

/**
   @ingroup pcb_utils_string
   @brief
   Convert the string to a date time structure.

   @details
   Parses the string buffer interpreting its content as a data\n
   The caller of this routine becomes owner of the returned pcb_datetime_t pointer and is responsible for freeing the memory.
   This function is identical to @ref string_toTime, except that it attempts to parse time in sub-second resolution.
   It does this by assuming the seconds are specified as a real number,
   i.e. the seconds part in "2018-07-01T10:11:12.3456Z" is 12.3456, so nanoseconds becomes 345600000

   @param string the string that has to be converted
   @param format the time/date format in that string,
   more information can about this can be found in datetime.h. If this parameter is null, a default
   datetimeformat will be used ("%Y-%m-%dT%H:%M:%SZ") or "%Y-%m-%dT%H:%M:%S.%fZ", depending on if nanoseconds != 0.

   @return
   - NULL if string is NULL or no more memory free or the conversion failed for some reason
   - a pointer to a correct pcb_datetime_t structure
 */
pcb_datetime_t* string_toTimeExtended(const string_t* string, const char* format) {
    struct pcb_datetime_t* dateTime = NULL;

    dateTime = calloc(1, sizeof(struct pcb_datetime_t));
    if(dateTime == NULL) {
        pcb_error = pcb_error_no_memory;
        return NULL;
    }

    if(!string_toTime_internal(string, format, dateTime)) {
        free(dateTime);
        return NULL;
    }

    return dateTime;
}
