/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#define _GNU_SOURCE
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <stdarg.h>
#include <assert.h>

#include <pcb/common/error.h>
#include <pcb/utils/circular_buffer.h>

typedef ssize_t (* readerhook)(void* circbuf, char* buffer, size_t size);

static inline ssize_t max(ssize_t a, ssize_t b) {
    return (a > b ? a : b);
}

static inline ssize_t min(ssize_t a, ssize_t b) {
    return (a > b ? b : a);
}

#ifndef NDEBUG
static void circbuf_dump(circbuf_t* cb) {
    printf("[");
    fwrite(cb->buffer, sizeof(char), cb->size, stdout);
    printf("]\n");
    if(cb->read < cb->write) {
        printf(" % *s% *s\n", cb->read - cb->buffer, "r", cb->write - cb->read, "w");
    } else if(cb->read > cb->write) {
        printf(" % *s% *s\n", cb->write - cb->buffer, "w", cb->read - cb->write, "r");
    } else {
        printf(" % *s\n", cb->write - cb->buffer, "b");
    }
}

#define assert_circbuf(CB) circbuf_isValid(CB, __FILE__, __LINE__)
static void circbuf_isValid(circbuf_t* cb, const char* file, int line) {
    /* circbuf_dump(cb); */
    assert(cb->buffer);
    assert(cb->size > 0);
    assert(cb->read);
    assert(cb->toRead >= 0);
    assert(cb->write);
    assert(cb->toWrite >= 0);
    assert(cb->size >= cb->toRead + cb->toWrite);
    assert(cb->buffer + cb->size > cb->read);
    assert(cb->buffer <= cb->read);
    assert(cb->buffer + cb->size > cb->write);
    assert(cb->buffer <= cb->write);
}
#else
#define assert_circbuf(CB) ((void) 0)
#endif

static ssize_t circbuf_resize(circbuf_t* cb, size_t size) {
    assert_circbuf(cb);

    /* store the offset of the read/write pointer to restore them */
    off_t read = cb->read - cb->buffer;
    off_t write = cb->write - cb->buffer;

    /* we might need to move the read pointer if it is after the write */
    bool readIsAfterWrite = (read > write) || (read == write && cb->toRead > 0);

    /* allocate more memory */
    char* newBuffer = realloc(cb->buffer, cb->size + size);
    if(newBuffer == NULL) {
        return -1;
    }

    /* restore state */
    cb->buffer = newBuffer;
    cb->write = cb->buffer + write;
    cb->read = cb->buffer + read;

    /* we might need to move the read pointer if it was after the write */
    if(readIsAfterWrite) {
        /* move the data that we still need to read */
        cb->read = memmove(cb->read + size, cb->read, cb->size - read);
    }

    /* we now have more space to write to */
    cb->size += size;
    cb->toWrite += size;

    assert_circbuf(cb);

    return cb->size;
}

static ssize_t circbuf_write(void* cookie, const char* buffer, size_t size) {
    circbuf_t* cb = (circbuf_t*) cookie;
    clearerr(cb->stream);
    errno = 0;
    ssize_t result = circbuf_writeBuffer(cb, buffer, size);
#ifdef __UCLIBC__
    return result;
#else
    return result < 0 ? 0 : result;
#endif
}

/**
 * when seeking on a circular buffer for read, the beginning and the end are
 * at the write pointer. When seeking a write buffer, the beginning and the end are
 * at the read pointer. Moving further thant the beginning or end of the buffer WON't
 * resize it.
 */
static int circbuf_seek(void* cookie, off64_t* position, int whence) {
    circbuf_t* cb = (circbuf_t*) cookie;
    char** seek = NULL, * bound = NULL;
    size_t* toSeek = NULL, * toBound = NULL;

    assert_circbuf(cb);

    /* check which pointer we have to move */
    switch(cb->seekMode) {
    case 'r':
        seek = &cb->read;
        toSeek = &cb->toRead;
        bound = cb->write;
        toBound = &cb->toWrite;
        break;
    case 'a':
    case 'w':
        seek = &cb->write;
        toSeek = &cb->toWrite;
        bound = cb->read;
        toBound = &cb->toRead;
        break;
    default:
        errno = EINVAL;
        return -1;
        break;
    }

    /* seek from ... */
    switch(whence) {
    case SEEK_SET: /* ... the beginning */
        /* set position to the beginning */
        *seek = bound;
        *toSeek = cb->size;
        *toBound = 0;
        break;

    case SEEK_END: /* ... the end */
        /* set position to the end */
        *seek = bound;
        *toSeek = 0;
        *toBound = cb->size;
        break;
    default:
        /* do nothing */
        break;
    }

    int8_t sign = (*position > 0) - (*position < 0);
    off64_t moved = 0, toMove = *position;

    /* going backward */
    if(sign < 0) {
        toMove *= sign;

        /* check that we are not crossing the bound pointer */
        if(toMove > (off64_t) (cb->size - *toSeek)) {
            toMove = cb->size - *toSeek;
        }

        if(*seek - toMove < cb->buffer) {
            /* crossing the boundary */
            moved += *seek - cb->buffer;
            toMove -= moved;
            *seek = cb->buffer + cb->size;
            *toSeek += moved;
            *toBound -= moved;
        }

        *seek -= toMove;
        moved += toMove;
        *toSeek += toMove;
        *toBound -= toMove;
    } else {
        /* check that we are not crossing the bound pointer */
        if(toMove > (off64_t) *toSeek) {
            toMove = *toSeek;
        }

        if(*seek + toMove >= cb->buffer + cb->size) {
            /* crossing the boundary */
            moved += cb->buffer + cb->size - *seek;
            toMove -= moved;
            *seek = cb->buffer;
            *toSeek -= moved;
            *toBound += moved;
        }

        *seek += toMove;
        moved += toMove;
        *toSeek -= toMove;
        *toBound += toMove;
    }

    assert_circbuf(cb);

    /* we now need to get the absolute position */
    *position = *seek - cb->buffer;

    return 0;
}

static int circbuf_close(void* cookie) {
    circbuf_t* cb = (circbuf_t*) cookie;
    cb->stream = NULL;
    return 1;
}

/**
   @ingroup pcb_utils_buffers_circbuf
   @brief
   Initialize the circular buffer.

   @details
   Initialize the circular buffer using the given size

   @param circbuf Pointer to the circular buffer structure
   @param size The initial size of the circular buffer

   @return
    - true the circular buffer is initialized and ready to use.
    - false no more memory available
 */
bool circbuf_initialize(circbuf_t* circbuf, size_t size) {
    circbuf_t* cb = (circbuf_t*) circbuf;

    cb->stream = NULL;

    cb->buffer = calloc(sizeof(char), size);
    if(cb->buffer == NULL) {
        errno = ENOMEM;
        goto error;
    }

    /* initialize buffer state */
    cb->read = cb->buffer;
    cb->write = cb->buffer;
    cb->size = size;
    cb->toRead = 0;
    cb->toWrite = size;

    assert_circbuf(cb);
    return true;

error:
    return false;
}

/**
   @ingroup pcb_utils_buffers_circbuf
   @brief
   Cleanup resources.

   @details
   Close the stream and free the allocated resources

   @param circbuf Pointer to the circular buffer structure
 */
void circbuf_cleanup(circbuf_t* circbuf) {
    circbuf_t* cb = (circbuf_t*) circbuf;
    if(cb->stream) {
        fclose(cb->stream);
    }
    free(cb->buffer);
}

/**
   @ingroup pcb_utils_buffers_circbuf
   @brief
   Empty and resize the buffer.

   @details
   Empty the buffer and resize using the given size.

   @param circbuf Pointer to the circular buffer structure
   @param size New size of the buffer
 */
void circbuf_reset(circbuf_t* circbuf, size_t size) {
    circbuf_t* cb = (circbuf_t*) circbuf;

    if(size && (size != cb->size)) {
        char* newBuffer = realloc(cb->buffer, size);
        if(newBuffer != NULL) {
            cb->buffer = newBuffer;
        }
    }

    cb->read = cb->buffer;
    cb->write = cb->buffer;
    cb->size = size;
    cb->toRead = 0;
    cb->toWrite = size;
    assert_circbuf(cb);
}

/**
   @ingroup pcb_utils_buffers_circbuf
   @brief
   Return the stream pointer.

   @details
   Return the stream pointer.

   @param circbuf Pointer to the circular buffer structure

   @return
    - returns the file pointer.
 */
FILE* circbuf_stream(circbuf_t* circbuf) {
    return ((circbuf_t*) circbuf)->stream;
}

/**
   @ingroup pcb_utils_buffers_circbuf
   @brief
   Return the total allocated size for the buffer

   @details
   Return the total allocated size for the buffer.

   @param circbuf Pointer to the circular buffer structure

   @return
    - returns the current size of the buffer.
 */
ssize_t circbuf_size(circbuf_t* circbuf) {
    return ((circbuf_t*) circbuf)->size;
}

/**
   @ingroup pcb_utils_buffers_circbuf
   @brief
   Return the bytes available for reading as contiguous memory

   @details
   Return the bytes available for reading as contiguous memory

   @param circbuf Pointer to the circular buffer structure

   @return
    - the number of bytes available for reading.
 */
ssize_t circbuf_availableForRead(circbuf_t* circbuf) {
    circbuf_t* cb = (circbuf_t*) circbuf;
    if(!cb) {
        return 0;
    }
    ssize_t leftUntilBoundary = cb->buffer + cb->size - cb->read;

    return min(leftUntilBoundary, cb->toRead);
}

/**
   @ingroup pcb_utils_buffers_circbuf
   @brief
   Return a buffer for reading

   @details
   Return a buffer for reading

   @param circbuf Pointer to the circular buffer structure

   @return
    - pointer to the read bufer.
 */
void* circbuf_readBuffer(circbuf_t* circbuf) {
    return ((circbuf_t*) circbuf)->read;
}

/**
   @ingroup pcb_utils_buffers_circbuf
   @brief
   Reads data from the buffer

   @details
   Reads data from the buffer

   @param circbuf Pointer to the circular buffer structure
   @param buffer Allocated buffer that will contain the read data
   @param size Maximum size of the data buffer

   @return
    - Number of bytes read
 */
ssize_t circbuf_read(circbuf_t* circbuf, char* buffer, size_t size) {
    circbuf_t* cb = circbuf;
    assert_circbuf(cb);

    /* EOF is when the readPos and writePos matches */
    if(cb->toRead == 0) {
        return 0;
    }
    ssize_t toRead = min(size, cb->toRead);
    ssize_t read = 0;

    /* read until boundary */
    ssize_t leftToBound = cb->buffer + cb->size - cb->read;
    if(toRead >= leftToBound) {
        memcpy(buffer, cb->read, leftToBound);
        cb->read = cb->buffer;
        read += leftToBound;
    }

    memcpy(buffer + read, cb->read, toRead - read);
    cb->toWrite += toRead;
    cb->toRead -= toRead;
    cb->read += toRead - read;

    assert_circbuf(cb);

    return toRead;
}

/**
   @ingroup pcb_utils_buffers_circbuf
   @brief
   Writes data to the buffer

   @details
   Writes data to the buffer

   @param circbuf Pointer to the circular buffer structure
   @param buffer Data that has to be written
   @param size Size of the data buffer

   @return
    - Number of bytes written
 */
ssize_t circbuf_writeBuffer(circbuf_t* circbuf, const char* buffer, size_t size) {
    if(!circbuf || !circbuf->write || !buffer) {
        fprintf(stderr, "circbuf_writeBuffer: Invalid buffer pointers or size\n");
        return -1;
    }
    /* check if we have enough space */
    if((size >= circbuf->toWrite) && (circbuf_resize(circbuf, max(size, circbuf->size)) < 0)) {
        return -1;
    }
    ssize_t writen = 0;

    /* write until boundary */
    ssize_t leftToBound = circbuf->buffer + circbuf->size - circbuf->write;
    if(size >= (size_t) leftToBound) {
        memcpy(circbuf->write, buffer, leftToBound);
        circbuf->write = circbuf->buffer;
        writen += leftToBound;
    }
    memcpy(circbuf->write, buffer + writen, size - writen);
    circbuf->toWrite -= size;
    circbuf->toRead += size;
    circbuf->write += size - writen;

    assert_circbuf(circbuf);
    return size;
}

/**
   @ingroup pcb_utils_buffers_circbuf
   @brief
   Mark a number of bytes as read

   @details
   Mark a number of bytes as read and return the number of bytes marked as read

   @param circbuf Pointer to the circular buffer structure
   @param size Number of bytes that needs to be marked as read

   @return
    - Number of bytes marked as read
 */
ssize_t circbuf_markAsRead(circbuf_t* circbuf, size_t size) {
    circbuf_t* cb = (circbuf_t*) circbuf;

    if(!cb || (cb->toRead == 0)) {
        return 0;
    }

    ssize_t toRead = min(size, cb->toRead);

    /* if the read is crossing the buffer boundary */
    ssize_t leftUntilBoundary = cb->buffer + cb->size - cb->read;
    if(toRead >= leftUntilBoundary) {
        cb->read = cb->buffer + toRead - leftUntilBoundary;
    } else {
        cb->read += toRead;
    }

    cb->toRead -= toRead;
    cb->toWrite += toRead;

    assert_circbuf(cb);

    return toRead;
}

/**
   @ingroup pcb_utils_buffers_circbuf
   @brief
   Open a stream

   @details
   Open a stream usable with the stdio functions
   When opening with r or r+, seeking the stream moves the read pointer.
   When opening with w, w+, a or a+, seeking moves the write pointer.

   @param circbuf Pointer to the circular buffer structure
   @param opentype r,r+,w,w+,a,a+

   @return
    - file pointer
 */
FILE* circbuf_fopen(circbuf_t* circbuf, const char* opentype) {
    circbuf_t* cb = (circbuf_t*) circbuf;

    cookie_io_functions_t hooks = {
        .read = (readerhook) & circbuf_read,
        .write = &circbuf_write,
        .seek = &circbuf_seek,
        .close = &circbuf_close
    };

    cb->stream = fopencookie(cb, opentype, hooks);
    if(cb->stream == NULL) {
        goto error;
    }

    /* buffer completely so we avoid writing /reading one byte at a time
     * it is the responsability from the writer to flush the buffer*/
    if(setvbuf(cb->stream, (char*) NULL, _IOFBF, cb->size) != 0) {
        goto error_close_cb_stream;
    }

    /* force eof if circbuf is empty */
    if(cb->toRead == 0) {
        char c;
        size_t items = fread(&c, sizeof(char), 1, cb->stream);
        (void) items;
    }

    /* set the seek mode */
    cb->seekMode = *opentype;

    return cb->stream;

error_close_cb_stream:
    fclose(cb->stream);
error:
    return NULL;
}
