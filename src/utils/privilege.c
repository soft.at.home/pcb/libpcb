/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include <linux/capability.h>
#include <linux/securebits.h>
#include <sys/types.h>
#include <sys/prctl.h>
#include <pwd.h>
#include <grp.h>
#include <unistd.h>
#include <errno.h>
#include <assert.h>

#include <components.h>

#include <debug/sahtrace.h>
#include <pcb/common/error.h>
#include <pcb/utils/privilege.h>

#define ME      "privilege"
#define MAX_NUMBER_OF_SUPPLEMENTARY_GROUPS 100

// Avoid compiler warning `warning: implicit declaration of function ‘getgrouplist’`
int getgrouplist(const char* user, gid_t group, gid_t* groups, int* ngroups);

/* These need to be here as they are potentially not defined the header files. */
extern int capset(cap_user_header_t header, const cap_user_data_t data);
extern int capget(cap_user_header_t header, cap_user_data_t data);

static char* priv_default_user = NULL;
static char* priv_default_group = NULL;
static priv_cap_t priv_default_retain;

static struct __user_cap_header_struct priv_cap_header = {
    .version = _LINUX_CAPABILITY_VERSION_3,
    .pid = 0,
};

static int priv_cap_versionToInt(int version) {
    switch(version) {
    case _LINUX_CAPABILITY_VERSION_1:
        return 1;
        break;
    case _LINUX_CAPABILITY_VERSION_2:
        return 2;
        break;
    case _LINUX_CAPABILITY_VERSION_3:
        return 3;
        break;
    default:
        break;
    }

    SAH_TRACEZ_WARNING(ME, "Unexpected capabilities version 0x%08X", version);
    return -1;
}

static bool priv_set_user(uid_t id) {
    uid_t euid = geteuid();

    if(euid == id) {
        return true;
    }

    if(setuid(id)) {
        SAH_TRACEZ_ERROR(ME, "Failed to set user id: %m");
        pcb_error = errno;
        return false;
    }

    SAH_TRACEZ_WARNING(ME, "Changed user id from %d to %d", euid, id);

    return true;
}

static bool priv_set_group(gid_t id) {
    gid_t egid = getegid();

    if(egid == id) {
        return true;
    }

    if(setgid(id)) {
        SAH_TRACEZ_ERROR(ME, "Failed to set group id: %m");
        pcb_error = errno;
        return false;
    }

    SAH_TRACEZ_WARNING(ME, "Changed group id from %d to %d", egid, id);

    return true;
}

static bool priv_set_supplementary_groups(
    const char* user,
    const gid_t current_group_id) {
    assert(user != NULL);

    gid_t groups[MAX_NUMBER_OF_SUPPLEMENTARY_GROUPS];
    int ngroups = MAX_NUMBER_OF_SUPPLEMENTARY_GROUPS;

    if(getgrouplist(user, current_group_id, &groups[0], &ngroups) == -1) {
        SAH_TRACEZ_ERROR(ME, "Error getting groups for user '%s': %m", user);
        return false;
    }
    if(setgroups(ngroups, &groups[0])) {
        SAH_TRACEZ_ERROR(ME, "Error setting groups: %m");
        return false;
    }
    SAH_TRACEZ_NOTICE(ME, "%d supplementary groups successfully added", ngroups);
    for(int i = 0; i != ngroups; ++i) {
        SAH_TRACEZ_NOTICE(ME, "Supplementary group successfully added: %d", groups[i]);
    }
    return true;
}

/**
   @ingroup pcb_utils_privilege
   @brief
   Convert a capability from a string

   @details
   Convert a capability from a string

   @param cap_str The capability as a string

   @return
    - The capability
    - PRIV_CAP_FROM_STRING_INVALID if an error occured
 */
cap_id_t priv_cap_fromString(const char* cap_str) {
    if(!cap_str || !*cap_str) {
        SAH_TRACEZ_ERROR(ME, "Invalid argument: cap_str");
        pcb_error = pcb_error_invalid_parameter;
        return PRIV_CAP_FROM_STRING_INVALID;
    }

    if(false) {

// *INDENT-OFF*
#define REPLACE_CAP_MACRO(cap) \
    } \
    else if(!strcmp(cap_str, #cap)) { \
        return cap;
#include "privilege_defs.h"
#undef REPLACE_CAP_MACRO
// *INDENT-ON*
    } else {
        SAH_TRACEZ_WARNING(ME, "Unknown capability string %s", cap_str);
        pcb_error = pcb_error_invalid_parameter;
        return PRIV_CAP_FROM_STRING_INVALID;
    }
}

/**
   @ingroup pcb_utils_privilege
   @brief
   REPLACE_CAP_MACRO a capability to a string

   @details
   REPLACE_CAP_MACRO a capability to a string

   @param cap The capability

   @return
    - Pointer to a string
    - NULL if an error occured
 */
const char* priv_cap_toString(cap_id_t cap_id) {
    switch(cap_id) {
// *INDENT-OFF*
#define REPLACE_CAP_MACRO(cap) \
    case cap: \
        return #cap;
#include "privilege_defs.h"
#undef REPLACE_CAP_MACRO
// *INDENT-ON*
    default:
        break;
    }

    SAH_TRACEZ_WARNING(ME, "Unknown capability %u", cap_id);
    pcb_error = pcb_error_invalid_parameter;
    return NULL;
}

/**
   @ingroup pcb_utils_privilege
   @brief
   Initialize a capabilities structure

   @details
   Initialize a capabilities structure.

   @param capt The capabilities structure

   @return
    - true if successful
    - false if an error occured
 */
bool priv_cap_initialize(priv_cap_t* capt) {
    if(!capt) {
        SAH_TRACEZ_ERROR(ME, "Invalid argument: capt");
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    memset(capt, 0, sizeof(priv_cap_t));
    return true;
}

/**
   @ingroup pcb_utils_privilege
   @brief
   Clean up a capabilities structure

   @details
   Clean up a capabilities structure.

   @param capt The capabilities structure

   @return void
 */
void priv_cap_cleanup(priv_cap_t* capt) {
    if(capt) {
        memset(capt, 0, sizeof(priv_cap_t));
    }
}

/**
   @ingroup pcb_utils_privilege
   @brief
   Set a capability

   @details
   Set a capability in a capabilities structure.

   @param capt The capabilities structure
   @param cap The capability

   @return
    - true if successful
    - false if an error occured
 */
bool priv_cap_set(priv_cap_t* capt, cap_id_t cap) {
    if(!capt) {
        SAH_TRACEZ_ERROR(ME, "Invalid argument: capt");
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(!cap_valid(cap)) {
        SAH_TRACEZ_ERROR(ME, "Invalid argument: cap");
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    capt->cap[CAP_TO_INDEX(cap)] |= CAP_TO_MASK(cap);
    return true;
}

/**
   @ingroup pcb_utils_privilege
   @brief
   Clear a capability

   @details
   Clean a capability in a capabilities structure.

   @param capt The capabilities structure
   @param cap The capability

   @return
    - true if successful
    - false if an error occured
 */
bool priv_cap_clear(priv_cap_t* capt, cap_id_t cap) {
    if(!capt) {
        SAH_TRACEZ_ERROR(ME, "Invalid argument: capt");
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(!cap_valid(cap)) {
        SAH_TRACEZ_ERROR(ME, "Invalid argument: cap");
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    capt->cap[CAP_TO_INDEX(cap)] &= ~CAP_TO_MASK(cap);
    return true;
}

/**
   @ingroup pcb_utils_privilege
   @brief
   Check if a capability is set

   @details
   Check if a capability is set in a capabilities structure.

   @param capt The capabilities structure
   @param cap The capability

   @return
    - true if set
    - false if not set or upon error
 */
bool priv_cap_isSet(const priv_cap_t* capt, cap_id_t cap) {
    if(!capt) {
        SAH_TRACEZ_ERROR(ME, "Invalid argument: capt");
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(!cap_valid(cap)) {
        SAH_TRACEZ_ERROR(ME, "Invalid argument: cap");
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    return (capt->cap[CAP_TO_INDEX(cap)] & CAP_TO_MASK(cap)) != 0;
}

/**
   @ingroup pcb_utils_privilege
   @brief
   Initialize privileged process functionality

   @details
   Initialize privileged process functionality.
   This function will probe the version supported by the kernel.

   @return
    - true if successful
    - false if an error occured
 */
bool priv_proc_initialize(void) {
    SAH_TRACEZ_INFO(ME, "Initializing");

    priv_cap_header.version = _LINUX_CAPABILITY_VERSION_3;
    if(capget(&priv_cap_header, NULL)) {
        if((errno == EINVAL) && (priv_cap_header.version != _LINUX_CAPABILITY_VERSION_3)) {
            SAH_TRACEZ_WARNING(ME, "Supported capabilities version by the kernel is dropped from %d to %d",
                               priv_cap_versionToInt(_LINUX_CAPABILITY_VERSION_3), priv_cap_versionToInt(priv_cap_header.version));
        } else {
            SAH_TRACEZ_ERROR(ME, "Failed to probe capabilities version: %m");
            pcb_error = errno;
            return false;
        }
    }

    SAH_TRACEZ_NOTICE(ME, "Capabilities version set to %d", priv_cap_versionToInt(priv_cap_header.version));

#ifdef CONFIG_SAH_LIB_PCB_PRIV_DROP_DEFAULT_USERNAME
    if(!priv_set_defaultUser(CONFIG_SAH_LIB_PCB_PRIV_DROP_DEFAULT_USERNAME)) {
        SAH_TRACEZ_ERROR(ME, "Setting default user as configured (%s) failed", CONFIG_SAH_LIB_PCB_PRIV_DROP_DEFAULT_USERNAME);
        return false;
    }
#else
    priv_set_defaultUser(NULL);
#endif

#ifdef CONFIG_SAH_LIB_PCB_PRIV_DROP_DEFAULT_GROUPNAME
    if(!priv_set_defaultGroup(CONFIG_SAH_LIB_PCB_PRIV_DROP_DEFAULT_GROUPNAME)) {
        SAH_TRACEZ_ERROR(ME, "Setting default group as configured (%s) failed", CONFIG_SAH_LIB_PCB_PRIV_DROP_DEFAULT_GROUPNAME);
        return false;
    }
#else
    priv_set_defaultGroup(NULL);
#endif

    priv_set_defaultRetain(NULL);

    return true;
}

/**
   @ingroup pcb_utils_privilege
   @brief
   Clean up privileged process functionality

   @details
   Clean up privileged process functionality.

   @return void
 */
void priv_proc_cleanup(void) {
    SAH_TRACEZ_INFO(ME, "Cleaning up");

    priv_set_defaultUser(NULL);
    priv_set_defaultGroup(NULL);
    priv_set_defaultRetain(NULL);
}

/**
   @ingroup pcb_utils_privilege
   @brief
   Retrieve the supported capabilities version

   @details
   Retrieve the supported capabilities version.

   @return
    - >= 1 : Capabilities version
    - -1 : Error
 */
int priv_proc_version(void) {
    return priv_cap_versionToInt(priv_cap_header.version);
}

/**
   @ingroup pcb_utils_privilege
   @brief
   Get the process' capabilities

   @details
   Get the current process' effective capabilities and
   store them in a capabilities structure.
   Note: capabilities are a per-thread attribute.

   @param capt The capabilities structure

   @return
    - true if successful
    - false if an error occured
 */
bool priv_proc_getCap(priv_cap_t* capt) {
    int i = 0;
    struct __user_cap_data_struct caps[_LINUX_CAPABILITY_U32S_3];

    if(!capt) {
        SAH_TRACEZ_ERROR(ME, "Invalid argument: capt");
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    memset(caps, 0, sizeof(caps));
    if(capget(&priv_cap_header, &(caps[0]))) {
        SAH_TRACEZ_ERROR(ME, "Failed to get process capabilities: %m");
        pcb_error = errno;
        return false;
    }

    for(i = 0; i < _LINUX_CAPABILITY_U32S_3; i++) {
        capt->cap[i] = caps[i].effective;
    }

    return true;
}

/**
   @ingroup pcb_utils_privilege
   @brief
   Set the process' capabilities

   @details
   Set the current process' effective capabilities
   provided by a capabilities structure.
   Once a capability has been lowered, it can not be
   raised again.
   Note: Capabilities are a per-thread attribute.

   @param capt The capabilities structure

   @return
    - true if successful
    - false if an error occured
 */
bool priv_proc_setCap(const priv_cap_t* capt) {
    int i = 0;
    struct __user_cap_data_struct caps[_LINUX_CAPABILITY_U32S_3];

    if(!capt) {
        SAH_TRACEZ_ERROR(ME, "Invalid argument: capt");
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    memset(caps, 0, sizeof(caps));
    if(capget(&priv_cap_header, &(caps[0]))) {
        SAH_TRACEZ_ERROR(ME, "Failed to get process capabilities: %m");
        pcb_error = errno;
        return false;
    }

    for(i = 0; i < _LINUX_CAPABILITY_U32S_3; i++) {
        caps[i].effective = capt->cap[i];
        caps[i].permitted = capt->cap[i];
        caps[i].inheritable = 0;
    }

    if(capset(&priv_cap_header, &(caps[0]))) {
        SAH_TRACEZ_ERROR(ME, "Failed to set process capabilities: %m");
        pcb_error = errno;
        return false;
    }

    // For now, don't change the Bounding set

    return true;
}

/**
   @ingroup pcb_utils_privilege
   @brief
   Drop the process' privileges

   @details
   Drop the current process' privileges by changing the user and group.
   If no user is provided, then the user it not changed.
   If no group is provided, then the group it not changed.
   The process can request to retain certain capabilities.
   If retain is a NULL pointer, then all capabilities will be dropped.

   @param user The user to change to
   @param group The group to change to
   @param retain The capabilities to retain

   @return
    - true if successful
    - false if an error occured
 */
bool priv_proc_dropPrivileges(const char* user, const char* group, const priv_cap_t* retain) {
    SAH_TRACEZ_INFO(ME, "Dropping privileges to user '%s'/group '%s'", user, group);

    uid_t user_id = geteuid();
    gid_t group_id = getegid();
    priv_cap_t capt;

    if(user && !priv_get_uid(user, &user_id)) {
        SAH_TRACEZ_ERROR(ME, "could not get uid of user '%s' (%m)", user);
        return false;
    }

    if(group && !priv_get_gid(group, &group_id)) {
        SAH_TRACEZ_ERROR(ME, "could not get group id of group '%s' (%m)", group);
        return false;
    }

    if(retain) {
        if((geteuid() == 0) && (user_id != 0)) {
            if(prctl(PR_SET_SECUREBITS, SECBIT_KEEP_CAPS)) {
                SAH_TRACEZ_ERROR(ME, "Failed to set process security bits: %m");
                pcb_error = errno;
                return false;
            }
        }
    }

    if(group && !priv_set_group(group_id)) {
        SAH_TRACEZ_ERROR(ME, "Failed to set process group '%s': %m", group);
        return false;
    }
    if(user && group && !priv_set_supplementary_groups(user, group_id)) {
        SAH_TRACEZ_ERROR(ME, "Failed to set process supplementary groups (user '%s', group '%s'): %m", user, group);
        return false;
    }
    if(user && !priv_set_user(user_id)) {
        SAH_TRACEZ_ERROR(ME, "Failed to set process user '%s': %m", user);
        return false;
    }

    if(retain) {
        if(!priv_proc_setCap(retain)) {
            return false;
        }
    } else {
        priv_cap_initialize(&capt);
        if(!priv_proc_setCap(&capt)) {
            priv_cap_cleanup(&capt);
            return false;
        }
        priv_cap_cleanup(&capt);
    }

    SAH_TRACEZ_NOTICE(ME, "Process dropped privileges");

    return true;
}

/**
   @ingroup priv_get_uid
   @brief
   Get the uid for a user from their name

   @details
   Queries the passwd database to find the uid
   matching a given user name

   @param username The name of the user to look up
   @param id output argument: a pointer to store the uid at

   @return
    - true if successful
    - false if an error occured
 */
bool priv_get_uid(const char* username, uid_t* id) {
    struct passwd* pwd = NULL;

    if(!username) {
        // nothing to be done
        return true;
    }

    errno = 0;
    pwd = getpwnam(username);
    if(!pwd) {
        if(errno) {
            SAH_TRACEZ_ERROR(ME, "User lookup failed: %m");
            pcb_error = errno;
            return false;
        }
        SAH_TRACEZ_ERROR(ME, "User %s not found", username);
        pcb_error = pcb_error_not_found;
        return false;
    }

    SAH_TRACEZ_NOTICE(ME, "User %s has user id %d", username, pwd->pw_uid);

    if(id) {
        *id = pwd->pw_uid;
    }

    return true;
}

/**
   @ingroup priv_get_gid
   @brief
   Get the gid for a group from its name

   @details
   Queries the group database to find the gid
   matching a given group name

   @param groupname The name of the group to look up
   @param id output argument: pointer to store the gid at - may be null to
   just check that the group exists.

   @return
    - true if successful
    - false if an error occured
 */
bool priv_get_gid(const char* groupname, gid_t* id) {
    if(!groupname) {
        // nothing to be done
        return true;
    }

    struct group* grp = NULL;
    errno = 0;
    grp = getgrnam(groupname);
    if(!grp) {
        if(errno) {
            SAH_TRACEZ_ERROR(ME, "Group lookup failed: %m");
            pcb_error = errno;
            return false;
        }
        SAH_TRACEZ_ERROR(ME, "Group %s not found", groupname);
        pcb_error = pcb_error_not_found;
        return false;
    }

    SAH_TRACEZ_NOTICE(ME, "Group %s has group id %d", groupname, grp->gr_gid);

    if(id) {
        *id = grp->gr_gid;
    }

    return true;
}

/**
   @ingroup priv_get_default_uid_and_gid
   @brief
   Get the uid and gid for the default user and default group

   @details
   Get the uid and gid for the default user and default group

   @param uid output argument: a non-null pointer to store the uid at
   @param gid output argument: a non-null pointer to store the gid at

   @return
    - true if successful
    - false if an error occured
 */
bool priv_get_default_uid_and_gid(uid_t* uid, gid_t* gid) {
    assert(uid && gid);
    uid_t my_uid;
    if(!priv_get_uid(priv_get_defaultUser(), &my_uid)) {
        return false;
    }
    gid_t my_gid;
    if(!priv_get_gid(priv_get_defaultGroup(), &my_gid)) {
        return false;
    }
    *uid = my_uid;
    *gid = my_gid;
    return true;
}

/**
   @ingroup pcb_utils_privilege
   @brief
   Get the default user

   @details
   Get the default user to use when dropping privileges

   @return
    - the default user
    - NULL if no user is set
 */
const char* priv_get_defaultUser(void) {
    return priv_default_user;
}

/**
   @ingroup pcb_utils_privilege
   @brief
   Get the default group

   @details
   Get the default group to use when dropping privileges

   @return
    - the default group
    - NULL if no group is set
 */
const char* priv_get_defaultGroup(void) {
    return priv_default_group;
}

/**
   @ingroup pcb_utils_privilege
   @brief
   Get the default retention capabilities

   @details
   Get the default retention capabilities to use when dropping privileges

   @return
    - the default retention capabilities
 */
const priv_cap_t* priv_get_defaultRetain(void) {
    return &priv_default_retain;
}

/**
   @ingroup pcb_utils_privilege
   @brief
   Set the default user

   @details
   Set the default user to use when dropping privileges

   @param user The default user
   @return
    - true if successful
    - false upon error
 */
bool priv_set_defaultUser(const char* user) {
    free(priv_default_user);
    priv_default_user = NULL;
    if(user && *user) {
        if(!priv_get_uid(user, NULL)) {
            return false;
        }

        priv_default_user = strdup(user);
        if(!priv_default_user) {
            SAH_TRACEZ_ERROR(ME, "Out of memory");
            pcb_error = pcb_error_no_memory;
            return false;
        }
    }

    return true;
}

/**
   @ingroup pcb_utils_privilege
   @brief
   Set the default group

   @details
   Set the default group to use when dropping privileges

   @param group The default group
   @return
    - true if successful
    - false upon error
 */
bool priv_set_defaultGroup(const char* group) {
    free(priv_default_group);
    priv_default_group = NULL;
    if(group && *group) {
        if(!priv_get_gid(group, NULL)) {
            SAH_TRACEZ_ERROR(ME, "could not get group id of group '%s' (%m)", group);
            return false;
        }

        priv_default_group = strdup(group);
        if(!priv_default_group) {
            SAH_TRACEZ_ERROR(ME, "Out of memory");
            pcb_error = pcb_error_no_memory;
            return false;
        }
    }

    return true;
}

/**
   @ingroup pcb_utils_privilege
   @brief
   Set the default retention capabilities

   @details
   Set the default retention capabilities to use when dropping privileges

   @param retain The default retention capabilities
   @return
    - true if successful
    - false upon error
 */
bool priv_set_defaultRetain(const priv_cap_t* retain) {
    priv_cap_cleanup(&priv_default_retain);
    if(retain) {
        memcpy(&priv_default_retain, retain, sizeof(priv_cap_t));
    }

    return true;
}
