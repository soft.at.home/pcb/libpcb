/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#define _GNU_SOURCE
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>

#include <debug/sahtrace.h>

#include <pcb/common/error.h>
#include <pcb/utils/uri.h>

/**
   @file
   @brief
   Implementation of the uri functions
 */

static void uri_parsePath(uri_t* uri) {
    char* tmp = NULL;
    if(!uri->path) {
        goto exit;
    }
    uri->parameters = strchr(uri->path, '?');
    if(!uri->parameters) {
        goto exit;
    }

    *uri->parameters = '\0';
    uri->parameters++;
    uri->parametersLength = strlen(uri->parameters);
    for(tmp = uri->parameters; tmp < uri->parameters + uri->parametersLength; tmp++) {
        if(*tmp == '&') {
            *tmp = '\0';
        }
    }

exit:
    return;
}

static void uri_parseAuthority(uri_t* uri, char* authority) {
    uri->host = authority;
    char* tmp = authority;
    bool curly_braces = false;
    bool braces = false;
    for(; *tmp && !uri->path; tmp++) {
        switch(*tmp) {
        case '{':
        case '}':
            if(!braces) {
                curly_braces = !curly_braces;
            }
            break;
        case '[':
        case ']':
            if(!curly_braces) {
                braces = !braces;
            }
            break;
        case ':':
            if(!braces && !curly_braces) {
                *tmp = 0;
                uri->port = tmp + 1;
            }
            break;
        case '@':
            if(!braces && !curly_braces) {
                *tmp = 0;
                uri->user = uri->host;
                uri->password = uri->port;
                uri->port = NULL;
                uri->host = tmp + 1;
            }
            break;
        case '/':
            if(!braces && !curly_braces) {
                *tmp = 0;
                uri->path = tmp + 1;
            }
            break;
        default:
            break;
        }
    }

    if((uri->host[0] == '{') || (uri->host[0] == '[')) {
        uri->host++;
        uri->host[strlen(uri->host) - 1] = 0;
    }
    if(uri->port && ((uri->port[0] == '{') || (uri->port[0] == '['))) {
        uri->port++;
        uri->port[strlen(uri->port) - 1] = 0;
    }
}

static void uri_resetParameters(const uri_t* uri, char current, char reset) {
    char* c;

    for(c = uri->parameters; c < uri->parameters + uri->parametersLength; c++) {
        if(*c == current) {
            *c = reset;
        }
    }
}

static inline const char* uri_hostString(const uri_t* uri) {
    return uri->host ? uri->host : "";
}

static inline const char* uri_portString(const uri_t* uri) {
    return uri->port ? uri->port : "";
}

static inline const char* uri_userString(const uri_t* uri) {
    return uri->user ? uri->user : "";
}

static inline const char* uri_passwordString(const uri_t* uri) {
    return uri->password ? uri->password : "";
}

static inline const char* uri_pathString(const uri_t* uri) {
    return uri->path ? uri->path : "";
}

static inline const char* uri_parameterString(const uri_t* uri) {
    return uri->parameters ? uri->parameters : "";
}

static inline const char* uri_userPasswordSeparator(const uri_t* uri) {
    return (uri->user && *(uri->user) && uri->password && (*uri->password)) ? ":" : "";
}

static inline const char* uri_userHostNameSeparator(const uri_t* uri) {
    return (uri->user && *(uri->user)) ? "@" : "";
}

static inline const char* uri_hostNamePortSeparator(const uri_t* uri) {
    return (uri->port && *(uri->port)) ? ":[" : "";
}

static inline const char* uri_closePort(const uri_t* uri) {
    return (uri->port && *(uri->port)) ? "]" : "";
}

static inline const char* uri_pathParametersSeparator(const uri_t* uri) {
    return (uri->parameters && *(uri->parameters)) ? "?" : "";
}

/**
   @ingroup pcb_utils_uri
   @brief
   Parse an uri string

   @details
   This functions parses an uri string.
   Call @ref uri_destroy to free up the memory used by the structure.
   Never free the structure using <b>free</b>

   @param URI String containing the uri

   @return
    - A pointer to the uri_t structure containing the parsed uri.
    - NULL when an error occured (invalid uri)
 */
uri_t* uri_parse(const char* URI) {
    uri_t* uri = NULL;
    char* scheme_end = NULL;
    char* authority = NULL;

    if(!URI) {
        return NULL;
    }

    uri = (uri_t*) calloc(1, sizeof(uri_t));
    if(!uri) {
        goto error;
    }

    uri->scheme = strdup(URI);
    if(!uri->scheme) {
        goto error_free_uri;
    }

    /* get scheme */
    scheme_end = strstr(uri->scheme, "://");
    if(!scheme_end) {
        goto error_free_scheme;
    }
    scheme_end[0] = 0;

    /* get authority */
    authority = scheme_end + 3;
    if(!*authority) {
        goto error_free_scheme;
    }

    uri_parseAuthority(uri, authority);

    uri_parsePath(uri);

    SAH_TRACEZ_INFO("pcb", "scheme = %s", uri->scheme);
    SAH_TRACEZ_INFO("pcb", "host = %s", uri_hostString(uri));
    SAH_TRACEZ_INFO("pcb", "port = %s", uri_portString(uri));
    SAH_TRACEZ_INFO("pcb", "user = %s", uri_userString(uri));
    SAH_TRACEZ_INFO("pcb", "password = %s", uri_passwordString(uri));
    SAH_TRACEZ_INFO("pcb", "path = %s", uri_pathString(uri));

    return uri;

error_free_scheme:
    free(uri->scheme);
error_free_uri:
    free(uri);
error:
    return NULL;
}

/**
   @ingroup pcb_utils_uri
   @brief
   Free the uri_t structure

   @details
   Free up the memory used by the structure.

   @param URI Pointer to the uri_t structure
 */
void uri_destroy(uri_t* URI) {
    if(!URI) {
        pcb_error = pcb_error_invalid_parameter;
        return;
    }

    free(URI->scheme);
    free(URI);
}

/**
   @ingroup pcb_utils_uri
   @brief
   Fetch the value of a parameter from the uri

   @details
   Fetch the value of a parameter from the uri string using a parameter name.

   @param URI Pointer to the uri_t structure
   @param name The name of the parameter

   @return
    - The value
    - NULL if the parameter is not found
 */
const char* uri_getParameter(const uri_t* URI, const char* name) {
    if(!URI || !name || !*name) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    int len = strlen(name);
    char* p;
    for(p = URI->parameters; p < URI->parameters + URI->parametersLength; p += strlen(p) + 1) {
        if((strncmp(p, name, len) == 0) && (*(p + len) == '=')) {
            return p + len + 1;
        }
    }
    return NULL;
}

/**
   @ingroup pcb_utils_uri
   @brief
   Build a uri string.

   @details
   Generate an uri string from the given uri_t structure.
   The returned string must be freed.

   @param URI Pointer to the uri_t structure

   @return
    - String representing the uri
 */
char* uri_string(const uri_t* URI) {
    if(!URI) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    uri_resetParameters(URI, '\0', '&');

    char* fulluri = NULL;
    if(asprintf(&fulluri, "%s://%s%s%s%s[%s]%s%s%s%s%s%s%s", URI->scheme,
                uri_userString(URI),
                uri_userPasswordSeparator(URI),
                uri_passwordString(URI),
                uri_userHostNameSeparator(URI),
                uri_hostString(URI),
                uri_hostNamePortSeparator(URI),
                uri_portString(URI),
                uri_closePort(URI),
                "/",
                uri_pathString(URI),
                uri_pathParametersSeparator(URI),
                uri_parameterString(URI)) == -1) {
        SAH_TRACEZ_ERROR("pcb", "Failed to allocate memory");
        return NULL;
    }

    uri_resetParameters(URI, '&', '\0');

    return fulluri;
}
