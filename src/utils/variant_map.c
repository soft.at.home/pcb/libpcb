/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <string.h>

#include <pcb/common/error.h>
#include <pcb/utils/variant_map.h>
#include <pcb/utils/variant_list.h>

/**
   @file
   @brief
   Implementation of the public variant map functions
 */

/**
   @ingroup pcb_utils_variant_map
   @brief
   Initialize a variant map.

   @details
   This function initializes a variant map.
   It is mandatory to initialize a variant map (@ref variant_map_t) before using it.
   Every @ref variant_map_initialize call should have a corresponding @ref variant_map_cleanup call to free the memory,
   even if the @ref variant_map_initialize function returned an error.
   This function calls @ref llist_initialize.

   @param varmap the variant map to be initialized

   @return
    - true: Initialization was succesfull, the llist_t item is ready to be used
    - false: An error has occurred
        - the varlist is a NULL pointer
        - memory allocation failed
 */
bool variant_map_initialize(variant_map_t* varmap) {
    return llist_initialize(varmap);
}

/**
   @ingroup pcb_utils_variant_map
   @brief
   Cleanup a variant map.

   @details
   This function <b>frees</b> the variant map. \n
   After calling this function, the variant map can not be used anymore, until it is re-initialized.\n
   The elements in the map are deleted.\n
   This function calls @ref variant_map_clear and @ref llist_cleanup.\n

   @param varmap the variant map that has to be cleaned up

 */
void variant_map_cleanup(variant_map_t* varmap) {
    variant_map_clear(varmap);
}

/**
   @ingroup pcb_utils_variant_map
   @brief
   Remove all elements from a variant map.

   @details
   This function <b>frees</b> all elements from the variant map. \n
   This function loops over all variant elements, using @ref variant_map_takeFirst to remove an \n
   element from the map and @ref variant_map_iterator_destroy to free the memory for that item.\n

   @param varmap the variant map that has to be cleared
 */
void variant_map_clear(variant_map_t* varmap) {
    variant_map_iterator_t* it = NULL;

    it = variant_map_takeFirst(varmap);
    while(it) {
        variant_map_iterator_destroy(it);
        it = variant_map_takeFirst(varmap);
    }
}

/**
   @ingroup pcb_utils_variant_map
   @brief
   Create and initialize a <b>variant map iterator</b> (=variant map item)

   @details
   This function <b>created and initializes</b> a <b>variant map iterator</b>. \n
   This function call creates the variant map iterator, and initializing the variant and map
   elements using @ref variant_initialize and @ref llist_iterator_initialize\n

   @param key the key
   @param variant the variant that has to be used to initialize the iterator (optional)

   @return
    - pointer to a variant map iterator
    - NULL an error has occured
 */
variant_map_iterator_t* variant_map_iterator_create(const char* key, const variant_t* variant) {
    if(!key || !(*key)) {
        return NULL;
    }
    variant_map_iterator_t* it = calloc(1, sizeof(variant_map_iterator_t));
    if(!it) {
        pcb_error = pcb_error_no_memory;
        return NULL;
    }

    it->key = strdup(key);
    if(!it->key) {
        free(it);
        return NULL;
    }

    if(variant) {
        variant_copy(&it->variant, variant);
    }

    return it;
}

/**
   @ingroup pcb_utils_variant_map
   @brief
   Create and initialize a <b>variant map iterator</b> (=variant map item)

   @details
   This function <b>created and initializes</b> a <b>variant map iterator</b>. \n
   This function call creates the variant map iterator, and initializing the variant and map
   elements using @ref variant_initialize and @ref llist_iterator_initialize\n

   @param key the key
   @param variant the variant that has to be used to initialize the iterator

   @return
    - pointer to a variant map iterator
    - NULL an error has occured
 */
variant_map_iterator_t* variant_map_iterator_createRef(const char* key, variant_t* variant) {
    if(!key || !(*key) || !variant) {
        return NULL;
    }
    variant_map_iterator_t* it = calloc(1, sizeof(variant_map_iterator_t));
    if(!it) {
        pcb_error = pcb_error_no_memory;
        return NULL;
    }
    if(!variant_initialize(&it->variant, variant_type_reference)) {
        free(it);
        pcb_error = pcb_error_no_memory;
        return NULL;
    }

    it->key = strdup(key);
    if(!it->key) {
        variant_cleanup(&it->variant);
        free(it);
        return NULL;
    }

    variant_setReference(&it->variant, variant);

    return it;
}

/**
   @ingroup pcb_utils_variant_map
   @brief
   Create and initialize a <b>variant map iterator</b> (=variant map item)

   @details
   This function <b>created and initializes</b> a <b>variant map iterator</b> with the data provided\n
   This function call creates the variant map iterator, and initializing the variant and map
   elements using @ref variant_initialize and @ref llist_iterator_initialize\n

   @param key the key
   @param data the variant that has to be used to initialize the iterator (optional)

   @return
    - pointer to a variant map iterator
    - NULL an error has occured
 */
variant_map_iterator_t* variant_map_iterator_createChar(const char* key, const char* data) {
    if(!key || !(*key)) {
        return NULL;
    }
    variant_map_iterator_t* it = calloc(1, sizeof(variant_map_iterator_t));
    if(!it) {
        pcb_error = pcb_error_no_memory;
        return NULL;
    }

    variant_initialize(&it->variant, variant_type_string);
    if(data) {
        variant_setChar(&it->variant, data);
    }

    it->key = strdup(key);
    if(!it->key) {
        variant_cleanup(&it->variant);
        free(it);
        return NULL;
    }

    return it;
}

/**
   @ingroup pcb_utils_variant_map
   @brief
   Create and initialize a <b>variant map iterator</b> (=variant map item)

   @details
   This function <b>created and initializes</b> a <b>variant map iterator</b> with the data provided\n

   @param key the key
   @param data the variant that has to be used to initialize the iterator (optional)

   @return
    - pointer to a variant map iterator
    - NULL an error has occured
 */
variant_map_iterator_t* variant_map_iterator_createString(const char* key, const string_t* data) {
    if(!key || !(*key)) {
        return NULL;
    }
    variant_map_iterator_t* it = calloc(1, sizeof(variant_map_iterator_t));
    if(!it) {
        pcb_error = pcb_error_no_memory;
        return NULL;
    }

    variant_initialize(&it->variant, variant_type_string);
    if(data) {
        variant_setString(&it->variant, data);
    }

    it->key = strdup(key);
    if(!it->key) {
        variant_cleanup(&it->variant);
        free(it);
        return NULL;
    }

    return it;
}

/**
   @ingroup pcb_utils_variant_map
   @brief
   Create and initialize a <b>variant map iterator</b> (=variant map item)

   @details
   This function <b>created and initializes</b> a <b>variant map iterator</b> with the data provided\n

   @param key the key
   @param data the variant that has to be used to initialize the iterator (optional)

   @return
    - pointer to a variant map iterator
    - NULL an error has occured
 */
variant_map_iterator_t* variant_map_iterator_createInt8(const char* key, int8_t data) {
    if(!key || !(*key)) {
        return NULL;
    }
    variant_map_iterator_t* it = calloc(1, sizeof(variant_map_iterator_t));
    if(!it) {
        pcb_error = pcb_error_no_memory;
        return NULL;
    }

    variant_initialize(&it->variant, variant_type_int8);
    variant_setInt8(&it->variant, data);

    it->key = strdup(key);
    if(!it->key) {
        variant_cleanup(&it->variant);
        free(it);
        return NULL;
    }

    return it;
}

/**
   @ingroup pcb_utils_variant_map
   @brief
   Create and initialize a <b>variant map iterator</b> (=variant map item)

   @details
   This function <b>created and initializes</b> a <b>variant map iterator</b> with the data provided\n

   @param key the key
   @param data the variant that has to be used to initialize the iterator (optional)

   @return
    - pointer to a variant map iterator
    - NULL an error has occured
 */
variant_map_iterator_t* variant_map_iterator_createInt16(const char* key, int16_t data) {
    if(!key || !(*key)) {
        return NULL;
    }
    variant_map_iterator_t* it = calloc(1, sizeof(variant_map_iterator_t));
    if(!it) {
        pcb_error = pcb_error_no_memory;
        return NULL;
    }

    variant_initialize(&it->variant, variant_type_int16);
    variant_setInt16(&it->variant, data);

    it->key = strdup(key);
    if(!it->key) {
        variant_cleanup(&it->variant);
        free(it);
        return NULL;
    }

    return it;
}

/**
   @ingroup pcb_utils_variant_map
   @brief
   Create and initialize a <b>variant map iterator</b> (=variant map item)

   @details
   This function <b>created and initializes</b> a <b>variant map iterator</b> with the data provided\n

   @param key the key
   @param data the variant that has to be used to initialize the iterator (optional)

   @return
    - pointer to a variant map iterator
    - NULL an error has occured
 */
variant_map_iterator_t* variant_map_iterator_createInt32(const char* key, int32_t data) {
    if(!key || !(*key)) {
        return NULL;
    }
    variant_map_iterator_t* it = calloc(1, sizeof(variant_map_iterator_t));
    if(!it) {
        pcb_error = pcb_error_no_memory;
        return NULL;
    }

    variant_initialize(&it->variant, variant_type_int32);
    variant_setInt32(&it->variant, data);

    it->key = strdup(key);
    if(!it->key) {
        variant_cleanup(&it->variant);
        free(it);
        return NULL;
    }

    return it;
}

/**
   @ingroup pcb_utils_variant_map
   @brief
   Create and initialize a <b>variant map iterator</b> (=variant map item)

   @details
   This function <b>created and initializes</b> a <b>variant map iterator</b> with the data provided\n

   @param key the key
   @param data the variant that has to be used to initialize the iterator (optional)

   @return
    - pointer to a variant map iterator
    - NULL an error has occured
 */
variant_map_iterator_t* variant_map_iterator_createInt64(const char* key, int64_t data) {
    if(!key || !(*key)) {
        return NULL;
    }
    variant_map_iterator_t* it = calloc(1, sizeof(variant_map_iterator_t));
    if(!it) {
        pcb_error = pcb_error_no_memory;
        return NULL;
    }

    variant_initialize(&it->variant, variant_type_int64);
    variant_setInt64(&it->variant, data);

    it->key = strdup(key);
    if(!it->key) {
        variant_cleanup(&it->variant);
        free(it);
        return NULL;
    }

    return it;
}

/**
   @ingroup pcb_utils_variant_map
   @brief
   Create and initialize a <b>variant map iterator</b> (=variant map item)

   @details
   This function <b>created and initializes</b> a <b>variant map iterator</b> with the data provided\n

   @param key the key
   @param data the variant that has to be used to initialize the iterator (optional)

   @return
    - pointer to a variant map iterator
    - NULL an error has occured
 */
variant_map_iterator_t* variant_map_iterator_createUInt8(const char* key, uint8_t data) {
    if(!key || !(*key)) {
        return NULL;
    }
    variant_map_iterator_t* it = calloc(1, sizeof(variant_map_iterator_t));
    if(!it) {
        pcb_error = pcb_error_no_memory;
        return NULL;
    }

    variant_initialize(&it->variant, variant_type_uint8);
    variant_setUInt8(&it->variant, data);

    it->key = strdup(key);
    if(!it->key) {
        variant_cleanup(&it->variant);
        free(it);
        return NULL;
    }

    return it;
}

/**
   @ingroup pcb_utils_variant_map
   @brief
   Create and initialize a <b>variant map iterator</b> (=variant map item)

   @details
   This function <b>created and initializes</b> a <b>variant map iterator</b> with the data provided\n

   @param key the key
   @param data the variant that has to be used to initialize the iterator (optional)

   @return
    - pointer to a variant map iterator
    - NULL an error has occured
 */
variant_map_iterator_t* variant_map_iterator_createUInt16(const char* key, uint16_t data) {
    if(!key || !(*key)) {
        return NULL;
    }
    variant_map_iterator_t* it = calloc(1, sizeof(variant_map_iterator_t));
    if(!it) {
        pcb_error = pcb_error_no_memory;
        return NULL;
    }

    variant_initialize(&it->variant, variant_type_uint16);
    variant_setUInt16(&it->variant, data);

    it->key = strdup(key);
    if(!it->key) {
        variant_cleanup(&it->variant);
        free(it);
        return NULL;
    }

    return it;
}

/**
   @ingroup pcb_utils_variant_map
   @brief
   Create and initialize a <b>variant map iterator</b> (=variant map item)

   @details
   This function <b>created and initializes</b> a <b>variant map iterator</b> with the data provided\n

   @param key the key
   @param data the variant that has to be used to initialize the iterator (optional)

   @return
    - pointer to a variant map iterator
    - NULL an error has occured
 */
variant_map_iterator_t* variant_map_iterator_createUInt32(const char* key, uint32_t data) {
    if(!key || !(*key)) {
        return NULL;
    }
    variant_map_iterator_t* it = calloc(1, sizeof(variant_map_iterator_t));
    if(!it) {
        pcb_error = pcb_error_no_memory;
        return NULL;
    }

    variant_initialize(&it->variant, variant_type_uint32);
    variant_setUInt32(&it->variant, data);

    it->key = strdup(key);
    if(!it->key) {
        variant_cleanup(&it->variant);
        free(it);
        return NULL;
    }

    return it;
}

/**
   @ingroup pcb_utils_variant_map
   @brief
   Create and initialize a <b>variant map iterator</b> (=variant map item)

   @details
   This function <b>created and initializes</b> a <b>variant map iterator</b> with the data provided\n

   @param key the key
   @param data the variant that has to be used to initialize the iterator (optional)

   @return
    - pointer to a variant map iterator
    - NULL an error has occured
 */
variant_map_iterator_t* variant_map_iterator_createUInt64(const char* key, uint64_t data) {
    if(!key || !(*key)) {
        return NULL;
    }
    variant_map_iterator_t* it = calloc(1, sizeof(variant_map_iterator_t));
    if(!it) {
        pcb_error = pcb_error_no_memory;
        return NULL;
    }

    variant_initialize(&it->variant, variant_type_uint64);
    variant_setUInt64(&it->variant, data);

    it->key = strdup(key);
    if(!it->key) {
        variant_cleanup(&it->variant);
        free(it);
        return NULL;
    }

    return it;
}

/**
   @ingroup pcb_utils_variant_map
   @brief
   Create and initialize a <b>variant map iterator</b> (=variant map item)

   @details
   This function <b>created and initializes</b> a <b>variant map iterator</b> with the data provided\n

   @param key the key
   @param data the variant that has to be used to initialize the iterator (optional)

   @return
    - pointer to a variant map iterator
    - NULL an error has occured
 */
variant_map_iterator_t* variant_map_iterator_createDouble(const char* key, double data) {
    if(!key || !(*key)) {
        return NULL;
    }
    variant_map_iterator_t* it = calloc(1, sizeof(variant_map_iterator_t));
    if(!it) {
        pcb_error = pcb_error_no_memory;
        return NULL;
    }

    variant_initialize(&it->variant, variant_type_double);
    variant_setDouble(&it->variant, data);

    it->key = strdup(key);
    if(!it->key) {
        variant_cleanup(&it->variant);
        free(it);
        return NULL;
    }

    return it;
}

/**
   @ingroup pcb_utils_variant_map
   @brief
   Create and initialize a <b>variant map iterator</b> (=variant map item)

   @details
   This function <b>created and initializes</b> a <b>variant map iterator</b> with the data provided\n

   @param key the key
   @param data the variant that has to be used to initialize the iterator (optional)

   @return
    - pointer to a variant map iterator
    - NULL an error has occured
 */
variant_map_iterator_t* variant_map_iterator_createBool(const char* key, bool data) {
    if(!key || !(*key)) {
        return NULL;
    }
    variant_map_iterator_t* it = calloc(1, sizeof(variant_map_iterator_t));
    if(!it) {
        pcb_error = pcb_error_no_memory;
        return NULL;
    }

    variant_initialize(&it->variant, variant_type_bool);
    variant_setBool(&it->variant, data);

    it->key = strdup(key);
    if(!it->key) {
        variant_cleanup(&it->variant);
        free(it);
        return NULL;
    }

    return it;
}

/**
   @ingroup pcb_utils_variant_map
   @brief
   Create and initialize a <b>variant map iterator</b> (=variant map item)

   @details
   This function <b>created and initializes</b> a <b>variant map iterator</b> with the data provided\n

   @param key the key
   @param data the variant that has to be used to initialize the iterator (optional)

   @return
    - pointer to a variant map iterator
    - NULL an error has occured
 */
variant_map_iterator_t* variant_map_iterator_createDateTime(const char* key, const struct tm* data) {
    if(!key || !(*key)) {
        return NULL;
    }
    variant_map_iterator_t* it = calloc(1, sizeof(variant_map_iterator_t));
    if(!it) {
        pcb_error = pcb_error_no_memory;
        return NULL;
    }

    variant_initialize(&it->variant, variant_type_date_time);
    if(data) {
        variant_setDateTime(&it->variant, data);
    }

    it->key = strdup(key);
    if(!it->key) {
        variant_cleanup(&it->variant);
        free(it);
        return NULL;
    }

    return it;
}

/**
   @ingroup pcb_utils_variant_map
   @brief
   Create and initialize a <b>variant map iterator</b> (=variant map item)

   @details
   This function <b>created and initializes</b> a <b>variant map iterator</b> with the data provided\n

   @param key the key
   @param data the variant that has to be used to initialize the iterator (optional)

   @return
    - pointer to a variant map iterator
    - NULL an error has occured
 */
variant_map_iterator_t* variant_map_iterator_createDateTimeExtended(const char* key, const pcb_datetime_t* data) {
    if(!key || !(*key)) {
        return NULL;
    }
    variant_map_iterator_t* it = calloc(1, sizeof(variant_map_iterator_t));
    if(!it) {
        pcb_error = pcb_error_no_memory;
        return NULL;
    }

    variant_initialize(&it->variant, variant_type_date_time);
    if(data) {
        variant_setDateTimeExtended(&it->variant, data);
    }

    it->key = strdup(key);
    if(!it->key) {
        variant_cleanup(&it->variant);
        free(it);
        return NULL;
    }

    return it;
}

/**
   @ingroup pcb_utils_variant_map
   @brief
   Create and initialize a <b>variant map iterator</b> (=variant map item)

   @details
   This function <b>created and initializes</b> a <b>variant map iterator</b> with the data provided\n

   @param key the key
   @param data the variant that has to be used to initialize the iterator (optional)

   @return
    - pointer to a variant map iterator
    - NULL an error has occured
 */
variant_map_iterator_t* variant_map_iterator_createListMove(const char* key, variant_list_t* data) {
    if(!key || !(*key)) {
        return NULL;
    }
    variant_map_iterator_t* it = calloc(1, sizeof(variant_map_iterator_t));
    if(!it) {
        pcb_error = pcb_error_no_memory;
        return NULL;
    }

    variant_initialize(&it->variant, variant_type_array);
    if(data) {
        variant_setListMove(&it->variant, data);
    }

    it->key = strdup(key);
    if(!it->key) {
        variant_cleanup(&it->variant);
        free(it);
        return NULL;
    }

    return it;
}

/**
   @ingroup pcb_utils_variant_map
   @brief
   Create and initialize a <b>variant map iterator</b> (=variant map item)

   @details
   This function <b>created and initializes</b> a <b>variant map iterator</b> with the data provided (takes a copy of the list)\n

   @param key the key
   @param data the variant that has to be used to initialize the iterator (optional)

   @return
    - pointer to a variant map iterator
    - NULL an error has occured
 */
variant_map_iterator_t* variant_map_iterator_createListCopy(const char* key, const variant_list_t* data) {
    if(!key || !(*key)) {
        return NULL;
    }
    variant_map_iterator_t* it = calloc(1, sizeof(variant_map_iterator_t));
    if(!it) {
        pcb_error = pcb_error_no_memory;
        return NULL;
    }

    variant_initialize(&it->variant, variant_type_array);
    if(data) {
        variant_setListCopy(&it->variant, data);
    }

    it->key = strdup(key);
    if(!it->key) {
        variant_cleanup(&it->variant);
        free(it);
        return NULL;
    }

    return it;
}

/**
   @ingroup pcb_utils_variant_map
   @brief
   Create and initialize a <b>variant map iterator</b> (=variant map item)

   @details
   This function <b>created and initializes</b> a <b>variant map iterator</b> with the data provided\n

   @param key the key
   @param data the variant that has to be used to initialize the iterator (optional)

   @return
    - pointer to a variant map iterator
    - NULL an error has occured
 */
variant_map_iterator_t* variant_map_iterator_createListRef(const char* key, variant_list_t* data) {
    if(!key || !(*key)) {
        return NULL;
    }
    variant_map_iterator_t* it = calloc(1, sizeof(variant_map_iterator_t));
    if(!it) {
        pcb_error = pcb_error_no_memory;
        return NULL;
    }

    variant_initialize(&it->variant, variant_type_array_reference);
    if(data) {
        variant_setListRef(&it->variant, data);
    }

    it->key = strdup(key);
    if(!it->key) {
        variant_cleanup(&it->variant);
        free(it);
        return NULL;
    }

    return it;
}

/**
   @ingroup pcb_utils_variant_map
   @brief
   Create and initialize a <b>variant map iterator</b> (=variant map item)

   @details
   This function <b>created and initializes</b> a <b>variant map iterator</b> with the data provided\n

   @param key the key
   @param data the variant that has to be used to initialize the iterator (optional)

   @return
    - pointer to a variant map iterator
    - NULL an error has occured
 */
variant_map_iterator_t* variant_map_iterator_createMapMove(const char* key, variant_map_t* data) {
    if(!key || !(*key)) {
        return NULL;
    }
    variant_map_iterator_t* it = calloc(1, sizeof(variant_map_iterator_t));
    if(!it) {
        pcb_error = pcb_error_no_memory;
        return NULL;
    }

    variant_initialize(&it->variant, variant_type_map);
    if(data) {
        variant_setMapMove(&it->variant, data);
    }

    it->key = strdup(key);
    if(!it->key) {
        variant_cleanup(&it->variant);
        free(it);
        return NULL;
    }

    return it;
}

/**
   @ingroup pcb_utils_variant_map
   @brief
   Create and initialize a <b>variant map iterator</b> (=variant map item)

   @details
   This function <b>created and initializes</b> a <b>variant map iterator</b> with the data provided (takes a copy of the variant_map)\n

   @param key the key
   @param data the variant that has to be used to initialize the iterator (optional)

   @return
    - pointer to a variant map iterator
    - NULL an error has occured
 */
variant_map_iterator_t* variant_map_iterator_createMapCopy(const char* key, const variant_map_t* data) {
    if(!key || !(*key)) {
        return NULL;
    }
    variant_map_iterator_t* it = calloc(1, sizeof(variant_map_iterator_t));
    if(!it) {
        pcb_error = pcb_error_no_memory;
        return NULL;
    }

    variant_initialize(&it->variant, variant_type_map);
    if(data) {
        variant_setMapCopy(&it->variant, data);
    }

    it->key = strdup(key);
    if(!it->key) {
        variant_cleanup(&it->variant);
        free(it);
        return NULL;
    }

    return it;
}

/**
   @ingroup pcb_utils_variant_map
   @brief
   Create and initialize a <b>variant map iterator</b> (=variant map item)

   @details
   This function <b>created and initializes</b> a <b>variant map iterator</b> with the data provided

   @param key the key
   @param data the variant that has to be used to initialize the iterator (optional)

   @return
    - pointer to a variant map iterator
    - NULL an error has occured
 */
variant_map_iterator_t* variant_map_iterator_createMapRef(const char* key, variant_map_t* data) {
    if(!key || !(*key)) {
        return NULL;
    }
    variant_map_iterator_t* it = calloc(1, sizeof(variant_map_iterator_t));
    if(!it) {
        pcb_error = pcb_error_no_memory;
        return NULL;
    }

    variant_initialize(&it->variant, variant_type_map_reference);
    if(data) {
        variant_setMapRef(&it->variant, data);
    }

    it->key = strdup(key);
    if(!it->key) {
        variant_cleanup(&it->variant);
        free(it);
        return NULL;
    }

    return it;
}

/**
   @ingroup pcb_utils_variant_map
   @brief
   Destroy a variant map iterator.

   @details
   This function <b>frees</b> the variant map iterator. \n
   After calling this function, the variant map iterator can not be used anymore, until it is re-created.\n
   The iterator will be removed from the map.\n
   This function calls @ref variant_cleanup and frees its memory. \n

   @param it the variant map iterator that has to be cleaned up
 */
void variant_map_iterator_destroy(variant_map_iterator_t* it) {
    if(!it) {
        pcb_error = pcb_error_invalid_parameter;
        return;
    }
    variant_map_iterator_take(it);
    variant_cleanup(&it->variant);
    free(it->key);
    free(it);
}

variant_t* variant_map_iterator_data(const variant_map_iterator_t* it) {
    if(!it) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    if(variant_type(&it->variant) == variant_type_reference) {
        return it->variant.data.v;
    }
    return (variant_t*) (&it->variant);
}

/**
   @ingroup pcb_utils_variant_map
   @brief
   Get the key of the iterator

   @details
   This function returns a the key of the iterator.

   @param it the variant map iterator

   @return
    - the key string
 */
const char* variant_map_iterator_key(const variant_map_iterator_t* it) {
    if(!it) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    return it->key;
}

/**
   @ingroup pcb_utils_variant_map
   @brief
   Change the key of the iterator

   @details
   This function changes the key of the iterator.

   @param it the variant map iterator
   @param key the new key

   @return
    - true
    - false
 */
bool variant_map_iterator_setKey(variant_map_iterator_t* it, const char* key) {
    if(!it || !key) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    uint32_t len = strlen(key);
    char* prevkey = it->key;
    if(prevkey) {
        if(strlen(it->key) < len) {
            it->key = realloc(it->key, len + 1);
            if(!it->key) {
                it->key = prevkey;
                return false;
            } else {
                strcpy(it->key, key);
            }
        } else {
            strcpy(it->key, key);
        }
    } else {
        it->key = strdup(key);
    }

    return true;
}

/**
   @ingroup pcb_utils_variant_map
   @brief
   Add a value to the variant map

   @details
   This function adds a value to the variant map.

   @param varmap The map the value has to be added in
   @param key the key
   @param variant the value that has to be added

   @return
    - true the value is added.
    - false an error occured
 */
bool variant_map_add(variant_map_t* varmap, const char* key, const variant_t* variant) {
    if(!varmap) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    variant_map_iterator_t* it = variant_map_iterator_create(key, variant);
    if(!it) {
        return false;
    }

    return variant_map_append(varmap, it);
}

/**
   @ingroup pcb_utils_variant_map
   @brief
   Move the value to the variant map

   @details
   This function move a value to the variant map.

   @param varmap The map the value has to be added in
   @param key the key
   @param variant the value that has to be moved

   @return
    - true the value is added.
    - false an error occured
 */
bool variant_map_addMove(variant_map_t* varmap, const char* key, variant_t* variant) {
    if(!varmap) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    variant_map_iterator_t* it = variant_map_iterator_create(key, NULL);
    if(!it) {
        return false;
    }

    variant_move(&it->variant, variant);

    return variant_map_append(varmap, it);
}

/**
   @ingroup pcb_utils_variant_map
   @brief
   Add a reference to a variant to the map

   @details
   This function adds a reference to the given variant to the map.

   @param varlist the variant list
   @param key the key
   @param variant the variant to add

   @return
    - true the reference is added.
    - false an error occured
 */
bool variant_map_addRef(variant_map_t* varlist, const char* key, variant_t* variant) {
    if(!varlist || !variant) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    variant_map_iterator_t* it = variant_map_iterator_createRef(key, variant);
    if(!it) {
        return false;
    }

    return variant_map_append(varlist, it);
}

/**
   @ingroup pcb_utils_variant_map
   @brief
   Add a value to the variant map

   @details
   This function adds a value to the variant map.

   @param varmap The map the value has to be added in
   @param key the key
   @param data the value that has to be added

   @return
    - true the value is added.
    - false an error occured
 */
bool variant_map_addChar(variant_map_t* varmap, const char* key, const char* data) {
    if(!varmap) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    variant_map_iterator_t* it = variant_map_iterator_createChar(key, data);
    if(!it) {
        return false;
    }

    return variant_map_append(varmap, it);
}

/**
   @ingroup pcb_utils_variant_map
   @brief
   Add a value to the variant map

   @details
   This function adds a value to the variant map.

   @param varmap The map the value has to be added in
   @param key the key
   @param data the value that has to be added

   @return
    - true the value is added.
    - false an error occured
 */
bool variant_map_addString(variant_map_t* varmap, const char* key, const string_t* data) {
    if(!varmap) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    variant_map_iterator_t* it = variant_map_iterator_createString(key, data);
    if(!it) {
        return false;
    }

    return variant_map_append(varmap, it);
}

/**
   @ingroup pcb_utils_variant_map
   @brief
   Add a value to the variant map

   @details
   This function adds a value to the variant map.

   @param varmap The map the value has to be added in
   @param key the key
   @param data the value that has to be added

   @return
    - true the value is added.
    - false an error occured
 */
bool variant_map_addInt8(variant_map_t* varmap, const char* key, int8_t data) {
    if(!varmap) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    variant_map_iterator_t* it = variant_map_iterator_createInt8(key, data);
    if(!it) {
        return false;
    }

    return variant_map_append(varmap, it);
}

/**
   @ingroup pcb_utils_variant_map
   @brief
   Add a value to the variant map

   @details
   This function adds a value to the variant map.

   @param varmap The map the value has to be added in
   @param key the key
   @param data the value that has to be added

   @return
    - true the value is added.
    - false an error occured
 */
bool variant_map_addInt16(variant_map_t* varmap, const char* key, int16_t data) {
    if(!varmap) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    variant_map_iterator_t* it = variant_map_iterator_createInt16(key, data);
    if(!it) {
        return false;
    }

    return variant_map_append(varmap, it);
}

/**
   @ingroup pcb_utils_variant_map
   @brief
   Add a value to the variant map

   @details
   This function adds a value to the variant map.

   @param varmap The map the value has to be added in
   @param key the key
   @param data the value that has to be added

   @return
    - true the value is added.
    - false an error occured
 */
bool variant_map_addInt32(variant_map_t* varmap, const char* key, int32_t data) {
    if(!varmap) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    variant_map_iterator_t* it = variant_map_iterator_createInt32(key, data);
    if(!it) {
        return false;
    }

    return variant_map_append(varmap, it);
}

/**
   @ingroup pcb_utils_variant_map
   @brief
   Add a value to the variant map

   @details
   This function adds a value to the variant map.

   @param varmap The map the value has to be added in
   @param key the key
   @param data the value that has to be added

   @return
    - true the value is added.
    - false an error occured
 */
bool variant_map_addInt64(variant_map_t* varmap, const char* key, int64_t data) {
    if(!varmap) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    variant_map_iterator_t* it = variant_map_iterator_createInt64(key, data);
    if(!it) {
        return false;
    }

    return variant_map_append(varmap, it);
}

/**
   @ingroup pcb_utils_variant_map
   @brief
   Add a value to the variant map

   @details
   This function adds a value to the variant map.

   @param varmap The map the value has to be added in
   @param key the key
   @param data the value that has to be added

   @return
    - true the value is added.
    - false an error occured
 */
bool variant_map_addUInt8(variant_map_t* varmap, const char* key, uint8_t data) {
    if(!varmap) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    variant_map_iterator_t* it = variant_map_iterator_createUInt8(key, data);
    if(!it) {
        return false;
    }

    return variant_map_append(varmap, it);
}

/**
   @ingroup pcb_utils_variant_map
   @brief
   Add a value to the variant map

   @details
   This function adds a value to the variant map.

   @param varmap The map the value has to be added in
   @param key the key
   @param data the value that has to be added

   @return
    - true the value is added.
    - false an error occured
 */
bool variant_map_addUInt16(variant_map_t* varmap, const char* key, uint16_t data) {
    if(!varmap) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    variant_map_iterator_t* it = variant_map_iterator_createUInt16(key, data);
    if(!it) {
        return false;
    }

    return variant_map_append(varmap, it);
}

/**
   @ingroup pcb_utils_variant_map
   @brief
   Add a value to the variant map

   @details
   This function adds a value to the variant map.

   @param varmap The map the value has to be added in
   @param key the key
   @param data the value that has to be added

   @return
    - true the value is added.
    - false an error occured
 */
bool variant_map_addUInt32(variant_map_t* varmap, const char* key, uint32_t data) {
    if(!varmap) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    variant_map_iterator_t* it = variant_map_iterator_createUInt32(key, data);
    if(!it) {
        return false;
    }

    return variant_map_append(varmap, it);
}

/**
   @ingroup pcb_utils_variant_map
   @brief
   Add a value to the variant map

   @details
   This function adds a value to the variant map.

   @param varmap The map the value has to be added in
   @param key the key
   @param data the value that has to be added

   @return
    - true the value is added.
    - false an error occured
 */
bool variant_map_addUInt64(variant_map_t* varmap, const char* key, uint64_t data) {
    if(!varmap) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    variant_map_iterator_t* it = variant_map_iterator_createUInt64(key, data);
    if(!it) {
        return false;
    }

    return variant_map_append(varmap, it);
}

/**
   @ingroup pcb_utils_variant_map
   @brief
   Add a value to the variant map

   @details
   This function adds a value to the variant map.

   @param varmap The map the value has to be added in
   @param key the key
   @param data the value that has to be added

   @return
    - true the value is added.
    - false an error occured
 */
bool variant_map_addDouble(variant_map_t* varmap, const char* key, double data) {
    if(!varmap) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    variant_map_iterator_t* it = variant_map_iterator_createDouble(key, data);
    if(!it) {
        return false;
    }

    return variant_map_append(varmap, it);
}

/**
   @ingroup pcb_utils_variant_map
   @brief
   Add a value to the variant map

   @details
   This function adds a value to the variant map.

   @param varmap The map the value has to be added in
   @param key the key
   @param data the value that has to be added

   @return
    - true the value is added.
    - false an error occured
 */
bool variant_map_addBool(variant_map_t* varmap, const char* key, bool data) {
    if(!varmap) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    variant_map_iterator_t* it = variant_map_iterator_createBool(key, data);
    if(!it) {
        return false;
    }

    return variant_map_append(varmap, it);
}

/**
   @ingroup pcb_utils_variant_map
   @brief
   Add a value to the variant map

   @details
   This function adds a value to the variant map.

   @param varmap The map the value has to be added in
   @param key the key
   @param data the value that has to be added

   @return
    - true the value is added.
    - false an error occured
 */
bool variant_map_addDateTime(variant_map_t* varmap, const char* key, const struct tm* data) {
    if(!varmap) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    variant_map_iterator_t* it = variant_map_iterator_createDateTime(key, data);
    if(!it) {
        return false;
    }

    return variant_map_append(varmap, it);
}

/**
   @ingroup pcb_utils_variant_map
   @brief
   Add a value to the variant map

   @details
   This function adds a value to the variant map.

   @param varmap The map the value has to be added in
   @param key the key
   @param data the value that has to be added

   @return
    - true the value is added.
    - false an error occured
 */
bool variant_map_addDateTimeExtended(variant_map_t* varmap, const char* key, const pcb_datetime_t* data) {
    if(!varmap) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    variant_map_iterator_t* it = variant_map_iterator_createDateTimeExtended(key, data);
    if(!it) {
        return false;
    }

    return variant_map_append(varmap, it);
}

/**
   @ingroup pcb_utils_variant_map
   @brief
   Add a value to the variant map

   @details
   This function adds a value to the variant map.

   @param varmap The map the value has to be added in
   @param key the key
   @param data the value that has to be added

   @return
    - true the value is added.
    - false an error occured
 */
bool variant_map_addListMove(variant_map_t* varmap, const char* key, variant_list_t* data) {
    if(!varmap) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    variant_map_iterator_t* it = variant_map_iterator_createListMove(key, data);
    if(!it) {
        return false;
    }

    return variant_map_append(varmap, it);
}

/**
   @ingroup pcb_utils_variant_map
   @brief
   Add a value to the variant map

   @details
   This function adds a value to the variant map.

   @param varmap The map the value has to be added in
   @param key the key
   @param data the value that has to be added

   @return
    - true the value is added.
    - false an error occured
 */
bool variant_map_addListCopy(variant_map_t* varmap, const char* key, const variant_list_t* data) {
    if(!varmap) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    variant_map_iterator_t* it = variant_map_iterator_createListCopy(key, data);
    if(!it) {
        return false;
    }

    return variant_map_append(varmap, it);
}

/**
   @ingroup pcb_utils_variant_map
   @brief
   Add a value to the variant map

   @details
   This function adds a value to the variant map.

   @param varmap The map the value has to be added in
   @param key the key
   @param data the value that has to be added

   @return
    - true the value is added.
    - false an error occured
 */
bool variant_map_addListRef(variant_map_t* varmap, const char* key, variant_list_t* data) {
    if(!varmap) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    variant_map_iterator_t* it = variant_map_iterator_createListRef(key, data);
    if(!it) {
        return false;
    }

    return variant_map_append(varmap, it);
}

/**
   @ingroup pcb_utils_variant_map
   @brief
   Add a value to the variant map

   @details
   This function adds a value to the variant map.

   @param varmap The map the value has to be added in
   @param key the key
   @param data the value that has to be added

   @return
    - true the value is added.
    - false an error occured
 */
bool variant_map_addMapMove(variant_map_t* varmap, const char* key, variant_map_t* data) {
    if(!varmap) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    variant_map_iterator_t* it = variant_map_iterator_createMapMove(key, data);
    if(!it) {
        return false;
    }

    return variant_map_append(varmap, it);
}

/**
   @ingroup pcb_utils_variant_map
   @brief
   Add a value to the variant map

   @details
   This function adds a value to the variant map.

   @param varmap The map the value has to be added in
   @param key the key
   @param data the value that has to be added

   @return
    - true the value is added.
    - false an error occured
 */
bool variant_map_addMapCopy(variant_map_t* varmap, const char* key, const variant_map_t* data) {
    if(!varmap) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    variant_map_iterator_t* it = variant_map_iterator_createMapCopy(key, data);
    if(!it) {
        return false;
    }

    return variant_map_append(varmap, it);
}

/**
   @ingroup pcb_utils_variant_map
   @brief
   Add a value to the variant map

   @details
   This function adds a value to the variant map.

   @param varmap The map the value has to be added in
   @param key the key
   @param data the value that has to be added

   @return
    - true the value is added.
    - false an error occured
 */
bool variant_map_addMapRef(variant_map_t* varmap, const char* key, variant_map_t* data) {
    if(!varmap) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    variant_map_iterator_t* it = variant_map_iterator_createMapRef(key, data);
    if(!it) {
        return false;
    }

    return variant_map_append(varmap, it);
}

/**
   @ingroup pcb_utils_variant_map
   @brief
   Get the first variant map element.

   @details
   This function returns the first variant map element of the variant map. \n
   This function calls @ref llist_first to get the specified element, @ref llist_item_data
   is used to get a pointer to the variant map iterator\n

   @param map the variant map

   @return
    - a <b>pointer</b> to the first variant map element of the variant map.
    - NULL if the variant map is empty.
    - NULL if the variant map parameter is NULL.
 */
variant_map_iterator_t* variant_map_first(const variant_map_t* map) {
    if(!map) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    llist_iterator_t* ll_item = llist_first(map);
    if(!ll_item) {
        return NULL;
    }

    return llist_item_data(ll_item, variant_map_iterator_t, llist_it);
}

/**
   @ingroup pcb_utils_variant_map
   @brief
   Get the last variant map element.

   @details
   This function returns the last variant map element of the variant map. \n
   This function calls @ref llist_last to get the specified element, @ref llist_item_data
   is used to get a pointer to the variant map iterator\n

   @param map the variant map

   @return
    - a <b>pointer</b> to the last variant map element of the variant map.
    - NULL if the variant map is empty.
    - NULL if the variant map parameter is NULL.
 */
variant_map_iterator_t* variant_map_last(const variant_map_t* map) {
    if(!map) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    llist_iterator_t* ll_item = llist_last(map);
    if(!ll_item) {
        return NULL;
    }
    return llist_item_data(ll_item, variant_map_iterator_t, llist_it);
}

/**
   @ingroup pcb_utils_variant_map
   @brief
   Get a pointer to the specified variant map element.

   @details
   This function returns a pointer to the variant map element at the specified index of the variant map. \n
   Due to the nature of a linked list the items are not garanteed to be at the same place all the time.\n
   Use this function with care, it can have performance impact, the variant map is iterated to find the correct element.\n
   This function calls @ref llist_at to get the specified element, @ref llist_item_data
   is used to get a pointer to the variant list iterator\n

   @param map the map
   @param index the item # in which you are interested (start counting from 0)

   @return
    - a <b>pointer</b> to the specified variant list element in the variant linked list.
    - NULL if the element does not exist.
    - NULL if the list parameter is NULL.
 */
variant_map_iterator_t* variant_map_at(const variant_map_t* map, unsigned int index) {
    if(!map) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    llist_iterator_t* ll_item = llist_at(map, index);
    if(!ll_item) {
        return NULL;
    }
    return llist_item_data(ll_item, variant_map_iterator_t, llist_it);
}

/**
   @ingroup pcb_utils_variant_map
   @brief
   Get a pointer to the next variant map element.

   @details
   This function returns a pointer to the next variant map element. \n
   This function calls @ref llist_iterator_next to get the next element, @ref llist_item_data
   is used to get a pointer to the variant list iterator\n

   @param it the map element

   @return
    - a <b>pointer</b> to the next variant map element in the linked list.
    - NULL if the it parameter is NULL.
    - NULL if the end of the variant map was reached
 */
variant_map_iterator_t* variant_map_iterator_next(const variant_map_iterator_t* it) {
    if(!it) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    llist_iterator_t* ll_item = llist_iterator_next(&it->llist_it);
    if(!ll_item) {
        return NULL;
    }
    return llist_item_data(ll_item, variant_map_iterator_t, llist_it);
}

/**
   @ingroup pcb_utils_variant_map
   @brief
   Get a pointer to the previous variant map element.

   @details
   This function returns a pointer to the previous variant map element. \n
   This function calls @ref llist_iterator_prev to get the previous element, @ref llist_item_data
   is used to get a pointer to the variant list iterator\n

   @param it the list element

   @return
    - a <b>pointer</b> to the previous variant map element in the linked list.
    - NULL if the it parameter is NULL.
    - NULL if the beginning of the variant map was reached
 */
variant_map_iterator_t* variant_map_iterator_prev(const variant_map_iterator_t* it) {
    if(!it) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    llist_iterator_t* ll_item = llist_iterator_prev(&it->llist_it);
    if(!ll_item) {
        return NULL;
    }
    return llist_item_data(ll_item, variant_map_iterator_t, llist_it);
}

/**
   @ingroup pcb_utils_variant_map
   @brief
   Append the element to a variant map.

   @details
   This function appends the specified variant map element to an existing variant list. If the element was part of
   another variant map then the element will be automatically removed from that variant map.\n
   This function calls @ref llist_append\n

   @param map the variant map to which the element has to be added
   @param insert the variant map element that has to be attached to the variant map

   @return
    - true: The map element was added succesfully appended to the map
    - false: An error has occurred
        - map, key or insert is a NULL pointer
 */
bool variant_map_append(variant_map_t* map, variant_map_iterator_t* insert) {
    if(!map || !insert) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    return llist_append(map, &insert->llist_it);
}

/**
   @ingroup pcb_utils_variant_map
   @brief
   Prepend the element to a variant map.

   @details
   This function prepends the specified variant map element to an existing variant map. If the element was part of
   another variant map then the element will be automatically removed from that variant map.\n
   This function calls @ref llist_prepend\n

   @param map the variant map to which the element has to be added
   @param insert the variant map element that has to be attached to the variant map

   @return
    - true: The map element was added succesfully prepended to the map
    - false: An error has occurred
        - map, key or insert is a NULL pointer
 */
bool variant_map_prepend(variant_map_t* map, variant_map_iterator_t* insert) {
    if(!map || !insert) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    return llist_prepend(map, &insert->llist_it);
}

/**
   @ingroup pcb_utils_variant_map
   @brief
   Attach the element to a variant map at a certain index.

   @details
   This function attaches the specified variant map element to an existing variant map. If the element is part of
   another variant map then the element will be automatically removed from that variant map.\n
   This function calls @ref llist_insertAt\n

   @param map the variant list to which the element has to be added
   @param index the item # where to add the element to the list (start counting from 0)
   @param insert the variant list element that has to be attached to the variant list

   @return
    - true: The variant list element was succesfully inserted into the variant list
    - false: An error has occurred
        - list or insert is a NULL pointer
        - index element does not exist
        - the insert element is the head or tail element of a list
 */
bool variant_map_insertAt(variant_map_t* map, uint32_t index, variant_map_iterator_t* insert) {
    if(!map || !insert) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    return llist_insertAt(map, index, &insert->llist_it);
}

/**
   @ingroup pcb_utils_variant_map
   @brief
   Detach the element from a variant map.

   @details
   This function detaches the specified variant map element from an existing variant map.\n
   This function calls @ref llist_iterator_take to remove the item from the linked list,
   @ref llist_item_data is used to get a pointer to the variant list iterator.\n

   @param it the variant map element that has to be detached from the variant map

   @return
    - a <b>pointer</b> to the detached variant map element.
    - NULL if an error occured
        - it is NULL
 */
variant_map_iterator_t* variant_map_iterator_take(variant_map_iterator_t* it) {
    if(!it) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    llist_iterator_t* ll_item = llist_iterator_take(&it->llist_it);
    return llist_item_data(ll_item, variant_map_iterator_t, llist_it);
}

/**
   @ingroup pcb_utils_variant_map
   @brief
   Detach the first element from a variant map.

   @details
   This function detaches the first variant map element from an existing variant map.\n
   This function calls @ref llist_takeFirst to get the first map element and @ref
   llist_item_data to get a pointer to the variant list iterator.\n

   @param map the list from which the first element has to be detached

   @return
    - a <b>pointer</b> to the detached variant map element.
    - NULL if an error occured
        - list is NULL
        - the list is empty
 */
variant_map_iterator_t* variant_map_takeFirst(variant_map_t* map) {
    if(!map) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    llist_iterator_t* ll_item = llist_takeFirst(map);
    if(!ll_item) {
        return NULL;
    }
    return llist_item_data(ll_item, variant_map_iterator_t, llist_it);
}

/**
   @ingroup pcb_utils_variant_map
   @brief
   Detach the last element from a variant map.

   @details
   This function detaches the last variant map element from an existing variant map.\n
   This function calls @ref llist_takeLast to get the last list element and @ref
   llist_item_data to get a pointer to the variant list iterator\n

   @param map the map from which the last element has to be detached

   @return
    - a <b>pointer</b> to the detached variant map element.
    - NULL if an error occured
        - list is NULL
        - the list is empty
 */
variant_map_iterator_t* variant_map_takeLast(variant_map_t* map) {
    if(!map) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    llist_iterator_t* ll_item = llist_takeLast(map);
    if(!ll_item) {
        return NULL;
    }
    return llist_item_data(ll_item, variant_map_iterator_t, llist_it);
}

/**
   @ingroup pcb_utils_variant_map
   @brief
   Get the size of the variant map.

   @details
   This function returns the size of the variant map.\n
   This function calls @ref llist_size\n

   @param map the list in wich you are interested

   @return
    - the number of elements in the map.
    - 0 if map is a NULL pointer.
 */
unsigned int variant_map_size(const variant_map_t* map) {
    return llist_size(map);
}

/**
   @ingroup pcb_utils_variant_map
   @brief
   Determine if the variant map is empty.

   @details
   This function checks wether the  variant map is empty or not.\n
   This function calls @ref llist_isEmpty\n

   @param map the map in wich you are interested

   @return
    - true:
        - The list is empty
        - list == NULL
    - false: The list is not empty
 */
bool variant_map_isEmpty(const variant_map_t* map) {
    return llist_isEmpty(map);
}

/**
   @ingroup pcb_utils_variant_map
   @brief
   Determine if the variant map contains a certain variant with a certain key.

   @details
   This function checks wether the variant map contains the specified key or not.\n
   This function calls @ref variant_map_for_each and compares the given variant using the
   @ref variant_compare functionality with all list items.\n

   @param map the list in wich you are interested
   @param key the key you want to find

   @return
    - true:
        - The element is part of the list
    - false: pcb_error is set to the correct error code
        - The list pointer or data pointer is invalid
        - The element is not found,
 */
bool variant_map_contains(const variant_map_t* map, const char* key) {
    if(!map || !key || !(*key)) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    variant_map_iterator_t* it = NULL;
    variant_map_for_each(it, map) {
        if(strcmp(key, it->key) == 0) {
            break;
        }
    }

    if(it) {
        return true;
    } else {
        pcb_error = pcb_error_element_not_found;
        return false;
    }
}

variant_map_iterator_t* variant_map_find(const variant_map_t* map, const char* key) {
    if(!map || !key || !(*key)) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    variant_map_iterator_t* it = NULL;
    variant_map_for_each(it, map) {
        if(strcmp(key, it->key) == 0) {
            break;
        }
    }

    if(it) {
        return it;
    } else {
        pcb_error = pcb_error_element_not_found;
        return NULL;
    }
}

/**
   @ingroup pcb_utils_variant_map
   @brief
   Compare two variant maps.

   @details
   This function compares two variant maps.\n

   First it does a comparison on pointer level:\n
   @li both variant maps are null pointers: result = 0
   @li only first variant map is null pointer: result = -1
   @li only second variant map is null pointer: result = 1
   Then it compares the sizes of the 2 variant maps:\n
   @li if the first variant map has more items than the second one: result > 0
   @li if the first variant map has less items than the second one: result < 0
   @li if they have the same size, the process continues by comparing individual map items.
   For all items in the first variant map, the corresponding item (= with the same key) is looked up in the second map. If it is not found, the comparison terminates with result = 1. If the corresponding item is found, the 2 items are compared. If they are different, the comparison terminates with result inherited from the item comparison. If they are equal, the process continues with the next item until the last item of the first variant map. If all items are equal, the variant maps are considered to be equal and result = 0.
   Remark: variant maps are the only kind of variants that do not form an ordered set.

   @param map1 variant map 1
   @param map2 variant map 2
   @param result the result of the comparison

   @return
    - true: the comparison was a succes, result contains the resulting value
    - false: An error has occurred, pcb_error contains the reason why the set failed
        - an invalid result pointer was provided
        - comparison between individual map items failed
 */
bool variant_map_compare(const variant_map_t* map1, const variant_map_t* map2, int* result) {
    if(!result) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(!map1 && !map2) {
        *result = 0;
        return true;
    }
    if(!map1) {
        *result = -1;
        return true;
    }
    if(!map2) {
        *result = 1;
        return true;
    }

    *result = variant_map_size(map1) - variant_map_size(map2);
    if(*result) {
        return true;
    }

    variant_map_iterator_t* it1, * it2;
    variant_map_for_each(it1, map1) {
        it2 = variant_map_find(map2, variant_map_iterator_key(it1));
        if(!it2) {
            *result = 1;
            return true;
        }
        if(!variant_compare(variant_map_iterator_data(it1), variant_map_iterator_data(it2), result)) {
            return false;
        }
        if(*result) {
            return true;
        }
    }
    return true;
}
