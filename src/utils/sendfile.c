/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#define _GNU_SOURCE
#include <errno.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <sys/sendfile.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <unistd.h>

#include <debug/sahtrace.h>
#include <pcb/utils.h>

#include "sendfileconfig.h"
#include "pcb/utils/sendfile.h"

#define ME                  "pcb-sendFile"

#define COPY_BUFFER_SIZE    (8 * 1024)

typedef struct _peer_sendFileContext {
    peer_info_t* peer;
    int fd;
    size_t length;
    off_t offset;
    ssize_t (* fileSend)(peer_info_t* peer, int fd, off_t* offset, size_t length);

    void* peerUserData;
    peer_event_handler_t write;
    peer_event_handler_t writeDone;
} peer_sendFileContext_t;

static void peer_sendFileContextCleanup(peer_sendFileContext_t* const ctx) {
    if(ctx) {
        peer_info_t* peer = ctx->peer;

        if(ctx->fd >= 0) {
            close(ctx->fd);
            ctx->fd = -1;
        }

        peer_setUserData(peer, ctx->peerUserData);
        peer_setEventHandler(peer, peer_event_write, peer_handler_write_always, ctx->write);
        peer_setEventHandler(peer, peer_event_write_done, peer_handler_write_always, ctx->writeDone);

        free(ctx);
    }
}

static ssize_t peer_writeWithSendfile(peer_info_t* const peer, int const fd, off_t* const offset, size_t const length) {
    ssize_t written;
    int err;

    SAH_TRACEZ_IN(ME);

    errno = 0;
    written = sendfile(peer_getFd(peer), fd, offset, length);

    err = errno;
    if(written < 0) {
        SAH_TRACEZ_ERROR(ME, "unable to execute sendfile: %s", strerror(errno));
    }
    errno = err;

    SAH_TRACEZ_OUT(ME);
    return written;
}

#ifdef HAVE_SPLICE
static ssize_t peer_writeWithSplice(peer_info_t* const peer, int const fd, off_t* const offset, size_t const length) {
    ssize_t written;
    SAH_TRACEZ_IN(ME);

    errno = 0;
    if(offset) {
        loff_t off = (loff_t) *offset;
        written = splice(fd, &off, peer_getFd(peer), NULL,
                         length, SPLICE_F_MOVE | SPLICE_F_NONBLOCK | SPLICE_F_MORE);
        *offset = (off_t) off;
    } else {
        written = splice(fd, NULL, peer_getFd(peer), NULL,
                         length, SPLICE_F_MOVE | SPLICE_F_NONBLOCK | SPLICE_F_MORE);
    }
    if(written < 0) {
        int err = errno;
        SAH_TRACEZ_ERROR(ME, "unable to execute splice: %s", strerror(errno));
        errno = err;
    }

    SAH_TRACEZ_OUT(ME);
    return written;
}
#endif

static ssize_t peer_writeByCopy(peer_info_t* const peer, int const fd, off_t* const offset, size_t const length) {
    char buffer[COPY_BUFFER_SIZE];
    ssize_t w;
    ssize_t r;

    SAH_TRACEZ_IN(ME);

    if(offset) {
        lseek(fd, *offset, SEEK_SET);
    }

    errno = 0;
    r = read(fd, buffer, length < COPY_BUFFER_SIZE ? length : COPY_BUFFER_SIZE);
    if(r < 0) {
        int err = errno;
        SAH_TRACEZ_ERROR(ME, "unable to execute read: %s", strerror(errno));
        errno = err;
        w = r;
    } else {
        SAH_TRACEZ_INFO(ME, "read %zd bytes from file", r);
        if(r > 0) {
            /* purpose of sendFile is to send file which can be really big, so
             * we can't use buffered io nor auto io, as in these case pcb will
             * take care of buffering what was not send when the output fd buffer
             * gets filled. So we use direct io in order to know when the buffer is
             * full, and do other thing while the buffer is being sent by the kernel */
            FILE* stream = peer_outStream(peer, peer_stream_direct_io);
            w = fwrite(buffer, sizeof(char), r, stream);
            if(w != r) {
                if(w <= 0) {
                    w = -1;
                    int err = errno;
                    if((errno != EAGAIN) && (errno != EWOULDBLOCK)) {
                        SAH_TRACEZ_ERROR(ME, "unable to execute write: %s", strerror(errno));
                    }
                    errno = err;
                } else {
                    // we have written less data than we have read
                    r -= w;
                }
                lseek(fd, -(r), SEEK_CUR);
            }

            if(w > 0) {
                fflush(stream);
                if(offset) {
                    *offset += w;
                }
            }
        } else {
            w = 0;
        }
    }

    SAH_TRACEZ_OUT(ME);
    return w;
}

static bool peer_handleSendFileClose(peer_info_t* const peer) {
    peer_sendFileContext_t* ctx;
    SAH_TRACEZ_IN(ME);

    SAH_TRACEZ_INFO(ME, "cleaning sendFile Context");
    ctx = (peer_sendFileContext_t*) peer_getUserData(peer);
    peer_sendFileContextCleanup(ctx);

    SAH_TRACEZ_OUT(ME);
    return true;
}

static bool peer_handleSendFileDone(peer_info_t* const peer);

static bool peer_handleSendFile(peer_info_t* const peer) {
    peer_sendFileContext_t* ctx;
    ssize_t written;
    size_t length;
    off_t offset;

    SAH_TRACEZ_IN(ME);

    ctx = (peer_sendFileContext_t*) peer_getUserData(peer);
    length = ctx->length;
    offset = ctx->offset;

    while(length > 0) {
        if(ctx->fileSend == NULL) {
            SAH_TRACEZ_NOTICE(ME, "detecting send method");

            /* detect which method to use */
            do {
                if(peer_capabilities(peer) & peer_cap_direct_fd_access) {
                    SAH_TRACEZ_INFO(ME, "trying send file");
                    written = peer_writeWithSendfile(peer, ctx->fd, &offset, length);
                    if(written >= 0) {
                        SAH_TRACEZ_NOTICE(ME, "using sendfile to send fd");
                        ctx->fileSend = peer_writeWithSendfile;
                        break;
                    }

#ifdef HAVE_SPLICE
                    SAH_TRACEZ_INFO(ME, "trying splice");
                    written = peer_writeWithSplice(peer, ctx->fd, &offset, length);
                    if(written >= 0) {
                        SAH_TRACEZ_NOTICE(ME, "using splice to send fd");
                        ctx->fileSend = peer_writeWithSplice;
                        break;
                    }
#endif
                }

                SAH_TRACEZ_INFO(ME, "trying copy");
                written = peer_writeByCopy(peer, ctx->fd, &offset, length);
                if(written >= 0) {
                    SAH_TRACEZ_NOTICE(ME, "using buffer copy to send fd");
                    ctx->fileSend = peer_writeByCopy;
                    break;
                }
            } while(0);
        } else {
            written = ctx->fileSend(peer, ctx->fd, &offset, length);
        }

        if((written < 0) || errno) {
            if((errno == EAGAIN) || (errno == EWOULDBLOCK)) {
                SAH_TRACEZ_INFO(ME, "need to send more data");
                SAH_TRACEZ_OUT(ME);
                return true;
            }

            SAH_TRACEZ_ERROR(ME, "unable to write to socket: %s", strerror(errno));
            break;
        }

        length -= written;
        ctx->length = length;
        ctx->offset = offset;
    }

    /* if we could send every thing */
    if(length == 0) {
        SAH_TRACEZ_NOTICE(ME, "file datas sent");

        if(!(peer->sendBuffer && (circbuf_availableForRead(peer->sendBuffer) > 0))) {
            bool ret = peer_handleSendFileDone(peer);
            SAH_TRACEZ_OUT(ME);
            return ret;
        }
        SAH_TRACEZ_OUT(ME);
        return true;
    }

    SAH_TRACEZ_INFO(ME, "cleanup context");
    peer_sendFileContextCleanup(ctx);
    peer_removeCloseHandler(peer, &peer_handleSendFileClose);

    return false;
}

static bool peer_handleSendFileDone(peer_info_t* const peer) {
    peer_sendFileContext_t* ctx;
    SAH_TRACEZ_IN(ME);

    ctx = (peer_sendFileContext_t*) peer_getUserData(peer);
    if(ctx->length > 0) {
        SAH_TRACEZ_NOTICE(ME, "sending file datas");
        if(!peer_setStreamType(peer, peer_stream_direct_io)) {
            SAH_TRACEZ_ERROR(ME, "unable to switch stream to direct io");
            return false;
        }

        // flush tcp socket and set TCP_CORK flag
        int state = 0;
        setsockopt(peer_getFd(peer), IPPROTO_TCP, TCP_CORK, &state, sizeof(state));
        state = 1;
        setsockopt(peer_getFd(peer), IPPROTO_TCP, TCP_CORK, &state, sizeof(state));

        peer_setEventHandler(peer, peer_event_write, peer_handler_write_always, &peer_handleSendFile);
        SAH_TRACEZ_OUT(ME);
        return true;
    }

    // flush tcp socket by removing TCP_CORK flag
    int state = 0;
    setsockopt(peer_getFd(peer), IPPROTO_TCP, TCP_CORK, &state, sizeof(state));

    // all datas are sent, restore event handlers and cleanup context
    peer_event_handler_t writeDone = ctx->writeDone;
    peer_sendFileContextCleanup(ctx);
    peer_removeCloseHandler(peer, &peer_handleSendFileClose);

    if(!peer_setStreamType(peer, peer_stream_buffered_io)) {
        SAH_TRACEZ_ERROR(ME, "unable to switch stream to buffered io");
        return false;
    }

    SAH_TRACEZ_OUT(ME);
    return writeDone(peer);
}

/**
   @ingroup pcb_utils_sendfile
   @brief
   TODO

   @details
   TODO

   @param peer TODO
   @param fd TODO
   @param offset TODO
   @param length TODO

   @return
    TODO
 */
int peer_sendFileFd(peer_info_t* const peer, int fd, off_t const offset, size_t length) {
    struct stat sb;
    peer_sendFileContext_t* ctx;
    int ret;

    SAH_TRACEZ_IN(ME);

    if(fd < 0) {
        SAH_TRACEZ_ERROR(ME, "invalid file descriptor");
        return -1;
    }

    if((fstat(fd, &sb) < 0) || !S_ISREG(sb.st_mode)) {
        SAH_TRACEZ_ERROR(ME, "fd is not backed by a file");
        return -1;
    }

    if((offset >= sb.st_size) || (length && (((uint64_t) offset + (uint64_t) length) > (uint64_t) sb.st_size))) {
        SAH_TRACEZ_ERROR(ME, "Invalid parameters, offset + size is greater than total file size");
        return -1;
    }
    if(!length) {
        // take all the remaining size
        length = sb.st_size - offset;
    }

    fd = dup(fd);
    if(fd < -1) {
        SAH_TRACEZ_ERROR(ME, "failed to duplicate file descriptor : %m");
        return -1;
    }

    ctx = (peer_sendFileContext_t*) calloc(1, sizeof(peer_sendFileContext_t));
    if(!ctx) {
        SAH_TRACEZ_ERROR(ME, "memory allocation failed");
        close(fd);
        return -1;
    }

    ctx->peer = peer;
    ctx->fd = fd;
    ctx->offset = offset;
    ctx->length = length;
    ctx->peerUserData = peer_getUserData(peer);
    ctx->write = peer->write;
    ctx->writeDone = peer->writeDone;

    peer_setUserData(peer, ctx);
    peer_setEventHandler(peer, peer_event_write_done, peer_handler_write_always, &peer_handleSendFileDone);
    peer_addCloseHandler(peer, &peer_handleSendFileClose);
    ret = 0;

    if(!peer->sendBuffer || (circbuf_availableForRead(peer->sendBuffer) <= 0)) {
        // no buffer set, or buffer empty, so the writeDone
        // handler won't be called automatically. call it now.
        if(!peer_handleSendFileDone(peer)) {
            peer_sendFileContextCleanup(ctx);
            peer_removeCloseHandler(peer, &peer_handleSendFileClose);

            ret = -1;
        }
    }

    SAH_TRACEZ_OUT(ME);
    return ret;
}
