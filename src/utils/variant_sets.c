/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <string.h>

#include <pcb/common/error.h>
#include <pcb/utils/variant_sets.h>

/**
   @file
   @brief
   Implementation of the public variant sets functions
 */

typedef bool (* variant_map_iterate_cb)(variant_map_iterator_t* a, variant_map_iterator_t* b, bool used_pk, void* userdata);
typedef bool (* variant_list_iterate_cb)(variant_list_iterator_t* a, variant_list_iterator_t* b, bool used_pk, void* userdata);

typedef struct _variant_sets_union_iterate_t {
    void* dst;
    bool update;
    bool descend;
    unsigned int free;
} variant_sets_union_iterate_t;

typedef struct _variant_sets_intersection_iterate_t {
    bool relComplement;
    bool descend;
    bool verifyOnly;
} variant_sets_intersection_iterate_t;

static bool variant_sets_union_getPKName(const char* pk_name_a, const char* pk_name_b, char** pk_name) {
    if(!pk_name) {
        return false;
    }

    if(pk_name_a && pk_name_b && !strcmp(pk_name_a, pk_name_b)) {
        *pk_name = strdup(pk_name_a);
        return true;
    }

    return (!pk_name_a && !pk_name_b);
}

static const char* variant_sets_getPKValue(const variant_t* var, const char* pk_name) {
    const char* pk_value = NULL;

    if(!var || (variant_type(var) != variant_type_map) || !pk_name || !*pk_name) {
        return NULL;
    }

    pk_value = variant_map_da_findChar(variant_da_map(var), pk_name);
    if(pk_value && *pk_value) {
        return pk_value;
    }

    return NULL;
}

static bool variant_sets_relComplement_data(variant_t* a, variant_t* b, bool descend, bool verifyOnly, bool* handled) {
    *handled = false;

    /* If descend into children and both of type map or array: intersection contents */
    if(descend && (variant_type(b) == variant_type(a))) {
        switch(variant_type(b)) {
        case variant_type_map:
        case variant_type_array:
            *handled = true;
            return variant_sets_intersection(a, b, descend, verifyOnly);
            break;
        default:
            break;
        }
    }

    return true;
}

static bool variant_sets_intersection_data(variant_t* a, variant_t* b, bool descend, bool verifyOnly) {
    /* If descend into children and both of type map or array: intersection contents */
    if(descend && (variant_type(b) == variant_type(a))) {
        switch(variant_type(b)) {
        case variant_type_map:
        case variant_type_array:
            return variant_sets_intersection(a, b, descend, verifyOnly);
            break;
        default:
            break;
        }
    }

    return true;
}

static bool variant_sets_union_data(variant_t* a, variant_t* b, bool update, bool descend) {
    /* If descend into children and both of type map or array: union contents */
    if(descend && (variant_type(b) == variant_type(a))) {
        switch(variant_type(b)) {
        case variant_type_map:
        case variant_type_array:
            return variant_sets_union(a, b, update, descend);
            break;
        default:
            break;
        }
    }

    /* Update contents */
    if(update) {
        variant_copy(a, b);
    }

    return true;
}

static variant_list_iterator_t* variant_list_findByPK(const variant_list_t* list, const char* pk_name, const char* pk_value) {
    const char* value = NULL;
    variant_list_iterator_t* iter = NULL;

    if(!list || !pk_name || !*pk_name || !pk_value || !*pk_value) {
        return NULL;
    }

    variant_list_for_each(iter, list) {
        value = variant_sets_getPKValue(variant_list_iterator_data(iter), pk_name);

        if(value && !strcmp(value, pk_value)) {
            return iter;
        }
    }

    return NULL;
}

static variant_map_iterator_t* variant_map_findByPK(const variant_map_t* map, const char* pk_name, const char* pk_value) {
    const char* value = NULL;
    variant_map_iterator_t* iter = NULL;

    if(!map || !pk_name || !*pk_name || !pk_value || !*pk_value) {
        return NULL;
    }

    variant_map_for_each(iter, map) {
        value = variant_sets_getPKValue(variant_map_iterator_data(iter), pk_name);

        if(value && !strcmp(value, pk_value)) {
            return iter;
        }
    }

    return NULL;
}

static const char* variant_map_nextKey(variant_map_t* map, unsigned int* nr, char* key, size_t key_size) {
    unsigned int i = 0;
    if(!map || !nr || !key || !key_size) {
        return NULL;
    }

    for(i = *nr + 1; i != 0; i++) {
        snprintf(key, key_size, "%u", i);

        if(variant_map_find(map, key) == NULL) {
            *nr = i;
            return key;
        }
    }

    return NULL;
}

static bool variant_map_intersection_iterateCB(variant_map_iterator_t* a, variant_map_iterator_t* b, bool used_pk, void* userdata) {
    bool handled = true;
    variant_sets_intersection_iterate_t* data = (variant_sets_intersection_iterate_t*) userdata;

    (void) used_pk;

    if(!b) {
        if(!data->relComplement) {
            /* Intersection: destroy 'a' */
            if(data->verifyOnly) {
                pcb_error = pcb_error_not_found;
                return false;
            } else {
                variant_map_iterator_destroy(a);
            }
        }
        /* else Relative Complement: keep 'a' */
    } else {
        if(!data->relComplement) {
            /* Intersection: keep 'a' */
            return variant_sets_intersection_data(variant_map_iterator_data(a), variant_map_iterator_data(b), data->descend, data->verifyOnly);
        } else {
            /* Relative Complement: destroy 'a' */
            if(!variant_sets_relComplement_data(variant_map_iterator_data(a), variant_map_iterator_data(b), data->descend, data->verifyOnly, &handled)) {
                return false;
            }

            if(!handled) {
                if(data->verifyOnly) {
                    pcb_error = pcb_error_not_found;
                    return false;
                } else {
                    variant_map_iterator_destroy(a);
                }
            }
        }
    }

    return true;
}

static bool variant_map_union_iterateCB(variant_map_iterator_t* a, variant_map_iterator_t* b, bool used_pk, void* userdata) {
    const char* key = NULL;
    char key_dst[15] = "";
    variant_sets_union_iterate_t* data = (variant_sets_union_iterate_t*) userdata;

    if(!b) {
        /* Not yet in destination, copy from source */
        if(used_pk) {
            /* Find next free key (unsigned int-based) */
            key = variant_map_nextKey((variant_map_t*) data->dst, &(data->free), key_dst, sizeof(key_dst));
        } else {
            key = variant_map_iterator_key(a);
        }

        return variant_map_add((variant_map_t*) data->dst, key, variant_map_iterator_data(a));
    } else {
        /* Switch 'a' and 'b' again (see variant_map_union()) */
        return variant_sets_union_data(variant_map_iterator_data(b), variant_map_iterator_data(a), data->update, data->descend);
    }
}

static bool variant_map_iterate(const variant_map_t* a, const variant_map_t* b, variant_map_iterate_cb cb, void* userdata) {
    bool retval = false;
    char* pk_name = NULL;
    const char* key = NULL, * pk_value = NULL;
    variant_map_iterator_t* iter_a = NULL, * iter_b = NULL, * iter_next = NULL;

    if(!cb) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    /* Retrieve name of Primary Key parameter (if available) */
    variant_sets_union_getPKName(variant_map_getPKName(a), variant_map_getPKName(b), &pk_name);

    for(iter_a = variant_map_first(a); iter_a; iter_a = iter_next) {
        iter_next = variant_map_iterator_next(iter_a);

        key = variant_map_iterator_key(iter_a);

        /* Skip Primary Key name indicator */
        if(!strcmp(key, VARIANT_SETS_PK_INDICATOR)) {
            continue;
        }

        /* Select destination based upon PK or member key */
        if(pk_name) {
            pk_value = variant_sets_getPKValue(variant_map_iterator_data(iter_a), pk_name);
            iter_b = variant_map_findByPK(b, pk_name, pk_value);
        } else {
            iter_b = variant_map_find(b, key);
        }

        if(!cb(iter_a, iter_b, (pk_name && pk_value), userdata)) {
            goto error;
        }
    }

    retval = true;

error:
    free(pk_name);
    return retval;
}

static bool variant_list_intersection_iterateCB(variant_list_iterator_t* a, variant_list_iterator_t* b, bool used_pk, void* userdata) {
    bool handled = true;
    variant_sets_intersection_iterate_t* data = (variant_sets_intersection_iterate_t*) userdata;

    (void) used_pk;

    if(!b) {
        if(!data->relComplement) {
            /* Intersection: destroy 'a' */
            if(data->verifyOnly) {
                pcb_error = pcb_error_not_found;
                return false;
            } else {
                variant_list_iterator_destroy(a);
            }
        }
        /* else Relative Complement: keep 'a' */
    } else {
        if(!data->relComplement) {
            /* Intersection: keep 'a' */
            return variant_sets_intersection_data(variant_list_iterator_data(a), variant_list_iterator_data(b), data->descend, data->verifyOnly);
        } else {
            /* Relative Complement: destroy 'a' */
            if(!variant_sets_relComplement_data(variant_list_iterator_data(a), variant_list_iterator_data(b), data->descend, data->verifyOnly, &handled)) {
                return false;
            }

            if(!handled) {
                if(data->verifyOnly) {
                    pcb_error = pcb_error_not_found;
                    return false;
                } else {
                    variant_list_iterator_destroy(a);
                }
            }
        }
    }

    return true;
}

static bool variant_list_union_iterateCB(variant_list_iterator_t* a, variant_list_iterator_t* b, bool used_pk, void* userdata) {
    variant_sets_union_iterate_t* data = (variant_sets_union_iterate_t*) userdata;

    (void) used_pk;

    if(!b) {
        /* Not yet in destination, copy from source */
        return variant_list_add((variant_list_t*) data->dst, variant_list_iterator_data(a));
    } else {
        /* Switch 'a' and 'b' again (see variant_map_union()) */
        return variant_sets_union_data(variant_list_iterator_data(b), variant_list_iterator_data(a), data->update, data->descend);
    }
}

static bool variant_list_iterate(const variant_list_t* a, const variant_list_t* b, variant_list_iterate_cb cb, void* userdata) {
    bool retval = false;
    char* pk_name = NULL;
    variant_list_iterator_t* iter_a = NULL, * iter_b = NULL, * iter_next = NULL;

    if(!cb) {
        pcb_error = pcb_error_invalid_parameter;
        goto error;
    }

    /* Retrieve name of Primary Key parameter */
    if(variant_sets_union_getPKName(variant_list_getPKName(a), variant_list_getPKName(b), &pk_name)) {
        pcb_error = pcb_error_invalid_parameter;
        goto error;
    }

    /* Skip Primary Key name indicator */
    iter_a = variant_list_first(a);
    if(pk_name) {
        iter_a = variant_list_iterator_next(iter_a);
    }

    for(; iter_a; iter_a = iter_next) {
        iter_next = variant_list_iterator_next(iter_a);

        /* Select destination based upon PK */
        iter_b = NULL;
        if(pk_name) {
            iter_b = variant_list_findByPK(b, pk_name, variant_sets_getPKValue(variant_list_iterator_data(iter_a), pk_name));
        }

        if(!cb(iter_a, iter_b, (pk_name), userdata)) {
            goto error;
        }
    }

    retval = true;

error:
    if(pk_name) {
        free(pk_name);
    }
    return retval;
}

bool variant_map_setPKName(variant_map_t* map, const char* value) {
    variant_t* pk_ind = NULL;

    if(!map || !value || !*value) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    pk_ind = variant_map_findVariant(map, VARIANT_SETS_PK_INDICATOR);
    if(pk_ind) {
        variant_setChar(pk_ind, value);
    } else {
        variant_map_addChar(map, VARIANT_SETS_PK_INDICATOR, value);
    }

    return true;
}

bool variant_map_clearPKName(variant_map_t* map) {
    if(!map) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    variant_map_iterator_destroy(variant_map_find(map, VARIANT_SETS_PK_INDICATOR));

    return true;
}

bool variant_map_hasPKName(const variant_map_t* map) {
    if(!map) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    return (variant_map_getPKName(map) != NULL);
}

const char* variant_map_getPKName(const variant_map_t* map) {
    if(!map) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    return variant_map_da_findChar(map, VARIANT_SETS_PK_INDICATOR);
}

bool variant_map_union(variant_map_t* a, const variant_map_t* b, bool update, bool descend) {
    variant_sets_union_iterate_t data;

    if(!a) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    data.dst = a;
    data.update = update;
    data.descend = descend;
    data.free = 0;

    /* Switch 'a' and 'b', because we want to iterate over 'b' */
    return variant_map_iterate(b, a, variant_map_union_iterateCB, &data);
}

bool variant_map_intersection(variant_map_t* a, const variant_map_t* b, bool descend, bool verifyOnly) {
    variant_sets_intersection_iterate_t data;

    data.relComplement = false;
    data.descend = descend;
    data.verifyOnly = verifyOnly;

    return variant_map_iterate(a, b, variant_map_intersection_iterateCB, &data);
}

bool variant_map_relComplement(variant_map_t* a, const variant_map_t* b, bool descend, bool verifyOnly) {
    variant_sets_intersection_iterate_t data;

    data.relComplement = true;
    data.descend = descend;
    data.verifyOnly = verifyOnly;

    return variant_map_iterate(a, b, variant_map_intersection_iterateCB, &data);
}

bool variant_list_setPKName(variant_list_t* list, const char* value) {
    const char* pk_value = NULL;
    string_t pk_ind;

    if(!list || !value || !*value) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    pk_value = variant_list_getPKName(list);
    if(pk_value) {
        if(!strcmp(pk_value, value)) {
            return true;
        }

        variant_list_clearPKName(list);
    }

    string_initialize(&pk_ind, 30);
    string_appendChar(&pk_ind, VARIANT_SETS_PK_INDICATOR);
    string_appendChar(&pk_ind, ":");
    string_appendChar(&pk_ind, value);

    variant_list_prepend(list, variant_list_iterator_createString(&pk_ind));

    string_cleanup(&pk_ind);

    return true;
}

bool variant_list_clearPKName(variant_list_t* list) {
    if(!list) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(variant_list_hasPKName(list)) {
        variant_list_iterator_destroy(variant_list_first(list));
    }

    return true;
}

bool variant_list_hasPKName(const variant_list_t* list) {
    if(!list) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    return (variant_list_getPKName(list) != NULL);
}

const char* variant_list_getPKName(const variant_list_t* list) {
    const char* pk_ind = NULL;

    if(!list) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    pk_ind = variant_da_char(variant_list_iterator_data(variant_list_first(list)));
    if(!pk_ind || !*pk_ind) {
        return NULL;
    }

    if(strncmp(pk_ind, VARIANT_SETS_PK_INDICATOR, strlen(VARIANT_SETS_PK_INDICATOR)) ||
       (pk_ind[strlen(VARIANT_SETS_PK_INDICATOR)] != ':')) {
        return NULL;
    }

    pk_ind += strlen(VARIANT_SETS_PK_INDICATOR) + 1;    /* +1 for ':' */

    while(*pk_ind == ' ') {
        pk_ind++;
    }

    if(!*pk_ind) {
        return NULL;
    }

    return pk_ind;
}

bool variant_list_union(variant_list_t* a, const variant_list_t* b, bool update, bool descend) {
    variant_sets_union_iterate_t data;

    if(!a) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    data.dst = a;
    data.update = update;
    data.descend = descend;
    data.free = 0;

    /* Switch 'a' and 'b', because we want to iterate over 'b' */
    return variant_list_iterate(b, a, variant_list_union_iterateCB, &data);
}

bool variant_list_intersection(variant_list_t* a, const variant_list_t* b, bool descend, bool verifyOnly) {
    variant_sets_intersection_iterate_t data;

    data.relComplement = false;
    data.descend = descend;
    data.verifyOnly = verifyOnly;

    return variant_list_iterate(a, b, variant_list_intersection_iterateCB, &data);
}

bool variant_list_relComplement(variant_list_t* a, const variant_list_t* b, bool descend, bool verifyOnly) {
    variant_sets_intersection_iterate_t data;

    data.relComplement = true;
    data.descend = descend;
    data.verifyOnly = verifyOnly;

    return variant_list_iterate(a, b, variant_list_intersection_iterateCB, &data);
}

bool variant_sets_setPKName(variant_t* var, const char* value) {
    if(!var || !value || !*value) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    switch(variant_type(var)) {
    case variant_type_map:
        return variant_map_setPKName(variant_da_map(var), value);
        break;
    case variant_type_array:
        return variant_list_setPKName(variant_da_list(var), value);
        break;
    default:
        break;
    }

    return false;
}

bool variant_sets_clearPKName(variant_t* var) {
    if(!var) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    switch(variant_type(var)) {
    case variant_type_map:
        return variant_map_clearPKName(variant_da_map(var));
        break;
    case variant_type_array:
        return variant_list_clearPKName(variant_da_list(var));
        break;
    default:
        break;
    }

    return false;
}

bool variant_sets_hasPKName(const variant_t* var) {
    if(!var) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    switch(variant_type(var)) {
    case variant_type_map:
        return variant_map_hasPKName(variant_da_map(var));
        break;
    case variant_type_array:
        return variant_list_hasPKName(variant_da_list(var));
        break;
    default:
        break;
    }

    return false;
}

const char* variant_sets_getPKName(const variant_t* var) {
    if(!var) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    switch(variant_type(var)) {
    case variant_type_map:
        return variant_map_getPKName(variant_da_map(var));
        break;
    case variant_type_array:
        return variant_list_getPKName(variant_da_list(var));
        break;
    default:
        break;
    }

    return NULL;
}

bool variant_sets_union(variant_t* a, const variant_t* b, bool update, bool descend) {
    if(!a || (variant_type(a) != variant_type(b))) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    switch(variant_type(a)) {
    case variant_type_map:
        return variant_map_union(variant_da_map(a), variant_da_map(b), update, descend);
        break;
    case variant_type_array:
        return variant_list_union(variant_da_list(a), variant_da_list(b), update, descend);
        break;
    default:
        break;
    }

    return false;
}

bool variant_sets_intersection(variant_t* a, const variant_t* b, bool descend, bool verifyOnly) {
    if(!a || (variant_type(a) != variant_type(b))) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    switch(variant_type(a)) {
    case variant_type_map:
        return variant_map_intersection(variant_da_map(a), variant_da_map(b), descend, verifyOnly);
        break;
    case variant_type_array:
        return variant_list_intersection(variant_da_list(a), variant_da_list(b), descend, verifyOnly);
        break;
    default:
        break;
    }

    return false;
}

bool variant_sets_relComplement(variant_t* a, const variant_t* b, bool descend, bool verifyOnly) {
    if(!a || (variant_type(a) != variant_type(b))) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    switch(variant_type(a)) {
    case variant_type_map:
        return variant_map_relComplement(variant_da_map(a), variant_da_map(b), descend, verifyOnly);
        break;
    case variant_type_array:
        return variant_list_relComplement(variant_da_list(a), variant_da_list(b), descend, verifyOnly);
        break;
    default:
        break;
    }

    return false;
}
