/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#define _GNU_SOURCE
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include <pcb/common/error.h>
#include <pcb/utils/string_list.h>


typedef int (* cmp_fn) (const char* s1, const char* s2, size_t n);

/**
   @file
   @brief
   Implementation of the public string list functions
 */

/**
   @ingroup pcb_utils_string_list
   @brief
   Initialize a string linked list.

   @details
   This function initializes a string linked list.\n
   It is mandatory to initialize a string linked list before using it.\n
   Every @ref string_list_initialize call should have a corresponding @ref string_list_cleanup call to free the memory.\n
   This function calls @ref llist_initialize.

   @param strlist the string list to be initialized

   @return
    - true: Initialization was succesfull, the llist_t item is ready to be used
    - false: An error has occurred
        - the strlist is a NULL pointer
        - memory allocation failed
 */
bool string_list_initialize(string_list_t* strlist) {
    return llist_initialize(strlist);
}

/**
   @ingroup pcb_utils_string_list
   @brief
   Cleanup a string linked list.

   @details
   This function <b>frees</b> the string linked list. \n
   After calling this function, the string linked list can not be used anymore, until it is re-initialized.\n
   The elements in the list are deleted.\n
   This function calls @ref string_list_clear and @ref llist_cleanup.\n

   @param strlist the string list that has to be cleaned up
 */
void string_list_cleanup(string_list_t* strlist) {
    string_list_clear(strlist);
}

/**
   @ingroup pcb_utils_string_list
   @brief
   Remove all elements from a string linked list.

   @details
   This function <b>frees</b> all elements from the string linked list. \n
   This function loops over all string elements, using @ref string_list_takeFirst to remove an \n
   element from the list and @ref string_list_iterator_destroy to free the memory for that item.\n

   @param strlist the string list that has to be cleared
 */
void string_list_clear(string_list_t* strlist) {
    string_list_iterator_t* strpart = NULL;

    strpart = string_list_takeFirst(strlist);
    while(strpart) {
        string_list_iterator_destroy(strpart);
        strpart = string_list_takeFirst(strlist);
    }
}

/**
   @ingroup pcb_utils_string_list
   @brief
   Create and initialize a <b>string linked list iterator</b> (=string list item)

   @details
   This function <b>created and initializes</b> a <b>string linked list iterator</b>. \n
   This function call creates the string list iterator, and initializing the string and list.
   elements using @ref string_initialize and @ref llist_iterator_initialize\n

   @param string the string that has to be used to initialize the iterator (optional)

   @return
    - true: Initialization was succesfull, the string_list_iterator_t item is ready to be used
    - false: An error has occurred
        - memory is full
 */
string_list_iterator_t* string_list_iterator_create(const string_t* string) {
    string_list_iterator_t* it = calloc(1, sizeof(string_list_iterator_t));
    if(!it) {
        pcb_error = pcb_error_no_memory;
        return NULL;
    }

    if(string) {
        string_copy(&it->string, string);
    }

    return it;
}

/**
   @ingroup pcb_utils_string_list
   @brief
   Create and initialize a <b>string linked list iterator</b> (=string list item)

   @details
   This function <b>created and initializes</b> a <b>string linked list iterator</b>. \n
   This function call creates the string list iterator, and initializing the string and list
   elements using @ref string_initialize and @ref llist_iterator_initialize\n

   @param string the string that has to be used to initialize the iterator (optional)

   @return
    - true: Initialization was succesfull, the string_list_iterator_t item is ready to be used
    - false: An error has occurred
        - memory is full
 */
string_list_iterator_t* string_list_iterator_createFromChar(const char* string) {
    string_list_iterator_t* it = calloc(1, sizeof(string_list_iterator_t));
    if(!it) {
        pcb_error = pcb_error_no_memory;
        return NULL;
    }

    if(string) {
        string_fromChar(&it->string, string);
    }

    return it;
}

/**
   @ingroup pcb_utils_string_list
   @brief
   Destroy a string linked list iterator.

   @details
   This function <b>frees</b> the string linked list iterator. \n
   After calling this function, the string linked list iterator can not be used anymore, until it is re-created.\n
   This function calls @ref string_cleanup and frees its memory. \n

   @param it the string list iterator that has to be cleaned up
 */
void string_list_iterator_destroy(string_list_iterator_t* it) {
    if(!it) {
        pcb_error = pcb_error_invalid_parameter;
        return;
    }
    string_cleanup(&it->string);
    string_list_iterator_take(it);
    free(it);
}

static void string_list_splitAddItem(string_list_t* list, const string_t* string, uint32_t start, uint32_t length, string_splitBehavior_t behavior) {
    if((behavior == strlist_skip_empty_parts) && (length == 0)) {
        return;
    }

    string_list_iterator_t* strItem = string_list_iterator_create(NULL);
    if(strItem) {
        string_mid(string_list_iterator_data(strItem), string, start, length);
        string_list_append(list, strItem);
    }
}

/**
   @ingroup pcb_utils_string_list
   @brief
   Split a string into different substrings

   @details
   This function splits a <b>string</b> into a <b>list</b> of strings, using a <b>sep</b>erator, with the specified
   split <b>behavior</b> and string <b>C</b>ase <b>S</b>ensitivity\n

   @param list the resulting string list
   @param string the string that has to be split into different items
   @param sep the seperator where the string has to be split
   @param behavior the split behavior @ref string_splitBehavior_t
   @param cs case sensitive splitting or not @ref string_case_t

   @return
   The number of items in the list.
 */
unsigned int string_list_split(string_list_t* list, const string_t* string, const char* sep, string_splitBehavior_t behavior, string_case_t cs) {
    if(!list || !string || !string->buffer || !sep) {
        pcb_error = pcb_error_invalid_parameter;
        return 0;
    }

    size_t size = strlen(string->buffer);
    if(size == 0) {
        return 0;
    }

    size_t sepSize = strlen(sep);
    size_t increase = 1;
    size_t startPos = 0;
    cmp_fn mystrncmp = (cs == string_case_sensitive) ? strncmp : strncasecmp;

    size_t i = 0;
    for(i = 0; i < size - sepSize; i += increase) {
        increase = 1;
        if(mystrncmp(string->buffer + i, sep, sepSize) != 0) {
            continue;
        }

        string_list_splitAddItem(list, string, startPos, i - startPos, behavior);
        increase = sepSize;
        startPos = i + increase;
    }

    if(mystrncmp(string->buffer + i, sep, sepSize) != 0) {
        i += sepSize;
    }

    string_list_splitAddItem(list, string, startPos, i - startPos, behavior);

    return string_list_size(list);
}

/**
   @ingroup pcb_utils_string_list
   @brief
   Split a string into different substrings

   @details
   This function is provided for your convience is behaving the same as @ref string_list_split

   @param list the resulting string list
   @param string the string that has to be split into different items
   @param sep the seperator where the string has to be split
   @param behavior the split behavior @ref string_splitBehavior_t
   @param cs case sensitive splitting or not @ref string_case_t

   @return
   The number of items in the list.
 */
unsigned int string_list_splitChar(string_list_t* list, const char* string, const char* sep, string_splitBehavior_t behavior, string_case_t cs) {
    if(!list || !string || !sep) {
        pcb_error = pcb_error_invalid_parameter;
        return 0;
    }

    string_t s;
    string_initialize(&s, strlen(string) + 1);
    string_fromChar(&s, string);
    unsigned int retval = string_list_split(list, &s, sep, behavior, cs);
    string_cleanup(&s);
    return retval;
}

/**
   @ingroup pcb_utils_string_list
   @brief
   Concatenate a string list into a new string

   @details
   This function concatenates a <b>string list</b> into a single <b>string</b>, while adding
   <b>sep</b>erators (optional)

   @param dest the resulting string
   @param strlist the string that has to be concatenated
   @param sep the seperator that should be placed between the different list items

   @return
   The number of items in the list.
 */
unsigned int string_join(string_t* dest, const string_list_t* strlist, const char* sep) {
    if(!dest) {
        pcb_error = pcb_error_invalid_parameter;
        return 0;
    }

    string_clear(dest);

    if(!strlist) {
        pcb_error = pcb_error_invalid_parameter;
        return 0;
    }

    string_list_iterator_t* part = NULL;

    string_list_for_each(part, strlist) {
        string_append(dest, string_list_iterator_data(part));
        if(string_list_iterator_next(part)) {
            string_appendChar(dest, sep);
        }
    }

    return string_isEmpty(dest) ? 0 : strlen(dest->buffer);
}

/**
   @ingroup pcb_utils_string_list
   @brief
   Get a pointer to the string in this string_list_iterator_t

   @details
   This function returns a pointer to the string_t item of this iterator

   @param it the string list iterator

   @return
    - a pointer to the string_t item
    - NULL if it is NULL
 */
string_t* string_list_iterator_data(const string_list_iterator_t* it) {
    if(!it) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    return (string_t*) (&it->string);
}

/**
   @ingroup pcb_utils_string_list
   @brief
   Search the string list for a specific string

   @details
   If the list contains the string you are searching for, this function will return true

   @param strList the string list to search
   @param string the string to search for.

   @return
    - true when the string is found
    - false when the string was not found
 */
bool string_list_containsChar(const string_list_t* strList, const char* string) {
    if(!strList) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    string_list_iterator_t* it = NULL;
    string_list_for_each(it, strList) {
        if(string_compareChar(string_list_iterator_data(it), string, string_case_sensitive) == 0) {
            return true;
        }
    }

    return false;
}

/**
   @ingroup pcb_utils_string_list
   @brief
   Search the string list for a specific string

   @details
   This function is provided for your convience and is behaving the same as @ref string_list_containsChar

   @param strList the string list to search
   @param string the string to search for.

   @return
    - true when the string is found
    - false when the string was not found
 */
bool string_list_contains(string_list_t* strList, const string_t* string) {
    if(!strList) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    return string_list_containsChar(strList, string_buffer(string));
}

string_list_iterator_t* string_list_findChar(const string_list_t* strList, const char* string) {
    if(!strList) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    string_list_iterator_t* it = NULL;
    string_list_for_each(it, strList) {
        if(string_compareChar(string_list_iterator_data(it), string, string_case_sensitive) == 0) {
            break;
        }
    }

    return it;
}

/**
   @ingroup pcb_utils_string_list
   @brief
   Get the first string list element.

   @details
   This function returns the first string list element of the string linked list. \n
   This function calls @ref llist_first to get the specified element, @ref llist_item_data
   is used to get a pointer to the string list iterator\n

   @param list the string linked list

   @return
    - a <b>pointer</b> to the first string list element of the string linked list.
    - NULL if the string list is empty.
    - NULL if the string list parameter is NULL
 */
string_list_iterator_t* string_list_first(const string_list_t* list) {
    if(!list) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    llist_iterator_t* ll_item = llist_first(list);
    if(!ll_item) {
        return NULL;
    }
    return llist_item_data(ll_item, string_list_iterator_t, llist_it);
}

/**
   @ingroup pcb_utils_string_list
   @brief
   Get the last string list element.

   @details
   This function returns the last string list element of the string linked list. \n
   This function calls @ref llist_last to get the specified element, @ref llist_item_data
   is used to get a pointer to the string list iterator\n

   @param list the string linked list

   @return
    - a <b>pointer</b> to the last string list element of the string linked list.
    - NULL if the string list is empty.
    - NULL if the string list parameter is NULL.
 */
string_list_iterator_t* string_list_last(const string_list_t* list) {
    if(!list) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    llist_iterator_t* ll_item = llist_last(list);
    if(!ll_item) {
        return NULL;
    }
    return llist_item_data(ll_item, string_list_iterator_t, llist_it);
}

/**
   @ingroup pcb_utils_string_list
   @brief
   Get a pointer to the specified string list element.

   @details
   This function returns a pointer to the string list element at the specified index of the string linked list. \n
   Due to the nature of a linked list the items are not garanteed to be at the same place all the time.\n
   Use this function with care, it can have performance impact, the string list is iterated to find the correct element.\n
   This function calls @ref llist_at to get the specified element, @ref llist_item_data
   is used to get a pointer to the string list iterator\n

   @param list the linked list
   @param index the item # in which you are interested (start counting from 0)

   @return
    - a <b>pointer</b> to the specified string list element in the string linked list.
    - NULL if the element does not exist.
    - NULL if the list parameter is NULL.
 */
string_list_iterator_t* string_list_at(const string_list_t* list, unsigned int index) {
    if(!list) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    llist_iterator_t* ll_item = llist_at(list, index);
    if(!ll_item) {
        return NULL;
    }

    return llist_item_data(ll_item, string_list_iterator_t, llist_it);
}

/**
   @ingroup pcb_utils_string_list
   @brief
   Get a pointer to the next string list element.

   @details
   This function returns a pointer to the next string list element. \n
   This function calls @ref llist_iterator_next to get the next element, @ref llist_item_data
   is used to get a pointer to the string list iterator\n

   @param it the list element

   @return
    - a <b>pointer</b> to the next string list element in the linked list.
    - NULL if the it parameter is NULL.
    - NULL if the end of the string list was reached
 */
string_list_iterator_t* string_list_iterator_next(const string_list_iterator_t* it) {
    if(!it) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    llist_iterator_t* ll_item = llist_iterator_next(&it->llist_it);
    if(!ll_item) {
        return NULL;
    }
    return llist_item_data(ll_item, string_list_iterator_t, llist_it);
}

/**
   @ingroup pcb_utils_string_list
   @brief
   Get a pointer to the previous string list element.

   @details
   This function returns a pointer to the previous string list element. \n
   This function calls @ref llist_iterator_prev to get the previous element, @ref llist_item_data
   is used to get a pointer to the string list iterator\n

   @param it the list element

   @return
    - a <b>pointer</b> to the previous string list element in the linked list.
    - NULL if the it parameter is NULL.
    - NULL if the beginning of the string list was reached
 */
string_list_iterator_t* string_list_iterator_prev(const string_list_iterator_t* it) {
    if(!it) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    llist_iterator_t* ll_item = llist_iterator_prev(&it->llist_it);
    if(!ll_item) {
        return NULL;
    }
    return llist_item_data(ll_item, string_list_iterator_t, llist_it);
}

/**
   @ingroup pcb_utils_string_list
   @brief
   Append the element to a string list.

   @details
   This function appends the specified string list element to an existing string list. If the element was part of
   another string list then the element will be automatically removed from that string list.\n
   This function calls @ref llist_append\n

   @param list the string list to which the element has to be added
   @param insert the string list element that has to be attached to the string list

   @return
    - true: The list element was added succesfully appenced to the list
    - false: An error has occurred
        - list or insert is a NULL pointer
 */
bool string_list_append(string_list_t* list, string_list_iterator_t* insert) {
    if(!list || !insert) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    return llist_append(list, &insert->llist_it);
}

/**
   @ingroup pcb_utils_string_list
   @brief
   Prepend the element to a string list.

   @details
   This function prepends the specified string list element to an existing string list. If the element was part of
   another string list then the element will be automatically removed from that string list.\n
   This function calls @ref llist_prepend\n

   @param list the string list to which the element has to be added
   @param insert the string list element that has to be attached to the string list

   @return
    - true: The list element was added succesfully prepended to the list
    - false: An error has occurred
        - list or insert is a NULL pointer
 */
bool string_list_prepend(string_list_t* list, string_list_iterator_t* insert) {
    if(!list || !insert) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }
    return llist_prepend(list, &insert->llist_it);
}

/**
   @ingroup pcb_utils_string_list
   @brief
   Attach the element to a string list at a certain index.

   @details
   This function attaches the specified string list element to an existing string list. If the element is part of
   another string list then the element will be automatically removed from that string list.\n
   This function calls @ref llist_insertAt\n

   @param list the string list to which the element has to be added
   @param index the item # where to add the element to the list (start counting from 0)
   @param insert the string list element that has to be attached to the string list

   @return
    - true: The string list element was succesfully inserted into the string list
    - false: An error has occurred
        - list or insert is a NULL pointer
        - index element does not exist
        - the insert element is the head or tail element of a list
 */
bool string_list_insertAt(string_list_t* list, unsigned int index, string_list_iterator_t* insert) {
    if(!list || !insert) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }
    return llist_insertAt(list, index, &insert->llist_it);
}

/**
   @ingroup pcb_utils_string_list
   @brief
   Detach the element from a string list.

   @details
   This function detaches the specified string list element from an existing string list.\n
   This function calls @ref llist_iterator_take to remove the item from the linked list,
   @ref llist_item_data is used to get a pointer to the string list iterator.\n

   @param it the string list element that has to be detached from the string list

   @return
    - a <b>pointer</b> to the detached string list element.
    - NULL if an error occured
        - it is NULL
 */
string_list_iterator_t* string_list_iterator_take(string_list_iterator_t* it) {
    if(!it) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    llist_iterator_take(&it->llist_it);
    return it;
}

/**
   @ingroup pcb_utils_string_list
   @brief
   Detach the first element from a string list.

   @details
   This function detaches the first string list element from an existing string list.\n
   This function calls @ref llist_takeFirst to get the first list element and @ref
   llist_item_data to get a pointer to the string list iterator.\n

   @param list the list from which the first element has to be detached

   @return
    - a <b>pointer</b> to the detached string list element.
    - NULL if an error occured
        - list is NULL
        - the list is empty
 */
string_list_iterator_t* string_list_takeFirst(string_list_t* list) {
    if(!list) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    llist_iterator_t* ll_item = llist_takeFirst(list);
    if(!ll_item) {
        return NULL;
    }
    return llist_item_data(ll_item, string_list_iterator_t, llist_it);
}

/**
   @ingroup pcb_utils_string_list
   @brief
   Detach the last element from a string list.

   @details
   This function detaches the last string list element from an existing string list.\n
   This function calls @ref llist_takeLast to get the last list element and @ref
   llist_item_data to get a pointer to the string list iterator\n

   @param list the list from which the last element has to be detached

   @return
    - a <b>pointer</b> to the detached string list element.
    - NULL if an error occured
        - list is NULL
        - the list is empty
 */
string_list_iterator_t* string_list_takeLast(string_list_t* list) {
    if(!list) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    llist_iterator_t* ll_item = llist_takeLast(list);
    if(!ll_item) {
        return NULL;
    }
    return llist_item_data(ll_item, string_list_iterator_t, llist_it);
}

/**
   @ingroup pcb_utils_string_list
   @brief
   Get the size of the string list.

   @details
   This function returns the size of the string list.\n
   This function calls @ref llist_size\n

   @param list the list in wich you are interested

   @return
    - the number of elements in the list.
    - 0 if list is a NULL pointer.
 */
unsigned int string_list_size(string_list_t* list) {
    if(!list) {
        pcb_error = pcb_error_invalid_parameter;
        return 0;
    }

    return llist_size(list);
}

/**
   @ingroup pcb_utils_string_list
   @brief
   Determine if the string list is empty.

   @details
   This function checks wether the  string list is empty or not.\n
   This function calls @ref llist_isEmpty\n

   @param list the list in wich you are interested

   @return
    - true:
        - The list is empty
        - list == NULL
    - false: The list is not empty

 */
bool string_list_isEmpty(const string_list_t* list) {
    if(!list) {
        pcb_error = pcb_error_invalid_parameter;
        return true;
    }

    return llist_isEmpty(list);
}

typedef struct _string_list_qsort_data {
    string_list_qsort_compar compar;
    void* userdata;
} string_list_qsort_data_t;

static int string_list_qsort_compare(const llist_iterator_t* a, const llist_iterator_t* b, void* userdata) {
    string_list_qsort_data_t* data = (string_list_qsort_data_t*) userdata;

    return data->compar(string_list_iterator_data(llist_item_data(a, string_list_iterator_t, llist_it)),
                        string_list_iterator_data(llist_item_data(b, string_list_iterator_t, llist_it)), data->userdata);
}

int string_list_qsort_strcmp(const string_t* a, const string_t* b, void* userdata) {
    const char* ba = NULL, * bb = NULL;

    (void) userdata;

    ba = string_buffer(a);
    bb = string_buffer(b);

    if(!ba && !bb) {
        return 0;
    } else if(!ba) {
        return -1;
    } else if(!bb) {
        return 1;
    }

    return strcmp(ba, bb);
}

/**
   @ingroup pcb_utils_string_list
   @brief
   Sort a string list using the provided comparison function.\n
   The contents of the list are sorted in ascending order according to a comparison function pointed to
   by <b>compar</b>, which is called with two arguments that point to the strings being compared.
   The comparison function must return an integer less than, equal to, or greater than zero if the first
   argument is considered to be respectively less than, equal to, or greater than the second.
   If two members compare as equal, their order in the sorted list is undefined.
   If <b>reverse</b> is set, the sorting order is reversed.

   The function <b>string_list_qsort_strcmp</b> is provided to perform sorting based upon strcmp().

   @warning
   This function uses a trampoline function to call the provided comparison function.
   When the qsort_r() function is not available by the toolchain, the sorting function is not reentrant
   nor safe to use in threads.

   @param list the list to sort
   @param compar the comparison function
   @param reverse reverse the sorting order
   @param userdata userdata passed on to comparison function

   @return
    - true when sorting is done.
    - false if an error occured, pcb_error will be set
        - pcb_error_invalid_parameter when list or compar is NULL
        - pcb_error_no_memory when out of memory
 */
bool string_list_qsort(string_list_t* list, string_list_qsort_compar compar, bool reverse, void* userdata) {
    string_list_qsort_data_t data;

    if(!list || !compar) {
        errno = pcb_error_invalid_parameter;
        return false;
    }

    data.compar = compar;
    data.userdata = userdata;

    return llist_qsort((llist_t*) list, string_list_qsort_compare, reverse, &data);
}
