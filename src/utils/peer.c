/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

/* -*- c-basic-offset: 4 -*- */
#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <fnmatch.h>
#include <fcntl.h>

#ifdef OPEN_SSL_SUPPORT
#include <ctype.h>
#include <openssl/err.h>
#endif

#include <debug/sahtrace.h>

#include <pcb/common/error.h>

#include <pcb/utils/linked_list.h>
#include <pcb/utils/peer.h>
#include <pcb/utils/connection.h>

/**
   @file
   @brief
   Implementation of the public peer functions
 */

#ifdef OPEN_SSL_SUPPORT
static bool peerSSLIndexInit = false;
static int peerSSLIndex = 0;
#endif

static ssize_t defaultBufferSize = 0;

static bool peer_createBuffer(circbuf_t** buffer) {
    if(*buffer) {
        return true;
    }

    *buffer = (circbuf_t*) calloc(1, sizeof(circbuf_t));
    if(!(*buffer)) {
        SAH_TRACEZ_ERROR("pcb", "Failed to allocate memory for circular buffer");
        pcb_error = pcb_error_no_memory;
        return false;
    }
    if(!defaultBufferSize) {
        defaultBufferSize = 1024;
        const char* data = getenv("PCB_BUFFER_SIZE");
        if(data) {
            defaultBufferSize = atol(data);
            if(defaultBufferSize == 0) {
                defaultBufferSize = 1024;
            }
            SAH_TRACEZ_INFO("pcb", "env PCB_BUFFER_SIZE is set, buffersize = %zd", defaultBufferSize);
        }
    }
    if(!circbuf_initialize(*buffer, defaultBufferSize)) {
        SAH_TRACEZ_ERROR("pcb", "Failed to initialize circular buffer");
        free(*buffer);
        *buffer = NULL;
        return false;
    }
    if(!circbuf_fopen(*buffer, "w+")) {
        SAH_TRACEZ_ERROR("pcb", "Failed to open stream for circular buffer");
        circbuf_cleanup(*buffer);
        free(*buffer);
        *buffer = NULL;
        return false;
    }

    return true;
}

#ifdef OPEN_SSL_SUPPORT
static ssize_t peer_read_ssl(peer_info_t* peer, char* buffer, size_t size) {
    if(peer->data_size) {
        uint32_t length = (size > (peer->data_size - peer->data_offset)) ? (peer->data_size - peer->data_offset) : size;
        memcpy(buffer, peer->buffer + peer->data_offset, length);
        peer->data_offset += length;
        if(peer->data_offset >= peer->data_size) {
            peer->data_size = 0;
            peer->data_offset = 0;
        }
        return length;
    } else {
        ssize_t result = SSL_read(peer->ssl, buffer, size);
        if(result <= 0) {
            switch(SSL_get_error(peer->ssl, result)) {
            case SSL_ERROR_NONE:
                /* all ok, do nothing */
                break;
            case SSL_ERROR_WANT_READ:
                /* just return egain, it is almost certain that there is no need to set the following state */
                errno = EAGAIN;
                return -1;
                break;
            case SSL_ERROR_WANT_WRITE:
                peer->state = peer_state_retry_read_need_write;
                errno = EAGAIN;
                return -1;
                break;
            case SSL_ERROR_ZERO_RETURN:
            default:
                /* ssl error occured.*/
                SAH_TRACEZ_ERROR("pcb_con", "SSL_read error %s", ERR_error_string(ERR_get_error(), NULL));
                return result;
                break;
            }
        }
        return result;
    }
}
#endif

static ssize_t peer_read_ipc(peer_info_t* peer, char* buffer, size_t size) {
    struct msghdr msg;
    struct iovec iov;
    struct cmsghdr* cmsg;

    char control[ CMSG_SPACE(sizeof( int ) * 10) ];

    memset(&msg, 0, sizeof(msg));
    iov.iov_base = buffer;
    iov.iov_len = size;
    msg.msg_iov = &iov;
    msg.msg_iovlen = 1;
    msg.msg_control = control;
    msg.msg_controllen = sizeof(control);

    SAH_TRACEZ_INFO("pcb_con", "Start reading messages from socket");
    ssize_t result = 0;
    result = recvmsg(peer->socketfd, &msg, 0);
    if(result <= 0) {
        SAH_TRACEZ_INFO("pcb_con", "recvmsg error %d", errno);
        return result;
    }

    /* Loop over all control messages */
    cmsg = CMSG_FIRSTHDR(&msg);
    while(cmsg != NULL) {
        if((cmsg->cmsg_level == SOL_SOCKET) && (cmsg->cmsg_type == SCM_RIGHTS)) {
            unsigned int i = 0;
            for(i = 0; i < ((cmsg->cmsg_len - CMSG_LEN(0)) / sizeof(int)); i++) {
                peer_transport_fd_t* tfd = (peer_transport_fd_t*) calloc(1, sizeof(peer_transport_fd_t));
                if(!tfd) {
                    SAH_TRACEZ_INFO("pcb_con", "Failed to allocate memory to store fd");
                    continue;
                }
                tfd->fd = ((int*) CMSG_DATA(cmsg))[i];
                SAH_TRACEZ_INFO("pcb_fd", "Received file descriptor = %d", tfd->fd);
                llist_append(&peer->recv_fd_list, &tfd->it);
            }
        }
        cmsg = CMSG_NXTHDR(&msg, cmsg);
    }

    SAH_TRACEZ_INFO("pcb_con", "Read ipc done result = %zd", result);
    return result;
}

static ssize_t peer_read(void* cookie, char* buffer, size_t size) {
    peer_info_t* peer = (peer_info_t*) cookie;
    if(peer->socketfd == -1) {
        errno = EPIPE;
        return -1;
    }
#ifdef OPEN_SSL_SUPPORT
    if(peer->ssl_bio) {
        return peer_read_ssl(peer, buffer, size);
    } else {
#endif
    if(peer_isIPCSocket(peer)) {
        return peer_read_ipc(peer, buffer, size);
    } else {
        return read(peer->socketfd, buffer, size);
    }
#ifdef OPEN_SSL_SUPPORT
}
#endif
}

#ifdef OPEN_SSL_SUPPORT
static ssize_t peer_send_ssl(peer_info_t* peer, const void* buf, size_t count) {
    if(peer->writesize != 0) {
        SAH_TRACEZ_INFO("pcb_con", "need to rewrite only %zu bytes (wanted %zu)", peer->writesize, count);
        count = peer->writesize;
        peer->writesize = 0;
    }
    ssize_t sendresult = SSL_write(peer->ssl, buf, count);
    if(sendresult <= 0) {
        switch(SSL_get_error(peer->ssl, sendresult)) {
        case SSL_ERROR_NONE:
            /* all ok, do nothing */
            errno = 0;
            break;
        case SSL_ERROR_WANT_READ:
            peer->state = peer_state_retry_write_need_read;
            peer->writesize = count;
            SAH_TRACEZ_INFO("pcb_con", "blocking, retry same write of %zu bytes", count);
            errno = EAGAIN;
            sendresult = -1;
            break;
        case SSL_ERROR_WANT_WRITE:
            peer->state = peer_state_retry_write_need_write;
            peer->writesize = count;
            SAH_TRACEZ_INFO("pcb_con", "blocking, retry same write of %zu bytes", count);
            errno = EAGAIN;
            sendresult = -1;
            break;
        case SSL_ERROR_ZERO_RETURN:
        default:
            /* ssl error occured. */
            SAH_TRACEZ_ERROR("pcb_con", "SSL_write error %s", ERR_error_string(ERR_peek_error(), NULL));
            peer->writesize = count;
            errno = ERR_get_error();
            sendresult = -1;
            break;
        }
        if(errno == EAGAIN) {
            SSL_set_mode(peer->ssl, SSL_MODE_ACCEPT_MOVING_WRITE_BUFFER);
        }
    }

    return sendresult;
}
#endif

static ssize_t peer_send_ipc(peer_info_t* peer, void* buf, size_t count) {
    char control[ CMSG_SPACE(sizeof( int ) * 5) ];
    struct cmsghdr* cmsg;
    struct msghdr msg;
    struct iovec iov;

    llist_t fd_list;
    llist_initialize(&fd_list);

    /* compose the message */
    memset(&msg, 0, sizeof(msg));
    msg.msg_iov = &iov;
    msg.msg_iovlen = 1;

    if(!llist_isEmpty(&peer->send_fd_list)) {
        msg.msg_control = control;
        msg.msg_controllen = sizeof(control);

        /* attach open fd */
        cmsg = CMSG_FIRSTHDR(&msg);
        cmsg->cmsg_level = SOL_SOCKET;
        cmsg->cmsg_type = SCM_RIGHTS;

        int i = 0;
        llist_iterator_t* it = NULL;
        peer_transport_fd_t* tfd = NULL;
        while(i < 5 && !llist_isEmpty(&peer->send_fd_list)) {
            it = llist_takeFirst(&peer->send_fd_list);
            tfd = llist_item_data(it, peer_transport_fd_t, it);
            ((int*) CMSG_DATA(cmsg))[i] = tfd->fd;
            SAH_TRACEZ_INFO("pcb_fd", "Sending file descriptor %d", tfd->fd);
            llist_append(&fd_list, &tfd->it);
            i++;
        }
        cmsg->cmsg_len = CMSG_LEN(i * sizeof(int));
        msg.msg_controllen = cmsg->cmsg_len;
    }

    if(count) {
        /* data */
        iov.iov_base = buf;
        iov.iov_len = count;
    } else {
        iov.iov_base = "pcb-control";
        iov.iov_len = 12;
    }

    int result = sendmsg(peer->socketfd, &msg, 0);

    if(!llist_isEmpty(&fd_list)) {
        if(result <= 0) {
            llist_iterator_t* it = llist_takeLast(&fd_list);
            while(it) {
                llist_prepend(&peer->send_fd_list, it);
                it = llist_takeLast(&fd_list);
            }
        } else {
            llist_iterator_t* it = llist_takeLast(&fd_list);
            peer_transport_fd_t* tfd = NULL;
            while(it) {
                tfd = llist_item_data(it, peer_transport_fd_t, it);
                if(peer->handler_flags & peer_close_fd_after_send) {
                    close(tfd->fd);
                }
                free(tfd);
                it = llist_takeLast(&fd_list);
            }
        }
    }
    return result;
}

static inline ssize_t peer_writeBuffered(peer_info_t* peer, const char* buffer, size_t size) {
    if(!peer->sendBuffer) {
#ifdef __UCLIBC__
        return -1;
#else
        return 0;
#endif
    }
    ssize_t result = circbuf_writeBuffer(peer->sendBuffer, buffer, size);
#ifdef __UCLIBC__
    return result;
#else
    return result < 0 ? 0 : result;
#endif
}

static ssize_t peer_write(void* cookie, const char* buffer, size_t size) {
    peer_info_t* peer = (peer_info_t*) cookie;
    if(peer->socketfd == -1) {
        errno = EPIPE;
#ifdef __UCLIBC__
        return -1;
#else
        return 0;
#endif
    }

    /* buffer io */
    if((peer->stream_type == peer_stream_buffered_io) || (peer->sendBuffer && circbuf_availableForRead(peer->sendBuffer))) {
        SAH_TRACEZ_INFO("pcb_con", "writing in buffer");
        return peer_writeBuffered(peer, buffer, size);
    }

    ssize_t sendresult = 0;
#ifdef OPEN_SSL_SUPPORT
    if(peer->ssl) {
        sendresult = peer_send_ssl(peer, buffer, size);
    } else {
#endif
    if(peer_isIPCSocket(peer)) {
        sendresult = peer_send_ipc(peer, (void*) buffer, size);
    } else {
        sendresult = write(peer->socketfd, buffer, size);
    }
#ifdef OPEN_SSL_SUPPORT
}
#endif
    if((sendresult == -1) && (errno == EAGAIN) && (peer->stream_type == peer_stream_auto_io)) {
        return peer_writeBuffered(peer, buffer, size);
    }
#ifdef __UCLIBC__
    return sendresult;
#else
    return sendresult < 0 ? 0 : sendresult;
#endif
}

static int peer_seek_dummy(void* cookie, off64_t* position, int whence) {
    (void) cookie;
    (void) position;
    (void) whence;
    return -1;
}

static int peer_close_instream(void* cookie) {
    peer_info_t* peer = (peer_info_t*) cookie;
    peer->in = NULL;
    return 0;
}

static int peer_close_outstream(void* cookie) {
    peer_info_t* peer = (peer_info_t*) cookie;
    peer->out = NULL;
    return 0;
}

#ifdef OPEN_SSL_SUPPORT

bool peer_setUsername(peer_info_t* peer, const char* username) {
    peer->ssl_auth_checked = true;
    peer->ssl_auth = false;

    if(!peer || !peer->ssl) {
        SAH_TRACEZ_ERROR("ssl", "invalid peer argument");
        return false;
    }

    if(!username) {
        SAH_TRACEZ_ERROR("ssl", "invalid username argument");
        return false;
    }

    SSL_SESSION* session = SSL_get0_session(peer->ssl);

    if(!session) {
        SAH_TRACEZ_ERROR("ssl", "peer does not have an ssl context");
        return false;
    }

    snprintf(peer->ssl_cert_cn, sizeof(peer->ssl_cert_cn), "%s", username);
    peer->ssl_auth = true;

    return true;
}

const char* peer_getUsername(peer_info_t* peer) {
    if(!peer) {
        return NULL;
    }
    return peer->ssl_cert_cn;
}

/**
   @brief
   Attach the peer structure to the SSL structure as userdata.

   @details
   The SSL structure can take userdata from the application and attach it.
   Afterwards, this data can be retrieved by the application when only
   the SSL structure is given.

   @param ssl The SSL structure
   @param peer The container of the peer information

   @return true when successful, false otherwise.
 */
bool peer_ssl_setPeer(SSL* ssl, peer_info_t* peer) {
    if(!ssl || !peer) {
        SAH_TRACEZ_ERROR("ssl", "Invalid argument");
        return false;
    }

    if(!peerSSLIndexInit) {
        peerSSLIndex = SSL_get_ex_new_index(0, "peer index", NULL, NULL, NULL);
        peerSSLIndexInit = true;
    }

    return (SSL_set_ex_data(ssl, peerSSLIndex, (void*) peer) == 1);
}

/**
   @brief
   Retrieve the peer structure from the SSL structure.

   @details
   This function returns the peer structure that was previously attached
   to the SSL structure.

   @param ssl The SSL structure

   @return A pointer to the peer structure if it was set, NULL otherwise.
 */
peer_info_t* peer_ssl_getPeer(SSL* ssl) {
    if(!ssl) {
        SAH_TRACEZ_ERROR("ssl", "Invalid argument");
        return NULL;
    }

    if(!peerSSLIndexInit) {
        SAH_TRACEZ_WARNING("ssl", "SSL index initialization not performed, yet");
        return NULL;
    }

    return (peer_info_t*) SSL_get_ex_data(ssl, peerSSLIndex);
}

/**
   @brief
   Connects to an SSL enabled server.

   @details
   Connects to an SSL/TLS enabled server.
   This function takes into account that the underlying socket is not blocking and can be called
   again to continue the SSL handshake if needed. If the SSL handshake and negotation is done
   the certificate verify function is called (see @ref connection_post_check).

   @param con A pointer to a connection object
   @param peer The container of the peer information
   @param node the host information part of the connection

   @return
    - true when SSL handshake/negotation is:
        - done successfully and the certificate is accepted
        - is buzy.
    - false in any other case.
 */
bool peer_ssl_connect(connection_info_t* con, peer_info_t* peer, const char* node) {
    if(!peer->node) {
        peer->node = strdup(node);
    }
    int code = SSL_connect(peer->ssl);
    if(code <= 0) {
        switch(SSL_get_error(peer->ssl, code)) {
        case SSL_ERROR_NONE:     /* everything is OK */
            SAH_TRACEZ_INFO("ssl", "ssl connect OK");
            /* do nothing */
            break;
        case SSL_ERROR_ZERO_RETURN:     /* SSL connection being closed */
            SAH_TRACEZ_INFO("ssl", "ssl connect being closed");
            return false;
            break;
        case SSL_ERROR_WANT_READ:     /* SSL need to read on the socket during connect */
            SAH_TRACEZ_INFO("ssl", "ssl connect want read");
            peer->state = peer_state_retry_connect_need_read;
            /* just return OK, this function will be called again. */
            return true;
            break;
        case SSL_ERROR_WANT_WRITE:     /* SSL need to read on the socket during connect */
            SAH_TRACEZ_INFO("ssl", "ssl connect want write");
            peer->state = peer_state_retry_connect_need_write;
            /* just return OK, this function will be called again. */
            return true;
            break;
        default:
            SAH_TRACEZ_ERROR("ssl", "Error connecting SSL object");
            return false;
            break;
        }
    }
    peer->state = peer_state_connected;

    long err = X509_V_OK;
    if(con->verify_server) {
        #if OPENSSL_VERSION_NUMBER >= 0x30000000L
        X509* cert = SSL_get1_peer_certificate(peer->ssl);
        #else
        X509* cert = SSL_get_peer_certificate(peer->ssl);
        #endif
        if(!cert) {
            SAH_TRACEZ_ERROR("ssl", "No server certificate available and verify server callback set");
            return false;
        }
        err = con->verify_server(cert, node, con->attrib);
        X509_free(cert);
        if(err != X509_V_OK) {
            SAH_TRACEZ_ERROR("ssl", "Error peer certificate %s", X509_verify_cert_error_string(err));
            return false;
        }
    }

    if(con->attrib & ssl_attribute_peer_not_verified) {
        /*
         * SSL always tries to verify the peer
         * so let's continue if we do not care of the result
         */
        err = X509_V_OK;
    } else {
        err = SSL_get_verify_result(peer->ssl);
    }
    switch(err) {
    case X509_V_OK:
        /* all ok, nothing to do */
        break;
    case X509_V_ERR_DEPTH_ZERO_SELF_SIGNED_CERT:     /* self signed certificate */
        if(!(con->attrib & ssl_attribute_accept_self_signed)) {
            SAH_TRACE_ERROR("Verify certificate error %s", X509_verify_cert_error_string(err));
            return false;
        }
        break;
    case X509_V_ERR_CERT_NOT_YET_VALID:
    case X509_V_ERR_CERT_HAS_EXPIRED:
        if(!(con->attrib & ssl_attribute_accept_expired)) {
            SAH_TRACE_ERROR("Verify certificate error %s", X509_verify_cert_error_string(err));
            return false;
        }
        break;
    default:
        SAH_TRACE_ERROR("Verify certificate error %s", X509_verify_cert_error_string(err));
        return false;
        break;
    }

    return true;
}

/**
   @brief
   Accepts a client connecting to an SSL enabled listen socket.

   @details
   Start the SSL handshake and negotation on a newly accepted socket.
   This function takes into account that the underlying socket is not blocking and can be called
   again to continue the SSL handshake if needed. If the SSL handshake and negotation is done
   the certificate verify function is called (see @ref connection_post_check).

   @param con A pointer to a connection object
   @param peer The container of the peer information

   @return
    - true when SSL handshake/negotation is:
        - done successfully and the certificate is accepted
        - is buzy.
    - false in any other case.
 */
static bool peer_ssl_accept(connection_info_t* con, peer_info_t* peer) {
    int code = SSL_accept(peer->ssl);
    if(code <= 0) {
        switch(SSL_get_error(peer->ssl, code)) {
        case SSL_ERROR_NONE:     /* everything is OK */
            /* do nothing */
            break;
        case SSL_ERROR_ZERO_RETURN:     /* SSL connection being closed */
            return false;
            break;
        case SSL_ERROR_WANT_READ:     /* SSL need to read on the socket during connect */
            peer->state = peer_state_retry_accept_need_read;
            /* just return OK, this function will be called again. */
            return true;
            break;
        case SSL_ERROR_WANT_WRITE:     /* SSL need to read on the socket during connect */
            peer->state = peer_state_retry_accept_need_write;
            /* just return OK, this function will be called again. */
            return true;
            break;
        default:
            SAH_TRACEZ_ERROR("pcb_con", "Error accepting SSL connection");
            SAH_TRACEZ_ERROR("pcb_con", "    %s", ERR_error_string(ERR_get_error(), NULL));
            return false;
            break;
        }
    }
    peer->state = peer_state_connected;
    long err = X509_V_OK;
    if(con->verify_client) {
        #if OPENSSL_VERSION_NUMBER >= 0x30000000L
        X509* cert = SSL_get1_peer_certificate(peer->ssl);
        #else
        X509* cert = SSL_get_peer_certificate(peer->ssl);
        #endif
        err = con->verify_client(cert);
        X509_free(cert);
        if(err != X509_V_OK) {
            SAH_TRACEZ_ERROR("pcb_con", "Error peer certificate %s", X509_verify_cert_error_string(err));
            return false;
        }
    }
    if(SSL_get_verify_result(peer->ssl) != X509_V_OK) {
        SAH_TRACEZ_ERROR("pcb_con", "Verify certificate error %s", X509_verify_cert_error_string(err));
        return false;
    }

    return true;
}
#endif

/**
   @brief
   Accept a client connection

   @details
   This functions accepts a client connection and creates a new client socket.
   The new incoming socket is always accepted, a new peer structure is allocated.
   If an accept event handler is available this will be called, if it returns false
   the socket is closed again.

   @param con A pointer to a connection object
   @param listenPeer The listen socket file descriptor.

   @return
    - true: connection is accepted
    - false: an error has occured, see @ref pcb_error for more detailed information
 */
static bool peer_accept(connection_info_t* con, peer_info_t* listenPeer) {
    SAH_TRACEZ_INFO("pcb_con", "Incoming socket accepted");
    connection_t* new_con = (connection_t*) calloc(1, sizeof(connection_t));
    if(!new_con) {
        pcb_error = pcb_error_no_memory;
        SAH_TRACEZ_ERROR("pcb_con", "%s", error_string(pcb_error));
        return false;
    }
    if(!peer_initialize(&new_con->info)) {
        SAH_TRACEZ_ERROR("pcb_con", "peer_initialize failed %s", error_string(pcb_error));
        free(new_con);
        return false;
    }

#ifdef OPEN_SSL_SUPPORT
    if(listenPeer->ssl_bio) {
        if(BIO_do_accept(listenPeer->ssl_bio) <= 0) {
            SAH_TRACEZ_ERROR("pcb_con", "Error accepting bio");
            peer_delete(&new_con->info);
            return false;
        } else {
            new_con->info.ssl_bio = BIO_pop(listenPeer->ssl_bio);
            new_con->info.ssl = SSL_new(con->ssl_ctx);
            SAH_TRACEZ_INFO("pcb_con", "Allocate SSL %p for peer %p", new_con->info.ssl, &new_con->info);
            if(!new_con->info.ssl) {
                SAH_TRACEZ_ERROR("pcb_con", "Error creating ssl context");
                peer_close(&new_con->info);
                peer_delete(&new_con->info);
                return false;
            }
            peer_ssl_setPeer(new_con->info.ssl, &(new_con->info));
            BIO_get_fd(new_con->info.ssl_bio, &new_con->info.socketfd);
            int flags = 0;
            if((flags = fcntl(new_con->info.socketfd, F_GETFL, 0)) < 0) {
                pcb_error = errno;
                SAH_TRACEZ_ERROR("pcb_con", "socket creation failed %s", error_string(pcb_error));
                peer_close(&new_con->info);
                peer_delete(&new_con->info);
                return false;
            }
            if(fcntl(new_con->info.socketfd, F_SETFL, flags | O_NONBLOCK) < 0) {
                pcb_error = errno;
                SAH_TRACEZ_ERROR("pcb_con", "socket creation failed %s", error_string(pcb_error));
                peer_close(&new_con->info);
                peer_delete(&new_con->info);
                return false;
            }
            SSL_set_bio(new_con->info.ssl, new_con->info.ssl_bio, new_con->info.ssl_bio);
            if(!peer_ssl_accept(con, &new_con->info)) {
                SAH_TRACEZ_ERROR("pcb_con", "ssl accept failed");
                peer_close(&new_con->info);
                peer_delete(&new_con->info);
                return false;
            }
        }
    } else {
#endif
    struct sockaddr_storage addr;
    socklen_t len = sizeof(struct sockaddr_storage);
    new_con->info.socketfd = accept(listenPeer->socketfd, (struct sockaddr*) &addr, &len);
    if(new_con->info.socketfd == -1) {
        pcb_error = errno;
        SAH_TRACEZ_ERROR("pcb_con", "accept failed %s", error_string(pcb_error));
        peer_close(&new_con->info);
        peer_delete(&new_con->info);
        return false;
    }
    int flags = fcntl(new_con->info.socketfd, F_GETFL, 0);
    if(flags < 0) {
        pcb_error = errno;
        SAH_TRACEZ_ERROR("pcb_con", "socket creation failed %s", error_string(pcb_error));
        peer_close(&new_con->info);
        peer_delete(&new_con->info);
        return false;
    }
    if(fcntl(new_con->info.socketfd, F_SETFL, flags | O_NONBLOCK) < 0) {
        pcb_error = errno;
        SAH_TRACEZ_ERROR("pcb_con", "socket creation failed %s", error_string(pcb_error));
        peer_close(&new_con->info);
        peer_delete(&new_con->info);
        return false;
    }
    memset(&new_con->info.addrInfo, 0, sizeof(struct addrinfo));
    new_con->info.addrInfo.ai_addr = (struct sockaddr*) calloc(1, len + 1);
    if(new_con->info.addrInfo.ai_addr) {
        memcpy(new_con->info.addrInfo.ai_addr, &addr, len);
    }
#ifdef OPEN_SSL_SUPPORT
}
#endif
    new_con->info.state = peer_state_connected;
    new_con->info.type = peer_type_client_connection;
    new_con->info.listensocket = listenPeer;

    peer_setUserData(&new_con->info, peer_getUserData(listenPeer));

    /* add to the list of accepted connections */
    llist_append(&con->connections, &new_con->it);

    if(!llist_isEmpty(&listenPeer->acceptHandlers)) {
        llist_iterator_t* it = NULL;
        peer_handler_list_t* acceptHandler = NULL;
        llist_for_each(it, &listenPeer->acceptHandlers) {
            acceptHandler = llist_item_data(it, peer_handler_list_t, it);
            if(acceptHandler->handler_function) {
                SAH_TRACEZ_INFO("pcb_con", "Calling accept handler ...");
                if(!acceptHandler->handler_function(&new_con->info)) {
                    SAH_TRACEZ_NOTICE("pcb_con", "connection accept callback did not accept socket");
                    peer_close(&new_con->info);
                    peer_delete(&new_con->info);
                    return false;
                } else {
                    SAH_TRACEZ_INFO("pcb_con", "Connection accepted");
                }
            }
        }
    }

    pcb_error = pcb_ok;
    return true;
}

/**
   @brief
   Try to write data to a socket.

   @details
   Tries to send as much data that is in the circular buffer over the socket.
   As soon as the socket would block (EGAIN) or the circular buffer is empty sending of data stops.
   For SSL enabled sockets SSL_write is used. The return codes SSL_ERROR_WANT_READ and SSL_ERROR_WANT_WRITE are
   handles as the socket would block.

   @param peer The peer structure

   @return
    - true: sending was succesfull.
    - false: an error has occured
 */
static bool peer_send(peer_info_t* peer) {
    if(peer->sendBuffer) {
        ssize_t sendresult = 0;
        ssize_t size = circbuf_availableForRead(peer->sendBuffer);
        while(size > 0) {
            SAH_TRACEZ_INFO("pcb_con", "Write %zd bytes from circbuf to socket", size);
#ifdef OPEN_SSL_SUPPORT
            if(peer->ssl) {
                sendresult = peer_send_ssl(peer, circbuf_readBuffer(peer->sendBuffer), size);
            } else {
#endif
            if(peer_isIPCSocket(peer)) {
                sendresult = peer_send_ipc(peer, circbuf_readBuffer(peer->sendBuffer), size);
            } else {
                sendresult = write(peer->socketfd, circbuf_readBuffer(peer->sendBuffer), size);
            }
#ifdef OPEN_SSL_SUPPORT
        }
#endif
            if(sendresult < 0) {
                if(errno == EAGAIN) {
                    break;
                } else {
                    pcb_error = errno;
                    // coverity does not like %m
                    SAH_TRACEZ_ERROR("pcb_con", "Failed to send %zd bytes %d", size, errno);
                    return false;
                }
            }
            circbuf_markAsRead(peer->sendBuffer, sendresult);
            if(sendresult != size) {
                break;
            }
            size = circbuf_availableForRead(peer->sendBuffer);
        }
        if((size == 0) && peer->writeDone) {
            return peer->writeDone(peer);
        }
    }
    pcb_error = pcb_ok;
    return true;
}


#ifdef OPEN_SSL_SUPPORT
static void peer_getUsernameFromCN(peer_info_t* peer) {
    if(!peer || !peer->ssl) {
        SAH_TRACEZ_ERROR("pcb_con", "invalid peer");
        return;
    }
    #if OPENSSL_VERSION_NUMBER >= 0x30000000L
    X509* cert = SSL_get1_peer_certificate(peer->ssl);
    #else
    X509* cert = SSL_get_peer_certificate(peer->ssl);
    #endif
    if(cert) {
        char sn[256];
        sn[0] = '\0';
        X509_NAME_oneline(X509_get_subject_name(cert), sn, 256);
        char* cn = strstr(sn, "CN=") + 3;
        if((cn >= sn) && (cn < sn + 256)) {
            char* cn_end = cn;
            while(isalnum(*cn_end) || *cn_end == '_') {
                cn_end++;
            }
            *cn_end = '\0';
            peer_setUsername(peer, cn);
            SAH_TRACEZ_WARNING("pcb_con", "Authenticated using client certificate as user %s", peer_getUsername(peer));
        } else {
            SAH_TRACEZ_ERROR("pcb_con", "No valid username (common name) found in client certificate (%s", sn);
        }
        X509_free(cert);
        peer->ssl_auth_checked = true;
    } else {
        SAH_TRACEZ_NOTICE("pcb_con", "couldn't retrieve certificate, are we authenticating via certifacte?");
    }
}
#endif

/**
   @brief
   Function to store client certificate info in the pcb ssl context.

   @details
   This function is called exclusively by peer_readInternal.
   In case ssl client certificate authentication was successful,
   the common name will be saved, to be used later as pcb username.

   @param peer The peer structure
 */
static void peer_handleClientCertAuth(connection_info_t* connection, peer_info_t* peer) {
    peer->ssl_auth = false;
#ifdef OPEN_SSL_SUPPORT
    if(!peer->ssl) {
        SAH_TRACEZ_NOTICE("pcb_con", "no ssl context in peer [socket %d, %p]", peer->socketfd, peer->ssl);
        return;
    }
    if((peer->state == peer_state_closing) || (peer->state == peer_state_closed)) {
        SAH_TRACEZ_WARNING("pcb_con", "peer is closing, abort auth");
        return;
    }
    if(!connection) {
        SAH_TRACEZ_ERROR("pcb_con", "no connection available");
        return;
    }
    SAH_TRACEZ_INFO("pcb_con", "peer (socket %d) state (%d)", peer->socketfd, peer->state);

    // authenticated
    if(connection->reuse_username) {
        if(connection->reuse_username(peer)) {
            SAH_TRACEZ_NOTICE("pcb_con", "Username [%s] reused from cached certificate", peer_getUsername(peer));
            peer->ssl_auth_checked = true;
        } else {
            SAH_TRACEZ_WARNING("pcb_con", "no client certificate provided (socket %d)", peer->socketfd);
        }
    } else {
        SAH_TRACEZ_NOTICE("pcb_con", "no reuse callback installed [%p], trying legacy fallback...", connection->reuse_username);
        // fallback legacy (deprecated) method to retrieve username
        peer_getUsernameFromCN(peer);
    }
#else
    (void) connection;
    peer->ssl_auth_checked = true;
#endif
}

/**
   @brief
   Generic read function for managed sockets

   @details
   For listen sockets the accept function is called. For other socjets the read handler is called.
   Reset the read event handler to NULL if not flagged as always read.

   @param con A pointer to a connection object
   @param peer The peer structure

   @return
    - true when the write is successfull.
    - false when an error occured.
 */
static bool peer_readInternal(connection_info_t* connection, peer_info_t* peer) {
    if(peer->type == peer_type_listen) {
        return peer_accept(connection, peer);
    } else if(peer->read) {
        peer_event_handler_t reader = peer->read;
        if(!(peer->handler_flags & peer_handler_read_always)) {
            peer->read = NULL;
        }
        if(!peer->ssl_auth_checked) {
            peer_handleClientCertAuth(connection, peer);
        }

#ifdef OPEN_SSL_SUPPORT
        SAH_TRACEZ_NOTICE("pcb_con", "Calling read event handler as user [%s] (socket %d) (%d/%d)...", peer_getUsername(peer), peer->socketfd, peer->ssl_auth_checked, peer->ssl_auth);
#else
        SAH_TRACEZ_NOTICE("pcb_con", "Calling read event handler...");
#endif
        if(!reader(peer)) {
            SAH_TRACEZ_NOTICE("pcb_con", "read handler failed, closing the socket and cleanup");
            return false;
        } else {
            SAH_TRACEZ_NOTICE("pcb_con", "Read handler done");
            return true;
        }
    } else {
        return true;
    }
}

/**
   @brief
   Generic write function for managed sockets

   @details
   When a circular buffer is available and it contains data, the circular buffer will be emptied
   as first. If no data is available in the circular buffer or no buffer is available
   the write handler will be called. The write handler can write to the socket direcly
   until the write to the socket returns EAGAIN.
   Reset the write event handler to NULL if not flagged as always write.

   @param peer The peer structure

   @return
    - true when the write is successfull.
    - false when an error occured.
 */
static bool peer_writeInternal(peer_info_t* peer) {
    if(peer->state == peer_state_connecting) {
        int valopt;
        socklen_t lon;
        lon = sizeof(int);
        /* Use getsockopt to check connection status */
        if(getsockopt(peer->socketfd, SOL_SOCKET, SO_ERROR, (void*) (&valopt), &lon) < 0) {
            // coverity does not like %m
            //SAH_TRACEZ_ERROR("pcb_con", "Error in getsockopt() %d - %m",errno);
            SAH_TRACEZ_ERROR("pcb_con", "Error in getsockopt() %d", errno);
            return false;
        }
        switch(valopt) {
        case 0:
            peer->type = peer_type_server_connection;
            peer->state = peer_state_connected;
            if(peer->connect) {
                peer->connect(peer);
            }
            return true;
            break;
        case EINPROGRESS:
            /* still buzy, should not have been called, but just continue */
            return true;
            break;
        default:
            SAH_TRACEZ_ERROR("pcb_con", "Error in connect() %d - %s", valopt, error_string(valopt));
            /* return false, close will be called (and the close handle(s)) */
            return false;
            break;
        }
    }

    if(peer->sendBuffer && (circbuf_availableForRead(peer->sendBuffer) > 0)) {
        if(!peer_send(peer)) {
            SAH_TRACEZ_NOTICE("pcb_con", "write failed, closing the socket and cleanup");
            return false;
        }
        return true;
    }
    if(peer->write) {
        peer_event_handler_t writer = peer->write;
        if(!(peer->handler_flags & peer_handler_write_always)) {
            peer->write = NULL;
        }
        SAH_TRACEZ_INFO("pcb_con", "Calling write event handler ...");
        if(!writer(peer)) {
            SAH_TRACEZ_NOTICE("pcb_con", "write handler failed, closing the socket and cleanup");
            return false;
        }
    }

    return true;
}

/**
   @ingroup pcb_socket_layer_peer
   @brief
   Initialize a peer structure

   @details
   This function will initializes the peer data structure with default values.\n

   @param peer The peer structure you want to initialize

   @return
    - true if the peer structure was initialized correctly
    - false if an error occurred, pcb_error contains more info about the error
        - peer is NULL
 */
bool peer_initialize(peer_info_t* peer) {
    if(!peer) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    peer_type_t type = peer->type;
    memset(peer, 0, sizeof(peer_info_t));
    peer->type = type;

    peer->socketfd = -1;

    peer->ssl_auth_checked = false;
    peer->ssl_auth = false;
    peer->ssl_cert_cn[0] = '\0';

    if(!llist_initialize(&peer->closeHandlers)) {
        return false;
    }

    if(!llist_initialize(&peer->acceptHandlers)) {
        return false;
    }

    if(!llist_initialize(&peer->send_fd_list)) {
        return false;
    }

    if(!llist_initialize(&peer->recv_fd_list)) {
        return false;
    }

    pcb_error = pcb_ok;
    return true;
}

/**
   @ingroup pcb_socket_layer_peer
   @brief
   Closes and removes the peer.

   @details
   Close the peer's socket and streams, clean up the peer structure.and free all allocated memory.

   @param peer The peer structure you are interested in
 */
void peer_delete(peer_info_t* peer) {
    SAH_TRACEZ_IN("pcb_con");
    if(!peer) {
        pcb_error = pcb_error_invalid_parameter;
        SAH_TRACEZ_OUT("pcb_con");
        return;
    }

    if(peer->addrInfo.ai_addr) {
        free(peer->addrInfo.ai_addr);
        memset(&peer->addrInfo, 0, sizeof(struct addrinfo));
    }

    if(peer->destroy) {
        peer->destroy(peer);
    }

    /* close streams, no access should be done afterwards */
    if(peer->in && (fclose(peer->in) != 0)) {
        pcb_error = errno;
        SAH_TRACEZ_ERROR("pcb_con", "Failed to close in stream");
    }
    peer->in = NULL;
    if(peer->out && (fclose(peer->out) != 0)) {
        pcb_error = errno;
        SAH_TRACEZ_ERROR("pcb_con", "Failed to close out stream");
    }
    peer->out = NULL;

    llist_iterator_t* it = llist_takeFirst(&peer->closeHandlers);
    peer_handler_list_t* ch = NULL;
    while(it) {
        ch = llist_item_data(it, peer_handler_list_t, it);
        free(ch);
        it = llist_takeFirst(&peer->closeHandlers);
    }

    it = llist_takeFirst(&peer->acceptHandlers);
    ch = NULL;
    while(it) {
        ch = llist_item_data(it, peer_handler_list_t, it);
        free(ch);
        it = llist_takeFirst(&peer->acceptHandlers);
    }

    it = llist_takeFirst(&peer->send_fd_list);
    peer_transport_fd_t* fd = NULL;
    while(it) {
        fd = llist_item_data(it, peer_transport_fd_t, it);
        if(peer->handler_flags & peer_close_fd_after_send) {
            close(fd->fd);
        }
        free(fd);
        it = llist_takeFirst(&peer->send_fd_list);
    }

    it = llist_takeFirst(&peer->recv_fd_list);
    fd = NULL;
    while(it) {
        fd = llist_item_data(it, peer_transport_fd_t, it);
        close(fd->fd);
        free(fd);
        it = llist_takeFirst(&peer->recv_fd_list);
    }

    connection_t* c = llist_item_data(peer, connection_t, info);
    SAH_TRACEZ_INFO("pcb_con", "Removing peer for connections list (it = 0x%p)", &c->it);
    llist_iterator_take(&c->it);

    if(peer->sendBuffer) {
        circbuf_cleanup(peer->sendBuffer);
        free(peer->sendBuffer);
    }

    free(c);

    SAH_TRACEZ_OUT("pcb_con");
    return;
}

/**
   @ingroup pcb_socket_layer_peer
   @brief
   Closes and removes the peer.

   @details
   Close the peer's socket, and clean the peer structure.

   @param peer The peer structure you are interested in

   @return
    - true: peer is destroyed and can not be used anymore
    - false: an error has occured
 */
bool peer_destroy(peer_info_t* peer) {
    if(!peer || (peer->state == peer_state_deleted)) {
        pcb_error = pcb_error_invalid_parameter;
        return true;
    }

    /* closing the file descriptor */
    peer_close(peer);

    /* mark peer as deleted, it will be deleted in the event handler loop at some point
       when it is safe to delete it
     */
    SAH_TRACEZ_INFO("pcb_con", "Mark peer for deletion");
    peer->state = peer_state_deleted;

    return true;
}

/**
   @ingroup pcb_socket_layer_peer
   @brief
   Close the peer.

   @details
   Close the peer's socket, but do not clean the peer structure.
   Call @ref peer_reconnect to set up the connection again. The @ref peer_reconnect
   only works for server conenctions and not for closed client (incomming) connections.

   @param peer The peer structure you are interested in

   @return
    - true: the socket has been closed.
    - false: an error has occured.
 */
bool peer_close(peer_info_t* peer) {
    bool retval = true;
    if(!peer) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    pcb_error = pcb_ok;

    if(peer->state == peer_state_closing) {
        /* recursive call to peer close !!
           was already being closed
         */
        return true;
    }

    if(peer->socketfd == -1) {
        return true;
    }

    SAH_TRACEZ_NOTICE("pcb_con", "Closing peer fd=%d", peer->socketfd);
    peer->state = peer_state_closing;

    llist_iterator_t* it = NULL;
    peer_handler_list_t* closeHandler = NULL;
    llist_for_each(it, &peer->closeHandlers) {
        closeHandler = llist_item_data(it, peer_handler_list_t, it);
        if(closeHandler->handler_function) {
            closeHandler->handler_function(peer);
        }
    }

    SAH_TRACEZ_INFO("ssl", "clean ssl auth info (socket %d)", peer->socketfd);
    peer->ssl_auth_checked = false;
    peer->ssl_auth = false;
#ifdef OPEN_SSL_SUPPORT
    if(peer->ssl) {
        SAH_TRACEZ_NOTICE("ssl", "SSL_shutdown");
        int retsd = SSL_shutdown(peer->ssl);
        SAH_TRACEZ_INFO("ssl", "SSL free %p in peer %p, shutdown ret:%d", peer->ssl, peer, retsd);
        if(retsd == -1) {
            SAH_TRACEZ_ERROR("ssl", "SSL free %p in peer %p, shutdown ret:%d", peer->ssl, peer, retsd);
        }
        SSL_set_shutdown(peer->ssl, SSL_SENT_SHUTDOWN);
        SSL_free(peer->ssl);
        peer->ssl = NULL;
        /*
         * NOTE from www.openssl.org
         * SSL_free() also calls the free()ing procedures for indirectly affected items, if applicable:
         * the buffering BIO, the read and write BIOs, cipher lists specially created for this ssl,
         * the SSL_SESSION. Do not explicitly free these indirectly freed up items before or after
         * calling SSL_free(), as trying to free things twice may lead to program failure.
         */
        peer->ssl_bio = NULL;
    }
    if(peer->ssl_bio) {
        SAH_TRACEZ_INFO("ssl", "BIO free");
        BIO_free(peer->ssl_bio);
        peer->ssl_bio = NULL;
    }

    free(peer->node);
    peer->node = NULL;
#endif
    if(close(peer->socketfd) == -1) {
        pcb_error = errno;
        SAH_TRACEZ_WARNING("pcb_con", "Failed to close socket %s", error_string(pcb_error));
        retval = false;
    }
    peer->socketfd = -1;

    /* the peer may have been destroyed in the close handler */
    if(peer->state == peer_state_closing) {
        peer->state = peer_state_closed;
    }

    if(peer->in) {
        fclose(peer->in);
        peer->in = NULL;
    }

    if(peer->out) {
        fclose(peer->out);
        peer->out = NULL;
    }

    return retval;
}

/**
   @ingroup pcb_socket_layer_peer
   @brief
   Reconnect to a server

   @details
   Try to reconnect to the server when the connection was broken or closed. The server must be
   connected to at least once before, if this is not the case this function will fail.

   @param peer A pointer to a peer_info_t structure

   @return
    - true: The connection to the server is recreated.
    - false: an error has occured. See @ref pcb_error to get error details.
 */
bool peer_reconnect(peer_info_t* peer) {
    if(!peer || (peer->state == peer_state_deleted)) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(!peer->addrInfo.ai_addr) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(!peer_isServerConnection(peer)) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(peer_isConnected(peer) || peer_isConnecting(peer)) {
        return true;
    }

    if(peer->addrInfo.ai_addr->sa_family == AF_UNIX) {
        peer->socketfd = socket(AF_UNIX, SOCK_STREAM, 0);
        if(peer->socketfd == -1) {
            pcb_error = errno;
            return false;
        }
    } else {
        peer->socketfd = socket(peer->addrInfo.ai_family, peer->addrInfo.ai_socktype, peer->addrInfo.ai_protocol);
        if(peer->socketfd == -1) {
            pcb_error = errno;
            return false;
        }
    }
    int flags = fcntl(peer->socketfd, F_GETFL, 0);
    if(flags < 0) {
        pcb_error = errno;
        SAH_TRACEZ_ERROR("pcb_con", "socket creation failed %s", error_string(pcb_error));
        close(peer->socketfd);
        peer->socketfd = -1;
        return false;
    }
    if(connect(peer->socketfd, peer->addrInfo.ai_addr, peer->addrInfo.ai_addrlen) == -1) {
        pcb_error = errno;
        SAH_TRACEZ_ERROR("pcb_con", "socket connect failed %s", error_string(pcb_error));
        close(peer->socketfd);
        peer->socketfd = -1;
        return false;
    }
    if(fcntl(peer->socketfd, F_SETFL, flags | O_NONBLOCK) < 0) {
        pcb_error = errno;
        SAH_TRACEZ_ERROR("pcb_con", "socket creation failed %s", error_string(pcb_error));
        close(peer->socketfd);
        peer->socketfd = -1;
        return false;
    }

    if(peer->connect) {
        peer->connect(peer);
    }
    connection_info_t* con = peer_connection(peer);
    llist_iterator_t* it = NULL;
    peer_handler_list_t* connectHandler = NULL;
    llist_for_each(it, &con->connectHandlers) {
        connectHandler = llist_item_data(it, peer_handler_list_t, it);
        if(connectHandler->handler_function) {
            connectHandler->handler_function(peer);
        }
    }

    return true;
}

peer_info_t* peer_listenSocket(peer_info_t* peer) {
    if(!peer) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    return peer->listensocket;
}

/**
   @ingroup pcb_socket_layer_peer
   @brief
   Change the encapsulated file descriptor

   @details
   Changing the file descriptors only works on peers of the custom type.

   @param peer A pointer to a peer_info_t structure
   @param fd the new file descriptor

   @return
    - true: The connection to the server is recreated.
    - false: an error has occured. See @ref pcb_error to get error details.
 */
bool peer_setFd(peer_info_t* peer, int fd) {
    if(!peer || (peer->state == peer_state_deleted)) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(!peer_isCustomFd(peer)) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    peer->socketfd = fd;

    pcb_error = pcb_ok;
    return true;
}

/**
   @ingroup pcb_socket_layer_peer
   @brief
   Check that the peer has a read handler

   @details
   This function will check that a read handler is installed on the peer.\n
   If data becomes available, it can be handled. If no read handler is installed,
   there is no possibility to read data from the encapsulated fd.

   @param peer A pointer to a peer_info_t structure

   @return
    - true: If read handler is installed or it is a listen socket
    - false: If disconnected or no read handler is installed
 */
bool peer_needRead(peer_info_t* peer) {
    if(!peer || (peer->state == peer_state_deleted) || (peer->socketfd == -1)) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(fcntl(peer->socketfd, F_GETFD) == -1) {
        return false; // File descriptor is not valid
    }

    if(peer_isListenSocket(peer)) {
        return true;
    }

    if(!peer_isConnected(peer)) {
        return false;
    }

#ifdef OPEN_SSL_SUPPORT
    if(peer->state == peer_state_connected) {
#endif
    if(peer->read) {
        return true;
    }
#ifdef OPEN_SSL_SUPPORT
} else {
    switch(peer->state) {
    case peer_state_retry_connect_need_read:
    case peer_state_retry_accept_need_read:
    case peer_state_retry_read_need_read:
    case peer_state_retry_write_need_read:
        return true;
    default:
        break;
    }
}
#endif

    return false;
}

/**
   @ingroup pcb_socket_layer_peer
   @brief
   Check that the peer has a data to write or a write handler

   @details
   This function will check that data is waiting to be written to the encapsulated fd.
   Also it checks that a write handler is installed.

   @param peer A pointer to a peer_info_t structure

   @return
    - true: If data is available to write or a write handler is installed
    - false: If disconnected or no data is available to write
 */
bool peer_needWrite(peer_info_t* peer) {
    if(!peer || (peer->state == peer_state_deleted) || (peer->socketfd == -1)) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(fcntl(peer->socketfd, F_GETFD) == -1) {
        return false; // File descriptor is not valid
    }

    if(peer_isListenSocket(peer)) {
        return false;
    }

    if(peer_isConnecting(peer)) {
        return true;
    }

    if(!peer_isConnected(peer)) {
        return false;
    }

#ifdef OPEN_SSL_SUPPORT
    if((peer->state == peer_state_connected) || (peer->state == peer_state_connecting)) {
#endif

    if((peer->sendBuffer && (circbuf_availableForRead(peer->sendBuffer) > 0)) || peer->write) {
        return true;
    } else if(peer->sendBuffer && (circbuf_availableForRead(peer->sendBuffer) == 0)) {
        if(peer->out) {
            circbuf_reset(peer->sendBuffer, defaultBufferSize);
        } else {
            circbuf_cleanup(peer->sendBuffer);
            free(peer->sendBuffer);
            peer->sendBuffer = NULL;
        }
    } else {
        return false;
    }
#ifdef OPEN_SSL_SUPPORT
} else {
    switch(peer->state) {
    case peer_state_retry_connect_need_write:
    case peer_state_retry_accept_need_write:
    case peer_state_retry_read_need_write:
    case peer_state_retry_write_need_write:
        return true;
    default:
        break;
    }
}
#endif

    return false;
}

/**
   @ingroup pcb_socket_layer_peer
   @brief
   When data is available on the peer, call this function to read and handle the data

   @details
   This function will call the read handler, which will read the data from the underlying fd.
   If needed it will perform other actions on the recieved data.\n
   If SSL was used, it will also handle all SSL functionality.

   @param peer A pointer to a peer_info_t structure

   @return
    - true: if the read was successful
    - false: an error occured.
 */
bool peer_handleRead(peer_info_t* peer) {
    if(!peer || (peer->state == peer_state_deleted)) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    connection_info_t* connection = peer_connection(peer);
#ifdef OPEN_SSL_SUPPORT
    if(peer->state == peer_state_connected) {
        return peer_readInternal(connection, peer);
    }
    switch(peer->state) {
    case peer_state_retry_connect_need_read:
        SAH_TRACEZ_NOTICE("ssl", "retry connect need read");
        if(!peer_ssl_connect(connection, peer, peer->node)) {
            SAH_TRACEZ_ERROR("ssl", "ssl connection failed, closing the socket and cleanup");
            return false;
        }
        return true;
        break;
    case peer_state_retry_accept_need_read:
        SAH_TRACEZ_NOTICE("ssl", "retry accept need read");
        if(!peer_ssl_accept(connection, peer)) {
            SAH_TRACEZ_ERROR("ssl", "ssl accept failed, closing the socket and cleanup");
            return false;
        }
        return true;
        break;
    case peer_state_retry_read_need_read:
        SAH_TRACEZ_NOTICE("ssl", "ssl retry read need read");
        peer->state = peer_state_connected;
        return peer_readInternal(connection, peer);
        break;
    case peer_state_retry_write_need_read:
        SAH_TRACEZ_NOTICE("ssl", "ssl retry write need read");
        peer->state = peer_state_connected;
        return peer_writeInternal(peer);
        break;
    default:
        break;
    }
#endif
    return peer_readInternal(connection, peer);
}

/**
   @ingroup pcb_socket_layer_peer
   @brief
   When data is available to write call this function to write the data

   @details
   This function will call the write handler, which will write the data to the underlying fd or
   if data is available in the buffer, tries to write as much as possible to the fd.\n
   If SSL was used, it will also handle all SSL functionality.

   @param peer A pointer to a peer_info_t structure

   @return
    - true: if the write was successful
    - false: an error occured.
 */
bool peer_handleWrite(peer_info_t* peer) {
    if(!peer || (peer->state == peer_state_deleted)) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

#ifdef OPEN_SSL_SUPPORT
    connection_info_t* connection = peer_connection(peer);
    if(peer->state == peer_state_connected) {
        return peer_writeInternal(peer);
    }
    switch(peer->state) {
    case peer_state_retry_connect_need_write:
        SAH_TRACEZ_NOTICE("ssl", "retry connect need write");
        if(!peer_ssl_connect(connection, peer, peer->node)) {
            SAH_TRACEZ_ERROR("ssl", "ssl connection failed, closing the socket and cleanup");
            return false;
        }
        return true;
        break;
    case peer_state_retry_accept_need_write:
        SAH_TRACEZ_NOTICE("ssl", "retry accept need write");
        if(!peer_ssl_accept(connection, peer)) {
            SAH_TRACEZ_ERROR("ssl", "ssl accept failed, closing the socket and cleanup");
            return false;
        }
        return true;
        break;
    case peer_state_retry_read_need_write:
        SAH_TRACEZ_NOTICE("ssl", "ssl retry read need write");
        peer->state = peer_state_connected;
        return peer_readInternal(connection, peer);
        break;
    case peer_state_retry_write_need_write:
        SAH_TRACEZ_NOTICE("ssl", "ssl retry write need write");
        peer->state = peer_state_connected;
        return peer_writeInternal(peer);
        break;
    default:
        break;
    }
#endif
    return peer_writeInternal(peer);
}

/**
   @ingroup pcb_socket_layer_peer
   @brief
   Stream like write to a peer.

   @details
   This function behaves like the fwrite of the c library. The data written is not written directly to the file
   descriptor, but stored in a buffer. The data will be pushed on the file descriptor when calling @ref peer_flush
   or when back in the event loop.

   @param peer The peer structure
   @param data pointer to the data memory block
   @param size size of a single data block
   @param nmemb number of data blocks

   @return
    - the size of the data written
    - -1 if an error occured
 */
size_t peer_fwrite(peer_info_t* peer, const void* data, size_t size, size_t nmemb) {
    if(!peer || !data) {
        return -1;
    }

    if(!peer->sendBuffer && !peer_createBuffer(&(peer->sendBuffer))) {
        return -1;
    }
    return circbuf_writeBuffer(peer->sendBuffer, data, size * nmemb);
}

/**
   @ingroup pcb_socket_layer_peer
   @brief
   Check if the peer uses a TCP socket

   @details
   This function checks if the peer is using a TCP socket.\n

   @param peer The peer structure you want to check

   @return
    - true if the peer is using a TCP socket
    - false if the peer is not using a TCP socket or if an error occurred, pcb_error contains more info about the error
        - peer is NULL
        - peer is not connected
 */
bool peer_isTCPSocket(peer_info_t* peer) {
    if(!peer || (peer->state == peer_state_deleted)) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(peer_isCustomFd(peer)) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(peer->socketfd == -1) {
        pcb_error = pcb_error_not_connected;
        return false;
    }

#ifdef OPEN_SSL_SUPPORT
    if(peer->ssl_bio) {
        return true;
    }
#endif

    pcb_error = pcb_ok;
    if(peer->addrInfo.ai_addr &&
       ((peer->addrInfo.ai_addr->sa_family == AF_INET) ||
        (peer->addrInfo.ai_addr->sa_family == AF_INET6))) {
        return true;
    }
    return false;
}

/**
   @ingroup pcb_socket_layer_peer
   @brief
   Check if the peer uses an IPC socket

   @details
   This function checks if the peer is using an IPC socket.\n

   @param peer The peer structure you want to check

   @return
    - true if the peer is using an IPC socket
    - false if the peer is not using an IPC socket or if an error occurred, pcb_error contains more info about the error
        - peer is NULL
        - peer is not connected
 */
bool peer_isIPCSocket(peer_info_t* peer) {
    if(!peer || (peer->state == peer_state_deleted)) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(peer_isCustomFd(peer)) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(peer->socketfd == -1) {
        pcb_error = pcb_error_not_connected;
        return false;
    }

#ifdef OPEN_SSL_SUPPORT
    if(peer->ssl_bio) {
        return false;
    }
#endif

    pcb_error = pcb_ok;
    if(peer->addrInfo.ai_addr && (peer->addrInfo.ai_addr->sa_family == AF_UNIX)) {
        return true;
    }

    return false;
}

/**
   @ingroup pcb_socket_layer_peer
   @brief
   Is this a client connection

   @details
   This function will check that the connection was initiated by a client.
   If the peer is not in the list of client connections (or accepted connections)
   this function will return false

   @param peer A peer to a peer_info_t structure

   @return
    - true: This connection was initiated by a client, and was accepted
    - false: This connection was initiated by us.
 */
bool peer_isClientConnection(peer_info_t* peer) {
    if(!peer || (peer->state == peer_state_deleted)) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(peer_isCustomFd(peer)) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    return (peer->type == peer_type_client_connection);
}

/**
   @ingroup pcb_socket_layer_peer
   @brief
   Is this a server connection

   @details
   This function will check that the connection was initiated by the application itself.
   If the peer is not in the list of server connections (or created connections)
   this function will return false

   @param peer A peer to a peer_info_t structure

   @return
    - true: This connection was initiated by the application itself
    - false: This connection was initiated by a client.
 */
bool peer_isServerConnection(peer_info_t* peer) {
    if(!peer || (peer->state == peer_state_deleted)) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(peer_isCustomFd(peer)) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    return (peer->type == peer_type_server_connection);
}

/**
   @ingroup pcb_socket_layer_peer
   @brief
   Is this a custom file descriptor

   @details

   @param peer A peer to a peer_info_t structure

   @return
    - true: This is a peer_info_t that contains a custom fd
    - false: This is not an custom file descritptor peer
 */
bool peer_isCustomFd(peer_info_t* peer) {
    if(!peer || (peer->state == peer_state_deleted)) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    return (peer->type == peer_type_custom_fd);
}

/**
   @ingroup pcb_socket_layer_peer
   @brief
   Is this a listen socket

   @details
   This function will check that the peer contains a listen socket

   @param peer A peer to a peer_info_t structure

   @return
    - true: This peer_info_t contains a listen socket
    - false: This peer_info_t does not contain a listen socket
 */
bool peer_isListenSocket(peer_info_t* peer) {
    if(!peer || (peer->state == peer_state_deleted)) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    return (peer->type == peer_type_listen);
}

/**
   @ingroup pcb_socket_layer_peer
   @brief
   Checks the the peer_info_t contains a valid file descriptor

   @details
   This function will check that the peer contains a valid file descriptor.
   If the peer was closed this function will return false, but it is still possible to do the @ref peer_reconnect

   @param peer A peer to a peer_info_t structure

   @return
    - true: This peer_info_t contains a valid file descriptor
    - false: This peer_info_t does not contain a valid file descriptor
 */
bool peer_isConnected(peer_info_t* peer) {
    if(!peer) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    switch(peer->state) {
    case peer_state_listening:  /* listening */
    case peer_state_connecting: /* still connecting */
    case peer_state_closed:     /* peer already closed, connection is gone */
    case peer_state_closing:    /* peer is closing, connection will be gone soon */
    case peer_state_deleted:    /* peer is marked for deletion */
        return false;
        break;
    default:
    case peer_state_connected:     /* peer is connected */
        return (peer->socketfd != -1);
        break;
    }
}

bool peer_isConnecting(peer_info_t* peer) {
    if(!peer || (peer->state == peer_state_deleted)) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(peer->socketfd == -1) {
        return false;
    }

    if(peer->state == peer_state_connecting) {
        /* still trying to connect */
        return true;
    }

    return false;
}


bool peer_isDeleted(peer_info_t* peer) {
    if(!peer || (peer->state == peer_state_deleted)) {
        return true;
    }

    return false;
}

/**
   @ingroup pcb_socket_layer_peer
   @brief
   Returns the peer capabilities

   @details
   Depending on the underlying type of the file descriptor or if SSL is enable or not for this peer,
   the capabilities can differ. With this function you will get a bitmap so you can check what is possible.
   The current capabilities are:
    - peer_cap_direct_fd_access: This flag is never set for SSL enabled sockets
    - peer_cap_buffered_io: This peer is using a buffer when writing to it.
    - peer_cap_ssl_enabled: SSL is enabled on this peer, all communication is encrypted
    - peer_cap_transport_fd: The peer can transport file descriptors, only available for ipc sockets

   @param peer A peer to a peer_info_t structure

   @return
    - A bitmap with the peer capabilities.
 */
uint32_t peer_capabilities(peer_info_t* peer) {
    if(!peer || (peer->state == peer_state_deleted)) {
        pcb_error = pcb_error_invalid_parameter;
        return 0;
    }

    uint32_t caps = 0;
#ifdef OPEN_SSL_SUPPORT
    if(!peer->ssl_bio) {
        caps |= peer_cap_direct_fd_access;
    } else {
        caps |= peer_cap_ssl_enabled;
    }
#else
    caps |= peer_cap_direct_fd_access;
#endif
    if(peer->out && (peer->stream_type == peer_stream_buffered_io)) {
        caps |= peer_cap_buffered_io;
    }

    if(peer_isIPCSocket(peer)) {
        caps |= peer_cap_transport_fd;
    }

    return caps;
}

/**
   @ingroup pcb_socket_layer_peer
   @brief
   Get the connection_info_t structure from a peer.

   @details
   This function will return a pointer to the connection_info_t structure where the given peer_info_t structures
   belongs to. A peer_info_t structure only belongs to exact one connection_info_t structure.

   @param peer A peer to a peer_info_t structure

   @return
    - the pointer to the connection_info_t structure
    - NULL: an error has occured.
 */
connection_info_t* peer_connection(peer_info_t* peer) {
    if(!peer) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    connection_info_t* con = NULL;
    if(peer->type != peer_type_listen) {
        connection_t* c = llist_item_data(peer, connection_t, info);
        con = llist_item_data(c->it.list, connection_info_t, connections);
    } else {
        connection_t* c = llist_item_data(peer, connection_t, info);
        con = llist_item_data(c->it.list, connection_info_t, listenSockets);
    }

    return con;
}

/**
   @ingroup pcb_socket_layer_peer
   @brief
   Tries to send as much as possible pending data.

   @details
   This functions tries to send as musch as possible data of the buffered data.
   No garantee is given that all data is written onto the sockets. If a write on a socket would block, no data is
   writen to the socket.

   @param peer

   @return
    - true: OK
    - false: failed to send data on one or more sockets
 */
bool peer_flush(peer_info_t* peer) {
    if(!peer || (peer->state == peer_state_deleted) || (peer->socketfd == -1)) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(peer_isListenSocket(peer)) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    /* if no data available return now */
    if(!peer->sendBuffer || ((circbuf_availableForRead(peer->sendBuffer) == 0) && !peer->write)) {
        return true;
    }

    if(peer->socketfd == -1) {
        return true;
    }

    if(circbuf_availableForRead(peer->sendBuffer) > 0) {
        SAH_TRACEZ_INFO("pcb_con", "=== DATA AVAILABLE  ==");
        if(!peer_send(peer)) {
            SAH_TRACEZ_NOTICE("pcb_con", "write failed for socket %d", peer->socketfd);
            return false;
        }
        return true;
    }
    if(peer->write) {
        SAH_TRACEZ_INFO("pcb_con", "Calling write event handler ...");
        if(!peer->write(peer)) {
            SAH_TRACEZ_NOTICE("pcb_con", "write handler failed for socket %d", peer->socketfd);
            return false;
        } else {
            if(!(peer->handler_flags & peer_handler_write_always)) {
                peer->write = NULL;
            }
        }
    }

    return true;
}

/**
   @ingroup pcb_socket_layer_peer
   @brief
   Get the out stream

   @details
   This function returns the out stream.\n You can use this stream with any c library functions
   that has a stream (FILE *) as an argument.
   This stream is write-only, Trying to read from the stream will give an error.

   @param peer The initialized peer structure
   @param streamtype The stream type is or direct io or buffered io. When using direct io a would block condition must be handled by the implementer, When using buffered io it is handled by the library itself.

   @return
    - An out stream
    - NULL
 */
FILE* peer_outStream(peer_info_t* peer, peer_stream_type_t streamtype) {
    if(!peer || (peer->state == peer_state_deleted)) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    if(peer->out) {
        if(peer->stream_type != streamtype) {
            peer_setStreamType(peer, streamtype);
        }

        return peer->out;
    }

    if(peer_isListenSocket(peer)) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    cookie_io_functions_t hooks = {
        .read = NULL,
        .write = &peer_write,
        .seek = &peer_seek_dummy,
        .close = &peer_close_outstream
    };

    if((streamtype != peer_stream_direct_io) && !peer_createBuffer(&(peer->sendBuffer))) {
        return NULL;
    }
    peer->stream_type = streamtype;

    peer->out = fopencookie(peer, "w+", hooks);
    if(peer->out == NULL) {
        return NULL;
    }

    if(setvbuf(peer->out, NULL, _IONBF, 0) == -1) {
        fclose(peer->out);
        peer->out = NULL;
        return NULL;
    }

    return peer->out;
}

/**
   @ingroup pcb_socket_layer_peer
   @brief
   Get the in stream

   @details
   This function returns the in stream.\n You can use this stream with any c library functions
   that has a stream (FILE *) as an argument.
   This stream is read-only, Trying to write o the stream will give an error.

   The in stream is always using direct io, no buffer is in between.

   @param peer The initialized peer structure

   @return
    - An out stream
    - NULL
 */
FILE* peer_inStream(peer_info_t* peer) {
    if(!peer || (peer->state == peer_state_deleted)) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    if(peer_isListenSocket(peer)) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    if(!peer->in) {
        cookie_io_functions_t hooks = {
            .read = &peer_read,
            .write = NULL,
            .seek = &peer_seek_dummy,
            .close = &peer_close_instream
        };

        peer->in = fopencookie(peer, "r+", hooks);
    }

    return peer->in;
}

/**
   @ingroup pcb_socket_layer_peer
   @brief
   Change the stream type

   @details
   This function will change the stream type of the out stream of the peer.
   When changing the stream type for buffer io to direct io, the buffer will be emptied and written to the underlying fd.
   So before writing to the stream, make sure there are no more bytes queued in the buffer. See @ref peer_bytesQueued

   @param peer The initialized peer structure
   @param streamtype The stream type is or direct io or buffered io. When using direct io a would block condition must be handled by the implementer, When using buffered io it is handled by the library itself.

   @return
    - true: switching the stream type was successful.
    - false an error occured.
 */
bool peer_setStreamType(peer_info_t* peer, peer_stream_type_t streamtype) {
    if(!peer || (peer->state == peer_state_deleted)) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(peer_isListenSocket(peer)) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(!peer->out) {
        SAH_TRACEZ_WARNING("pcb_con", "No output stream open");
        return false;
    }

    if((streamtype != peer_stream_direct_io) && !peer_createBuffer(&peer->sendBuffer)) {
        SAH_TRACEZ_ERROR("pcb_con", "Failed to create circular buffer");
        return false;
    }

    peer->stream_type = streamtype;
    return true;
}

/**
   @ingroup pcb_socket_layer_peer
   @brief
   Check to see if the out buffer still contains bytes.

   @details
   This function can be used to check that all data wwas written to the underlying file descriptor.

   @param peer The initialized peer structure

   @return
    - The number of bytes in the buffer.
 */
size_t peer_bytesQueued(peer_info_t* peer) {
    if(!peer || (peer->state == peer_state_deleted)) {
        pcb_error = pcb_error_invalid_parameter;
        return 0;
    }

    return peer->sendBuffer ? peer->sendBuffer->toRead : 0;
}

/**
   @ingroup pcb_socket_layer_peer
   @brief
   Peek a peer.

   @details
   You can peek in a peer, to check the incomming bytes and look at theire content, without really removing them.
   When doing a read on the in stream of the peer after a peek, the same bytes are returned.

   @param peer The initialized peer structure
   @param buffer A preallocated buffer to store the bytes in.
   @param size The size of the buffer

   @return
    - The number of bytes in the buffer.
 */
ssize_t peer_peek(peer_info_t* peer, char* buffer, uint32_t size) {
    if(!peer || (peer->state == peer_state_deleted)) {
        pcb_error = pcb_error_invalid_parameter;
        return 0;
    }

    memset(buffer, 0, size);
#ifdef OPEN_SSL_SUPPORT
    if(peer->ssl_bio) {
        ssize_t retval = 0;
        if(!peer->data_size) {
            memset(peer->buffer, 0, 128);
            peer->data_offset = 0;
            retval = peer->data_size = SSL_read(peer->ssl, peer->buffer, 128);
            if(retval <= 0) {
                SAH_TRACEZ_ERROR("pcb_con", "%s", ERR_error_string(ERR_get_error(), NULL));
                peer->data_size = 0;
                return retval;
            }
        }
        ssize_t length = (size > (peer->data_size - peer->data_offset)) ? (peer->data_size - peer->data_offset) : size;
        memcpy(buffer, peer->buffer + peer->data_offset, length);
        return length;
    } else {
#endif
    return recv(peer->socketfd, buffer, size, MSG_PEEK);
#ifdef OPEN_SSL_SUPPORT
}
#endif
}

/**
   @ingroup pcb_socket_layer_peer
   @brief
   Set an event handler on the peer

   @details
   All event handlers have the same prototype. See @ref peer_event_handler_t.

   The different types if events are:
    - peer_event_accepted: only for listen sockets
    - peer_event_read: set a read handler, will be called whenever data is available for read
    - peer_event_write: set a write handler, will be called when the socket is available for write
    - peer_event_write_done: set a write done handler. Will be called when all data in the buffer is written to the fd
    - peer_event_close: add a close handler, will be called when the underlying file descriptor is closed
    - peer_event_destroy: add a destroy handler, will be called when the peer_info_t structure is going to be deleted
    - peer_event_reconnect: add a reconnect handler, will be called when the socket is reconnected.

   Also some flags can be given to modify the behavior.
    - peer_handler_read_always: only for a read handler or accepted handler, if this flag is not set, the read handler will be removed after the next read.
    - peer_handler_write_always: only for a write handler, if this flag is not set, the write handler will be removed after the next write.

   @param peer The initialized peer structure
   @param event The event type.
   @param handlerFlags flags
   @param handlerfn The handler function. See @ref peer_event_handler_t

   @return
    - true: The handler was set.
    - false an error occured.
 */
bool peer_setEventHandler(peer_info_t* peer, peer_event_type_t event, uint32_t handlerFlags, peer_event_handler_t handlerfn) {
    if(!peer || (peer->state == peer_state_deleted)) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    switch(event) {
    case peer_event_accepted:
        if(!peer_addAcceptHandler(peer, handlerfn)) {
            return false;
        }
        peer->handler_flags |= (handlerFlags & ~peer_handler_write_always);
        break;
    case peer_event_read:
        if(peer_isListenSocket(peer)) {
            pcb_error = pcb_error_invalid_parameter;
            return false;
        }
        peer->read = handlerfn;
        peer->handler_flags |= (handlerFlags & ~peer_handler_write_always);
        break;
    case peer_event_write:
        if(peer_isListenSocket(peer)) {
            pcb_error = pcb_error_invalid_parameter;
            return false;
        }
        peer->write = handlerfn;
        peer->handler_flags |= (handlerFlags & ~peer_handler_read_always);
        break;
    case peer_event_write_done:
        if(peer_isListenSocket(peer)) {
            pcb_error = pcb_error_invalid_parameter;
            return false;
        }
        peer->writeDone = handlerfn;
        peer->handler_flags |= (handlerFlags & ~peer_handler_read_always);
        break;
    case peer_event_close:
        if(!peer_addCloseHandler(peer, handlerfn)) {
            return false;
        }
        peer->handler_flags |= (handlerFlags & ~(peer_handler_read_always & peer_handler_write_always));
        break;
    case peer_event_destroy:
        peer->destroy = handlerfn;
        peer->handler_flags |= (handlerFlags & ~(peer_handler_read_always & peer_handler_write_always));
        break;
    case peer_event_connected:
        if(peer->type != peer_type_server_connection) {
            pcb_error = pcb_error_invalid_parameter;
            return false;
        }
        peer->connect = handlerfn;
        if(peer->connect) {
            peer->connect(peer);
        }
        break;
    default:
        break;
    }

    return true;
}

/**
   @ingroup pcb_socket_layer_peer
   @brief
   Add a close handler

   @details
   The close event supports multiple event handlers. The same handler can not be set multiple times.

   @param peer The initialized peer structure
   @param handlerfn The handler function. See @ref peer_event_handler_t

   @return
    - true: The handler was added.
    - false an error occured.
 */
bool peer_addCloseHandler(peer_info_t* peer, peer_event_handler_t handlerfn) {
    if(!peer || (peer->state == peer_state_deleted)) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(peer_isListenSocket(peer)) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(!handlerfn) {
        return true;
    }

    llist_iterator_t* it = NULL;
    peer_handler_list_t* closeHandler = NULL;
    llist_for_each(it, &peer->closeHandlers) {
        closeHandler = llist_item_data(it, peer_handler_list_t, it);
        if(closeHandler->handler_function == handlerfn) {
            return true;
        }
    }

    peer_handler_list_t* handler = (peer_handler_list_t*) calloc(1, sizeof(peer_handler_list_t));
    if(!handler) {
        return false;
    }

    handler->handler_function = handlerfn;
    llist_prepend(&peer->closeHandlers, &handler->it);

    return true;
}

/**
   @ingroup pcb_socket_layer_peer
   @brief
   Add an accept handler

   @details
   The accept event supports multiple event handlers. The same handler can not be set multiple times.

   @param peer The initialized peer structure
   @param handlerfn The handler function. See @ref peer_event_handler_t

   @return
    - true: The handler was added.
    - false an error occured.
 */
bool peer_addAcceptHandler(peer_info_t* peer, peer_event_handler_t handlerfn) {
    if(!peer || (peer->state == peer_state_deleted)) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(!peer_isListenSocket(peer)) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(!handlerfn) {
        return true;
    }

    llist_iterator_t* it = NULL;
    peer_handler_list_t* acceptHandler = NULL;
    llist_for_each(it, &peer->acceptHandlers) {
        acceptHandler = llist_item_data(it, peer_handler_list_t, it);
        if(acceptHandler->handler_function == handlerfn) {
            return true;
        }
    }

    peer_handler_list_t* handler = (peer_handler_list_t*) calloc(1, sizeof(peer_handler_list_t));
    if(!handler) {
        return false;
    }

    handler->handler_function = handlerfn;
    llist_append(&peer->acceptHandlers, &handler->it);

    return true;
}

/**
   @ingroup pcb_socket_layer_peer
   @brief
   Remove a close handler

   @details
   If the given handler was added to the close handlers, it will be removed. If the handler was not in the list
   of close handlers nothing happens

   @param peer The initialized peer structure
   @param handlerfn The handler function. See @ref peer_event_handler_t

   @return
    - true: The handler was removed.
    - false an error occured.
 */
bool peer_removeCloseHandler(peer_info_t* peer, peer_event_handler_t handlerfn) {
    if(!peer) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(peer_isListenSocket(peer)) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    llist_iterator_t* it = NULL;
    peer_handler_list_t* closeHandler = NULL;
    llist_for_each(it, &peer->closeHandlers) {
        closeHandler = llist_item_data(it, peer_handler_list_t, it);
        if(closeHandler->handler_function == handlerfn) {
            closeHandler->handler_function = NULL;
            break;
        }
    }

    return true;
}

/**
   @ingroup pcb_socket_layer_peer
   @brief
   Remove an accept handler

   @details
   If the given handler was added to the accept handlers, it will be removed. If the handler was not in the list
   of accept handlers nothing happens

   @param peer The initialized peer structure
   @param handlerfn The handler function. See @ref peer_event_handler_t

   @return
    - true: The handler was removed.
    - false an error occured.
 */
bool peer_removeAcceptHandler(peer_info_t* peer, peer_event_handler_t handlerfn) {
    if(!peer) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(!peer_isListenSocket(peer)) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    llist_iterator_t* it = NULL;
    peer_handler_list_t* acceptHandler = NULL;
    llist_for_each(it, &peer->acceptHandlers) {
        acceptHandler = llist_item_data(it, peer_handler_list_t, it);
        if(acceptHandler->handler_function == handlerfn) {
            acceptHandler->handler_function = NULL;
            break;
        }
    }

    return true;
}

/**
   @ingroup pcb_socket_layer_peer
   @brief
   Add a file descriptor for transport.

   @details
   Attach a file descriptor to the peer for transport. File descriptors can only be transported using IPC sockets.\n
   With the next write operation the file descriptor is send. The file descriptor will be close automatically if
   the close flag was set. See @ref peer_setCloseFd

   @param peer The peer structure
   @param fd The file descriptor to transport

   @return
    - true: the file descriptor is added.
    - false an error occured.
 */
bool peer_attachFd(peer_info_t* peer, int fd) {
    if(!peer) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(!peer_isIPCSocket(peer)) {
        return false;
    }

    peer_transport_fd_t* tfd = (peer_transport_fd_t*) calloc(1, sizeof(peer_transport_fd_t));
    if(!tfd) {
        return false;
    }

    tfd->fd = fd;
    llist_append(&peer->send_fd_list, &tfd->it);

    return true;
}

/**
   @ingroup pcb_socket_layer_peer
   @brief
   Get the first received file descriptor.

   @details
   This function will return the next available file descriptor. These file descriptors where transported.
   Sending and receiving file descriptors can only be done using IPC sockets.

   @param peer The peer structure

   @return
    - A received file descriptor.
    - -1 if no file descriptor was received or is available.
 */
int peer_fetchFd(peer_info_t* peer) {
    if(!peer) {
        pcb_error = pcb_error_invalid_parameter;
        return -1;
    }

    if(!peer_isIPCSocket(peer)) {
        return -1;
    }

    llist_iterator_t* it = llist_takeFirst(&peer->recv_fd_list);
    if(!it) {
        return -1;
    }

    peer_transport_fd_t* tfd = llist_item_data(it, peer_transport_fd_t, it);
    int fd = tfd->fd;
    free(tfd);

    return fd;
}

/**
   @ingroup pcb_socket_layer_peer
   @brief
   Check that file descriptors are received on this peer.

   @details
   Check that file descriptors are received on this peer.

   @param peer The peer structure

   @return
    - true when received file descriptors are available
    - false when no received file descriptors are available
 */
bool peer_hasReceivedFd(peer_info_t* peer) {
    if(!peer) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(!peer_isIPCSocket(peer)) {
        return false;
    }

    return !llist_isEmpty(&peer->recv_fd_list);
}

/**
   @ingroup pcb_socket_layer_peer
   @brief
   Set automated close of transported file descriptors

   @details
   Use this function to tell the library that file descriptors must be closed after transport.

   @param peer The peer structure
   @param close Set to true when file descriptors must be closed after transport, false otherwise

   @return
    - true: .
    - false an error occured.
 */
bool peer_setCloseFd(peer_info_t* peer, bool close) {
    if(!peer) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(!peer_isIPCSocket(peer)) {
        return false;
    }

    if(close) {
        peer->handler_flags |= peer_close_fd_after_send;
    } else {
        peer->handler_flags &= ~peer_close_fd_after_send;
    }

    return true;
}


