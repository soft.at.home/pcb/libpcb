/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>

#include <pcb/common/error.h>
#include <pcb/utils/variant_list.h>

/**
   @file
   @brief
   Implementation of the public variant list functions
 */

/**
   @ingroup pcb_utils_variant_list
   @brief
   Initialize a variant linked list.

   @details
   This function initializes a variant linked list.\n
   It is mandatory to initialize a variant linked list before using it.\n
   Every @ref variant_list_initialize call should have a corresponding @ref variant_list_cleanup call to free the memory.\n

   @param varlist the address to the variant list to needs to be initialized

   @return
    - true: Initialization was succesfull, the llist_t item is ready to be used
    - false: An error has occurred
        - the varlist is a NULL pointer
        - memory allocation failed
 */
bool variant_list_initialize(variant_list_t* varlist) {
    return llist_initialize(varlist);
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Cleanup a variant list.

   @details
   This function <b>frees</b> the variant list.\n
   After calling this function, the variant list is empty and all variants in the list are deleted.\n

   @param varlist the variant list that has to be cleaned up
 */
void variant_list_cleanup(variant_list_t* varlist) {
    variant_list_clear(varlist);
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Remove all elements from a variant list.

   @details
   This function removes all elements from the variant list. \n
   This function loops over all variant elements, using @ref variant_list_takeFirst to remove an \n
   element from the list and @ref variant_list_iterator_destroy to free the memory for that item.\n

   @param varlist the variant list that has to be cleared
 */
void variant_list_clear(variant_list_t* varlist) {
    variant_list_iterator_t* it = NULL;

    it = variant_list_takeFirst(varlist);
    while(it) {
        variant_list_iterator_destroy(it);
        it = variant_list_takeFirst(varlist);
    }
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Create and initialize a <b>variant list iterator</b> (=variant list item)

   @details
   This function <b>creates and initializes</b> a @ref variant_list_iterator_t. \n
   This function call creates the variant list iterator, and initializing the @ref variant_t and @ref llist_iterator_t
   elements using @ref variant_initialize and @ref llist_iterator_initialize\n

   The iterator will contain a variant that is initialize to the same value and type as the provided variant.
   If a NULL pointer was given to this function the iterator will contain a variant of type @ref variant_type_unknown

   @warning
   The returned iterator must be freed when not inserted in a list using @ref variant_list_iterator_destoy

   @param variant the variant that has to be used to initialize the iterator (optional)

   @return
    - true: Initialization was succesfull, the variant_list_iterator_t item is ready to be used
    - false: An error has occurred
        - memory is full
 */
variant_list_iterator_t* variant_list_iterator_create(const variant_t* variant) {
    variant_list_iterator_t* it = calloc(1, sizeof(variant_list_iterator_t));
    if(!it) {
        pcb_error = pcb_error_no_memory;
        return NULL;
    }
    if(!variant_initialize(&it->variant, variant_type(variant))) {
        free(it);
        pcb_error = pcb_error_no_memory;
        return NULL;
    }
    if(variant) {
        variant_copy(&it->variant, variant);
    }

    return it;
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Create and initialize a <b>variant list iterator</b> (=variant list item)

   @details
   This function <b>creates and initializes</b> a <b>variant list iterator</b>. \n
   This function call creates the variant list iterator, and initializing the variant and list
   elements using @ref variant_initialize and @ref llist_iterator_initialize\n
   The returned iterator must be freed when not inserted in a list.

   @param variant the variant that has to be used to initialize the iterator

   @return
    - true: Initialization was succesfull, the variant_list_iterator_t item is ready to be used
    - false: An error has occurred
        - memory is full

 */
variant_list_iterator_t* variant_list_iterator_createRef(variant_t* variant) {
    if(!variant) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    variant_list_iterator_t* it = calloc(1, sizeof(variant_list_iterator_t));
    if(!it) {
        pcb_error = pcb_error_no_memory;
        return NULL;
    }
    if(!variant_initialize(&it->variant, variant_type_reference)) {
        free(it);
        pcb_error = pcb_error_no_memory;
        return NULL;
    }

    variant_setReference(&it->variant, variant);

    return it;
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Create and initialize a <b>variant list iterator</b> (=variant list item)

   @details
   This function <b>creates and initializes</b> a <b>variant list iterator</b> and initializes the
   data part of the iterator.\n
   The returned iterator must be freed when not inserted in a list.

   @param data the data that has to be used to initialize the iterator (optional)

   @return
    - true: Initialization was succesfull, the variant_list_iterator_t item is ready to be used
    - false: An error has occurred
        - memory is full
 */
variant_list_iterator_t* variant_list_iterator_createChar(const char* data) {
    variant_list_iterator_t* it = calloc(1, sizeof(variant_list_iterator_t));
    if(!it) {
        pcb_error = pcb_error_no_memory;
        return NULL;
    }

    variant_initialize(&it->variant, variant_type_string);
    if(data) {
        variant_setChar(&it->variant, data);
    }

    return it;
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Create and initialize a <b>variant list iterator</b> (=variant list item)

   @details
   This function <b>creates and initializes</b> a <b>variant list iterator</b> and initializes the
   data part of the iterator. \n
   The returned iterator must be freed when not inserted in a list.

   @param data the data that has to be used to initialize the iterator (optional)

   @return
    - true: Initialization was succesfull, the variant_list_iterator_t item is ready to be used
    - false: An error has occurred
        - memory is full

 */
variant_list_iterator_t* variant_list_iterator_createString(const string_t* data) {
    variant_list_iterator_t* it = calloc(1, sizeof(variant_list_iterator_t));
    if(!it) {
        pcb_error = pcb_error_no_memory;
        return NULL;
    }

    variant_initialize(&it->variant, variant_type_string);
    if(data) {
        variant_setString(&it->variant, data);
    }

    return it;
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Create and initialize a <b>variant list iterator</b> (=variant list item)

   @details
   This function <b>creates and initializes</b> a <b>variant list iterator</b> and initializes the
   data part of the iterator. \n
   The returned iterator must be freed when not inserted in a list.

   @param data the data that has to be used to initialize the iterator (optional)

   @return
    - true: Initialization was succesfull, the variant_list_iterator_t item is ready to be used
    - false: An error has occurred
        - memory is full
 */
variant_list_iterator_t* variant_list_iterator_createInt8(int8_t data) {
    variant_list_iterator_t* it = calloc(1, sizeof(variant_list_iterator_t));
    if(!it) {
        pcb_error = pcb_error_no_memory;
        return NULL;
    }

    variant_initialize(&it->variant, variant_type_int8);
    variant_setInt8(&it->variant, data);

    return it;
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Create and initialize a <b>variant list iterator</b> (=variant list item)

   @details
   This function <b>creates and initializes</b> a <b>variant list iterator</b> and initializes the
   data part of the iterator. \n
   The returned iterator must be freed when not inserted in a list.

   @param data the data that has to be used to initialize the iterator (optional)

   @return
    - true: Initialization was succesfull, the variant_list_iterator_t item is ready to be used
    - false: An error has occurred
        - memory is full
 */
variant_list_iterator_t* variant_list_iterator_createInt16(int16_t data) {
    variant_list_iterator_t* it = calloc(1, sizeof(variant_list_iterator_t));
    if(!it) {
        pcb_error = pcb_error_no_memory;
        return NULL;
    }

    variant_initialize(&it->variant, variant_type_int16);
    variant_setInt16(&it->variant, data);

    return it;
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Create and initialize a <b>variant list iterator</b> (=variant list item)

   @details
   This function <b>creates and initializes</b> a <b>variant list iterator</b> and initializes the
   data part of the iterator.\n
   The returned iterator must be freed when not inserted in a list.

   @param data the data that has to be used to initialize the iterator (optional)

   @return
    - true: Initialization was succesfull, the variant_list_iterator_t item is ready to be used
    - false: An error has occurred
        - memory is full
 */
variant_list_iterator_t* variant_list_iterator_createInt32(int32_t data) {
    variant_list_iterator_t* it = calloc(1, sizeof(variant_list_iterator_t));
    if(!it) {
        pcb_error = pcb_error_no_memory;
        return NULL;
    }

    variant_initialize(&it->variant, variant_type_int32);
    variant_setInt32(&it->variant, data);

    return it;
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Create and initialize a <b>variant list iterator</b> (=variant list item)

   @details
   This function <b>creates and initializes</b> a <b>variant list iterator</b> and initializes the
   data part of the iterator. \n
   \n
   The returned iterator must be freed when not inserted in a list.

   @param data the data that has to be used to initialize the iterator (optional)

   @return
    - true: Initialization was succesfull, the variant_list_iterator_t item is ready to be used
    - false: An error has occurred
        - memory is full
 */
variant_list_iterator_t* variant_list_iterator_createInt64(int64_t data) {
    variant_list_iterator_t* it = calloc(1, sizeof(variant_list_iterator_t));
    if(!it) {
        pcb_error = pcb_error_no_memory;
        return NULL;
    }

    variant_initialize(&it->variant, variant_type_int64);
    variant_setInt64(&it->variant, data);

    return it;
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Create and initialize a <b>variant list iterator</b> (=variant list item)

   @details
   This function <b>creates and initializes</b> a <b>variant list iterator</b> and initializes the
   data part of the iterator. \n
   The returned iterator must be freed when not inserted in a list.

   @param data the data that has to be used to initialize the iterator (optional)

   @return
    - true: Initialization was succesfull, the variant_list_iterator_t item is ready to be used
    - false: An error has occurred
        - memory is full
 */
variant_list_iterator_t* variant_list_iterator_createUInt8(uint8_t data) {
    variant_list_iterator_t* it = calloc(1, sizeof(variant_list_iterator_t));
    if(!it) {
        pcb_error = pcb_error_no_memory;
        return NULL;
    }

    variant_initialize(&it->variant, variant_type_uint8);
    variant_setUInt8(&it->variant, data);

    return it;
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Create and initialize a <b>variant list iterator</b> (=variant list item)

   @details
   This function <b>creates and initializes</b> a <b>variant list iterator</b> and initializes the
   data part of the iterator. \n
   The returned iterator must be freed when not inserted in a list.

   @param data the data that has to be used to initialize the iterator (optional)

   @return
    - true: Initialization was succesfull, the variant_list_iterator_t item is ready to be used
    - false: An error has occurred
        - memory is full
 */
variant_list_iterator_t* variant_list_iterator_createUInt16(uint16_t data) {
    variant_list_iterator_t* it = calloc(1, sizeof(variant_list_iterator_t));
    if(!it) {
        pcb_error = pcb_error_no_memory;
        return NULL;
    }

    variant_initialize(&it->variant, variant_type_uint16);
    variant_setUInt16(&it->variant, data);

    return it;
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Create and initialize a <b>variant list iterator</b> (=variant list item)

   @details
   This function <b>creates and initializes</b> a <b>variant list iterator</b> and initializes the
   data part of the iterator. \n
   The returned iterator must be freed when not inserted in a list.

   @param data the data that has to be used to initialize the iterator (optional)

   @return
    - true: Initialization was succesfull, the variant_list_iterator_t item is ready to be used
    - false: An error has occurred
        - memory is full
 */
variant_list_iterator_t* variant_list_iterator_createUInt32(uint32_t data) {
    variant_list_iterator_t* it = calloc(1, sizeof(variant_list_iterator_t));
    if(!it) {
        pcb_error = pcb_error_no_memory;
        return NULL;
    }

    variant_initialize(&it->variant, variant_type_int32);
    variant_setUInt32(&it->variant, data);

    return it;
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Create and initialize a <b>variant list iterator</b> (=variant list item)

   @details
   This function <b>creates and initializes</b> a <b>variant list iterator</b> and initializes the
   data part of the iterator. \n
   The returned iterator must be freed when not inserted in a list.

   @param data the data that has to be used to initialize the iterator (optional)

   @return
    - true: Initialization was succesfull, the variant_list_iterator_t item is ready to be used
    - false: An error has occurred
        - memory is full
 */
variant_list_iterator_t* variant_list_iterator_createUInt64(uint64_t data) {
    variant_list_iterator_t* it = calloc(1, sizeof(variant_list_iterator_t));
    if(!it) {
        pcb_error = pcb_error_no_memory;
        return NULL;
    }

    variant_initialize(&it->variant, variant_type_int64);
    variant_setUInt64(&it->variant, data);

    return it;
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Create and initialize a <b>variant list iterator</b> (=variant list item)

   @details
   This function <b>creates and initializes</b> a <b>variant list iterator</b> and initializes the
   data part of the iterator. \n
   The returned iterator must be freed when not inserted in a list.

   @param data the data that has to be used to initialize the iterator (optional)

   @return
    - true: Initialization was succesfull, the variant_list_iterator_t item is ready to be used
    - false: An error has occurred
        - memory is full
 */
variant_list_iterator_t* variant_list_iterator_createDouble(double data) {
    variant_list_iterator_t* it = calloc(1, sizeof(variant_list_iterator_t));
    if(!it) {
        pcb_error = pcb_error_no_memory;
        return NULL;
    }

    variant_initialize(&it->variant, variant_type_double);
    variant_setDouble(&it->variant, data);

    return it;
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Create and initialize a <b>variant list iterator</b> (=variant list item)

   @details
   This function <b>creates and initializes</b> a <b>variant list iterator</b> and initializes the
   data part of the iterator. \n
   The returned iterator must be freed when not inserted in a list.

   @param data the data that has to be used to initialize the iterator (optional)

   @return
    - true: Initialization was succesfull, the variant_list_iterator_t item is ready to be used
    - false: An error has occurred
        - memory is full
 */
variant_list_iterator_t* variant_list_iterator_createBool(bool data) {
    variant_list_iterator_t* it = calloc(1, sizeof(variant_list_iterator_t));
    if(!it) {
        pcb_error = pcb_error_no_memory;
        return NULL;
    }

    variant_initialize(&it->variant, variant_type_bool);
    variant_setBool(&it->variant, data);

    return it;
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Create and initialize a <b>variant list iterator</b> (=variant list item)

   @details
   This function <b>creates and initializes</b> a <b>variant list iterator</b> and initializes the
   data part of the iterator. \n
   The returned iterator must be freed when not inserted in a list.

   @param data the data that has to be used to initialize the iterator (optional)

   @return
    - true: Initialization was succesfull, the variant_list_iterator_t item is ready to be used
    - false: An error has occurred
        - memory is full
 */
variant_list_iterator_t* variant_list_iterator_createDateTime(const struct tm* data) {
    variant_list_iterator_t* it = calloc(1, sizeof(variant_list_iterator_t));
    if(!it) {
        pcb_error = pcb_error_no_memory;
        return NULL;
    }

    variant_initialize(&it->variant, variant_type_date_time);
    if(data) {
        variant_setDateTime(&it->variant, data);
    }

    return it;
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Create and initialize a <b>variant list iterator</b> (=variant list item)

   @details
   This function <b>creates and initializes</b> a <b>variant list iterator</b> and initializes the
   data part of the iterator. \n
   The returned iterator must be freed when not inserted in a list.

   @param data the data that has to be used to initialize the iterator (optional)

   @return
    - true: Initialization was successful, the variant_list_iterator_t item is ready to be used
    - false: An error has occurred
        - memory is full
 */
variant_list_iterator_t* variant_list_iterator_createDateTimeExtended(const pcb_datetime_t* data) {
    variant_list_iterator_t* it = calloc(1, sizeof(variant_list_iterator_t));
    if(!it) {
        pcb_error = pcb_error_no_memory;
        return NULL;
    }

    variant_initialize(&it->variant, variant_type_date_time);
    if(data) {
        variant_setDateTimeExtended(&it->variant, data);
    }

    return it;
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Create and initialize a <b>variant list iterator</b> (=variant list item)

   @details
   This function <b>creates and initializes</b> a <b>variant list iterator</b> and initializes the
   data part of the iterator. \n
   The returned iterator must be freed when not inserted in a list.

   @param data the data that has to be used to initialize the iterator (optional)

   @return
    - true: Initialization was succesfull, the variant_list_iterator_t item is ready to be used
    - false: An error has occurred
        - memory is full
 */
variant_list_iterator_t* variant_list_iterator_createListMove(variant_list_t* data) {
    variant_list_iterator_t* it = calloc(1, sizeof(variant_list_iterator_t));
    if(!it) {
        pcb_error = pcb_error_no_memory;
        return NULL;
    }

    variant_initialize(&it->variant, variant_type_array);
    if(data) {
        variant_setListMove(&it->variant, data);
    }

    return it;
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Create and initialize a <b>variant list iterator</b> (=variant list item)

   @details
   This function <b>creates and initializes</b> a <b>variant list iterator</b> and initializes the
   data part of the iterator by copying the given variant list. \n
   The returned iterator must be freed when not inserted in a list.

   @param data the data that has to be used to initialize the iterator (optional)

   @return
    - true: Initialization was succesfull, the variant_list_iterator_t item is ready to be used
    - false: An error has occurred
        - memory is full
 */
variant_list_iterator_t* variant_list_iterator_createListCopy(const variant_list_t* data) {
    variant_list_iterator_t* it = calloc(1, sizeof(variant_list_iterator_t));
    if(!it) {
        pcb_error = pcb_error_no_memory;
        return NULL;
    }

    variant_initialize(&it->variant, variant_type_array);
    if(data) {
        variant_setListCopy(&it->variant, data);
    }

    return it;
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Create and initialize a <b>variant list iterator</b> (=variant list item)

   @details
   This function <b>creates and initializes</b> a <b>variant list iterator</b> and initializes the
   data part of the iterator by copying the given variant list. \n
   The returned iterator must be freed when not inserted in a list.

   @param data the data that has to be used to initialize the iterator (optional)

   @return
    - true: Initialization was succesfull, the variant_list_iterator_t item is ready to be used
    - false: An error has occurred
        - memory is full
 */
variant_list_iterator_t* variant_list_iterator_createListRef(variant_list_t* data) {
    variant_list_iterator_t* it = calloc(1, sizeof(variant_list_iterator_t));
    if(!it) {
        pcb_error = pcb_error_no_memory;
        return NULL;
    }

    variant_initialize(&it->variant, variant_type_array_reference);
    if(data) {
        variant_setListRef(&it->variant, data);
    }

    return it;
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Create and initialize a <b>variant list iterator</b> (=variant list item)

   @details
   This function <b>creates and initializes</b> a <b>variant list iterator</b> and initializes the
   data part of the iterator. \n
   \n
   The returned iterator must be freed when not inserted in a list.

   @param data the data that has to be used to initialize the iterator (optional)

   @return
    - true: Initialization was succesfull, the variant_list_iterator_t item is ready to be used
    - false: An error has occurred
        - memory is full
 */
variant_list_iterator_t* variant_list_iterator_createMapMove(variant_map_t* data) {
    variant_list_iterator_t* it = calloc(1, sizeof(variant_list_iterator_t));
    if(!it) {
        pcb_error = pcb_error_no_memory;
        return NULL;
    }

    variant_initialize(&it->variant, variant_type_map);
    if(data) {
        variant_setMapMove(&it->variant, data);
    }

    return it;
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Create and initialize a <b>variant list iterator</b> (=variant list item)

   @details
   This function <b>creates and initializes</b> a <b>variant list iterator</b> and initializes the
   data part of the iterator by copying the given variant map. \n
   The returned iterator must be freed when not inserted in a list.

   @param data the data that has to be used to initialize the iterator (optional)

   @return
    - true: Initialization was succesfull, the variant_list_iterator_t item is ready to be used
    - false: An error has occurred
        - memory is full
 */
variant_list_iterator_t* variant_list_iterator_createMapCopy(const variant_map_t* data) {
    variant_list_iterator_t* it = calloc(1, sizeof(variant_list_iterator_t));
    if(!it) {
        pcb_error = pcb_error_no_memory;
        return NULL;
    }

    variant_initialize(&it->variant, variant_type_map);
    if(data) {
        variant_setMapCopy(&it->variant, data);
    }

    return it;
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Create and initialize a <b>variant list iterator</b> (=variant list item)

   @details
   This function <b>creates and initializes</b> a <b>variant list iterator</b> and initializes the
   data part of the iterator by copying the given variant map. \n
   The returned iterator must be freed when not inserted in a list.

   @param data the data that has to be used to initialize the iterator (optional)

   @return
    - true: Initialization was succesfull, the variant_list_iterator_t item is ready to be used
    - false: An error has occurred
        - memory is full
 */
variant_list_iterator_t* variant_list_iterator_createMapRef(variant_list_t* data) {
    variant_list_iterator_t* it = calloc(1, sizeof(variant_list_iterator_t));
    if(!it) {
        pcb_error = pcb_error_no_memory;
        return NULL;
    }

    variant_initialize(&it->variant, variant_type_map_reference);
    if(data) {
        variant_setMapRef(&it->variant, data);
    }

    return it;
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Destroy a variant linked list iterator.

   @details
   This function <b>frees</b> the variant linked list iterator. \n
   After calling this function, the variant linked list iterator can not be used anymore, until it is re-created.\n
   The iterator will be removed from a list.\n
   The data part of the iterator will be freed\n

   @param it the variant list iterator that has to be cleaned up
 */
void variant_list_iterator_destroy(variant_list_iterator_t* it) {
    if(!it) {
        pcb_error = pcb_error_invalid_parameter;
        return;
    }
    variant_list_iterator_take(it);
    variant_cleanup(&it->variant);
    free(it);
}

variant_t* variant_list_iterator_data(const variant_list_iterator_t* it) {
    if(!it) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    if(variant_type(&it->variant) == variant_type_reference) {
        return it->variant.data.v;
    }
    return (variant_t*) (&it->variant);
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Add a copy of a variant to the list

   @details
   This function adds a copy of the given variant to the list.

   @param varlist the variant list
   @param variant the variant to add

   @return
    - true when successful.
 */
bool variant_list_add(variant_list_t* varlist, const variant_t* variant) {
    if(!varlist) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    variant_list_iterator_t* it = variant_list_iterator_create(variant);
    if(!it) {
        return false;
    }

    return variant_list_append(varlist, it);

    return true;
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Move variant to the list

   @details
   This function move the given variant to the list.

   @param varlist the variant list
   @param variant the variant to move

   @return
    - true when successful.
 */
bool variant_list_addMove(variant_list_t* varlist, variant_t* variant) {
    if(!varlist) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    variant_list_iterator_t* it = variant_list_iterator_create(NULL);
    if(!it) {
        return false;
    }

    variant_move(&it->variant, variant);

    return variant_list_append(varlist, it);
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Add a reference to a variant to the list

   @details
   This function adds a reference to the given variant to the list.

   @param varlist the variant list
   @param variant the variant to add

   @return
    - true when successful.
 */
bool variant_list_addRef(variant_list_t* varlist, variant_t* variant) {
    if(!varlist || !variant) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    variant_list_iterator_t* it = variant_list_iterator_createRef(variant);
    if(!it) {
        return false;
    }

    return variant_list_append(varlist, it);
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Add a copy of data to a variant list

   @details
   This function adds a copy of the given data to the list.

   @param varlist the variant list
   @param data the data to add

   @return
    - true when successful.
 */
bool variant_list_addChar(variant_list_t* varlist, const char* data) {
    if(!varlist) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    variant_list_iterator_t* it = variant_list_iterator_createChar(data);
    if(!it) {
        return false;
    }

    return variant_list_append(varlist, it);
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Add a copy of data to a variant list

   @details
   This function adds a copy of the given data to the list.

   @param varlist the variant list
   @param data the data to add

   @return
    - true when successful.
 */
bool variant_list_addString(variant_list_t* varlist, const string_t* data) {
    if(!varlist) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    variant_list_iterator_t* it = variant_list_iterator_createString(data);
    if(!it) {
        return false;
    }

    return variant_list_append(varlist, it);
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Add a copy of data to a variant list

   @details
   This function adds a copy of the given data to the list.

   @param varlist the variant list
   @param data the data to add

   @return
    - true when successful.
 */
bool variant_list_addInt8(variant_list_t* varlist, int8_t data) {
    if(!varlist) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    variant_list_iterator_t* it = variant_list_iterator_createInt8(data);
    if(!it) {
        return false;
    }

    return variant_list_append(varlist, it);
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Add a copy of data to a variant list

   @details
   This function adds a copy of the given data to the list.

   @param varlist the variant list
   @param data the data to add

   @return
    - true when successful.
 */
bool variant_list_addInt16(variant_list_t* varlist, int16_t data) {
    if(!varlist) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    variant_list_iterator_t* it = variant_list_iterator_createInt16(data);
    if(!it) {
        return false;
    }

    return variant_list_append(varlist, it);
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Add a copy of data to a variant list

   @details
   This function adds a copy of the given data to the list.

   @param varlist the variant list
   @param data the data to add

   @return
    - true when successful.
 */
bool variant_list_addInt32(variant_list_t* varlist, int32_t data) {
    if(!varlist) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    variant_list_iterator_t* it = variant_list_iterator_createInt32(data);
    if(!it) {
        return false;
    }

    return variant_list_append(varlist, it);
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Add a copy of data to a variant list

   @details
   This function adds a copy of the given data to the list.

   @param varlist the variant list
   @param data the data to add

   @return
    - true when successful.
 */
bool variant_list_addInt64(variant_list_t* varlist, int64_t data) {
    if(!varlist) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    variant_list_iterator_t* it = variant_list_iterator_createInt64(data);
    if(!it) {
        return false;
    }

    return variant_list_append(varlist, it);
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Add a copy of data to a variant list

   @details
   This function adds a copy of the given data to the list.

   @param varlist the variant list
   @param data the data to add

   @return
    - true when successful.
 */
bool variant_list_addUInt8(variant_list_t* varlist, uint8_t data) {
    if(!varlist) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    variant_list_iterator_t* it = variant_list_iterator_createUInt8(data);
    if(!it) {
        return false;
    }

    return variant_list_append(varlist, it);
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Add a copy of data to a variant list

   @details
   This function adds a copy of the given data to the list.

   @param varlist the variant list
   @param data the data to add

   @return
    - true when successful.
 */
bool variant_list_addUInt16(variant_list_t* varlist, uint16_t data) {
    if(!varlist) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    variant_list_iterator_t* it = variant_list_iterator_createUInt16(data);
    if(!it) {
        return false;
    }

    return variant_list_append(varlist, it);
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Add a copy of data to a variant list

   @details
   This function adds a copy of the given data to the list.

   @param varlist the variant list
   @param data the data to add

   @return
    - true when successful.
 */
bool variant_list_addUInt32(variant_list_t* varlist, uint32_t data) {
    if(!varlist) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    variant_list_iterator_t* it = variant_list_iterator_createUInt32(data);
    if(!it) {
        return false;
    }

    return variant_list_append(varlist, it);
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Add a copy of data to a variant list

   @details
   This function adds a copy of the given data to the list.

   @param varlist the variant list
   @param data the data to add

   @return
    - true when successful.
 */
bool variant_list_addUInt64(variant_list_t* varlist, uint64_t data) {
    if(!varlist) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    variant_list_iterator_t* it = variant_list_iterator_createUInt64(data);
    if(!it) {
        return false;
    }

    return variant_list_append(varlist, it);
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Add a copy of data to a variant list

   @details
   This function adds a copy of the given data to the list.

   @param varlist the variant list
   @param data the data to add

   @return
    - true when successful.
 */
bool variant_list_addDouble(variant_list_t* varlist, double data) {
    if(!varlist) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    variant_list_iterator_t* it = variant_list_iterator_createDouble(data);
    if(!it) {
        return false;
    }

    return variant_list_append(varlist, it);
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Add a copy of data to a variant list

   @details
   This function adds a copy of the given data to the list.

   @param varlist the variant list
   @param data the data to add

   @return
    - true when successful.
 */
bool variant_list_addBool(variant_list_t* varlist, bool data) {
    if(!varlist) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    variant_list_iterator_t* it = variant_list_iterator_createBool(data);
    if(!it) {
        return false;
    }

    return variant_list_append(varlist, it);
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Add a copy of data to a variant list

   @details
   This function adds a copy of the given data to the list.

   @param varlist the variant list
   @param data the data to add

   @return
    - true when successful.
 */
bool variant_list_addDateTime(variant_list_t* varlist, const struct tm* data) {
    if(!varlist) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    variant_list_iterator_t* it = variant_list_iterator_createDateTime(data);
    if(!it) {
        return false;
    }

    return variant_list_append(varlist, it);
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Add a copy of data to a variant list

   @details
   This function adds a copy of the given data to the list.

   @param varlist the variant list
   @param data the data to add

   @return
    - true when successful.
 */
bool variant_list_addDateTimeExtended(variant_list_t* varlist, const pcb_datetime_t* data) {
    if(!varlist) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    variant_list_iterator_t* it = variant_list_iterator_createDateTimeExtended(data);
    if(!it) {
        return false;
    }

    return variant_list_append(varlist, it);
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Create a new variant list iterator and moves the data to the new variant.

   @details
   This functions creates a new variant_list_iterator and initializes the data part of this
   new iterator to a variant list and moves all items from the given list to the iterator.

   @param varlist the variant list
   @param data the data to add

   @return
    - true when successful.
 */
bool variant_list_addListMove(variant_list_t* varlist, variant_list_t* data) {
    if(!varlist) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    variant_list_iterator_t* it = variant_list_iterator_createListMove(data);
    if(!it) {
        return false;
    }

    return variant_list_append(varlist, it);
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Add a copy of data to a variant list

   @details
   This function adds a copy of the given data to the list.

   @param varlist the variant list
   @param data the data to add

   @return
    - true when successful.
 */
bool variant_list_addListCopy(variant_list_t* varlist, const variant_list_t* data) {
    if(!varlist) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    variant_list_iterator_t* it = variant_list_iterator_createListCopy(data);
    if(!it) {
        return false;
    }

    return variant_list_append(varlist, it);
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Add a reference of data to a variant list

   @details
   This function adds a reference of the given data to the list.

   @param varlist the variant list
   @param data the data to add

   @return
    - true when successful.
 */
bool variant_list_addListRef(variant_list_t* varlist, variant_list_t* data) {
    if(!varlist) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    variant_list_iterator_t* it = variant_list_iterator_createListRef(data);
    if(!it) {
        return false;
    }

    return variant_list_append(varlist, it);
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Create a new variant list iterator and moves the data to the new variant.

   @details
   This functions creates a new variant_list_iterator and initializes the data part of this
   new iterator to a variant map and moves all items from the given map to the iterator.

   @param varlist the variant list
   @param data the data to add

   @return
    - true when successful.
 */
bool variant_list_addMapMove(variant_list_t* varlist, variant_map_t* data) {
    if(!varlist) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    variant_list_iterator_t* it = variant_list_iterator_createMapMove(data);
    if(!it) {
        return false;
    }

    return variant_list_append(varlist, it);
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Add a copy of data to a variant list

   @details
   This function adds a copy of the given data to the list.

   @param varlist the variant list
   @param data the data to add

   @return
    - true when successful.
 */
bool variant_list_addMapCopy(variant_list_t* varlist, const variant_map_t* data) {
    if(!varlist) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    variant_list_iterator_t* it = variant_list_iterator_createMapCopy(data);
    if(!it) {
        return false;
    }

    return variant_list_append(varlist, it);
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Add a copy of data to a variant list

   @details
   This function adds a copy of the given data to the list.

   @param varlist the variant list
   @param data the data to add

   @return
    - true when successful.
 */
bool variant_list_addMapRef(variant_list_t* varlist, variant_map_t* data) {
    if(!varlist) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    variant_list_iterator_t* it = variant_list_iterator_createMapRef(data);
    if(!it) {
        return false;
    }

    return variant_list_append(varlist, it);
}

variant_list_iterator_t* variant_list_first(const variant_list_t* list) {
    if(!list) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    llist_iterator_t* ll_item = llist_first(list);
    if(!ll_item) {
        return NULL;
    }
    return llist_item_data(ll_item, variant_list_iterator_t, llist_it);
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Get the last variant list element.

   @details
   This function returns the last variant list element of the variant linked list. \n

   @param list the variant linked list

   @return
    - a <b>pointer</b> to the last variant list element of the variant linked list.
    - NULL if the variant list is empty.
    - NULL if the variant list parameter is NULL.
 */
variant_list_iterator_t* variant_list_last(const variant_list_t* list) {
    if(!list) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    llist_iterator_t* ll_item = llist_last(list);
    if(!ll_item) {
        return NULL;
    }
    return llist_item_data(ll_item, variant_list_iterator_t, llist_it);
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Get a pointer to the specified variant list element.

   @details
   This function returns a pointer to the variant list element at the specified index of the variant linked list. \n
   Due to the nature of a linked list the items are not garanteed to be at the same place all the time.\n
   Use this function with care, it can have performance impact, the variant list is iterated to find the correct element.\n

   @param list the linked list
   @param index the item # in which you are interested (start counting from 0)

   @return
    - a <b>pointer</b> to the specified variant list element in the variant linked list.
    - NULL if the element does not exist.
    - NULL if the list parameter is NULL.
 */
variant_list_iterator_t* variant_list_at(const variant_list_t* list, unsigned int index) {
    if(!list) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    llist_iterator_t* ll_item = llist_at(list, index);
    if(!ll_item) {
        return NULL;
    }
    return llist_item_data(ll_item, variant_list_iterator_t, llist_it);
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Get a pointer to the next variant list element.

   @details
   This function returns a pointer to the next variant list element. \n
   This function calls @ref llist_iterator_next to get the next element, @ref llist_item_data
   is used to get a pointer to the variant list iterator\n

   @param it the list element

   @return
    - a <b>pointer</b> to the next variant list element in the linked list.
    - NULL if the it parameter is NULL.
    - NULL if the end of the variant list was reached
 */
variant_list_iterator_t* variant_list_iterator_next(const variant_list_iterator_t* it) {
    if(!it) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    llist_iterator_t* ll_item = llist_iterator_next(&it->llist_it);
    if(!ll_item) {
        return NULL;
    }
    return llist_item_data(ll_item, variant_list_iterator_t, llist_it);
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Get a pointer to the previous variant list element.

   @details
   This function returns a pointer to the previous variant list element.\n

   This function calls @ref llist_iterator_prev to get the previous element, @ref llist_item_data
   is used to get a pointer to the variant list iterator\n

   @param it the list element

   @return
    - a <b>pointer</b> to the previous variant list element in the linked list.
    - NULL if the it parameter is NULL.
    - NULL if the beginning of the variant list was reached
 */
variant_list_iterator_t* variant_list_iterator_prev(const variant_list_iterator_t* it) {
    if(!it) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    llist_iterator_t* ll_item = llist_iterator_prev(&it->llist_it);
    if(!ll_item) {
        return NULL;
    }
    return llist_item_data(ll_item, variant_list_iterator_t, llist_it);
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Append the element to a variant list.

   @details
   This function appends the specified variant list element to an existing variant list. If the element was part of
   another variant list then the element will be automatically removed from that variant list.\n
   This function calls @ref llist_append\n

   @param list the variant list to which the element has to be added
   @param insert the variant list element that has to be attached to the variant list

   @return
    - true: The list element was added succesfully appenced to the list
    - false: An error has occurred
        - list or insert is a NULL pointer
 */
bool variant_list_append(variant_list_t* list, variant_list_iterator_t* insert) {
    if(!list || !insert) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    return llist_append(list, &insert->llist_it);
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Prepend the element to a variant list.

   @details
   This function prepends the specified variant list element to an existing variant list. If the element was part of
   another variant list then the element will be automatically removed from that variant list.\n
   This function calls @ref llist_prepend\n

   @param list the variant list to which the element has to be added
   @param insert the variant list element that has to be attached to the variant list

   @return
    - true: The list element was added succesfully prepended to the list
    - false: An error has occurred
        - list or insert is a NULL pointer
 */
bool variant_list_prepend(variant_list_t* list, variant_list_iterator_t* insert) {
    if(!list || !insert) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    return llist_prepend(list, &insert->llist_it);
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Attach the element to a variant list at a certain index.

   @details
   This function attaches the specified variant list element to an existing variant list. If the element is part of
   another variant list then the element will be automatically removed from that variant list.\n
   This function calls @ref llist_insertAt\n

   @param list the variant list to which the element has to be added
   @param index the item # where to add the element to the list (start counting from 0)
   @param insert the variant list element that has to be attached to the variant list

   @return
    - true: The variant list element was succesfully inserted into the variant list
    - false: An error has occurred
        - list or insert is a NULL pointer
        - index element does not exist
        - the insert element is the head or tail element of a list
 */
bool variant_list_insertAt(variant_list_t* list, unsigned int index, variant_list_iterator_t* insert) {
    if(!list || !insert) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    return llist_insertAt(list, index, &insert->llist_it);
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Detach the element from a variant list.

   @details
   This function detaches the specified variant list element from an existing variant list.\n
   This function calls @ref llist_iterator_take to remove the item from the linked list,
   @ref llist_item_data is used to get a pointer to the variant list iterator.\n

   @param it the variant list element that has to be detached from the variant list

   @return
    - a <b>pointer</b> to the detached variant list element.
    - NULL if an error occured
        - it is NULL
 */
variant_list_iterator_t* variant_list_iterator_take(variant_list_iterator_t* it) {
    if(!it) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    llist_iterator_t* ll_item = llist_iterator_take(&it->llist_it);
    return llist_item_data(ll_item, variant_list_iterator_t, llist_it);
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Detach the first element from a variant list.

   @details
   This function detaches the first variant list element from an existing variant list.\n
   This function calls @ref llist_takeFirst to get the first list element and @ref
   llist_item_data to get a pointer to the variant list iterator.\n

   @param list the list from which the first element has to be detached

   @return
    - a <b>pointer</b> to the detached variant list element.
    - NULL if an error occured
        - list is NULL
        - the list is empty
 */
variant_list_iterator_t* variant_list_takeFirst(variant_list_t* list) {
    if(!list) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    llist_iterator_t* ll_item = llist_takeFirst(list);
    if(!ll_item) {
        return NULL;
    }
    return llist_item_data(ll_item, variant_list_iterator_t, llist_it);
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Detach the last element from a variant list.

   @details
   This function detaches the last variant list element from an existing variant list.\n
   This function calls @ref llist_takeLast to get the last list element and @ref
   llist_item_data to get a pointer to the variant list iterator\n

   @param list the list from which the last element has to be detached

   @return
    - a <b>pointer</b> to the detached variant list element.
    - NULL if an error occured
        - list is NULL
        - the list is empty
 */
variant_list_iterator_t* variant_list_takeLast(variant_list_t* list) {
    if(!list) {
        pcb_error = pcb_error_invalid_parameter;
        return NULL;
    }

    llist_iterator_t* ll_item = llist_takeLast(list);
    if(!ll_item) {
        return NULL;
    }
    return llist_item_data(ll_item, variant_list_iterator_t, llist_it);
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Get the size of the variant list.

   @details
   This function returns the size of the variant list.\n
   This function calls @ref llist_size\n

   @param list the list in wich you are interested

   @return
    - the number of elements in the list.
    - 0 if list is a NULL pointer.
 */
unsigned int variant_list_size(const variant_list_t* list) {
    return llist_size(list);
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Determine if the variant list is empty.

   @details
   This function checks wether the  variant list is empty or not.\n
   This function calls @ref llist_isEmpty\n

   @param list the list in wich you are interested

   @return
    - true:
        - The list is empty
        - list == NULL
    - false: The list is not empty

 */
bool variant_list_isEmpty(const variant_list_t* list) {
    return llist_isEmpty(list);
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Determine if the variant list contains a certain variant.

   @details
   This function checks wether the variant list contains the specified variant or not.\n
   This function calls @ref variant_list_for_each and compares the given variant using the
   @ref variant_compare functionality with all list items.\n

   @param list the list in wich you are interested
   @param data the variant you want to find

   @return
    - true:
        - The element is part of the list
    - false: pcb_error is set to the correct error code
        - The list pointer or data pointer is invalid
        - The element is not found,
 */
bool variant_list_contains(const variant_list_t* list, const variant_t* data) {
    if(!list || !data) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    int32_t result = 0;
    variant_list_iterator_t* it = NULL;
    variant_list_for_each(it, list) {
        if(!variant_compare(data, variant_list_iterator_data(it), &result)) {
            continue;
        }
        if(result == 0) {
            break;
        }
    }

    if(variant_list_iterator_data(it)) {
        return true;
    } else {
        pcb_error = pcb_error_element_not_found;
        return false;
    }
}

/**
   @ingroup pcb_utils_variant_list
   @brief
   Compare two variant lists.

   @details
   This function compares two variant lists.\n

   First it does a comparison on pointer level:\n
   @li both variant lists are null pointers: result = 0
   @li only first variant list is null pointer: result = -1
   @li only second variant list is null pointer: result = 1

   Then it compares the sizes of the 2 variant lists:\n
   @li if the first variant list has more items than the second one: result > 0
   @li if the first variant list has less items than the second one: result < 0
   @li if they have the same size, the process continues by comparing individual list items.

   If all items of the first variant list equal the respective item of the second variant list, the lists are considered to be equal and the result = 0. Otherwise the result is set to the result of comparing the first non-equal items.

   @param list1 variant list 1
   @param list2 variant list 2
   @param result the result of the comparison

   @return
    - true: the comparison was a succes, result contains the resulting value
    - false: An error has occurred, pcb_error contains the reason why the set failed
        - an invalid result pointer was provided
        - comparison between individual list items failed
 */
bool variant_list_compare(const variant_list_t* list1, const variant_list_t* list2, int* result) {
    if(!result) {
        pcb_error = pcb_error_invalid_parameter;
        return false;
    }

    if(!list1 && !list2) {
        *result = 0;
        return true;
    }
    if(!list1) {
        *result = -1;
        return true;
    }
    if(!list2) {
        *result = 1;
        return true;
    }

    *result = variant_list_size(list1) - variant_list_size(list2);
    if(*result) {
        return true;
    }

    variant_list_iterator_t* it1, * it2;
    for(it1 = variant_list_first(list1), it2 = variant_list_first(list2); it1 && it2;
        it1 = variant_list_iterator_next(it1), it2 = variant_list_iterator_next(it2)) {
        if(!variant_compare(variant_list_iterator_data(it1), variant_list_iterator_data(it2), result)) {
            return false;
        }
        if(*result) {
            return true;
        }
    }
    return true;
}
