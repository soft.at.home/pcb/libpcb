#!/bin/bash

export USER="pcbapp"
create_user_env.sh
groupadd pcbapp > /dev/null 2>&1
GROUPID=$(getent group pcbapp | awk -F: '{printf "%d", $3}')
useradd -g $GROUPID -s /bin/bash pcbapp  > /dev/null 2>&1
/usr/local/bin/config.sh $USER
usermod -a -G dialout $USER
echo -e "${GREEN}sahnte setup done${NC}"
su $USER
