#!/bin/bash
PROJECT_DIR=$(cd $(dirname $0)/..; pwd)

echo "BE SURE TO RUN THIS INSIDE A SAHNTE CONTAINER"

COMPONENT_OPENSSL_DIR=${COMPONENT_OPENSSL_DIR-gen_1.0.2k}
COMPONENT_DEPS_DIR=${COMPONENT_DEPS_DIR-master}
COMPONENT_DEPS=${COMPONENT_DEPS-"$COMPONENT_OPENSSL_DIR/sah-lib-openssl-dev $COMPONENT_DEPS_DIR/sah-lib-usermngt-dev"}

# sed -n -e "/COMPONENT_DEPS/,/include:/p" $PROJECT_DIR/.gitlab-ci.yml
install_packages.sh /tmp $COMPONENT_DEPS