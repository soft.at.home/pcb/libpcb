/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <sahtest/sahtest.h>
#include <pcb/utils/variant_map.h>

static variant_map_t map;
static variant_map_t emptymap;
static variant_map_iterator_t* it[5];
static variant_map_iterator_t* orphan;

void variant_map_remove_setup() {
    int i;
    variant_map_initialize(&map);
    variant_map_initialize(&emptymap);
    for(i = 0; i < 5; i++) {
        it[i] = variant_map_iterator_create("test", NULL);
        variant_map_append(&map, it[i]);
    }
    orphan = variant_map_iterator_create("test", NULL);
}

void variant_map_remove_teardown() {
    int i;
    variant_map_iterator_destroy(orphan);

    for(i = 0; i < 5; i++) {
        variant_map_iterator_destroy(it[i]);
    }

    variant_map_cleanup(&map);
    variant_map_cleanup(&emptymap);
}

int variant_map_remove_take_first_null() {
    T_VERIFY(variant_map_takeFirst(NULL) == NULL);
    return 0;
}

int variant_map_remove_take_last_null() {
    T_VERIFY(variant_map_takeLast(NULL) == NULL);
    return 0;
}

int variant_map_remove_take_first_empty_map() {
    T_VERIFY(variant_map_takeFirst(&emptymap) == NULL);

    return 0;
}

int variant_map_remove_take_last_empty_map() {
    T_VERIFY(variant_map_takeLast(&emptymap) == NULL);
    return 0;
}

int variant_map_remove_take_first() {
    variant_map_iterator_t* taken = variant_map_takeFirst(&map);
    T_VERIFY(taken == it[0]);
    T_VERIFY(variant_map_iterator_next(taken) == NULL);
    T_VERIFY(variant_map_iterator_prev(taken) == NULL);
    T_VERIFY(variant_map_first(&map) == it[1]);

    return 0;
}

int variant_map_remove_take_last() {
    variant_map_iterator_t* taken = variant_map_takeLast(&map);
    T_VERIFY(taken == it[4]);
    T_VERIFY(variant_map_iterator_next(taken) == NULL);
    T_VERIFY(variant_map_iterator_prev(taken) == NULL);
    T_VERIFY(variant_map_last(&map) == it[3]);

    return 0;
}

int variant_map_remove_take_null() {
    T_VERIFY(variant_map_iterator_take(NULL) == NULL);

    return 0;
}
