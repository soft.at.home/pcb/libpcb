/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <sahtest/sahtest.h>
#include <pcb/utils/variant_list.h>
#include <pcb/utils/variant_map.h>

static variant_map_t list;
static variant_t var;

void variant_map_add_setup() {
    variant_map_initialize(&list);
    variant_initialize(&var, variant_type_unknown);
    variant_setInt32(&var, 1024);
}

void variant_map_add_teardown() {
    variant_map_cleanup(&list);
}

int variant_map_add_variant() {
    assert_bool(variant_map_add(&list, "test", &var) == true);
    assert_bool(variant_map_isEmpty(&list) == false);
    assert_bool(variant_map_size(&list) == 1);

    variant_map_iterator_t* it = variant_map_first(&list);
    assert_bool(it != NULL);
    assert_bool(variant_type(variant_map_iterator_data(it)) == variant_type_int32);

    assert_bool(variant_map_add(&list, NULL, &var) == false);
    assert_bool(variant_map_add(NULL, "test", &var) == false);

    sahtest_success();
}

int variant_map_add_char() {
    assert_bool(variant_map_addChar(&list, "test", "Hallo World") == true);
    assert_bool(variant_map_isEmpty(&list) == false);
    assert_bool(variant_map_size(&list) == 1);

    variant_map_iterator_t* it = variant_map_first(&list);
    assert_bool(it != NULL);
    assert_bool(variant_type(variant_map_iterator_data(it)) == variant_type_string);

    assert_bool(variant_map_addChar(&list, NULL, "Hallo World") == false);
    assert_bool(variant_map_addChar(NULL, "test", "Hallo World") == false);

    sahtest_success();
}

int variant_map_add_string() {
    string_t txt;
    string_initialize(&txt, 0);
    string_fromChar(&txt, "Hallo World");

    assert_bool(variant_map_addString(&list, "test", &txt) == true);
    assert_bool(variant_map_isEmpty(&list) == false);
    assert_bool(variant_map_size(&list) == 1);

    variant_map_iterator_t* it = variant_map_first(&list);
    assert_bool(it != NULL);
    assert_bool(variant_type(variant_map_iterator_data(it)) == variant_type_string);

    assert_bool(variant_map_addString(&list, NULL, &txt) == false);
    assert_bool(variant_map_addString(NULL, "test", &txt) == false);

    string_cleanup(&txt);
    sahtest_success();
}

int variant_map_add_int8() {
    assert_bool(variant_map_addInt8(&list, "test", 127) == true);
    assert_bool(variant_map_isEmpty(&list) == false);
    assert_bool(variant_map_size(&list) == 1);

    variant_map_iterator_t* it = variant_map_first(&list);
    assert_bool(it != NULL);
    assert_bool(variant_type(variant_map_iterator_data(it)) == variant_type_int8);

    assert_bool(variant_map_addInt8(&list, NULL, 127) == false);
    assert_bool(variant_map_addInt8(NULL, "test", 127) == false);

    sahtest_success();
}

int variant_map_add_int16() {
    assert_bool(variant_map_addInt16(&list, "test", 127) == true);
    assert_bool(variant_map_isEmpty(&list) == false);
    assert_bool(variant_map_size(&list) == 1);

    variant_map_iterator_t* it = variant_map_first(&list);
    assert_bool(it != NULL);
    assert_bool(variant_type(variant_map_iterator_data(it)) == variant_type_int16);

    assert_bool(variant_map_addInt16(&list, NULL, 127) == false);
    assert_bool(variant_map_addInt16(NULL, "test", 127) == false);

    sahtest_success();
}

int variant_map_add_int32() {
    assert_bool(variant_map_addInt32(&list, "test", 127) == true);
    assert_bool(variant_map_isEmpty(&list) == false);
    assert_bool(variant_map_size(&list) == 1);

    variant_map_iterator_t* it = variant_map_first(&list);
    assert_bool(it != NULL);
    assert_bool(variant_type(variant_map_iterator_data(it)) == variant_type_int32);

    assert_bool(variant_map_addInt32(&list, NULL, 127) == false);
    assert_bool(variant_map_addInt32(NULL, "test", 127) == false);

    sahtest_success();
}

int variant_map_add_int64() {
    assert_bool(variant_map_addInt64(&list, "test", 127) == true);
    assert_bool(variant_map_isEmpty(&list) == false);
    assert_bool(variant_map_size(&list) == 1);

    variant_map_iterator_t* it = variant_map_first(&list);
    assert_bool(it != NULL);
    assert_bool(variant_type(variant_map_iterator_data(it)) == variant_type_int64);

    assert_bool(variant_map_addInt64(&list, NULL, 127) == false);
    assert_bool(variant_map_addInt64(NULL, "test", 127) == false);

    sahtest_success();
}

int variant_map_add_uint8() {
    assert_bool(variant_map_addUInt8(&list, "test", 127) == true);
    assert_bool(variant_map_isEmpty(&list) == false);
    assert_bool(variant_map_size(&list) == 1);

    variant_map_iterator_t* it = variant_map_first(&list);
    assert_bool(it != NULL);
    assert_bool(variant_type(variant_map_iterator_data(it)) == variant_type_uint8);

    assert_bool(variant_map_addUInt8(&list, NULL, 127) == false);
    assert_bool(variant_map_addUInt8(NULL, "test", 127) == false);

    sahtest_success();
}

int variant_map_add_uint16() {
    assert_bool(variant_map_addUInt16(&list, "test", 127) == true);
    assert_bool(variant_map_isEmpty(&list) == false);
    assert_bool(variant_map_size(&list) == 1);

    variant_map_iterator_t* it = variant_map_first(&list);
    assert_bool(it != NULL);
    assert_bool(variant_type(variant_map_iterator_data(it)) == variant_type_uint16);

    assert_bool(variant_map_addUInt16(&list, NULL, 127) == false);
    assert_bool(variant_map_addUInt16(NULL, "test", 127) == false);

    sahtest_success();
}

int variant_map_add_uint32() {
    assert_bool(variant_map_addUInt32(&list, "test", 127) == true);
    assert_bool(variant_map_isEmpty(&list) == false);
    assert_bool(variant_map_size(&list) == 1);

    variant_map_iterator_t* it = variant_map_first(&list);
    assert_bool(it != NULL);
    assert_bool(variant_type(variant_map_iterator_data(it)) == variant_type_uint32);

    assert_bool(variant_map_addUInt32(&list, NULL, 127) == false);
    assert_bool(variant_map_addUInt32(NULL, "test", 127) == false);

    sahtest_success();
}

int variant_map_add_uint64() {
    assert_bool(variant_map_addUInt64(&list, "test", 127) == true);
    assert_bool(variant_map_isEmpty(&list) == false);
    assert_bool(variant_map_size(&list) == 1);

    variant_map_iterator_t* it = variant_map_first(&list);
    assert_bool(it != NULL);
    assert_bool(variant_type(variant_map_iterator_data(it)) == variant_type_uint64);

    assert_bool(variant_map_addUInt64(&list, NULL, 127) == false);
    assert_bool(variant_map_addUInt64(NULL, "test", 127) == false);

    sahtest_success();
}

int variant_map_add_bool() {
    assert_bool(variant_map_addBool(&list, "test", true) == true);
    assert_bool(variant_map_isEmpty(&list) == false);
    assert_bool(variant_map_size(&list) == 1);

    variant_map_iterator_t* it = variant_map_first(&list);
    assert_bool(it != NULL);
    assert_bool(variant_type(variant_map_iterator_data(it)) == variant_type_bool);

    assert_bool(variant_map_addBool(&list, NULL, true) == false);
    assert_bool(variant_map_addBool(NULL, "test", true) == false);

    sahtest_success();
}

int variant_map_add_double() {
    assert_bool(variant_map_addDouble(&list, "test", 125.125) == true);
    assert_bool(variant_map_isEmpty(&list) == false);
    assert_bool(variant_map_size(&list) == 1);

    variant_map_iterator_t* it = variant_map_first(&list);
    assert_bool(it != NULL);
    assert_bool(variant_type(variant_map_iterator_data(it)) == variant_type_double);

    assert_bool(variant_map_addDouble(&list, NULL, 125.125) == false);
    assert_bool(variant_map_addDouble(NULL, "test", 125.125) == false);

    sahtest_success();
}

int variant_map_add_date_time() {
    time_t now;
    time(&now);
    struct tm* t = gmtime(&now);

    assert_bool(variant_map_addDateTime(&list, "test", t) == true);
    assert_bool(variant_map_isEmpty(&list) == false);
    assert_bool(variant_map_size(&list) == 1);

    variant_map_iterator_t* it = variant_map_first(&list);
    assert_bool(it != NULL);
    assert_bool(variant_type(variant_map_iterator_data(it)) == variant_type_date_time);

    assert_bool(variant_map_addDateTime(&list, NULL, t) == false);
    assert_bool(variant_map_addDateTime(NULL, "test", t) == false);

    sahtest_success();
}

int variant_map_add_list_move() {
    variant_list_t l;
    assert_bool(variant_list_initialize(&l) == true);
    variant_list_addUInt32(&l, 1);
    variant_list_addUInt32(&l, 2);

    assert_bool(variant_map_addListMove(&list, "test", &l) == true);
    assert_bool(variant_map_isEmpty(&list) == false);
    assert_bool(variant_map_size(&list) == 1);

    variant_map_iterator_t* it = variant_map_first(&list);
    assert_bool(it != NULL);
    assert_bool(variant_type(variant_map_iterator_data(it)) == variant_type_array);

    assert_bool(variant_map_addListMove(&list, NULL, &l) == false);
    assert_bool(variant_map_addListMove(NULL, "test", &l) == false);

    variant_list_cleanup(&l);

    sahtest_success();
}

int variant_map_add_list_copy() {
    variant_list_t l;
    assert_bool(variant_list_initialize(&l) == true);
    variant_list_addUInt32(&l, 1);
    variant_list_addUInt32(&l, 2);

    assert_bool(variant_map_addListCopy(&list, "test", &l) == true);
    assert_bool(variant_map_isEmpty(&list) == false);
    assert_bool(variant_map_size(&list) == 1);

    variant_map_iterator_t* it = variant_map_first(&list);
    assert_bool(it != NULL);
    assert_bool(variant_type(variant_map_iterator_data(it)) == variant_type_array);

    assert_bool(variant_map_addListCopy(&list, NULL, &l) == false);
    assert_bool(variant_map_addListCopy(NULL, "test", &l) == false);

    variant_list_cleanup(&l);

    sahtest_success();
}

int variant_map_add_map_move() {
    variant_map_t l;
    assert_bool(variant_map_initialize(&l) == true);
    variant_map_addUInt32(&l, "1", 1);
    variant_map_addUInt32(&l, "2", 2);

    assert_bool(variant_map_addMapMove(&list, "test", &l) == true);
    assert_bool(variant_map_isEmpty(&list) == false);
    assert_bool(variant_map_size(&list) == 1);

    variant_map_iterator_t* it = variant_map_first(&list);
    assert_bool(it != NULL);
    assert_bool(variant_type(variant_map_iterator_data(it)) == variant_type_map);

    assert_bool(variant_map_addMapMove(&list, NULL, &l) == false);
    assert_bool(variant_map_addMapMove(NULL, "test", &l) == false);

    variant_map_cleanup(&l);

    sahtest_success();
}

int variant_map_add_map_copy() {
    variant_map_t l;
    assert_bool(variant_map_initialize(&l) == true);
    variant_map_addUInt32(&l, "1", 1);
    variant_map_addUInt32(&l, "2", 2);

    assert_bool(variant_map_addMapCopy(&list, "test", &l) == true);
    assert_bool(variant_map_isEmpty(&list) == false);
    assert_bool(variant_map_size(&list) == 1);

    variant_map_iterator_t* it = variant_map_first(&list);
    assert_bool(it != NULL);
    assert_bool(variant_type(variant_map_iterator_data(it)) == variant_type_map);

    assert_bool(variant_map_addListCopy(&list, NULL, &l) == false);
    assert_bool(variant_map_addListCopy(NULL, "test", &l) == false);

    variant_map_cleanup(&l);

    sahtest_success();
}
