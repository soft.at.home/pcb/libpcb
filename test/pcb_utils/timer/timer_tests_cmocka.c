/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "timer_tests_cmocka.h"
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>

struct testgroup {
    const char* name;
    const struct CMUnitTest* tests;
    size_t numTests;
    CMFixtureFunction setup;
    CMFixtureFunction teardown;
};

static struct testgroup* testGroups = NULL;
static size_t numTestGroups = 0;

void register_test_group(const char* name, const struct CMUnitTest* tests, size_t numTests, CMFixtureFunction setup, CMFixtureFunction teardown) {
    testGroups = realloc(testGroups, sizeof(struct testgroup) * (numTestGroups + 1));
    if(!testGroups) {
        perror("Allocation failed!");
        return;
    }
    struct testgroup newgroup = {
        .name = name,
        .tests = tests,
        .numTests = numTests,
        .setup = setup,
        .teardown = teardown
    };
    testGroups[numTestGroups] = newgroup;
    ++numTestGroups;
}

int main(int argc, char** argv) {
    int i = 0;
    int numFailed = 0;
    for(; i < numTestGroups; ++i) {
        int failed = _cmocka_run_group_tests(testGroups[i].name, testGroups[i].tests, testGroups[i].numTests, testGroups[i].setup, testGroups[i].teardown);
        if(failed != 0) {
            print_error("Test group %s failed with %d test failures\n", testGroups[i].name, failed);
        }
        numFailed += failed;
    }
    free(testGroups);
    /* https://www.gnu.org/software/libc/manual/html_node/Exit-Status.html :
       /*
     * Warning: Don’t try to use the number of errors as the exit status.
     * This is actually not very useful; a parent process would generally not care how many errors occurred.
     * Worse than that, it does not work, because the status value is truncated to eight bits.
     * Thus, if the program tried to report 256 errors, the parent would receive a report of 0 errors—that is, success.
     *
     * => It seems we use the exit value this way anyway, but we can at least limit it to at most 255.
     */
    return numFailed > 255 ? 255 : numFailed;
}
