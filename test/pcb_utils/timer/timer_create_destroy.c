/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "timer_tests_cmocka.h"
#include <unistd.h>
#include <signal.h>
#include <stdio.h>
#include <pcb/utils/timer.h>

static void signalHandler(int signal) {
    (void) signal;
}

static int setup(void** state) {
    pcb_timer_initializeTimers();
    signal(SIGALRM, signalHandler);
    return 0;
}

static int teardown(void** state) {
    pcb_timer_cleanupTimers();
    signal(SIGALRM, SIG_DFL);
    return 0;
}

static void timer_create_destroy(void** state) {
    assert_true(pcb_timer_initializeTimers() == true);

    // create
    pcb_timer_t* timer = pcb_timer_create();
    assert_non_null(timer);
    assert_true(pcb_timer_getState(timer) == pcb_timer_off);

    assert_true(pcb_timer_start(timer, 5000));
    assert_true(pcb_timer_getState(timer) == pcb_timer_started);

    // destroy
    pcb_timer_destroy(timer);
    assert_true(!pcb_timer_start(timer, 5000));
    assert_true(pcb_timer_getState(timer) == pcb_timer_off);

    pcb_timer_cleanupTimers();
}

static void timer_destroy_null_timer(void** state) {
    // should not segfault - segfaults will be captured
    pcb_timer_destroy(NULL);
}

static void timer_functions_fail_on_destroyed_timer(void** state) {
    assert_true(pcb_timer_initializeTimers() == true);

    // create
    pcb_timer_t* timer = pcb_timer_create();
    assert_non_null(timer);
    assert_true(pcb_timer_setUserData(timer, (void*) 5));

    // destroy
    pcb_timer_destroy(timer);
    assert_true(!pcb_timer_start(timer, 5000));
    assert_true(!pcb_timer_stop(timer));
    assert_true(pcb_timer_remainingTime(timer) == 0);
    assert_true(!pcb_timer_setUserData(timer, (void*) 1));
    assert_true(pcb_timer_getUserData(timer) == (void*) 5);
    assert_true(!pcb_timer_setHandler(timer, NULL));
    assert_true(!pcb_timer_setInterval(timer, 5000));

    pcb_timer_cleanupTimers();
}

static void timer_check_timers_on_destroyed_timer(void** state) {
    assert_true(pcb_timer_initializeTimers() == true);

    // create
    pcb_timer_t* timer = pcb_timer_create();
    assert_non_null(timer);
    assert_true(pcb_timer_setUserData(timer, (void*) 5));

    // destroy
    pcb_timer_destroy(timer);
    pcb_timer_checkTimers();

    pcb_timer_cleanupTimers();
}

static void deferred_set_true(void* userdata) {
    bool* boolean = userdata;

    *boolean = true;
}

static void timer_destroy_finished_deferred_timer(void** state) {
    assert_true(pcb_timer_initializeTimers() == true);

    // Given a timer for a deferred execution, which has reached its timeout.
    bool callbackCalled = false;
    bool ok = pcb_timer_defer(deferred_set_true, &callbackCalled, 2);
    assert_true(ok);
    while(!callbackCalled) {
        pcb_timer_calculateTimers();
        pcb_timer_checkTimers();
    }

    // When destroying it (if it was not already)
    pcb_timer_cleanupTimers();

    // Then valgrind did not detect a memory leak.
    // Also, then the deferred execution was executed.
    assert_true(callbackCalled);
}

static void timer_destroy_unfinished_deferred_timer(void** state) {
    assert_true(pcb_timer_initializeTimers() == true);

    // Given a timer for a deferred execution, which has not reached its timeout yet.
    bool callbackCalled = false;
    bool ok = pcb_timer_defer(deferred_set_true, &callbackCalled, 3000);
    assert_true(ok);
    pcb_timer_checkTimers();

    // When destroying it
    pcb_timer_cleanupTimers();

    // Then valgrind did not detect a memory leak.
    // Also, then the deferred execution was never executed.
    assert_false(callbackCalled);
}

static void deferred_set_true_with_timer(pcb_timer_t* timer, void* userdata) {
    bool* boolean = userdata;

    *boolean = true;
}

static void timer_createAndStart_normalCase(void** state) {
    assert_true(pcb_timer_initializeTimers() == true);

    // GIVEN a timer pointer.
    bool callbackCalled = false;
    pcb_timer_t* timer = NULL;

    // WHEN creating and starting a timer
    bool ok = pcb_timer_createAndStart(&timer, deferred_set_true_with_timer, &callbackCalled, 0);

    // THEN it was created successfully and after a while runs the callback.
    assert_true(ok);
    assert_non_null(timer);
    while(!callbackCalled) {
        pcb_timer_calculateTimers();
        pcb_timer_checkTimers();
    }

    callbackCalled = false;
    pcb_timer_cleanupTimers();
    assert_false(callbackCalled);
}

static void timer_createAndStart_destroyBeforeUse(void** state) {
    assert_true(pcb_timer_initializeTimers() == true);

    // GIVEN a created and started timer
    bool callbackCalled = false;
    pcb_timer_t* timer = NULL;
    bool ok = pcb_timer_createAndStart(&timer, &deferred_set_true_with_timer, &callbackCalled, 200);
    assert_true(ok);
    assert_non_null(timer);

    // WHEN destroying that specific timer before it reaches its timeout.
    int i = 0;
    for(i = 0; i < 10; i++) {
        pcb_timer_calculateTimers();
        pcb_timer_checkTimers();
    }
    pcb_timer_destroy(timer);
    sleep(1);
    for(i = 0; i < 10; i++) {
        pcb_timer_calculateTimers();
        pcb_timer_checkTimers();
    }

    // THEN the callback was not called.
    assert_false(callbackCalled);
}

static const struct CMUnitTest tests[] = {
    cmocka_unit_test(timer_create_destroy),
    cmocka_unit_test(timer_destroy_null_timer),
    cmocka_unit_test(timer_functions_fail_on_destroyed_timer),
    cmocka_unit_test(timer_check_timers_on_destroyed_timer),
    cmocka_unit_test(timer_destroy_finished_deferred_timer),
    cmocka_unit_test(timer_destroy_unfinished_deferred_timer),
    cmocka_unit_test(timer_createAndStart_normalCase),
    cmocka_unit_test(timer_createAndStart_destroyBeforeUse),
};

__attribute__((constructor)) void timer_create_destroy_register(void) {
    register_test_group("timer_create_destroy", tests, ARRAY_SIZE(tests), setup, teardown);
}


