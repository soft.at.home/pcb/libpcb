/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <sahtest/sahtest.h>
#include <pcb/utils/string_list.h>

int string_list_init_cleanup() {
    string_list_t list;
    T_VERIFY(string_list_initialize(&list));
    T_VERIFY(!string_list_first(&list));
    T_VERIFY(!string_list_last(&list));
    string_list_cleanup(&list);

    return 0;
}

int string_list_init_null() {
    T_VERIFY(!string_list_initialize(NULL));

    return 0;
}

int string_list_cleanup_null() {
    string_list_cleanup(NULL);

    return 0;
}

int string_list_create_iterator() {
    string_t txt;
    string_initialize(&txt, 0);
    string_fromChar(&txt, "Hello World");

    string_list_iterator_t* it = NULL;
    it = string_list_iterator_create(&txt);
    T_VERIFY(it != NULL);
    T_VERIFY(!string_list_iterator_prev(it));
    T_VERIFY(!string_list_iterator_next(it));

    string_list_iterator_destroy(it);
    string_cleanup(&txt);
    return 0;
}

int string_list_create_iterator_null() {
    string_list_iterator_t* it = NULL;
    it = string_list_iterator_create(NULL);

    T_VERIFY(it != NULL);
    T_VERIFY(!string_list_iterator_prev(it));
    T_VERIFY(!string_list_iterator_next(it));

    string_list_iterator_destroy(it);
    return 0;
}

int string_list_create_iterator_char() {
    string_list_iterator_t* it = NULL;
    it = string_list_iterator_createFromChar("Hello World");
    T_VERIFY(it != NULL);
    T_VERIFY(!string_list_iterator_prev(it));
    T_VERIFY(!string_list_iterator_next(it));

    string_list_iterator_destroy(it);
    return 0;
}

int string_list_create_iterator_char_null() {
    string_list_iterator_t* it = NULL;
    it = string_list_iterator_createFromChar(NULL);

    T_VERIFY(it != NULL);
    T_VERIFY(!string_list_iterator_prev(it));
    T_VERIFY(!string_list_iterator_next(it));

    string_list_iterator_destroy(it);
    return 0;
}

int string_list_init_fill_cleanup() {
    string_list_t list;
    string_list_iterator_t* it;
    int i;
    T_VERIFY(string_list_initialize(&list));
    for(i = 0; i < 5; i++) {
        it = string_list_iterator_createFromChar("text");
        T_VERIFY(it != NULL);
        T_VERIFY(string_list_append(&list, it));
    }
    T_VERIFY(string_list_size(&list) == 5);
    string_list_cleanup(&list);
    T_VERIFY(string_list_size(&list) == 0);

    return 0;
}

int string_list_iterator_destroy_null() {
    string_list_iterator_destroy(NULL);

    return 0;
}
