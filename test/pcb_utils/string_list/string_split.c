/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <sahtest/sahtest.h>
#include <pcb/utils/string_list.h>

int string_list_split_empty_string() {
    string_t text;
    string_initialize(&text, 64);

    string_list_t list;
    T_VERIFY(string_list_initialize(&list));

    T_VERIFY(string_list_split(&list, &text, ",", strlist_keep_empty_parts, string_case_sensitive) == 0);

    string_list_cleanup(&list);
    string_cleanup(&text);

    return 0;
}

int string_list_split_null_string() {
    string_list_t list;
    T_VERIFY(string_list_initialize(&list));

    T_VERIFY(string_list_split(&list, NULL, ",", strlist_keep_empty_parts, string_case_sensitive) == 0);

    return 0;
}

int string_list_split_null_list() {
    string_t text;
    string_initialize(&text, 64);
    string_fromChar(&text, "HelloSepWorldSep!");

    T_VERIFY(string_list_split(NULL, &text, ",", strlist_keep_empty_parts, string_case_sensitive) == 0);

    string_cleanup(&text);
    return 0;
}

int string_list_split_null_separator() {
    string_list_t list;
    T_VERIFY(string_list_initialize(&list));

    string_t text;
    string_initialize(&text, 64);
    string_fromChar(&text, "HelloSepWorldSep!");

    T_VERIFY(string_list_split(&list, &text, NULL, strlist_keep_empty_parts, string_case_sensitive) == 0);

    string_cleanup(&text);
    return 0;
}

int string_list_split_case_sensitive() {
    string_t text;
    string_initialize(&text, 64);
    string_fromChar(&text, "HelloSepWorldSep!");

    string_list_t list;
    T_VERIFY(string_list_initialize(&list));

    T_VERIFY(string_list_split(&list, &text, "Sep", strlist_keep_empty_parts, string_case_sensitive) == 3);
    string_list_iterator_t* it = string_list_first(&list);
    T_VERIFY(strcmp(string_buffer(string_list_iterator_data(it)), "Hello") == 0);
    it = string_list_iterator_next(it);
    T_VERIFY(strcmp(string_buffer(string_list_iterator_data(it)), "World") == 0);
    it = string_list_iterator_next(it);
    T_VERIFY(strcmp(string_buffer(string_list_iterator_data(it)), "!") == 0);

    string_list_cleanup(&list);
    string_cleanup(&text);

    return 0;
}

int string_list_split_case_insensitive() {
    string_t text;
    string_initialize(&text, 64);
    string_fromChar(&text, "HelloSepWorldSep!");

    string_list_t list;
    T_VERIFY(string_list_initialize(&list));

    T_VERIFY(string_list_split(&list, &text, "SEP", strlist_keep_empty_parts, string_case_insensitive) == 3);
    string_list_iterator_t* it = string_list_first(&list);
    T_VERIFY(strcmp(string_buffer(string_list_iterator_data(it)), "Hello") == 0);
    it = string_list_iterator_next(it);
    T_VERIFY(strcmp(string_buffer(string_list_iterator_data(it)), "World") == 0);
    it = string_list_iterator_next(it);
    T_VERIFY(strcmp(string_buffer(string_list_iterator_data(it)), "!") == 0);

    string_list_cleanup(&list);
    string_cleanup(&text);

    return 0;
}

int string_list_split_keep_empty() {
    string_t text;
    string_initialize(&text, 64);
    string_fromChar(&text, "HelloSepSepWorldSep!Sep");

    string_list_t list;
    T_VERIFY(string_list_initialize(&list));

    T_VERIFY(string_list_split(&list, &text, "SEP", strlist_keep_empty_parts, string_case_insensitive) == 4);
    string_list_iterator_t* it = string_list_first(&list);
    T_VERIFY(strcmp(string_buffer(string_list_iterator_data(it)), "Hello") == 0);
    it = string_list_iterator_next(it);
    T_VERIFY(strcmp(string_buffer(string_list_iterator_data(it)), "") == 0);
    it = string_list_iterator_next(it);
    T_VERIFY(strcmp(string_buffer(string_list_iterator_data(it)), "World") == 0);
    it = string_list_iterator_next(it);
    T_VERIFY(strcmp(string_buffer(string_list_iterator_data(it)), "!") == 0);

    string_list_cleanup(&list);
    string_cleanup(&text);

    return 0;
}

int string_list_split_skip_empty() {
    string_t text;
    string_initialize(&text, 64);
    string_fromChar(&text, "HelloSepSepWorldSep!Sep");

    string_list_t list;
    T_VERIFY(string_list_initialize(&list));

    T_VERIFY(string_list_split(&list, &text, "SEP", strlist_skip_empty_parts, string_case_insensitive) == 3);
    string_list_iterator_t* it = string_list_first(&list);
    T_VERIFY(strcmp(string_buffer(string_list_iterator_data(it)), "Hello") == 0);
    it = string_list_iterator_next(it);
    T_VERIFY(strcmp(string_buffer(string_list_iterator_data(it)), "World") == 0);
    it = string_list_iterator_next(it);
    T_VERIFY(strcmp(string_buffer(string_list_iterator_data(it)), "!") == 0);

    string_list_cleanup(&list);
    string_cleanup(&text);

    return 0;
}

int string_list_split_empty_char() {
    string_list_t list;
    T_VERIFY(string_list_initialize(&list));

    T_VERIFY(string_list_splitChar(&list, "", ",", strlist_keep_empty_parts, string_case_sensitive) == 0);

    string_list_cleanup(&list);

    return 0;
}

int string_list_split_null_char() {
    string_list_t list;
    T_VERIFY(string_list_initialize(&list));

    T_VERIFY(string_list_splitChar(&list, NULL, ",", strlist_keep_empty_parts, string_case_sensitive) == 0);

    string_list_cleanup(&list);

    return 0;
}

int string_list_split_char_null_list() {
    T_VERIFY(string_list_splitChar(NULL, "HelloSepWorldSep!", "Sep", strlist_keep_empty_parts, string_case_sensitive) == 0);
    return 0;
}

int string_list_split_char_null_separator() {
    string_list_t list;
    T_VERIFY(string_list_initialize(&list));

    T_VERIFY(string_list_splitChar(&list, "HelloSepWorldSep!", NULL, strlist_keep_empty_parts, string_case_sensitive) == 0);

    string_list_cleanup(&list);
    return 0;
}

int string_list_split_char() {
    string_list_t list;
    T_VERIFY(string_list_initialize(&list));

    T_VERIFY(string_list_splitChar(&list, "HelloSepWorldSep!", "Sep", strlist_keep_empty_parts, string_case_sensitive) == 3);
    string_list_iterator_t* it = string_list_first(&list);
    T_VERIFY(strcmp(string_buffer(string_list_iterator_data(it)), "Hello") == 0);
    it = string_list_iterator_next(it);
    T_VERIFY(strcmp(string_buffer(string_list_iterator_data(it)), "World") == 0);
    it = string_list_iterator_next(it);
    T_VERIFY(strcmp(string_buffer(string_list_iterator_data(it)), "!") == 0);

    string_list_cleanup(&list);

    return 0;
}
