/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef STRING_SPLIT_H
#define STRING_SPLIT_H

#include <sahtest/define.h>

int string_list_split_empty_string();
int string_list_split_null_string();
int string_list_split_null_list();
int string_list_split_null_separator();
int string_list_split_case_sensitive();
int string_list_split_case_insensitive();
int string_list_split_keep_empty();
int string_list_split_skip_empty();

int string_list_split_empty_char();
int string_list_split_null_char();
int string_list_split_char_null_list();
int string_list_split_char_null_separator();
int string_list_split_char();

sahtest_group_begin(sl_split, NULL, NULL)
sahtest_add_test(string_list_split_empty_string, "Test splitting an empty string")
sahtest_add_test(string_list_split_null_string, "Test splitting a null string")
sahtest_add_test(string_list_split_null_list, "Test splitting a string into a null list")
sahtest_add_test(string_list_split_null_separator, "Test splitting a string using a null separator")
sahtest_add_test(string_list_split_case_sensitive, "Test splitting using a case sensitive separator")
sahtest_add_test(string_list_split_case_insensitive, "Test splitting using a case insensitive separator")
sahtest_add_test(string_list_split_keep_empty, "Test splitting and keep empty parts")
sahtest_add_test(string_list_split_skip_empty, "Test splitting and skip empty parts")

sahtest_add_test(string_list_split_empty_char, "Test splitting an empty char")
sahtest_add_test(string_list_split_null_char, "Test splitting a null char")
sahtest_add_test(string_list_split_char_null_list, "Test splitting a char into a null list")
sahtest_add_test(string_list_split_char_null_separator, "Test splitting a char using a null separator")
sahtest_add_test(string_list_split_char, "Test splitting a char string")
sahtest_group_end()

#endif // STRING_SPLIT_H
