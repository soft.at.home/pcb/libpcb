/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <sahtest/sahtest.h>
#include <pcb/utils/string_list.h>

static string_list_t list;
static string_list_t emptyList;
static string_list_iterator_t* it[5];
static const char* parts[] = { "Hello", "World", "!", "Foo", "Bar" };
static string_list_iterator_t* orphan;

void string_list_insert_build_list_setup() {
    int i;
    string_list_initialize(&list);
    string_list_initialize(&emptyList);
    for(i = 0; i < 5; i++) {
        it[i] = string_list_iterator_createFromChar(parts[i]);
        string_list_append(&list, it[i]);
    }
    orphan = string_list_iterator_createFromChar("Test");
}

void string_list_insert_build_list_teardown() {
    int i;
    for(i = 0; i < 5; i++) {
        string_list_iterator_destroy(it[i]);
    }
    string_list_iterator_destroy(orphan);

    string_list_cleanup(&list);
    string_list_cleanup(&emptyList);
}

int string_list_insert_indexed_null_list() {
    T_VERIFY(!string_list_insertAt(NULL, 0, orphan));
    T_VERIFY(!string_list_insertAt(NULL, 10, orphan));

    return 0;
}

int string_list_insert_indexed_empty_list_index_0() {
    T_VERIFY(string_list_size(&emptyList) == 0);
    T_VERIFY(string_list_insertAt(&emptyList, 0, orphan));
    T_VERIFY(string_list_iterator_next(orphan) == NULL);
    T_VERIFY(string_list_iterator_prev(orphan) == NULL);
    T_VERIFY(string_list_first(&emptyList) == orphan);
    T_VERIFY(string_list_last(&emptyList) == orphan);

    return 0;
}

int string_list_insert_indexed_empty_list_index_2() {
    T_VERIFY(string_list_size(&emptyList) == 0);
    T_VERIFY(string_list_insertAt(&emptyList, 0, orphan));
    T_VERIFY(string_list_iterator_next(orphan) == NULL);
    T_VERIFY(string_list_iterator_prev(orphan) == NULL);
    T_VERIFY(string_list_first(&emptyList) == orphan);
    T_VERIFY(string_list_last(&emptyList) == orphan);

    return 0;
}

int string_list_insert_indexed_list_index_out_of_boundry() {
    unsigned int size = string_list_size(&list);
    T_VERIFY(string_list_insertAt(&list, size + 10, orphan));
    T_VERIFY(string_list_iterator_next(orphan) == NULL);
    T_VERIFY(string_list_iterator_prev(orphan) == it[4]);

    return 0;
}

int string_list_insert_indexed_list_index_0() {
    T_VERIFY(string_list_insertAt(&list, 0, orphan));
    T_VERIFY(string_list_iterator_next(orphan) == it[0]);
    T_VERIFY(string_list_iterator_prev(orphan) == NULL);

    return 0;
}

int string_list_insert_indexed_list_index_2() {
    T_VERIFY(string_list_insertAt(&list, 2, orphan));
    T_VERIFY(string_list_iterator_next(orphan) == it[2]);
    T_VERIFY(string_list_iterator_prev(orphan) == it[1]);

    return 0;
}

int string_list_insert_indexed_list_index_last() {
    unsigned int size = string_list_size(&list);
    T_VERIFY(string_list_insertAt(&list, size - 1, orphan));
    T_VERIFY(string_list_iterator_next(orphan) == it[4]);
    T_VERIFY(string_list_iterator_prev(orphan) == it[3]);

    return 0;
}
