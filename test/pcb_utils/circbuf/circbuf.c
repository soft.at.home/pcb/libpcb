/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <string.h>

#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include "pcb/utils/circular_buffer.h"

#define LOOPS 100
#define CIRCBUF_SIZE 100

static circbuf_t cb;

static int test_circular_buffer_setup() {
    if(!circbuf_initialize(&cb, CIRCBUF_SIZE)) {
        fail_msg("Failed to initialize circular buffer");
    }
    return 0;
}

static int test_circular_buffer_teardown() {
    circbuf_cleanup(&cb);
    return 0;
}

static void test_open_close(void** state) {
    (void) state;
    FILE* stream = circbuf_fopen(&cb, "w+");
    assert_non_null(stream);
    assert_int_not_equal(0, fclose(stream));
}

static void test_rw(void** state) {
    (void) state;
    FILE* stream = circbuf_fopen(&cb, "w+");
    char buffer[256];

    assert_int_equal(fwrite("123456", sizeof(char), 7, stream), 7);
    fflush(stream);
    assert_int_equal(fread(buffer, sizeof(char), 7, stream), 7);
    assert_memory_equal(buffer, "123456", 7);
    assert_int_not_equal(0, fprintf(stream, "123456"));
    fclose(stream);
}

static void test_write_off_by_one_bug(void** state) {
    (void) state;
    char out[1024];
    FILE* stream = circbuf_fopen(&cb, "w+");

    //After opening the stream, the EOF is set.
    assert_int_equal(feof(stream), 1);

    //No bytes should be available for reading.
    assert_int_equal(circbuf_availableForRead(&cb), 0);

    clearerr(stream);
    assert_int_equal(feof(stream), 0);

    char* str = (char*) "123";
    fwrite(str, sizeof(char), 3, stream);
    fflush(stream);

    fwrite(str, sizeof(char), 3, stream);
    fflush(stream);

    ssize_t size = circbuf_availableForRead(&cb);

    assert_int_equal(size, 6);
    strncpy(out, (char*) circbuf_readBuffer(&cb), 6);
    circbuf_markAsRead(&cb, 6);
    assert_memory_equal(out, "123123", 6);

    fclose(stream);
}

static void test_reset(void** state) {
    (void) state;
    FILE* stream = circbuf_fopen(&cb, "w+");

    assert_int_equal(fwrite("123456", sizeof(char), 7, stream), 7);
    fflush(stream);
    circbuf_reset(&cb, CIRCBUF_SIZE);
    assert_int_equal(feof(stream), 0);
    fclose(stream);
}


static void test_rw_over_boundary(void** state) {
    (void) state;
    FILE* stream = circbuf_fopen(&cb, "w+");
    char buffer[256];
    int i = 0;
    char* str[2] = { (char*) "123456", (char*) "abcdef" };
    char* test = NULL;
    size_t len;

    for(i = 0; i < LOOPS; i++) {
        test = str[i % 2];
        len = strlen(test);

        assert_int_equal(fwrite(test, sizeof(char), len, stream), len);
        fflush(stream);
        assert_int_equal(fread(buffer, sizeof(char), len, stream), len);
        assert_memory_equal(buffer, test, len);
    }
    fclose(stream);
}

static void test_rw_cornercase(void** state) {
    (void) state;
    FILE* stream = circbuf_fopen(&cb, "w+");
    char buffer[256];
    int i = 0;
    char* str[2] = { (char*) "12345", (char*) "abcde" };
    for(i = 0; i < LOOPS; i++) {
        char* test = str[i % 2];
        assert_int_equal(fwrite(test, sizeof(char), 5, stream), 5);
        fflush(stream);
        assert_int_equal(fread(buffer, sizeof(char), 5, stream), 5);
        assert_memory_equal(buffer, test, 5);
    }
    fclose(stream);
}

static void test_direct_api(void** state) {
    (void) state;
    FILE* stream = circbuf_fopen(&cb, "w+");

    assert_non_null(stream);
    assert_true(circbuf_stream(&cb) == stream);
    assert_int_equal(circbuf_size(&cb), 100);
    assert_int_equal(circbuf_availableForRead(&cb), 0);

    int i = 0;
    char* str[2] = { (char*) "123456", (char*) "abcdef" };
    ssize_t toRead = 0;
    for(i = 0; i < LOOPS; i++) {
        char* test = str[i % 2];
        assert_int_equal(fwrite(test, sizeof(char), 7, stream), 7);
        fflush(stream);
        toRead = circbuf_availableForRead(&cb);
        assert_memory_equal((char*) circbuf_readBuffer(&cb), test, toRead);
        assert_true(circbuf_markAsRead(&cb, toRead) == toRead);
        if(toRead < 7) {
            assert_memory_equal((char*) circbuf_readBuffer(&cb), test + toRead, 7 - toRead);
            assert_true(circbuf_markAsRead(&cb, 7 - toRead) == 7 - toRead);
        }
    }
    fclose(stream);
}

static void test_eof(void** state) {
    (void) state;
    char buffer[256];
    FILE* stream = circbuf_fopen(&cb, "w+");
    assert_true(feof(stream));
    assert_int_equal(fwrite("123456", sizeof(char), 7, stream), 7);
    fflush(stream);
    assert_true(!feof(stream));
    assert_int_equal(fread(buffer, sizeof(char), 10, stream), 7);
    assert_true(feof(stream));
    assert_int_equal(fread(buffer, sizeof(char), 10, stream), 0);
    assert_true(feof(stream));
    clearerr(stream);
    assert_true(!feof(stream));
    assert_int_equal(fread(buffer, sizeof(char), 10, stream), 0);
    assert_true(feof(stream));
    fclose(stream);
}

static void test_rw_increase_size(void** state) {
    (void) state;
    FILE* stream = circbuf_fopen(&cb, "w+");
    char buffer[256];
    int i = 0;
    char* str[2] = { (char*) "123456", (char*) "abcdef" };
    for(i = 0; i < LOOPS; i++) {
        char* test = str[i % 2];
        assert_int_equal(fwrite(test, sizeof(char), 7, stream), 7);
        fflush(stream);
    }
    for(i = 0; i < LOOPS; i++) {
        char* test = str[i % 2];
        assert_int_equal(fread(buffer, sizeof(char), 7, stream), 7);
        assert_memory_equal(buffer, test, 7);
    }

    fclose(stream);
}

static void test_resize_with_read_after_write(void** state) {
    (void) state;
    FILE* stream = circbuf_fopen(&cb, "w+");
    char buffer[256];

    /* fill the buffer */
    assert_int_equal(fwrite("0123456789", sizeof(char), 10, stream), 10);
    fflush(stream);
    /* read a bit */
    assert_int_equal(fread(buffer, sizeof(char), 7, stream), 7);

    /* trigger a resize */
    assert_int_equal(fwrite("0123456789", sizeof(char), 5, stream), 5);
    fflush(stream);
    assert_int_equal(fwrite("0123456789", sizeof(char), 5, stream), 5);
    fflush(stream);
    fclose(stream);
}

/*
   static void test_under_constraint(void **state) {
    (void) state;
    int limit = 5 * 1024 * 1024, oldLimit;
    int i;
    struct rlimit l;
    if (getenv("VALGRIND_OPTS") != NULL) {
        fprintf(stderr, "WARN   : PCBTestCircBuf::test_under_constraint() skipped, valgrind detected\n");
    return 0;
    }

    assert_int_equal(getrlimit(RLIMIT_AS, &l), 0);
    oldLimit = l.rlim_cur;
    l.rlim_cur = limit;
    assert_int_equal(setrlimit(RLIMIT_AS, &l), 0);

    circbuf_t cb;
    assert_true(!circbuf_initialize(&cb, limit * 2));
    assert_true(errno == ENOMEM);

    circbuf_initialize(&cb, 100);
    FILE *stream = circbuf_fopen(&cb, "w+");
    assert_true(stream);

    for (i = 0; i < limit; i++) {
        errno = 0;
        if (fwrite("0123456789", sizeof(char), 10, stream) < 10)
            break;
        errno = 0;
        fflush(stream);
        if (errno) {
            break;
        }
        assert_int_equal(errno, 0);
    }
    assert_true(errno == ENOMEM);

    fclose(stream);

    circbuf_cleanup(&cb);

    l.rlim_cur = oldLimit;
    assert_int_equal(setrlimit(RLIMIT_AS, &l), 0);
   }
 */

static void test_seek_write(void** state) {
    (void) state;
    FILE* stream = circbuf_fopen(&cb, "w+");
    char result[100];

    /* simple case */
    char* str = (char*) "foo is to be replaced by bar";
    size_t len = strlen(str);

    assert_true(fwrite(str, sizeof(char), len, stream) == len);
    assert_int_equal(fseek(stream, 0 - len, SEEK_CUR), 0);
    assert_int_equal(fwrite("bar", sizeof(char), 3, stream), 3);
    assert_int_equal(fseek(stream, len - 3, SEEK_CUR), 0);
    assert_int_equal(fflush(stream), 0);
    assert_true(fread(result, sizeof(char), len, stream) == len);
    assert_memory_equal(result, "bar is to be replaced by bar", len);

    /*  cross boundaries */
    int i;
    for(i = 0; i < LOOPS; i++) {
        assert_true(fwrite(str, sizeof(char), len, stream) == len);
        assert_int_equal(fseek(stream, 0 - len, SEEK_CUR), 0);
        assert_int_equal(fwrite("bar", sizeof(char), 3, stream), 3);
        assert_int_equal(fseek(stream, len - 3, SEEK_CUR), 0);
        assert_int_equal(fflush(stream), 0);
        assert_true(fread(result, sizeof(char), len, stream) == len);
        assert_memory_equal(result, "bar is to be replaced by bar", len);
    }

    fclose(stream);
}

static void test_seek_read(void** state) {
    (void) state;
    FILE* stream = NULL;
    char result[100] = {};

    stream = circbuf_fopen(&cb, "r+");
    /* simple case */
    char* str = "i am seeking something here";
    size_t len = strlen(str);

    /** Write string and flush */
    assert_true(fwrite(str, sizeof(char), len, stream) == len);
    fflush(stream);

    /** Read buffer. Must match str length */
    assert_true(fread(result, sizeof(char), len, stream) == len);

    /** Go back full str length */
    assert_int_equal(fseek(stream, (long) -len, SEEK_CUR), 0);

    /** Read buffer again. Must match str length */
    assert_true(fread(result, sizeof(char), len, stream) == len);
    assert_true(strncmp(result, str, len) == 0);

    //Go back 4 bytes to find 'here'.
    assert_int_equal(fseek(stream, -4, SEEK_CUR), 0);
    assert_true(fread(result, sizeof(char), 4, stream) == 4);
    assert_int_equal(strncmp(result, "here", 4), 0);

    //Go back 1 byte to find 'e'.
    assert_int_equal(fseek(stream, -1, SEEK_CUR), 0);
    assert_true(fread(result, sizeof(char), 1, stream) == 1);
    assert_int_equal(strncmp(result, "e", 1), 0);

    fclose(stream);
}

static void test_seek_read_cross_boundaries(void** state) {
    (void*) state;
    FILE* stream = NULL;
    char* ten_chars = "0123456789";
    char* cross_bound = "ten";
    size_t len = strlen(ten_chars);
    size_t cross_bound_len = strlen(cross_bound);
    char result[10] = {};

    stream = circbuf_fopen(&cb, "r+");

    //Write 10 chars
    assert_true(fwrite(ten_chars, sizeof(char), len, stream) == len);
    fflush(stream);

    //Read them back
    assert_true(fread(result, sizeof(char), len, stream) == len);

    //Go back one byte
    assert_int_equal(fseek(stream, -1, SEEK_CUR), 0);

    //Write cross boundary chars
    assert_true(fwrite(cross_bound, sizeof(char), cross_bound_len, stream) == cross_bound_len);
    fflush(stream);

    //Bytes available are "9ten"
    assert_int_equal(cb.toRead, 4);

    //Bytes available are "9ten"
    assert_int_equal(circbuf_availableForRead(&cb), 4);

    //Mark one bytes "9" as read
    assert_int_equal(circbuf_markAsRead(&cb, 1), 1);

    //Read leftover "ten"
    assert_true(fread(result, sizeof(char), circbuf_availableForRead(&cb), stream) == cross_bound_len);
    assert_int_equal(strncmp(result, cross_bound, cross_bound_len), 0);

    //Go back, crossing the boundary again.
    assert_int_equal(fseek(stream, -10, SEEK_CUR), 0);

    //Bytes available are "3456789ten"
    assert_int_equal(circbuf_availableForRead(&cb), 10);
    assert_true(fread(result, sizeof(char), circbuf_availableForRead(&cb), stream) == 10);
    assert_int_equal(strncmp(result, "3456789ten", 10), 0);

    fclose(stream);
}

int main(int argc, char** argv) {
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_open_close),
        cmocka_unit_test(test_rw),
        cmocka_unit_test(test_rw_over_boundary),
        cmocka_unit_test(test_write_off_by_one_bug),
        cmocka_unit_test(test_reset),
        cmocka_unit_test(test_seek_write),
        cmocka_unit_test(test_seek_read),
        cmocka_unit_test(test_seek_read_cross_boundaries),
        cmocka_unit_test(test_rw_cornercase),
        cmocka_unit_test(test_direct_api),
        cmocka_unit_test(test_eof),
        cmocka_unit_test(test_rw_increase_size),
        cmocka_unit_test(test_resize_with_read_after_write),
    };
    return cmocka_run_group_tests(tests, test_circular_buffer_setup, test_circular_buffer_teardown);
}
