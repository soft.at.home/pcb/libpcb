/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef TREE_ITERATING_H
#define TREE_ITERATING_H

#include <sahtest/define.h>

void tree_iterating_setup();
void tree_iterating_teardown();

int tree_parent_null();
int tree_parent_root();
int tree_iterating_first_null();
int tree_iterating_last_null();
int tree_iterating_next_null();
int tree_iterating_prev_null();
int tree_iterating_first_empty();
int tree_iterating_first();
int tree_iterating_last_empty();
int tree_iterating_last();
int tree_iterating_first_prev();
int tree_iterating_last_next();
int tree_iterating_first_next();
int tree_iterating_last_prev();
int tree_iterating_orphan_next();
int tree_iterating_orphan_prev();
int tree_iterating_loop_forward();
int tree_iterating_loop_reverse();
int tree_iterating_indexed();
int tree_iterating_indexed_null();

sahtest_group_begin(tree_iterating, tree_iterating_setup, tree_iterating_teardown)
sahtest_add_test(tree_parent_null, "Get parent of null tree item")
sahtest_add_test(tree_parent_root, "Get parent of root tree item")
sahtest_add_test(tree_iterating_first_null, "Test getting first child item from null pointer tree")
sahtest_add_test(tree_iterating_last_null, "Test getting last child item from null pointer tree")
sahtest_add_test(tree_iterating_next_null, "Test getting next child item with null pointer as reference")
sahtest_add_test(tree_iterating_prev_null, "Test getting prev child item with null pointer as reference")
sahtest_add_test(tree_iterating_first_empty, "Test getting first child item from empty tree item")
sahtest_add_test(tree_iterating_first, "Test getting first child item")
sahtest_add_test(tree_iterating_last_empty, "Test getting last child item from empty tree item")
sahtest_add_test(tree_iterating_last, "Test getting last child item")
sahtest_add_test(tree_iterating_first_prev, "Test getting the child item before the first")
sahtest_add_test(tree_iterating_last_next, "Test getting the child item after the last")
sahtest_add_test(tree_iterating_first_next, "Iterate to the next child item after the first")
sahtest_add_test(tree_iterating_last_prev, "Iterate to the prev child item before last")
sahtest_add_test(tree_iterating_orphan_next, "Iterate to the next child item using orphan iterator")
sahtest_add_test(tree_iterating_orphan_prev, "Iterate to the prev child item using orphan iterator ")
sahtest_add_test(tree_iterating_loop_forward, "Iterate over full tree (forward) using for_each macro")
sahtest_add_test(tree_iterating_loop_reverse, "Iterate over full tree (backward) using for_each macro")
sahtest_add_test(tree_iterating_indexed, "Fetch childeren from a tree item using an index")
sahtest_add_test(tree_iterating_indexed_null, "Fetch childeren using an index from a null tree item")
sahtest_group_end()

#endif // TREE_ITERATING_H
