/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <sahtest/sahtest.h>
#include <pcb/utils/tree.h>

static tree_t tree;
static tree_item_t item[5];
static tree_item_t orphan;

void tree_inserting_setup() {
    int i;
    tree_initialize(&tree);
    for(i = 0; i < 5; i++) {
        tree_item_initialize(&item[i]);
    }
    tree_item_initialize(&orphan);
}

void tree_inserting_teardown() {
    tree_cleanup(&tree);
}

void tree_insert_build_tree_setup() {
    int i;
    tree_initialize(&tree);
    tree_item_t* root = tree_root(&tree);
    for(i = 0; i < 5; i++) {
        tree_item_initialize(&item[i]);
        tree_item_appendChild(root, &item[i]);
    }
    tree_item_initialize(&orphan);
}

void tree_insert_build_tree_teardown() {
    tree_cleanup(&tree);
}

int tree_append_child_null_tree() {
    T_VERIFY(!tree_item_appendChild(NULL, &orphan));

    return 0;
}

int tree_append_child_null_item() {
    tree_item_t* root = tree_root(&tree);
    T_VERIFY(!tree_item_appendChild(root, NULL));

    return 0;
}

int tree_append_child_orphan_item() {
    tree_item_t* root = tree_root(&tree);
    T_VERIFY(tree_item_appendChild(root, &orphan));

    T_VERIFY(tree_item_childCount(root) == 1);
    T_VERIFY(tree_item_firstChild(root) == &orphan);
    T_VERIFY(tree_item_lastChild(root) == &orphan);

    return 0;
}

int tree_append_child_item_twice() {
    tree_item_t* root = tree_root(&tree);
    T_VERIFY(tree_item_appendChild(root, &orphan));
    T_VERIFY(tree_item_appendChild(root, &orphan));

    T_VERIFY(tree_item_childCount(root) == 1);
    T_VERIFY(tree_item_firstChild(root) == &orphan);
    T_VERIFY(tree_item_lastChild(root) == &orphan);

    return 0;
}

int tree_append_multiple_childeren() {
    tree_item_t* root = tree_root(&tree);
    int i = 0;
    for(i = 0; i < 5; i++) {
        T_VERIFY(tree_item_appendChild(root, &item[i]));
    }
    T_VERIFY(tree_item_childCount(root) == 5);

    T_VERIFY(tree_item_firstChild(root) == &item[0]);
    T_VERIFY(tree_item_lastChild(root) == &item[4]);

    T_VERIFY(tree_item_nextSibling(&item[0]) == &item[1]);
    T_VERIFY(tree_item_nextSibling(&item[1]) == &item[2]);
    T_VERIFY(tree_item_nextSibling(&item[2]) == &item[3]);
    T_VERIFY(tree_item_nextSibling(&item[3]) == &item[4]);
    T_VERIFY(tree_item_nextSibling(&item[4]) == NULL);

    T_VERIFY(tree_item_prevSibling(&item[0]) == NULL);
    T_VERIFY(tree_item_prevSibling(&item[1]) == &item[0]);
    T_VERIFY(tree_item_prevSibling(&item[2]) == &item[1]);
    T_VERIFY(tree_item_prevSibling(&item[3]) == &item[2]);
    T_VERIFY(tree_item_prevSibling(&item[4]) == &item[3]);

    T_VERIFY(tree_item_parent(&item[0]) == root);
    T_VERIFY(tree_item_parent(&item[1]) == root);
    T_VERIFY(tree_item_parent(&item[2]) == root);
    T_VERIFY(tree_item_parent(&item[3]) == root);
    T_VERIFY(tree_item_parent(&item[4]) == root);

    return 0;
}

int tree_prepend_child_null_tree() {
    T_VERIFY(!tree_item_prependChild(NULL, &orphan));

    return 0;
}

int tree_prepend_child_null_item() {
    tree_item_t* root = tree_root(&tree);
    T_VERIFY(!tree_item_prependChild(root, NULL));

    return 0;
}

int tree_prepend_child_orphan_item() {
    tree_item_t* root = tree_root(&tree);
    T_VERIFY(tree_item_prependChild(root, &orphan));

    T_VERIFY(tree_item_childCount(root) == 1);
    T_VERIFY(tree_item_firstChild(root) == &orphan);
    T_VERIFY(tree_item_lastChild(root) == &orphan);

    return 0;
}

int tree_prepend_child_item_twice() {
    tree_item_t* root = tree_root(&tree);
    T_VERIFY(tree_item_prependChild(root, &orphan));
    T_VERIFY(tree_item_prependChild(root, &orphan));

    T_VERIFY(tree_item_childCount(root) == 1);
    T_VERIFY(tree_item_firstChild(root) == &orphan);
    T_VERIFY(tree_item_lastChild(root) == &orphan);

    return 0;
}

int tree_prepend_multiple_childeren() {
    tree_item_t* root = tree_root(&tree);
    int i = 0;
    for(i = 0; i < 5; i++) {
        T_VERIFY(tree_item_prependChild(root, &item[i]));
    }
    T_VERIFY(tree_item_childCount(root) == 5);

    T_VERIFY(tree_item_firstChild(root) == &item[4]);
    T_VERIFY(tree_item_lastChild(root) == &item[0]);

    T_VERIFY(tree_item_nextSibling(&item[4]) == &item[3]);
    T_VERIFY(tree_item_nextSibling(&item[3]) == &item[2]);
    T_VERIFY(tree_item_nextSibling(&item[2]) == &item[1]);
    T_VERIFY(tree_item_nextSibling(&item[1]) == &item[0]);
    T_VERIFY(tree_item_nextSibling(&item[0]) == NULL);

    T_VERIFY(tree_item_prevSibling(&item[4]) == NULL);
    T_VERIFY(tree_item_prevSibling(&item[3]) == &item[4]);
    T_VERIFY(tree_item_prevSibling(&item[2]) == &item[3]);
    T_VERIFY(tree_item_prevSibling(&item[1]) == &item[2]);
    T_VERIFY(tree_item_prevSibling(&item[0]) == &item[1]);

    T_VERIFY(tree_item_parent(&item[0]) == root);
    T_VERIFY(tree_item_parent(&item[1]) == root);
    T_VERIFY(tree_item_parent(&item[2]) == root);
    T_VERIFY(tree_item_parent(&item[3]) == root);
    T_VERIFY(tree_item_parent(&item[4]) == root);

    return 0;
}

int tree_insert_at_null_tree() {
    T_VERIFY(!tree_item_insertChildAt(NULL, 0, &orphan));
    T_VERIFY(!tree_item_insertChildAt(NULL, 10, &orphan));

    return 0;
}

int tree_insert_at_empty_tree_index_0() {
    tree_item_t* root = tree_root(&tree);
    tree_cleanup(&tree);
    T_VERIFY(tree_item_childCount(root) == 0);
    T_VERIFY(tree_item_insertChildAt(root, 0, &orphan));
    T_VERIFY(tree_item_nextSibling(&orphan) == NULL);
    T_VERIFY(tree_item_prevSibling(&orphan) == NULL);
    T_VERIFY(tree_item_firstChild(root) == &orphan);
    T_VERIFY(tree_item_lastChild(root) == &orphan);

    return 0;
}

int tree_insert_at_empty_tree_index_2() {
    tree_item_t* root = tree_root(&tree);
    tree_cleanup(&tree);
    T_VERIFY(tree_item_childCount(root) == 0);
    T_VERIFY(tree_item_insertChildAt(root, 2, &orphan));
    T_VERIFY(tree_item_nextSibling(&orphan) == NULL);
    T_VERIFY(tree_item_prevSibling(&orphan) == NULL);
    T_VERIFY(tree_item_firstChild(root) == &orphan);
    T_VERIFY(tree_item_lastChild(root) == &orphan);

    return 0;
}

int tree_insert_at_index_out_of_boundry() {
    tree_item_t* root = tree_root(&tree);
    unsigned int size = tree_item_childCount(root);
    T_VERIFY(tree_item_insertChildAt(root, size + 10, &orphan));
    T_VERIFY(tree_item_nextSibling(&orphan) == NULL);
    T_VERIFY(tree_item_prevSibling(&orphan) == &item[4]);

    return 0;
}

int tree_insert_at_index_0() {
    tree_item_t* root = tree_root(&tree);
    T_VERIFY(tree_item_insertChildAt(root, 0, &orphan));
    T_VERIFY(tree_item_nextSibling(&orphan) == &item[0]);
    T_VERIFY(tree_item_prevSibling(&orphan) == NULL);

    return 0;
}

int tree_insert_at_index_2() {
    tree_item_t* root = tree_root(&tree);
    T_VERIFY(tree_item_insertChildAt(root, 2, &orphan));
    T_VERIFY(tree_item_nextSibling(&orphan) == &item[2]);
    T_VERIFY(tree_item_prevSibling(&orphan) == &item[1]);

    return 0;
}

int tree_insert_at_index_last() {
    tree_item_t* root = tree_root(&tree);
    unsigned int size = tree_item_childCount(root);
    T_VERIFY(tree_item_insertChildAt(root, size - 1, &orphan));
    T_VERIFY(tree_item_nextSibling(&orphan) == &item[4]);
    T_VERIFY(tree_item_prevSibling(&orphan) == &item[3]);

    return 0;
}

int tree_insert_at_null_child() {
    tree_item_t* root = tree_root(&tree);
    T_VERIFY(!tree_item_insertChildAt(root, 3, NULL));

    return 0;
}
