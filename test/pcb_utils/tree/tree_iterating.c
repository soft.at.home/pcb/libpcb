/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <sahtest/sahtest.h>
#include <pcb/utils/tree.h>

static tree_t tree;
static tree_item_t item[5];
static tree_item_t orphan;

void tree_iterating_setup() {
    int i;
    tree_initialize(&tree);
    tree_item_t* root = tree_root(&tree);
    for(i = 0; i < 5; i++) {
        tree_item_initialize(&item[i]);
        tree_item_appendChild(root, &item[i]);
    }
    tree_item_initialize(&orphan);
}

void tree_iterating_teardown() {
    tree_cleanup(&tree);
}

int tree_iterating_first_null() {
    T_VERIFY(tree_item_firstChild(NULL) == NULL);

    return 0;
}

int tree_iterating_last_null() {
    T_VERIFY(tree_item_lastChild(NULL) == NULL);

    return 0;
}

int tree_iterating_next_null() {
    T_VERIFY(tree_item_nextSibling(NULL) == NULL);

    return 0;
}

int tree_iterating_prev_null() {
    T_VERIFY(tree_item_prevSibling(NULL) == NULL);

    return 0;
}

int tree_iterating_first_empty() {
    tree_item_t* root = tree_root(&tree);
    tree_item_cleanup(root);
    T_VERIFY(tree_item_firstChild(root) == NULL);

    return 0;
}

int tree_iterating_first() {
    tree_item_t* root = tree_root(&tree);
    T_VERIFY(tree_item_firstChild(root) == &item[0]);

    return 0;
}

int tree_iterating_last_empty() {
    tree_item_t* root = tree_root(&tree);
    tree_item_cleanup(root);
    T_VERIFY(tree_item_lastChild(root) == NULL);

    return 0;
}

int tree_iterating_last() {
    tree_item_t* root = tree_root(&tree);
    T_VERIFY(tree_item_lastChild(root) == &item[4]);

    return 0;
}

int tree_iterating_first_prev() {
    tree_item_t* root = tree_root(&tree);
    tree_item_t* iter = tree_item_firstChild(root);
    T_VERIFY(tree_item_prevSibling(iter) == NULL);

    return 0;
}

int tree_iterating_last_next() {
    tree_item_t* root = tree_root(&tree);
    tree_item_t* iter = tree_item_lastChild(root);
    T_VERIFY(tree_item_nextSibling(iter) == NULL);

    return 0;
}

int tree_iterating_first_next() {
    tree_item_t* root = tree_root(&tree);
    tree_item_t* iter = tree_item_firstChild(root);
    T_VERIFY(tree_item_nextSibling(iter) == &item[1]);

    return 0;
}

int tree_iterating_last_prev() {
    tree_item_t* root = tree_root(&tree);
    tree_item_t* iter = tree_item_lastChild(root);
    T_VERIFY(tree_item_prevSibling(iter) == &item[3]);

    return 0;
}

int tree_iterating_orphan_next() {
    T_VERIFY(tree_item_nextSibling(&orphan) == NULL);

    return 0;
}

int tree_iterating_orphan_prev() {
    T_VERIFY(tree_item_prevSibling(&orphan) == NULL);

    return 0;
}

int tree_iterating_loop_forward() {
    tree_item_t* root = tree_root(&tree);
    tree_item_t* iter = NULL;
    int i = 0;
    tree_for_each_child(iter, root) {
        T_VERIFY(i < 5);
        T_VERIFY(iter == &item[i]);
        T_VERIFY(tree_item_parent(iter) == root);
        i++;
    }

    return 0;
}

int tree_iterating_loop_reverse() {
    tree_item_t* root = tree_root(&tree);
    tree_item_t* iter = NULL;
    int i = 4;
    tree_for_each_child_reverse(iter, root) {
        T_VERIFY(i >= 0);
        T_VERIFY(iter == &item[i]);
        T_VERIFY(tree_item_parent(iter) == root);
        i--;
    }

    return 0;
}

int tree_parent_null() {
    T_VERIFY(tree_item_parent(NULL) == NULL);

    return 0;
}

int tree_parent_root() {
    tree_item_t* root = tree_root(&tree);
    T_VERIFY(tree_item_parent(root) == NULL);

    return 0;
}

int tree_iterating_indexed() {
    tree_item_t* root = tree_root(&tree);
    T_VERIFY(tree_item_childAt(root, 3) == &item[3]);
    T_VERIFY(tree_item_childAt(root, 5) == NULL);
    T_VERIFY(tree_item_childAt(root, 1) == &item[1]);

    return 0;
}

int tree_iterating_indexed_null() {
    T_VERIFY(tree_item_childAt(NULL, 3) == NULL);

    return 0;
}
