/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef TREE_INSERT_H
#define TREE_INSERT_H

#include <sahtest/define.h>

void tree_inserting_setup();
void tree_inserting_teardown();

void tree_insert_build_tree_setup();
void tree_insert_build_tree_teardown();

int tree_append_child_null_tree();
int tree_append_child_null_item();
int tree_append_child_orphan_item();
int tree_append_child_item_twice();
int tree_append_multiple_childeren();

int tree_prepend_child_null_tree();
int tree_prepend_child_null_item();
int tree_prepend_child_orphan_item();
int tree_prepend_child_item_twice();
int tree_prepend_multiple_childeren();

int tree_insert_at_null_tree();
int tree_insert_at_empty_tree_index_0();
int tree_insert_at_empty_tree_index_2();
int tree_insert_at_index_out_of_boundry();
int tree_insert_at_index_0();
int tree_insert_at_index_2();
int tree_insert_at_index_last();
int tree_insert_at_null_child();

sahtest_group_begin(tree_append, tree_inserting_setup, tree_inserting_teardown)
sahtest_add_test(tree_append_child_null_tree, "Test appending item to null tree")
sahtest_add_test(tree_append_child_null_item, "Test appending null item to tree")
sahtest_add_test(tree_append_child_orphan_item, "Test appending a valid item")
sahtest_add_test(tree_append_child_item_twice, "Test appending the same item twice")
sahtest_add_test(tree_append_multiple_childeren, "Test appending multiple items after each other")
sahtest_group_end()

sahtest_group_begin(tree_prepend, tree_inserting_setup, tree_inserting_teardown)
sahtest_add_test(tree_prepend_child_null_tree, "Test prepending item to null tree")
sahtest_add_test(tree_prepend_child_null_item, "Test prepending null item to tree")
sahtest_add_test(tree_prepend_child_orphan_item, "Test prepending a valid item")
sahtest_add_test(tree_prepend_child_item_twice, "Test prepending the same item twice")
sahtest_add_test(tree_prepend_multiple_childeren, "Test prepending multiple items before each other")
sahtest_group_end()

sahtest_group_begin(tree_insert_at, tree_insert_build_tree_setup, tree_insert_build_tree_teardown)
sahtest_add_test(tree_insert_at_null_tree, "Test inserting in a null tree using index")
sahtest_add_test(tree_insert_at_empty_tree_index_0, "Test inserting in a empty tree with index 0")
sahtest_add_test(tree_insert_at_empty_tree_index_2, "Test inserting in a empty tree with index 2")
sahtest_add_test(tree_insert_at_index_out_of_boundry, "Test inserting in a tree with index higher then child count")
sahtest_add_test(tree_insert_at_index_0, "Test inserting in a tree with index 0")
sahtest_add_test(tree_insert_at_index_2, "Test inserting with tree set to 2")
sahtest_add_test(tree_insert_at_index_last, "Test inserting with index set to last item")
sahtest_add_test(tree_insert_at_null_child, "Test inserting null child")
sahtest_group_end()

sahtest_group_begin(tree_inserting, NULL, NULL)
sahtest_add_group(tree_append, "Test appending items")
sahtest_add_group(tree_prepend, "Test prepending items")
sahtest_add_group(tree_insert_at, "Test inserting at an indexp")
sahtest_group_end()

#endif // TREE_INSERT_H
