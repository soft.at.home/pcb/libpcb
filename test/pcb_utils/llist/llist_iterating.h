/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef LLIST_ITERATING_H
#define LLIST_ITERATING_H

#include <sahtest/define.h>

void llist_iterating_setup();
void llist_iterating_teardown();

int llist_iterating_first_null();
int llist_iterating_last_null();
int llist_iterating_next_null();
int llist_iterating_prev_null();
int llist_iterating_first();
int llist_iterating_last();
int llist_iterating_first_prev();
int llist_iterating_last_next();
int llist_iterating_first_next();
int llist_iterating_last_prev();
int llist_iterating_orphan_next();
int llist_iterating_orphan_prev();
int llist_iterating_loop_forward();
int llist_iterating_loop_reverse();

sahtest_group_begin(iterating, llist_iterating_setup, llist_iterating_teardown)
sahtest_add_test(llist_iterating_first_null, "Test getting first item from null pointer list")
sahtest_add_test(llist_iterating_last_null, "Test getting last item from null pointer list")
sahtest_add_test(llist_iterating_next_null, "Test getting next item with null pointer as reference")
sahtest_add_test(llist_iterating_prev_null, "Test getting prev item with null pointer as reference")
sahtest_add_test(llist_iterating_first, "Test getting first item")
sahtest_add_test(llist_iterating_last, "Test getting last item")
sahtest_add_test(llist_iterating_first_prev, "Test getting the item before the first")
sahtest_add_test(llist_iterating_last_next, "Test getting the item after the last")
sahtest_add_test(llist_iterating_first_next, "Iterate to the next item after the first")
sahtest_add_test(llist_iterating_last_prev, "Iterate to the prev element before last")
sahtest_add_test(llist_iterating_orphan_next, "Iterate to the next element using orphan iterator")
sahtest_add_test(llist_iterating_orphan_prev, "Iterate to the prev element using orphan iterator ")
sahtest_add_test(llist_iterating_loop_forward, "Iterate over full list (forward) using for_each macro")
sahtest_add_test(llist_iterating_loop_reverse, "Iterate over full list (backward) using for_each macro")
sahtest_group_end()

#endif // LLIST_ITERATING_H
