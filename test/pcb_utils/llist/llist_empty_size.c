/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include "pcb/utils/linked_list.h"

void llist_null_list_is_empty(void** state) {
    (void) state;
    assert_true(llist_isEmpty(NULL));
}

void llist_is_empty_after_initialization(void** state) {
    (void) state;
    llist_t list;
    llist_initialize(&list);
    assert_true(llist_isEmpty(&list));
}

void llist_is_not_empty_after_appending_iterators(void** state) {
    (void) state;
    int i = 0;
    llist_t list;
    llist_iterator_t it[5];
    llist_initialize(&list);

    for(i = 0; i < 5; i++) {
        llist_iterator_initialize(&it[i]);
        llist_append(&list, &it[i]);
    }

    assert_false(llist_isEmpty(&list));
}

void llist_is_empty_after_cleanup(void** state) {
    (void) state;
    int i = 0;
    llist_t list;
    llist_iterator_t it[5];
    llist_initialize(&list);

    for(i = 0; i < 5; i++) {
        llist_iterator_initialize(&it[i]);
        llist_append(&list, &it[i]);
    }

    assert_false(llist_isEmpty(&list));
    llist_cleanup(&list);
    assert_true(llist_isEmpty(&list));
}

void llist_null_list_size_is_0(void** state) {
    (void) state;
    assert_int_equal(0, llist_size(NULL));
}

int llist_size_is_0_after_initialization(void** state) {
    (void) state;
    llist_t list;
    llist_initialize(&list);
    assert_int_equal(0, llist_size(&list));
}

void llist_size_equals_number_of_iterators_appended(void** state) {
    (void) state;
    int i = 0;
    llist_t list;
    llist_iterator_t it[5];
    llist_initialize(&list);

    for(i = 0; i < 5; i++) {
        llist_iterator_initialize(&it[i]);
        llist_append(&list, &it[i]);
    }

    assert_int_equal(5, llist_size(&list));
}

void llist_size_is_0_after_cleanup(void** state) {
    (void) state;
    int i = 0;
    llist_t list;
    llist_iterator_t it[5];
    llist_initialize(&list);

    for(i = 0; i < 5; i++) {
        llist_iterator_initialize(&it[i]);
        llist_append(&list, &it[i]);
    }

    assert_int_not_equal(0, llist_size(&list));
    llist_cleanup(&list);
    assert_int_equal(0, llist_size(&list));
}
