/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef LLIST_INSERT_H
#define LLIST_INSERT_H

#include <sahtest/define.h>

void llist_inserting_setup();
void llist_inserting_teardown();

void llist_insert_build_list_setup();
void llist_insert_build_list_teardown();

int llist_insert_append_null_list();
int llist_insert_append_null_it();
int llist_insert_append_orphan();
int llist_insert_append_iterator_twice();
int llist_insert_append_multiple();

int llist_insert_prepend_null_list();
int llist_insert_prepend_null_it();
int llist_insert_prepend_orphan();
int llist_insert_prepend_iterator_twice();
int llist_insert_prepend_multiple();

int llist_insert_before_null_list();
int llist_insert_before_null_reference();
int llist_insert_before_null_it();
int llist_insert_before_first();
int llist_insert_before_last();
int llist_insert_before_second();
int llist_insert_before_reference_is_insert_it();
int llist_insert_before_in_empty_list();
int llist_insert_before_using_orphan_reference();
int llist_insert_before_move_iterator_in_list();

int llist_insert_after_null_list();
int llist_insert_after_null_reference();
int llist_insert_after_null_it();
int llist_insert_after_first();
int llist_insert_after_last();
int llist_insert_after_second();
int llist_insert_after_reference_is_insert_it();
int llist_insert_after_in_empty_list();
int llist_insert_after_using_orphan_reference();
int llist_insert_after_move_iterator_in_list();

int llist_insert_indexed_null_list();
int llist_insert_indexed_empty_list_index_0();
int llist_insert_indexed_empty_list_index_2();
int llist_insert_indexed_list_index_out_of_boundry();
int llist_insert_indexed_list_index_0();
int llist_insert_indexed_list_index_2();
int llist_insert_indexed_list_index_last();

sahtest_group_begin(ll_append, llist_inserting_setup, llist_inserting_teardown)
sahtest_add_test(llist_insert_append_null_list, "Test appending iterator to null list")
sahtest_add_test(llist_insert_append_null_it, "Test appending null iterator to list")
sahtest_add_test(llist_insert_append_orphan, "Test appending a valid iterator")
sahtest_add_test(llist_insert_append_iterator_twice, "Test appending the same iterator twice")
sahtest_add_test(llist_insert_append_multiple, "Test appending multiple iterators after each other")
sahtest_group_end()

sahtest_group_begin(ll_prepend, llist_inserting_setup, llist_inserting_teardown)
sahtest_add_test(llist_insert_prepend_null_list, "Test prepending iterator to null list")
sahtest_add_test(llist_insert_prepend_null_it, "Test prepending null iterator to list")
sahtest_add_test(llist_insert_prepend_orphan, "Test prepending a valid iterator")
sahtest_add_test(llist_insert_prepend_iterator_twice, "Test prepending the same iterator twice")
sahtest_add_test(llist_insert_prepend_multiple, "Test prepending multiple iterators before each other")
sahtest_group_end()

sahtest_group_begin(insert_before, llist_insert_build_list_setup, llist_insert_build_list_teardown)
sahtest_add_test(llist_insert_before_null_list, "Test inserting in a null list")
sahtest_add_test(llist_insert_before_null_reference, "Test inserting before null reference")
sahtest_add_test(llist_insert_before_null_it, "Test inserting null it")
sahtest_add_test(llist_insert_before_first, "Test inserting before first item")
sahtest_add_test(llist_insert_before_last, "Test inserting before last item")
sahtest_add_test(llist_insert_before_second, "Test inserting before second item")
sahtest_add_test(llist_insert_before_reference_is_insert_it, "Test inserting item which is used as reference")
sahtest_add_test(llist_insert_before_in_empty_list, "Test inserting item in an empty list")
sahtest_add_test(llist_insert_before_using_orphan_reference, "Test inserting using a reference not in the list")
sahtest_add_test(llist_insert_before_move_iterator_in_list, "Test inserting item which is in the list")
sahtest_group_end()

sahtest_group_begin(insert_after, llist_insert_build_list_setup, llist_insert_build_list_teardown)
sahtest_add_test(llist_insert_after_null_list, "Test inserting in a null list")
sahtest_add_test(llist_insert_after_null_reference, "Test inserting after null reference")
sahtest_add_test(llist_insert_after_null_it, "Test inserting null it")
sahtest_add_test(llist_insert_after_first, "Test inserting after first item")
sahtest_add_test(llist_insert_after_last, "Test inserting after last item")
sahtest_add_test(llist_insert_after_second, "Test inserting after second item")
sahtest_add_test(llist_insert_after_reference_is_insert_it, "Test inserting item which is used as reference")
sahtest_add_test(llist_insert_after_in_empty_list, "Test inserting item in an empty list")
sahtest_add_test(llist_insert_after_using_orphan_reference, "Test inserting using a reference not in the list")
sahtest_add_test(llist_insert_after_move_iterator_in_list, "Test inserting item which is in the list")
sahtest_group_end()

sahtest_group_begin(insert_indexed, llist_insert_build_list_setup, llist_insert_build_list_teardown)
sahtest_add_test(llist_insert_indexed_null_list, "Test inserting in a null list using index")
sahtest_add_test(llist_insert_indexed_empty_list_index_0, "Test inserting in a empty list with index 0")
sahtest_add_test(llist_insert_indexed_empty_list_index_2, "Test inserting in a empty list with index 2")
sahtest_add_test(llist_insert_indexed_list_index_out_of_boundry, "Test inserting in a list with index higher then size of list")
sahtest_add_test(llist_insert_indexed_list_index_0, "Test inserting in a list with index 0")
sahtest_add_test(llist_insert_indexed_list_index_2, "Test inserting with index set to 2")
sahtest_add_test(llist_insert_indexed_list_index_last, "Test inserting with index set to last item")
sahtest_group_end()

sahtest_group_begin(inserting, NULL, NULL)
sahtest_add_group(ll_append, "Test appending items")
sahtest_add_group(ll_prepend, "Test prepending items")
sahtest_add_group(insert_before, "Test inserting items before a reference item")
sahtest_add_group(insert_after, "Test inserting items after a reference item")
sahtest_add_group(insert_indexed, "Test inserting items using index")
sahtest_group_end()

#endif // LLIST_INSERT_H
