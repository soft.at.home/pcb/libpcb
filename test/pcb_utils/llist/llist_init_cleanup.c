/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include "pcb/utils/linked_list.h"

void llist_is_correctly_initialized(void** state) {
    (void) state;
    llist_t list;
    assert_true(llist_initialize(&list));
    assert_null(llist_first(&list));
    assert_null(llist_last(&list));
    llist_cleanup(&list);
}

void llist_null_list_init_fails(void** state) {
    (void) state;
    assert_false(llist_initialize(NULL));
}

void llist_null_list_cleanup_does_not_crash(void** state) {
    (void) state;
    llist_cleanup(NULL);
}

void llist_iterator_is_correctly_initialized(void** state) {
    (void) state;
    llist_iterator_t it;
    assert_true(llist_iterator_initialize(&it));
    assert_null(llist_iterator_prev(&it));
    assert_null(llist_iterator_next(&it));
}

void llist_null_iterator_init_fails(void** state) {
    (void) state;
    assert_false(llist_iterator_initialize(NULL));
}

void llist_fill_and_cleanup_correctly_sets_first_and_last(void** state) {
    (void) state;
    llist_t list;
    llist_iterator_t it[5];
    int i;
    assert_true(llist_initialize(&list));
    for(i = 0; i < 5; i++) {
        assert_true(llist_iterator_initialize(&it[i]));
        assert_true(llist_append(&list, &it[i]));
    }
    assert_int_equal(llist_size(&list), 5);
    assert_ptr_equal(llist_first(&list), &it[0]);
    assert_ptr_equal(llist_last(&list), &it[4]);
    llist_cleanup(&list);
    assert_int_equal(llist_size(&list), 0);
    assert_null(llist_first(&list));
    assert_null(llist_last(&list));
}
