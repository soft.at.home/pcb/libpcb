/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <sahtest/sahtest.h>
#include <pcb/utils/linked_list.h>

static llist_t list;
static llist_iterator_t it[5];
static llist_iterator_t orphan;

void llist_inserting_setup() {
    int i;
    llist_initialize(&list);
    for(i = 0; i < 5; i++) {
        llist_iterator_initialize(&it[i]);
    }
    llist_iterator_initialize(&orphan);
}

void llist_inserting_teardown() {
    llist_cleanup(&list);
}

void llist_insert_build_list_setup() {
    int i;
    llist_initialize(&list);
    for(i = 0; i < 5; i++) {
        llist_iterator_initialize(&it[i]);
        llist_append(&list, &it[i]);
    }
    llist_iterator_initialize(&orphan);
}

void llist_insert_build_list_teardown() {
    llist_cleanup(&list);
}

int llist_insert_append_null_list() {
    T_VERIFY(!llist_append(NULL, &orphan));

    return 0;
}

int llist_insert_append_null_it() {
    T_VERIFY(!llist_append(&list, NULL));

    return 0;
}

int llist_insert_append_orphan() {
    T_VERIFY(llist_append(&list, &orphan));

    T_VERIFY(llist_size(&list) == 1);
    T_VERIFY(llist_first(&list) == &orphan);
    T_VERIFY(llist_last(&list) == &orphan);

    return 0;
}

int llist_insert_append_iterator_twice() {
    T_VERIFY(llist_append(&list, &orphan));
    T_VERIFY(llist_append(&list, &orphan));

    T_VERIFY(llist_size(&list) == 1);
    T_VERIFY(llist_first(&list) == &orphan);
    T_VERIFY(llist_last(&list) == &orphan);

    return 0;
}

int llist_insert_append_multiple() {
    int i = 0;
    for(i = 0; i < 5; i++) {
        T_VERIFY(llist_append(&list, &it[i]));
    }
    T_VERIFY(llist_size(&list) == 5);

    T_VERIFY(llist_first(&list) == &it[0]);
    T_VERIFY(llist_last(&list) == &it[4]);

    T_VERIFY(llist_iterator_next(&it[0]) == &it[1]);
    T_VERIFY(llist_iterator_next(&it[1]) == &it[2]);
    T_VERIFY(llist_iterator_next(&it[2]) == &it[3]);
    T_VERIFY(llist_iterator_next(&it[3]) == &it[4]);
    T_VERIFY(llist_iterator_next(&it[4]) == NULL);

    T_VERIFY(llist_iterator_prev(&it[0]) == NULL);
    T_VERIFY(llist_iterator_prev(&it[1]) == &it[0]);
    T_VERIFY(llist_iterator_prev(&it[2]) == &it[1]);
    T_VERIFY(llist_iterator_prev(&it[3]) == &it[2]);
    T_VERIFY(llist_iterator_prev(&it[4]) == &it[3]);

    return 0;
}

int llist_insert_prepend_null_list() {
    T_VERIFY(!llist_prepend(NULL, &orphan));

    return 0;
}

int llist_insert_prepend_null_it() {
    T_VERIFY(!llist_prepend(&list, NULL));

    return 0;
}

int llist_insert_prepend_orphan() {
    T_VERIFY(llist_prepend(&list, &orphan));

    T_VERIFY(llist_size(&list) == 1);
    T_VERIFY(llist_first(&list) == &orphan);
    T_VERIFY(llist_last(&list) == &orphan);

    return 0;
}

int llist_insert_prepend_iterator_twice() {
    T_VERIFY(llist_prepend(&list, &orphan));
    T_VERIFY(llist_prepend(&list, &orphan));

    T_VERIFY(llist_size(&list) == 1);
    T_VERIFY(llist_first(&list) == &orphan);
    T_VERIFY(llist_last(&list) == &orphan);

    return 0;
}

int llist_insert_prepend_multiple() {
    int i = 0;
    for(i = 0; i < 5; i++) {
        T_VERIFY(llist_prepend(&list, &it[i]));
    }
    T_VERIFY(llist_size(&list) == 5);

    T_VERIFY(llist_first(&list) == &it[4]);
    T_VERIFY(llist_last(&list) == &it[0]);

    T_VERIFY(llist_iterator_next(&it[4]) == &it[3]);
    T_VERIFY(llist_iterator_next(&it[3]) == &it[2]);
    T_VERIFY(llist_iterator_next(&it[2]) == &it[1]);
    T_VERIFY(llist_iterator_next(&it[1]) == &it[0]);
    T_VERIFY(llist_iterator_next(&it[0]) == NULL);

    T_VERIFY(llist_iterator_prev(&it[4]) == NULL);
    T_VERIFY(llist_iterator_prev(&it[3]) == &it[4]);
    T_VERIFY(llist_iterator_prev(&it[2]) == &it[3]);
    T_VERIFY(llist_iterator_prev(&it[1]) == &it[2]);
    T_VERIFY(llist_iterator_prev(&it[0]) == &it[1]);

    return 0;
}

int llist_insert_before_null_list() {
    T_VERIFY(!llist_insertBefore(NULL, &it[0], &orphan));

    return 0;
}

int llist_insert_before_null_reference() {
    T_VERIFY(llist_insertBefore(&list, NULL, &orphan));

    T_VERIFY(llist_first(&list) == &orphan);
    T_VERIFY(llist_iterator_next(&orphan) == &it[0]);

    return 0;
}

int llist_insert_before_null_it() {
    T_VERIFY(!llist_insertBefore(&list, &it[0], NULL));

    return 0;
}

int llist_insert_before_first() {
    T_VERIFY(llist_insertBefore(&list, &it[0], &orphan));
    T_VERIFY(llist_iterator_next(&orphan) == &it[0]);
    T_VERIFY(llist_iterator_prev(&orphan) == NULL);

    return 0;
}

int llist_insert_before_last() {
    T_VERIFY(llist_insertBefore(&list, &it[4], &orphan));
    T_VERIFY(llist_iterator_next(&orphan) == &it[4]);
    T_VERIFY(llist_iterator_prev(&orphan) == &it[3]);

    return 0;
}

int llist_insert_before_second() {
    T_VERIFY(llist_insertBefore(&list, &it[1], &orphan));
    T_VERIFY(llist_iterator_next(&orphan) == &it[1]);
    T_VERIFY(llist_iterator_prev(&orphan) == &it[0]);

    return 0;
}

int llist_insert_before_reference_is_insert_it() {
    T_VERIFY(llist_insertBefore(&list, &it[2], &it[2]));
    T_VERIFY(llist_size(&list) == 5);

    return 0;
}

int llist_insert_before_in_empty_list() {
    llist_cleanup(&list);
    T_VERIFY(llist_size(&list) == 0);
    T_VERIFY(llist_insertBefore(&list, NULL, &orphan));

    return 0;
}

int llist_insert_before_using_orphan_reference() {
    T_VERIFY(!llist_insertBefore(&list, &orphan, &it[0]));

    return 0;
}

int llist_insert_before_move_iterator_in_list() {
    T_VERIFY(llist_insertBefore(&list, &it[4], &it[0]));
    T_VERIFY(llist_iterator_prev(&it[4]) == &it[0]);

    return 0;
}

int llist_insert_after_null_list() {
    T_VERIFY(!llist_insertAfter(NULL, &it[0], &orphan));

    return 0;
}

int llist_insert_after_null_reference() {
    T_VERIFY(llist_insertAfter(&list, NULL, &orphan));

    return 0;
}

int llist_insert_after_null_it() {
    T_VERIFY(!llist_insertAfter(&list, &it[0], NULL));

    return 0;
}

int llist_insert_after_first() {
    T_VERIFY(llist_insertAfter(&list, &it[0], &orphan));
    T_VERIFY(llist_iterator_next(&orphan) == &it[1]);
    T_VERIFY(llist_iterator_prev(&orphan) == &it[0]);

    return 0;
}

int llist_insert_after_last() {
    T_VERIFY(llist_insertAfter(&list, &it[4], &orphan));
    T_VERIFY(llist_iterator_next(&orphan) == NULL);
    T_VERIFY(llist_iterator_prev(&orphan) == &it[4]);

    return 0;
}

int llist_insert_after_second() {
    T_VERIFY(llist_insertAfter(&list, &it[1], &orphan));
    T_VERIFY(llist_iterator_next(&orphan) == &it[2]);
    T_VERIFY(llist_iterator_prev(&orphan) == &it[1]);

    return 0;
}

int llist_insert_after_reference_is_insert_it() {
    T_VERIFY(llist_insertAfter(&list, &it[2], &it[2]));
    T_VERIFY(llist_size(&list) == 5);

    return 0;
}

int llist_insert_after_in_empty_list() {
    llist_cleanup(&list);
    T_VERIFY(llist_size(&list) == 0);
    T_VERIFY(llist_insertAfter(&list, NULL, &orphan));
    T_VERIFY(llist_first(&list) == &orphan);
    T_VERIFY(llist_last(&list) == &orphan);

    return 0;
}

int llist_insert_after_using_orphan_reference() {
    T_VERIFY(!llist_insertAfter(&list, &orphan, &it[0]));

    return 0;
}

int llist_insert_after_move_iterator_in_list() {
    T_VERIFY(llist_insertAfter(&list, &it[0], &it[4]));
    T_VERIFY(llist_iterator_prev(&it[4]) == &it[0]);

    return 0;
}

int llist_insert_indexed_null_list() {
    T_VERIFY(!llist_insertAt(NULL, 0, &orphan));
    T_VERIFY(!llist_insertAt(NULL, 10, &orphan));

    return 0;
}

int llist_insert_indexed_empty_list_index_0() {
    llist_cleanup(&list);
    T_VERIFY(llist_size(&list) == 0);
    T_VERIFY(llist_insertAt(&list, 0, &orphan));
    T_VERIFY(llist_iterator_next(&orphan) == NULL);
    T_VERIFY(llist_iterator_prev(&orphan) == NULL);
    T_VERIFY(llist_first(&list) == &orphan);
    T_VERIFY(llist_last(&list) == &orphan);

    return 0;
}

int llist_insert_indexed_empty_list_index_2() {
    llist_cleanup(&list);
    T_VERIFY(llist_size(&list) == 0);
    T_VERIFY(llist_insertAt(&list, 0, &orphan));
    T_VERIFY(llist_iterator_next(&orphan) == NULL);
    T_VERIFY(llist_iterator_prev(&orphan) == NULL);
    T_VERIFY(llist_first(&list) == &orphan);
    T_VERIFY(llist_last(&list) == &orphan);

    return 0;
}

int llist_insert_indexed_list_index_out_of_boundry() {
    unsigned int size = llist_size(&list);
    T_VERIFY(llist_insertAt(&list, size + 10, &orphan));
    T_VERIFY(llist_iterator_next(&orphan) == NULL);
    T_VERIFY(llist_iterator_prev(&orphan) == &it[4]);

    return 0;
}

int llist_insert_indexed_list_index_0() {
    T_VERIFY(llist_insertAt(&list, 0, &orphan));
    T_VERIFY(llist_iterator_next(&orphan) == &it[0]);
    T_VERIFY(llist_iterator_prev(&orphan) == NULL);

    return 0;
}

int llist_insert_indexed_list_index_2() {
    T_VERIFY(llist_insertAt(&list, 2, &orphan));
    T_VERIFY(llist_iterator_next(&orphan) == &it[2]);
    T_VERIFY(llist_iterator_prev(&orphan) == &it[1]);

    return 0;
}

int llist_insert_indexed_list_index_last() {
    unsigned int size = llist_size(&list);
    T_VERIFY(llist_insertAt(&list, size - 1, &orphan));
    T_VERIFY(llist_iterator_next(&orphan) == &it[4]);
    T_VERIFY(llist_iterator_prev(&orphan) == &it[3]);

    return 0;
}
