/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef VARIANT_INIT_H
#define VARIANT_INIT_H

#include <sahtest/define.h>

void variant_init_setup();
void variant_init_teardown();

int variant_init_using_initialize();
int variant_init_setChar();
int variant_init_setString();
int variant_init_setInt8();
int variant_init_setInt16();
int variant_init_setInt32();
int variant_init_setInt64();
int variant_init_setUInt8();
int variant_init_setUInt16();
int variant_init_setUInt32();
int variant_init_setUInt64();
int variant_init_setBool();
int variant_init_setDouble();
int variant_init_setDateTime();
int variant_init_setListMove();
int variant_init_setListCopy();
int variant_init_setMapMove();
int variant_init_setMapCopy();
int variant_init_setFd();
int variant_init_setByteArray();

sahtest_group_begin(var_init_cleanup, variant_init_setup, variant_init_teardown)
sahtest_add_test(variant_init_using_initialize, "Test initializing a variant using variant_initialize")
sahtest_add_test(variant_init_setChar, "Test initializing a variant using variant_setChar")
sahtest_add_test(variant_init_setString, "Test initializing a variant using variant_setString")
sahtest_add_test(variant_init_setInt8, "Test initializing a variant using variant_setInt8")
sahtest_add_test(variant_init_setInt16, "Test initializing a variant using variant_setInt16")
sahtest_add_test(variant_init_setInt32, "Test initializing a variant using variant_setInt32")
sahtest_add_test(variant_init_setInt64, "Test initializing a variant using variant_setInt64")
sahtest_add_test(variant_init_setUInt8, "Test initializing a variant using variant_setUInt8")
sahtest_add_test(variant_init_setUInt16, "Test initializing a variant using variant_setInt16")
sahtest_add_test(variant_init_setUInt32, "Test initializing a variant using variant_setInt32")
sahtest_add_test(variant_init_setUInt64, "Test initializing a variant using variant_setInt64")
sahtest_add_test(variant_init_setBool, "Test initializing a variant using variant_setBool")
sahtest_add_test(variant_init_setDouble, "Test initializing a variant using variant_setDouble")
sahtest_add_test(variant_init_setDateTime, "Test initializing a variant using variant_setDateTime")
sahtest_add_test(variant_init_setListMove, "Test initializing a variant using variant_setListMove")
sahtest_add_test(variant_init_setListCopy, "Test initializing a variant using variant_setListCopy")
sahtest_add_test(variant_init_setMapMove, "Test initializing a variant using variant_setMapMove")
sahtest_add_test(variant_init_setMapCopy, "Test initializing a variant using variant_setMapCopy")
sahtest_add_test(variant_init_setFd, "Test initializing a variant using variant_setFd")
sahtest_add_test(variant_init_setByteArray, "Test initializing a variant using variant_setByteArray")
sahtest_group_end()

#endif // VARIANT_INIT_H
