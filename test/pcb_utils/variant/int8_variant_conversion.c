/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <sahtest/sahtest.h>
#include <pcb/utils/variant.h>
#include <pcb/utils/variant_list.h>
#include <pcb/utils/variant_map.h>

static variant_t variant_possitive;
static variant_t variant_negative;

void variant_int8_conversion_setup() {
    variant_initialize(&variant_possitive, variant_type_int8);
    variant_initialize(&variant_negative, variant_type_int8);

    variant_setInt8(&variant_possitive, 127);
    variant_setInt8(&variant_negative, -1);
}

void variant_int8_conversion_teardown() {
    variant_cleanup(&variant_possitive);
    variant_cleanup(&variant_negative);
}


int variant_int8_tostring() {
    string_t result;
    string_t* string = NULL;
    string_initialize(&result, 0);

    assert_bool(variant_toString(&result, &variant_possitive));
    assert_bool(strcmp(string_buffer(&result), "127") == 0);

    assert_bool(variant_toString(&result, &variant_negative));
    assert_bool(strcmp(string_buffer(&result), "-1") == 0);

    string = variant_string(&variant_possitive);
    assert_bool(string != NULL);
    assert_bool(strcmp(string_buffer(string), "127") == 0);
    string_cleanup(string);
    free(string);

    string = variant_string(&variant_negative);
    assert_bool(string != NULL);
    assert_bool(strcmp(string_buffer(string), "-1") == 0);
    string_cleanup(string);
    free(string);

    string_cleanup(&result);
    sahtest_success();
}

int variant_int8_tojson() {
    string_t result;
    string_initialize(&result, 0);

    assert_bool(variant_toJSON(&result, &variant_possitive));
    assert_bool(strcmp(string_buffer(&result), "127") == 0);

    assert_bool(variant_toJSON(&result, &variant_negative));
    assert_bool(strcmp(string_buffer(&result), "-1") == 0);

    string_cleanup(&result);
    sahtest_success();
}

int variant_int8_toChar() {
    char* result = NULL;

    assert_bool(variant_toChar(&result, &variant_possitive));
    assert_bool(result != NULL);
    assert_bool(strcmp(result, "127") == 0);
    free(result);
    result = variant_char(&variant_possitive);
    assert_bool(result != NULL);
    assert_bool(strcmp(result, "127") == 0);
    free(result);

    assert_bool(variant_toChar(&result, &variant_negative));
    assert_bool(result != NULL);
    assert_bool(strcmp(result, "-1") == 0);
    free(result);
    result = variant_char(&variant_negative);
    assert_bool(result != NULL);
    assert_bool(strcmp(result, "-1") == 0);
    free(result);

    sahtest_success();
}

int variant_int8_toInt8() {
    int8_t result = 0;

    assert_bool(variant_toInt8(&result, &variant_possitive));
    assert_bool(result == 127);
    result = variant_int8(&variant_possitive);
    assert_bool(result == 127);

    assert_bool(variant_toInt8(&result, &variant_negative));
    assert_bool(result == -1);
    result = variant_int8(&variant_negative);
    assert_bool(result == -1);

    sahtest_success();
}

int variant_int8_toInt16() {
    int16_t result = 0;

    assert_bool(variant_toInt16(&result, &variant_possitive));
    assert_bool(result == 127);
    result = variant_int16(&variant_possitive);
    assert_bool(result == 127);

    assert_bool(variant_toInt16(&result, &variant_negative));
    assert_bool(result == -1);
    result = variant_int16(&variant_negative);
    assert_bool(result == -1);

    sahtest_success();
}

int variant_int8_toInt32() {
    int32_t result = 0;

    assert_bool(variant_toInt32(&result, &variant_possitive));
    assert_bool(result == 127);
    result = variant_int32(&variant_possitive);
    assert_bool(result == 127);

    assert_bool(variant_toInt32(&result, &variant_negative));
    assert_bool(result == -1);
    result = variant_int32(&variant_negative);
    assert_bool(result == -1);

    sahtest_success();
}

int variant_int8_toInt64() {
    int64_t result = 0;

    assert_bool(variant_toInt64(&result, &variant_possitive));
    assert_bool(result == 127);
    result = variant_int64(&variant_possitive);
    assert_bool(result == 127);

    assert_bool(variant_toInt64(&result, &variant_negative));
    assert_bool(result == -1);
    result = variant_int64(&variant_negative);
    assert_bool(result == -1);

    sahtest_success();
}

int variant_int8_toUInt8() {
    uint8_t result = 0;

    assert_bool(variant_toUInt8(&result, &variant_possitive));
    assert_bool(result == 127);
    result = variant_uint8(&variant_possitive);
    assert_bool(result == 127);

    assert_bool(variant_toUInt8(&result, &variant_negative));
    assert_bool(result == 1);
    result = variant_uint8(&variant_negative);
    assert_bool(result == 1);

    sahtest_success();
}

int variant_int8_toUInt16() {
    uint16_t result = 0;

    assert_bool(variant_toUInt16(&result, &variant_possitive));
    assert_bool(result == 127);
    result = variant_uint16(&variant_possitive);
    assert_bool(result == 127);

    assert_bool(variant_toUInt16(&result, &variant_negative));
    assert_bool(result == 1);
    result = variant_uint16(&variant_negative);
    assert_bool(result == 1);

    sahtest_success();
}

int variant_int8_toUInt32() {
    uint32_t result = 0;

    assert_bool(variant_toUInt32(&result, &variant_possitive));
    assert_bool(result == 127);
    result = variant_uint32(&variant_possitive);
    assert_bool(result == 127);

    assert_bool(variant_toUInt32(&result, &variant_negative));
    assert_bool(result == 1);
    result = variant_uint32(&variant_negative);
    assert_bool(result == 1);

    sahtest_success();
}

int variant_int8_toUInt64() {
    uint64_t result = 0;

    assert_bool(variant_toUInt64(&result, &variant_possitive));
    assert_bool(result == 127);
    result = variant_uint64(&variant_possitive);
    assert_bool(result == 127);

    assert_bool(variant_toUInt64(&result, &variant_negative));
    assert_bool(result == 1);
    result = variant_uint64(&variant_negative);
    assert_bool(result == 1);

    sahtest_success();
}

int variant_int8_toBool() {
    bool result = false;

    assert_bool(variant_toBool(&result, &variant_possitive));
    assert_bool(result == true);
    result = variant_bool(&variant_possitive);
    assert_bool(result == true);

    assert_bool(variant_toBool(&result, &variant_negative));
    assert_bool(result == true);
    result = variant_bool(&variant_negative);
    assert_bool(result == true);

    sahtest_success();
}

int variant_int8_toFd() {
    int result = 0;

    assert_bool(variant_toFd(&result, &variant_possitive) == false);
    assert_bool(variant_toFd(&result, &variant_negative) == false);

    assert_bool(variant_fd(&variant_possitive) == -1);
    assert_bool(variant_fd(&variant_negative) == -1);

    sahtest_success();
}

int variant_int8_toDouble() {
    double result = 0;

    assert_bool(variant_toDouble(&result, &variant_possitive));
    assert_bool(result == 127);
    result = variant_double(&variant_possitive);
    assert_bool(result == 127);

    assert_bool(variant_toDouble(&result, &variant_negative));
    assert_bool(result == -1);
    result = variant_double(&variant_negative);
    assert_bool(result == -1);

    sahtest_success();
}

int variant_int8_toList() {
    variant_list_t result;
    variant_list_t* result_list = NULL;
    variant_list_initialize(&result);

    assert_bool(variant_toList(&result, &variant_possitive) == true);
    assert_bool(variant_list_size(&result) == 1);
    result_list = variant_list(&variant_possitive);
    assert_bool(result_list != NULL);
    assert_bool(variant_list_size(result_list) == 1);

    variant_list_clear(&result);
    variant_list_clear(result_list);
    free(result_list);

    sahtest_success();
}

int variant_int8_toMap() {
    variant_map_t result;
    variant_map_t* result_map = NULL;
    variant_map_initialize(&result);

    assert_bool(variant_toMap(&result, &variant_possitive) == false);
    assert_bool(variant_map_size(&result) == 0);
    result_map = variant_map(&variant_possitive);
    assert_bool(result_map == NULL);

    sahtest_success();
}
