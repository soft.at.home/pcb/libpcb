/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <sahtest/sahtest.h>
#include <pcb/utils/variant.h>
#include <pcb/utils/variant_list.h>
#include <pcb/utils/variant_map.h>

static variant_t variant_unknown;

void variant_null_unknown_conversion_setup() {
    variant_initialize(&variant_unknown, variant_type_unknown);
}

void variant_null_unknown_conversion_teardown() {

}

int variant_null_unknown_tostring() {
    string_t result;
    string_t* string = NULL;
    string_initialize(&result, 0);

    assert_bool(variant_toString(&result, &variant_unknown) == false);
    assert_bool(result.buffer == NULL);

    assert_bool(variant_toString(&result, NULL) == false);

    string = variant_string(&variant_unknown);
    assert_bool(string == NULL);
    string_cleanup(string);
    free(string);

    string = variant_string(NULL);
    assert_bool(string == NULL);
    string_cleanup(string);
    free(string);

    string_cleanup(&result);
    sahtest_success();
}

int variant_null_unknown_tojson() {
    string_t result;
    string_initialize(&result, 0);

    assert_bool(variant_toJSON(&result, NULL) == false);
    assert_bool(result.buffer == NULL);

    assert_bool(variant_toJSON(&result, &variant_unknown) == true);
    assert_bool(result.buffer != NULL);
    assert_bool(strcmp(string_buffer(&result), "null") == 0);

    string_cleanup(&result);
    sahtest_success();
}

int variant_null_unknown_toChar() {
    char* result = NULL;

    assert_bool(variant_toChar(&result, &variant_unknown) == false);
    assert_bool(result == NULL);
    free(result);

    assert_bool(variant_toChar(&result, NULL) == false);

    assert_bool(variant_char(NULL) == NULL);
    assert_bool(variant_char(&variant_unknown) == NULL);

    sahtest_success();
}

int variant_null_unknown_toInt8() {
    int8_t result = 0;

    assert_bool(variant_toInt8(&result, &variant_unknown) == false);
    assert_bool(result == 0);

    assert_bool(variant_toInt8(&result, NULL) == false);

    assert_bool(variant_int8(NULL) == 0);
    assert_bool(variant_int8(&variant_unknown) == 0);

    sahtest_success();
}

int variant_null_unknown_toInt16() {
    int16_t result = 0;

    assert_bool(variant_toInt16(&result, &variant_unknown) == false);
    assert_bool(result == 0);

    assert_bool(variant_toInt16(&result, NULL) == false);

    assert_bool(variant_int16(NULL) == 0);
    assert_bool(variant_int16(&variant_unknown) == 0);

    sahtest_success();
}

int variant_null_unknown_toInt32() {
    int32_t result = 0;

    assert_bool(variant_toInt32(&result, &variant_unknown) == false);
    assert_bool(result == 0);

    assert_bool(variant_toInt32(&result, NULL) == false);

    assert_bool(variant_int32(NULL) == 0);
    assert_bool(variant_int32(&variant_unknown) == 0);

    sahtest_success();
}

int variant_null_unknown_toInt64() {
    int64_t result = 0;

    assert_bool(variant_toInt64(&result, &variant_unknown) == false);
    assert_bool(result == 0);

    assert_bool(variant_toInt64(&result, NULL) == false);

    assert_bool(variant_int64(NULL) == 0);
    assert_bool(variant_int64(&variant_unknown) == 0);

    sahtest_success();
}

int variant_null_unknown_toUInt8() {
    uint8_t result = 0;

    assert_bool(variant_toUInt8(&result, &variant_unknown) == false);
    assert_bool(result == 0);

    assert_bool(variant_toUInt8(&result, NULL) == false);

    assert_bool(variant_uint8(NULL) == 0);
    assert_bool(variant_uint8(&variant_unknown) == 0);

    sahtest_success();
}

int variant_null_unknown_toUInt16() {
    uint16_t result = 0;

    assert_bool(variant_toUInt16(&result, &variant_unknown) == false);
    assert_bool(result == 0);

    assert_bool(variant_toUInt16(&result, NULL) == false);

    assert_bool(variant_uint16(NULL) == 0);
    assert_bool(variant_uint16(&variant_unknown) == 0);

    sahtest_success();
}

int variant_null_unknown_toUInt32() {
    uint32_t result = 0;

    assert_bool(variant_toUInt32(&result, &variant_unknown) == false);
    assert_bool(result == 0);

    assert_bool(variant_toUInt32(&result, NULL) == false);

    assert_bool(variant_uint32(NULL) == 0);
    assert_bool(variant_uint32(&variant_unknown) == 0);

    sahtest_success();
}

int variant_null_unknown_toUInt64() {
    uint64_t result = 0;

    assert_bool(variant_toUInt64(&result, &variant_unknown) == false);
    assert_bool(result == 0);

    assert_bool(variant_toUInt64(&result, NULL) == false);

    assert_bool(variant_uint64(NULL) == 0);
    assert_bool(variant_uint64(&variant_unknown) == 0);

    sahtest_success();
}

int variant_null_unknown_toBool() {
    bool result = false;

    assert_bool(variant_toBool(&result, &variant_unknown) == false);
    assert_bool(result == false);

    assert_bool(variant_toBool(&result, NULL) == false);

    assert_bool(variant_bool(NULL) == false);
    assert_bool(variant_bool(&variant_unknown) == false);

    sahtest_success();
}

int variant_null_unknown_toFd() {
    int result = 0;

    assert_bool(variant_toFd(&result, &variant_unknown) == false);

    assert_bool(variant_toFd(&result, NULL) == false);

    assert_bool(variant_fd(NULL) == -1);
    assert_bool(variant_fd(&variant_unknown) == -1);

    sahtest_success();
}

int variant_null_unknown_toDouble() {
    double result = 0;

    assert_bool(variant_toDouble(&result, &variant_unknown) == false);
    assert_bool(result == 0);
    assert_bool(variant_toDouble(&result, NULL) == false);
    assert_bool(result == 0);

    result = variant_double(&variant_unknown);
    assert_bool(result == 0);
    result = variant_double(NULL);
    assert_bool(result == 0);

    sahtest_success();
}

int variant_null_unknown_toList() {
    variant_list_t result;
    variant_list_t* result_list = NULL;
    variant_list_initialize(&result);

    assert_bool(variant_toList(&result, &variant_unknown) == true);
    assert_bool(variant_list_size(&result) == 1);
    assert_bool(variant_toList(&result, NULL) == false);
    assert_bool(variant_list_size(&result) == 1);

    result_list = variant_list(&variant_unknown);
    assert_bool(result_list != NULL);
    assert_bool(variant_list_size(result_list) == 1);
    variant_list_clear(result_list);
    free(result_list);

    result_list = variant_list(NULL);
    assert_bool(result_list == NULL);

    variant_list_clear(&result);
    variant_list_clear(result_list);
    free(result_list);

    sahtest_success();
}

int variant_null_unknown_toMap() {
    variant_map_t result;
    variant_map_t* result_map = NULL;
    variant_map_initialize(&result);

    assert_bool(variant_toMap(&result, &variant_unknown) == false);
    assert_bool(variant_map_size(&result) == 0);
    assert_bool(variant_toMap(&result, NULL) == false);
    assert_bool(variant_map_size(&result) == 0);

    result_map = variant_map(&variant_unknown);
    assert_bool(result_map == NULL);
    result_map = variant_map(NULL);
    assert_bool(result_map == NULL);

    sahtest_success();
}
