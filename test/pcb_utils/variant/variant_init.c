/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <fcntl.h>
#include <errno.h>

#include <sahtest/sahtest.h>
#include <pcb/common/error.h>
#include <pcb/utils/variant.h>
#include <pcb/utils/variant_list.h>
#include <pcb/utils/variant_map.h>

#include "mock.h"

static variant_t variant;

void variant_init_setup() {
    variant_initialize(&variant, variant_type_unknown);
}

void variant_init_teardown() {
    variant_cleanup(&variant);
}

int variant_init_using_initialize() {
    assert_bool(variant.type == variant_type_unknown);
    assert_bool(variant.data.ui64 == 0);

    assert_bool(variant_initialize(&variant, variant_type_string));
    assert_bool(variant.type == variant_type_string);
    assert_bool(variant.data.ui64 == 0);

    assert_bool(variant_initialize(&variant, variant_type_int8));
    assert_bool(variant.type == variant_type_int8);
    assert_bool(variant.data.ui64 == 0);

    assert_bool(variant_initialize(&variant, variant_type_int16));
    assert_bool(variant.type == variant_type_int16);
    assert_bool(variant.data.ui64 == 0);

    assert_bool(variant_initialize(&variant, variant_type_int32));
    assert_bool(variant.type == variant_type_int32);
    assert_bool(variant.data.ui64 == 0);

    assert_bool(variant_initialize(&variant, variant_type_int64));
    assert_bool(variant.type == variant_type_int64);
    assert_bool(variant.data.ui64 == 0);

    assert_bool(variant_initialize(&variant, variant_type_uint8));
    assert_bool(variant.type == variant_type_uint8);
    assert_bool(variant.data.ui64 == 0);

    assert_bool(variant_initialize(&variant, variant_type_uint16));
    assert_bool(variant.type == variant_type_uint16);
    assert_bool(variant.data.ui64 == 0);

    assert_bool(variant_initialize(&variant, variant_type_uint32));
    assert_bool(variant.type == variant_type_uint32);
    assert_bool(variant.data.ui64 == 0);

    assert_bool(variant_initialize(&variant, variant_type_uint64));
    assert_bool(variant.type == variant_type_uint64);
    assert_bool(variant.data.ui64 == 0);

    assert_bool(variant_initialize(&variant, variant_type_bool));
    assert_bool(variant.type == variant_type_bool);
    assert_bool(variant.data.ui64 == 0);

    assert_bool(variant_initialize(&variant, variant_type_double));
    assert_bool(variant.type == variant_type_double);
    assert_bool(variant.data.ui64 == 0);

    assert_bool(variant_initialize(&variant, variant_type_date_time));
    assert_bool(variant.type == variant_type_date_time);
    assert_bool(variant.data.dt != NULL);

    assert_bool(variant_initialize(NULL, variant_type_date_time) == false);
    variant_cleanup(NULL);

    sahtest_success();
}

int variant_init_setChar() {
    assert_bool(variant_setChar(&variant, "Hallo world"));
    assert_bool(variant.type == variant_type_string);
    assert_bool(variant.data.str.buffer != NULL);
    assert_bool(variant.data.str.bufferSize == 12);

    assert_bool(variant_setChar(NULL, "Hallo world") == false);

    sahtest_success();
}

int variant_init_setString() {
    string_t string;
    string_initialize(&string, 0);
    string_fromChar(&string, "Hallo world");

    assert_bool(variant_setString(&variant, &string));
    assert_bool(variant.type == variant_type_string);
    assert_bool(variant.data.str.buffer != NULL);
    assert_bool(variant.data.str.bufferSize == 12);

    assert_bool(variant_setString(NULL, &string) == false);

    string_cleanup(&string);

    sahtest_success();
}

int variant_init_setInt8() {
    assert_bool(variant_setInt8(&variant, 100));
    assert_bool(variant.type == variant_type_int8);
    assert_bool(variant.data.i8 == 100);

    assert_bool(variant_setInt8(NULL, 100) == false);

    sahtest_success();
}

int variant_init_setInt16() {
    assert_bool(variant_setInt16(&variant, 100));
    assert_bool(variant.type == variant_type_int16);
    assert_bool(variant.data.i16 == 100);

    assert_bool(variant_setInt16(NULL, 100) == false);

    sahtest_success();
}

int variant_init_setInt32() {
    assert_bool(variant_setInt32(&variant, 100));
    assert_bool(variant.type == variant_type_int32);
    assert_bool(variant.data.i32 == 100);

    assert_bool(variant_setInt32(NULL, 100) == false);

    sahtest_success();
}

int variant_init_setInt64() {
    assert_bool(variant_setInt64(&variant, 100));
    assert_bool(variant.type == variant_type_int64);
    assert_bool(variant.data.i64 == 100);

    assert_bool(variant_setInt64(NULL, 100) == false);

    sahtest_success();
}

int variant_init_setUInt8() {
    assert_bool(variant_setUInt8(&variant, 100));
    assert_bool(variant.type == variant_type_uint8);
    assert_bool(variant.data.ui8 == 100);

    assert_bool(variant_setUInt8(NULL, 100) == false);

    sahtest_success();
}

int variant_init_setUInt16() {
    assert_bool(variant_setUInt16(&variant, 100));
    assert_bool(variant.type == variant_type_uint16);
    assert_bool(variant.data.ui16 == 100);

    assert_bool(variant_setUInt16(NULL, 100) == false);

    sahtest_success();
}

int variant_init_setUInt32() {
    assert_bool(variant_setUInt32(&variant, 100));
    assert_bool(variant.type == variant_type_uint32);
    assert_bool(variant.data.i32 == 100);

    assert_bool(variant_setUInt32(NULL, 100) == false);

    sahtest_success();
}

int variant_init_setUInt64() {
    assert_bool(variant_setUInt64(&variant, 100));
    assert_bool(variant.type == variant_type_uint64);
    assert_bool(variant.data.i64 == 100);

    assert_bool(variant_setUInt64(NULL, 100) == false);

    sahtest_success();
}

int variant_init_setBool() {
    assert_bool(variant_setBool(&variant, true));
    assert_bool(variant.type == variant_type_bool);
    assert_bool(variant.data.b == true);

    assert_bool(variant_setBool(NULL, true) == false);

    sahtest_success();
}

int variant_init_setDouble() {
    assert_bool(variant_setDouble(&variant, 10.25));
    assert_bool(variant.type == variant_type_double);
    assert_bool(variant.data.d == 10.25);

    assert_bool(variant_setDouble(NULL, 10.25) == false);

    sahtest_success();
}

int variant_init_setDateTime() {
    time_t now;
    time(&now);
    struct tm* t = gmtime(&now);

    assert_bool(variant_setDateTime(&variant, t) == true);
    assert_bool(variant.type == variant_type_date_time);
    assert_bool(variant.data.dt != NULL);
    assert_bool(memcmp(t, variant.data.dt, sizeof(struct tm)) == 0);

    assert_bool(variant_setDateTime(NULL, t) == false);

    sahtest_success();
}

int variant_init_setListMove() {
    variant_list_t list;
    assert_bool(variant_list_initialize(&list) == true);
    variant_list_addUInt32(&list, 1);
    variant_list_addUInt32(&list, 2);

    assert_bool(variant_setListMove(&variant, &list) == true);
    assert_bool(variant.type == variant_type_array);
    assert_bool(variant.data.vl != NULL);
    assert_bool(variant_list_size(variant.data.vl) == 2);
    assert_bool(variant_list_size(&list) == 0);

    assert_bool(variant_setListMove(NULL, &list) == false);
    variant_list_cleanup(&list);

    sahtest_success();
}

int variant_init_setListCopy() {
    variant_list_t list;
    assert_bool(variant_list_initialize(&list) == true);
    variant_list_addUInt32(&list, 1);
    variant_list_addUInt32(&list, 2);

    assert_bool(variant_setListCopy(&variant, &list) == true);
    assert_bool(variant.type == variant_type_array);
    assert_bool(variant.data.vl != NULL);
    assert_bool(variant_list_size(variant.data.vl) == 2);
    assert_bool(variant_list_size(&list) == 2);

    assert_bool(variant_setListCopy(NULL, &list) == false);
    variant_list_cleanup(&list);

    sahtest_success();
}

int variant_init_setMapMove() {
    variant_map_t map;
    assert_bool(variant_map_initialize(&map) == true);
    variant_map_addUInt32(&map, "1", 1);
    variant_map_addUInt32(&map, "2", 2);

    assert_bool(variant_setMapMove(&variant, &map) == true);
    assert_bool(variant.type == variant_type_map);
    assert_bool(variant.data.vm != NULL);
    assert_bool(variant_map_size(variant.data.vm) == 2);
    assert_bool(variant_map_size(&map) == 0);

    assert_bool(variant_setMapMove(NULL, &map) == false);
    variant_map_cleanup(&map);

    sahtest_success();
}

int variant_init_setMapCopy() {
    variant_map_t map;
    assert_bool(variant_map_initialize(&map) == true);
    variant_map_addUInt32(&map, "1", 1);
    variant_map_addUInt32(&map, "2", 2);

    assert_bool(variant_setMapCopy(&variant, &map) == true);
    assert_bool(variant.type == variant_type_map);
    assert_bool(variant.data.vm != NULL);
    assert_bool(variant_map_size(variant.data.vm) == 2);
    assert_bool(variant_map_size(&map) == 2);

    assert_bool(variant_setMapCopy(NULL, &map) == false);
    variant_map_cleanup(&map);

    sahtest_success();
}

int variant_init_setFd() {
    mock_add_expectation(fcntl)->fd = 11;
    mock_add_expectation(fcntl)->cmd = F_GETFD;
    mock_add_expectation(fcntl)->retval = 0;

    mock_add_expectation(fcntl)->fd = 12;
    mock_add_expectation(fcntl)->cmd = F_GETFD;
    mock_add_expectation(fcntl)->retval = -1;
    mock_add_expectation(fcntl)->error = 0;

    mock_add_expectation(fcntl)->fd = 12;
    mock_add_expectation(fcntl)->cmd = F_GETFD;
    mock_add_expectation(fcntl)->retval = -1;
    mock_add_expectation(fcntl)->error = EBADF;

    assert_bool(variant_setFd(&variant, 11) == true);
    assert_bool(variant.type == variant_type_file_descriptor);
    assert_bool(variant.data.fd == 11);

    assert_bool(variant_setFd(&variant, 12) == true);
    assert_bool(variant.type == variant_type_file_descriptor);
    assert_bool(variant.data.fd == 12);

    assert_bool(variant_setFd(&variant, 12) == false);
    assert_bool(pcb_error == EBADF);
    assert_bool(variant.type == variant_type_file_descriptor);
    assert_bool(variant.data.fd == -1);

    assert_bool(variant_setFd(NULL, 10) == false);

    assert_bool(mock_verify_expectations(fcntl) == true);

    sahtest_success();
}

int variant_init_setByteArray() {
    char data[10] = "abcdefghij";

    assert_bool(variant_setByteArray(&variant, data, 10) == true);
    assert_bool(variant.type == variant_type_byte_array);
    assert_bool(variant.data.ba != NULL);
    assert_bool(variant.data.ba->size == 10);

    assert_bool(variant_setByteArray(&variant, data, 0) == true);
    assert_bool(variant.type == variant_type_byte_array);
    assert_bool(variant.data.ba != NULL);
    assert_bool(variant.data.ba->data == NULL);
    assert_bool(variant.data.ba->size == 0);

    assert_bool(variant_setByteArray(NULL, data, 10) == false);

    sahtest_success();
}
