/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <setjmp.h>
#include <stdarg.h>
#include <string.h>
#include <cmocka.h>
#include <pcb/utils/variant.h>
#include <pcb/utils/variant_list.h>
#include <pcb/utils/variant_map.h>

void variant_bool_tostring(void** state) {
    string_t result;
    string_t* string = NULL;
    string_initialize(&result, 0);

    assert_true(variant_toString(&result, variant_true));
    assert_string_equal(string_buffer(&result), "1");

    assert_true(variant_toString(&result, variant_false));
    assert_string_equal(string_buffer(&result), "0");

    string = variant_string(variant_true);
    assert_non_null(string);
    assert_string_equal(string_buffer(string), "1");
    string_cleanup(string);
    free(string);

    string = variant_string(variant_false);
    assert_non_null(string);
    assert_string_equal(string_buffer(string), "0");
    string_cleanup(string);
    free(string);

    string_cleanup(&result);
}

void variant_bool_tojson(void** state) {
    string_t result;
    string_initialize(&result, 0);

    assert_true(variant_toJSON(&result, variant_true));
    assert_string_equal(string_buffer(&result), "true");

    assert_true(variant_toJSON(&result, variant_false));
    assert_string_equal(string_buffer(&result), "false");

    string_cleanup(&result);
}

void variant_bool_toChar(void* state) {
    char* result = NULL;

    assert_true(variant_toChar(&result, variant_true));
    assert_non_null(result);
    assert_string_equal(result, "1");
    free(result);
    result = variant_char(variant_true);
    assert_non_null(result);
    assert_string_equal(result, "1");
    free(result);

    assert_true(variant_toChar(&result, variant_false));
    assert_non_null(result);
    assert_string_equal(result, "0");
    free(result);
    result = variant_char(variant_false);
    assert_non_null(result);
    assert_string_equal(result, "0");
    free(result);

}

void variant_bool_toInt8(void* state) {
    int8_t result = 0;

    assert_true(variant_toInt8(&result, variant_true));
    assert_int_equal(result, 1);
    result = variant_int8(variant_true);
    assert_int_equal(result, 1);

    assert_true(variant_toInt8(&result, variant_false));
    assert_int_equal(result, 0);
    result = variant_int8(variant_false);
    assert_int_equal(result, 0);

}

void variant_bool_toInt16(void** state) {
    int16_t result = 0;

    assert_true(variant_toInt16(&result, variant_true));
    assert_int_equal(result, 1);
    result = variant_int16(variant_true);
    assert_int_equal(result, 1);

    assert_true(variant_toInt16(&result, variant_false));
    assert_int_equal(result, 0);
    result = variant_int16(variant_false);
    assert_int_equal(result, 0);

}

void variant_bool_toInt32(void** state) {
    int32_t result = 0;

    assert_true(variant_toInt32(&result, variant_true));
    assert_int_equal(result, 1);
    result = variant_int32(variant_true);
    assert_int_equal(result, 1);

    assert_true(variant_toInt32(&result, variant_false));
    assert_int_equal(result, 0);
    result = variant_int32(variant_false);
    assert_int_equal(result, 0);

}

void variant_bool_toInt64(void** state) {
    int64_t result = 0;

    assert_true(variant_toInt64(&result, variant_true));
    assert_int_equal(result, 1);
    result = variant_int64(variant_true);
    assert_int_equal(result, 1);

    assert_true(variant_toInt64(&result, variant_false));
    assert_int_equal(result, 0);
    result = variant_int64(variant_false);
    assert_int_equal(result, 0);

}

void variant_bool_toUInt8(void** state) {
    uint8_t result = 0;

    assert_true(variant_toUInt8(&result, variant_true));
    assert_int_equal(result, 1);
    result = variant_uint8(variant_true);
    assert_int_equal(result, 1);

    assert_true(variant_toUInt8(&result, variant_false));
    assert_int_equal(result, 0);
    result = variant_uint8(variant_false);
    assert_int_equal(result, 0);

}

void variant_bool_toUInt16(void** state) {
    uint16_t result = 0;

    assert_true(variant_toUInt16(&result, variant_true));
    assert_int_equal(result, 1);
    result = variant_uint16(variant_true);
    assert_int_equal(result, 1);

    assert_true(variant_toUInt16(&result, variant_false));
    assert_int_equal(result, 0);
    result = variant_uint16(variant_false);
    assert_int_equal(result, 0);

}

void variant_bool_toUInt32(void** state) {
    uint32_t result = 0;

    assert_true(variant_toUInt32(&result, variant_true));
    assert_int_equal(result, 1);
    result = variant_uint32(variant_true);
    assert_int_equal(result, 1);

    assert_true(variant_toUInt32(&result, variant_false));
    assert_int_equal(result, 0);
    result = variant_uint32(variant_false);
    assert_int_equal(result, 0);

}

void variant_bool_toUInt64(void** state) {
    uint64_t result = 0;

    assert_true(variant_toUInt64(&result, variant_true));
    assert_int_equal(result, 1);
    result = variant_uint64(variant_true);
    assert_int_equal(result, 1);

    assert_true(variant_toUInt64(&result, variant_false));
    assert_int_equal(result, 0);
    result = variant_uint64(variant_false);
    assert_int_equal(result, 0);

}

void variant_bool_toBool(void** state) {
    bool result = false;

    assert_true(variant_toBool(&result, variant_true));
    assert_true(result);
    result = variant_bool(variant_true);
    assert_true(result);

    assert_true(variant_toBool(&result, variant_false));
    assert_false(result);
    result = variant_bool(variant_false);
    assert_false(result);

}

void variant_bool_toFd(void** state) {
    int result = 0;

    assert_false(variant_toFd(&result, variant_true));
    assert_false(variant_toFd(&result, variant_false));

    assert_true(variant_fd(variant_true) == -1);
    assert_true(variant_fd(variant_false) == -1);

}

void variant_bool_toDouble(void** state) {
    double result = 0;

    assert_true(variant_toDouble(&result, variant_true));
    assert_int_equal(result, 1);
    result = variant_double(variant_true);
    assert_int_equal(result, 1);

    assert_true(variant_toDouble(&result, variant_false));
    assert_int_equal(result, 0);
    result = variant_double(variant_false);
    assert_int_equal(result, 0);

}

void variant_bool_toList(void** state) {
    variant_list_t result;
    variant_list_t* result_list = NULL;
    variant_list_initialize(&result);

    assert_true(variant_toList(&result, variant_true));
    assert_int_equal(variant_list_size(&result), 1);
    result_list = variant_list(variant_true);
    assert_non_null(result_list);
    assert_int_equal(variant_list_size(result_list), 1);

    variant_list_clear(&result);
    variant_list_clear(result_list);
    free(result_list);

}

void variant_bool_toMap(void** state) {
    variant_map_t result;
    variant_map_t* result_map = NULL;
    variant_map_initialize(&result);

    assert_false(variant_toMap(&result, variant_true));
    assert_int_equal(variant_map_size(&result), 0);
    result_map = variant_map(variant_true);
    assert_null(result_map);

}
