/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef VARIANT_COMPARE_H
#define VARIANT_COMPARE_H

#include <sahtest/define.h>

void variant_compare_setup();
void variant_compare_teardown();

int variant_compare_null_null();
int variant_compare_null_variant();
int variant_compare_variant_null();

int variant_compare_unknown_unknown();
int variant_compare_unknown_variant();

int variant_compare_int8_unknown();
int variant_compare_int8_null();
int variant_compare_int8_smaller_variant();
int variant_compare_int8_equal_variant();
int variant_compare_int8_bigger_variant();

int variant_compare_int16_unknown();
int variant_compare_int16_null();
int variant_compare_int16_smaller_variant();
int variant_compare_int16_equal_variant();
int variant_compare_int16_bigger_variant();

int variant_compare_int32_unknown();
int variant_compare_int32_null();
int variant_compare_int32_smaller_variant();
int variant_compare_int32_equal_variant();
int variant_compare_int32_bigger_variant();

int variant_compare_int64_unknown();
int variant_compare_int64_null();
int variant_compare_int64_smaller_variant();
int variant_compare_int64_equal_variant();
int variant_compare_int64_bigger_variant();

int variant_compare_uint8_unknown();
int variant_compare_uint8_null();
int variant_compare_uint8_smaller_variant();
int variant_compare_uint8_equal_variant();
int variant_compare_uint8_bigger_variant();

int variant_compare_uint16_unknown();
int variant_compare_uint16_null();
int variant_compare_uint16_smaller_variant();
int variant_compare_uint16_equal_variant();
int variant_compare_uint16_bigger_variant();

int variant_compare_uint32_unknown();
int variant_compare_uint32_null();
int variant_compare_uint32_smaller_variant();
int variant_compare_uint32_equal_variant();
int variant_compare_uint32_bigger_variant();

int variant_compare_uint64_unknown();
int variant_compare_uint64_null();
int variant_compare_uint64_smaller_variant();
int variant_compare_uint64_equal_variant();
int variant_compare_uint64_bigger_variant();

int variant_compare_char_unknown();
int variant_compare_char_null();
int variant_compare_char_smaller_variant();
int variant_compare_char_equal_variant();
int variant_compare_char_bigger_variant();

int variant_compare_initialized_string_initialized_string();

sahtest_group_begin(var_compare, variant_compare_setup, variant_compare_teardown)
sahtest_add_test(variant_compare_null_null, "Compare NULL with NULL")
sahtest_add_test(variant_compare_null_variant, "Compare NULL with variant")
sahtest_add_test(variant_compare_variant_null, "Compare variant with NULL")
sahtest_add_test(variant_compare_unknown_unknown, "Compare unknown variant with unknown variant")
sahtest_add_test(variant_compare_unknown_variant, "Compare unknwon with variant")
sahtest_add_test(variant_compare_int8_unknown, "Compare int8 with unknown")
sahtest_add_test(variant_compare_int8_null, "Compare int8 with NULL")
sahtest_add_test(variant_compare_int8_smaller_variant, "Compare int8 with smaler variant")
sahtest_add_test(variant_compare_int8_equal_variant, "Compare int8 with equal variant")
sahtest_add_test(variant_compare_int8_bigger_variant, "Compare int8 with bigger variant")
sahtest_add_test(variant_compare_int16_unknown, "Compare int16 with unknown")
sahtest_add_test(variant_compare_int16_null, "Compare int16 with NULL")
sahtest_add_test(variant_compare_int16_smaller_variant, "Compare int16 with smaler variant")
sahtest_add_test(variant_compare_int16_equal_variant, "Compare int16 with equal variant")
sahtest_add_test(variant_compare_int16_bigger_variant, "Compare int16 with bigger variant")
sahtest_add_test(variant_compare_int32_unknown, "Compare int32 with unknown")
sahtest_add_test(variant_compare_int32_null, "Compare int32 with NULL")
sahtest_add_test(variant_compare_int32_smaller_variant, "Compare int32 with smaler variant")
sahtest_add_test(variant_compare_int32_equal_variant, "Compare int32 with equal variant")
sahtest_add_test(variant_compare_int32_bigger_variant, "Compare int32 with bigger variant")
sahtest_add_test(variant_compare_int64_unknown, "Compare int64 with unknown")
sahtest_add_test(variant_compare_int64_null, "Compare int64 with NULL")
sahtest_add_test(variant_compare_int64_smaller_variant, "Compare int64 with smaler variant")
sahtest_add_test(variant_compare_int64_equal_variant, "Compare int64 with equal variant")
sahtest_add_test(variant_compare_int64_bigger_variant, "Compare int64 with bigger variant")
sahtest_add_test(variant_compare_uint8_unknown, "Compare uint8 with unknown")
sahtest_add_test(variant_compare_uint8_null, "Compare uint8 with NULL")
sahtest_add_test(variant_compare_uint8_smaller_variant, "Compare uint8 with smaler variant")
sahtest_add_test(variant_compare_uint8_equal_variant, "Compare uint8 with equal variant")
sahtest_add_test(variant_compare_uint8_bigger_variant, "Compare uint8 with bigger variant")
sahtest_add_test(variant_compare_uint16_unknown, "Compare uint16 with unknown")
sahtest_add_test(variant_compare_uint16_null, "Compare uint16 with NULL")
sahtest_add_test(variant_compare_uint16_smaller_variant, "Compare uint16 with smaler variant")
sahtest_add_test(variant_compare_uint16_equal_variant, "Compare uint16 with equal variant")
sahtest_add_test(variant_compare_uint16_bigger_variant, "Compare uint16 with bigger variant")
sahtest_add_test(variant_compare_uint32_unknown, "Compare uint32 with unknown")
sahtest_add_test(variant_compare_uint32_null, "Compare uint32 with NULL")
sahtest_add_test(variant_compare_uint32_smaller_variant, "Compare uint32 with smaler variant")
sahtest_add_test(variant_compare_uint32_equal_variant, "Compare uint32 with equal variant")
sahtest_add_test(variant_compare_uint32_bigger_variant, "Compare uint32 with bigger variant")
sahtest_add_test(variant_compare_uint64_unknown, "Compare uint64 with unknown")
sahtest_add_test(variant_compare_uint64_null, "Compare uint64 with NULL")
sahtest_add_test(variant_compare_uint64_smaller_variant, "Compare uint64 with smaler variant")
sahtest_add_test(variant_compare_uint64_equal_variant, "Compare uint64 with equal variant")
sahtest_add_test(variant_compare_uint64_bigger_variant, "Compare uint64 with bigger variant")
sahtest_add_test(variant_compare_char_unknown, "Compare char with unknown")
sahtest_add_test(variant_compare_char_null, "Compare char with NULL")
sahtest_add_test(variant_compare_char_smaller_variant, "Compare char with smaler variant")
sahtest_add_test(variant_compare_char_equal_variant, "Compare char with equal variant")
sahtest_add_test(variant_compare_char_bigger_variant, "Compare char with bigger variant")
sahtest_add_test(variant_compare_initialized_string_initialized_string, "Compare initialized string with initialized string, see bug 33303")
sahtest_group_end()
#endif // VARIANT_COMPARE_H
