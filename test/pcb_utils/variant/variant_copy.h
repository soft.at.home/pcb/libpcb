/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef VARIANT_COPY_H
#define VARIANT_COPY_H

#include <sahtest/define.h>

void variant_copy_setup();
void variant_copy_teardown();

int variant_string_copy();
int variant_int8_copy();
int variant_int16_copy();
int variant_int32_copy();
int variant_int64_copy();
int variant_uint8_copy();
int variant_uint16_copy();
int variant_uint32_copy();
int variant_uint64_copy();
int variant_bool_copy();
int variant_double_copy();
int variant_date_time_copy();
int variant_list_copy();
int variant_map_copy();
int variant_fd_copy();
int variant_byte_array_copy();
int variant_null_copy();

sahtest_group_begin(var_copy, variant_copy_setup, variant_copy_teardown)
sahtest_add_test(variant_string_copy, "Test copying a string variant")
sahtest_add_test(variant_int8_copy, "Test copying a int8 variant")
sahtest_add_test(variant_int16_copy, "Test copying a int16 variant")
sahtest_add_test(variant_int32_copy, "Test copying a int32 variant")
sahtest_add_test(variant_int64_copy, "Test copying a int64 variant")
sahtest_add_test(variant_uint8_copy, "Test copying a uint8 variant")
sahtest_add_test(variant_uint16_copy, "Test copying a uint16 variant")
sahtest_add_test(variant_uint32_copy, "Test copying a uint32 variant")
sahtest_add_test(variant_uint64_copy, "Test copying a uint64 variant")
sahtest_add_test(variant_bool_copy, "Test copying a bool variant")
sahtest_add_test(variant_double_copy, "Test copying a double variant")
sahtest_add_test(variant_date_time_copy, "Test copying a date time variant")
sahtest_add_test(variant_list_copy, "Test copying a list variant")
sahtest_add_test(variant_map_copy, "Test copying a map variant")
sahtest_add_test(variant_fd_copy, "Test copying a file descriptor variant")
sahtest_add_test(variant_byte_array_copy, "Test copying a byte array variant")
sahtest_add_test(variant_null_copy, "Test copying with a null variant as source or destination")
sahtest_group_end()

#endif // VARIANT_COPY_H
