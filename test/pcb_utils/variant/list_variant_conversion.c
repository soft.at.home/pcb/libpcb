/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#define _DEFAULT_SOURCE
#define _BSD_SOURCE /* needed for timegm() */
#include <stdio.h>
#include <stdlib.h>
#include <setjmp.h>
#include <stdarg.h>
#include <string.h>
#include <inttypes.h>
#include <cmocka.h>
#include <pcb/utils/variant.h>
#include <pcb/utils/variant_list.h>
#include <pcb/utils/variant_map.h>

static variant_t variant_l;

int variant_list_conversion_setup(void** state) {
    variant_initialize(&variant_l, variant_type_array);
    variant_list_t* list = variant_da_list(&variant_l);
    variant_list_addBool(list, true);
    variant_list_addUInt32(list, 4096);
    variant_list_addChar(list, "Hallo World");
    struct tm devdate = {
        .tm_year = 2019 - 1900,     /* struct tm counts years since 1900 */
        .tm_mon = 7 - 1,            /* struct tm counts months from 0 (=January) */
        .tm_mday = 12,
        .tm_hour = 13,
        .tm_min = 13,
        .tm_sec = 32
    };
    pcb_datetime_t ts = { .datetime = devdate, .nanoseconds = 12345678UL };
    variant_list_addDateTimeExtended(list, &ts);
    return 0;
}

int variant_list_conversion_teardown(void** state) {
    variant_cleanup(&variant_l);
    return 0;
}

void variant_list_tostring(void** state) {
    string_t result;
    string_t* string = NULL;
    string_initialize(&result, 0);

    assert_true(variant_toString(&result, &variant_l));
    assert_string_equal(string_buffer(&result), "[1,4096,Hallo World,2019-07-12T13:13:32.012345Z]");

    string = variant_string(&variant_l);
    assert_non_null(string);
    assert_string_equal(string_buffer(string), "[1,4096,Hallo World,2019-07-12T13:13:32.012345Z]");
    string_cleanup(string);
    free(string);

    string_cleanup(&result);
}

void variant_list_tojson(void** state) {
    string_t result;
    string_initialize(&result, 0);

    assert_true(variant_toJSON(&result, &variant_l));
    assert_string_equal(string_buffer(&result), "[true,4096,\"Hallo World\",\"2019-07-12T13:13:32.012345Z\"]");

    string_cleanup(&result);
}

void variant_list_toChar(void** state) {
    char* result = NULL;

    assert_true(variant_toChar(&result, &variant_l));
    assert_string_equal(result, "[1,4096,Hallo World,2019-07-12T13:13:32.012345Z]");
    free(result);

    result = variant_char(&variant_l);
    assert_non_null(result);
    assert_string_equal(result, "[1,4096,Hallo World,2019-07-12T13:13:32.012345Z]");
    free(result);
}

void variant_list_toInt8(void** state) {
    int8_t result = 0;

    assert_false(variant_toInt8(&result, &variant_l));

    result = variant_int8(&variant_l);
    assert_int_equal(result, 0);


}

void variant_list_toInt16(void** state) {
    int16_t result = 0;

    assert_false(variant_toInt16(&result, &variant_l));

    result = variant_int16(&variant_l);
    assert_int_equal(result, 0);


}

void variant_list_toInt32(void** state) {
    int32_t result = 0;

    assert_false(variant_toInt32(&result, &variant_l));

    result = variant_int32(&variant_l);
    assert_int_equal(result, 0);


}

void variant_list_toInt64(void** state) {
    int64_t result = 0;

    assert_false(variant_toInt64(&result, &variant_l));

    result = variant_int64(&variant_l);
    assert_int_equal(result, 0);


}

void variant_list_toUInt8(void** state) {
    uint8_t result = 0;

    assert_false(variant_toUInt8(&result, &variant_l));

    result = variant_uint8(&variant_l);
    assert_int_equal(result, 0);


}

void variant_list_toUInt16(void** state) {
    uint16_t result = 0;

    assert_false(variant_toUInt16(&result, &variant_l));

    result = variant_uint16(&variant_l);
    assert_int_equal(result, 0);


}

void variant_list_toUInt32(void** state) {
    uint32_t result = 0;

    assert_false(variant_toUInt32(&result, &variant_l));

    result = variant_uint32(&variant_l);
    assert_int_equal(result, 0);


}

void variant_list_toUInt64(void** state) {
    uint64_t result = 0;

    assert_false(variant_toUInt64(&result, &variant_l));

    result = variant_uint64(&variant_l);
    assert_int_equal(result, 0);


}

void variant_list_toBool(void** state) {
    bool result = true;

    assert_false(variant_toBool(&result, &variant_l));

    result = variant_bool(&variant_l);
    assert_false(result);


}

void variant_list_toFd(void** state) {
    int result = true;

    assert_false(variant_toFd(&result, &variant_l));

    result = variant_fd(&variant_l);
    assert_true(result == -1);


}

void variant_list_toDouble(void** state) {
    double result = 0;

    assert_false(variant_toDouble(&result, &variant_l));

    result = variant_double(&variant_l);
    assert_int_equal(result, 0);


}

void variant_list_toList(void** state) {
    variant_list_t result;
    variant_list_t* list = NULL;
    variant_list_initialize(&result);

    assert_true(variant_toList(&result, &variant_l));
    assert_int_equal(variant_list_size(&result), 4);
    variant_list_cleanup(&result);

    list = variant_list(&variant_l);
    assert_non_null(list);
    assert_int_equal(variant_list_size(list), 4);
    variant_list_cleanup(list);
    free(list);


}

void variant_list_toMap(void** state) {
    variant_map_t result;
    variant_map_t* list = NULL;
    variant_map_initialize(&result);

    assert_false(variant_toMap(&result, &variant_l));
    variant_map_cleanup(&result);

    list = variant_map(&variant_l);
    assert_null(list);


}

void variant_list_toTime(void** state) {
    struct tm result1, reference1;
    pcb_datetime_t result2, reference2;
    memset(&result1, 0, sizeof result1);
    memset(&result2, 0, sizeof result2);
    memset(&reference1, 0, sizeof reference1);
    memset(&reference2, 0, sizeof reference2);

    assert_false(variant_toDateTime(&result1, &variant_l));
    assert_memory_equal(&result1, &reference1, sizeof result1);

    assert_false(variant_toDateTimeExtended(&result2, &variant_l));
    assert_memory_equal(&result2, &reference2, sizeof result2);
}
