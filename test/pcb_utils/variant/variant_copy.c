/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <sahtest/sahtest.h>
#include <pcb/utils/variant.h>
#include <pcb/utils/variant_list.h>
#include <pcb/utils/variant_map.h>

static variant_t dest;
static variant_t src;

void variant_copy_setup() {
    variant_initialize(&dest, variant_type_unknown);
    variant_initialize(&src, variant_type_unknown);
}

void variant_copy_teardown() {
    variant_cleanup(&dest);
    variant_cleanup(&src);
}

int variant_string_copy() {
    char* txt = NULL;

    assert_bool(variant_setChar(&src, "Hallo world") == true);

    variant_copy(&dest, &src);
    assert_bool(variant_type(&dest) == variant_type_string);
    txt = variant_char(&dest);
    assert_bool(strcmp(txt, "Hallo world") == 0);
    free(txt);

    sahtest_success();
}

int variant_int8_copy() {
    assert_bool(variant_setInt8(&src, 127) == true);

    variant_copy(&dest, &src);

    assert_bool(variant_type(&dest) == variant_type_int8);
    assert_bool(variant_int8(&dest) == 127);

    sahtest_success();
}

int variant_int16_copy() {
    assert_bool(variant_setInt16(&src, 127) == true);

    variant_copy(&dest, &src);

    assert_bool(variant_type(&dest) == variant_type_int16);
    assert_bool(variant_int16(&dest) == 127);

    sahtest_success();
}

int variant_int32_copy() {
    assert_bool(variant_setInt32(&src, 127) == true);

    variant_copy(&dest, &src);

    assert_bool(variant_type(&dest) == variant_type_int32);
    assert_bool(variant_int32(&dest) == 127);

    sahtest_success();
}

int variant_int64_copy() {
    assert_bool(variant_setInt64(&src, 127) == true);

    variant_copy(&dest, &src);

    assert_bool(variant_type(&dest) == variant_type_int64);
    assert_bool(variant_int64(&dest) == 127);

    sahtest_success();
}

int variant_uint8_copy() {
    assert_bool(variant_setUInt8(&src, 127) == true);

    variant_copy(&dest, &src);

    assert_bool(variant_type(&dest) == variant_type_uint8);
    assert_bool(variant_uint8(&dest) == 127);

    sahtest_success();
}

int variant_uint16_copy() {
    assert_bool(variant_setUInt16(&src, 127) == true);

    variant_copy(&dest, &src);

    assert_bool(variant_type(&dest) == variant_type_uint16);
    assert_bool(variant_uint16(&dest) == 127);

    sahtest_success();
}

int variant_uint32_copy() {
    assert_bool(variant_setUInt32(&src, 127) == true);

    variant_copy(&dest, &src);

    assert_bool(variant_type(&dest) == variant_type_uint32);
    assert_bool(variant_uint32(&dest) == 127);

    sahtest_success();
}

int variant_uint64_copy() {
    assert_bool(variant_setUInt64(&src, 127) == true);

    variant_copy(&dest, &src);

    assert_bool(variant_type(&dest) == variant_type_uint64);
    assert_bool(variant_uint64(&dest) == 127);

    sahtest_success();
}

int variant_bool_copy() {
    assert_bool(variant_setBool(&src, true) == true);

    variant_copy(&dest, &src);

    assert_bool(variant_type(&dest) == variant_type_bool);
    assert_bool(variant_bool(&dest) == true);

    sahtest_success();
}

int variant_double_copy() {
    assert_bool(variant_setDouble(&src, 100.25) == true);

    variant_copy(&dest, &src);

    assert_bool(variant_type(&dest) == variant_type_double);
    assert_bool(variant_double(&dest) == 100.25);

    sahtest_success();
}

int variant_date_time_copy() {
    time_t now;
    time(&now);
    struct tm* t = gmtime(&now);

    assert_bool(variant_setDateTime(&src, t) == true);
    variant_copy(&dest, &src);
    assert_bool(variant_type(&dest) == variant_type_date_time);

    sahtest_success();
}

int variant_list_copy() {
    assert_bool(variant_initialize(&src, variant_type_array));

    variant_list_t* list = variant_da_list(&src);
    assert_bool(list != NULL);

    variant_list_addUInt32(list, 1);
    variant_list_addUInt32(list, 2);

    variant_copy(&dest, &src);

    assert_bool(variant_list_size(list) == 2);
    assert_bool(variant_type(&dest) == variant_type_array);
    list = variant_da_list(&dest);
    assert_bool(list != NULL);
    assert_bool(variant_list_size(list) == 2);

    sahtest_success();
}

int variant_map_copy() {
    assert_bool(variant_initialize(&src, variant_type_map));

    variant_map_t* map = variant_da_map(&src);
    assert_bool(map != NULL);

    variant_map_addUInt32(map, "1", 1);
    variant_map_addUInt32(map, "2", 2);

    variant_copy(&dest, &src);

    assert_bool(variant_map_size(map) == 2);
    assert_bool(variant_type(&dest) == variant_type_map);
    map = variant_da_map(&dest);
    assert_bool(map != NULL);
    assert_bool(variant_map_size(map) == 2);

    sahtest_success();
}

int variant_fd_copy() {
    assert_bool(variant_initialize(&src, variant_type_file_descriptor));

    variant_copy(&dest, &src);
    assert_bool(variant_type(&dest) == variant_type_file_descriptor);
    assert_bool(variant_fd(&dest) == -1);

    sahtest_success();
}

int variant_byte_array_copy() {
    char data[10] = "abcdefghij";
    variant_setByteArray(&src, data, 10);
    uint32_t size = 0;

    variant_copy(&dest, &src);
    assert_bool(variant_type(&dest) == variant_type_byte_array);
    void* d2 = variant_byteArray(&dest, &size);
    assert_bool(size == 10);
    assert_bool(memcmp(d2, data, size) == 0);

    free(d2);
    sahtest_success();
}

int variant_null_copy() {
    variant_copy(NULL, &src);
    variant_copy(&dest, NULL);

    sahtest_success();
}
