/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef UINT32_VARIANT_CONVERSION_H
#define UINT32_VARIANT_CONVERSION_H

#include <sahtest/define.h>

void variant_uint32_conversion_setup();
void variant_uint32_conversion_teardown();

int variant_uint32_tostring();
int variant_uint32_tojson();
int variant_uint32_toChar();
int variant_uint32_toInt8();
int variant_uint32_toInt16();
int variant_uint32_toInt32();
int variant_uint32_toInt64();
int variant_uint32_toUInt8();
int variant_uint32_toUInt16();
int variant_uint32_toUInt32();
int variant_uint32_toUInt64();
int variant_uint32_toBool();

sahtest_group_begin(uint32_conversion, variant_uint32_conversion_setup, variant_uint32_conversion_teardown)
sahtest_add_test(variant_uint32_tostring, "Test converting a uint32 variant to a string")
sahtest_add_test(variant_uint32_tojson, "Test converting a uint32 variant to a json string")
sahtest_add_test(variant_uint32_toChar, "Test converting a uint32 variant to a char pointer")
sahtest_add_test(variant_uint32_toInt8, "Test converting a uint32 variant to an int8")
sahtest_add_test(variant_uint32_toInt16, "Test converting a uint32 variant to an int16")
sahtest_add_test(variant_uint32_toInt32, "Test converting a uint32 variant to an int32")
sahtest_add_test(variant_uint32_toInt64, "Test converting a uint32 variant to an int64")
sahtest_add_test(variant_uint32_toUInt8, "Test converting a uint32 variant to an uint8")
sahtest_add_test(variant_uint32_toUInt16, "Test converting a uint32 variant to an uint16")
sahtest_add_test(variant_uint32_toUInt32, "Test converting a uint32 variant to an uint32")
sahtest_add_test(variant_uint32_toUInt64, "Test converting a uint32 variant to an uint64")
sahtest_add_test(variant_uint32_toBool, "Test converting a uint32 variant to a bool")
sahtest_group_end()

#endif // UINT32_VARIANT_CONVERSION_H
