/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <setjmp.h>
#include <stdarg.h>
#include <string.h>
#include <inttypes.h>
#include <cmocka.h>
#include <pcb/utils/variant.h>
#include <pcb/utils/variant_list.h>
#include <pcb/utils/variant_map.h>
#include <assert.h>
#include <time.h>
#include <unistd.h>
#include <stdlib.h>
#include "linear_regression.h"
#include <math.h>

double correlation_coefficient(const sample_t* samples, size_t n) {
    // https://www.thoughtco.com/how-to-calculate-the-correlation-coefficient-3126228
    double sum_x = 0;
    double sum_y = 0;
    double sumsquare_x = 0;
    double sumsquare_y = 0;
    for(const sample_t* s = samples; s != samples + n; ++s) {
        sum_x += s->size;
        sum_y += s->time;
        sumsquare_x += s->size * s->size;
        sumsquare_y += s->time * s->time;
    }

    const double mean_x = sum_x / n;
    const double mean_y = sum_y / n;

    double sum_squared_dx = 0;
    double sum_squared_dy = 0;
    for(const sample_t* s = samples; s != samples + n; ++s) {
        const double dx = mean_x - s->size;
        sum_squared_dx += dx * dx;
        const double dy = mean_y - s->time;
        sum_squared_dy += dy * dy;
    }
    const double stddev_x = sqrt(sum_squared_dx / (n - 1));
    const double stddev_y = sqrt(sum_squared_dy / (n - 1));

    double std_x_y = 0;
    for(const sample_t* s = samples; s != samples + n; ++s) {
        const double std_x = (s->size - mean_x) / stddev_x;
        const double std_y = (s->time - mean_y) / stddev_y;
        std_x_y += std_x * std_y;
    }

    const double correlation_coefficient = std_x_y / (n - 1);

    return correlation_coefficient;
}

bool is_quasi_linear(const sample_t* samples, size_t n) {
    return correlation_coefficient(samples, n) > 0.99;
}