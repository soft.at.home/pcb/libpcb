/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <setjmp.h>
#include <stdarg.h>
#include <string.h>
#include <inttypes.h>
#include <cmocka.h>
#include <pcb/utils/variant.h>
#include <pcb/utils/variant_list.h>
#include <pcb/utils/variant_map.h>


static string_t result;
static variant_t unknown;
static variant_t int8;
static variant_t int16;
static variant_t int32;
static variant_t int64;
static variant_t uint8;
static variant_t uint16;
static variant_t uint32;
static variant_t uint64;
static variant_t f64;
static variant_t str;
static variant_t map;
static variant_t submap;
static variant_t list;
static variant_t sublist;
static variant_t datetime;
static variant_t datetime2;


int variant_print_setup(void** state) {
    string_initialize(&result, 0);
    variant_initialize(&int8, variant_type_unknown);
    variant_initialize(&int8, variant_type_int8);
    variant_initialize(&int16, variant_type_int16);
    variant_initialize(&int32, variant_type_int32);
    variant_initialize(&int64, variant_type_int64);
    variant_initialize(&uint8, variant_type_uint8);
    variant_initialize(&uint16, variant_type_uint16);
    variant_initialize(&uint32, variant_type_uint32);
    variant_initialize(&uint64, variant_type_uint64);
    variant_initialize(&f64, variant_type_double);
    variant_initialize(&str, variant_type_string);
    variant_initialize(&map, variant_type_map);
    variant_initialize(&submap, variant_type_map);
    variant_initialize(&list, variant_type_array);
    variant_initialize(&sublist, variant_type_array);
    variant_initialize(&datetime, variant_type_date_time);
    variant_initialize(&datetime2, variant_type_date_time);

    variant_setInt8(&int8, -128);
    variant_setInt16(&int16, -32768);
    variant_setInt32(&int32, -2147483648);
    variant_setInt64(&int64, -9223372036854775807LL);
    variant_setUInt8(&uint8, 255);
    variant_setUInt16(&uint16, 65535);
    variant_setUInt32(&uint32, 2147483647);
    variant_setUInt64(&uint64, 18446744073709551615ULL);
    variant_setDouble(&f64, -1.7E+308);
    variant_setChar(&str, "str");
    variant_map_addChar(variant_da_map(&map), "key0", "value0");
    variant_map_addChar(variant_da_map(&map), "key01", "value01");
    variant_map_addUInt32(variant_da_map(&submap), "num", 42);
    variant_map_addMapRef(variant_da_map(&map), "key2", variant_da_map(&submap));
    variant_list_addChar(variant_da_list(&list), "e0");
    variant_list_addChar(variant_da_list(&list), "element1");
    variant_list_addChar(variant_da_list(&sublist), "e2");
    variant_list_addInt8(variant_da_list(&sublist), 3);
    variant_list_addListCopy(variant_da_list(&list), variant_da_list(&sublist));

    time_t tim = 0xCAFE1234;
    struct tm tm;
    gmtime_r(&tim, &tm);
    variant_setDateTime(&datetime, &tm);
    pcb_datetime_t dt = { .datetime = tm, .nanoseconds = 123456789UL };
    variant_setDateTimeExtended(&datetime2, &dt);

/*
    variant_map_t *map = variant_da_map(&in);
    variant_map_addBool(map, "true", true);
    variant_map_addBool(map, "false", false);
    variant_map_addInt8(map, "int8", -8);
    variant_map_addInt16(map, "int16", -16);
    variant_map_addInt32(map, "int32", -32);
    variant_map_addInt64(map, "int64", -64);
    variant_map_addUInt8(map, "uint8", 8);
    variant_map_addUInt16(map, "uint16", 16);
    variant_map_addUInt32(map, "uint32", 32);
    variant_map_addUInt64(map, "uint64", 64);
    variant_map_addDouble(map, "double", 8.8);
    variant_map_addChar(map, "char", "char");
    //variant_map_addDateTime(map, "date", NULL);
 */
    return 0;
}

int variant_print_teardown(void** state) {
    string_cleanup(&result);
    variant_cleanup(&int8);
    variant_cleanup(&int16);
    variant_cleanup(&int32);
    variant_cleanup(&int64);
    variant_cleanup(&uint8);
    variant_cleanup(&uint16);
    variant_cleanup(&uint32);
    variant_cleanup(&uint64);
    variant_cleanup(&f64);
    variant_cleanup(&str);
    variant_cleanup(&map);
    variant_cleanup(&submap);
    variant_cleanup(&list);
    variant_cleanup(&sublist);
    variant_cleanup(&datetime);
    variant_cleanup(&datetime2);
    return 0;
}


#if 0
#define variant_print_multiline        0x01 /**< Separate list and map items by newlines and
                                                 start each line with proper indentation.*/
#define variant_print_spaces           0x02 /**< Insert spaces before/after delimiters.*/
#define variant_print_outline_colons   0x04 /**< Put all colons (or key/value delimiter of map
                                                 items) nicely under each other. This attribute
                                                 only makes sense if
                                                 @ref variant_print_multiline is specified
                                                 too.*/
#define variant_print_quote_keys       0x08 /**< Surround map keys with quotes and escape
                                                 special characters.*/
#define variant_print_quote_strings    0x10 /**< Surround string values with quotes and escape
                                                 special characters.*/
#define variant_print_bool_to_num      0x20 /**< Print booleans as 1 or 0 instead of true or
                                                 false.*/
#define variant_print_need_newline     0x40 /**< Insert a newline before actually printing the
                                                 variant if it is a map or a list.*/
#endif
void variant_print_nullptr(void** state) {
    assert_false(variant_print(&result, NULL, 0, NULL));
    assert_false(variant_print(NULL, variant_true, 0, NULL));
}

void variant_print_unknown(void** state) {
    assert_false(variant_print(&result, &unknown, 0, NULL));
}

void variant_print_true(void** state) {
    assert_true(variant_print(&result, variant_true, 0, NULL));
    assert_string_equal(string_buffer(&result), "true");

    assert_true(variant_print(&result, variant_true, variant_print_bool_to_num, NULL));
    assert_string_equal(string_buffer(&result), "1");
}

void variant_print_false(void** state) {
    assert_true(variant_print(&result, variant_false, 0, NULL));
    assert_string_equal(string_buffer(&result), "false");

    assert_true(variant_print(&result, variant_false, variant_print_bool_to_num, NULL));
    assert_string_equal(string_buffer(&result), "0");
}


void variant_print_int8(void** state) {
    assert_true(variant_print(&result, &int8, 0, NULL));
    assert_string_equal(string_buffer(&result), "-128");
}


void variant_print_int16(void** state) {
    assert_true(variant_print(&result, &int16, 0, NULL));
    assert_string_equal(string_buffer(&result), "-32768");
}


void variant_print_int32(void** state) {
    assert_true(variant_print(&result, &int32, 0, NULL));
    assert_string_equal(string_buffer(&result), "-2147483648");
}


void variant_print_int64(void** state) {
    assert_true(variant_print(&result, &int64, 0, NULL));
    assert_string_equal(string_buffer(&result), "-9223372036854775807");
}


void variant_print_uint8(void** state) {
    assert_true(variant_print(&result, &uint8, 0, NULL));
    assert_string_equal(string_buffer(&result), "255");
}


void variant_print_uint16(void** state) {
    assert_true(variant_print(&result, &uint16, 0, NULL));
    assert_string_equal(string_buffer(&result), "65535");
}


void variant_print_uint32(void** state) {
    assert_true(variant_print(&result, &uint32, 0, NULL));
    assert_string_equal(string_buffer(&result), "2147483647");
}


void variant_print_uint64(void** state) {
    assert_true(variant_print(&result, &uint64, 0, NULL));
    assert_string_equal(string_buffer(&result), "18446744073709551615");
}


void variant_print_double(void** state) {
    assert_true(variant_print(&result, &f64, 0, NULL));
    assert_string_equal(string_buffer(&result), "-1.700000e+308");
}


void variant_print_char(void** state) {
    assert_true(variant_print(&result, &str, 0, NULL));
    assert_string_equal(string_buffer(&result), "str");
}


void variant_print_map(void** state) {
    uint32_t attribute = 0;
    assert_true(variant_print(&result, &map, attribute, NULL));
    assert_string_equal(string_buffer(&result), "{key0:value0,key01:value01,key2:{num:42}}");

    attribute = variant_print_multiline;
    assert_true(variant_print(&result, &map, attribute, NULL));
    assert_string_equal(string_buffer(&result),
                        "{\n"
                        "    key0:value0,\n"
                        "    key01:value01,\n"
                        "    key2:\n"
                        "    {\n"
                        "        num:42\n"
                        "    }\n"
                        "}");

    attribute = variant_print_spaces;
    assert_true(variant_print(&result, &map, attribute, NULL));
    assert_string_equal(string_buffer(&result), "{ key0 : value0, key01 : value01, key2 : { num : 42 } }");

    attribute = variant_print_multiline | variant_print_outline_colons;
    assert_true(variant_print(&result, &map, attribute, NULL));
    assert_string_equal(string_buffer(&result),
                        "{\n"
                        "    key0 :value0,\n"
                        "    key01:value01,\n"
                        "    key2 :\n"
                        "    {\n"
                        "        num:42\n"
                        "    }\n"
                        "}");

    attribute = variant_print_quote_keys;
    assert_true(variant_print(&result, &map, attribute, NULL));
    assert_string_equal(string_buffer(&result), "{\"key0\":value0,\"key01\":value01,\"key2\":{\"num\":42}}");

    attribute = variant_print_quote_strings;
    assert_true(variant_print(&result, &map, attribute, NULL));
    assert_string_equal(string_buffer(&result), "{key0:\"value0\",key01:\"value01\",key2:{num:42}}");

    attribute = variant_print_multiline | variant_print_need_newline;
    assert_true(variant_print(&result, &map, attribute, NULL));
    assert_string_equal(string_buffer(&result),
                        "\n"
                        "{\n"
                        "    key0:value0,\n"
                        "    key01:value01,\n"
                        "    key2:\n"
                        "    {\n"
                        "        num:42\n"
                        "    }\n"
                        "}");


    const char* delimiters[] = { "<", "=", "|", ">", "(", "|", ")", NULL };
    attribute = 0;
    assert_true(variant_print(&result, &map, attribute, delimiters));
    assert_string_equal(string_buffer(&result), "<key0=value0|key01=value01|key2=<num=42>>");
}


void variant_print_list(void** state) {
    uint32_t attribute = 0;
    assert_true(variant_print(&result, &list, attribute, NULL));
    assert_string_equal(string_buffer(&result), "[e0,element1,[e2,3]]");

    attribute = variant_print_multiline;
    assert_true(variant_print(&result, &list, attribute, NULL));
    assert_string_equal(string_buffer(&result),
                        "[\n"
                        "    e0,\n"
                        "    element1,\n"
                        "    [\n"
                        "        e2,\n"
                        "        3\n"
                        "    ]\n"
                        "]");

    attribute = variant_print_spaces;
    assert_true(variant_print(&result, &list, attribute, NULL));
    assert_string_equal(string_buffer(&result), "[ e0, element1, [ e2, 3 ] ]");

    attribute = variant_print_multiline | variant_print_outline_colons;
    assert_true(variant_print(&result, &list, attribute, NULL));
    assert_string_equal(string_buffer(&result),
                        "[\n"
                        "    e0,\n"
                        "    element1,\n"
                        "    [\n"
                        "        e2,\n"
                        "        3\n"
                        "    ]\n"
                        "]");


    attribute = variant_print_quote_keys;
    assert_true(variant_print(&result, &list, attribute, NULL));
    assert_string_equal(string_buffer(&result), "[e0,element1,[e2,3]]");

    attribute = variant_print_quote_strings;
    assert_true(variant_print(&result, &list, attribute, NULL));
    assert_string_equal(string_buffer(&result), "[\"e0\",\"element1\",[\"e2\",3]]");

    attribute = variant_print_multiline | variant_print_need_newline;
    assert_true(variant_print(&result, &list, attribute, NULL));
    assert_string_equal(string_buffer(&result),
                        "\n"
                        "[\n"
                        "    e0,\n"
                        "    element1,\n"
                        "    [\n"
                        "        e2,\n"
                        "        3\n"
                        "    ]\n"
                        "]");

    const char* delimiters[] = { "<", "=", "|", ">", "(", "|", ")", NULL };
    attribute = 0;
    assert_true(variant_print(&result, &list, attribute, delimiters));
    assert_string_equal(string_buffer(&result), "(e0|element1|(e2|3))");
}

void variant_print_date(void** state) {
    assert_true(variant_print(&result, &datetime, 0, NULL));
    assert_string_equal(string_buffer(&result), "2077-12-02T05:27:16Z");
    assert_true(variant_print(&result, &datetime2, 0, NULL));
    assert_string_equal(string_buffer(&result), "2077-12-02T05:27:16.123456Z");
}

