/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <sahtest/sahtest.h>
#include <pcb/utils/variant.h>

static variant_t unknown_var;
static variant_t int8_var;
static variant_t int16_var;
static variant_t int16_var2;
static variant_t int32_var;
static variant_t int64_var;
static variant_t uint8_var;
static variant_t uint16_var;
static variant_t uint32_var;
static variant_t uint64_var;
static variant_t uint64_var2;
static variant_t char_var;

void variant_compare_setup() {
    variant_setInt8(&int8_var, -100);
    variant_setInt16(&int16_var, -110);
    variant_setInt16(&int16_var2, -1100);
    variant_setInt32(&int32_var, 100);
    variant_setInt64(&int64_var, 150);
    variant_setUInt8(&uint8_var, 200);
    variant_setUInt16(&uint16_var, 240);
    variant_setUInt32(&uint32_var, 600);
    variant_setUInt64(&uint64_var, 800);
    variant_setUInt64(&uint64_var2, 9200);
    variant_setChar(&char_var, "800");
}

void variant_compare_teardown() {
    variant_cleanup(&int8_var);
    variant_cleanup(&int16_var);
    variant_cleanup(&int16_var2);
    variant_cleanup(&int32_var);
    variant_cleanup(&int64_var);
    variant_cleanup(&uint8_var);
    variant_cleanup(&uint16_var);
    variant_cleanup(&uint32_var);
    variant_cleanup(&uint64_var);
    variant_cleanup(&uint64_var2);
    variant_cleanup(&char_var);
}

int variant_compare_null_null() {
    int result = 0;
    assert_bool(variant_compare(NULL, NULL, &result) == true);
    assert_bool(result == 0);

    sahtest_success();
}

int variant_compare_null_variant() {
    int result = 0;
    assert_bool(variant_compare(NULL, &int8_var, &result) == true);
    assert_bool(result < 0);

    sahtest_success();
}

int variant_compare_variant_null() {
    int result = 0;
    assert_bool(variant_compare(&int8_var, NULL, &result) == true);
    assert_bool(result > 0);

    sahtest_success();
}

int variant_compare_unknown_unknown() {
    int result = 0;
    assert_bool(variant_compare(&unknown_var, &unknown_var, &result) == true);
    assert_bool(result == 0);

    sahtest_success();
}

int variant_compare_unknown_variant() {
    int result = 0;
    assert_bool(variant_compare(&unknown_var, &int8_var, &result) == true);
    assert_bool(result < 0);

    sahtest_success();
}

int variant_compare_int8_unknown() {
    int result = 0;
    assert_bool(variant_compare(&int8_var, &unknown_var, &result) == false);

    sahtest_success();
}

int variant_compare_int8_null() {
    int result = 0;
    assert_bool(variant_compare(&int8_var, NULL, &result) == true);
    assert_bool(result > 0);

    sahtest_success();
}

int variant_compare_int8_smaller_variant() {
    int result = 0;
    assert_bool(variant_compare(&int8_var, &int16_var, &result) == true);
    assert_bool(result > 0);

    sahtest_success();
}

int variant_compare_int8_equal_variant() {
    int result = 0;
    assert_bool(variant_compare(&int8_var, &int8_var, &result) == true);
    assert_bool(result == 0);

    sahtest_success();
}

int variant_compare_int8_bigger_variant() {
    int result = 0;
    assert_bool(variant_compare(&int8_var, &int32_var, &result) == true);
    assert_bool(result < 0);

    sahtest_success();
}

int variant_compare_int16_unknown() {
    int result = 0;
    assert_bool(variant_compare(&int16_var, &unknown_var, &result) == false);

    sahtest_success();
}

int variant_compare_int16_null() {
    int result = 0;
    assert_bool(variant_compare(&int16_var, NULL, &result) == true);
    assert_bool(result > 0);

    sahtest_success();
}

int variant_compare_int16_smaller_variant() {
    int result = 0;
    assert_bool(variant_compare(&int16_var, &int16_var2, &result) == true);
    assert_bool(result > 0);

    sahtest_success();
}

int variant_compare_int16_equal_variant() {
    int result = 0;
    assert_bool(variant_compare(&int16_var, &int16_var, &result) == true);
    assert_bool(result == 0);

    sahtest_success();
}

int variant_compare_int16_bigger_variant() {
    int result = 0;
    assert_bool(variant_compare(&int16_var, &int32_var, &result) == true);
    assert_bool(result < 0);

    sahtest_success();
}

int variant_compare_int32_unknown() {
    int result = 0;
    assert_bool(variant_compare(&int32_var, &unknown_var, &result) == false);

    sahtest_success();
}

int variant_compare_int32_null() {
    int result = 0;
    assert_bool(variant_compare(&int32_var, NULL, &result) == true);
    assert_bool(result > 0);

    sahtest_success();
}

int variant_compare_int32_smaller_variant() {
    int result = 0;
    assert_bool(variant_compare(&int32_var, &int16_var, &result) == true);
    assert_bool(result > 0);

    sahtest_success();
}

int variant_compare_int32_equal_variant() {
    int result = 0;
    assert_bool(variant_compare(&int32_var, &int32_var, &result) == true);
    assert_bool(result == 0);

    sahtest_success();
}

int variant_compare_int32_bigger_variant() {
    int result = 0;
    assert_bool(variant_compare(&int32_var, &int64_var, &result) == true);
    assert_bool(result < 0);

    sahtest_success();
}

int variant_compare_int64_unknown() {
    int result = 0;
    assert_bool(variant_compare(&int64_var, &unknown_var, &result) == false);

    sahtest_success();
}

int variant_compare_int64_null() {
    int result = 0;
    assert_bool(variant_compare(&int64_var, NULL, &result) == true);
    assert_bool(result > 0);

    sahtest_success();
}

int variant_compare_int64_smaller_variant() {
    int result = 0;
    assert_bool(variant_compare(&int64_var, &int16_var, &result) == true);
    assert_bool(result > 0);

    sahtest_success();
}

int variant_compare_int64_equal_variant() {
    int result = 0;
    assert_bool(variant_compare(&int64_var, &int64_var, &result) == true);
    assert_bool(result == 0);

    sahtest_success();
}

int variant_compare_int64_bigger_variant() {
    int result = 0;
    assert_bool(variant_compare(&int64_var, &uint8_var, &result) == true);
    assert_bool(result < 0);

    sahtest_success();
}

int variant_compare_uint8_unknown() {
    int result = 0;
    assert_bool(variant_compare(&uint8_var, &unknown_var, &result) == false);

    sahtest_success();
}

int variant_compare_uint8_null() {
    int result = 0;
    assert_bool(variant_compare(&uint8_var, NULL, &result) == true);
    assert_bool(result > 0);

    sahtest_success();
}

int variant_compare_uint8_smaller_variant() {
    int result = 0;
    assert_bool(variant_compare(&uint8_var, &int32_var, &result) == true);
    assert_bool(result > 0);

    sahtest_success();
}

int variant_compare_uint8_equal_variant() {
    int result = 0;
    assert_bool(variant_compare(&uint8_var, &uint8_var, &result) == true);
    assert_bool(result == 0);

    sahtest_success();
}

int variant_compare_uint8_bigger_variant() {
    int result = 0;
    assert_bool(variant_compare(&uint8_var, &uint16_var, &result) == true);
    assert_bool(result < 0);

    sahtest_success();
}

int variant_compare_uint16_unknown() {
    int result = 0;
    assert_bool(variant_compare(&uint16_var, &unknown_var, &result) == false);

    sahtest_success();
}

int variant_compare_uint16_null() {
    int result = 0;
    assert_bool(variant_compare(&uint16_var, NULL, &result) == true);
    assert_bool(result > 0);

    sahtest_success();
}

int variant_compare_uint16_smaller_variant() {
    int result = 0;
    assert_bool(variant_compare(&uint16_var, &int8_var, &result) == true);
    assert_bool(result > 0);

    sahtest_success();
}

int variant_compare_uint16_equal_variant() {
    int result = 0;
    assert_bool(variant_compare(&uint16_var, &uint16_var, &result) == true);
    assert_bool(result == 0);

    sahtest_success();
}

int variant_compare_uint16_bigger_variant() {
    int result = 0;
    assert_bool(variant_compare(&uint16_var, &uint32_var, &result) == true);
    assert_bool(result < 0);

    sahtest_success();
}

int variant_compare_uint32_unknown() {
    int result = 0;
    assert_bool(variant_compare(&uint32_var, &unknown_var, &result) == false);

    sahtest_success();
}

int variant_compare_uint32_null() {
    int result = 0;
    assert_bool(variant_compare(&uint32_var, NULL, &result) == true);
    assert_bool(result > 0);

    sahtest_success();
}

int variant_compare_uint32_smaller_variant() {
    int result = 0;
    assert_bool(variant_compare(&uint32_var, &int8_var, &result) == true);
    assert_bool(result > 0);

    sahtest_success();
}

int variant_compare_uint32_equal_variant() {
    int result = 0;
    assert_bool(variant_compare(&uint32_var, &uint32_var, &result) == true);
    assert_bool(result == 0);

    sahtest_success();
}

int variant_compare_uint32_bigger_variant() {
    int result = 0;
    assert_bool(variant_compare(&uint32_var, &uint64_var, &result) == true);
    assert_bool(result < 0);

    sahtest_success();
}

int variant_compare_uint64_unknown() {
    int result = 0;
    assert_bool(variant_compare(&uint64_var, &unknown_var, &result) == false);

    sahtest_success();
}

int variant_compare_uint64_null() {
    int result = 0;
    assert_bool(variant_compare(&uint64_var, NULL, &result) == true);
    assert_bool(result > 0);

    sahtest_success();
}

int variant_compare_uint64_smaller_variant() {
    int result = 0;
    assert_bool(variant_compare(&uint64_var, &int8_var, &result) == true);
    assert_bool(result > 0);

    sahtest_success();
}

int variant_compare_uint64_equal_variant() {
    int result = 0;
    assert_bool(variant_compare(&uint64_var, &uint64_var, &result) == true);
    assert_bool(result == 0);

    sahtest_success();
}

int variant_compare_uint64_bigger_variant() {
    int result = 0;
    assert_bool(variant_compare(&uint64_var, &uint64_var2, &result) == true);
    assert_bool(result < 0);

    sahtest_success();
}

int variant_compare_char_unknown() {
    int result = 0;
    assert_bool(variant_compare(&char_var, &unknown_var, &result) == false);

    sahtest_success();
}

int variant_compare_char_null() {
    int result = 0;
    assert_bool(variant_compare(&char_var, NULL, &result) == true);
    assert_bool(result > 0);

    sahtest_success();
}

int variant_compare_char_smaller_variant() {
    int result = 0;
    assert_bool(variant_compare(&char_var, &int8_var, &result) == true);
    assert_bool(result > 0);

    sahtest_success();
}

int variant_compare_char_equal_variant() {
    int result = 0;
    assert_bool(variant_compare(&char_var, &uint64_var, &result) == true);
    assert_bool(result == 0);

    sahtest_success();
}

int variant_compare_char_bigger_variant() {
    int result = 0;
    assert_bool(variant_compare(&char_var, &uint64_var2, &result) == true);
    assert_bool(result < 0);

    sahtest_success();
}

int variant_compare_initialized_string_initialized_string() {
    // see bug 33303
    variant_t v1;
    variant_initialize(&v1, variant_type_string);
    variant_t v2;
    variant_initialize(&v2, variant_type_string);

    int result = 0;
    assert_bool(variant_compare(&v1, &v2, &result) == true);
    assert_bool(result == 0);

    sahtest_success();
}
