/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <sahtest/sahtest.h>
#include <pcb/utils/variant.h>

static variant_t var_bytearray;
static variant_t var_string;
static variant_t var_datetime;
static variant_t var_list;
static variant_t var_map;
static variant_t var_integer;
static void* data;

void variant_da_setup() {
    data = calloc(1, 10);
    variant_initialize(&var_bytearray, variant_type_byte_array);
    variant_setByteArray(&var_bytearray, data, 10);
    variant_initialize(&var_string, variant_type_string);
    variant_setChar(&var_string, "Hello world");
    variant_initialize(&var_datetime, variant_type_date_time);
    variant_initialize(&var_list, variant_type_array);
    variant_initialize(&var_map, variant_type_map);
    variant_initialize(&var_integer, variant_type_int32);
}

void variant_da_teardown() {
    variant_cleanup(&var_bytearray);
    variant_cleanup(&var_string);
    variant_cleanup(&var_datetime);
    variant_cleanup(&var_list);
    variant_cleanup(&var_map);
    variant_cleanup(&var_integer);
    free(data);
}

int variant_check_da_bytearray() {
    uint32_t size = 0;

    assert_bool(variant_da_byteArray(&var_bytearray, &size) != NULL);
    assert_bool(variant_da_byteArray(&var_string, &size) == NULL);
    assert_bool(variant_da_byteArray(&var_datetime, &size) == NULL);
    assert_bool(variant_da_byteArray(&var_list, &size) == NULL);
    assert_bool(variant_da_byteArray(&var_map, &size) == NULL);
    assert_bool(variant_da_byteArray(&var_integer, &size) == NULL);
    assert_bool(variant_da_byteArray(NULL, &size) == NULL);

    sahtest_success();
}

int variant_check_da_char() {
    assert_bool(variant_da_char(&var_bytearray) == NULL);
    assert_bool(variant_da_char(&var_string) != NULL);
    assert_bool(variant_da_char(&var_datetime) == NULL);
    assert_bool(variant_da_char(&var_list) == NULL);
    assert_bool(variant_da_char(&var_map) == NULL);
    assert_bool(variant_da_char(&var_integer) == NULL);
    assert_bool(variant_da_char(NULL) == NULL);

    sahtest_success();
}

int variant_check_da_datetime() {
    assert_bool(variant_da_dateTime(&var_bytearray) == NULL);
    assert_bool(variant_da_dateTime(&var_string) == NULL);
    assert_bool(variant_da_dateTime(&var_datetime) != NULL);
    assert_bool(variant_da_dateTime(&var_list) == NULL);
    assert_bool(variant_da_dateTime(&var_map) == NULL);
    assert_bool(variant_da_dateTime(&var_integer) == NULL);
    assert_bool(variant_da_dateTime(NULL) == NULL);

    sahtest_success();
}

int variant_check_da_list() {
    assert_bool(variant_da_list(&var_bytearray) == NULL);
    assert_bool(variant_da_list(&var_string) == NULL);
    assert_bool(variant_da_list(&var_datetime) == NULL);
    assert_bool(variant_da_list(&var_list) != NULL);
    assert_bool(variant_da_list(&var_map) == NULL);
    assert_bool(variant_da_list(&var_integer) == NULL);
    assert_bool(variant_da_list(NULL) == NULL);

    sahtest_success();
}

int variant_check_da_map() {
    assert_bool(variant_da_map(&var_bytearray) == NULL);
    assert_bool(variant_da_map(&var_string) == NULL);
    assert_bool(variant_da_map(&var_datetime) == NULL);
    assert_bool(variant_da_map(&var_list) == NULL);
    assert_bool(variant_da_map(&var_map) != NULL);
    assert_bool(variant_da_map(&var_integer) == NULL);
    assert_bool(variant_da_map(NULL) == NULL);

    sahtest_success();
}

int variant_check_da_string() {
    assert_bool(variant_da_string(&var_bytearray) == NULL);
    assert_bool(variant_da_string(&var_string) != NULL);
    assert_bool(variant_da_string(&var_datetime) == NULL);
    assert_bool(variant_da_string(&var_list) == NULL);
    assert_bool(variant_da_string(&var_map) == NULL);
    assert_bool(variant_da_string(&var_integer) == NULL);
    assert_bool(variant_da_string(NULL) == NULL);

    sahtest_success();
}
