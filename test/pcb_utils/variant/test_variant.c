/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include "bool_variant_conversion.h"
#include "int16_variant_conversion.h"
#include "int32_variant_conversion.h"
#include "int64_variant_conversion.h"
#include "variant_print.h"
#include "linear_regression_test.h"
#include "variant_print_performance.h"
#include "string_variant_conversion.h"
#include "list_variant_conversion.h"

int main(int argc, char** argv) {
    int res = 0;
    {
        const struct CMUnitTest tests[] = {
            cmocka_unit_test(variant_bool_tostring),
            cmocka_unit_test(variant_bool_tojson),
            cmocka_unit_test(variant_bool_toChar),
            cmocka_unit_test(variant_bool_toInt8),
            cmocka_unit_test(variant_bool_toInt16),
            cmocka_unit_test(variant_bool_toInt32),
            cmocka_unit_test(variant_bool_toInt64),
            cmocka_unit_test(variant_bool_toUInt8),
            cmocka_unit_test(variant_bool_toUInt16),
            cmocka_unit_test(variant_bool_toUInt32),
            cmocka_unit_test(variant_bool_toUInt64),
            cmocka_unit_test(variant_bool_toBool),
            cmocka_unit_test(variant_bool_toFd),
            cmocka_unit_test(variant_bool_toDouble),
            cmocka_unit_test(variant_bool_toList),
            cmocka_unit_test(variant_bool_toMap),
        };
        res &= cmocka_run_group_tests(tests, NULL, NULL);
    }
    {
        const struct CMUnitTest tests[] = {
            cmocka_unit_test(variant_int16_tostring),
            cmocka_unit_test(variant_int16_tojson),
            cmocka_unit_test(variant_int16_toChar),
            cmocka_unit_test(variant_int16_toInt8),
            cmocka_unit_test(variant_int16_toInt16),
            cmocka_unit_test(variant_int16_toInt32),
            cmocka_unit_test(variant_int16_toInt64),
            cmocka_unit_test(variant_int16_toUInt8),
            cmocka_unit_test(variant_int16_toUInt16),
            cmocka_unit_test(variant_int16_toUInt32),
            cmocka_unit_test(variant_int16_toUInt64),
            cmocka_unit_test(variant_int16_toBool),
            cmocka_unit_test(variant_int16_toFd),
            cmocka_unit_test(variant_int16_toDouble),
            cmocka_unit_test(variant_int16_toList),
            cmocka_unit_test(variant_int16_toMap),
        };
        res &= cmocka_run_group_tests(tests, variant_int16_conversion_setup, variant_int16_conversion_teardown);
    }
    {
        const struct CMUnitTest tests[] = {
            cmocka_unit_test(variant_int32_tostring),
            cmocka_unit_test(variant_int32_tojson),
            cmocka_unit_test(variant_int32_toChar),
            cmocka_unit_test(variant_int32_toInt8),
            cmocka_unit_test(variant_int32_toInt16),
            cmocka_unit_test(variant_int32_toInt32),
            cmocka_unit_test(variant_int32_toInt64),
            cmocka_unit_test(variant_int32_toUInt8),
            cmocka_unit_test(variant_int32_toUInt16),
            cmocka_unit_test(variant_int32_toUInt32),
            cmocka_unit_test(variant_int32_toUInt64),
            cmocka_unit_test(variant_int32_toBool),
            cmocka_unit_test(variant_int32_toFd),
            cmocka_unit_test(variant_int32_toDouble),
            cmocka_unit_test(variant_int32_toList),
            cmocka_unit_test(variant_int32_toMap),
        };
        res &= cmocka_run_group_tests(tests, variant_int32_conversion_setup, variant_int32_conversion_teardown);
    }
    {
        const struct CMUnitTest tests[] = {
            cmocka_unit_test(variant_int64_tostring),
            cmocka_unit_test(variant_int64_tojson),
            cmocka_unit_test(variant_int64_toChar),
            cmocka_unit_test(variant_int64_toInt8),
            cmocka_unit_test(variant_int64_toInt16),
            cmocka_unit_test(variant_int64_toInt32),
            cmocka_unit_test(variant_int64_toInt64),
            cmocka_unit_test(variant_int64_toUInt8),
            cmocka_unit_test(variant_int64_toUInt16),
            cmocka_unit_test(variant_int64_toUInt32),
            cmocka_unit_test(variant_int64_toUInt64),
            cmocka_unit_test(variant_int64_toBool),
            cmocka_unit_test(variant_int64_toFd),
            cmocka_unit_test(variant_int64_toDouble),
            cmocka_unit_test(variant_int64_toList),
            cmocka_unit_test(variant_int64_toMap),
        };
        res &= cmocka_run_group_tests(tests, variant_int64_conversion_setup, variant_int64_conversion_teardown);
    }
    {
        const struct CMUnitTest tests[] = {
            cmocka_unit_test(variant_print_nullptr),
            cmocka_unit_test(variant_print_unknown),
            cmocka_unit_test(variant_print_true),
            cmocka_unit_test(variant_print_false),
            cmocka_unit_test(variant_print_int8),
            cmocka_unit_test(variant_print_int16),
            cmocka_unit_test(variant_print_int32),
            cmocka_unit_test(variant_print_int64),
            cmocka_unit_test(variant_print_uint8),
            cmocka_unit_test(variant_print_uint16),
            cmocka_unit_test(variant_print_uint32),
            cmocka_unit_test(variant_print_uint64),
            cmocka_unit_test(variant_print_double),
            cmocka_unit_test(variant_print_char),
            cmocka_unit_test(variant_print_map),
            cmocka_unit_test(variant_print_list),
            cmocka_unit_test(variant_print_date),
        };
        res &= cmocka_run_group_tests(tests, variant_print_setup, variant_print_teardown);
    }
    {
        const struct CMUnitTest tests[] = {
            cmocka_unit_test(test_linear_regression_quasi_linear_sample_sets_are_recognised),
        };
        res &= cmocka_run_group_tests(tests, variant_print_setup, variant_print_teardown);
    }
    {
        const struct CMUnitTest tests[] = {
            cmocka_unit_test(variant_print_map_performance_linear_in_map_size),
        };
        res &= cmocka_run_group_tests(tests, variant_print_setup, variant_print_teardown);
    }
    {
        const struct CMUnitTest tests[] = {
            cmocka_unit_test(variant_string_tostring),
            cmocka_unit_test(variant_string_tojson),
            cmocka_unit_test(variant_string_toChar),
            cmocka_unit_test(variant_string_toInt8),
            cmocka_unit_test(variant_string_toInt16),
            cmocka_unit_test(variant_string_toInt32),
            cmocka_unit_test(variant_string_toInt64),
            cmocka_unit_test(variant_string_toUInt8),
            cmocka_unit_test(variant_string_toUInt16),
            cmocka_unit_test(variant_string_toUInt32),
            cmocka_unit_test(variant_string_toUInt64),
            cmocka_unit_test(variant_string_toBool),
            cmocka_unit_test(variant_string_toFd),
            cmocka_unit_test(variant_string_toDouble),
            cmocka_unit_test(variant_string_toList),
            cmocka_unit_test(variant_string_toMap),
            cmocka_unit_test(variant_string_toDateTime),
            cmocka_unit_test(variant_string_toDateTimeExtended),
        };
        res &= cmocka_run_group_tests(tests, variant_string_conversion_setup, variant_string_conversion_teardown);
    }
    {
        const struct CMUnitTest tests[] = {
            cmocka_unit_test(variant_list_tostring),
            cmocka_unit_test(variant_list_tojson),
            cmocka_unit_test(variant_list_toChar),
            cmocka_unit_test(variant_list_toInt8),
            cmocka_unit_test(variant_list_toInt16),
            cmocka_unit_test(variant_list_toInt32),
            cmocka_unit_test(variant_list_toInt64),
            cmocka_unit_test(variant_list_toUInt8),
            cmocka_unit_test(variant_list_toUInt16),
            cmocka_unit_test(variant_list_toUInt32),
            cmocka_unit_test(variant_list_toUInt64),
            cmocka_unit_test(variant_list_toBool),
            cmocka_unit_test(variant_list_toFd),
            cmocka_unit_test(variant_list_toDouble),
            cmocka_unit_test(variant_list_toList),
            cmocka_unit_test(variant_list_toMap),
            cmocka_unit_test(variant_list_toTime),
        };
        res &= cmocka_run_group_tests(tests, variant_list_conversion_setup, variant_list_conversion_teardown);
    }

    return res;
}
