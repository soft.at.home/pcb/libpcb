/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <sahtest/sahtest.h>
#include <pcb/utils/variant.h>

static variant_t variant_low;
static variant_t variant_high;

void variant_uint32_conversion_setup() {
    variant_initialize(&variant_low, variant_type_uint32);
    variant_initialize(&variant_high, variant_type_uint32);

    variant_setUInt32(&variant_low, INT32_MAX);
    variant_setUInt32(&variant_high, UINT32_MAX);
}

void variant_uint32_conversion_teardown() {
    variant_cleanup(&variant_low);
    variant_cleanup(&variant_high);
}


int variant_uint32_tostring() {
    string_t result;
    string_initialize(&result, 0);

    assert_bool(variant_toString(&result, &variant_low));
    assert_bool(strcmp(string_buffer(&result), "2147483647") == 0);

    assert_bool(variant_toString(&result, &variant_high));
    assert_bool(strcmp(string_buffer(&result), "4294967295") == 0);

    string_cleanup(&result);
    sahtest_success();
}

int variant_uint32_tojson() {
    string_t result;
    string_initialize(&result, 0);

    assert_bool(variant_toJSON(&result, &variant_low));
    assert_bool(strcmp(string_buffer(&result), "2147483647") == 0);

    assert_bool(variant_toJSON(&result, &variant_high));
    assert_bool(strcmp(string_buffer(&result), "4294967295") == 0);

    string_cleanup(&result);
    sahtest_success();
}

int variant_uint32_toChar() {
    char* result = NULL;

    assert_bool(variant_toChar(&result, &variant_low));
    assert_bool(result != NULL);
    assert_bool(strcmp(result, "2147483647") == 0);
    free(result);
    result = variant_char(&variant_low);
    assert_bool(result != NULL);
    assert_bool(strcmp(result, "2147483647") == 0);
    free(result);

    assert_bool(variant_toChar(&result, &variant_high));
    assert_bool(result != NULL);
    assert_bool(strcmp(result, "4294967295") == 0);
    free(result);
    result = variant_char(&variant_high);
    assert_bool(result != NULL);
    assert_bool(strcmp(result, "4294967295") == 0);
    free(result);

    sahtest_success();
}

int variant_uint32_toInt8() {
    int8_t result = 0;

    assert_bool(variant_toInt8(&result, &variant_low));
    assert_bool(result == INT8_MAX);
    result = variant_int8(&variant_low);
    assert_bool(result == INT8_MAX);

    assert_bool(variant_toInt8(&result, &variant_high));
    assert_bool(result == INT8_MAX);
    result = variant_int8(&variant_high);
    assert_bool(result == INT8_MAX);

    sahtest_success();
}

int variant_uint32_toInt16() {
    int16_t result = 0;

    assert_bool(variant_toInt16(&result, &variant_low));
    assert_bool(result == INT16_MAX);
    result = variant_int16(&variant_low);
    assert_bool(result == INT16_MAX);

    assert_bool(variant_toInt16(&result, &variant_high));
    assert_bool(result == INT16_MAX);
    result = variant_int16(&variant_high);
    assert_bool(result == INT16_MAX);

    sahtest_success();
}

int variant_uint32_toInt32() {
    int32_t result = 0;

    assert_bool(variant_toInt32(&result, &variant_low));
    assert_bool(result == INT32_MAX);
    result = variant_int32(&variant_low);
    assert_bool(result == INT32_MAX);

    assert_bool(variant_toInt32(&result, &variant_high));
    assert_bool(result == INT32_MAX);
    result = variant_int32(&variant_high);
    assert_bool(result == INT32_MAX);

    sahtest_success();
}

int variant_uint32_toInt64() {
    int64_t result = 0;

    assert_bool(variant_toInt64(&result, &variant_low));
    assert_bool(result == INT32_MAX);
    result = variant_int64(&variant_low);
    assert_bool(result == INT32_MAX);

    assert_bool(variant_toInt64(&result, &variant_high));
    assert_bool(result == UINT32_MAX);
    result = variant_int64(&variant_high);
    assert_bool(result == UINT32_MAX);

    sahtest_success();
}

int variant_uint32_toUInt8() {
    uint8_t result = 0;

    assert_bool(variant_toUInt8(&result, &variant_low));
    assert_bool(result == UINT8_MAX);
    result = variant_uint8(&variant_low);
    assert_bool(result == UINT8_MAX);

    assert_bool(variant_toUInt8(&result, &variant_high));
    assert_bool(result == UINT8_MAX);
    result = variant_uint8(&variant_high);
    assert_bool(result == UINT8_MAX);

    sahtest_success();
}

int variant_uint32_toUInt16() {
    uint16_t result = 0;

    assert_bool(variant_toUInt16(&result, &variant_low));
    assert_bool(result == UINT16_MAX);
    result = variant_uint16(&variant_low);
    assert_bool(result == UINT16_MAX);

    assert_bool(variant_toUInt16(&result, &variant_high));
    assert_bool(result == UINT16_MAX);
    result = variant_uint16(&variant_high);
    assert_bool(result == UINT16_MAX);

    sahtest_success();
}

int variant_uint32_toUInt32() {
    uint32_t result = 0;

    assert_bool(variant_toUInt32(&result, &variant_low));
    assert_bool(result == INT32_MAX);
    result = variant_uint32(&variant_low);
    assert_bool(result == INT32_MAX);

    assert_bool(variant_toUInt32(&result, &variant_high));
    assert_bool(result == UINT32_MAX);
    result = variant_uint32(&variant_high);
    assert_bool(result == UINT32_MAX);

    sahtest_success();
}

int variant_uint32_toUInt64() {
    uint64_t result = 0;

    assert_bool(variant_toUInt64(&result, &variant_low));
    assert_bool(result == 2147483647);
    result = variant_uint64(&variant_low);
    assert_bool(result == 2147483647);

    assert_bool(variant_toUInt64(&result, &variant_high));
    assert_bool(result == UINT32_MAX);
    result = variant_uint64(&variant_high);
    assert_bool(result == UINT32_MAX);

    sahtest_success();
}

int variant_uint32_toBool() {
    bool result = false;

    assert_bool(variant_toBool(&result, &variant_low));
    assert_bool(result == true);
    result = variant_bool(&variant_low);
    assert_bool(result == true);

    assert_bool(variant_toBool(&result, &variant_high));
    assert_bool(result == true);
    result = variant_bool(&variant_high);
    assert_bool(result == true);

    sahtest_success();
}
