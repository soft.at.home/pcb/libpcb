/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef TEST_VARIANT_H
#define TEST_VARIANT_H

#include <sahtest/define.h>

#include "variant/variant_init.h"
#include "variant/string_variant_conversion.h"
#include "variant/int8_variant_conversion.h"
#include "variant/int16_variant_conversion.h"
#include "variant/int32_variant_conversion.h"
#include "variant/int64_variant_conversion.h"
#include "variant/uint8_variant_conversion.h"
#include "variant/uint16_variant_conversion.h"
#include "variant/uint32_variant_conversion.h"
#include "variant/uint64_variant_conversion.h"
#include "variant/bool_variant_conversion.h"
#include "variant/null_unknown_variant_conversion.h"
#include "variant/variant_copy.h"
#include "variant/variant_da.h"
#include "variant/variant_compare.h"
#include "variant/variant_print.h"
#include "variant/list_variant_conversion.h"

sahtest_group_begin(variant, NULL, NULL)
sahtest_add_group(var_init_cleanup, "Test initializing a variant in all different posibilities")
sahtest_add_group(string_conversion, "Test string variant coversions to other types")
sahtest_add_group(int8_conversion, "Test int8 variant coversions to other types")
sahtest_add_group(int16_conversion, "Test int16 variant coversions to other types")
sahtest_add_group(int32_conversion, "Test int32 variant coversions to other types")
sahtest_add_group(int64_conversion, "Test int64 variant coversions to other types")
sahtest_add_group(uint8_conversion, "Test uint8 variant coversions to other types")
sahtest_add_group(uint16_conversion, "Test uint16 variant coversions to other types")
sahtest_add_group(uint32_conversion, "Test uint32 variant coversions to other types")
sahtest_add_group(uint64_conversion, "Test uint64 variant coversions to other types")
sahtest_add_group(bool_conversion, "Test bool variant coversions to other types")
sahtest_add_group(null_unknown_conversion, "Test null or unknown variant coversions to other types")
sahtest_add_group(var_copy, "Test variant copy function")
sahtest_add_group(direct_access, "Test variant direct access functions")
sahtest_add_group(var_compare, "Test variant compare")
sahtest_add_group(var_print, "Test variant print")
sahtest_add_group(list_conversion, "Test list variant conversions to other types")
sahtest_group_end()

#endif // TEST_VARIANT_H
