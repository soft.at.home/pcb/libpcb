/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <setjmp.h>
#include <stdarg.h>
#include <string.h>
#include <inttypes.h>
#include <cmocka.h>
#include <pcb/utils/variant.h>
#include <pcb/utils/variant_list.h>
#include <pcb/utils/variant_map.h>
#include <assert.h>
#include <time.h>
#include <unistd.h>
#include <stdlib.h>
#include "variant_print_performance.h"
#include "linear_regression.h"

static sample_t time_printing(variant_t* map) {
    sample_t sample = {0};
    uint32_t attribute = 0;
    string_t result;
    string_initialize(&result, 0);

    struct timespec t_start, t_stop;
    assert(!clock_gettime(CLOCK_MONOTONIC_RAW, &t_start));

    variant_print(&result, map, 0, NULL);
    string_cleanup(&result);

    assert(!clock_gettime(CLOCK_MONOTONIC_RAW, &t_stop));

    sample.size = (double) variant_map_size(variant_da_map(map));
    // convert to nanoseconds
    const long diff_sec_ns = (t_stop.tv_sec - t_start.tv_sec) * 1000L * 1000L * 1000L;
    const long diff_nsec_ns = (t_stop.tv_nsec - t_start.tv_nsec);
    sample.time = (double) (diff_sec_ns + diff_nsec_ns) * 1e-9;

    assert(sample.time >= 0);
    return sample;
}

static void grow_map(variant_map_t* map, const size_t start_id, const size_t end_id) {
    string_t key;
    string_initialize(&key, 0);

    for(size_t i = start_id; i != end_id; ++i) {
        string_fromInt32(&key, start_id);
        variant_map_addChar(map, key.buffer, "value");
    }
    string_cleanup(&key);
}

void variant_print_map_performance_linear_in_map_size(void** state) {
    variant_t variant;
    assert(variant_initialize(&variant, variant_type_map));

    sample_t samples[1000] = {0};
    sample_t* const samples_end = samples + 1000;

    sample_t* next_sample = samples;
    size_t start_id = 1;
    size_t end_id = 2;

    for(; end_id < 100000 && samples_end - next_sample > 10;
        start_id *= 2, end_id *= 2, ++next_sample) {

        grow_map(variant_da_map(&variant), start_id, end_id);

        for(int i = 0; i != 10; ++i) {
            *next_sample = time_printing(&variant);
        }
    }

    variant_cleanup(&variant);
    assert_true(is_quasi_linear(samples, next_sample - samples));
}