/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#define _DEFAULT_SOURCE
#define _BSD_SOURCE /* needed for timegm() */
#include <stdio.h>
#include <stdlib.h>
#include <setjmp.h>
#include <stdarg.h>
#include <string.h>
#include <inttypes.h>
#include <cmocka.h>
#include <pcb/utils/variant.h>
#include <pcb/utils/variant_list.h>
#include <pcb/utils/variant_map.h>
#include <pcb/common/error.h>

static variant_t variant_text;
static variant_t variant_string_zero;
static variant_t variant_string_number;
static variant_t variant_string_yes;
static variant_t variant_string_true;
static variant_t variant_string_false;
static variant_t variant_string_datetime;

int variant_string_conversion_setup(void** state) {
    variant_initialize(&variant_text, variant_type_string);
    variant_initialize(&variant_string_zero, variant_type_string);
    variant_initialize(&variant_string_number, variant_type_string);
    variant_initialize(&variant_string_yes, variant_type_string);
    variant_initialize(&variant_string_true, variant_type_string);
    variant_initialize(&variant_string_false, variant_type_string);
    variant_initialize(&variant_string_datetime, variant_type_string);

    variant_setChar(&variant_text, "Hallo World");
    variant_setChar(&variant_string_zero, "0");
    variant_setChar(&variant_string_number, "4096");
    variant_setChar(&variant_string_yes, "yes");
    variant_setChar(&variant_string_true, "true");
    variant_setChar(&variant_string_false, "false");
    variant_setChar(&variant_string_datetime, "2019-03-03T05:45:21.24Z");
    return 0;
}

int variant_string_conversion_teardown(void** state) {
    variant_cleanup(&variant_text);
    variant_cleanup(&variant_string_zero);
    variant_cleanup(&variant_string_number);
    variant_cleanup(&variant_string_yes);
    variant_cleanup(&variant_string_true);
    variant_cleanup(&variant_string_false);
    variant_cleanup(&variant_string_datetime);
    return 0;
}

void variant_string_tostring(void** state) {
    string_t result;
    string_t* string = NULL;
    string_initialize(&result, 0);

    assert_true(variant_toString(&result, &variant_text));
    assert_string_equal(string_buffer(&result), "Hallo World");

    assert_true(variant_toString(&result, &variant_string_zero));
    assert_string_equal(string_buffer(&result), "0");

    assert_true(variant_toString(&result, &variant_string_number));
    assert_string_equal(string_buffer(&result), "4096");

    assert_true(variant_toString(&result, &variant_string_yes));
    assert_string_equal(string_buffer(&result), "yes");

    assert_true(variant_toString(&result, &variant_string_true));
    assert_string_equal(string_buffer(&result), "true");

    assert_true(variant_toString(&result, &variant_string_false));
    assert_string_equal(string_buffer(&result), "false");

    assert_true(variant_toString(&result, &variant_string_datetime));
    assert_string_equal(string_buffer(&result), "2019-03-03T05:45:21.24Z");

    string = variant_string(&variant_text);
    assert_non_null(string);
    assert_string_equal(string_buffer(string), "Hallo World");
    string_cleanup(string);
    free(string);

    string = variant_string(&variant_string_zero);
    assert_non_null(string);
    assert_string_equal(string_buffer(string), "0");
    string_cleanup(string);
    free(string);

    string = variant_string(&variant_string_number);
    assert_non_null(string);
    assert_string_equal(string_buffer(string), "4096");
    string_cleanup(string);
    free(string);

    string = variant_string(&variant_string_yes);
    assert_non_null(string);
    assert_string_equal(string_buffer(string), "yes");
    string_cleanup(string);
    free(string);

    string = variant_string(&variant_string_true);
    assert_non_null(string);
    assert_string_equal(string_buffer(string), "true");
    string_cleanup(string);
    free(string);

    string = variant_string(&variant_string_false);
    assert_non_null(string);
    assert_string_equal(string_buffer(string), "false");
    string_cleanup(string);
    free(string);

    string = variant_string(&variant_string_datetime);
    assert_non_null(string);
    assert_string_equal(string_buffer(&result), "2019-03-03T05:45:21.24Z");
    string_cleanup(string);
    free(string);

    string_cleanup(&result);
}

void variant_string_tojson(void** state) {
    string_t result;
    string_initialize(&result, 0);

    assert_true(variant_toJSON(&result, &variant_text));
    assert_string_equal(string_buffer(&result), "\"Hallo World\"");

    assert_true(variant_toJSON(&result, &variant_string_zero));
    assert_string_equal(string_buffer(&result), "\"0\"");

    assert_true(variant_toJSON(&result, &variant_string_number));
    assert_string_equal(string_buffer(&result), "\"4096\"");

    assert_true(variant_toJSON(&result, &variant_string_yes));
    assert_string_equal(string_buffer(&result), "\"yes\"");

    assert_true(variant_toJSON(&result, &variant_string_true));
    assert_string_equal(string_buffer(&result), "\"true\"");

    assert_true(variant_toJSON(&result, &variant_string_false));
    assert_string_equal(string_buffer(&result), "\"false\"");

    assert_true(variant_toJSON(&result, &variant_string_datetime));
    assert_string_equal(string_buffer(&result), "\"2019-03-03T05:45:21.24Z\"");

    string_cleanup(&result);
}

void variant_string_toChar(void** state) {
    char* result = NULL;

    assert_true(variant_toChar(&result, &variant_text));
    assert_non_null(result);
    assert_string_equal(result, "Hallo World");
    free(result);
    result = variant_char(&variant_text);
    assert_non_null(result);
    assert_string_equal(result, "Hallo World");
    free(result);

    assert_true(variant_toChar(&result, &variant_string_number));
    assert_non_null(result);
    assert_string_equal(result, "4096");
    free(result);
    result = variant_char(&variant_string_number);
    assert_non_null(result);
    assert_string_equal(result, "4096");
    free(result);

    assert_true(variant_toChar(&result, &variant_string_yes));
    assert_non_null(result);
    assert_string_equal(result, "yes");
    free(result);
    result = variant_char(&variant_string_yes);
    assert_non_null(result);
    assert_string_equal(result, "yes");
    free(result);

    assert_true(variant_toChar(&result, &variant_string_true));
    assert_non_null(result);
    assert_string_equal(result, "true");
    free(result);
    result = variant_char(&variant_string_true);
    assert_non_null(result);
    assert_string_equal(result, "true");
    free(result);

    assert_true(variant_toChar(&result, &variant_string_false));
    assert_non_null(result);
    assert_string_equal(result, "false");
    free(result);
    result = variant_char(&variant_string_false);
    assert_non_null(result);
    assert_string_equal(result, "false");
    free(result);

    assert_true(variant_toChar(&result, &variant_string_datetime));
    assert_non_null(result);
    assert_string_equal(result, "2019-03-03T05:45:21.24Z");
    free(result);
    result = variant_char(&variant_string_datetime);
    assert_non_null(result);
    assert_string_equal(result, "2019-03-03T05:45:21.24Z");
    free(result);
}

void variant_string_toInt8(void** state) {
    int8_t result = 0;

    assert_false(variant_toInt8(&result, &variant_text));
    assert_int_equal(result, 0);
    result = variant_int8(&variant_text);
    assert_int_equal(result, 0);

    assert_true(variant_toInt8(&result, &variant_string_zero));
    assert_int_equal(result, 0);
    result = variant_int8(&variant_string_zero);
    assert_int_equal(result, 0);

    assert_false(variant_toInt8(&result, &variant_string_number));
    assert_int_equal(result, 0);
    result = variant_int8(&variant_string_number);
    assert_int_equal(result, 0);

    assert_false(variant_toInt8(&result, &variant_string_yes));
    assert_int_equal(result, 0);
    result = variant_int8(&variant_string_yes);
    assert_int_equal(result, 0);

    assert_false(variant_toInt8(&result, &variant_string_true));
    assert_int_equal(result, 0);
    result = variant_int8(&variant_string_true);
    assert_int_equal(result, 0);

    assert_false(variant_toInt8(&result, &variant_string_false));
    assert_int_equal(result, 0);
    result = variant_int8(&variant_string_false);
    assert_int_equal(result, 0);

    assert_false(variant_toInt8(&result, &variant_string_datetime));
    assert_int_equal(result, 0);
    result = variant_int8(&variant_string_datetime);
    assert_int_equal(result, 0);
}

void variant_string_toInt16(void** state) {
    int16_t result = 0;

    assert_false(variant_toInt16(&result, &variant_text));
    assert_int_equal(result, 0);
    result = variant_int16(&variant_text);
    assert_int_equal(result, 0);

    assert_true(variant_toInt16(&result, &variant_string_zero));
    assert_int_equal(result, 0);
    result = variant_int16(&variant_string_zero);
    assert_int_equal(result, 0);

    assert_true(variant_toInt16(&result, &variant_string_number));
    assert_int_equal(result, (int16_t) 4096);
    result = variant_int16(&variant_string_number);
    assert_int_equal(result, (int16_t) 4096);

    assert_false(variant_toInt16(&result, &variant_string_yes));
    assert_int_equal(result, 0);
    result = variant_int16(&variant_string_yes);
    assert_int_equal(result, 0);

    assert_false(variant_toInt16(&result, &variant_string_true));
    assert_int_equal(result, 0);
    result = variant_int16(&variant_string_true);
    assert_int_equal(result, 0);

    assert_false(variant_toInt16(&result, &variant_string_false));
    assert_int_equal(result, 0);
    result = variant_int16(&variant_string_false);
    assert_int_equal(result, 0);

    assert_false(variant_toInt16(&result, &variant_string_datetime));
    assert_int_equal(result, 0);
    result = variant_int16(&variant_string_datetime);
    assert_int_equal(result, 0);
}

void variant_string_toInt32(void** state) {
    int32_t result = 0;

    assert_false(variant_toInt32(&result, &variant_text));
    assert_int_equal(result, 0);
    result = variant_int32(&variant_text);
    assert_int_equal(result, 0);

    assert_true(variant_toInt32(&result, &variant_string_zero));
    assert_int_equal(result, 0);
    result = variant_int32(&variant_string_zero);
    assert_int_equal(result, 0);

    assert_true(variant_toInt32(&result, &variant_string_number));
    assert_int_equal(result, (int32_t) 4096);
    result = variant_int32(&variant_string_number);
    assert_int_equal(result, (int32_t) 4096);

    assert_false(variant_toInt32(&result, &variant_string_yes));
    assert_int_equal(result, 0);
    result = variant_int32(&variant_string_yes);
    assert_int_equal(result, 0);

    assert_false(variant_toInt32(&result, &variant_string_true));
    assert_int_equal(result, 0);
    result = variant_int32(&variant_string_true);
    assert_int_equal(result, 0);

    assert_false(variant_toInt32(&result, &variant_string_false));
    assert_int_equal(result, 0);
    result = variant_int32(&variant_string_false);
    assert_int_equal(result, 0);

    assert_false(variant_toInt32(&result, &variant_string_datetime));
    assert_int_equal(result, 0);
    result = variant_int32(&variant_string_datetime);
    assert_int_equal(result, 0);
}

void variant_string_toInt64(void** state) {
    int64_t result = 0;

    assert_false(variant_toInt64(&result, &variant_text));
    assert_int_equal(result, 0);
    result = variant_int64(&variant_text);
    assert_int_equal(result, 0);

    assert_true(variant_toInt64(&result, &variant_string_zero));
    assert_int_equal(result, 0);
    result = variant_int64(&variant_string_zero);
    assert_int_equal(result, 0);

    assert_true(variant_toInt64(&result, &variant_string_number));
    assert_int_equal(result, (int64_t) 4096);
    result = variant_int64(&variant_string_number);
    assert_int_equal(result, (int64_t) 4096);

    assert_false(variant_toInt64(&result, &variant_string_yes));
    assert_int_equal(result, 0);
    result = variant_int64(&variant_string_yes);
    assert_int_equal(result, 0);

    assert_false(variant_toInt64(&result, &variant_string_true));
    assert_int_equal(result, 0);
    result = variant_int64(&variant_string_true);
    assert_int_equal(result, 0);

    assert_false(variant_toInt64(&result, &variant_string_false));
    assert_int_equal(result, 0);
    result = variant_int64(&variant_string_false);
    assert_int_equal(result, 0);

    assert_false(variant_toInt64(&result, &variant_string_datetime));
    assert_int_equal(result, 0);
    result = variant_int64(&variant_string_datetime);
    assert_int_equal(result, 0);
}

void variant_string_toUInt8(void** state) {
    uint8_t result = 0;

    assert_false(variant_toUInt8(&result, &variant_text));
    assert_int_equal(result, 0);
    result = variant_uint8(&variant_text);
    assert_int_equal(result, 0);

    assert_true(variant_toUInt8(&result, &variant_string_zero));
    assert_int_equal(result, 0);
    result = variant_uint8(&variant_string_zero);
    assert_int_equal(result, 0);

    assert_false(variant_toUInt8(&result, &variant_string_number));
    assert_int_equal(result, 0);
    result = variant_uint8(&variant_string_number);
    assert_int_equal(result, 0);

    assert_false(variant_toUInt8(&result, &variant_string_yes));
    assert_int_equal(result, 0);
    result = variant_uint8(&variant_string_yes);
    assert_int_equal(result, 0);

    assert_false(variant_toUInt8(&result, &variant_string_true));
    assert_int_equal(result, 0);
    result = variant_uint8(&variant_string_true);
    assert_int_equal(result, 0);

    assert_false(variant_toUInt8(&result, &variant_string_false));
    assert_int_equal(result, 0);
    result = variant_uint8(&variant_string_false);
    assert_int_equal(result, 0);

    assert_false(variant_toUInt8(&result, &variant_string_datetime));
    assert_int_equal(result, 0);
    result = variant_uint8(&variant_string_datetime);
    assert_int_equal(result, 0);
}

void variant_string_toUInt16(void** state) {
    uint16_t result = 0;

    assert_false(variant_toUInt16(&result, &variant_text));
    assert_int_equal(result, 0);
    result = variant_uint16(&variant_text);
    assert_int_equal(result, 0);

    assert_true(variant_toUInt16(&result, &variant_string_zero));
    assert_int_equal(result, 0);
    result = variant_uint16(&variant_string_zero);
    assert_int_equal(result, 0);

    assert_true(variant_toUInt16(&result, &variant_string_number));
    assert_int_equal(result, (uint16_t) 4096);
    result = variant_uint16(&variant_string_number);
    assert_int_equal(result, (uint16_t) 4096);

    assert_false(variant_toUInt16(&result, &variant_string_yes));
    assert_int_equal(result, 0);
    result = variant_uint16(&variant_string_yes);
    assert_int_equal(result, 0);

    assert_false(variant_toUInt16(&result, &variant_string_true));
    assert_int_equal(result, 0);
    result = variant_uint16(&variant_string_true);
    assert_int_equal(result, 0);

    assert_false(variant_toUInt16(&result, &variant_string_false));
    assert_int_equal(result, 0);
    result = variant_uint16(&variant_string_false);
    assert_int_equal(result, 0);

    assert_false(variant_toUInt16(&result, &variant_string_datetime));
    assert_int_equal(result, 0);
    result = variant_uint16(&variant_string_datetime);
    assert_int_equal(result, 0);
}

void variant_string_toUInt32(void** state) {
    uint32_t result = 0;

    assert_false(variant_toUInt32(&result, &variant_text));
    assert_int_equal(result, 0);
    result = variant_uint32(&variant_text);
    assert_int_equal(result, 0);

    assert_true(variant_toUInt32(&result, &variant_string_zero));
    assert_int_equal(result, 0);
    result = variant_uint32(&variant_string_zero);
    assert_int_equal(result, 0);

    assert_true(variant_toUInt32(&result, &variant_string_number));
    assert_int_equal(result, (uint32_t) 4096);
    result = variant_uint32(&variant_string_number);
    assert_int_equal(result, (uint32_t) 4096);

    assert_false(variant_toUInt32(&result, &variant_string_yes));
    assert_int_equal(result, 0);
    result = variant_uint32(&variant_string_yes);
    assert_int_equal(result, 0);

    assert_false(variant_toUInt32(&result, &variant_string_true));
    assert_int_equal(result, 0);
    result = variant_uint32(&variant_string_true);
    assert_int_equal(result, 0);

    assert_false(variant_toUInt32(&result, &variant_string_false));
    assert_int_equal(result, 0);
    result = variant_uint32(&variant_string_false);
    assert_int_equal(result, 0);

    assert_false(variant_toUInt32(&result, &variant_string_datetime));
    assert_int_equal(result, 0);
    result = variant_uint32(&variant_string_datetime);
    assert_int_equal(result, 0);
}

void variant_string_toUInt64(void** state) {
    uint64_t result = 0;

    assert_false(variant_toUInt64(&result, &variant_text));
    assert_int_equal(result, 0);
    result = variant_uint64(&variant_text);
    assert_int_equal(result, 0);

    assert_true(variant_toUInt64(&result, &variant_string_zero));
    assert_int_equal(result, 0);
    result = variant_uint64(&variant_string_zero);
    assert_int_equal(result, 0);

    assert_true(variant_toUInt64(&result, &variant_string_number));
    assert_int_equal(result, (uint64_t) 4096);
    result = variant_uint64(&variant_string_number);
    assert_int_equal(result, (uint64_t) 4096);

    assert_false(variant_toUInt64(&result, &variant_string_yes));
    assert_int_equal(result, 0);
    result = variant_uint64(&variant_string_yes);
    assert_int_equal(result, 0);

    assert_false(variant_toUInt64(&result, &variant_string_true));
    assert_int_equal(result, 0);
    result = variant_uint64(&variant_string_true);
    assert_int_equal(result, 0);

    assert_false(variant_toUInt64(&result, &variant_string_false));
    assert_int_equal(result, 0);
    result = variant_uint64(&variant_string_false);
    assert_int_equal(result, 0);

    assert_false(variant_toUInt64(&result, &variant_string_datetime));
    assert_int_equal(result, 0);
    result = variant_uint64(&variant_string_datetime);
    assert_int_equal(result, 0);
}

void variant_string_toBool(void** state) {
    bool result = false;

    assert_false(variant_toBool(&result, &variant_text));
    assert_false(result);
    result = variant_bool(&variant_text);
    assert_false(result);

    assert_true(variant_toBool(&result, &variant_string_zero));
    assert_false(result);
    result = variant_bool(&variant_string_zero);
    assert_false(result);

    assert_false(variant_toBool(&result, &variant_string_number));
    assert_false(result);
    result = variant_bool(&variant_string_number);
    assert_false(result);

    assert_true(variant_toBool(&result, &variant_string_yes));
    assert_true(result);
    result = variant_bool(&variant_string_yes);
    assert_true(result);

    assert_true(variant_toBool(&result, &variant_string_true));
    assert_true(result);
    result = variant_bool(&variant_string_true);
    assert_true(result);

    assert_true(variant_toBool(&result, &variant_string_false));
    assert_false(result);
    result = variant_bool(&variant_string_false);
    assert_false(result);

    assert_false(variant_toBool(&result, &variant_string_datetime));
    assert_false(result);
    result = variant_bool(&variant_string_datetime);
    assert_false(result);
}

void variant_string_toFd(void** state) {
    int result = 0;

    assert_false(variant_toFd(&result, &variant_text));
    result = variant_fd(&variant_text);
    assert_int_equal(result, -1);

    assert_false(variant_toFd(&result, &variant_string_number));
    result = variant_fd(&variant_string_number);
    assert_int_equal(result, -1);
}

void variant_string_toDouble(void** state) {
    double result = 0;
    assert_false(variant_toDouble(&result, &variant_text));
    result = variant_double(&variant_text);
    assert_int_equal(result, 0);

    assert_true(variant_toDouble(&result, &variant_string_zero));
    result = variant_double(&variant_string_zero);
    assert_int_equal(result, 0);

    assert_true(variant_toDouble(&result, &variant_string_number));
    result = variant_double(&variant_string_number);
    assert_int_equal(result, 4096);
}

void variant_string_toList(void** state) {
    variant_list_t result;
    variant_list_t* result_list = NULL;
    variant_list_initialize(&result);

    assert_true(variant_toList(&result, &variant_text));
    assert_int_equal(variant_list_size(&result), 1);
    result_list = variant_list(&variant_text);
    assert_non_null(result_list);
    assert_int_equal(variant_list_size(result_list), 1);

    variant_list_clear(&result);
    variant_list_clear(result_list);
    free(result_list);
}

void variant_string_toMap(void** state) {
    variant_map_t result;
    variant_map_t* result_map = NULL;
    variant_map_initialize(&result);

    assert_false(variant_toMap(&result, &variant_text));
    assert_int_equal(variant_map_size(&result), 0);
    result_map = variant_map(&variant_text);
    assert_null(result_map);
}

void variant_string_toDateTime(void** state) {
    struct tm result1;
    struct tm* result2;
    const struct tm* result3;

    assert_false(variant_toDateTime(&result1, &variant_text));
    result2 = variant_dateTime(&variant_text);
    assert_null(result2);
    result3 = variant_da_dateTime(&variant_text);
    assert_null(result3);

    assert_true(variant_toDateTime(&result1, &variant_string_zero));
    assert_int_equal(result1.tm_year, -1899);  // 0001-01-01T00:00:00Z
    assert_int_equal(result1.tm_mon, 0);
    assert_int_equal(result1.tm_mday, 1);
    assert_int_equal(result1.tm_hour, 0);
    assert_int_equal(result1.tm_min, 0);
    assert_int_equal(result1.tm_sec, 0);
    result2 = variant_dateTime(&variant_string_datetime);
    assert_non_null(result2);
    free(result2);

    assert_false(variant_toDateTime(&result1, &variant_string_number));
    result2 = variant_dateTime(&variant_string_number);
    assert_null(result2);
    result3 = variant_da_dateTime(&variant_string_number);
    assert_null(result3);

    assert_false(variant_toDateTime(&result1, &variant_string_yes));
    result2 = variant_dateTime(&variant_string_yes);
    assert_null(result2);
    result3 = variant_da_dateTime(&variant_string_yes);
    assert_null(result3);

    assert_false(variant_toDateTime(&result1, &variant_string_true));
    result2 = variant_dateTime(&variant_string_true);
    assert_null(result2);
    result3 = variant_da_dateTime(&variant_string_true);
    assert_null(result3);

    assert_false(variant_toDateTime(&result1, &variant_string_false));
    result2 = variant_dateTime(&variant_string_false);
    assert_null(result2);
    result3 = variant_da_dateTime(&variant_string_false);
    assert_null(result3);

    // date -d "2019-03-03T05:45:21.24Z" +%s -> 1551591921
    assert_true(variant_toDateTime(&result1, &variant_string_datetime));
    assert_int_equal(timegm(&result1), ((time_t) 1551591921ULL));
    result2 = variant_dateTime(&variant_string_datetime);
    assert_non_null(result2);
    if(result2) {
        assert_int_equal(timegm(result2), ((time_t) 1551591921ULL));
        free(result2);
    }
    result3 = variant_da_dateTime(&variant_string_datetime);
    assert_null(result3);
}

void variant_string_toDateTimeExtended(void** state) {
    pcb_datetime_t result1;
    pcb_datetime_t* result2;
    const pcb_datetime_t* result3;

    assert_false(variant_toDateTimeExtended(&result1, &variant_text));
    result2 = variant_dateTimeExtended(&variant_text);
    assert_null(result2);

    assert_true(variant_toDateTimeExtended(&result1, &variant_string_zero));
    assert_int_equal(result1.datetime.tm_year, -1899);  // 0001-01-01T00:00:00.000000Z
    assert_int_equal(result1.datetime.tm_mon, 0);
    assert_int_equal(result1.datetime.tm_mday, 1);
    assert_int_equal(result1.datetime.tm_hour, 0);
    assert_int_equal(result1.datetime.tm_min, 0);
    assert_int_equal(result1.datetime.tm_sec, 0);
    assert_int_equal(result1.nanoseconds, 0);
    result2 = variant_dateTimeExtended(&variant_string_datetime);
    assert_non_null(result2);
    free(result2);

    assert_false(variant_toDateTimeExtended(&result1, &variant_string_number));
    result2 = variant_dateTimeExtended(&variant_string_number);
    assert_null(result2);

    assert_false(variant_toDateTimeExtended(&result1, &variant_string_yes));
    result2 = variant_dateTimeExtended(&variant_string_yes);
    assert_null(result2);

    assert_false(variant_toDateTimeExtended(&result1, &variant_string_true));
    result2 = variant_dateTimeExtended(&variant_string_true);
    assert_null(result2);

    assert_false(variant_toDateTimeExtended(&result1, &variant_string_false));
    result2 = variant_dateTimeExtended(&variant_string_false);
    assert_null(result2);

    // date -d "2019-03-03T05:45:21.24Z" +%s -> 1551591921
    assert_true(variant_toDateTimeExtended(&result1, &variant_string_datetime));
    assert_int_equal(timegm(&result1.datetime), ((time_t) 1551591921ULL));
    assert_int_equal(result1.nanoseconds, 240000000UL);
    result2 = variant_dateTimeExtended(&variant_string_datetime);
    assert_non_null(result2);
    if(result2) {
        assert_int_equal(timegm(&result2->datetime), ((time_t) 1551591921ULL));
        assert_int_equal(result2->nanoseconds, 240000000UL);
        free(result2);
    }
    result3 = variant_da_dateTimeExtended(&variant_string_datetime);
    assert_null(result3);
}
