/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <sahtest/sahtest.h>
#include <pcb/utils/uri.h>

int test_parse_null() {
    uri_t* uri = uri_parse(NULL);

    T_VERIFY(uri == NULL);

    return 0;
}

int test_parse_empty_string() {
    uri_t* uri = uri_parse("");

    T_VERIFY(uri == NULL);
    T_VERIFY(uri_getScheme(uri) == NULL);
    T_VERIFY(uri_getHost(uri) == NULL);
    T_VERIFY(uri_getPort(uri) == NULL);
    T_VERIFY(uri_getUser(uri) == NULL);
    T_VERIFY(uri_getPassword(uri) == NULL);
    T_VERIFY(uri_getPath(uri) == NULL);

    uri_destroy(NULL);

    return 0;
}

int test_parse_scheme_only() {
    uri_t* uri = uri_parse("http://");

    T_VERIFY(uri == NULL);

    return 0;
}

int test_parse_authority_no_host() {
    uri_t* uri = uri_parse("http://userame:password@:8181");

    T_VERIFY(uri != NULL);
    T_VERIFY(uri_getScheme(uri) != NULL);
    T_VERIFY(uri_getHost(uri) != NULL);
    T_VERIFY(uri_getPort(uri) != NULL);
    T_VERIFY(uri_getUser(uri) != NULL);
    T_VERIFY(uri_getPassword(uri) != NULL);
    T_VERIFY(uri_getPath(uri) == NULL);

    T_CMP(uri_getHost(uri), "");

    uri_destroy(uri);

    return 0;
}


int test_parse_simple_authority() {
    uri_t* uri = uri_parse("http://www.google.be");

    T_VERIFY(uri != NULL);
    T_VERIFY(uri_getScheme(uri) != NULL);
    T_VERIFY(uri_getHost(uri) != NULL);
    T_VERIFY(uri_getPort(uri) == NULL);
    T_VERIFY(uri_getUser(uri) == NULL);
    T_VERIFY(uri_getPassword(uri) == NULL);
    T_VERIFY(uri_getPath(uri) == NULL);

    T_CMP(uri_getScheme(uri), "http");
    T_CMP(uri_getHost(uri), "www.google.be");

    uri_destroy(uri);

    return 0;
}

int test_parse_authority_with_port() {
    uri_t* uri = uri_parse("http://www.google.be:8181");

    T_VERIFY(uri != NULL);
    T_VERIFY(uri_getScheme(uri) != NULL);
    T_VERIFY(uri_getHost(uri) != NULL);
    T_VERIFY(uri_getPort(uri) != NULL);
    T_VERIFY(uri_getUser(uri) == NULL);
    T_VERIFY(uri_getPassword(uri) == NULL);
    T_VERIFY(uri_getPath(uri) == NULL);

    T_CMP(uri_getScheme(uri), "http");
    T_CMP(uri_getHost(uri), "www.google.be");
    T_CMP(uri_getPort(uri), "8181");

    uri_destroy(uri);

    return 0;
}

int test_parse_authority_with_user() {
    uri_t* uri = uri_parse("http://username@www.google.be");

    T_VERIFY(uri != NULL);
    T_VERIFY(uri_getScheme(uri) != NULL);
    T_VERIFY(uri_getHost(uri) != NULL);
    T_VERIFY(uri_getPort(uri) == NULL);
    T_VERIFY(uri_getUser(uri) != NULL);
    T_VERIFY(uri_getPassword(uri) == NULL);
    T_VERIFY(uri_getPath(uri) == NULL);

    T_CMP(uri_getScheme(uri), "http");
    T_CMP(uri_getUser(uri), "username");
    T_CMP(uri_getHost(uri), "www.google.be");

    uri_destroy(uri);

    return 0;
}

int test_parse_authority_with_user_password() {
    uri_t* uri = uri_parse("http://username:password@www.google.be");

    T_VERIFY(uri != NULL);
    T_VERIFY(uri_getScheme(uri) != NULL);
    T_VERIFY(uri_getHost(uri) != NULL);
    T_VERIFY(uri_getPort(uri) == NULL);
    T_VERIFY(uri_getUser(uri) != NULL);
    T_VERIFY(uri_getPassword(uri) != NULL);
    T_VERIFY(uri_getPath(uri) == NULL);

    T_CMP(uri_getScheme(uri), "http");
    T_CMP(uri_getUser(uri), "username");
    T_CMP(uri_getPassword(uri), "password");
    T_CMP(uri_getHost(uri), "www.google.be");

    uri_destroy(uri);

    return 0;
}

int test_parse_authority_full() {
    uri_t* uri = uri_parse("http://username:password@www.google.be:8181");

    T_VERIFY(uri != NULL);
    T_VERIFY(uri_getScheme(uri) != NULL);
    T_VERIFY(uri_getHost(uri) != NULL);
    T_VERIFY(uri_getPort(uri) != NULL);
    T_VERIFY(uri_getUser(uri) != NULL);
    T_VERIFY(uri_getPassword(uri) != NULL);
    T_VERIFY(uri_getPath(uri) == NULL);

    T_CMP(uri_getScheme(uri), "http");
    T_CMP(uri_getUser(uri), "username");
    T_CMP(uri_getPassword(uri), "password");
    T_CMP(uri_getHost(uri), "www.google.be");
    T_CMP(uri_getPort(uri), "8181");

    uri_destroy(uri);

    return 0;
}

int test_parse_authority_full_with_curly_braces() {
    uri_t* uri = uri_parse("http://username:password@{www.google.be}:{8181}");

    T_VERIFY(uri != NULL);
    T_VERIFY(uri_getScheme(uri) != NULL);
    T_VERIFY(uri_getHost(uri) != NULL);
    T_VERIFY(uri_getPort(uri) != NULL);
    T_VERIFY(uri_getUser(uri) != NULL);
    T_VERIFY(uri_getPassword(uri) != NULL);
    T_VERIFY(uri_getPath(uri) == NULL);

    T_CMP(uri_getScheme(uri), "http");
    T_CMP(uri_getUser(uri), "username");
    T_CMP(uri_getPassword(uri), "password");
    T_CMP(uri_getHost(uri), "www.google.be");
    T_CMP(uri_getPort(uri), "8181");

    uri_destroy(uri);

    return 0;
}

int test_parse_authority_full_with_curly_braces_special_chars() {
    uri_t* uri = uri_parse("pcb://username:password@ipc:{/var/run/[@test]:}");

    T_VERIFY(uri != NULL);
    T_VERIFY(uri_getScheme(uri) != NULL);
    T_VERIFY(uri_getHost(uri) != NULL);
    T_VERIFY(uri_getPort(uri) != NULL);
    T_VERIFY(uri_getUser(uri) != NULL);
    T_VERIFY(uri_getPassword(uri) != NULL);
    T_VERIFY(uri_getPath(uri) == NULL);

    T_CMP(uri_getScheme(uri), "pcb");
    T_CMP(uri_getUser(uri), "username");
    T_CMP(uri_getPassword(uri), "password");
    T_CMP(uri_getHost(uri), "ipc");
    T_CMP(uri_getPort(uri), "/var/run/[@test]:");

    uri_destroy(uri);

    return 0;
}

int test_parse_authority_full_with_braces() {
    uri_t* uri = uri_parse("http://username:password@[www.google.be]:[8181]");

    T_VERIFY(uri != NULL);
    T_VERIFY(uri_getScheme(uri) != NULL);
    T_VERIFY(uri_getHost(uri) != NULL);
    T_VERIFY(uri_getPort(uri) != NULL);
    T_VERIFY(uri_getUser(uri) != NULL);
    T_VERIFY(uri_getPassword(uri) != NULL);
    T_VERIFY(uri_getPath(uri) == NULL);

    T_CMP(uri_getScheme(uri), "http");
    T_CMP(uri_getUser(uri), "username");
    T_CMP(uri_getPassword(uri), "password");
    T_CMP(uri_getHost(uri), "www.google.be");
    T_CMP(uri_getPort(uri), "8181");

    uri_destroy(uri);

    return 0;
}

int test_parse_authority_full_with_braces_special_chars() {
    uri_t* uri = uri_parse("pcb://username:password@ipc:[/var/run/{@test}:]");

    T_VERIFY(uri != NULL);
    T_VERIFY(uri_getScheme(uri) != NULL);
    T_VERIFY(uri_getHost(uri) != NULL);
    T_VERIFY(uri_getPort(uri) != NULL);
    T_VERIFY(uri_getUser(uri) != NULL);
    T_VERIFY(uri_getPassword(uri) != NULL);
    T_VERIFY(uri_getPath(uri) == NULL);

    T_CMP(uri_getScheme(uri), "pcb");
    T_CMP(uri_getUser(uri), "username");
    T_CMP(uri_getPassword(uri), "password");
    T_CMP(uri_getHost(uri), "ipc");
    T_CMP(uri_getPort(uri), "/var/run/{@test}:");

    uri_destroy(uri);

    return 0;
}

int test_parse_authority_full_with_host_ipv6() {
    uri_t* uri = uri_parse("http://username:password@[::1]:[8181]");

    T_VERIFY(uri != NULL);
    T_VERIFY(uri_getScheme(uri) != NULL);
    T_VERIFY(uri_getHost(uri) != NULL);
    T_VERIFY(uri_getPort(uri) != NULL);
    T_VERIFY(uri_getUser(uri) != NULL);
    T_VERIFY(uri_getPassword(uri) != NULL);
    T_VERIFY(uri_getPath(uri) == NULL);

    T_CMP(uri_getScheme(uri), "http");
    T_CMP(uri_getUser(uri), "username");
    T_CMP(uri_getPassword(uri), "password");
    T_CMP(uri_getHost(uri), "::1");
    T_CMP(uri_getPort(uri), "8181");

    uri_destroy(uri);

    return 0;
}

int test_parse_authority_full_with_port_path() {
    uri_t* uri = uri_parse("pcb://username:password@ipc:[/var/run/mysocket]");

    T_VERIFY(uri != NULL);
    T_VERIFY(uri_getScheme(uri) != NULL);
    T_VERIFY(uri_getHost(uri) != NULL);
    T_VERIFY(uri_getPort(uri) != NULL);
    T_VERIFY(uri_getUser(uri) != NULL);
    T_VERIFY(uri_getPassword(uri) != NULL);
    T_VERIFY(uri_getPath(uri) == NULL);

    T_CMP(uri_getScheme(uri), "pcb");
    T_CMP(uri_getUser(uri), "username");
    T_CMP(uri_getPassword(uri), "password");
    T_CMP(uri_getHost(uri), "ipc");
    T_CMP(uri_getPort(uri), "/var/run/mysocket");

    uri_destroy(uri);

    return 0;
}

int test_parse_authority_path() {
    uri_t* uri = uri_parse("http://www.google.be/this/is/my/path");

    T_VERIFY(uri != NULL);
    T_VERIFY(uri_getScheme(uri) != NULL);
    T_VERIFY(uri_getHost(uri) != NULL);
    T_VERIFY(uri_getPort(uri) == NULL);
    T_VERIFY(uri_getUser(uri) == NULL);
    T_VERIFY(uri_getPassword(uri) == NULL);
    T_VERIFY(uri_getPath(uri) != NULL);

    T_CMP(uri_getScheme(uri), "http");
    T_CMP(uri_getHost(uri), "www.google.be");
    T_CMP(uri_getPath(uri), "this/is/my/path");

    uri_destroy(uri);

    return 0;
}

int test_parse_authority_path_with_parameters() {
    uri_t* uri = uri_parse("http://www.google.be/this/is/my/path?param1=abcd&param2=1234&param3=echo");

    T_VERIFY(uri != NULL);
    T_VERIFY(uri_getScheme(uri) != NULL);
    T_VERIFY(uri_getHost(uri) != NULL);
    T_VERIFY(uri_getPort(uri) == NULL);
    T_VERIFY(uri_getUser(uri) == NULL);
    T_VERIFY(uri_getPassword(uri) == NULL);
    T_VERIFY(uri_getPath(uri) != NULL);

    T_CMP(uri_getScheme(uri), "http");
    T_CMP(uri_getHost(uri), "www.google.be");
    T_CMP(uri_getPath(uri), "this/is/my/path");

    T_VERIFY(uri_getParameter(uri, "param1") != NULL);
    T_VERIFY(uri_getParameter(uri, "param2") != NULL);
    T_VERIFY(uri_getParameter(uri, "param3") != NULL);
    T_VERIFY(uri_getParameter(uri, "param4") == NULL);

    T_CMP(uri_getParameter(uri, "param1"), "abcd");
    T_CMP(uri_getParameter(uri, "param2"), "1234");
    T_CMP(uri_getParameter(uri, "param3"), "echo");

    uri_destroy(uri);

    return 0;
}

int test_parse_and_build_string() {
    uri_t* uri = NULL;
    char* uri_str = NULL;

    uri = uri_parse("http://www.google.be");
    uri_str = uri_string(uri);
    T_CMP(uri_str, "http://[www.google.be]/");
    uri_destroy(uri);
    free(uri_str);

    uri = uri_parse("http://www.google.be:8181");
    uri_str = uri_string(uri);
    T_CMP(uri_str, "http://[www.google.be]:[8181]/");
    uri_destroy(uri);
    free(uri_str);

    uri = uri_parse("http://username@www.google.be");
    uri_str = uri_string(uri);
    T_CMP(uri_str, "http://username@[www.google.be]/");
    uri_destroy(uri);
    free(uri_str);

    uri = uri_parse("http://username:password@www.google.be");
    uri_str = uri_string(uri);
    T_CMP(uri_str, "http://username:password@[www.google.be]/");
    uri_destroy(uri);
    free(uri_str);

    uri = uri_parse("http://username:password@www.google.be:8181");
    uri_str = uri_string(uri);
    T_CMP(uri_str, "http://username:password@[www.google.be]:[8181]/");
    uri_destroy(uri);
    free(uri_str);

    uri = uri_parse("http://www.google.be/this/is/my/path?param1=abcd&param2=1234&param3=echo");
    uri_str = uri_string(uri);
    T_CMP(uri_str, "http://[www.google.be]/this/is/my/path?param1=abcd&param2=1234&param3=echo");
    uri_destroy(uri);
    free(uri_str);

    return 0;
}

int test_parse_empty_authority_path() {
    uri_t* uri = uri_parse("http:///is/my/path?param1=abcd&param2=1234&param3=echo");
    T_VERIFY(uri != NULL);
    uri_destroy(uri);

    return 0;
}
