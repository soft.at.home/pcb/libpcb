/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef URL_PARSEING_H
#define URL_PARSEING_H

#include <sahtest/define.h>

int test_parse_null();
int test_parse_empty_string();
int test_parse_scheme_only();
int test_parse_authority_no_host();
int test_parse_simple_authority();
int test_parse_authority_with_port();
int test_parse_authority_with_user();
int test_parse_authority_with_user_password();
int test_parse_authority_full();
int test_parse_authority_full_with_curly_braces();
int test_parse_authority_full_with_curly_braces_special_chars();
int test_parse_authority_full_with_braces();
int test_parse_authority_full_with_braces_special_chars();
int test_parse_authority_full_with_host_ipv6();
int test_parse_authority_full_with_port_path();
int test_parse_authority_path();
int test_parse_authority_path_with_parameters();
int test_parse_and_build_string();
int test_parse_empty_authority_path();

sahtest_group_begin(url_parser, NULL, NULL)
sahtest_add_test(test_parse_null, "Test parse null pointer")
sahtest_add_test(test_parse_empty_string, "Test parse empty string")
sahtest_add_test(test_parse_scheme_only, "Test parse string containing only scheme")
sahtest_add_test(test_parse_authority_no_host, "Test parse string containing authority without host")
sahtest_add_test(test_parse_simple_authority, "Test parse string containing simple authotity (host only)")
sahtest_add_test(test_parse_authority_with_port, "Test parse string containing simple authotity with port")
sahtest_add_test(test_parse_authority_with_user, "Test parse string containing simple authotity, with user name")
sahtest_add_test(test_parse_authority_with_user_password, "Test parse string containing simple authotity, with user name and password")
sahtest_add_test(test_parse_authority_full, "Test parse string containing full authority")
sahtest_add_test(test_parse_authority_full_with_curly_braces, "Test parse string containing full authority, host and port are in curly braces")
sahtest_add_test(test_parse_authority_full_with_curly_braces_special_chars, "Test parse string containing full authority, host and port are in curly braces and contains special chars")
sahtest_add_test(test_parse_authority_full_with_braces, "Test parse string containing full authority, host and port are in braces")
sahtest_add_test(test_parse_authority_full_with_braces_special_chars, "Test parse string containing full authority, host and port are in braces and contains special chars")
sahtest_add_test(test_parse_authority_full_with_host_ipv6, "Test parse string containing full authority, host is in ipv6 notation")
sahtest_add_test(test_parse_authority_full_with_port_path, "Test parse string containing full authority, port is linux path notation")
sahtest_add_test(test_parse_authority_path, "Test parsing path")
sahtest_add_test(test_parse_authority_path_with_parameters, "Test parsing path containing parameters")
sahtest_add_test(test_parse_and_build_string, "Test building uri string")
sahtest_add_test(test_parse_empty_authority_path, "Test parse an uri with empty authority and a path")
sahtest_group_end()

#endif // URL_PARSEING_H
