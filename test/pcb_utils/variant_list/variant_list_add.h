/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef VARIANT_LIST_ADD_H
#define VARIANT_LIST_ADD_H

#include <sahtest/define.h>

void variant_list_add_setup();
void variant_list_add_teardown();

int variant_list_add_variant();
int variant_list_add_char();
int variant_list_add_string();
int variant_list_add_int8();
int variant_list_add_int16();
int variant_list_add_int32();
int variant_list_add_int64();
int variant_list_add_uint8();
int variant_list_add_uint16();
int variant_list_add_uint32();
int variant_list_add_uint64();
int variant_list_add_bool();
int variant_list_add_double();
int variant_list_add_date_time();
int variant_list_add_list_move();
int variant_list_add_list_copy();
int variant_list_add_map_move();
int variant_list_add_map_copy();

sahtest_group_begin(vl_add, variant_list_add_setup, variant_list_add_teardown)
sahtest_add_test(variant_list_add_variant, "Test adding a variant")
sahtest_add_test(variant_list_add_char, "Test cadding a const char pointer")
sahtest_add_test(variant_list_add_string, "Test adding string_t")
sahtest_add_test(variant_list_add_int8, "Test adding from int8")
sahtest_add_test(variant_list_add_int16, "Test adding int16")
sahtest_add_test(variant_list_add_int32, "Test adding int32")
sahtest_add_test(variant_list_add_int64, "Test adding int64")
sahtest_add_test(variant_list_add_uint8, "Test adding uint8")
sahtest_add_test(variant_list_add_uint16, "Test adding uint16")
sahtest_add_test(variant_list_add_uint32, "Test adding uint32")
sahtest_add_test(variant_list_add_uint64, "Test adding uint64")
sahtest_add_test(variant_list_add_bool, "Test adding bool")
sahtest_add_test(variant_list_add_double, "Test adding double")
sahtest_add_test(variant_list_add_date_time, "Test adding struct tm")
sahtest_add_test(variant_list_add_list_move, "Test adding variant list, moving the elements")
sahtest_add_test(variant_list_add_list_copy, "Test adding variant list, copying the elements")
sahtest_add_test(variant_list_add_map_move, "Test adding variant map, moving the elements")
sahtest_add_test(variant_list_add_map_copy, "Test adding variant map, copying the elements")
sahtest_group_end()

#endif // VARIANT_LIST_ADD_H
