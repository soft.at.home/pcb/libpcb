/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <sahtest/sahtest.h>
#include <pcb/utils/variant_list.h>
#include <pcb/utils/variant_map.h>

static variant_t var;
static variant_list_iterator_t* it = NULL;

void variant_list_it_setup() {
    variant_initialize(&var, variant_type_unknown);
    variant_setInt32(&var, 1024);
    it = NULL;
}

void variant_list_it_teardown() {
    variant_cleanup(&var);
    variant_list_iterator_destroy(it);
    it = NULL;
}

int variant_list_it_create() {
    it = variant_list_iterator_create(&var);
    assert_bool(it != NULL);
    assert_bool(variant_type(variant_list_iterator_data(it)) == variant_type_int32);

    sahtest_success();
}

int variant_list_it_create_char() {
    it = variant_list_iterator_createChar("Hallo World");
    assert_bool(it != NULL);
    assert_bool(variant_type(variant_list_iterator_data(it)) == variant_type_string);
    assert_bool(strcmp(variant_da_char(variant_list_iterator_data(it)), "Hallo World") == 0);

    sahtest_success();
}

int variant_list_it_create_string() {
    string_t txt;
    string_initialize(&txt, 0);
    string_fromChar(&txt, "Hallo World");

    it = variant_list_iterator_createString(&txt);
    assert_bool(it != NULL);
    assert_bool(variant_type(variant_list_iterator_data(it)) == variant_type_string);
    assert_bool(strcmp(variant_da_char(variant_list_iterator_data(it)), "Hallo World") == 0);

    string_cleanup(&txt);
    sahtest_success();
}

int variant_list_it_create_int8() {
    it = variant_list_iterator_createInt8(127);
    assert_bool(it != NULL);
    assert_bool(variant_type(variant_list_iterator_data(it)) == variant_type_int8);
    assert_bool(variant_int8(variant_list_iterator_data(it)) == 127);

    sahtest_success();
}

int variant_list_it_create_int16() {
    it = variant_list_iterator_createInt16(127);
    assert_bool(it != NULL);
    assert_bool(variant_type(variant_list_iterator_data(it)) == variant_type_int16);
    assert_bool(variant_int16(variant_list_iterator_data(it)) == 127);

    sahtest_success();
}

int variant_list_it_create_int32() {
    it = variant_list_iterator_createInt32(127);
    assert_bool(it != NULL);
    assert_bool(variant_type(variant_list_iterator_data(it)) == variant_type_int32);
    assert_bool(variant_int32(variant_list_iterator_data(it)) == 127);

    sahtest_success();
}

int variant_list_it_create_int64() {
    it = variant_list_iterator_createInt64(127);
    assert_bool(it != NULL);
    assert_bool(variant_type(variant_list_iterator_data(it)) == variant_type_int64);
    assert_bool(variant_int64(variant_list_iterator_data(it)) == 127);

    sahtest_success();
}

int variant_list_it_create_uint8() {
    it = variant_list_iterator_createUInt8(127);
    assert_bool(it != NULL);
    assert_bool(variant_type(variant_list_iterator_data(it)) == variant_type_uint8);
    assert_bool(variant_uint8(variant_list_iterator_data(it)) == 127);

    sahtest_success();
}

int variant_list_it_create_uint16() {
    it = variant_list_iterator_createUInt16(127);
    assert_bool(it != NULL);
    assert_bool(variant_type(variant_list_iterator_data(it)) == variant_type_uint16);
    assert_bool(variant_uint16(variant_list_iterator_data(it)) == 127);

    sahtest_success();
}

int variant_list_it_create_uint32() {
    it = variant_list_iterator_createUInt32(127);
    assert_bool(it != NULL);
    assert_bool(variant_type(variant_list_iterator_data(it)) == variant_type_uint32);
    assert_bool(variant_uint32(variant_list_iterator_data(it)) == 127);

    sahtest_success();
}

int variant_list_it_create_uint64() {
    it = variant_list_iterator_createUInt64(127);
    assert_bool(it != NULL);
    assert_bool(variant_type(variant_list_iterator_data(it)) == variant_type_uint64);
    assert_bool(variant_uint64(variant_list_iterator_data(it)) == 127);

    sahtest_success();
}

int variant_list_it_create_bool() {
    it = variant_list_iterator_createBool(true);
    assert_bool(it != NULL);
    assert_bool(variant_type(variant_list_iterator_data(it)) == variant_type_bool);
    assert_bool(variant_bool(variant_list_iterator_data(it)) == true);

    sahtest_success();
}

int variant_list_it_create_double() {
    it = variant_list_iterator_createDouble(125.125);
    assert_bool(it != NULL);
    assert_bool(variant_type(variant_list_iterator_data(it)) == variant_type_double);
    assert_bool(variant_double(variant_list_iterator_data(it)) == 125.125);

    sahtest_success();
}

int variant_list_it_create_date_time() {
    time_t now;
    time(&now);
    struct tm* t = gmtime(&now);

    it = variant_list_iterator_createDateTime(t);
    assert_bool(it != NULL);
    assert_bool(variant_type(variant_list_iterator_data(it)) == variant_type_date_time);
    struct tm* ret = variant_dateTime(variant_list_iterator_data(it));
    assert_bool(ret != NULL);
    assert_bool(memcmp(t, ret, sizeof(struct tm)) == 0);

    free(ret);

    sahtest_success();
}

int variant_list_it_create_list_move() {
    variant_list_t list;
    assert_bool(variant_list_initialize(&list) == true);
    variant_list_addUInt32(&list, 1);
    variant_list_addUInt32(&list, 2);

    it = variant_list_iterator_createListMove(&list);
    assert_bool(it != NULL);
    assert_bool(variant_type(variant_list_iterator_data(it)) == variant_type_array);
    variant_list_t* l = variant_da_list(variant_list_iterator_data(it));
    assert_non_null(l);
    assert_bool(variant_list_size(l) == 2);
    assert_bool(variant_list_size(&list) == 0);

    variant_list_cleanup(&list);

    sahtest_success();
}

int variant_list_it_create_list_copy() {
    variant_list_t list;
    assert_bool(variant_list_initialize(&list) == true);
    variant_list_addUInt32(&list, 1);
    variant_list_addUInt32(&list, 2);

    it = variant_list_iterator_createListCopy(&list);
    assert_bool(it != NULL);
    assert_bool(variant_type(variant_list_iterator_data(it)) == variant_type_array);
    variant_list_t* l = variant_da_list(variant_list_iterator_data(it));
    assert_non_null(l);
    assert_bool(variant_list_size(l) == 2);
    assert_bool(variant_list_size(&list) == 2);

    variant_list_cleanup(&list);

    sahtest_success();
}

int variant_list_it_create_map_move() {
    variant_map_t list;
    assert_bool(variant_map_initialize(&list) == true);
    variant_map_addUInt32(&list, "1", 1);
    variant_map_addUInt32(&list, "2", 2);

    it = variant_list_iterator_createMapMove(&list);
    assert_bool(it != NULL);
    assert_bool(variant_type(variant_list_iterator_data(it)) == variant_type_map);
    variant_map_t* l = variant_da_map(variant_list_iterator_data(it));
    assert_non_null(l);
    assert_bool(variant_map_size(l) == 2);
    assert_bool(variant_map_size(&list) == 0);

    variant_map_cleanup(&list);

    sahtest_success();
}

int variant_list_it_create_map_copy() {
    variant_map_t list;
    assert_bool(variant_map_initialize(&list) == true);
    variant_map_addUInt32(&list, "1", 1);
    variant_map_addUInt32(&list, "2", 2);

    it = variant_list_iterator_createMapCopy(&list);
    assert_bool(it != NULL);
    assert_bool(variant_type(variant_list_iterator_data(it)) == variant_type_map);
    variant_map_t* l = variant_da_map(variant_list_iterator_data(it));
    assert_non_null(l);
    assert_bool(variant_map_size(l) == 2);
    assert_bool(variant_map_size(&list) == 2);

    variant_map_cleanup(&list);

    sahtest_success();
}
