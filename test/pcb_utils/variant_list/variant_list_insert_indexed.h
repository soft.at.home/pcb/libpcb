/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef VARIANT_LIST_INSERT_INDEXED_H
#define VARIANT_LIST_INSERT_INDEXED_H

#include <sahtest/define.h>

void variant_list_insert_build_list_setup();
void variant_list_insert_build_list_teardown();

int variant_list_insert_indexed_null_list();
int variant_list_insert_indexed_empty_list_index_0();
int variant_list_insert_indexed_empty_list_index_2();
int variant_list_insert_indexed_list_index_out_of_boundry();
int variant_list_insert_indexed_list_index_0();
int variant_list_insert_indexed_list_index_2();
int variant_list_insert_indexed_list_index_last();

sahtest_group_begin(vl_insert_indexed, variant_list_insert_build_list_setup, variant_list_insert_build_list_teardown)
sahtest_add_test(variant_list_insert_indexed_null_list, "Test inserting in a null list using index")
sahtest_add_test(variant_list_insert_indexed_empty_list_index_0, "Test inserting in a empty list with index 0")
sahtest_add_test(variant_list_insert_indexed_empty_list_index_2, "Test inserting in a empty list with index 2")
sahtest_add_test(variant_list_insert_indexed_list_index_out_of_boundry, "Test inserting in a list with index higher then size of list")
sahtest_add_test(variant_list_insert_indexed_list_index_0, "Test inserting in a list with index 0")
sahtest_add_test(variant_list_insert_indexed_list_index_2, "Test inserting with index set to 2")
sahtest_add_test(variant_list_insert_indexed_list_index_last, "Test inserting with index set to last item")
sahtest_group_end()


#endif // VARIANT_LIST_INSERT_INDEXED_H
