/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <stdint.h>
#include <linux/capability.h>
#include <cmocka.h>

#include "pcb/utils/privilege.h"

void priv_cap_initialize_succeeds(void** state) {
    uint32_t i = 0;
    priv_cap_t capt;

    (void) state;

    assert_true(priv_cap_initialize(&capt));
    for(i = 0; i < CAP_LAST_CAP; i++) {
        assert_false(priv_cap_isSet(&capt, i));
    }
    priv_cap_cleanup(&capt);
}

void priv_cap_invalid_cleanup_succeeds(void** state) {
    (void) state;

    priv_cap_cleanup(NULL);
}

void priv_cap_setting_valid_cap_succeeds(void** state) {
    priv_cap_t capt;

    (void) state;

    priv_cap_initialize(&capt);
    assert_true(priv_cap_set(&capt, CAP_CHOWN));
    assert_true(priv_cap_set(&capt, CAP_SYS_ADMIN));
    assert_true(priv_cap_set(&capt, CAP_SYS_TIME));
    priv_cap_cleanup(&capt);
}

void priv_cap_clearing_valid_cap_succeeds(void** state) {
    priv_cap_t capt;

    (void) state;

    priv_cap_initialize(&capt);
    priv_cap_set(&capt, CAP_CHOWN);
    /* No CAP_SYS_ADMIN set on purpose */
    priv_cap_set(&capt, CAP_SYS_TIME);
    assert_true(priv_cap_clear(&capt, CAP_CHOWN));
    assert_true(priv_cap_clear(&capt, CAP_SYS_ADMIN));
    assert_true(priv_cap_clear(&capt, CAP_SYS_TIME));
    priv_cap_cleanup(&capt);
}

void priv_cap_verifying_valid_cap_succeeds(void** state) {
    priv_cap_t capt;

    (void) state;

    priv_cap_initialize(&capt);
    priv_cap_set(&capt, CAP_CHOWN);
    /* No CAP_SYS_ADMIN set on purpose */
    priv_cap_set(&capt, CAP_SYS_TIME);
    assert_true(priv_cap_isSet(&capt, CAP_CHOWN));
    assert_false(priv_cap_isSet(&capt, CAP_SYS_ADMIN));
    assert_true(priv_cap_isSet(&capt, CAP_SYS_TIME));
    priv_cap_cleanup(&capt);
}

void priv_cap_setting_invalid_cap_fails(void** state) {
    priv_cap_t capt;

    (void) state;

    priv_cap_initialize(&capt);
    assert_false(priv_cap_set(&capt, CAP_LAST_CAP + 1));
    assert_false(priv_cap_set(&capt, 0xFFFFFFFF));
    priv_cap_cleanup(&capt);
}

void priv_cap_clearing_invalid_cap_fails(void** state) {
    priv_cap_t capt;

    (void) state;

    priv_cap_initialize(&capt);
    assert_false(priv_cap_clear(&capt, CAP_LAST_CAP + 1));
    assert_false(priv_cap_clear(&capt, 0xFFFFFFFF));
    priv_cap_cleanup(&capt);
}

void priv_cap_verifying_invalid_cap_fails(void** state) {
    priv_cap_t capt;

    (void) state;

    priv_cap_initialize(&capt);
    assert_false(priv_cap_isSet(&capt, CAP_LAST_CAP + 1));
    assert_false(priv_cap_isSet(&capt, 0xFFFFFFFF));
    priv_cap_cleanup(&capt);
}

void priv_cap_providing_invalid_cap_struct_fails(void** state) {
    (void) state;

    assert_false(priv_cap_initialize(NULL));
    assert_false(priv_cap_set(NULL, CAP_CHOWN));
    assert_false(priv_cap_clear(NULL, CAP_CHOWN));
    assert_false(priv_cap_isSet(NULL, CAP_CHOWN));
}

void priv_cap_convert_valid_cap_to_string_succeeds(void** state) {
    (void) state;

    assert_string_equal(priv_cap_toString(CAP_CHOWN), "CAP_CHOWN");
    assert_string_equal(priv_cap_toString(CAP_SYS_ADMIN), "CAP_SYS_ADMIN");
    assert_string_equal(priv_cap_toString(CAP_SYS_TIME), "CAP_SYS_TIME");
}

void priv_cap_convert_valid_cap_from_string_succeeds(void** state) {
    (void) state;

    assert_int_equal(priv_cap_fromString("CAP_CHOWN"), CAP_CHOWN);
    assert_int_equal(priv_cap_fromString("CAP_SYS_ADMIN"), CAP_SYS_ADMIN);
    assert_int_equal(priv_cap_fromString("CAP_SYS_TIME"), CAP_SYS_TIME);
}

void priv_cap_convert_invalid_cap_to_string_fails(void** state) {
    (void) state;

    assert_null(priv_cap_toString(CAP_LAST_CAP + 1));
    assert_null(priv_cap_toString(0xFFFFFFFF));
}

void priv_cap_convert_invalid_cap_from_string_fails(void** state) {
    (void) state;

    assert_int_equal(priv_cap_fromString(NULL), PRIV_CAP_FROM_STRING_INVALID);
    assert_int_equal(priv_cap_fromString("some random text"), PRIV_CAP_FROM_STRING_INVALID);
    assert_int_equal(priv_cap_fromString("CAP_MADE_UP"), PRIV_CAP_FROM_STRING_INVALID);
}

