/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <stdint.h>
#include <unistd.h>
#include <sys/types.h>
#include <string.h>
#include <linux/capability.h>
#include <assert.h>
#include <cmocka.h>

#include "pcb/utils/privilege.h"

static uint32_t priv_proc_find_cap(priv_cap_t* capt, bool isSet, int num) {
    int n = 0;
    uint32_t i = 0;

    for(i = 0; i <= CAP_LAST_CAP; i++) {
        if((priv_cap_isSet(capt, i) == isSet) && (n++ == num)) {
            return i;
        }
    }

    assert_true(i <= CAP_LAST_CAP);

    return CAP_LAST_CAP + 1;
}

// avoid dependency to whole libpcb
static uint32_t pcb_error;

int (* __wrap_capget__current)(cap_user_header_t header, cap_user_data_t data);
int __wrap_capget(cap_user_header_t header, cap_user_data_t data) {
    return (*__wrap_capget__current)(header, data);
}

static int __wrap_capget_succeed(cap_user_header_t header, cap_user_data_t data) {
    return 0;
}

extern int __real_capget(cap_user_header_t header, cap_user_data_t data);
int __wrap_capget_forward(cap_user_header_t header, cap_user_data_t data) {
    return (*__real_capget)(header, data);
}

static struct __user_cap_data_struct __wrap_capget_result;
static int __wrap_capget_mock(cap_user_header_t header, cap_user_data_t data) {
    if(data) {
        data->effective = __wrap_capget_result.effective;
        data->permitted = __wrap_capget_result.permitted;
        data->inheritable = __wrap_capget_result.inheritable;
    }
    return (int) mock();
}

int (* __wrap_capset__current)(cap_user_header_t header, cap_user_data_t data);
int __wrap_capset(cap_user_header_t header, cap_user_data_t data) {
    return (*__wrap_capset__current)(header, data);
}

static int __wrap_capset_pass(cap_user_header_t header, cap_user_data_t data) {
    return 0;
}

extern int __real_capset(cap_user_header_t header, cap_user_data_t data);
int __wrap_capset_forward(cap_user_header_t header, cap_user_data_t data) {
    return (*__real_capset)(header, data);
}

static int __wrap_capset_succeed(cap_user_header_t header, cap_user_data_t data) {
    return 0;
}
static int __wrap_capset_mock(cap_user_header_t header, cap_user_data_t data) {
    return (int) mock();
}
static int __wrap_capset_mock_checked(cap_user_header_t header, cap_user_data_t data) {
    check_expected(data);
    return (int) mock();
}

static bool assert_cap_data_equal(
    struct __user_cap_data_struct* actual,
    struct __user_cap_data_struct* expected
    ) {
    bool success = true;
    assert_int_equal(actual->effective, expected->effective);
    assert_int_equal(actual->permitted, expected->permitted);
    assert_int_equal(actual->inheritable, expected->inheritable);
    return !memcmp(actual, expected, sizeof(*actual));
}

static int mock_expect_cap_data_equal(const LargestIntegralType value,
                                      const LargestIntegralType check_value_data) {
    struct __user_cap_data_struct* actual = (struct __user_cap_data_struct*) value;
    struct __user_cap_data_struct* expected = (struct __user_cap_data_struct*) check_value_data;
    assert(actual);
    assert(expected);

    return assert_cap_data_equal(actual, expected) ? 1 : 0;
}

void priv_proc_get_valid_caps_succeeds(void** state) {
    (void) state;

    priv_cap_t capt;
    assert(priv_proc_initialize());

    assert(priv_cap_initialize(&capt));

    {
        __wrap_capget__current = __wrap_capget_mock;
        __wrap_capget_result.effective = 0b11;
        will_return(__wrap_capget_mock, 0);

        assert_true(priv_proc_getCap(&capt));

        __wrap_capget__current = __wrap_capget_succeed;
    }

    assert_true(priv_cap_isSet(&capt, 0));
    assert_true(priv_cap_isSet(&capt, 1));
    assert_true(!priv_cap_isSet(&capt, 2));

    priv_cap_cleanup(&capt);
    priv_proc_cleanup();
}

void priv_proc_get_invalid_caps_fails(void** state) {
    (void) state;

    assert(priv_proc_initialize());

    assert_false(priv_proc_getCap(NULL));

    priv_proc_cleanup();
}

void priv_proc_set_valid_caps_succeeds(void** state) {
    uint32_t i = 0, to_clear = CAP_LAST_CAP + 1;
    priv_cap_t capt;

    (void) state;

    priv_proc_initialize();
    priv_cap_initialize(&capt);

    assert_true(priv_proc_getCap(&capt));
    assert_true(priv_proc_setCap(&capt));

    __wrap_capset__current = __wrap_capset_succeed;
    __wrap_capget__current = __wrap_capget_mock;

    __wrap_capget_result.effective = 0b1111;
    __wrap_capget_result.permitted = 0b1111;
    __wrap_capget_result.inheritable = 0b0;
    will_return(__wrap_capget_mock, 0);
    assert_true(priv_proc_getCap(&capt));
    __wrap_capget__current = __wrap_capget_succeed;

    for(int to_clear = 0; to_clear != 4; ++to_clear) {
        assert_true(priv_cap_isSet(&capt, to_clear));
        assert_true(priv_cap_clear(&capt, to_clear));
        assert_false(priv_cap_isSet(&capt, to_clear));

        assert_true(priv_proc_setCap(&capt));
    }

    priv_cap_cleanup(&capt);
    priv_proc_cleanup();
}

void priv_proc_set_invalid_caps_fails(void** state) {
    priv_cap_t capt;

    (void) state;

    priv_proc_initialize();
    priv_cap_initialize(&capt);

    assert_false(priv_proc_setCap(NULL));

    priv_cap_cleanup(&capt);
    priv_proc_cleanup();
}

void priv_proc_set_unauthorized_cap_fails(void** state) {
    (void) state;

    priv_proc_initialize();

    priv_cap_t capt;
    priv_cap_initialize(&capt);

    __wrap_capset__current = __wrap_capset_succeed;
    __wrap_capget__current = __wrap_capget_mock;

    __wrap_capget_result.effective = 0b0001;
    __wrap_capget_result.permitted = 0b0001;
    __wrap_capget_result.inheritable = 0b0;
    will_return(__wrap_capget_mock, 0);
    assert_true(priv_proc_getCap(&capt));
    __wrap_capget__current = __wrap_capget_succeed;

    assert_true(priv_cap_set(&capt, 1));
    assert_true(priv_cap_isSet(&capt, 1));

    __wrap_capset__current = __wrap_capset_mock;
    will_return(__wrap_capset_mock, 1);
    assert_false(priv_proc_setCap(&capt));

    priv_cap_cleanup(&capt);
    priv_proc_cleanup();
}

extern int __real_getgrouplist(const char* user, gid_t group, gid_t* groups, int* ngroups);
int (* __wrap_getgrouplist__current)(const char* user, gid_t group, gid_t* groups, int* ngroups);

int __wrap_getgrouplist(const char* user, gid_t group, gid_t* groups, int* ngroups) {
    return __wrap_getgrouplist__current(user, group, groups, ngroups);
}
struct __wrap_getgrouplist__const_result_t {
    int returnvalue;
    gid_t* groups;
    int ngroups;
} __wrap_getgrouplist__const_result;

static int __wrap_getgrouplist__const(const char* user, gid_t group, gid_t* groups, int* ngroups) {
    for(int i = 0; i != __wrap_getgrouplist__const_result.ngroups; ++i) {
        groups[i] = __wrap_getgrouplist__const_result.groups[i];
    }
    *ngroups = __wrap_getgrouplist__const_result.ngroups;
    return __wrap_getgrouplist__const_result.returnvalue;
}

extern int __real_setgroups(size_t size, const gid_t* list);
int (* __wrap_setgroups__current)(size_t size, const gid_t* list);

int __wrap_setgroups(size_t size, const gid_t* list) {
    return __wrap_setgroups__current(size, list);
}
int __wrap_setgroups__const(size_t size, const gid_t* list) {
    (void) size;
    (void) list;
    return 0;
}
int __wrap_setgroups__checked(size_t size, const gid_t* list) {
    check_expected(size);
    check_expected(list);
    return 0;
}

void priv_proc_drop_privileges_succeeds(void** state) {
    uint32_t i = 0, to_keep = CAP_LAST_CAP + 1;
    priv_cap_t capt, retain;

    (void) state;

    priv_proc_initialize();
    priv_cap_initialize(&capt);
    priv_cap_initialize(&retain);

    /* STEP 1: Find three caps we want to keep. */
    assert_true(priv_proc_getCap(&capt));
    to_keep = priv_proc_find_cap(&capt, true, 1);
    assert_true(priv_cap_set(&retain, to_keep));
    to_keep = priv_proc_find_cap(&capt, true, 4);
    assert_true(priv_cap_set(&retain, to_keep));
    to_keep = priv_proc_find_cap(&capt, true, 8);
    assert_true(priv_cap_set(&retain, to_keep));

    assert_int_equal(getuid(), 0);
    assert_int_equal(getgid(), 0);
    assert_int_equal(geteuid(), 0);
    assert_int_equal(getegid(), 0);

    /* STEP 2: Drop privileges with cap retention. */
    assert_false(priv_proc_dropPrivileges("invaliduser", "invalidgroup", &retain));
    assert_false(priv_proc_dropPrivileges("invaliduser", NULL, &retain));
    assert_false(priv_proc_dropPrivileges(NULL, "invalidgroup", &retain));
    assert_false(priv_proc_dropPrivileges("invaliduser", "mygroup", &retain));
    assert_false(priv_proc_dropPrivileges("myuser", "invalidgroup", &retain));
    assert_true(priv_proc_dropPrivileges("myuser", "mygroup", &retain));
    assert_true(priv_proc_dropPrivileges(NULL, NULL, &retain));

    assert_true(priv_proc_getCap(&capt));
    for(i = 0; i <= CAP_LAST_CAP; i++) {
        assert_true(priv_cap_isSet(&retain, i) == priv_cap_isSet(&capt, i));
    }
    assert_int_not_equal(getuid(), 0);
    assert_int_not_equal(getgid(), 0);
    assert_int_not_equal(geteuid(), 0);
    assert_int_not_equal(getegid(), 0);

    /* STEP 3: Drop caps some more. */
    to_keep = priv_proc_find_cap(&retain, true, 2);
    assert_true(priv_cap_clear(&retain, to_keep));

    __wrap_getgrouplist__current = __wrap_getgrouplist__const;
    __wrap_setgroups__current = __wrap_setgroups__const;
    assert_true(priv_proc_dropPrivileges(NULL, NULL, &retain));

    assert_true(priv_proc_getCap(&capt));
    for(i = 0; i <= CAP_LAST_CAP; i++) {
        assert_true(priv_cap_isSet(&retain, i) == priv_cap_isSet(&capt, i));
    }
    assert_int_not_equal(getuid(), 0);
    assert_int_not_equal(getgid(), 0);
    assert_int_not_equal(geteuid(), 0);
    assert_int_not_equal(getegid(), 0);

    /* STEP 4: Drop all caps. */
    assert_true(priv_proc_dropPrivileges("myuser", "mygroup", NULL));
    assert_true(priv_proc_dropPrivileges(NULL, NULL, NULL));

    assert_true(priv_proc_getCap(&capt));
    for(i = 0; i <= CAP_LAST_CAP; i++) {
        assert_false(priv_cap_isSet(&capt, i));
    }
    assert_int_not_equal(getuid(), 0);
    assert_int_not_equal(getgid(), 0);
    assert_int_not_equal(geteuid(), 0);
    assert_int_not_equal(getegid(), 0);

    priv_cap_cleanup(&capt);
    priv_cap_cleanup(&retain);
    priv_proc_cleanup();
}

static void priv_proc_drop_privileges_sets_supplementary_groups(void** state) {
    priv_cap_t retain;
    assert_true(priv_cap_initialize(&retain));

    priv_proc_initialize();
    gid_t groups[] = {126, 2001, 3002, 4003};
    const size_t group_size = sizeof(groups) / sizeof(groups[0]);
    __wrap_getgrouplist__const_result.returnvalue = group_size;
    __wrap_getgrouplist__const_result.groups = groups;
    __wrap_getgrouplist__const_result.ngroups = group_size;
    __wrap_getgrouplist__current = __wrap_getgrouplist__const;
    // expect the supplementary groups for "user" to be queried -> return [group1, group2]
    // expect setgroups to be called with [group1, group2]
    __wrap_setgroups__current = __wrap_setgroups__checked;
    expect_value(__wrap_setgroups__checked, size, group_size);
    expect_memory(__wrap_setgroups__checked, list, groups, sizeof(groups));
    assert_true(priv_proc_dropPrivileges("myuser", "mygroup", &retain));

    priv_proc_cleanup();
}

static void test_when_capset_fails_priv_proc_setCap_fails(void** state) {
    (void) state;

    priv_cap_t capt;

    (void) state;

    assert(priv_proc_initialize());
    assert(priv_cap_initialize(&capt));

    assert(priv_proc_getCap(&capt));

    {
        __wrap_capset__current = __wrap_capset_mock;
        will_return(__wrap_capset_mock, -1);

        assert_false(priv_proc_setCap(&capt));

        __wrap_capset__current = __wrap_capset_pass;
    }

    priv_cap_cleanup(&capt);
    priv_proc_cleanup();

}

static void test_set_caps_are_forwarded_to_setCap(void** state) {
    (void) state;


    priv_cap_t capt;

    (void) state;

    assert(priv_proc_initialize());
    assert(priv_cap_initialize(&capt));

    __wrap_capget__current = __wrap_capget_mock;
    __wrap_capget_result.effective = 0b00000;
    __wrap_capget_result.permitted = 0b01010;
    __wrap_capget_result.inheritable = 0b00000;
    will_return(__wrap_capget_mock, 0);
    assert(priv_proc_getCap(&capt));
    __wrap_capget__current = __wrap_capget_succeed;


    {
        assert(!priv_cap_isSet(&capt, 0));
        assert(!priv_cap_isSet(&capt, 1));
        assert(!priv_cap_isSet(&capt, 2));
        assert(!priv_cap_isSet(&capt, 3));
        assert(priv_cap_set(&capt, 3));
        assert(priv_cap_set(&capt, 2));

        struct __user_cap_data_struct expected = {
            .effective = 0b1100,
            .permitted = 0b1100,
            .inheritable = __wrap_capget_result.inheritable,
        };
        __wrap_capset__current = __wrap_capset_mock_checked;
        expect_check(__wrap_capset_mock_checked, data, mock_expect_cap_data_equal, &expected);
        will_return(__wrap_capset_mock_checked, 0);

        assert(priv_proc_setCap(&capt));

        __wrap_capset__current = __wrap_capset_pass;
    }

    priv_cap_cleanup(&capt);
    priv_proc_cleanup();
}

//typedef int (*CMFixtureFunction)(void **state);
static int init_mocks(void** state) {
    (void) state;
    __wrap_capget__current = __wrap_capget_forward;
    __wrap_capset__current = __wrap_capset_forward;
    __wrap_getgrouplist__current = __real_getgrouplist;
    __wrap_setgroups__current = __real_setgroups;
    return 0;
}

int main(int argc, char** argv) {

    const struct CMUnitTest tests[] = {
        cmocka_unit_test_setup(priv_proc_get_valid_caps_succeeds, init_mocks),
        cmocka_unit_test_setup(priv_proc_get_invalid_caps_fails, init_mocks),
        cmocka_unit_test_setup(priv_proc_set_valid_caps_succeeds, init_mocks),
        cmocka_unit_test_setup(priv_proc_set_invalid_caps_fails, init_mocks),
        cmocka_unit_test_setup(priv_proc_drop_privileges_succeeds, init_mocks),
        cmocka_unit_test_setup(priv_proc_drop_privileges_sets_supplementary_groups, init_mocks),
        cmocka_unit_test_setup(priv_proc_set_unauthorized_cap_fails, init_mocks),
        cmocka_unit_test_setup(test_when_capset_fails_priv_proc_setCap_fails, init_mocks),
        cmocka_unit_test_setup(test_set_caps_are_forwarded_to_setCap, init_mocks),
    };
    return cmocka_run_group_tests(tests, NULL, NULL);
}
