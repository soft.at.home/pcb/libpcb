/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <stdint.h>
#include <linux/capability.h>
#include <cmocka.h>

#include <components.h>
#include "pcb/utils/privilege.h"

void priv_default_verify_defaults(void** state) {
    uint32_t i = 0;
    const char* str = NULL;
    const priv_cap_t* retain = NULL;

    (void) state;

    priv_proc_initialize();

    str = priv_get_defaultUser();
#ifdef CONFIG_SAH_LIB_PCB_PRIV_DROP_DEFAULT_USERNAME
    assert_string_equal(str, CONFIG_SAH_LIB_PCB_PRIV_DROP_DEFAULT_USERNAME);
#else
    assert_null(str);
#endif

    str = priv_get_defaultGroup();
#ifdef CONFIG_SAH_LIB_PCB_PRIV_DROP_DEFAULT_GROUPNAME
    assert_string_equal(str, CONFIG_SAH_LIB_PCB_PRIV_DROP_DEFAULT_GROUPNAME);
#else
    assert_null(str);
#endif

    retain = priv_get_defaultRetain();
    assert_non_null(retain);
    for(i = 0; i <= CAP_LAST_CAP; i++) {
        assert_false(priv_cap_isSet(retain, i));
    }

    priv_proc_cleanup();
}

void priv_default_setting_valid_defaults_succeeds(void** state) {
    uint32_t i = 0;
    const char* user = "myuser", * group = "mygroup", * str = NULL;
    priv_cap_t retain;
    const priv_cap_t* new_retain = NULL;

    (void) state;

    priv_proc_initialize();
    priv_cap_initialize(&retain);
    priv_cap_set(&retain, CAP_NET_RAW);

    assert_true(priv_set_defaultUser(user));
    str = priv_get_defaultUser();
    assert_non_null(str);
    assert_string_equal(str, user);

    assert_true(priv_set_defaultGroup(group));
    str = priv_get_defaultGroup();
    assert_non_null(str);
    assert_string_equal(str, group);

    assert_true(priv_set_defaultRetain(&retain));
    new_retain = priv_get_defaultRetain();
    assert_non_null(new_retain);
    for(i = 0; i <= CAP_LAST_CAP; i++) {
        if(i == CAP_NET_RAW) {
            assert_true(priv_cap_isSet(new_retain, i));
        } else {
            assert_false(priv_cap_isSet(new_retain, i));
        }
    }

    priv_cap_cleanup(&retain);
    priv_proc_cleanup();
}

void priv_default_clearing_defaults_succeeds(void** state) {
    uint32_t i = 0;
    const char* user = "myuser", * group = "mygroup", * str = NULL;
    priv_cap_t retain;
    const priv_cap_t* new_retain = NULL;

    (void) state;

    priv_proc_initialize();
    priv_cap_initialize(&retain);
    priv_cap_set(&retain, CAP_NET_RAW);

    assert_true(priv_set_defaultUser(user));
    assert_true(priv_set_defaultUser(NULL));
    str = priv_get_defaultUser();
    assert_null(str);

    assert_true(priv_set_defaultGroup(group));
    assert_true(priv_set_defaultGroup(NULL));
    str = priv_get_defaultGroup();
    assert_null(str);

    assert_true(priv_set_defaultRetain(&retain));
    assert_true(priv_set_defaultRetain(NULL));
    new_retain = priv_get_defaultRetain();
    assert_non_null(new_retain);
    for(i = 0; i <= CAP_LAST_CAP; i++) {
        assert_false(priv_cap_isSet(new_retain, i));
    }

    priv_cap_cleanup(&retain);
    priv_proc_cleanup();
}

void priv_default_setting_invalid_defaults_fails(void** state) {
    const char* user = "myuser", * group = "mygroup", * str = NULL;
    const char* inv_user = "invaliduser", * inv_group = "invalidgroup";

    (void) state;

    priv_proc_initialize();

    assert_true(priv_set_defaultUser(user));
    assert_false(priv_set_defaultUser(inv_user));
    str = priv_get_defaultUser();
    assert_null(str);

    assert_true(priv_set_defaultGroup(group));
    assert_false(priv_set_defaultGroup(inv_group));
    str = priv_get_defaultGroup();
    assert_null(str);

    /*
     * The 'retain' argument can not be invalid, otherwise the priv_cap_XXX() API
     * would already complain and not update the priv_cap_t structure.
     * (Disclaimer: at least with what we can test. In C, any pointer can be invalid.)
     */

    priv_proc_cleanup();
}
