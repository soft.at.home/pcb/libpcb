/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "datetime_tests_cmocka.h"
#include "pcb/utils/datetime.h"
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <mcheck.h>
#include <stdio.h>


static void test_datetime_conversion_fromdatetime(UNUSED void** state) {
    // our reference time
    const pcb_datetime_t dt = { .datetime = { .tm_mday = 18, .tm_mon = 7 - 1, .tm_year = 2019 - 1900, .tm_sec = 13}, .nanoseconds = 123456789UL };
    //$ date -d '2019-07-18T00:00:13.123456789' -u "+%s.%N"
    //1563408013.123456789
    time_t reference_tim = (time_t) 1563408013UL;
    uint32_t reference_ns = (uint32_t) 123456789UL;
    struct timeval tv, * ptv;
    struct timespec ts, * pts;
    time_t tim;
    struct tm tm = {0};
    struct tm* ptm;
    const struct tm* ptm2;
    uint32_t ns;

    // pcb_datetime_to* functions
    pcb_datetime_toTimespec(&dt, &ts);
    assert_int_equal(ts.tv_sec, reference_tim);
    assert_int_equal(ts.tv_nsec, reference_ns);

    pcb_datetime_toTimeval(&dt, &tv);
    assert_int_equal(tv.tv_sec, reference_tim);
    assert_int_equal(tv.tv_usec, reference_ns / 1000);

    pcb_datetime_toTime(&dt, &tim);
    assert_int_equal(tim, reference_tim);

    pcb_datetime_toTm(&dt, &tm);
    struct tm copy = tm;
    assert_int_equal(timegm(&copy), reference_tim);
    assert_memory_equal(&dt.datetime, &tm, sizeof(struct tm));

    // allocating functions
    pts = pcb_datetime_timespec(&dt);
    assert_non_null(pts);
    assert_int_equal(pts->tv_sec, reference_tim);
    assert_int_equal(pts->tv_nsec, reference_ns);
    free(pts);

    ptv = pcb_datetime_timeval(&dt);
    assert_non_null(ptv);
    assert_int_equal(ptv->tv_sec, reference_tim);
    assert_int_equal(ptv->tv_usec, reference_ns / 1000);
    free(ptv);

    ptm = pcb_datetime_tm(&dt);
    assert_non_null(ptm);
    copy = *ptm;
    assert_int_equal(timegm(&copy), reference_tim);
    assert_memory_equal(&dt.datetime, ptm, sizeof(struct tm));
    free(ptm);

    //integer and direct access methods
    tim = pcb_datetime_time(&dt);
    assert_int_equal(tim, reference_tim);

    ns = pcb_datetime_nanoseconds(&dt);
    assert_int_equal(ns, reference_ns);

    ptm2 = pcb_datetime_da_tm(&dt);
    assert_ptr_equal(ptm2, &dt.datetime);
}


static void test_datetime_conversion_todatetime(UNUSED void** state) {
    // our reference time
    //$ date -d '2019-07-18T00:00:13.123456789' -u "+%s.%N"
    //1563408013.123456789
    const pcb_datetime_t reference_dt_ns = { .datetime = { .tm_mday = 18, .tm_mon = 7 - 1, .tm_year = 2019 - 1900, .tm_sec = 13}, .nanoseconds = 123456789UL };
    const pcb_datetime_t reference_dt_us = { .datetime = { .tm_mday = 18, .tm_mon = 7 - 1, .tm_year = 2019 - 1900, .tm_sec = 13}, .nanoseconds = 123456000UL };
    const pcb_datetime_t reference_dt_s = { .datetime = { .tm_mday = 18, .tm_mon = 7 - 1, .tm_year = 2019 - 1900, .tm_sec = 13}, .nanoseconds = 0 };
    const struct timespec reference_ts = { .tv_sec = (time_t) 1563408013UL, .tv_nsec = 123456789L };
    const struct timeval reference_tv = { .tv_sec = (time_t) 1563408013UL, .tv_usec = 123456L };
    const time_t reference_tim = (time_t) 1563408013UL;
    const uint32_t reference_ns = (uint32_t) 123456789UL;
    pcb_datetime_t dt;

    // pcb_datetime_from* functions
    pcb_datetime_fromTimespec(&dt, &reference_ts);
    assert_true(pcb_datetime_equals(&dt, &reference_dt_ns));

    pcb_datetime_fromTimeval(&dt, &reference_tv);
    assert_true(pcb_datetime_equals(&dt, &reference_dt_us));

    pcb_datetime_fromTime(&dt, reference_tim);
    assert_true(pcb_datetime_equals(&dt, &reference_dt_s));

    pcb_datetime_fromTm(&dt, &reference_dt_ns.datetime);
    assert_true(pcb_datetime_equals(&dt, &reference_dt_s));
}

// our reference time, stored in 5 different ways
//$ date -d '2019-07-18T00:00:13.123456789' -u "+%s.%N"
//1563408013.123456789
const pcb_datetime_t reference_dt_us = { .datetime = { .tm_mday = 18, .tm_mon = 7 - 1, .tm_year = 2019 - 1900, .tm_sec = 13}, .nanoseconds = 123456000UL };
const pcb_datetime_t reference_dt_s = { .datetime = { .tm_mday = 18, .tm_mon = 7 - 1, .tm_year = 2019 - 1900, .tm_sec = 13}, .nanoseconds = 0 };
const char reference_str_us[] = "2019-07-18T00:00:13.123456Z";
const char reference_str_us_trimmed[] = "2019-07-18T00:00:13.000000Z";
const char reference_str_s[] = "2019-07-18T00:00:13Z";

static void test_datetime_conversion_tostring(UNUSED void** state) {
    char buffer[50];

    // dt -> str
    pcb_datetime_toIso(&reference_dt_s, buffer, sizeof buffer);
    assert_string_equal(buffer, reference_str_s);
    pcb_datetime_toIso(&reference_dt_us, buffer, sizeof buffer);
    assert_string_equal(buffer, reference_str_s);

    pcb_datetime_toIsoUs(&reference_dt_us, buffer, sizeof buffer);
    assert_string_equal(buffer, reference_str_us);
    pcb_datetime_toIsoUs(&reference_dt_s, buffer, sizeof buffer);
    assert_string_equal(buffer, reference_str_us_trimmed);

    pcb_datetime_toChar(&reference_dt_us, buffer, sizeof buffer);
    assert_string_equal(buffer, reference_str_us);
    pcb_datetime_toChar(&reference_dt_s, buffer, sizeof buffer);
    assert_string_equal(buffer, reference_str_s);

    //invalid arguments
    assert_int_equal(pcb_datetime_toIso(NULL, buffer, sizeof buffer), 0);
    assert_int_equal(pcb_datetime_toIsoUs(&reference_dt_s, buffer, 0), 0);
    assert_int_equal(pcb_datetime_toChar(&reference_dt_s, NULL, sizeof buffer), 0);
}

static void test_datetime_conversion_fromstring(UNUSED void** state) {
    pcb_datetime_t dt;
    // str -> dt
    pcb_datetime_fromChar(&dt, reference_str_us);
    assert_true(pcb_datetime_equals(&reference_dt_us, &dt));
    pcb_datetime_fromChar(&dt, reference_str_s);
    assert_true(pcb_datetime_equals(&reference_dt_s, &dt));

    // invalid arguments
    assert_null(pcb_datetime_fromChar(&dt, NULL));
    assert_null(pcb_datetime_fromChar(NULL, reference_str_us));
    assert_null(pcb_datetime_fromChar(&dt, "XYZ"));

    // partial format
    assert_non_null(pcb_datetime_fromChar(&dt, "2022-03"));  // Only year and month
    assert_int_equal(dt.datetime.tm_year, 2022 - 1900);
    assert_int_equal(dt.datetime.tm_mon, 3 - 1);
    assert_int_equal(dt.datetime.tm_mday, 0);       // Other parts should be set to 0
    assert_int_equal(dt.datetime.tm_hour, 0);       // Other parts should be set to 0
    assert_int_equal(dt.datetime.tm_min, 0);        // Other parts should be set to 0
    assert_int_equal(dt.datetime.tm_sec, 0);        // Other parts should be set to 0
    assert_int_equal(dt.nanoseconds, 0);            // Other parts should be set to 0
}

static const struct CMUnitTest tests[] = {
    cmocka_unit_test(test_datetime_conversion_fromdatetime),
    cmocka_unit_test(test_datetime_conversion_todatetime),
    cmocka_unit_test(test_datetime_conversion_fromstring),
    cmocka_unit_test(test_datetime_conversion_tostring),
};

__attribute__((constructor)) void datetime_conversion_register(void) {
    register_test_group("datetime_conversion", tests, ARRAY_SIZE(tests), NULL, NULL);
}

