/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#define _GNU_SOURCE
#include "datetime_tests_cmocka.h"
#include "pcb/utils/datetime.h"
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <mcheck.h>
#include <stdio.h>

struct test {
    char as_string[100];
    char format[100];
    pcb_datetime_t as_struct;
};

static char* to_iso(const pcb_datetime_t* dt) {
    char* s = NULL;
    !!asprintf(&s, "%04d-%02d-%02dT%02d:%02d:%02d.%09uZ",
               dt->datetime.tm_year + 1900, dt->datetime.tm_mon + 1, dt->datetime.tm_mday, dt->datetime.tm_hour, dt->datetime.tm_min, dt->datetime.tm_sec, dt->nanoseconds);
    return s;
}

static struct test basic_tests[] =
{
    { "hello", "hello", { .datetime = {.tm_sec = 0}} },
    { "03", "%S", { .datetime = {.tm_sec = 3}, .nanoseconds = 0 }},
    { "03 September", "%d %B", { .datetime = {.tm_mday = 3, .tm_mon = 9 - 1}, .nanoseconds = 0 }},
    { "2017-08-09", "%Y-%m-%d", { .datetime = { .tm_mday = 9, .tm_mon = 8 - 1, .tm_year = 2017 - 1900} }},
    { "0001-01-01", "%Y-%m-%d", { .datetime = { .tm_mday = 1, .tm_mon = 1 - 1, .tm_year = 1 - 1900} }},
    { "2017-08-09T03:04:05Z", "%FT%H:%M:%SZ", { .datetime = { .tm_mday = 9, .tm_mon = 8 - 1, .tm_year = 2017 - 1900, .tm_hour = 03, .tm_min = 04, .tm_sec = 05} }},
    { "2017-08-09T03:04:05.000000Z", "%FT%H:%M:%iZ", { .datetime = { .tm_mday = 9, .tm_mon = 8 - 1, .tm_year = 2017 - 1900, .tm_hour = 03, .tm_min = 04, .tm_sec = 05} }},
    { "2017-08-09T03:04:05.000001Z", "%FT%H:%M:%iZ", { .datetime = { .tm_mday = 9, .tm_mon = 8 - 1, .tm_year = 2017 - 1900, .tm_hour = 03, .tm_min = 04, .tm_sec = 05}, .nanoseconds = 1000UL}},
    { "2017-08-09T03:04:05.033000Z", "%FT%H:%M:%iZ", { .datetime = { .tm_mday = 9, .tm_mon = 8 - 1, .tm_year = 2017 - 1900, .tm_hour = 03, .tm_min = 04, .tm_sec = 05}, .nanoseconds = 33000000UL }},
    { "44.123456", "%i", { .datetime = { .tm_sec = 44}, .nanoseconds = 123456000UL }},
    { "44 seconds 123456789 nanoseconds", "%S seconds %N nanoseconds", { .datetime = { .tm_sec = 44}, .nanoseconds = 123456789UL }},
    { "123456 microseconds", "%f microseconds", { .datetime = { .tm_sec = 0}, .nanoseconds = 123456000UL }},
//edge cases
    { "1900", "%Y", { .datetime = {0} }},
// No Y2038 bugs, please
    { "\U0001F3BC He said \"I've been to the year 3000, not much a change but they live under water\"",
        "\U0001F3BC He said \"I've been to the year %Y, not much a change but they live under water\"",
        { .datetime = { .tm_year = 3000 - 1900} }},
    { "9999-12-31T23:59:59.999876543", "%FT%H:%M:%S.%N",
        { .datetime = { .tm_mday = 31, .tm_mon = 12 - 1, .tm_year = 9999 - 1900, .tm_hour = 23, .tm_min = 59, .tm_sec = 59}, .nanoseconds = 999876543UL }},
// leap year
    { "2020-02-29", "%F", { .datetime = { .tm_mday = 29, .tm_mon = 2 - 1, .tm_year = 2020 - 1900}}},
};
static size_t numTests = ARRAY_SIZE(basic_tests);

static void test_datetime_basic_format_parse(UNUSED void** state) {
    int i;
    for(i = 0; i < (int) numTests; ++i) {
        pcb_datetime_t tmp_struct;
        const char* e = pcb_strptime(basic_tests[i].as_string, basic_tests[i].format, &tmp_struct);
        char* o1 = to_iso(&tmp_struct);
        char* o2 = to_iso(&basic_tests[i].as_struct);
        assert_string_equal(o1, o2);
        free(o1);
        free(o2);
        assert_non_null(e);
        assert_true(pcb_datetime_equals(&tmp_struct, &basic_tests[i].as_struct));

        char tmp_s[200];
        size_t rv = pcb_strftime(tmp_s, sizeof tmp_s, basic_tests[i].format, &basic_tests[i].as_struct);
        assert_string_equal(tmp_s, basic_tests[i].as_string);
        assert_int_equal(rv, strlen(tmp_s));
    }
}

static void test_datetime_format_fcall(UNUSED void** state) {
    char buffer[50] = "";
    pcb_datetime_t dt = { .datetime = { .tm_mday = 18, .tm_mon = 7 - 1, .tm_year = 2019 - 1900, .tm_sec = 13}, .nanoseconds = 123456789UL };
    size_t rv;

    // Test wrong arguments
    rv = pcb_strftime(NULL, 500, "%F", &dt);                // NULL first argument
    assert_int_equal(rv, 0);
    rv = pcb_strftime(buffer, sizeof buffer, "%F", NULL);   // NULL input datetime
    assert_int_equal(rv, 0);
    assert_string_equal(buffer, "");
    rv = pcb_strftime(buffer, sizeof buffer, "%&", &dt);    // %& is not a valid format specifier
    assert_int_equal(rv, 2);
    assert_string_equal(buffer, "%&");
    memset(buffer, '\0', sizeof buffer);
    rv = pcb_strftime(buffer, 0, "%F", &dt);                // Zero-sized buffer
    assert_int_equal(rv, 0);
    assert_string_equal(buffer, "");
    rv = pcb_strftime(buffer, 1, "%Y", &dt);                // Buffer not big enough
    assert_int_equal(rv, 0);
    assert_string_equal(buffer, "");

    // Test return value matches string length
    rv = pcb_strftime(buffer, sizeof buffer, "%F", &dt);
    assert_string_equal(buffer, "2019-07-18");
    assert_int_equal(rv, strlen(buffer));
    assert_int_equal(rv, sizeof("2019-07-18") - 1);
    rv = pcb_strftime(buffer, sizeof buffer, "%N", &dt);
    assert_string_equal(buffer, "123456789");
    assert_int_equal(rv, strlen(buffer));
    rv = pcb_strftime(buffer, sizeof buffer, "%i", &dt);
    assert_string_equal(buffer, "13.123456");
    assert_int_equal(rv, strlen(buffer));

    // Test truncation
    rv = pcb_strftime(buffer, 5, "%Y-%m-%d", &dt);  // Only enough space in buffer to store the year
    assert_string_equal(buffer, "2019");
    assert_int_equal(rv, 4);
    rv = pcb_strftime(buffer, 7, "%Y-%m-%d", &dt);  // Only enough space in buffer to store the year and the '-'
    assert_string_equal(buffer, "2019-");
    assert_int_equal(rv, 5);
    rv = pcb_strftime(buffer, 5, "%N", &dt);  // Not enough space to store nanoseconds
    assert_string_equal(buffer, "");
    assert_int_equal(rv, 0);
}


static const struct CMUnitTest tests[] = {
    cmocka_unit_test(test_datetime_basic_format_parse),
    cmocka_unit_test(test_datetime_format_fcall),
};

__attribute__((constructor)) void datetime_formatting_register(void) {
    register_test_group("datetime_formatting", tests, ARRAY_SIZE(tests), NULL, NULL);
}

