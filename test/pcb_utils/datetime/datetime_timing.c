/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "datetime_tests_cmocka.h"
#include "pcb/utils/datetime.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <limits.h>

// Test that initializing a pcb_datetime_t yields a date of 1970-01-01T00:00:00Z with any method.
static void test_datetime_init(UNUSED void** state) {
    pcb_datetime_t reference1 = {.datetime = {.tm_year = 70, .tm_mday = 1}, .nanoseconds = 0};
    pcb_datetime_t reference2 = {.datetime = {.tm_year = -1899, .tm_mday = 1}, .nanoseconds = 0};

    pcb_datetime_t dt1 = PCB_DATETIME_STATIC_INITIALIZER;
    assert_memory_equal(&dt1, &reference1, sizeof(pcb_datetime_t));

    pcb_datetime_t dt2;
    pcb_datetime_initialize(&dt2, PCB_EPOCH_UNIX);
    assert_memory_equal(&dt2, &reference1, sizeof(pcb_datetime_t));

    // Reuse the same struct -- test that values are overwritten
    pcb_datetime_initialize(&dt2, PCB_EPOCH_TR098);
    assert_memory_equal(&dt2, &reference2, sizeof(pcb_datetime_t));
}

/**
 * Test that pcb_datetime_now() works.
 * Because this just calls clock_gettime, we can only really test the return value.
 */
static void test_datetime_now(UNUSED void** state) {
    pcb_datetime_t dt;
    dt.datetime.tm_year = 0;

    assert_true(pcb_datetime_now(&dt));

    assert_int_not_equal(dt.datetime.tm_year, 0);

    // Test invalid input
    assert_false(pcb_datetime_now(NULL));
}

static void test_datetime_delta(UNUSED void** state) {
    pcb_datetime_t dt_start = { .datetime = {.tm_sec = 1}, .nanoseconds = 100000000ULL };
    pcb_datetime_t dt_end = { .datetime = {.tm_sec = 2}, .nanoseconds = 234567890ULL };
    int64_t expected_diff = 1134567890L;

    int64_t diff1 = pcb_datetime_delta(&dt_start, &dt_end);
    int64_t diff2 = pcb_datetime_delta(&dt_end, &dt_start);

    assert_int_equal(diff1, -diff2);        // Test that the delta function is symmetric
    assert_int_equal(diff1, expected_diff); // Test that the delta function produced the correct result

    // Test invalid input
    assert_int_equal(pcb_datetime_delta(&dt_start, NULL), LLONG_MAX);
    assert_int_equal(pcb_datetime_delta(NULL, &dt_end), LLONG_MAX);
}

static const struct CMUnitTest tests[] = {
    cmocka_unit_test(test_datetime_init),
    cmocka_unit_test(test_datetime_now),
    cmocka_unit_test(test_datetime_delta),
};

__attribute__((constructor)) void datetime_timing_register(void) {
    register_test_group("datetime_timing", tests, ARRAY_SIZE(tests), NULL, NULL);
}

