/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#define _XOPEN_SOURCE
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#include <pcb/utils/string.h>
#include <sahtest/sahtest.h>

static string_t string_true;
static string_t string_false;
static string_t string_yes;
static string_t string_int;
static string_t string_neg_int;
static string_t string_double;
static string_t string_date_time;
static string_t string_date_time_milliseconds;
static string_t string_text;

void test_string_to_type_setup() {
    string_initialize(&string_true, 0);
    string_initialize(&string_false, 0);
    string_initialize(&string_yes, 0);
    string_initialize(&string_int, 0);
    string_initialize(&string_neg_int, 0);
    string_initialize(&string_double, 0);
    string_initialize(&string_date_time, 0);
    string_initialize(&string_date_time_milliseconds, 0);
    string_initialize(&string_text, 0);

    string_fromChar(&string_true, "true");
    string_fromChar(&string_false, "false");
    string_fromChar(&string_yes, "yes");
    string_fromChar(&string_int, "127");
    string_fromChar(&string_neg_int, "-127");
    string_fromChar(&string_double, "-2.002500e+02");
    string_fromChar(&string_date_time, "2001-11-12T18:31:01Z");
    string_fromChar(&string_date_time_milliseconds, "2001-11-12T18:31:01.123Z");
    string_fromChar(&string_text, "Hallo world");
}

void test_string_to_type_teardown() {
    string_cleanup(&string_true);
    string_cleanup(&string_false);
    string_cleanup(&string_yes);
    string_cleanup(&string_int);
    string_cleanup(&string_neg_int);
    string_cleanup(&string_double);
    string_cleanup(&string_date_time);
    string_cleanup(&string_date_time_milliseconds);
    string_cleanup(&string_text);
}

int test_string_to_bool() {
    T_VERIFY(string_toBool(&string_true) == true);
    T_VERIFY(string_toBool(&string_false) == false);
    T_VERIFY(string_toBool(&string_yes) == true);
    T_VERIFY(string_toBool(&string_int) == false);
    T_VERIFY(string_toBool(&string_neg_int) == false);
    T_VERIFY(string_toBool(&string_double) == false);
    T_VERIFY(string_toBool(&string_date_time) == false);
    T_VERIFY(string_toBool(&string_date_time_milliseconds) == false);
    T_VERIFY(string_toBool(&string_text) == false);

    return 0;
}

int test_NULL_string_to_bool() {
    T_VERIFY(string_toBool(NULL) == false);

    return 0;
}

int test_string_to_char() {
    char* txt = NULL;

    txt = string_toChar(&string_true);
    T_CMP(txt, "true");
    free(txt);

    txt = string_toChar(&string_false);
    T_CMP(txt, "false");
    free(txt);

    txt = string_toChar(&string_yes);
    T_CMP(txt, "yes");
    free(txt);

    txt = string_toChar(&string_int);
    T_CMP(txt, "127");
    free(txt);

    txt = string_toChar(&string_neg_int);
    T_CMP(txt, "-127");
    free(txt);

    txt = string_toChar(&string_double);
    T_CMP(txt, "-2.002500e+02");
    free(txt);

    txt = string_toChar(&string_date_time);
    T_CMP(txt, "2001-11-12T18:31:01Z");
    free(txt);

    txt = string_toChar(&string_date_time_milliseconds);
    T_CMP(txt, "2001-11-12T18:31:01.123Z");
    free(txt);

    txt = string_toChar(&string_text);
    T_CMP(txt, "Hallo world");
    free(txt);

    return 0;
}

int test_NULL_string_to_char() {
    T_VERIFY(string_toChar(NULL) == NULL);

    return 0;
}

int test_string_to_int8() {
    T_VERIFY(string_toInt8(&string_true) == 0);
    T_VERIFY(string_toInt8(&string_false) == 0);
    T_VERIFY(string_toInt8(&string_yes) == 0);
    T_VERIFY(string_toInt8(&string_int) == 127);
    T_VERIFY(string_toInt8(&string_neg_int) == -127);
    T_VERIFY(string_toInt8(&string_double) == 0);
    T_VERIFY(string_toInt8(&string_date_time) == 0);
    T_VERIFY(string_toInt8(&string_date_time_milliseconds) == 0);
    T_VERIFY(string_toInt8(&string_text) == 0);

    return 0;
}

int test_NULL_string_to_int8() {
    T_VERIFY(string_toInt8(NULL) == 0);

    return 0;
}

int test_string_to_int16() {
    T_VERIFY(string_toInt16(&string_true) == 0);
    T_VERIFY(string_toInt16(&string_false) == 0);
    T_VERIFY(string_toInt16(&string_yes) == 0);
    T_VERIFY(string_toInt16(&string_int) == 127);
    T_VERIFY(string_toInt16(&string_neg_int) == -127);
    T_VERIFY(string_toInt16(&string_double) == 0);
    T_VERIFY(string_toInt16(&string_date_time) == 0);
    T_VERIFY(string_toInt16(&string_date_time_milliseconds) == 0);
    T_VERIFY(string_toInt16(&string_text) == 0);

    return 0;
}

int test_NULL_string_to_int16() {
    T_VERIFY(string_toInt16(NULL) == 0);

    return 0;
}

int test_string_to_int32() {
    T_VERIFY(string_toInt32(&string_true) == 0);
    T_VERIFY(string_toInt32(&string_false) == 0);
    T_VERIFY(string_toInt32(&string_yes) == 0);
    T_VERIFY(string_toInt32(&string_int) == 127);
    T_VERIFY(string_toInt32(&string_neg_int) == -127);
    T_VERIFY(string_toInt32(&string_double) == 0);
    T_VERIFY(string_toInt32(&string_date_time) == 0);
    T_VERIFY(string_toInt32(&string_date_time_milliseconds) == 0);
    T_VERIFY(string_toInt32(&string_text) == 0);

    return 0;
}

int test_NULL_string_to_int32() {
    T_VERIFY(string_toInt32(NULL) == 0);

    return 0;
}

int test_string_to_int64() {
    T_VERIFY(string_toInt64(&string_true) == 0);
    T_VERIFY(string_toInt64(&string_false) == 0);
    T_VERIFY(string_toInt64(&string_yes) == 0);
    T_VERIFY(string_toInt64(&string_int) == 127);
    T_VERIFY(string_toInt64(&string_neg_int) == -127);
    T_VERIFY(string_toInt64(&string_double) == 0);
    T_VERIFY(string_toInt64(&string_date_time) == 0);
    T_VERIFY(string_toInt64(&string_date_time_milliseconds) == 0);
    T_VERIFY(string_toInt64(&string_text) == 0);

    return 0;
}

int test_NULL_string_to_int64() {
    T_VERIFY(string_toInt64(NULL) == 0);

    return 0;
}

int test_string_to_uint8() {
    T_VERIFY(string_toUInt8(&string_true) == 0);
    T_VERIFY(string_toUInt8(&string_false) == 0);
    T_VERIFY(string_toUInt8(&string_yes) == 0);
    T_VERIFY(string_toUInt8(&string_int) == 127);
    T_VERIFY(string_toUInt8(&string_neg_int) == 0);
    T_VERIFY(string_toUInt8(&string_double) == 0);
    T_VERIFY(string_toUInt8(&string_date_time) == 0);
    T_VERIFY(string_toUInt8(&string_date_time_milliseconds) == 0);
    T_VERIFY(string_toUInt8(&string_text) == 0);

    return 0;
}

int test_NULL_string_to_uint8() {
    T_VERIFY(string_toUInt8(NULL) == 0);

    return 0;
}

int test_string_to_uint16() {
    T_VERIFY(string_toUInt16(&string_true) == 0);
    T_VERIFY(string_toUInt16(&string_false) == 0);
    T_VERIFY(string_toUInt16(&string_yes) == 0);
    T_VERIFY(string_toUInt16(&string_int) == 127);
    T_VERIFY(string_toUInt16(&string_neg_int) == 0);
    T_VERIFY(string_toUInt16(&string_double) == 0);
    T_VERIFY(string_toUInt16(&string_date_time) == 0);
    T_VERIFY(string_toUInt16(&string_date_time_milliseconds) == 0);
    T_VERIFY(string_toUInt16(&string_text) == 0);

    return 0;
}

int test_NULL_string_to_uint16() {
    T_VERIFY(string_toUInt16(NULL) == 0);

    return 0;
}

int test_string_to_uint32() {
    T_VERIFY(string_toUInt32(&string_true) == 0);
    T_VERIFY(string_toUInt32(&string_false) == 0);
    T_VERIFY(string_toUInt32(&string_yes) == 0);
    T_VERIFY(string_toUInt32(&string_int) == 127);
    T_VERIFY(string_toUInt32(&string_date_time) == 0);
    T_VERIFY(string_toUInt32(&string_date_time_milliseconds) == 0);
    T_VERIFY(string_toUInt32(&string_text) == 0);

    return 0;
}

int test_NULL_string_to_uint32() {
    T_VERIFY(string_toUInt32(NULL) == 0);

    return 0;
}

int test_string_to_uint64() {
    T_VERIFY(string_toUInt64(&string_true) == 0);
    T_VERIFY(string_toUInt64(&string_false) == 0);
    T_VERIFY(string_toUInt64(&string_yes) == 0);
    T_VERIFY(string_toUInt64(&string_int) == 127);
    T_VERIFY(string_toUInt64(&string_date_time) == 0);
    T_VERIFY(string_toUInt64(&string_date_time_milliseconds) == 0);
    T_VERIFY(string_toUInt64(&string_text) == 0);

    return 0;
}

int test_NULL_string_to_uint64() {
    T_VERIFY(string_toUInt64(NULL) == 0);

    return 0;
}

int test_string_to_time() {
    struct tm* time = string_toTime(&string_date_time, NULL);
    T_VERIFY(time != NULL);
    free(time);

    time = string_toTime(&string_date_time_milliseconds, NULL);
    T_VERIFY(time != NULL);
    free(time);

    return 0;
}

int test_NULL_string_to_time() {
    T_VERIFY(string_toTime(NULL, NULL) == NULL);

    return 0;
}
