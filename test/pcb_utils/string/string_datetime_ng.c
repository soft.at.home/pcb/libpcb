/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#define _GNU_SOURCE
#include "string_tests_cmocka.h"
#include "pcb/utils/string.h"
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <mcheck.h>
#include <stdio.h>

static string_t string1;
static string_t string2;
static string_t string3;
static string_t string4;

static char* to_iso(const pcb_datetime_t* dt) {
    char* s = NULL;
    (void) !!asprintf(&s, "%04d-%02d-%02dT%02d:%02d:%02d.%09uZ",
                      dt->datetime.tm_year + 1900, dt->datetime.tm_mon + 1, dt->datetime.tm_mday, dt->datetime.tm_hour, dt->datetime.tm_min, dt->datetime.tm_sec, dt->nanoseconds);
    return s;
}

static bool compare_structs(const struct tm* tm1, const struct tm* tm2) {
    return (tm1->tm_year == tm2->tm_year &&
            tm1->tm_mon == tm2->tm_mon &&
            tm1->tm_mday == tm2->tm_mday &&
            tm1->tm_hour == tm2->tm_hour &&
            tm1->tm_min == tm2->tm_min &&
            tm1->tm_sec == tm2->tm_sec);
}

static int test_string_datetime_setup(UNUSED void** state) {
    string_fromChar(&string1, "2008-09-01T22:10:59Z");
    string_fromChar(&string2, "2008-09-01T22:10:59.12345Z");
    string_fromChar(&string3, "It was September 01 of the year of our lord 2008, 123450 microseconds past 22:10:59");
    string_fromChar(&string4, "0001-01-01T00:00:00Z");
    return 0;
}

static int test_string_datetime_teardown(UNUSED void** state) {
    string_cleanup(&string1);
    string_cleanup(&string2);
    string_cleanup(&string3);
    string_cleanup(&string4);
    return 0;
}

static void test_string_toTime(UNUSED void** state) {
    struct tm* tm1 = NULL;
    struct tm* tm2 = NULL;
    struct tm* tm3 = NULL;
    struct tm reference =
    {
        .tm_year = 2008 - 1900,
        .tm_mon = 9 - 1,   /* struct tm months count from 0 */
        .tm_mday = 1,
        .tm_hour = 22,
        .tm_min = 10,
        .tm_sec = 59
    };
    struct tm reference2 =
    {
        .tm_year = 1 - 1900,
        .tm_mon = 1 - 1,   /* struct tm months count from 0 */
        .tm_mday = 1,
        .tm_hour = 0,
        .tm_min = 0,
        .tm_sec = 0
    };
    tm1 = string_toTime(&string1, NULL);
    tm2 = string_toTime(&string2, NULL);
    tm3 = string_toTime(&string4, NULL);

    assert_non_null(tm1);
    assert_non_null(tm2);
    assert_non_null(tm3);
    assert_true(compare_structs(tm1, &reference));
    assert_true(compare_structs(tm2, &reference));
    assert_true(compare_structs(tm3, &reference2));

    free(tm1);
    free(tm2);
    free(tm3);
}

static void test_string_toTimeExtended(UNUSED void** state) {
    //2008-09-01T22:10:59.12345Z
    //$ date -d '2008-09-01T22:10:59Z' +%s
    //1220307059

    pcb_datetime_t* ts1 = NULL;
    pcb_datetime_t* ts2 = NULL;
    pcb_datetime_t* ts3 = NULL;
    pcb_datetime_t* ts4 = NULL;
    pcb_datetime_t reference1 =
    {
        .datetime =
        {
            .tm_year = 2008 - 1900,
            .tm_mon = 9 - 1,   /* struct tm months count from 0 */
            .tm_mday = 1,
            .tm_hour = 22,
            .tm_min = 10,
            .tm_sec = 59
        },
        .nanoseconds = 0
    };
    pcb_datetime_t reference2 =
    {
        .datetime =
        {
            .tm_year = 2008 - 1900,
            .tm_mon = 9 - 1,   /* struct tm months count from 0 */
            .tm_mday = 1,
            .tm_hour = 22,
            .tm_min = 10,
            .tm_sec = 59
        },
        .nanoseconds = 123450000UL
    };
    pcb_datetime_t reference3 =
    {
        .datetime =
        {
            .tm_year = 1 - 1900,
            .tm_mon = 1 - 1,   /* struct tm months count from 0 */
            .tm_mday = 1,
            .tm_hour = 0,
            .tm_min = 0,
            .tm_sec = 0
        },
        .nanoseconds = 0UL
    };
    char* refs1 = to_iso(&reference1);
    char* refs2 = to_iso(&reference2);
    char* refs3 = to_iso(&reference3);
    ts1 = string_toTimeExtended(&string1, NULL);
    ts2 = string_toTimeExtended(&string2, NULL);
    ts3 = string_toTimeExtended(&string3, "It was %B %d of the year of our lord %Y, %f microseconds past %H:%M:%S");
    ts4 = string_toTimeExtended(&string4, "%Y-%m-%d");
    assert_non_null(ts1);
    assert_non_null(ts2);
    assert_non_null(ts3);
    assert_non_null(ts4);
    char* s1 = to_iso(ts1);
    char* s2 = to_iso(ts2);
    char* s3 = to_iso(ts3);
    char* s4 = to_iso(ts4);
    assert_string_equal(s1, refs1);
    assert_string_equal(s2, refs2);
    assert_string_equal(s3, refs2);
    assert_string_equal(s4, refs3);
    free(ts1);
    free(ts2);
    free(ts3);
    free(ts4);
    free(refs1);
    free(refs2);
    free(refs3);
    free(s1);
    free(s2);
    free(s3);
    free(s4);
}

static void test_string_fromTime(UNUSED void** state) {

    string_t output;
    size_t outsize;

    const struct tm input1 =
    {
        .tm_year = 2008 - 1900, /* struct tm counts years since 1900 */
        .tm_mon = 9 - 1,        /* struct tm counts months from 0 */
        .tm_mday = 1,
        .tm_hour = 22,
        .tm_min = 10,
        .tm_sec = 59
    };
    const pcb_datetime_t input2 =
    {
        .datetime =
        {
            .tm_year = 2008 - 1900,
            .tm_mon = 9 - 1,   /* struct tm months count from 0 */
            .tm_mday = 1,
            .tm_hour = 22,
            .tm_min = 10,
            .tm_sec = 59
        },
        .nanoseconds = 123450001UL
    };
    const pcb_datetime_t input3 =
    {
        .datetime =
        {
            .tm_year = 1 - 1900,
            .tm_mon = 1 - 1,   /* struct tm months count from 0 */
            .tm_mday = 1,
            .tm_hour = 0,
            .tm_min = 0,
            .tm_sec = 0
        },
        .nanoseconds = 0UL
    };
    const char* reference1 = "2008-09-01T22:10:59Z";
    const char* reference2 = "2008-09-01T22:10:59.123450Z";
    const char* reference3 = "It was September 01 of the year of our lord 2008, 123450001 nanoseconds past 22:10:59";
    const char* reference4 = "0001-01-01T00:00:00Z";
    string_initialize(&output, 0);
    // Test fromTime with first input, should result in reference1
    outsize = string_fromTime(&output, &input1, NULL);
    assert_int_equal(outsize, strlen(reference1));   // Function should return output string size
    assert_string_equal(string_buffer(&output), reference1);

    outsize = string_fromTimeExtended(&output, &input2, NULL);
    assert_int_equal(outsize, strlen(reference2));   // Function should return output string size
    assert_string_equal(string_buffer(&output), reference2);

    outsize = string_fromTimeExtended(&output, &input2, "It was %B %d of the year of our lord %Y, %N nanoseconds past %H:%M:%S");
    assert_int_equal(outsize, string_length(&output));   // Function should return output string size
    assert_string_equal(string_buffer(&output), reference3);

    outsize = string_fromTimeExtended(&output, &input3, NULL);
    assert_int_equal(outsize, strlen(reference4));
    assert_string_equal(string_buffer(&output), reference4);

    outsize = string_fromTime(&output, &input3.datetime, NULL);
    assert_int_equal(outsize, strlen(reference4));
    assert_string_equal(string_buffer(&output), reference4);
    string_cleanup(&output);
}

static const struct CMUnitTest tests[] =
{
    cmocka_unit_test(test_string_toTime),
    cmocka_unit_test(test_string_toTimeExtended),
    cmocka_unit_test(test_string_fromTime),
};

__attribute__((constructor)) void string_datetime_register(void) {
    register_test_group("string_datetime", tests, ARRAY_SIZE(tests), test_string_datetime_setup, test_string_datetime_teardown);
}

