/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
// Copyright (C) 2018 SoftAtHome. More info at end of file

#include "string_tests_cmocka.h"
#include <stdlib.h>
#include <setjmp.h>
#include <stdarg.h>

#include "pcb/utils/string.h"
#include <string.h>
#include <assert.h>
#include <mcheck.h>
#include <stdio.h>


static const char* mcheck_status_name(enum mcheck_status s) {
    assert(MCHECK_DISABLED == -1);
    assert(MCHECK_OK == 0);
    assert(MCHECK_FREE == 1);
    assert(MCHECK_HEAD == 2);
    assert(MCHECK_TAIL == 3);
    assert(s >= MCHECK_DISABLED);
    assert(s <= MCHECK_TAIL);
    const char* status_names[] = {
        "MCHECK_DISABLED = -1",         /* Consistency checking is not turned on.  */
        "MCHECK_OK",                    /* Block is fine.  */
        "MCHECK_FREE",                  /* Block freed twice.  */
        "MCHECK_HEAD",                  /* Memory before the block was clobbered.  */
        "MCHECK_TAIL"
    };
    return status_names[s + 1];
}


static void string_attachBuffer_will_accept_zero_terminated_string(void** state) {
    string_t s;
    string_initialize(&s, 100);

    char* buff = calloc(5, sizeof(char));
    memcpy(buff, "abcd", 5);
    string_attachBuffer(&s, buff, 5);

    assert_ptr_equal(buff, string_buffer(&s));
    assert_string_equal("abcd", string_buffer(&s));
    string_cleanup(&s);
}

static void string_attachBuffer_will_take_ownership(void** state) {
    string_t s;
    assert(string_initialize(&s, 100));

    char* buff = calloc(5, sizeof(char));
    assert(string_attachBuffer(&s, buff, 5));

    string_cleanup(&s);

    // Here, buff should be freed by `string_cleanup`.
    // This test should be run by valgrind to check it.
}

static void string_attachBuffer_will_free_previous_buffer(void** state) {
    (void) state;

    string_t s;
    assert(string_initialize(&s, 100));

    char* previous_buffer = calloc(5, sizeof(char));
    assert(string_attachBuffer(&s, previous_buffer, 5));

    char* buff = calloc(5, sizeof(char));
    assert(string_attachBuffer(&s, buff, 5));
    // Here, previous_buffer should be freed by `string_attachBuffer`.
    // This test should be run by valgrind to check it.

    string_cleanup(&s);
    // Here,  buff should be freed by `string_cleanup`.
    // This test should be run by valgrind to check it.
}


static void string_attachBuffer_will_accept_null_if_size_is_zero(void** state) {
    (void) state;

    string_t s;
    assert(string_initialize(&s, 0));

    assert_true(string_attachBuffer(&s, NULL, 0));

    assert_false(string_attachBuffer(&s, NULL, 10));
    char* buffer = (char*) 100;
    assert_false(string_attachBuffer(&s, buffer, 0));

    string_cleanup(&s);
}

static const struct CMUnitTest tests[] = {
    cmocka_unit_test(string_attachBuffer_will_accept_zero_terminated_string),
    cmocka_unit_test(string_attachBuffer_will_take_ownership),
    cmocka_unit_test(string_attachBuffer_will_free_previous_buffer),
    cmocka_unit_test(string_attachBuffer_will_accept_null_if_size_is_zero),
};

__attribute__((constructor)) void string_attachBuffer_register(void) {
    register_test_group("string_attachBuffer", tests, ARRAY_SIZE(tests), NULL, NULL);
}

/****************************************************************************
**
** Copyright (C) 2018 SoftAtHome. All rights reserved.
**

** SoftAtHome reserves all rights not expressly granted herein.
**
** - DISCLAIMER OF WARRANTY -
**
** THIS FILE IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER
** EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR
** PURPOSE.
**
** THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE SOURCE
** CODE IS WITH YOU. SHOULD THE SOURCE CODE PROVE DEFECTIVE, YOU
** ASSUME THE COST OF ALL NECESSARY SERVICING, REPAIR OR CORRECTION.
**
** - LIMITATION OF LIABILITY -
**
** IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN
** WRITING WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MODIFIES
** AND/OR DISTRIBUTES THE SOURCE CODE, BE LIABLE TO YOU FOR DAMAGES,
** INCLUDING ANY GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES
** ARISING OUT OF THE USE OR INABILITY TO USE THE SOURCE CODE
** (INCLUDING BUT NOT LIMITED TO LOSS OF DATA OR DATA BEING RENDERED
** INACCURATE OR LOSSES SUSTAINED BY YOU OR THIRD PARTIES OR A FAILURE
** OF THE SOURCE CODE TO OPERATE WITH ANY OTHER PROGRAM), EVEN IF SUCH
** HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH
** DAMAGES.
**
****************************************************************************/