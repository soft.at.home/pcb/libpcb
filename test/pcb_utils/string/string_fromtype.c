/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#define _XOPEN_SOURCE
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#include <pcb/utils/string.h>
#include <sahtest/sahtest.h>

static string_t string1;
static string_t string2;

void test_string_from_type_setup() {
    string_initialize(&string1, 0);
    string_initialize(&string2, 0);
}

void test_string_from_type_teardown() {
    string_cleanup(&string1);
    string_cleanup(&string2);
}

int test_string_from_bool() {
    T_VERIFY(string_fromBool(&string1, true) == 1);
    T_VERIFY(strcmp(string1.buffer, "1") == 0);

    T_VERIFY(string_fromBool(&string1, false) == 1);
    T_VERIFY(strcmp(string1.buffer, "0") == 0);

    return 0;
}

int test_NULL_string_from_bool() {
    T_VERIFY(string_fromBool(NULL, true) == 0);

    return 0;
}

int test_string_from_char() {
    T_VERIFY(string_fromChar(&string1, "Hallo") == 5);
    T_VERIFY(strcmp(string1.buffer, "Hallo") == 0);
    T_VERIFY(string1.bufferSize == 6);

    T_VERIFY(string_fromChar(&string1, "Hallo world") == 11);
    T_VERIFY(strcmp(string1.buffer, "Hallo world") == 0);
    T_VERIFY(string1.bufferSize == 12);

    return 0;
}

int test_NULL_string_from_char() {
    T_VERIFY(string_fromChar(NULL, "Hallo") == 0);

    return 0;
}

int test_string_from_NULL_char() {
    T_VERIFY(string_fromChar(&string1, NULL) == 0);
    T_VERIFY(string1.buffer == NULL);

    T_VERIFY(string_fromChar(&string1, "Hallo") == 5);
    T_VERIFY(string_fromChar(&string1, NULL) == 0);
    T_VERIFY(strcmp(string1.buffer, "") == 0);

    return 0;
}

int test_string_from_int8() {
    T_VERIFY(string_fromInt8(&string1, 127) == 3);
    T_VERIFY(strcmp(string1.buffer, "127") == 0);

    T_VERIFY(string_fromInt8(&string1, -1) == 2);
    T_VERIFY(strcmp(string1.buffer, "-1") == 0);

    T_VERIFY(string_fromInt8(&string1, -127) == 4);
    T_VERIFY(strcmp(string1.buffer, "-127") == 0);

    return 0;
}

int test_NULL_string_from_int8() {
    T_VERIFY(string_fromInt8(NULL, 100) == 0);

    return 0;
}

int test_string_from_int16() {
    T_VERIFY(string_fromInt16(&string1, 32767) == 5);
    T_VERIFY(strcmp(string1.buffer, "32767") == 0);

    T_VERIFY(string_fromInt16(&string1, -1) == 2);
    T_VERIFY(strcmp(string1.buffer, "-1") == 0);

    T_VERIFY(string_fromInt16(&string1, -32767) == 6);
    T_VERIFY(strcmp(string1.buffer, "-32767") == 0);

    return 0;
}

int test_NULL_string_from_int16() {
    T_VERIFY(string_fromInt16(NULL, 100) == 0);

    return 0;
}

int test_string_from_int32() {
    T_VERIFY(string_fromInt32(&string1, 2147483647) == 10);
    T_VERIFY(strcmp(string1.buffer, "2147483647") == 0);

    T_VERIFY(string_fromInt32(&string1, -1) == 2);
    T_VERIFY(strcmp(string1.buffer, "-1") == 0);

    T_VERIFY(string_fromInt32(&string1, -2147483647) == 11);
    T_VERIFY(strcmp(string1.buffer, "-2147483647") == 0);

    return 0;
}

int test_NULL_string_from_int32() {
    T_VERIFY(string_fromInt32(NULL, 100) == 0);

    return 0;
}

int test_string_from_int64() {
    T_VERIFY(string_fromInt64(&string1, 214748364700) == 12);
    T_VERIFY(strcmp(string1.buffer, "214748364700") == 0);

    T_VERIFY(string_fromInt64(&string1, -1) == 2);
    T_VERIFY(strcmp(string1.buffer, "-1") == 0);

    T_VERIFY(string_fromInt64(&string1, -214748364700) == 13);
    T_VERIFY(strcmp(string1.buffer, "-214748364700") == 0);

    return 0;
}

int test_NULL_string_from_int64() {
    T_VERIFY(string_fromInt64(NULL, 100) == 0);

    return 0;
}

int test_string_from_uint8() {
    T_VERIFY(string_fromUInt8(&string1, 25) == 2);
    T_VERIFY(strcmp(string1.buffer, "25") == 0);

    T_VERIFY(string_fromUInt8(&string1, 255) == 3);
    T_VERIFY(strcmp(string1.buffer, "255") == 0);

    return 0;
}

int test_NULL_string_from_uint8() {
    T_VERIFY(string_fromUInt8(NULL, 100) == 0);

    return 0;
}

int test_string_from_uint16() {
    T_VERIFY(string_fromUInt16(&string1, 3276) == 4);
    T_VERIFY(strcmp(string1.buffer, "3276") == 0);

    T_VERIFY(string_fromUInt16(&string1, 65534) == 5);
    T_VERIFY(strcmp(string1.buffer, "65534") == 0);

    return 0;
}

int test_NULL_string_from_uint16() {
    T_VERIFY(string_fromUInt16(NULL, 100) == 0);

    return 0;
}

int test_string_from_uint32() {
    T_VERIFY(string_fromUInt32(&string1, 14748364) == 8);
    T_VERIFY(strcmp(string1.buffer, "14748364") == 0);

    T_VERIFY(string_fromUInt32(&string1, 429496729) == 9);
    T_VERIFY(strcmp(string1.buffer, "429496729") == 0);

    return 0;
}

int test_NULL_string_from_uint32() {
    T_VERIFY(string_fromUInt32(NULL, 100) == 0);

    return 0;
}

int test_string_from_uint64() {
    T_VERIFY(string_fromUInt64(&string1, 21474836470) == 11);
    T_VERIFY(strcmp(string1.buffer, "21474836470") == 0);

    T_VERIFY(string_fromUInt64(&string1, 214748364700) == 12);
    T_VERIFY(strcmp(string1.buffer, "214748364700") == 0);

    return 0;
}

int test_NULL_string_from_uint64() {
    T_VERIFY(string_fromUInt64(NULL, 100) == 0);

    return 0;
}

int test_string_from_double() {
    T_VERIFY(string_fromDouble(&string1, 100.5) == 12);
    T_VERIFY(strcmp(string1.buffer, "1.005000e+02") == 0);

    T_VERIFY(string_fromDouble(&string1, -200.25) == 13);
    T_VERIFY(strcmp(string1.buffer, "-2.002500e+02") == 0);

    return 0;
}

int test_NULL_string_from_double() {
    T_VERIFY(string_fromDouble(NULL, 100.100) == 0);

    return 0;
}


int test_string_from_time() {
    struct tm datetime; // = (struct tm *)malloc(sizeof(struct tm));

    strptime("2001-11-12 18:31:01Z", "%Y-%m-%d %H:%M:%SZ", &datetime);

    T_VERIFY(string_fromTime(&string1, &datetime, "%Y-%m-%d") == 10);
    T_VERIFY(strcmp(string1.buffer, "2001-11-12") == 0);

    T_VERIFY(string_fromTime(&string1, &datetime, NULL) == 20);
    T_VERIFY(strcmp(string1.buffer, "2001-11-12T18:31:01Z") == 0);

    return 0;
}

/*
   int test_NULL_string_from_time() {
    T_VERIFY(string_fromTime(NULL,time,NULL) == 0);

    return 0;
   }
 */
