/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef TEST_STRING_H
#define TEST_STRING_H

#include "string/string_init.h"
#include "string/string_clear.h"
#include "string/string_fromtype.h"
#include "string/string_totype.h"
#include "string/string_copy.h"
#include "string/string_append.h"
#include "string/string_prepend.h"
#include "string/string_toupper.h"
#include "string/string_tolower.h"
#include "string/string_mid.h"
#include "string/string_left.h"
#include "string/string_right.h"
#include "string/string_find.h"
#include "string/string_contains.h"
#include "string/string_replace.h"
#include "string/string_trim.h"
#include "string/string_resolve_env.h"
#include "string/string_compare.h"
#include "string/string_numeric.h"

sahtest_group_begin(string, NULL, NULL)
sahtest_add_group(init_cleanup, "Test string appending and prepending")
sahtest_add_group(clear_func, "Test clearing a string")
sahtest_add_group(from_type, "Test string creation from different types")
sahtest_add_group(copy_func, "Test copying a string")
sahtest_add_group(to_type, "Test string coversion to different types")
sahtest_add_group(to_upper, "Test string coversion to all upper case")
sahtest_add_group(to_lower, "Test string coversion to all lowers case")
sahtest_add_group(str_append, "Test string appending")
sahtest_add_group(str_prepend, "Test string prepending")
sahtest_add_group(mid, "Test string taking substrings")
sahtest_add_group(left, "Test string taking left string")
sahtest_add_group(right, "Test string taking right string")
sahtest_add_group(find, "Test string searching")
sahtest_add_group(contains, "Test string checking for substring")
sahtest_add_group(replace, "Test replacing a substring in a string")
sahtest_add_group(trim, "Test string trim functions")
sahtest_add_group(resolve_env, "Test resolving of env variables")
sahtest_add_group(compare, "Test comparing strings")
sahtest_add_group(numeric, "Test is numeric string")
sahtest_group_end()

#endif // TEST_STRING_H
