/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#define _XOPEN_SOURCE
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#include <pcb/utils/string.h>
#include <sahtest/sahtest.h>

static string_t string_dest;
static string_t string_source;

void test_string_mid_setup() {
    string_initialize(&string_dest, 10);
    //                              1234567890123456789012345678901234567
    string_fromChar(&string_source, "A wooden woodpecker pecks in the wood");
}

void test_string_mid_teardown() {
    string_cleanup(&string_dest);
    string_cleanup(&string_source);
}

int test_mid_from_0_with_length_maxint() {
    T_VERIFY(string_mid(&string_dest, &string_source, 0, -1) == 37);
    T_VERIFY(strcmp(string_buffer(&string_dest), "A wooden woodpecker pecks in the wood") == 0);

    return 0;
}

int test_mid_from_index_20_length_5() {
    T_VERIFY(string_mid(&string_dest, &string_source, 20, 5) == 5);
    T_VERIFY(strcmp(string_buffer(&string_dest), "pecks") == 0);

    return 0;
}

int test_mid_start_out_of_boundery() {
    T_VERIFY(string_mid(&string_dest, &string_source, 40, 5) == 0);
    T_VERIFY(string_buffer(&string_dest) == NULL);

    return 0;
}

int test_mid_length_out_of_boundery() {
    T_VERIFY(string_mid(&string_dest, &string_source, 20, 30) == 17);
    T_VERIFY(strcmp(string_buffer(&string_dest), "pecks in the wood") == 0);

    return 0;
}

int test_mid_source_null_string() {
    T_VERIFY(string_mid(&string_dest, NULL, 20, 30) == 0);
    T_VERIFY(string_buffer(&string_dest) == NULL);

    return 0;
}

int test_mid_dest_null_string() {
    T_VERIFY(string_mid(NULL, &string_source, 10, 10) == 0);

    return 0;
}
