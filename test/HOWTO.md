# Running the unit tests on your machine

## Prerequisites

Obviously, you clone this repository:

```bash
git clone git@gitlab.com:soft.at.home/pcb/libpcb.git
```

And you'll need sahnte (https://gitlab.com/soft.at.home/sofa/dockers/sah_nte).


## Prepare the environment and build lib_pcb

Let's first make our lifes easier:

```bash
you@your_host$ ln -s Component.mk Makefile
```

Now create a container to build and run our tests in

```bash
you@your_host$ sahnte pull
you@your_host$ sahnte create lib_pcb_tests $PWD
you@your_host$ sahnte shell lib_pcb_tests

you@4003cb6a29f4:/$ cd /where/you/cloned/this/repo
you@4003cb6a29f4:/where/you/cloned/this/repo$
```

Install the `components.config` from gitlab

```bash
you@4003cb6a29f4:~/sah/network/lib_pcb$ STAGINGDIR=~/output/staging/
you@4003cb6a29f4:~/sah/network/lib_pcb$ git clone https://gitlab.com/soft.at.home/sofa/gitlab-ci/config.git $STAGINGDIR
Cloning into '/home/you/output/staging'...
Username for 'https://gitlabinternal.softathome.com': you
Password for 'https://sahnalys11@gitlabinternal.softathome.com': 
remote: Enumerating objects: 2920, done.
remote: Counting objects: 100% (2920/2920), done.
remote: Compressing objects: 100% (898/898), done.
remote: Total 2920 (delta 1741), reused 2904 (delta 1725)
Receiving objects: 100% (2920/2920), 1.48 MiB | 0 bytes/s, done.
Resolving deltas: 100% (1741/1741), done.
```

Now all is set up to install any missing dependencies:

```bash
you@4003cb6a29f4:/where/you/cloned/this/repo$ sudo -E factory.py install_deps . master
Warning: Permanently added 'packages.be.softathome.com,192.168.16.59' (RSA) to the list of known hosts.
Dep = lib_sahtrace
Need to install dependencies? True
Searching for lib_sahtrace-x86_64_linux_gnu-REL_2018_02_14_V1.3.17 in JFrog-dev
Trying version references in this order master,REL/2018-02-14_V1.3.17
Searching for lib_sahtrace-x86_64_linux_gnu-REL_2018_02_14_V1.3.17 in JFrog-rel
Trying version references in this order master,REL/2018-02-14_V1.3.17
Select package lib_sahtrace-x86_64_linux_gnu-master
[Info] Searching items to download...
[Info] [Thread 2] Downloading sah-component-rel/com.softathome.network/lib_sahtrace/lib_sahtrace-x86_64_linux_gnu-master.tar.gz
Dep = lib_usermngt
Need to install dependencies? True
Searching for lib_usermngt-x86_64_linux_gnu-REL_2018_02_14_V1.5.2 in JFrog-dev
Trying version references in this order master,REL/2018-02-14_V1.5.2
Searching for lib_usermngt-x86_64_linux_gnu-REL_2018_02_14_V1.5.2 in JFrog-rel
Trying version references in this order master,REL/2018-02-14_V1.5.2
Select package lib_usermngt-x86_64_linux_gnu-master
[Info] Searching items to download...
[Info] [Thread 2] Downloading sah-component-rel/com.softathome.network/lib_usermngt/lib_usermngt-x86_64_linux_gnu-master.tar.gz
Need to install dependencies? True
Searching for lib_openssl-x86_64_linux_gnu-gen_1.0.2k in JFrog-dev
Trying version references in this order master,gen_1.0.2k,gen
Searching for lib_openssl-x86_64_linux_gnu-gen_1.0.2k in JFrog-rel
Trying version references in this order master,gen_1.0.2k,gen
Select package lib_openssl-x86_64_linux_gnu-gen_1.0.2k
[Info] Searching items to download...
[Info] [Thread 2] Downloading sah-component-rel/com.softathome.network/lib_openssl/lib_openssl-x86_64_linux_gnu-gen_1.0.2k.tar.gz
Dep = lib_openssl
```


## Build and run the tests

And now, you're ready to start the tests:

```bash
you@4003cb6a29f4:/where/you/cloned/this/repo$ sudo -E make runtest
```

## Cleaning up

Exit the shell, and 

```bash
you@your_host$ sahnte stop lib_pcb_tests
lib_pcb_tests
you@your_host$ sahnte rm lib_pcb_tests
lib_pcb_tests
```
