# `lib_pcb` unit tests

This folder contains the unit tests for lib_pcb.  The tests
are intended to be run both by Continuous Integration,
checking quality continuously, and by developers, shortening
the 'developer inner loop'.

## Running the tests

Docker is the preferred way to run the tests, as described in
the [HOWTO.md](HOWTO.md).
