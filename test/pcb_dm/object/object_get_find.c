/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <assert.h>
#include <setjmp.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <cmocka.h>
#include <pcb/core/datamodel.h>
#include <pcb/core/object.h>
#include <pcb/utils.h>
#include <debug/sahtrace.h>
#include <datamodel_priv.h>
#include <object_priv.h>

static datamodel_t dm;
static object_t* root = NULL;
static object_t* testroot[5];
static object_t* child = NULL;
static object_t* instances_child[6];

static int setup_suite(void** state) {
    (void) state;

    /**
     * The data model we build.
     * 1. (root) has no name actually, hence the '()'
     * 2. [T] denotes a template object
     * 3. [i] denotes an instance object, with 'i' being the index number
     *
     * (root).[T]TestRoot1.Child
     *                     [1]Key1.Child
     *                     [2]Key2.Child
     *                     [3]Key3.Child
     *        TestRoot2.Child
     *        TestRoot3
     *        [T]TestRoot4.[1]Key1
     *                     [2]Key2
     *                     [3]Key3
     *        [T]TestRoot5
     */
    datamodel_initialize(&dm);
    root = datamodel_root(&dm);
    testroot[0] = object_create(root, "TestRoot1", object_attr_template);
    child = object_create(testroot[0], "Child", 0);
    object_t* obj = object_createInstance(testroot[0], 0, "Key1");
    instances_child[0] = object_firstChild(obj);
    obj = object_createInstance(testroot[0], 0, "Key2");
    instances_child[1] = object_firstChild(obj);
    obj = object_createInstance(testroot[0], 0, "Key3");
    instances_child[2] = object_firstChild(obj);

    testroot[1] = object_create(root, "TestRoot2", 0);
    object_create(testroot[1], "Child", 0);

    testroot[2] = object_create(root, "TestRoot3", 0);

    testroot[3] = object_create(root, "TestRoot4", object_attr_template);
    obj = object_createInstance(testroot[3], 0, "Key1");
    instances_child[3] = obj;
    obj = object_createInstance(testroot[3], 0, "Key2");
    instances_child[4] = obj;
    obj = object_createInstance(testroot[3], 0, "Key3");
    instances_child[5] = obj;

    testroot[4] = object_create(root, "TestRoot5", object_attr_template);

    object_commit(root);

    return 0;
}

static int teardown_suite(void** state) {
    (void) state;

    datamodel_cleanup(&dm);

    return 0;
}

static void test_obj_get_index_path() {
    object_t* obj = object_getObject(root, "TestRoot1.1.Child", path_attr_default, NULL);
    assert_non_null(obj);
    assert_true(obj == instances_child[0]);
    obj = object_getObject(testroot[0], "3.Child", path_attr_default, NULL);
    assert_non_null(obj);
    assert_true(obj == instances_child[2]);
}

static void test_obj_get_key_path() {
    object_t* obj = object_getObject(root, "TestRoot1.Key1.Child", path_attr_key_notation, NULL);
    assert_non_null(obj);
    assert_true(obj == instances_child[0]);
    obj = object_getObject(testroot[0], "Key3.Child", path_attr_key_notation, NULL);
    assert_non_null(obj);
    assert_true(obj == instances_child[2]);
}

static void test_obj_get_index_slash_path() {
    object_t* obj = object_getObject(root, "TestRoot1/1/Child", path_attr_slash_seperator, NULL);
    assert_non_null(obj);
    assert_true(obj == instances_child[0]);
    obj = object_getObject(testroot[0], "3/Child", path_attr_slash_seperator, NULL);
    assert_non_null(obj);
    assert_true(obj == instances_child[2]);
}

static void test_obj_get_key_slash_path() {
    object_t* obj = object_getObject(root, "TestRoot1/Key1/Child", path_attr_key_notation | path_attr_slash_seperator, NULL);
    assert_non_null(obj);
    assert_true(obj == instances_child[0]);
    obj = object_getObject(testroot[0], "Key3/Child", path_attr_key_notation | path_attr_slash_seperator, NULL);
    assert_non_null(obj);
    assert_true(obj == instances_child[2]);
}

static void test_obj_get_partial_match() {
    bool exactMatch = false;
    object_t* obj = object_getObject(root, "TestRoot1.1.Child.SubChild.Extra", path_attr_partial_match, &exactMatch);
    assert_non_null(obj);
    assert_true(exactMatch == false);
    assert_true(obj == instances_child[0]);
    obj = object_getObject(testroot[0], "3.Child", path_attr_partial_match, &exactMatch);
    assert_non_null(obj);
    assert_true(exactMatch == true);
    assert_true(obj == instances_child[2]);
    obj = object_getObject(testroot[0], "2", path_attr_partial_match, &exactMatch);
    assert_non_null(obj);
    assert_true(exactMatch == true);
    assert_true(obj == object_parent(instances_child[1]));
    obj = object_getObject(testroot[0], "2.Test", path_attr_partial_match, &exactMatch);
    assert_non_null(obj);
    assert_true(exactMatch == true);
    assert_true(obj == object_parent(instances_child[1]));
}

static void test_obj_get_empty_path() {
    bool exactMatch = false;
    object_t* obj = object_getObject(root, "", path_attr_default, NULL);
    assert_non_null(obj);
    assert_true(obj == root);
    obj = object_getObject(testroot[0], "", path_attr_default, NULL);
    assert_non_null(obj);
    assert_true(obj == testroot[0]);
    obj = object_getObject(testroot[1], "", path_attr_partial_match, &exactMatch);
    assert_non_null(obj);
    assert_true(obj == testroot[1]);
    assert_true(exactMatch == true);
}

static void test_obj_get_null_path() {
    object_t* obj = object_getObject(root, NULL, path_attr_default, NULL);
    assert_non_null(obj);
    assert_true(obj == root);
    obj = object_getObject(testroot[0], NULL, path_attr_default, NULL);
    assert_non_null(obj);
    assert_true(obj == testroot[0]);
}

static void test_obj_get_null_object() {
    object_t* obj = object_getObject(NULL, "TestRoot1.1.Child", path_attr_default, NULL);
    assert_null(obj);
}

static void test_obj_get_invalid_path() {
    object_t* obj = object_getObject(root, "TestRoot1.1.Child.SubChild.Extra", path_attr_default, NULL);
    assert_null(obj);
}

static void test_obj_get_use_path_type_modifiers() {
    object_t* obj = object_getObject(root, "TestRoot1.$Key1.Child", path_attr_default, NULL);
    assert_non_null(obj);
    assert_true(obj == instances_child[0]);
    obj = object_getObject(testroot[0], "$Key3.Child", path_attr_default, NULL);
    assert_non_null(obj);
    assert_true(obj == instances_child[2]);

    obj = object_getObject(root, "TestRoot1.%1.Child", path_attr_key_notation, NULL);
    assert_non_null(obj);
    assert_true(obj == instances_child[0]);
    obj = object_getObject(testroot[0], "%3.Child", path_attr_key_notation, NULL);
    assert_non_null(obj);
    assert_true(obj == instances_child[2]);
}

static void test_obj_get_by_key() {
    object_t* obj = object_getObjectByKey(root, "TestRoot1.%s.Child", "Key1");
    assert_non_null(obj);
    assert_true(obj == instances_child[0]);

    obj = object_getObjectByKey(root, "TestRoot1.%%%d.%s", 2, "Child");
    assert_non_null(obj);
    assert_true(obj == instances_child[1]);
}

static void test_obj_find_index_path() {
    object_list_t* objs = object_findObjects(root, "TestRoot?.1.*", -1, search_attr_default);
    assert_non_null(objs);
    assert_true(llist_size(objs) == 1);
    llist_iterator_t* it = llist_first(objs);
    object_t* obj = object_item(it);
    assert_true(obj == instances_child[0]);
    llist_cleanup(objs);
    free(objs);

    objs = object_findObjects(root, "TestRoot?.[13]", -1, search_attr_default);
    assert_non_null(objs);
    assert_true(llist_size(objs) == 4);
    it = llist_first(objs);
    obj = object_item(it);
    assert_true(obj == object_parent(instances_child[0]));
    it = llist_iterator_next(it);
    obj = object_item(it);
    assert_true(obj == object_parent(instances_child[2]));
    it = llist_iterator_next(it);
    obj = object_item(it);
    assert_true(obj == instances_child[3]);
    it = llist_iterator_next(it);
    obj = object_item(it);
    assert_true(obj == instances_child[5]);
    llist_cleanup(objs);
    free(objs);

    objs = object_findObjects(testroot[0], "TestRoot?.[13]", -1, search_attr_default);
    assert_non_null(objs);
    assert_true(llist_size(objs) == 2);
    llist_cleanup(objs);
    free(objs);
}

static void test_obj_find_key_path() {
    object_list_t* objs = object_findObjects(root, "TestRoot?.Key1.*", -1, search_attr_key_notation);
    assert_non_null(objs);
    assert_true(llist_size(objs) == 1);
    llist_iterator_t* it = llist_first(objs);
    object_t* obj = object_item(it);
    assert_true(obj == instances_child[0]);
    llist_cleanup(objs);
    free(objs);

    objs = object_findObjects(root, "TestRoot?.Key[13]", -1, search_attr_key_notation);
    assert_non_null(objs);
    assert_true(llist_size(objs) == 4);
    it = llist_first(objs);
    obj = object_item(it);
    assert_true(obj == object_parent(instances_child[0]));
    it = llist_iterator_next(it);
    obj = object_item(it);
    assert_true(obj == object_parent(instances_child[2]));
    it = llist_iterator_next(it);
    obj = object_item(it);
    assert_true(obj == instances_child[3]);
    it = llist_iterator_next(it);
    obj = object_item(it);
    assert_true(obj == instances_child[5]);
    llist_cleanup(objs);
    free(objs);

    objs = object_findObjects(testroot[0], "TestRoot?.Key[13]", -1, search_attr_key_notation);
    assert_non_null(objs);
    assert_true(llist_size(objs) == 2);
    llist_cleanup(objs);
    free(objs);

    objs = object_findObjects(testroot[0], "*", -1, search_attr_key_notation);
    assert_non_null(objs);
    assert_true(llist_size(objs) == 3);
    llist_cleanup(objs);
    free(objs);

    objs = object_findObjects(testroot[0], "*.*", -1, search_attr_key_notation);
    assert_non_null(objs);
    assert_true(llist_size(objs) == 3);
    llist_cleanup(objs);
    free(objs);

    objs = object_findObjects(root, "*", -1, search_attr_key_notation);
    assert_non_null(objs);
    assert_true(llist_size(objs) == 5);
    llist_cleanup(objs);
    free(objs);

    objs = object_findObjects(root, "*.*", -1, search_attr_key_notation);
    assert_non_null(objs);
    assert_true(llist_size(objs) == 7);
    llist_cleanup(objs);
    free(objs);

    objs = object_findObjects(root, "TestRoot1.*", -1, search_attr_key_notation);
    assert_non_null(objs);
    assert_true(llist_size(objs) == 3);
    it = llist_first(objs);
    obj = object_item(it);
    assert_true(obj == object_parent(instances_child[0]));
    it = llist_iterator_next(it);
    obj = object_item(it);
    assert_true(obj == object_parent(instances_child[1]));
    it = llist_iterator_next(it);
    obj = object_item(it);
    assert_true(obj == object_parent(instances_child[2]));
    llist_cleanup(objs);
    free(objs);

    objs = object_findObjects(root, "*.*.*", -1, search_attr_key_notation);
    assert_non_null(objs);
    assert_true(llist_size(objs) == 3);
    it = llist_first(objs);
    obj = object_item(it);
    assert_true(obj == instances_child[0]);
    it = llist_iterator_next(it);
    obj = object_item(it);
    assert_true(obj == instances_child[1]);
    it = llist_iterator_next(it);
    obj = object_item(it);
    assert_true(obj == instances_child[2]);
    llist_cleanup(objs);
    free(objs);
}

static void test_obj_find_slash_path() {
    object_list_t* objs = object_findObjects(root, "TestRoot?/1/*", -1, search_attr_slash_seperator);
    assert_non_null(objs);
    assert_true(llist_size(objs) == 1);
    llist_iterator_t* it = llist_first(objs);
    object_t* obj = object_item(it);
    assert_true(obj == instances_child[0]);
    llist_cleanup(objs);
    free(objs);

    objs = object_findObjects(root, "TestRoot?/[13]", -1, search_attr_slash_seperator);
    assert_non_null(objs);
    assert_true(llist_size(objs) == 4);
    it = llist_first(objs);
    obj = object_item(it);
    assert_true(obj == object_parent(instances_child[0]));
    it = llist_iterator_next(it);
    obj = object_item(it);
    assert_true(obj == object_parent(instances_child[2]));
    it = llist_iterator_next(it);
    obj = object_item(it);
    assert_true(obj == instances_child[3]);
    it = llist_iterator_next(it);
    obj = object_item(it);
    assert_true(obj == instances_child[5]);
    llist_cleanup(objs);
    free(objs);

    objs = object_findObjects(testroot[0], "TestRoot?/[13]", -1, search_attr_slash_seperator);
    assert_non_null(objs);
    assert_true(llist_size(objs) == 2);
    llist_cleanup(objs);
    free(objs);

    objs = object_findObjects(testroot[0], "*", -1, search_attr_slash_seperator);
    assert_non_null(objs);
    assert_true(llist_size(objs) == 3);
    llist_cleanup(objs);
    free(objs);

    objs = object_findObjects(testroot[0], "*/*", -1, search_attr_slash_seperator);
    assert_non_null(objs);
    assert_true(llist_size(objs) == 3);
    llist_cleanup(objs);
    free(objs);

    objs = object_findObjects(root, "*", -1, search_attr_slash_seperator);
    assert_non_null(objs);
    assert_true(llist_size(objs) == 5);
    llist_cleanup(objs);
    free(objs);

    objs = object_findObjects(root, "*/*", -1, search_attr_slash_seperator);
    assert_non_null(objs);
    assert_true(llist_size(objs) == 7);
    llist_cleanup(objs);
    free(objs);

    objs = object_findObjects(root, "TestRoot1/*", -1, search_attr_slash_seperator);
    assert_non_null(objs);
    assert_true(llist_size(objs) == 3);
    it = llist_first(objs);
    obj = object_item(it);
    assert_true(obj == object_parent(instances_child[0]));
    it = llist_iterator_next(it);
    obj = object_item(it);
    assert_true(obj == object_parent(instances_child[1]));
    it = llist_iterator_next(it);
    obj = object_item(it);
    assert_true(obj == object_parent(instances_child[2]));
    llist_cleanup(objs);
    free(objs);

    objs = object_findObjects(root, "*/*/*", -1, search_attr_slash_seperator);
    assert_non_null(objs);
    assert_true(llist_size(objs) == 3);
    it = llist_first(objs);
    obj = object_item(it);
    assert_true(obj == instances_child[0]);
    it = llist_iterator_next(it);
    obj = object_item(it);
    assert_true(obj == instances_child[1]);
    it = llist_iterator_next(it);
    obj = object_item(it);
    assert_true(obj == instances_child[2]);
    llist_cleanup(objs);
    free(objs);
}

static void test_obj_find_null_object() {
    object_list_t* objs = object_findObjects(NULL, "TestRoot?.1.*", -1, search_attr_default);
    assert_null(objs);

    object_t* obj = object_item(NULL);
    assert_null(obj);
}

static void test_obj_find_different_depths() {
    object_list_t* objs = object_findObjects(root, "TestRoot?.1.*", 0, search_attr_default);
    assert_non_null(objs);
    assert_true(llist_size(objs) == 0);
    llist_cleanup(objs);
    free(objs);

    objs = object_findObjects(root, "TestRoot?.1.*", 1, search_attr_default);
    assert_non_null(objs);
    assert_true(llist_size(objs) == 0);
    llist_cleanup(objs);
    free(objs);

    objs = object_findObjects(root, "TestRoot?.1.*", 3, search_attr_default);
    assert_non_null(objs);
    assert_true(llist_size(objs) == 1);
    llist_cleanup(objs);
    free(objs);

    objs = object_findObjects(root, "TestRoot?*", 0, search_attr_default);
    assert_non_null(objs);
    assert_true(llist_size(objs) == 0);
    llist_cleanup(objs);
    free(objs);

    objs = object_findObjects(root, "TestRoot?", 1, search_attr_default);
    assert_non_null(objs);
    assert_true(llist_size(objs) == 5);
    llist_cleanup(objs);
    free(objs);

    objs = object_findObjects(root, "TestRoot?", 2, search_attr_default);
    assert_non_null(objs);
    assert_true(llist_size(objs) == 5);
    llist_cleanup(objs);
    free(objs);
}

static void test_obj_find_instance_only() {
    object_list_t* objs = object_findObjects(root, "TestRoot1.*.Child", -1, search_attr_instance_wildcards_only);
    assert_non_null(objs);
    assert_true(llist_size(objs) == 3);
    llist_iterator_t* it = llist_first(objs);
    object_t* obj = object_item(it);
    assert_true(obj == instances_child[0]);
    it = llist_iterator_next(it);
    obj = object_item(it);
    assert_true(obj == instances_child[1]);
    it = llist_iterator_next(it);
    obj = object_item(it);
    assert_true(obj == instances_child[2]);
    llist_cleanup(objs);
    free(objs);

    objs = object_findObjects(root, "TestRoot1.*", -1, search_attr_key_notation | search_attr_instance_wildcards_only);
    assert_non_null(objs);
    assert_true(llist_size(objs) == 3);
    it = llist_first(objs);
    obj = object_item(it);
    assert_true(obj == object_parent(instances_child[0]));
    it = llist_iterator_next(it);
    obj = object_item(it);
    assert_true(obj == object_parent(instances_child[1]));
    it = llist_iterator_next(it);
    obj = object_item(it);
    assert_true(obj == object_parent(instances_child[2]));
    llist_cleanup(objs);
    free(objs);

    objs = object_findObjects(root, "TestRoot1.Key[13].Child", -1, search_attr_key_notation | search_attr_instance_wildcards_only);
    assert_non_null(objs);
    assert_true(llist_size(objs) == 2);
    it = llist_first(objs);
    obj = object_item(it);
    assert_true(obj == instances_child[0]);
    it = llist_iterator_next(it);
    obj = object_item(it);
    assert_true(obj == instances_child[2]);
    llist_cleanup(objs);
    free(objs);

    objs = object_findObjects(root, "TestRoot1.Key[45].Child", -1, search_attr_key_notation | search_attr_instance_wildcards_only);
    assert_non_null(objs);
    assert_true(llist_size(objs) == 0);
    llist_cleanup(objs);
    free(objs);

    objs = object_findObjects(root, "TestRoot?.Key[13].Child", -1, search_attr_key_notation | search_attr_instance_wildcards_only);
    assert_null(objs);
    assert_true(pcb_error == pcb_error_not_instance);

    objs = object_findObjects(root, "TestRoot?.Key1.Child", -1, search_attr_key_notation | search_attr_instance_wildcards_only);
    assert_null(objs);
    assert_true(pcb_error == pcb_error_not_instance);

    objs = object_findObjects(root, "TestRoot2.*", -1, search_attr_instance_wildcards_only);
    assert_null(objs);
    assert_true(pcb_error == pcb_error_not_instance);

    objs = object_findObjects(root, "TestRoot4.Key?", -1, search_attr_key_notation | search_attr_instance_wildcards_only);
    assert_non_null(objs);
    assert_true(llist_size(objs) == 3);
    it = llist_first(objs);
    obj = object_item(it);
    assert_true(obj == instances_child[3]);
    it = llist_iterator_next(it);
    obj = object_item(it);
    assert_true(obj == instances_child[4]);
    it = llist_iterator_next(it);
    obj = object_item(it);
    assert_true(obj == instances_child[5]);
    llist_cleanup(objs);
    free(objs);
}

int main(int argc, char** argv) {
    sahTraceOpen(__FILE__, TRACE_TYPE_STDERR);
    if(!sahTraceIsOpen()) {
        fprintf(stderr, "FAILED to open SAH TRACE\n");
    }
    sahTraceSetLevel(TRACE_LEVEL_WARNING);
    sahTraceSetTimeFormat(TRACE_TIME_APP_SECONDS);
    sahTraceAddZone(sahTraceLevel(), "pcb");

    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_obj_get_index_path),
        cmocka_unit_test(test_obj_get_key_path),
        cmocka_unit_test(test_obj_get_index_slash_path),
        cmocka_unit_test(test_obj_get_key_slash_path),
        cmocka_unit_test(test_obj_get_partial_match),
        cmocka_unit_test(test_obj_get_empty_path),
        cmocka_unit_test(test_obj_get_null_path),
        cmocka_unit_test(test_obj_get_null_object),
        cmocka_unit_test(test_obj_get_invalid_path),
        cmocka_unit_test(test_obj_get_use_path_type_modifiers),
        cmocka_unit_test(test_obj_get_by_key),
        cmocka_unit_test(test_obj_find_index_path),
        cmocka_unit_test(test_obj_find_key_path),
        cmocka_unit_test(test_obj_find_slash_path),
        cmocka_unit_test(test_obj_find_null_object),
        cmocka_unit_test(test_obj_find_different_depths),
        cmocka_unit_test(test_obj_find_instance_only),
    };
    int rc = cmocka_run_group_tests(tests, setup_suite, teardown_suite);
    sahTraceClose();
    return rc;
}
