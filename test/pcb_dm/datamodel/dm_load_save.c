/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <setjmp.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <cmocka.h>

#include <debug/sahtrace.h>

#include "pcb/core/datamodel.h"
#include "pcb/core/serialize.h"

// TYPEDEFS
typedef struct mock_existing_file {
    const char* path;
    int fd;
} mock_existing_file_t;

static mock_existing_file_t mock_existing_files_sentinel = { NULL, -1 };
static mock_existing_file_t* mock_existing_files = &mock_existing_files_sentinel;
mock_existing_file_t* find_existing_file_for_path(const char* path) {
    mock_existing_file_t* i = mock_existing_files;
    while(i && i->path && strcmp(i->path, path) != 0) {
        i++;
    }
    return i;
}

// TEST_FORMAT Serializer
#define TEST_FORMAT         10

static int mock_serializer_load__expected_test_fd;
static bool mock_serializer_load__unexpected_test_fd = false;
bool mock_serializer_load(int fd, object_t* object, const char* filename) {
    if(fd == mock_serializer_load__expected_test_fd) {
        return true;
    } else {
        mock_serializer_load__unexpected_test_fd = true;
        return false;
    }
}

bool mock_serializer_verify(int socketfd, const char* filename, variant_map_t* info, bool* hasDefinition) {
    return true;
}
bool mock_serializer_save(int fd, object_t* object, uint32_t depth, const char* filename) {
    return true;
}

// Static objects
static datamodel_t* dm;

// MOCKS
// int stat(const char *pathname, struct stat *statbuf);
extern int __real_stat(const char* pathname, struct stat* statbuf);
int (* __wrap_stat__current)(const char* pathname, struct stat* statbuf) = __real_stat;
int __wrap_stat(const char* pathname, struct stat* statbuf) {
    return (*__wrap_stat__current)(pathname, statbuf);
}

static int __wrap_stat_fail(const char* pathname, struct stat* statbuf) {
    errno = ENOENT;
    return -1;
}

static int __wrap_stat__check(const char* pathname, struct stat* statbuf) {
    if(find_existing_file_for_path(pathname)->path) {
        statbuf->st_mode = S_IFREG;
        return 0;
    }
    errno = ENOENT;
    return -1;
}

// char *getenv(const char *name);
extern char* __real_getenv(const char* name);
char* (* __wrap_getenv__current)(const char* name) = __real_getenv;
char* __wrap_getenv(const char* name) {
    return (*__wrap_getenv__current)(name);
}

static char* __wrap_getenv__null(const char* name) {
    return NULL;
}

// int open(const char *path, int oflag,...);
extern int __real_open(const char* path, int oflag, ...);
int (* __wrap_open__current)(const char* path, int oflag, ...) = __real_open;
int __wrap_open(const char* path, int oflag, ...) {
    return (*__wrap_open__current)(path, oflag);
}

static int __wrap_open__fail(const char* path, int oflag, ...) {
    errno = ENOENT;
    return -1;
}

static int __wrap_open__set_fd(const char* path, int oflag, ...) {
    return find_existing_file_for_path(path)->fd;
}

// const char *connection_name(connection_info_t *con);
extern const char* __real_connection_name(connection_info_t* con);
const char* (* __wrap_connection_name__current)(connection_info_t* con) = __real_connection_name;
const char* __wrap_connection_name(connection_info_t* con) {
    return (*__wrap_connection_name__current)(con);
}

static const char* __wrap_connection_name_default(connection_info_t* con) {
    return "test";
}


// TESTS
/**
 * Tests loading a file using an absolute path
 */
static void test_dm_load_with_absolute_path(void** state) {
    (void) state;
    mock_serializer_load__expected_test_fd = 15;
    mock_serializer_load__unexpected_test_fd = false;
    mock_existing_file_t __wrap_open__set_fd__expected_struct_local[] = {
        { "/usr/lib/test/test_load.txt", 15 },
        { NULL, -1},
    };
    mock_existing_files = __wrap_open__set_fd__expected_struct_local;
    __wrap_open__current = __wrap_open__set_fd;

    __wrap_stat__current = __wrap_stat__check;

    assert_true(datamodel_load(dm, "/usr/lib/test/test_load.txt", SERIALIZE_FORMAT(TEST_FORMAT, 0, 0)));
    assert_false(mock_serializer_load__unexpected_test_fd);
}

/**
 * Tests searching for a file using a relative filename.
 * The file is looked for in "/etc/defaults/" and in
 * "/usr/lib/test/".
 */
static void test_dm_load_with_relative_path_uses_defaults(void** state) {
    (void) state;
    __wrap_open__current = __wrap_open__set_fd;
    __wrap_stat__current = __wrap_stat__check;

    mock_existing_file_t __wrap_open__set_fd__expected_struct_local[] = {
        { "/etc/defaults/test_load.txt", 15},
        { "test_load.txt", 16 },
        { "/usr/lib/test/test_load.txt", 17 },
        { NULL, -1},
    };
    mock_existing_files = __wrap_open__set_fd__expected_struct_local;
    mock_serializer_load__expected_test_fd = 15;
    assert_true(datamodel_load(dm, "test_load.txt", SERIALIZE_FORMAT(TEST_FORMAT, 0, 0)));
    assert_false(mock_serializer_load__unexpected_test_fd);
}

static void test_dm_load_with_relative_path_uses_filename(void** state) {
    (void) state;
    __wrap_open__current = __wrap_open__set_fd;
    __wrap_stat__current = __wrap_stat__check;

    mock_existing_file_t __wrap_open__set_fd__expected_struct_local[] = {
        { "test_load.txt", 16 },
        { "/usr/lib/test/test_load.txt", 17 },
        { NULL, -1},
    };
    mock_existing_files = __wrap_open__set_fd__expected_struct_local;
    mock_serializer_load__expected_test_fd = 16;
    assert_true(datamodel_load(dm, "test_load.txt", SERIALIZE_FORMAT(TEST_FORMAT, 0, 0)));
    assert_false(mock_serializer_load__unexpected_test_fd);
}

static void test_dm_load_with_relative_path_uses_usr_lib(void** state) {
    (void) state;
    __wrap_open__current = __wrap_open__set_fd;
    __wrap_stat__current = __wrap_stat__check;
    __wrap_connection_name__current = __wrap_connection_name_default;

    mock_existing_file_t __wrap_open__set_fd__expected_struct_local[] = {
        { "/usr/lib/test/test_load.txt", 17 },
        { NULL, -1},
    };
    mock_existing_files = __wrap_open__set_fd__expected_struct_local;
    mock_serializer_load__expected_test_fd = 17;
    assert_true(datamodel_load(dm, "test_load.txt", SERIALIZE_FORMAT(TEST_FORMAT, 0, 0)));
    assert_false(mock_serializer_load__unexpected_test_fd);
}

static void test_dm_load_fails_when_not_found(void** state) {
    (void) state;
    __wrap_open__current = __wrap_open__set_fd;
    __wrap_stat__current = __wrap_stat__check;
    __wrap_connection_name__current = __wrap_connection_name_default;

    mock_existing_file_t __wrap_open__set_fd__expected_struct_local[] = {
        { NULL, -1},
    };
    mock_existing_files = __wrap_open__set_fd__expected_struct_local;
    assert_false(datamodel_load(dm, "test_load.txt", SERIALIZE_FORMAT(TEST_FORMAT, 0, 0)));
}


//typedef int (*CMFixtureFunction)(void **state);
static int init_mocks(void** state) {
    (void) state;
    __wrap_stat__current = __real_stat;
    __wrap_getenv__current = __wrap_getenv__null;
    __wrap_open__current = __real_open;
    __wrap_connection_name__current = __real_connection_name;

    mock_serializer_load__unexpected_test_fd = false;
    mock_existing_files = &mock_existing_files_sentinel;
    return 0;
}

static int cleanup_mocks(void** state) {
    (void) state;
    __wrap_stat__current = __real_stat;
    __wrap_getenv__current = __real_getenv;
    __wrap_open__current = __real_open;
    __wrap_connection_name__current = __real_connection_name;

    mock_serializer_load__unexpected_test_fd = false;
    mock_existing_files = &mock_existing_files_sentinel;
    return 0;
}

static int setup_suite(void** state) {
    (void) state;
    // Setup
    dm = datamodel_create();
    serialization_initialize();

    serialize_handlers_t* handlers = (serialize_handlers_t*) calloc(1, sizeof(serialize_handlers_t));
    if(!handlers) {
        return -1;
    }
    handlers->load = mock_serializer_load;
    handlers->verify = mock_serializer_verify;
    handlers->save = mock_serializer_save;
    serialization_addSerializer(handlers, SERIALIZE_FORMAT(TEST_FORMAT, 0, 0));
    return 0;
}

static int teardown_suite(void** state) {
    (void) state;
    serialization_unregister();
    datamodel_destroy(dm);
    return 0;
}

int main(int argc, char** argv) {
    sahTraceOpen(__FILE__, TRACE_TYPE_STDERR);
    if(!sahTraceIsOpen()) {
        fprintf(stderr, "FAILED to open SAH TRACE\n");
    }
    sahTraceSetLevel(TRACE_LEVEL_WARNING);
    sahTraceSetTimeFormat(TRACE_TIME_APP_SECONDS);
    sahTraceAddZone(sahTraceLevel(), "pcb");

    const struct CMUnitTest tests[] = {
        cmocka_unit_test_setup_teardown(test_dm_load_with_absolute_path, init_mocks, cleanup_mocks),
        cmocka_unit_test_setup_teardown(test_dm_load_with_relative_path_uses_defaults, init_mocks, cleanup_mocks),
        cmocka_unit_test_setup_teardown(test_dm_load_with_relative_path_uses_filename, init_mocks, cleanup_mocks),
        cmocka_unit_test_setup_teardown(test_dm_load_with_relative_path_uses_usr_lib, init_mocks, cleanup_mocks),
        cmocka_unit_test_setup_teardown(test_dm_load_fails_when_not_found, init_mocks, cleanup_mocks),
    };
    int rc = cmocka_run_group_tests(tests, setup_suite, teardown_suite);
    sahTraceClose();
    return rc;
}
