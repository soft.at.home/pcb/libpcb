/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <sys/time.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <stdarg.h>

#include "mock.h"

mock_struct_declare(getenv, 50);
mock_struct_declare(open, 50);
mock_struct_declare(rename, 50);
mock_struct_declare(dirname, 50);
mock_struct_declare(fsync, 50);
mock_struct_declare(close, 50);

typedef char* (* getenv_func_t)(const char* name);
typedef int (* open_func_t)(const char* pathname, int flags, ...);
typedef int (* rename_func_t)(const char* oldpath, const char* newpath);
typedef char* (* dirname_func_t)(char* path);
typedef int (* fsync_func_t)(int fd);
typedef int (* close_func_t)(int fd);

char* getenv(const char* name) {
    if(sahtest_isRunning()) {
        if(strcmp(mock_get_expectation(getenv)->varname, name) != 0) {
            return NULL;
        }
        const char* value = mock_get_expectation(getenv)->value;
        return value? value:NULL;
    }

    getenv_func_t real_getenv = mock_real_function("libc.so.6", "getenv");
    if(!real_getenv) {
        return NULL;
    }

    return real_getenv(name);
}

int open(const char* pathname, int flags, ...) {
    if(sahtest_isRunning()) {
        T_CMP(mock_get_expectation(open)->pathname, pathname);
        T_VERIFY(mock_get_expectation(open)->flags == flags);
        return mock_get_expectation(open)->fd;
    }

    open_func_t real_open = mock_real_function("libc.so.6", "open");
    if(!real_open) {
        return -1;
    }

    va_list arguments;
    va_start(arguments, flags);
    int fd = real_open(pathname, flags, va_arg(arguments, mode_t));
    va_end(arguments);

    return fd;
}

int rename(const char* oldpath, const char* newpath) {
    if(sahtest_isRunning()) {
        assert_string_equals(mock_get_expectation(rename)->oldpath, oldpath);
        assert_string_equals(mock_get_expectation(rename)->newpath, newpath);
        return mock_get_expectation(rename)->retval;
    }

    rename_func_t real_rename = mock_real_function("libc.so.6", "rename");
    if(!real_rename) {
        return -1;
    }

    return real_rename(oldpath, newpath);
}

char* dirname(char* path) {
    if(sahtest_isRunning()) {
        if(strcmp(mock_get_expectation(dirname)->path, path) != 0) {
            sahtest_report(__LINE__, __FILE__, __FUNCTION__, "test failed");
            return ".";
        }
        return mock_get_expectation(dirname)->dirname;
    }

    dirname_func_t real_dirname = mock_real_function("libc.so.6", "dirname");
    if(!real_dirname) {
        return ".";
    }

    return real_dirname(path);
}

int fsync(int fd) {
    if(sahtest_isRunning()) {
        assert_bool(mock_get_expectation(fsync)->fd = fd);
        return mock_get_expectation(fsync)->retval;
    }

    fsync_func_t real_fsync = mock_real_function("libc.so.6", "fsync");
    if(!real_fsync) {
        return -1;
    }

    return real_fsync(fd);
}

int close(int fd) {
    if(sahtest_isRunning()) {
        assert_bool(mock_get_expectation(close)->fd = fd);
        return mock_get_expectation(close)->retval;
    }

    close_func_t real_close = mock_real_function("libc.so.6", "close");
    if(!real_close) {
        return -1;
    }

    return real_close(fd);
}
