/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef MOCK_H
#define MOCK_H

#include <sahtest/sahtest.h>
#include <pcb/core/object.h>
#include <pcb/utils/string_list.h>
#include <pcb/utils/variant_map.h>

mock_struct_begin(getenv)
mock_struct_add(const char* varname)
mock_struct_add(const char* value)
mock_struct_end(getenv)

mock_struct_begin(open)
mock_struct_add(const char* pathname)
mock_struct_add(int flags)
mock_struct_add(int fd)
mock_struct_end(open)

mock_struct_begin(rename)
mock_struct_add(const char* oldpath)
mock_struct_add(const char* newpath)
mock_struct_add(int retval)
mock_struct_end(rename)

mock_struct_begin(dirname)
mock_struct_add(char* path)
mock_struct_add(char* dirname)
mock_struct_end(dirname)

mock_struct_begin(fsync)
mock_struct_add(int fd)
mock_struct_add(int retval)
mock_struct_end(fsync)

mock_struct_begin(close)
mock_struct_add(int fd)
mock_struct_add(int retval)
mock_struct_end(close)

mock_struct_begin(test_load)
mock_struct_add(int fd)
mock_struct_add(object_t * object)
mock_struct_add(const char* filename)
mock_struct_add(bool retval)
mock_struct_end(test_load)

mock_struct_begin(test_verify)
mock_struct_add(int fd)
mock_struct_add(const char* filename)
mock_struct_add(variant_map_t * info)
mock_struct_add(bool* hasDefinition)
mock_struct_add(bool retval)
mock_struct_end(test_verify)

mock_struct_begin(test_save)
mock_struct_add(int fd)
mock_struct_add(object_t * object)
mock_struct_add(uint32_t depth)
mock_struct_add(const char* filename)
mock_struct_add(bool retval)
mock_struct_end(test_save)

#endif // MOCK_H
