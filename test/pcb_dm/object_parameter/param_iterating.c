/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <sahtest/sahtest.h>
#include <pcb/core/datamodel.h>
#include <pcb/core/object.h>
#include <pcb/core/parameter.h>
#include <pcb/utils.h>
#include "datamodel_priv.h"
#include "object_priv.h"
#include "parameter_priv.h"

static datamodel_t dm;
static object_t* root = NULL;
static object_t* object = NULL;
static parameter_t* params[5];

void param_iterating_setup() {
    datamodel_initialize(&dm);
    root = datamodel_root(&dm);
    object = object_create(root, "TestRoot1", object_attr_default);

    params[0] = parameter_create(root, "Foo1", parameter_type_string, parameter_attr_default);
    params[1] = parameter_create(root, "Foo2", parameter_type_string, parameter_attr_default);
    params[2] = parameter_create(root, "Foo3", parameter_type_string, parameter_attr_default);
    params[3] = parameter_create(root, "Foo4", parameter_type_string, parameter_attr_default);
    params[4] = parameter_create(root, "Foo5", parameter_type_string, parameter_attr_default);

}

void param_iterating_teardown() {
    datamodel_cleanup(&dm);
}

int obj_first_parameter() {
    assert_bool(object_firstParameter(root) == params[0]);
    assert_bool(object_firstParameter(object) == NULL);
    assert_bool(object_firstParameter(NULL) == NULL);

    sahtest_success();
}

int obj_last_parameter() {
    assert_bool(object_lastParameter(root) == params[4]);
    assert_bool(object_lastParameter(object) == NULL);
    assert_bool(object_lastParameter(NULL) == NULL);

    sahtest_success();
}

int obj_next_parameter() {
    assert_bool(object_nextParameter(params[0]) == params[1]);
    assert_bool(object_nextParameter(params[1]) == params[2]);
    assert_bool(object_nextParameter(params[2]) == params[3]);
    assert_bool(object_nextParameter(params[3]) == params[4]);
    assert_bool(object_nextParameter(params[4]) == NULL);
    assert_bool(object_nextParameter(NULL) == NULL);

    sahtest_success();
}

int obj_prev_parameter() {
    assert_bool(object_prevParameter(params[0]) == NULL);
    assert_bool(object_prevParameter(params[1]) == params[0]);
    assert_bool(object_prevParameter(params[2]) == params[1]);
    assert_bool(object_prevParameter(params[3]) == params[2]);
    assert_bool(object_prevParameter(params[4]) == params[3]);
    assert_bool(object_prevParameter(NULL) == NULL);

    sahtest_success();
}

int obj_has_parameters() {
    assert_bool(object_hasParameters(root) == true);
    assert_bool(object_hasParameters(NULL) == false);

    sahtest_success();
}

int obj_parameter_count() {
    assert_bool(object_parameterCount(root) == 5);
    assert_bool(object_parameterCount(NULL) == 0);

    sahtest_success();
}

int obj_get_parameter() {
    assert_bool(object_getParameter(root, "Foo1") == params[0]);
    assert_bool(object_getParameter(root, "Foo2") == params[1]);
    assert_bool(object_getParameter(root, "Foo3") == params[2]);
    assert_bool(object_getParameter(root, "Foo4") == params[3]);
    assert_bool(object_getParameter(root, "Foo5") == params[4]);
    assert_bool(object_getParameter(root, "Foo6") == NULL);
    assert_bool(object_getParameter(NULL, "Foo1") == NULL);
    assert_bool(object_getParameter(root, NULL) == NULL);

    sahtest_success();
}
