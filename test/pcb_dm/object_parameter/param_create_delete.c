/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <sahtest/sahtest.h>
#include <pcb/core/datamodel.h>
#include <pcb/core/object.h>
#include <pcb/core/parameter.h>
#include <pcb/utils.h>
#include "datamodel_priv.h"
#include "object_priv.h"
#include "parameter_priv.h"

static datamodel_t dm;
static object_t* root = NULL;
static object_t* templ = NULL;
static object_t* inst = NULL;
static parameter_t* root_param = NULL;
static parameter_t* templ_param = NULL;
static parameter_t* inst_param = NULL;

void param_create_setup() {
    datamodel_initialize(&dm);
    root = datamodel_root(&dm);
    templ = object_create(root, "TestRoot1", object_attr_template);
}

void param_create_teardown() {
    datamodel_cleanup(&dm);
}

void param_delete_setup() {
    param_create_setup();
    root_param = parameter_create(root, "Foo", parameter_type_string, parameter_attr_default);
    templ_param = parameter_create(root, "Foo", parameter_type_string, parameter_attr_default);
    inst = object_createInstance(templ, 5, "Foo");
    inst_param = object_getParameter(inst, "Foo");

    object_commit(root);
}

void param_delete_teardown() {
    datamodel_cleanup(&dm);
}

int param_create() {
    parameter_t* param = parameter_create(root, "Foo", parameter_type_string, parameter_attr_default);
    assert_non_null(param);
    assert_bool(parameter_owner(param) == root);
    assert_bool(parameter_state(param) == parameter_state_created);
    assert_bool(object_commit(root));
    assert_bool(parameter_state(param) == parameter_state_ready);

    param = parameter_create(templ, "Foo", parameter_type_string, parameter_attr_default);
    assert_non_null(param);
    assert_bool(parameter_owner(param) == templ);
    assert_bool(parameter_state(param) == parameter_state_created);
    object_t* instance = object_createInstance(templ, 5, "Foo");
    assert_bool(object_hasParameters(instance));
    parameter_t* instance_param = object_getParameter(instance, "Foo");
    assert_non_null(instance_param);
    assert_bool(parameter_state(instance_param) == parameter_state_validate_created);
    assert_bool(instance_param->definition == param->definition);
    assert_bool(parameter_owner(instance_param) == instance);

    assert_bool(object_commit(root));
    assert_bool(parameter_state(param) == parameter_state_ready);
    assert_bool(parameter_state(instance_param) == parameter_state_ready);

    sahtest_success();
}

int param_create_null_object() {
    parameter_t* param = parameter_create(NULL, "Foo", parameter_type_string, parameter_attr_default);
    assert_null(param);

    assert_null(parameter_owner(NULL));

    sahtest_success();
}

int param_create_empty_name() {
    parameter_t* param = parameter_create(root, "", parameter_type_string, parameter_attr_default);
    assert_null(param);

    sahtest_success();
}

int param_create_invalid_name() {
    parameter_t* param = parameter_create(root, "My.String/Param.", parameter_type_string, parameter_attr_default);
    assert_null(param);

    sahtest_success();
}

int param_create_null_name() {
    parameter_t* param = parameter_create(root, NULL, parameter_type_string, parameter_attr_default);
    assert_null(param);

    sahtest_success();
}

int param_create_duplicate_name() {
    parameter_t* param = parameter_create(root, "Foo", parameter_type_string, parameter_attr_default);
    assert_non_null(param);
    assert_bool(parameter_owner(param) == root);
    param = parameter_create(root, "Foo", parameter_type_string, parameter_attr_default);
    assert_null(param);

    sahtest_success();
}

int param_create_template_only_param() {
    parameter_t* param = parameter_create(templ, "Foo", parameter_type_string, parameter_attr_template_only);
    assert_non_null(param);
    assert_bool(parameter_owner(param) == templ);
    object_t* instance = object_createInstance(templ, 5, "Foo");
    assert_bool(object_hasParameters(instance) == false);

    assert_bool(object_commit(root));

    sahtest_success();
}

int param_copy() {
    parameter_t* param = parameter_create(root, "Foo", parameter_type_string, parameter_attr_default);
    assert_non_null(param);
    assert_bool(parameter_owner(param) == root);

    param = parameter_copy(templ, param);
    assert_non_null(param);
    assert_bool(parameter_owner(param) == templ);

    assert_bool(object_commit(root));

    sahtest_success();
}

int param_copy_parameter_name_already_used() {
    parameter_t* param = parameter_create(root, "Foo", parameter_type_string, parameter_attr_default);
    assert_non_null(param);
    assert_bool(parameter_owner(param) == root);

    param = parameter_copy(root, param);
    assert_null(param);

    sahtest_success();
}

int param_copy_null_source() {
    parameter_t* param = parameter_create(root, "Foo", parameter_type_string, parameter_attr_default);
    assert_non_null(param);
    assert_bool(parameter_owner(param) == root);

    param = parameter_copy(templ, NULL);
    assert_null(param);

    sahtest_success();
}

int param_copy_null_dest() {
    parameter_t* param = parameter_create(root, "Foo", parameter_type_string, parameter_attr_default);
    assert_non_null(param);
    assert_bool(parameter_owner(param) == root);

    param = parameter_copy(NULL, param);
    assert_null(param);

    sahtest_success();
}

int param_delete() {
    assert_bool(parameter_delete(root_param));
    assert_bool(parameter_state(root_param) == parameter_state_deleted);
    assert_bool(object_parameterCount(root) == 1);
    assert_bool(object_commit(root));
    assert_bool(object_parameterCount(root) == 0);

    sahtest_success();
}

int param_delete_wrong_state() {
    parameter_t* param = parameter_create(root, "Bar", parameter_type_string, parameter_attr_default);
    assert_bool(parameter_delete(param) == false);
    sahtest_success();
}

int param_delete_twice() {
    assert_bool(parameter_delete(root_param));
    assert_bool(parameter_state(root_param) == parameter_state_deleted);
    assert_bool(object_parameterCount(root) == 1);
    assert_bool(parameter_delete(root_param));
    assert_bool(parameter_state(root_param) == parameter_state_deleted);
    assert_bool(object_parameterCount(root) == 1);

    sahtest_success();
}

int param_delete_null() {
    assert_bool(parameter_delete(NULL) == false);

    sahtest_success();
}
