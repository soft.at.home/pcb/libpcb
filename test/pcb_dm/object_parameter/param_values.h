/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef PARAM_VALUES_H
#define PARAM_VALUES_H

#include <sahtest/define.h>

void param_values_setup();
void param_values_teardown();

int obj_param_value_string_type();
int obj_param_value_int8_type();
int obj_param_value_int16_type();
int obj_param_value_int32_type();
int obj_param_value_int64_type();
int obj_param_value_uint8_type();
int obj_param_value_uint16_type();
int obj_param_value_uint32_type();
int obj_param_value_uint64_type();
int obj_param_value_bool_type();
int obj_param_value_date_time_type();
int obj_param_value_object_null();
int obj_param_value_parameter_null();

sahtest_group_begin(param_values, param_values_setup, param_values_teardown)
sahtest_add_test(obj_param_value_string_type, "Object parameter get value from a string parameter")
sahtest_add_test(obj_param_value_int8_type, "Object parameter get value from an int8 parameter")
sahtest_add_test(obj_param_value_int16_type, "Object parameter get value from an int16 parameter")
sahtest_add_test(obj_param_value_int32_type, "Object parameter get value from an int32 parameter")
sahtest_add_test(obj_param_value_int64_type, "Object parameter get value from an int64 parameter")
sahtest_add_test(obj_param_value_uint8_type, "Object parameter get value from an uint8 parameter")
sahtest_add_test(obj_param_value_uint16_type, "Object parameter get value from an uint16 parameter")
sahtest_add_test(obj_param_value_uint32_type, "Object parameter get value from an uint32 parameter")
sahtest_add_test(obj_param_value_uint64_type, "Object parameter get value from an uint64 parameter")
sahtest_add_test(obj_param_value_bool_type, "Object parameter get value from a bool parameter")
sahtest_add_test(obj_param_value_date_time_type, "Object parameter get value from a date time parameter")
sahtest_add_test(obj_param_value_object_null, "Object parameter get value from a null object")
sahtest_add_test(obj_param_value_parameter_null, "Object parameter get value using null parameter or empty name")
sahtest_group_end()

#endif // PARAM_VALUES_H
