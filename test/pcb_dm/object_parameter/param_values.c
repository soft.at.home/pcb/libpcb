/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <sahtest/sahtest.h>
#include <pcb/core/datamodel.h>
#include <pcb/core/object.h>
#include <pcb/core/parameter.h>
#include <pcb/utils.h>
#include "datamodel_priv.h"
#include "object_priv.h"
#include "parameter_priv.h"

static datamodel_t dm;
static object_t* root = NULL;
static parameter_t* params[11];

void param_values_setup() {
    datamodel_initialize(&dm);
    root = datamodel_root(&dm);

    params[0] = parameter_create(root, "Foo1", parameter_type_string, parameter_attr_default);
    params[1] = parameter_create(root, "Foo2", parameter_type_int8, parameter_attr_default);
    params[2] = parameter_create(root, "Foo3", parameter_type_int16, parameter_attr_default);
    params[3] = parameter_create(root, "Foo4", parameter_type_int32, parameter_attr_default);
    params[4] = parameter_create(root, "Foo5", parameter_type_int64, parameter_attr_default);
    params[5] = parameter_create(root, "Foo6", parameter_type_uint8, parameter_attr_default);
    params[6] = parameter_create(root, "Foo7", parameter_type_uint16, parameter_attr_default);
    params[7] = parameter_create(root, "Foo8", parameter_type_uint32, parameter_attr_default);
    params[8] = parameter_create(root, "Foo9", parameter_type_uint64, parameter_attr_default);
    params[9] = parameter_create(root, "Foo10", parameter_type_bool, parameter_attr_default);
    params[10] = parameter_create(root, "Foo11", parameter_type_date_time, parameter_attr_default);
}

void param_values_teardown() {
    datamodel_cleanup(&dm);
}

int obj_param_value_string_type() {
    const variant_t* value = parameter_getValue(params[0]);
    assert_non_null(value);
    assert_bool(variant_type(value) == variant_type_string);

    value = object_parameterValue(root, "Foo1");
    assert_non_null(value);
    assert_bool(variant_type(value) == variant_type_string);

    sahtest_success();
}

int obj_param_value_int8_type() {
    const variant_t* value = parameter_getValue(params[1]);
    assert_non_null(value);
    assert_bool(variant_type(value) == variant_type_int8);

    value = object_parameterValue(root, "Foo2");
    assert_non_null(value);
    assert_bool(variant_type(value) == variant_type_int8);

    sahtest_success();
}

int obj_param_value_int16_type() {
    const variant_t* value = parameter_getValue(params[2]);
    assert_non_null(value);
    assert_bool(variant_type(value) == variant_type_int16);

    value = object_parameterValue(root, "Foo3");
    assert_non_null(value);
    assert_bool(variant_type(value) == variant_type_int16);

    sahtest_success();
}

int obj_param_value_int32_type() {
    const variant_t* value = parameter_getValue(params[3]);
    assert_non_null(value);
    assert_bool(variant_type(value) == variant_type_int32);

    value = object_parameterValue(root, "Foo4");
    assert_non_null(value);
    assert_bool(variant_type(value) == variant_type_int32);

    sahtest_success();
}

int obj_param_value_int64_type() {
    const variant_t* value = parameter_getValue(params[4]);
    assert_non_null(value);
    assert_bool(variant_type(value) == variant_type_int64);

    value = object_parameterValue(root, "Foo5");
    assert_non_null(value);
    assert_bool(variant_type(value) == variant_type_int64);

    sahtest_success();
}

int obj_param_value_uint8_type() {
    const variant_t* value = parameter_getValue(params[5]);
    assert_non_null(value);
    assert_bool(variant_type(value) == variant_type_uint8);

    value = object_parameterValue(root, "Foo6");
    assert_non_null(value);
    assert_bool(variant_type(value) == variant_type_uint8);

    sahtest_success();
}

int obj_param_value_uint16_type() {
    const variant_t* value = parameter_getValue(params[6]);
    assert_non_null(value);
    assert_bool(variant_type(value) == variant_type_uint16);

    value = object_parameterValue(root, "Foo7");
    assert_non_null(value);
    assert_bool(variant_type(value) == variant_type_uint16);

    sahtest_success();
}

int obj_param_value_uint32_type() {
    const variant_t* value = parameter_getValue(params[7]);
    assert_non_null(value);
    assert_bool(variant_type(value) == variant_type_uint32);

    value = object_parameterValue(root, "Foo8");
    assert_non_null(value);
    assert_bool(variant_type(value) == variant_type_uint32);

    sahtest_success();
}

int obj_param_value_uint64_type() {
    const variant_t* value = parameter_getValue(params[8]);
    assert_non_null(value);
    assert_bool(variant_type(value) == variant_type_uint64);

    value = object_parameterValue(root, "Foo9");
    assert_non_null(value);
    assert_bool(variant_type(value) == variant_type_uint64);

    sahtest_success();
}

int obj_param_value_bool_type() {
    const variant_t* value = parameter_getValue(params[9]);
    assert_non_null(value);
    assert_bool(variant_type(value) == variant_type_bool);

    value = object_parameterValue(root, "Foo10");
    assert_non_null(value);
    assert_bool(variant_type(value) == variant_type_bool);

    sahtest_success();
}

int obj_param_value_date_time_type() {
    const variant_t* value = parameter_getValue(params[10]);
    assert_non_null(value);
    assert_bool(variant_type(value) == variant_type_date_time);

    value = object_parameterValue(root, "Foo11");
    assert_non_null(value);
    assert_bool(variant_type(value) == variant_type_date_time);

    sahtest_success();
}

int obj_param_value_object_null() {
    const variant_t* value = object_parameterValue(NULL, "Foo1");
    assert_null(value);

    sahtest_success();
}

int obj_param_value_parameter_null() {
    const variant_t* value = parameter_getValue(NULL);
    assert_null(value);
    value = object_parameterValue(root, NULL);
    assert_null(value);
    value = object_parameterValue(root, "");
    assert_null(value);

    sahtest_success();
}

