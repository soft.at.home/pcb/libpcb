/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef PARAM_CREATE_DELETE_H
#define PARAM_CREATE_DELETE_H

#include <sahtest/define.h>

void param_create_setup();
void param_create_teardown();

int param_create();
int param_create_null_object();
int param_create_empty_name();
int param_create_invalid_name();
int param_create_null_name();
int param_create_duplicate_name();
int param_create_template_only_param();
int param_copy();
int param_copy_parameter_name_already_used();
int param_copy_null_source();
int param_copy_null_dest();

void param_delete_setup();
void param_delete_teardown();

int param_delete();
int param_delete_wrong_state();
int param_delete_twice();
int param_delete_null();

sahtest_group_begin(param_create_gr, param_create_setup, param_create_teardown)
sahtest_add_test(param_create, "Create a parameter")
sahtest_add_test(param_create_null_object, "Create a parameter on a null object")
sahtest_add_test(param_create_empty_name, "Create a parameter with an empty name")
sahtest_add_test(param_create_invalid_name, "Create a parameter with an invalid name")
sahtest_add_test(param_create_null_name, "Create a parameter with a null name")
sahtest_add_test(param_create_duplicate_name, "Create a parameters with the same name")
sahtest_add_test(param_create_template_only_param, "Create a template only parameter")
sahtest_add_test(param_copy, "Copy parameter")
sahtest_add_test(param_copy_parameter_name_already_used, "Copy parameter")
sahtest_add_test(param_copy_null_source, "Copy null parameter")
sahtest_add_test(param_copy_null_dest, "Copy to a null destination")
sahtest_group_end()

sahtest_group_begin(param_delete_gr, param_delete_setup, param_delete_teardown)
sahtest_add_test(param_delete, "Delete a parameter")
sahtest_add_test(param_delete_wrong_state, "Delete a parameter which is in the wrong state")
sahtest_add_test(param_delete_twice, "Delete a parameter twice")
sahtest_add_test(param_delete_null, "Delete a null parameter")
sahtest_group_end()

sahtest_group_begin(param_create_delete, NULL, NULL)
sahtest_add_group(param_create_gr, "Creating parameters")
sahtest_add_group(param_delete_gr, "Deleting parameters")
sahtest_group_end()

#endif // PARAM_CREATE_DELETE_H
