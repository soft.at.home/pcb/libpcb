# libpcb

## Summary

Library providing common functionality to the PCB ecosystem.
The functionality includes variants management, connections management, management of the own data model and functions to execute requests and RPC's to other PCB plug-in's.

## Description

This software provides the following functionality sets to PCB applications:
* Utils : API to manage
  * Variants, strings, linked lists, ...
  * Connections, child processes, ...
* Core : API to manage the data model, sending and receiving requests, ...

Each set is built into its own shared object file.
