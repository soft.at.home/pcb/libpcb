-include $(CONFIGDIR)/components.config

export INSTALL ?= install
export PKG_CONFIG_LIBDIR ?= /usr/lib/pkgconfig
export BINDIR ?= /usr/bin
export LIBDIR ?= /usr/lib
export SLIBDIR ?= /usr/lib
export LUALIBDIR ?= /usr/lib/lua
export INCLUDEDIR ?= /usr/include
export INITDIR ?= /etc/init.d
export ACLDIR ?= /etc/acl
export DOCDIR ?= $(D)/usr/share/doc/libpcb
export PROCMONDIR ?= /usr/lib/processmonitor/scripts
export RESETDIR ?= /etc/reset
export MACHINE ?= $(shell $(CC) -dumpmachine)

export COMPONENT = libpcb

compile:
	$(MAKE) -C src pcb_utils
	$(MAKE) -C src pcb_preload
	$(MAKE) -C src pcb_sl
ifeq ($(CONFIG_PCB_OPEN_SSL_SUPPORT),y)
	$(MAKE) -C src pcb_ssl
endif
	$(MAKE) -C src pcb_utils_sendfile
ifneq ($(CONFIG_PCB_UTILS_ONLY),y)
	$(MAKE) -C src pcb_dm
endif
	$(MAKE) -C pkgconfig all

clean:
	$(MAKE) -C src clean
	$(MAKE) -C doc clean
	$(MAKE) -C pkgconfig clean

install:
	$(INSTALL) -d -m 0755 $(D)/$(INCLUDEDIR)/pcb/common
	$(INSTALL) -D -p -m 0644 include/pcb/common/*.h $(D)$(INCLUDEDIR)/pcb/common/
	$(INSTALL) -d -m 0755 $(D)/$(INCLUDEDIR)/pcb/utils
	$(INSTALL) -D -p -m 0644 include/pcb/utils/*.h $(D)$(INCLUDEDIR)/pcb/utils/
	$(INSTALL) -D -p -m 0644 include/pcb/common.h $(D)$(INCLUDEDIR)/pcb/common.h
	$(INSTALL) -D -p -m 0644 include/pcb/utils.h $(D)$(INCLUDEDIR)/pcb/utils.h
ifneq ($(CONFIG_PCB_UTILS_ONLY),y)
	$(INSTALL) -d -m 0755 $(D)/$(INCLUDEDIR)/pcb/core
	$(INSTALL) -D -p -m 0644 include/pcb/core/*.h $(D)$(INCLUDEDIR)/pcb/core/
endif
ifneq ($(CONFIG_PCB_UTILS_ONLY),y)
	$(INSTALL) -D -p -m 0644 include/pcb/core.h $(D)$(INCLUDEDIR)/pcb/core.h
endif
ifneq ($(CONFIG_PCB_UTILS_ONLY),y)
	$(INSTALL) -D -p -m 0644 include/pcb/pcb_client.h $(D)$(INCLUDEDIR)/pcb/pcb_client.h
endif
	$(INSTALL) -D -p -m 0755 src/libpcb_utils.so $(D)/lib/libpcb_utils.so.$(PV)
	ln -sfr $(D)/lib/libpcb_utils.so.$(PV) $(D)/lib/libpcb_utils.so
	$(INSTALL) -D -p -m 0755 src/libpcb_preload.so $(D)/lib/libpcb_preload.so.$(PV)
	ln -sfr $(D)/lib/libpcb_preload.so.$(PV) $(D)/lib/libpcb_preload.so
	$(INSTALL) -D -p -m 0755 src/libpcb_sl.so $(D)/lib/libpcb_sl.so.$(PV)
	ln -sfr $(D)/lib/libpcb_sl.so.$(PV) $(D)/lib/libpcb_sl.so
	$(INSTALL) -D -p -m 0644 src/libpcb_utils.a $(D)/lib/libpcb_utils.a
	$(INSTALL) -D -p -m 0644 src/libpcb_sl.a $(D)/lib/libpcb_sl.a
ifeq ($(CONFIG_PCB_OPEN_SSL_SUPPORT),y)
	$(INSTALL) -D -p -m 0755 src/libpcb_ssl.so $(D)/lib/libpcb_ssl.so.$(PV)
	ln -sfr $(D)/lib/libpcb_ssl.so.$(PV) $(D)/lib/libpcb_ssl.so
endif
	$(INSTALL) -D -p -m 0755 src/libpcb_utils_sendfile.so $(D)/lib/libpcb_utils_sendfile.so.$(PV)
	ln -sfr $(D)/lib/libpcb_utils_sendfile.so.$(PV) $(D)/lib/libpcb_utils_sendfile.so
ifeq ($(CONFIG_PCB_OPEN_SSL_SUPPORT),y)
	$(INSTALL) -D -p -m 0644 src/libpcb_ssl.a $(D)/lib/libpcb_ssl_pic.a
endif
	$(INSTALL) -D -p -m 0644 pkgconfig/pkg-config-utils.pc $(PKG_CONFIG_LIBDIR)/pcb_utils.pc
	$(INSTALL) -D -p -m 0644 pkgconfig/pkg-config-sockets.pc $(PKG_CONFIG_LIBDIR)/pcb_sockets.pc
	$(INSTALL) -D -p -m 0644 pkgconfig/pkg-config-secure-sockets.pc $(PKG_CONFIG_LIBDIR)/pcb_secure_sockets.pc
ifneq ($(CONFIG_PCB_UTILS_ONLY),y)
	$(INSTALL) -D -p -m 0755 src/libpcb_dm.so $(D)/lib/libpcb_dm.so.$(PV)
	ln -sfr $(D)/lib/libpcb_dm.so.$(PV) $(D)/lib/libpcb_dm.so
endif
ifneq ($(CONFIG_PCB_UTILS_ONLY),y)
	$(INSTALL) -D -p -m 0644 src/libpcb_dm.a $(D)/lib/libpcb_dm.a
endif
ifneq ($(CONFIG_PCB_UTILS_ONLY),y)
	$(INSTALL) -D -p -m 0644 pkgconfig/pkg-config.pc $(PKG_CONFIG_LIBDIR)/pcb.pc
endif
ifneq ($(CONFIG_PCB_UTILS_ONLY),y)
	$(INSTALL) -D -p -m 0644 pkgconfig/pkg-config-ssl.pc $(PKG_CONFIG_LIBDIR)/pcbssl.pc
endif
ifneq ($(CONFIG_PCB_UTILS_ONLY),y)
	$(INSTALL) -D -p -m 0644 pkgconfig/pkg-config-sendfile.pc $(PKG_CONFIG_LIBDIR)/pcbsendfile.pc
endif

.PHONY: compile clean install


