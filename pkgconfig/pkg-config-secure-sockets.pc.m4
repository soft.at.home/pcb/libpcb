Name: pcb
Description: PCB Library
Requires: sahtrace pcb_utils ifdef(`CONFIG_PCB_OPEN_SSL_SUPPORT',openssl)
Libs: -lpcb_ssl
Cflags: ifdef(`CONFIG_PCB_OPEN_SSL_SUPPORT',-DOPEN_SSL_SUPPORT)
Version: 0.0.0
