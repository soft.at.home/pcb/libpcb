Name: pcb
Description: PCB Library
Requires: sahtrace pcb_utils pcb_sockets ifdef(`CONFIG_PCB_ACL_USERMNGT', usermngt)
Libs: -lpcb_dm
Cflags: ifdef(`CONFIG_PCB_HELP_SUPPORT', -DPCB_HELP_SUPPORT)
Version: 0.0.0
