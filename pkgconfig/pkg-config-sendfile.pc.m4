Name: pcb
Description: PCB Library
Requires: sahtrace pcb_utils ifdef(`CONFIG_PCB_OPEN_SSL_SUPPORT',pcb_secure_sockets,pcb_sockets)
Libs: -lpcb_utils_sendfile
Cflags: -pthread
Version: 0.0.0
