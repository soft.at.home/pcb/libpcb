Name: pcb
Description: PCB Library
Requires: sahtrace pcb_utils ifdef(`CONFIG_PCB_OPEN_SSL_SUPPORT',pcb_secure_sockets,pcb_sockets) ifdef(`CONFIG_PCB_ACL_USERMNGT', usermngt)
Libs: -lpcb_dm
Cflags: ifdef(`CONFIG_PCB_HELP_SUPPORT', -DPCB_HELP_SUPPORT)
Version: 0.0.0
